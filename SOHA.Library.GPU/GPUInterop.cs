﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Cloo;
using OpenCLTemplate;
using System.Diagnostics;
using System.IO;

namespace SOHA.Library.GPU
{
    public static class GPUInterop
    {
        private static Dictionary<int, List<double>> stats = new Dictionary<int, List<double>>();

        private const bool _DEBUG = false;

        private static bool m_initialized = false;

        public static bool Initialized
        {
            get
            {
                return m_initialized;
            }
        }

        public static void Initialize()
        {
            if(m_initialized) return;

            try
            {
                if(CLCalc.CLAcceleration == CLCalc.CLAccelerationType.Unknown) CLCalc.InitCL();

                //Compiles source code
                CLCalc.Program.Compile(CLFilterSrc.src);

                kernelApplyFilterWorkDim2 = new CLCalc.Program.Kernel("ImgFilter");

                m_initialized = true;
            }
            catch(Exception exc)
            {

                m_initialized = false;
            }
            finally
            {

            }

        }

        #region Apply Filter


        private static float[] FilteredVals;
        private static CLCalc.Program.Variable varFiltered;
        private static CLCalc.Program.Variable varWidth;
        private static CLCalc.Program.Kernel kernelApplyFilterWorkDim2;

        public static void ApplyFilter(CLImage original, CLFilter filter)
        {
            int FILTER_SIZE = filter.Size;

            Stopwatch clock = new Stopwatch();
            clock.Start();

            if(FilteredVals == null || FilteredVals.Length != original.Height * original.Width * 3)
            {
                //Filtered values
                FilteredVals = new float[original.Height * original.Width * 3];
                varFiltered = new CLCalc.Program.Variable(FilteredVals);
            }

            varWidth = new CLCalc.Program.Variable(new int[1]);
            varWidth.WriteToDevice(new int[] { original.Width });
            
            int mean = (filter.Size - 1) / 2;

            CLCalc.Program.Variable[] args = new CLCalc.Program.Variable[] { original.varData, filter.varFilter, varFiltered, varWidth };
            kernelApplyFilterWorkDim2.Execute(args, new int[] { original.Width - filter.Size, original.Height - filter.Size });
            varFiltered.ReadFromDeviceTo(FilteredVals);

            for(int y = mean; y < original.Height - mean - 1; y++)
            {
                int wy = original.Width * y;
                for(int x = mean; x < original.Width - mean - 1; x++)
                {
                    int ind = 3 * (x + wy);
                    original.Data[ind] = (byte)FilteredVals[ind];
                    original.Data[ind + 1] = (byte)FilteredVals[ind + 1];
                    original.Data[ind + 2] = (byte)FilteredVals[ind + 2];
                }
            }

            original.varData.WriteToDevice(original.Data);

            clock.Stop();

            if(!stats.ContainsKey(FILTER_SIZE))
                stats.Add(FILTER_SIZE, new List<double>());

            stats[FILTER_SIZE].Add(clock.ElapsedMilliseconds);

            WriteStats();
        }

        public static void ApplyFilters(CLImage original, List<CLFilter> filters)
        {
            foreach(CLFilter filter in filters)
            {
                ApplyFilter(original, filter);
            }

            WriteStats();
        }

        public static void ApplyFilters(CLImage original, CLFilter filter)
        {
            ApplyFilter(original, filter);

            WriteStats();
        }

        public static void WriteStats()
        {
            if(!_DEBUG) return;

            foreach(KeyValuePair<int, List<double>> kvp in stats)
            {
                using(StreamWriter writer = new StreamWriter(@"C:\stat_out" + kvp.Key + ".csv", true))
                {
                    foreach(double d in kvp.Value)
                        writer.WriteLine(d);
                }
            }
            
        }


        #endregion

    }
}
