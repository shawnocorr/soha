﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.GPU
{
    public static class CLFilterSrc
    {
    
            public static string src = @"

#define FILTERSIZE 7
#define CENTER 3

__kernel void ApplyFilter(__global uchar * ImgData,
                          __global float * Filter,
                          __global float * FilteredImage,
                          __global int   * ImgWidth)

{
   int y = get_global_id(0);
   int w = ImgWidth[0];
   
   //Image values
   float ImgValues[3][FILTERSIZE][FILTERSIZE];

   //Copies filter to local memory
   float localfilter[3][FILTERSIZE][FILTERSIZE];
   
   //Initialization of ImgValues
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ImgValues[0][i][j] = ImgData[3*((y+i)*w+j)];
           ImgValues[1][i][j] = ImgData[3*((y+i)*w+j)+1];
           ImgValues[2][i][j] = ImgData[3*((y+i)*w+j)+2];

           localfilter[0][i][j] = Filter[3*(i*FILTERSIZE + j)];
           localfilter[1][i][j] = Filter[3*(i*FILTERSIZE + j)+1];
           localfilter[2][i][j] = Filter[3*(i*FILTERSIZE + j)+2];
       }
   }
   
   
   int xMax = w-CENTER;
   int ind = 0;
   for (int x = 0; x < xMax; x++)
   {
       //Calculates filtered value
       float4 filteredValue = 0;
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
              float4 pixVals = (float4)(ImgValues[0][i][j],ImgValues[1][i][j],ImgValues[2][i][j],0);
              float4 filterVals = (float4)(localfilter[0][i][j],localfilter[1][i][j],localfilter[2][i][j],0);
              
              filteredValue = pixVals*filterVals+filteredValue;

//              filteredValue.x = mad(ImgValues[0][i][j], localfilter[0][i][j], filteredValue.x);
//              filteredValue.y = mad(ImgValues[1][i][j], localfilter[1][i][j], filteredValue.y);
//              filteredValue.z = mad(ImgValues[2][i][j], localfilter[2][i][j], filteredValue.z);
          }
       }
       ind = 3*((y+CENTER)*w+x+CENTER);       
       FilteredImage[ind] = clamp(filteredValue.x,0.0f,255.0f);
       FilteredImage[ind+1] = clamp(filteredValue.y,0.0f,255.0f);
       FilteredImage[ind+2] = clamp(filteredValue.z,0.0f,255.0f);
   
       //Gets next filter values
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
             ImgValues[0][i][j] = ImgValues[0][i][j+1];
             ImgValues[1][i][j] = ImgValues[1][i][j+1];
             ImgValues[2][i][j] = ImgValues[2][i][j+1];
          }
          ind = 3*((y+i)*w+x+FILTERSIZE-1);
          ImgValues[0][i][FILTERSIZE-1] = ImgData[ind];
          ImgValues[1][i][FILTERSIZE-1] = ImgData[ind+1];
          ImgValues[2][i][FILTERSIZE-1] = ImgData[ind+2];
       }
       
   }    
}

kernel void ImgFilter(global uchar * image,
                      global float * Filter,
                      global float * FilteredImage,
                      global int * Width)
                      
{
   int x = get_global_id(0);
   int y = get_global_id(1);
   int w = Width[0];
   int ind = 0;   
   int ind2 = 0;   

   float4 filteredVal = (float4)(0,0,0,0);
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ind = 3*(x+j + w*(y+i));
           ind2 = 3*(i*FILTERSIZE + j);
           filteredVal.x =  mad(Filter[ind2] ,   (float)image[ind],  filteredVal.x);
           filteredVal.y =  mad(Filter[ind2+1] , (float)image[ind+1],filteredVal.y);
           filteredVal.z =  mad(Filter[ind2+2] , (float)image[ind+2],filteredVal.z);
       }
   }
   ind = 3*(x+CENTER + w*(y+CENTER));
   FilteredImage[ind] = clamp(filteredVal.x,0.0f,255.0f);
   FilteredImage[ind+1] = clamp(filteredVal.y,0.0f,255.0f);
   FilteredImage[ind+2] = clamp(filteredVal.z,0.0f,255.0f);
}
";



            public static string src3 = @"

#define FILTERSIZE 3
#define CENTER 1

__kernel void ApplyFilter(__global uchar * ImgData,
                          __global float * Filter,
                          __global float * FilteredImage,
                          __global int   * ImgWidth)

{
   int y = get_global_id(0);
   int w = ImgWidth[0];
   
   //Image values
   float ImgValues[3][FILTERSIZE][FILTERSIZE];

   //Copies filter to local memory
   float localfilter[3][FILTERSIZE][FILTERSIZE];
   
   //Initialization of ImgValues
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ImgValues[0][i][j] = ImgData[3*((y+i)*w+j)];
           ImgValues[1][i][j] = ImgData[3*((y+i)*w+j)+1];
           ImgValues[2][i][j] = ImgData[3*((y+i)*w+j)+2];

           localfilter[0][i][j] = Filter[3*(i*FILTERSIZE + j)];
           localfilter[1][i][j] = Filter[3*(i*FILTERSIZE + j)+1];
           localfilter[2][i][j] = Filter[3*(i*FILTERSIZE + j)+2];
       }
   }
   
   
   int xMax = w-CENTER;
   int ind = 0;
   for (int x = 0; x < xMax; x++)
   {
       //Calculates filtered value
       float4 filteredValue = 0;
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
              float4 pixVals = (float4)(ImgValues[0][i][j],ImgValues[1][i][j],ImgValues[2][i][j],0);
              float4 filterVals = (float4)(localfilter[0][i][j],localfilter[1][i][j],localfilter[2][i][j],0);
              
              filteredValue = pixVals*filterVals+filteredValue;

//              filteredValue.x = mad(ImgValues[0][i][j], localfilter[0][i][j], filteredValue.x);
//              filteredValue.y = mad(ImgValues[1][i][j], localfilter[1][i][j], filteredValue.y);
//              filteredValue.z = mad(ImgValues[2][i][j], localfilter[2][i][j], filteredValue.z);
          }
       }
       ind = 3*((y+CENTER)*w+x+CENTER);       
       FilteredImage[ind] = clamp(filteredValue.x,0.0f,255.0f);
       FilteredImage[ind+1] = clamp(filteredValue.y,0.0f,255.0f);
       FilteredImage[ind+2] = clamp(filteredValue.z,0.0f,255.0f);
   
       //Gets next filter values
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
             ImgValues[0][i][j] = ImgValues[0][i][j+1];
             ImgValues[1][i][j] = ImgValues[1][i][j+1];
             ImgValues[2][i][j] = ImgValues[2][i][j+1];
          }
          ind = 3*((y+i)*w+x+FILTERSIZE-1);
          ImgValues[0][i][FILTERSIZE-1] = ImgData[ind];
          ImgValues[1][i][FILTERSIZE-1] = ImgData[ind+1];
          ImgValues[2][i][FILTERSIZE-1] = ImgData[ind+2];
       }
       
   }    
}

kernel void ImgFilter(global uchar * image,
                      global float * Filter,
                      global float * FilteredImage,
                      global int * Width)
                      
{
   int x = get_global_id(0);
   int y = get_global_id(1);
   int w = Width[0];
   int ind = 0;   
   int ind2 = 0;   

   float4 filteredVal = (float4)(0,0,0,0);
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ind = 3*(x+j + w*(y+i));
           ind2 = 3*(i*FILTERSIZE + j);
           filteredVal.x =  mad(Filter[ind2] ,   (float)image[ind],  filteredVal.x);
           filteredVal.y =  mad(Filter[ind2+1] , (float)image[ind+1],filteredVal.y);
           filteredVal.z =  mad(Filter[ind2+2] , (float)image[ind+2],filteredVal.z);
       }
   }
   ind = 3*(x+CENTER + w*(y+CENTER));
   FilteredImage[ind] = clamp(filteredVal.x,0.0f,255.0f);
   FilteredImage[ind+1] = clamp(filteredVal.y,0.0f,255.0f);
   FilteredImage[ind+2] = clamp(filteredVal.z,0.0f,255.0f);
}
";


    }
}
