﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cloo;
using OpenCLTemplate;
using System.Xml.Serialization;

namespace SOHA.Library.GPU
{
    [XmlRoot("Filter")]
    public class CLFilter
    {

        [XmlIgnore()]
        public CLCalc.Program.Variable varFilter;

        public int Size { get; set; }
        [XmlArray("Filters")]
        [XmlArrayItem("float")]
        public float[] FilterValues { get; set; }


        public CLFilter() { }

        public CLFilter(float[] filter)
        {
            Size = (int)Math.Sqrt(filter.Length / 3);

            if(filter.Length != 3 * Size * Size)
                throw new Exception("Invalid filter");

            GPUInterop.Initialize();

            FilterValues = filter;

            varFilter = new CLCalc.Program.Variable(new float[3 * Size * Size]);
            varFilter.WriteToDevice(filter);

        }

        //#region IFilter Members

        //public CLImage PerformFilter(CLImage image)
        //{
        //    GPUInterop.ApplyFilter(image, this);
        //    return image;
        //}


        //#endregion

    }
}
