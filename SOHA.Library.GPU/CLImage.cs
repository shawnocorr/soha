﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using OpenCLTemplate;

namespace SOHA.Library.GPU
{
    public class CLImage : ICloneable
    {
        public readonly Bitmap Bmp;
        private const int PIXELSIZE = 3;

        public byte[] Data;
        public CLCalc.Program.Variable varData;

        private int m_width;
        private int m_height;

        public int Width
        {
            get { return m_width; }
        }
        public int Height
        {
            get { return m_height; }
        }

        public CLImage(Bitmap bmp)
        {
            Bmp = bmp;

            if(CLCalc.CLAcceleration == CLCalc.CLAccelerationType.Unknown) CLCalc.InitCL();

            m_width = bmp.Width;
            m_height = bmp.Height;

            if(bmp.PixelFormat != PixelFormat.Format24bppRgb)
                throw new Exception("Bad pixel format");

            Data = new byte[3 * m_width * m_height];
            ReadToLocalData(bmp);
            varData = new CLCalc.Program.Variable(Data);
            varData.WriteToDevice(Data);
        }

        private void ReadToLocalData(Bitmap bmp)
        {
            //Lock bits
            BitmapData bmd = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                             System.Drawing.Imaging.ImageLockMode.ReadOnly, bmp.PixelFormat);
            //Read data
            unsafe
            {
                for(int y = 0; y < bmd.Height; y++)
                {
                    byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

                    for(int x = 0; x < bmd.Width; x++)
                    {
                        Data[3 * (x + m_width * y)] = row[x * PIXELSIZE];
                        Data[3 * (x + m_width * y) + 1] = row[x * PIXELSIZE + 1];
                        Data[3 * (x + m_width * y) + 2] = row[x * PIXELSIZE + 2];
                    }
                }
            }

            //Unlock bits
            bmp.UnlockBits(bmd);
        }

        public Bitmap GetStoredBitmap(Bitmap bmp)
        {
            Bitmap resp = new Bitmap(m_width, m_height, bmp.PixelFormat);

            BitmapData bmd = resp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
             System.Drawing.Imaging.ImageLockMode.ReadOnly, bmp.PixelFormat);

            //Write data
            unsafe
            {
                for(int y = 0; y < bmd.Height; y++)
                {
                    byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

                    for(int x = 0; x < bmd.Width; x++)
                    {
                        row[x * PIXELSIZE] = Data[3 * (x + m_width * y)];
                        row[x * PIXELSIZE + 1] = Data[3 * (x + m_width * y) + 1];
                        row[x * PIXELSIZE + 2] = Data[3 * (x + m_width * y) + 2];
                    }
                }
            }

            //Unlock bits
            resp.UnlockBits(bmd);

            return resp;
        }

        public static CLImage Hypot(CLImage A, CLImage B)
        {
            Bitmap resp = new Bitmap(A.Width, A.Height, A.Bmp.PixelFormat);

            BitmapData bmd = resp.LockBits(new Rectangle(0, 0, A.Width, A.Height),
             System.Drawing.Imaging.ImageLockMode.ReadOnly, A.Bmp.PixelFormat);

            //Write data
            unsafe
            {
                for(int y = 0; y < bmd.Height; y++)
                {
                    byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

                    for(int x = 0; x < bmd.Width; x++)
                    {
                        byte x1 = A.Data[3 * (x + A.Width * y)];
                        byte y1 = B.Data[3 * (x + B.Width * y)];

                        byte r = (byte)Math.Sqrt(x1 * x1 + y1 * y1);

                        row[x * PIXELSIZE] = r;
                        row[x * PIXELSIZE + 1] = r;
                        row[x * PIXELSIZE + 2] = r;
                    }
                }
            }

            //Unlock bits
            resp.UnlockBits(bmd);

            return new CLImage(resp);
        }

        public static CLImage Arctan(CLImage A, CLImage B)
        {
            Bitmap resp = new Bitmap(A.Width, A.Height, A.Bmp.PixelFormat);

            BitmapData bmd = resp.LockBits(new Rectangle(0, 0, A.Width, A.Height),
             System.Drawing.Imaging.ImageLockMode.ReadOnly, A.Bmp.PixelFormat);

            //Write data
            unsafe
            {
                for(int y = 0; y < bmd.Height; y++)
                {
                    byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

                    for(int x = 0; x < bmd.Width; x++)
                    {
                        byte x1 = A.Data[3 * (x + A.Width * y)];
                        byte y1 = B.Data[3 * (x + B.Width * y)];

                        double radians = Math.Atan2(y1, x1);

                        byte r = (byte)Math.Atan2(y1,x1);

                        if(radians < Math.PI / 4)
                        {
                            r = 0;
                        }
                        else if(radians < Math.PI / 2)
                        {
                            r = 85;
                        }
                        else if(radians < 3 * Math.PI / 2)
                        {
                            r = 170;
                        }
                        else if(radians <= Math.PI)
                        {
                            r = 255;
                        }

                        row[x * PIXELSIZE] = r;
                        row[x * PIXELSIZE + 1] = r;
                        row[x * PIXELSIZE + 2] = r;
                    }
                }
            }

            //Unlock bits
            resp.UnlockBits(bmd);

            return new CLImage(resp);
        }

        public static CLImage NonMaxSuppess(CLImage A, CLImage B)
        {
            Bitmap resp = new Bitmap(A.Width, A.Height, A.Bmp.PixelFormat);

            BitmapData bmd = resp.LockBits(new Rectangle(0, 0, A.Width, A.Height),
             System.Drawing.Imaging.ImageLockMode.ReadOnly, A.Bmp.PixelFormat);

            //Write data
            unsafe
            {
                for(int y = 1; y < bmd.Height - 1; y++)
                {
                    byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

                    for(int x = 1; x < bmd.Width - 1; x++)
                    {
                        byte x1 = A.Data[3 * (x + A.Width * y)];
                        byte y1 = B.Data[3 * (x + B.Width * y)];

                        double radians = Math.Atan2(y1, x1);

                        byte r = (byte)0;

                        if(y1 == 0)
                        {
                            r = (byte)(x1 > A.Data[3 * ((x - 1) + A.Width * y)]
                                && x1 > A.Data[3 * ((x + 1) + A.Width * y)] ? 255 : 0);
                        }
                        else if(y1 == 85)
                        {
                            r = (byte)(x1 > A.Data[3 * ((x - 1) + A.Width * (y - 1))]
                                && x1 > A.Data[3 * ((x + 1) + A.Width * (y + 1))] ? 255 : 0);
                        }
                        else if(y1 == 170)
                        {
                            r = (byte)(x1 > A.Data[3 * (x + A.Width * (y - 1))]
                                && x1 > A.Data[3 * (x + A.Width * (y + 1))] ? 255 : 0);
                        }
                        else if(y1 == 255)
                        {
                            r = (byte)(x1 > A.Data[3 * ((x + 1) + A.Width * (y - 1))]
                                && x1 > A.Data[3 * ((x - 1) + A.Width * (y + 1))] ? 255 : 0);
                        }

                        row[x * PIXELSIZE] = r;
                        row[x * PIXELSIZE + 1] = r;
                        row[x * PIXELSIZE + 2] = r;
                    }
                }
            }

            //Unlock bits
            resp.UnlockBits(bmd);

            return new CLImage(resp);
        }

        #region ICloneable Members

        public object Clone()
        {
            return new CLImage(this.Bmp);
        }

        #endregion
    }
}
