﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tracing
{
    public class ExceptionMessage : BMessage
    {
        public Exception Exception { get; protected set; }

        public ExceptionMessage(Exception exc)
        {
            Exception = exc;
        }

        public override void Write(System.IO.StreamWriter writer)
        {
            writer.Write(Exception.ToString());
        }
    }
}
