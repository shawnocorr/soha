﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tracing
{
    public class LogMessage : BMessage
    {

        public override void Write(System.IO.StreamWriter writer)
        {
            writer.Write(DateTime.Now.ToString("hh_mm_ss.fff") + "\t");
        }

    }
}
