﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Tracing
{
    public class Log
    {

        private object m_lock_log = new object();

        private string m_folder_path;
        private bool m_initialized = false;

        private string m_log_path;
        private string m_exception_path;
        private string m_journal_path;

        public Log(string folder_path)
        {
            m_folder_path = folder_path;

            m_log_path = m_folder_path + @"\Log";
            m_exception_path = m_folder_path + @"\Exceptions";
            m_journal_path = m_folder_path + @"\Journals";

            Directory.CreateDirectory(m_log_path);
            Directory.CreateDirectory(m_exception_path);
            Directory.CreateDirectory(m_journal_path);

            m_initialized = true;
        }

        public void WriteLog(LogMessage msg)
        {

            if(!m_initialized)
                throw new SystemException("Log not Initialized");

            lock(m_lock_log)
            {
                string log_file_name = m_log_path + @"\" + DateTime.Today.ToString("yyyy_MM_dd");

                using(StreamWriter writer = new StreamWriter(log_file_name, true))
                {
                    msg.Write(writer);
                }
            }
        }

        public void WriteException(ExceptionMessage msg)
        {

            if(!m_initialized)
                throw new SystemException("Log not Initialized");

            lock(m_lock_log)
            {
                string exclog_file_name = m_log_path + @"\" + DateTime.Today.ToString("yyyy_MM_dd");

                Directory.CreateDirectory(exclog_file_name);

                exclog_file_name += msg.Exception.GetType();

                using(StreamWriter writer = new StreamWriter(exclog_file_name, true))
                {
                    msg.Write(writer);
                }
            }
        }

        

    }
}
