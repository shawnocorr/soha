﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Drawing;
using BaseObjects;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Mathematics;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

namespace VideoInteropDLL
{
    public class AVIVideo : Video
    {
        #region AVI DLL IMPORTS

        public const int StreamtypeVIDEO = 1935960438; //mmioStringToFOURCC("vids", 0)
        public const int OF_SHARE_DENY_WRITE = 32;
        public const int BMP_MAGIC_COOKIE = 19778; //ascii string "BM"

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RECT
        {
            public UInt32 left;
            public UInt32 top;
            public UInt32 right;
            public UInt32 bottom;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct BITMAPINFOHEADER
        {
            public UInt32 biSize;
            public Int32 biWidth;
            public Int32 biHeight;
            public Int16 biPlanes;
            public Int16 biBitCount;
            public UInt32 biCompression;
            public UInt32 biSizeImage;
            public Int32 biXPelsPerMeter;
            public Int32 biYPelsPerMeter;
            public UInt32 biClrUsed;
            public UInt32 biClrImportant;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AVISTREAMINFO
        {
            public UInt32 fccType;
            public UInt32 fccHandler;
            public UInt32 dwFlags;
            public UInt32 dwCaps;
            public UInt16 wPriority;
            public UInt16 wLanguage;
            public UInt32 dwScale;
            public UInt32 dwRate;
            public UInt32 dwStart;
            public UInt32 dwLength;
            public UInt32 dwInitialFrames;
            public UInt32 dwSuggestedBufferSize;
            public UInt32 dwQuality;
            public UInt32 dwSampleSize;
            public RECT rcFrame;
            public UInt32 dwEditCount;
            public UInt32 dwFormatChangeCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public UInt16[] szName;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct BITMAPFILEHEADER
        {
            public Int16 bfType; //"magic cookie" - must be "BM"
            public Int32 bfSize;
            public Int16 bfReserved1;
            public Int16 bfReserved2;
            public Int32 bfOffBits;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AVICOMPRESSOPTIONS
        {
            public UInt32 fccType;
            public UInt32 fccHandler;
            public UInt32 dwKeyFrameEvery;  // only used with AVICOMRPESSF_KEYFRAMES
            public UInt32 dwQuality;
            public UInt32 dwBytesPerSecond; // only used with AVICOMPRESSF_DATARATE
            public UInt32 dwFlags;
            public IntPtr lpFormat;
            public UInt32 cbFormat;
            public IntPtr lpParms;
            public UInt32 cbParms;
            public UInt32 dwInterleaveEvery;
        }

        //Initialize the AVI library

        [DllImport("avifil32.dll")]
        public static extern void AVIFileInit();

        //Open an AVI file

        [DllImport("avifil32.dll", PreserveSig = true)]
        public static extern int AVIFileOpen(
            ref int ppfile,
            String szFile,
            int uMode,
            int pclsidHandler);

        //Get a stream from an open AVI file

        [DllImport("avifil32.dll")]
        public static extern int AVIFileGetStream(
            int pfile,
            out IntPtr ppavi,
            int fccType,
            int lParam);

        [DllImport("avifil32.dll")]
        private static extern int AVIFileCreateStream(
            int pfile,
            out IntPtr ppavi,
            ref AVISTREAMINFO ptr_streaminfo);

        [DllImport("avifil32.dll")]
        private static extern int AVIMakeCompressedStream(
          out IntPtr ppsCompressed, IntPtr aviStream, ref AVICOMPRESSOPTIONS ao, int dummy);

        [DllImport("avifil32.dll")]
        private static extern int AVIStreamSetFormat(
          IntPtr aviStream, Int32 lPos, ref BITMAPINFOHEADER lpFormat, Int32 cbFormat);

        //Release an open AVI stream

        [DllImport("avifil32.dll")]
        public static extern int AVIStreamRelease(IntPtr aviStream);

        //Release an ope AVI file

        [DllImport("avifil32.dll")]
        public static extern int AVIFileRelease(int pfile);

        //Close the AVI library

        [DllImport("avifil32.dll")]
        public static extern void AVIFileExit();

        //Get the start position of a stream

        [DllImport("avifil32.dll", PreserveSig = true)]
        public static extern int AVIStreamStart(int pavi);

        //Get the length of a stream in frames

        [DllImport("avifil32.dll", PreserveSig = true)]
        public static extern int AVIStreamLength(int pavi);

        //Get header information about an open stream

        [DllImport("avifil32.dll")]
        public static extern int AVIStreamInfo(
            int pAVIStream,
            ref AVISTREAMINFO psi,
            int lSize);

        //Get a pointer to a GETFRAME object (returns 0 on error)

        [DllImport("avifil32.dll")]
        public static extern int AVIStreamGetFrameOpen(
            IntPtr pAVIStream,
            ref BITMAPINFOHEADER bih);

        //Get a pointer to a packed DIB (returns 0 on error)

        [DllImport("avifil32.dll")]
        public static extern int AVIStreamGetFrame(
            int pGetFrameObj,
            int lPos);

        //Release the GETFRAME object

        [DllImport("avifil32.dll")]
        public static extern int AVIStreamGetFrameClose(int pGetFrameObj);

        [DllImport("avifil32.dll")]
        private static extern int AVIStreamWrite(
            IntPtr aviStream,
            Int32 lStart,
            Int32 lSamples,
            IntPtr lpBuffer,
            Int32 cbBuffer,
            Int32 dwFlags,
            Int32 dummy1,
            Int32 dummy2);

        [DllImport("avifil32.dll")]
        public static extern int AVIStreamRead(
            IntPtr pavi,
            Int32 lStart,
            Int32 lSamples,
            IntPtr lpBuffer,
            Int32 cbBuffer,
            Int32 plBytes,
            Int32 plSamples);

        [DllImport("avifil32.dll")]
        private static extern bool AVISaveOptions(
            IntPtr hwnd,
            UInt32 uiFlags,
            Int32 nStreams,
            ref IntPtr ppavi,
            ref AVICOMPRESSOPTIONS_CLASS plpOptions);

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public class AVICOMPRESSOPTIONS_CLASS
        {
            public UInt32 fccType;
            public UInt32 fccHandler;
            public UInt32 dwKeyFrameEvery;
            public UInt32 dwQuality;
            public UInt32 dwBytesPerSecond;
            public UInt32 dwFlags;
            public IntPtr lpFormat;
            public UInt32 cbFormat;
            public IntPtr lpParms;
            public UInt32 cbParms;
            public UInt32 dwInterleaveEvery;

            public AVICOMPRESSOPTIONS ToStruct()
            {
                AVICOMPRESSOPTIONS returnVar = new AVICOMPRESSOPTIONS();
                returnVar.fccType = this.fccType;
                returnVar.fccHandler = this.fccHandler;
                returnVar.dwKeyFrameEvery = this.dwKeyFrameEvery;
                returnVar.dwQuality = this.dwQuality;
                returnVar.dwBytesPerSecond = this.dwBytesPerSecond;
                returnVar.dwFlags = this.dwFlags;
                returnVar.lpFormat = this.lpFormat;
                returnVar.cbFormat = this.cbFormat;
                returnVar.lpParms = this.lpParms;
                returnVar.cbParms = this.cbParms;
                returnVar.dwInterleaveEvery = this.dwInterleaveEvery;
                return returnVar;
            }
        }

        protected enum AviMode : int
        {
            WRITE = 0x01,
            READWRITE = 0x02,
            SHARE_DENY_WRITE = 0x32,
            DENY_WRITE = 0x20,
            CREATE = 0x4096
        }

        protected enum BMPCompressionType : uint
        {
            BI_RGB = 0,
            BI_RLE8,
            BI_RLE4,
            BI_BITFIELDS, //Also Huffman 1D compression for BITMAPCOREHEADER2
            BI_JPEG,      //Also RLE-24 compression for BITMAPCOREHEADER2
            BI_PNG,
        }

        protected enum AviCompressionTypes : uint
        {

        }

        #endregion

        private const int PALETTE_SIZE = 4 * 256;

        private int m_root;
        private int m_firstframe;
        private IntPtr m_stream;
        private AVISTREAMINFO m_stream_info;
        private BITMAPINFOHEADER m_bmp_info;

        public AVIVideo() { }

        public AVIVideo(string path)
        {
            m_video_path = path;

            Open();

            int result = AVIStreamInfo(m_stream.ToInt32(), ref m_stream_info, Marshal.SizeOf(m_stream_info));
            if (result != 0)
                throw new Exception("Error getting AVI stream info.");

            m_firstframe = AVIStreamStart(m_stream.ToInt32());
            m_total_frames = (int)m_stream_info.dwLength;
            m_fps = (int)(m_stream_info.dwRate / m_stream_info.dwScale);
            m_frame_height = (int)m_stream_info.rcFrame.bottom;
            m_frame_width = (int)m_stream_info.rcFrame.right;

            //m_bmp_info.biBitCount = 0;
            //m_bmp_info.biClrImportant = 0;
            //m_bmp_info.biClrUsed = 0;
            //m_bmp_info.biCompression = 0;
            //m_bmp_info.biHeight = 0;      //(Int32)(m_stream_info.rcFrame.bottom);
            //m_bmp_info.biWidth = 0;      //(Int32)m_stream_info.rcFrame.right;
            //m_bmp_info.biPlanes = 1;
            //m_bmp_info.biSize = (UInt32)Marshal.SizeOf(m_bmp_info);
            ////m_bih.biSizeImage = (uint)(m_bih.biWidth * m_bih.biHeight * 2);
            //m_bmp_info.biXPelsPerMeter = 0;
            //m_bmp_info.biYPelsPerMeter = 0;

            #region Get First Frame & Set Cropping

            short[] bmpcounts = new short[] { 8, 24 };

            m_bmp_info.biBitCount = 24;
            m_bmp_info.biClrImportant = 0;
            m_bmp_info.biClrUsed = 0;
            m_bmp_info.biCompression = (uint)BMPCompressionType.BI_RGB;
            m_bmp_info.biHeight = (Int32)(m_stream_info.rcFrame.bottom);
            m_bmp_info.biWidth = (Int32)m_stream_info.rcFrame.right;
            m_bmp_info.biPlanes = 1;
            m_bmp_info.biSize = (UInt32)Marshal.SizeOf(m_bmp_info);
            //m_bih.biSizeImage = (uint)(m_bih.biWidth * m_bih.biHeight * 2);
            m_bmp_info.biXPelsPerMeter = 0;
            m_bmp_info.biYPelsPerMeter = 0;

            int frameObj = 0;
            for (int i = 0; i < bmpcounts.Length; i++)
            {
                m_bmp_info.biBitCount = bmpcounts[i];
                frameObj = AVIStreamGetFrameOpen(m_stream, ref m_bmp_info);

                if (frameObj != 0)
                    break;
            }

            if (frameObj == 0)
                OnFrameComplete(this, Frame.BlankFrame(400,100));
                //OnFrameGrabComplete(new Frame(new Size(400, 100), 1));

            #region Standard AVI File

            result = AVIStreamGetFrame(frameObj, m_firstframe);

            //IntPtr
            //AVIStreamRead(m_stream, 1, 1, buff, (int)m_bih.biSizeImage, 0, 0);

            BITMAPINFOHEADER bih = (BITMAPINFOHEADER)Marshal.PtrToStructure(new IntPtr(result), m_bmp_info.GetType());

            //Copy the image
            byte[] bitmapData;
            int address = result + Marshal.SizeOf(bih);
            if (bih.biBitCount < 16)
            {
                bitmapData = new byte[bih.biSizeImage + PALETTE_SIZE];
            }
            else
            {
                bitmapData = new byte[bih.biSizeImage];
            }
            Marshal.Copy(new IntPtr(address), bitmapData, 0, bitmapData.Length);


            //Copy bitmap info
            byte[] bitmapInfo = new byte[Marshal.SizeOf(bih)];
            IntPtr ptr;
            ptr = Marshal.AllocHGlobal(bitmapInfo.Length);
            Marshal.StructureToPtr(bih, ptr, false);
            address = ptr.ToInt32();
            Marshal.Copy(new IntPtr(address), bitmapInfo, 0, bitmapInfo.Length);


            Marshal.FreeHGlobal(ptr);

            BITMAPFILEHEADER bfh = new BITMAPFILEHEADER();
            bfh.bfType = BMP_MAGIC_COOKIE;
            bfh.bfSize = (Int32)(55 + bih.biSizeImage);
            bfh.bfReserved1 = 0;
            bfh.bfReserved2 = 0;
            bfh.bfOffBits = Marshal.SizeOf(bih) + Marshal.SizeOf(bfh);
            if (bih.biBitCount < 16)
                bfh.bfOffBits += PALETTE_SIZE;

            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write(bfh.bfType);
            bw.Write(bfh.bfSize);
            bw.Write(bfh.bfReserved1);
            bw.Write(bfh.bfReserved2);
            bw.Write(bfh.bfOffBits);
            bw.Write(bitmapInfo);
            bw.Write(bitmapData);

            Bitmap bmp = (Bitmap)Image.FromStream(bw.BaseStream);
            Bitmap saveableBitmap = new Bitmap(bmp.Width, bmp.Height);
            Graphics g = Graphics.FromImage(saveableBitmap);
            g.DrawImage(bmp, 0, 0);
            g.Dispose();
            bmp.Dispose();
            g = null;
            bmp = null;

            bw.Close();

            Frame frame = new Frame(saveableBitmap, 1, m_fps);
            saveableBitmap.Dispose();
            saveableBitmap = null;
            if (frame.Size != new Size(m_frame_width,m_frame_height))
            {
                Frame.Crop(ref frame, new Rectangle(new Point(0, 0), new Size(m_frame_width,m_frame_height)));
                //frame = frame.GetRegion(new Rectangle(new Point(0, 0), m_framesize));
            }

            Size new_size = frame.TrimSize();
            m_frame_height = new_size.Height;
            m_frame_width = new_size.Width;

            #endregion

            #endregion

            //m_framesize = GetFrame(1).TrimSize();
            //m_frame_times = new double[m_frames];

            Close();
        }

        private static uint CreateFourCC(string code)
        {
            if (code.Length != 4)
                return 0;

            return (uint)(((byte)code[0]) | ((byte)code[1] << 8) | ((byte)code[2] << 16) | ((byte)code[3] << 24));
        }

        public static AVIVideo FromVideo(Video video)
        {
            AVIVideo avi = new AVIVideo();
            avi.m_fps = video.Fps;
            avi.m_total_frames = video.Frames;
            avi.m_frame_height = video.Height;
            avi.m_frame_width = video.Width;
            avi.m_video_path = video.VideoPath;
            avi.m_camera = video.Camera;
            avi.m_frame_times = video.FrameTimes;

            return avi;
        }

        public override void Open()
        {
            if (!m_opened)
            {
                if (!File.Exists(m_video_path)) throw new VideoFileNotFoundException("File Not Found: " + m_video_path);

                m_root = 0;
                int result = AVIFileOpen(ref m_root, m_video_path, (int)AviMode.DENY_WRITE, 0);

                if (result != 0)
                    throw new Exception("Error opening AVI file.");

                result = AVIFileGetStream(m_root, out m_stream, StreamtypeVIDEO, 0);
                if (result != 0)
                    throw new Exception("Error getting AVI stream.");

                result = AVIStreamInfo(m_stream.ToInt32(), ref m_stream_info, Marshal.SizeOf(m_stream_info));
                if (result != 0)
                    throw new Exception("Error getting AVI stream info.");

                m_firstframe = AVIStreamStart(m_stream.ToInt32());
            }
        }

        public override void Close()
        {
            int ret;
            if(m_stream != null && m_stream.ToInt32() != 0)
                ret = AVIStreamRelease(m_stream);

            AVIFileExit();
        }

        public override void GetFrame(int p_frame)
        {
            Open();

            short[] bmpcounts = new short[] { 8,16,24 };

            int frameObj = 0;

            m_bmp_info.biCompression = (uint)BMPCompressionType.BI_RGB;
            m_bmp_info.biHeight = (Int32)(m_stream_info.rcFrame.bottom);
            m_bmp_info.biWidth = (Int32)(m_stream_info.rcFrame.right);
            m_bmp_info.biPlanes = 1;
            m_bmp_info.biSize = (UInt32)Marshal.SizeOf(m_bmp_info);

            for (int i = 0; i < bmpcounts.Length; i++)
            {
                m_bmp_info.biBitCount = bmpcounts[i];
                frameObj = AVIStreamGetFrameOpen(m_stream, ref m_bmp_info);

                if (frameObj != 0)
                    break;
            }

            if (frameObj == 0)
            {
                OnFrameComplete(this, Frame.BlankFrame(400, 100));
                return;
            }


            int result = AVIStreamGetFrame(frameObj, p_frame);
            if (result == 0)
            {
                OnFrameComplete(this, Frame.BlankFrame(400, 100));
                return;
            }

            //IntPtr
            //AVIStreamRead(m_stream, 1, 1, buff, (int)m_bih.biSizeImage, 0, 0);

            BITMAPINFOHEADER bih = (BITMAPINFOHEADER)Marshal.PtrToStructure(new IntPtr(result), m_bmp_info.GetType());

            //Copy the image
            byte[] bitmapData;
            int address = result + Marshal.SizeOf(bih);
            if (bih.biBitCount < 16)
            {
                bitmapData = new byte[bih.biSizeImage + PALETTE_SIZE];
            }
            else
            {
                bitmapData = new byte[bih.biSizeImage];
            }
            Marshal.Copy(new IntPtr(address), bitmapData, 0, bitmapData.Length);


            //Copy bitmap info
            byte[] bitmapInfo = new byte[Marshal.SizeOf(bih)];
            IntPtr ptr;
            ptr = Marshal.AllocHGlobal(bitmapInfo.Length);
            Marshal.StructureToPtr(bih, ptr, false);
            address = ptr.ToInt32();
            Marshal.Copy(new IntPtr(address), bitmapInfo, 0, bitmapInfo.Length);


            Marshal.FreeHGlobal(ptr);

            BITMAPFILEHEADER bfh = new BITMAPFILEHEADER();
            bfh.bfType = BMP_MAGIC_COOKIE;
            bfh.bfSize = (Int32)(55 + bih.biSizeImage);
            bfh.bfReserved1 = 0;
            bfh.bfReserved2 = 0;
            bfh.bfOffBits = Marshal.SizeOf(bih) + Marshal.SizeOf(bfh);
            if (bih.biBitCount < 16)
                bfh.bfOffBits += PALETTE_SIZE;

            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write(bfh.bfType);
            bw.Write(bfh.bfSize);
            bw.Write(bfh.bfReserved1);
            bw.Write(bfh.bfReserved2);
            bw.Write(bfh.bfOffBits);
            bw.Write(bitmapInfo);
            bw.Write(bitmapData);

            Bitmap bmp = (Bitmap)Image.FromStream(bw.BaseStream);
            Bitmap saveableBitmap = new Bitmap(bmp.Width, bmp.Height);
            Graphics g = Graphics.FromImage(saveableBitmap);
            g.DrawImage(bmp, 0, 0);
            g.Dispose();
            bmp.Dispose();
            g = null;
            bmp = null;

            bw.Close();

            Frame frame = new Frame(saveableBitmap, p_frame, m_fps);
            saveableBitmap.Dispose();
            saveableBitmap = null;
            if (frame.Size != new Size(m_frame_width, m_frame_height))
            {
                Frame.Crop(ref frame, new Rectangle(new Point(0, 0), new Size(m_frame_width, m_frame_height)));
                //frame = frame.GetRegion(new Rectangle(new Point(0, 0), m_framesize));
            }

            OnFrameComplete(this, frame);
        }

        public override Frame GetFrame_Sync(int p_frame)
        {
            Open();

            short[] bmpcounts = new short[] { 8, 24 };

            int frameObj = 0;
            for (int i = 0; i < bmpcounts.Length; i++)
            {
                m_bmp_info.biBitCount = bmpcounts[i];
                frameObj = AVIStreamGetFrameOpen(m_stream, ref m_bmp_info);

                if (frameObj != 0)
                    break;
            }

            if (frameObj == 0)
                OnFrameComplete(this, Frame.BlankFrame(400, 100));


            int result = AVIStreamGetFrame(frameObj, p_frame);

            //IntPtr
            //AVIStreamRead(m_stream, 1, 1, buff, (int)m_bih.biSizeImage, 0, 0);

            BITMAPINFOHEADER bih = (BITMAPINFOHEADER)Marshal.PtrToStructure(new IntPtr(result), m_bmp_info.GetType());

            //Copy the image
            byte[] bitmapData;
            int address = result + Marshal.SizeOf(bih);
            if (bih.biBitCount < 16)
            {
                bitmapData = new byte[bih.biSizeImage + PALETTE_SIZE];
            }
            else
            {
                bitmapData = new byte[bih.biSizeImage];
            }
            Marshal.Copy(new IntPtr(address), bitmapData, 0, bitmapData.Length);


            //Copy bitmap info
            byte[] bitmapInfo = new byte[Marshal.SizeOf(bih)];
            IntPtr ptr;
            ptr = Marshal.AllocHGlobal(bitmapInfo.Length);
            Marshal.StructureToPtr(bih, ptr, false);
            address = ptr.ToInt32();
            Marshal.Copy(new IntPtr(address), bitmapInfo, 0, bitmapInfo.Length);


            Marshal.FreeHGlobal(ptr);

            BITMAPFILEHEADER bfh = new BITMAPFILEHEADER();
            bfh.bfType = BMP_MAGIC_COOKIE;
            bfh.bfSize = (Int32)(55 + bih.biSizeImage);
            bfh.bfReserved1 = 0;
            bfh.bfReserved2 = 0;
            bfh.bfOffBits = Marshal.SizeOf(bih) + Marshal.SizeOf(bfh);
            if (bih.biBitCount < 16)
                bfh.bfOffBits += PALETTE_SIZE;

            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write(bfh.bfType);
            bw.Write(bfh.bfSize);
            bw.Write(bfh.bfReserved1);
            bw.Write(bfh.bfReserved2);
            bw.Write(bfh.bfOffBits);
            bw.Write(bitmapInfo);
            bw.Write(bitmapData);

            Bitmap bmp = (Bitmap)Image.FromStream(bw.BaseStream);
            Bitmap saveableBitmap = new Bitmap(bmp.Width, bmp.Height);
            Graphics g = Graphics.FromImage(saveableBitmap);
            g.DrawImage(bmp, 0, 0);
            g.Dispose();
            bmp.Dispose();
            g = null;
            bmp = null;

            bw.Close();

            Frame frame = new Frame(saveableBitmap, p_frame, m_fps);
            saveableBitmap.Dispose();
            saveableBitmap = null;
            if (frame.Size != new Size(m_frame_width, m_frame_height))
            {
                Frame.Crop(ref frame, new Rectangle(new Point(0, 0), new Size(m_frame_width, m_frame_height)));
                //frame = frame.GetRegion(new Rectangle(new Point(0, 0), m_framesize));
            }

            Close();

            return frame;
        }

        private byte[] bitmapData;
        private void GetFrame(int framePtr, ref Frame frame, int i)
        {
            int result = AVIStreamGetFrame(framePtr, m_firstframe + (i - 1));
            BITMAPINFOHEADER bih = (BITMAPINFOHEADER)Marshal.PtrToStructure(new IntPtr(result), m_bmp_info.GetType());

            //Copy the image
            int address = result + Marshal.SizeOf(bih);
            if (bih.biBitCount < 16)
            {
                bitmapData = new byte[bih.biSizeImage + PALETTE_SIZE];
            }
            else
            {
                bitmapData = new byte[bih.biSizeImage];
            }
            Marshal.Copy(new IntPtr(address), bitmapData, 0, bitmapData.Length);


            //Copy bitmap info
            byte[] bitmapInfo = new byte[Marshal.SizeOf(bih)];
            IntPtr ptr;
            ptr = Marshal.AllocHGlobal(bitmapInfo.Length);
            Marshal.StructureToPtr(bih, ptr, false);
            address = ptr.ToInt32();
            Marshal.Copy(new IntPtr(address), bitmapInfo, 0, bitmapInfo.Length);


            Marshal.FreeHGlobal(ptr);

            BITMAPFILEHEADER bfh = new BITMAPFILEHEADER();
            bfh.bfType = BMP_MAGIC_COOKIE;
            bfh.bfSize = (Int32)(55 + bih.biSizeImage);
            bfh.bfReserved1 = 0;
            bfh.bfReserved2 = 0;
            bfh.bfOffBits = Marshal.SizeOf(bih) + Marshal.SizeOf(bfh);
            if (bih.biBitCount < 16)
                bfh.bfOffBits += PALETTE_SIZE;

            BinaryWriter bw = new BinaryWriter(new MemoryStream());
            bw.Write(bfh.bfType);
            bw.Write(bfh.bfSize);
            bw.Write(bfh.bfReserved1);
            bw.Write(bfh.bfReserved2);
            bw.Write(bfh.bfOffBits);
            bw.Write(bitmapInfo);
            bw.Write(bitmapData);

            Bitmap bmp = (Bitmap)Image.FromStream(bw.BaseStream);
            Bitmap saveableBitmap = new Bitmap(bmp.Width, bmp.Height);
            Graphics g = Graphics.FromImage(saveableBitmap);
            g.DrawImage(bmp, 0, 0);
            g.Dispose();
            bmp.Dispose();
            g = null;
            bmp = null;

            bw.Close();

            frame = new Frame(saveableBitmap, i, m_fps);
            saveableBitmap.Dispose();
            saveableBitmap = null;
            Size frame_size = new Size(this.Width, this.Height);
            if (frame.Size != frame_size)
            {
                Frame.Crop(ref frame, new Rectangle(new Point(0, 0), frame_size));
                //frame = frame.GetRegion(new Rectangle(new Point(0, 0), m_framesize));
            }
        }

        public override void GetMMode(int p_x_coordinate, MModeType type)
        {
            frmProcessNotify frmProgress = new frmProcessNotify("Gathering MMode...", "Gathering MMode");
            frmProgress.TopMost = true;
            frmProgress.Show();

            Frame frame = new Frame();

            Open();

            MMode ret_mmode = new MMode();

            ret_mmode.PixelValues = new double[m_total_frames, this.Height];
            ret_mmode.XCoordinate = p_x_coordinate;

            //m_frame_times = new double[m_total_frames + 1];

            int n = 1;
            switch (type)
            {
                case MModeType.Snapshot:
                    n = 1000;
                    break;
                case MModeType.Full:
                    n = m_total_frames;
                    break;
                default:
                    break;
            }

            for (int i = 1; i < n; i++)
            {

                #region Cancellation

                Application.DoEvents();

                if (frmProgress.IsCancelled)
                {
                    frmProgress.IsCancelled = false;
                    frmProgress.Close();
                    Close();
                    OnMModeComplete(this, null);
                    return;
                }

                #endregion

                
                frame = GetFrame_Sync(i);
                for (int y = 0; y < ret_mmode.Size.Height; y++)
                {
                    ret_mmode[i - 1, y] = frame[ret_mmode.XCoordinate, y];
                }

                int pct_complete = i * 100 / n;
                frmProgress.UpdateProgress(pct_complete);
            }

            frmProgress.Close();
            Close();

            OnMModeComplete(this, ret_mmode);
        }

        public override void GetMMode(int p_x_coordinate, int start_frame, int end_frame)
        {
            if (start_frame < 1 || end_frame > m_total_frames) throw new Exception("Invalid MMode Interval.");

            int N = end_frame - start_frame + 1;

            frmProcessNotify frmProgress = new frmProcessNotify("Gathering MMode...","Gathering MMode");
            frmProgress.TopMost = true;
            frmProgress.Show();

            Frame frame = new Frame();

            Open();

            MMode ret_mmode = new MMode();

            ret_mmode.PixelValues = new double[N, this.Height];
            ret_mmode.XCoordinate = p_x_coordinate;

            for (int i = start_frame; i < end_frame; i++)
            {

                #region Cancellation

                Application.DoEvents();

                if (frmProgress.IsCancelled)
                {
                    frmProgress.IsCancelled = false;
                    frmProgress.Close();
                    Close();
                    OnMModeComplete(this, null);
                    return;
                }

                #endregion


                frame = GetFrame_Sync(i);
                for (int y = 0; y < ret_mmode.Size.Height; y++)
                {
                    ret_mmode[i - 1, y] = frame[ret_mmode.XCoordinate, y];
                }

                int pct_complete = (i - start_frame) * 100 / (end_frame - start_frame + 1);
                frmProgress.UpdateProgress(pct_complete);
            }

            frmProgress.Close();
            Close();

            OnMModeComplete(this, ret_mmode);
        }

        public override MMode GetMMode(int p_x_coordinate)
        {
            Frame frame = new Frame();

            Open();

            MMode ret_mmode = new MMode();

            ret_mmode.PixelValues = new double[m_total_frames, this.Height];
            ret_mmode.XCoordinate = p_x_coordinate;

            //m_frame_times = new double[m_total_frames + 1];

            int n = m_total_frames;

            for (int i = 1; i < n; i++)
            {


                frame = GetFrame_Sync(i);
                for (int y = 0; y < ret_mmode.Size.Height; y++)
                {
                    ret_mmode[i - 1, y] = frame[ret_mmode.XCoordinate, y];
                }

            }

            Close();

            return ret_mmode;
        }

        public override void GetPreprocess(List<Roi> rois)
        {
            PreprocessData[] data_out = new PreprocessData[1];

            try
            {
                if (rois.Count == 0)
                {
                    rois.Add(new Roi(0, 0, new Size(this.Width, this.Height)));
                }

                Frame[] frame_cache = new Frame[2];
                data_out = new PreprocessData[rois.Count];
                for (int i = 0; i < data_out.Length; i++)
                {
                    data_out[i] = new PreprocessData(this.Frames);
                }

                Open();

                int frameObj = 0;

                short[] bmpcounts = new short[] { 8, 24 };

                m_bmp_info.biBitCount = 24;
                m_bmp_info.biClrImportant = 0;
                m_bmp_info.biClrUsed = 0;
                m_bmp_info.biCompression = (uint)BMPCompressionType.BI_RGB;
                m_bmp_info.biHeight = (Int32)(m_stream_info.rcFrame.bottom);
                m_bmp_info.biWidth = (Int32)m_stream_info.rcFrame.right;
                m_bmp_info.biPlanes = 1;
                m_bmp_info.biSize = (UInt32)Marshal.SizeOf(m_bmp_info);
                //m_bih.biSizeImage = (uint)(m_bih.biWidth * m_bih.biHeight * 2);
                m_bmp_info.biXPelsPerMeter = 0;
                m_bmp_info.biYPelsPerMeter = 0;

                for (int i = 0; i < bmpcounts.Length; i++)
                {
                    m_bmp_info.biBitCount = bmpcounts[i];
                    frameObj = AVIStreamGetFrameOpen(m_stream, ref m_bmp_info);

                    if (frameObj != 0)
                        break;
                }

                for (int n = 0; n < data_out.Length; n++)
                {
                    data_out[n].MMode.PixelValues = new double[m_total_frames, rois[n].Height];
                    data_out[n].MMode.XCoordinate = rois[n].X + rois[n].Width / 2;

                    m_frame_times = new double[m_total_frames];

                    Frame f = new Frame();
                    GetFrame(frameObj, ref f, 1);
                    Frame.Crop(ref f, (Rectangle)rois[n]);
                    frame_cache[1] = f;

                    Frame mmode_frame = new Frame();
                    GetFrame(frameObj, ref mmode_frame, 1);

                    for (int i = 1; i <= m_total_frames; i++)
                    {
                        if (m_cancel_process)
                        {
                            m_cancel_process = false;
                            OnPreprocessComplete(this, null);
                            return;
                        }

                        //cFrame = GetFrame(i).GetRegion((Rectangle)roi[n]);
                        //cFrame = GetFrame(i);

                        frame_cache[0] = frame_cache[1];

                        if (i < m_total_frames)
                        {
                            f = new Frame();
                            GetFrame(frameObj, ref f, i + 1);
                            Frame.Crop(ref f, (Rectangle)rois[n]);
                            frame_cache[1] = f;
                        }

                        if (i > 1)
                        {
                            mmode_frame = new Frame();
                            GetFrame(frameObj, ref mmode_frame, i);
                        }
                        for (int y = 0; y < data_out[n].MMode.Size.Height; y++)
                        {
                            data_out[n].MMode[i - 1, y] = mmode_frame[data_out[n].MMode.XCoordinate, y];
                        }

                        //Frame f = new Frame();

                        //double std = frame_cache.Current.Mean();
                        //double std_smooth = frame_cache.Current.Mean();

                        //double area = frame_cache.Current.Values.Length;
                        double angle = 0;
                        double sum_bright = 0;
                        double max_relative_change = 0;
                        double sum_intensity = 0;
                        double sum_phase = 0;
                        for (int j = 0; j < frame_cache[0].PixelValues.GetLength(0); j++)
                        {
                            for (int k = 0; k < frame_cache[0].PixelValues.GetLength(1); k++)
                            {
                                #region FrameBrightness
                                sum_bright += -frame_cache[0][j, k];
                                #endregion

                                #region 2nd Order Functions
                                if (i < m_total_frames)
                                {
                                    #region PixelIntensityChange

                                    double delta = 0;
                                    delta += Math.Abs(frame_cache[1][j, k] - frame_cache[0][j, k]);
                                    sum_intensity += delta;

                                    #endregion
                                }
                                #endregion
                            }
                        }

                        m_frame_times[i - 1] = (i - 1) / (double)this.m_fps;
                        data_out[n].Darkness[i - 1] = (double)(sum_bright);
                        if (i < m_total_frames)
                        {
                            data_out[n].Intensity[i - 1] = (double)(sum_intensity);
                        }

                        //Send Progress

                        int progress = (int)(i / (double)m_total_frames * 100 * ((double)(n + 1) / data_out.Length));
                    }

                    #region Eliminate Intensity +/- 2 Std Dev

                    //data_out[n].Intensity = ArrayFunctions.PlusMinusWindow(data_out[n].Intensity, 3);

                    #endregion

                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error in AVI:PreProcess(): " + exc.Message);
            }
            finally
            {
                Close();

                OnPreprocessComplete(this, data_out);
            }
        }

        public override void GetFrameTimes()
        {
            try
            {
                Open();

                this.m_frame_times = new double[m_total_frames];
                
                for (int i = 1; i <= m_total_frames; i++)
                {
                    this.FrameTimes[i - 1] = (i - 1) / (double)this.m_fps;
                }

            }
            catch (Exception exc)
            {

            }
            finally
            {
                Close();
            }
        }

        public override void Run(object arg, CancellationToken cancel_token)
        {
            Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;


            BindingList<Roi> rois = arg as BindingList<Roi>;
            Stopwatch stopwatch = Stopwatch.StartNew();
            PreprocessData[] data_out = new PreprocessData[1];
            int progress = 0;
            Exception exc_return = null;
            bool cancelled = false;
            bool error = false;
            Logs.LogTrace("Began Run(): [" + Thread.CurrentThread.ManagedThreadId + "] " + this.VideoPath);

            try
            {
                if (rois.Count == 0)
                {
                    rois.Add(new Roi(0, 0, new Size(this.Width, this.Height)));
                }

                Frame[] frame_cache = new Frame[2];
                data_out = new PreprocessData[rois.Count];
                for (int i = 0; i < data_out.Length; i++)
                {
                    data_out[i] = new PreprocessData(this.Frames);
                }

                Open();

                int frameObj = 0;

                short[] bmpcounts = new short[] { 8, 24 };

                m_bmp_info.biBitCount = 24;
                m_bmp_info.biClrImportant = 0;
                m_bmp_info.biClrUsed = 0;
                m_bmp_info.biCompression = (uint)BMPCompressionType.BI_RGB;
                m_bmp_info.biHeight = (Int32)(m_stream_info.rcFrame.bottom);
                m_bmp_info.biWidth = (Int32)m_stream_info.rcFrame.right;
                m_bmp_info.biPlanes = 1;
                m_bmp_info.biSize = (UInt32)Marshal.SizeOf(m_bmp_info);
                //m_bih.biSizeImage = (uint)(m_bih.biWidth * m_bih.biHeight * 2);
                m_bmp_info.biXPelsPerMeter = 0;
                m_bmp_info.biYPelsPerMeter = 0;

                for (int i = 0; i < bmpcounts.Length; i++)
                {
                    m_bmp_info.biBitCount = bmpcounts[i];
                    frameObj = AVIStreamGetFrameOpen(m_stream, ref m_bmp_info);

                    if (frameObj != 0)
                        break;
                }

                for (int n = 0; n < data_out.Length; n++)
                {
                    data_out[n].MMode.PixelValues = new double[m_total_frames, rois[n].Height];
                    data_out[n].MMode.XCoordinate = rois[n].X + rois[n].Width / 2;

                    m_frame_times = new double[m_total_frames];

                    Frame f = new Frame();
                    GetFrame(frameObj, ref f, 1);
                    Frame.Crop(ref f, (Rectangle)rois[n]);
                    frame_cache[1] = f;

                    Frame mmode_frame = new Frame();
                    GetFrame(frameObj, ref mmode_frame, 1);

                    for (int i = 1; i <= m_total_frames; i++)
                    {
                        if (cancel_token.IsCancellationRequested)
                        {
                            cancel_token.ThrowIfCancellationRequested();
                            //m_cancel_process = false;
                            //OnPreprocessComplete(this, null);
                            //return;
                        }

                        frame_cache[0] = frame_cache[1];

                        if (i < m_total_frames)
                        {
                            f = new Frame();
                            GetFrame(frameObj, ref f, i + 1);
                            Frame.Crop(ref f, (Rectangle)rois[n]);
                            frame_cache[1] = f;
                        }

                        if (i > 1)
                        {
                            mmode_frame = new Frame();
                            GetFrame(frameObj, ref mmode_frame, i);
                        }
                        for (int y = 0; y < data_out[n].MMode.Size.Height; y++)
                        {
                            data_out[n].MMode[i - 1, y] = mmode_frame[data_out[n].MMode.XCoordinate, y];
                        }

                        //Frame f = new Frame();

                        //double std = frame_cache.Current.Mean();
                        //double std_smooth = frame_cache.Current.Mean();

                        //double area = frame_cache.Current.Values.Length;
                        double angle = 0;
                        double sum_bright = 0;
                        double max_relative_change = 0;
                        double sum_intensity = 0;
                        double sum_phase = 0;
                        for (int j = 0; j < frame_cache[0].PixelValues.GetLength(0); j++)
                        {
                            for (int k = 0; k < frame_cache[0].PixelValues.GetLength(1); k++)
                            {
                                #region FrameBrightness
                                sum_bright += -frame_cache[0][j, k];
                                #endregion

                                #region 2nd Order Functions
                                if (i < m_total_frames)
                                {
                                    #region PixelIntensityChange

                                    double delta = 0;
                                    delta += Math.Abs(frame_cache[1][j, k] - frame_cache[0][j, k]);
                                    sum_intensity += delta;

                                    #endregion
                                }
                                #endregion
                            }
                        }

                        m_frame_times[i - 1] = (i - 1) / (double)this.m_fps;
                        data_out[n].Darkness[i - 1] = (double)(sum_bright);
                        if (i < m_total_frames)
                        {
                            data_out[n].Intensity[i - 1] = (double)(sum_intensity);
                        }

                        //Send Progress
                        int new_progress = i * 100 / m_total_frames;
                        if (progress < new_progress)
                        {
                            progress = new_progress;
                            OnProgressUpdate(this, progress / data_out.Length);
                        }
                    }

                    #region Eliminate Intensity +/- 2 Std Dev

                    //data_out[n].Intensity = ArrayFunctions.PlusMinusWindow(data_out[n].Intensity, 3);

                    #endregion

                }
                Close();
            }
            catch (AggregateException agg_exc)
            {

            }
            catch (OperationCanceledException op_cancel_exc)
            {
                cancelled = true;
                data_out = new PreprocessData[1];
                error = false;
                exc_return = op_cancel_exc;
            }
            catch (Exception exc)
            {
                Logs.LogException(exc);
                exc_return = exc;
                error = true;
            }


            Logs.LogTrace("[" + Thread.CurrentThread.ManagedThreadId + "] Processing took " + stopwatch.ElapsedMilliseconds
                    + " milliseconds. [" + m_total_frames + " Frames, " + m_total_frames * rois[0].Area() / stopwatch.ElapsedMilliseconds * 1000 + " pps]");

            OnComplete(new ProgressEventArgs(data_out, false, false, null));

        }

        public static unsafe void WriteAvi(Video video, double threshold, string path)
        {
            int root = 0;
            IntPtr ppavi = new IntPtr(0);
            IntPtr ppcompressedavi = new IntPtr(0);
            int frame_rate = 0; int start = 1; int end = 1;

            #region Get Recorded Framerate

            frmUserInputFramerate frm = new frmUserInputFramerate(video.Fps, video.Frames);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                frame_rate = frm.NewFrameRate;
                start = frm.Start;
                end = frm.End;
            }
            else
            {
                return;
            }

            #endregion
                        
            frmProcessNotify frmProgress = new frmProcessNotify("Writing AVI...", "Writing AVI");
            try
            {
                unsafe
                {

                    AVIFileExit();
                    AVIFileInit();

                    /* Open/Create AVI */
                    int result = AVIFileOpen(ref root, path, 4097/*(int)(AviMode.WRITE | AviMode.CREATE)*/, 0);
                    if (result != 0)
                        throw new Exception("Error Writing AVI: Could Not Create AVI.");

                    /* Create Stream */
                    Frame prevF = video.GetFrame_Sync(1);
                    //prevF.To8bGrey();
                    Bitmap initBmp = prevF.ToBitmap();
                    //Bitmap initBmp = prevF.To16bGrey();
                    //initBmp.Save(@"C:\test.bmp", ImageFormat.Bmp);
                    BitmapData bmpdata = initBmp.LockBits(new Rectangle(0, 0, initBmp.Width, initBmp.Height), ImageLockMode.ReadOnly, initBmp.PixelFormat);

                    AVISTREAMINFO stream_info = new AVISTREAMINFO();
                    stream_info.fccType = AVIVideo.StreamtypeVIDEO;
                    stream_info.fccHandler = CreateFourCC("divx");//1668707181; //808810089 : use with -1 dwQuality
                    stream_info.dwScale = 1;
                    stream_info.dwRate = (uint)(frame_rate);
                    stream_info.dwSuggestedBufferSize = (uint)(bmpdata.Stride * bmpdata.Height);
                    stream_info.dwQuality = 0; /* 0xffffffff : -1, default |  */
                    stream_info.rcFrame.right = (uint)prevF.Size.Width;
                    stream_info.rcFrame.bottom = (uint)prevF.Size.Height;

                    result = AVIFileCreateStream(root, out ppavi, ref stream_info);
                    if (result != 0)
                        throw new Exception("Error Writing AVI: Could Not Create Stream.");

                    /* Set Options */
                    AVICOMPRESSOPTIONS options = new AVICOMPRESSOPTIONS();
                    switch (Path.GetExtension(video.VideoPath).ToUpper())
                    {
                        case ".CXD":
                            break;
                        case ".AVI":
                            options.fccType = 0;
                            options.fccHandler = 1684633208;//1987410281;
                            options.dwKeyFrameEvery = 0;
                            options.dwQuality = 0;
                            options.dwFlags = 8;
                            options.lpFormat = new IntPtr(0);
                            options.cbFormat = 0;
                            options.lpParms = new IntPtr(0);
                            options.cbParms = 3532;//56;
                            break;
                        default:
                            throw new Exception("Null Video Type Reference.");
                    }

                    AVICOMPRESSOPTIONS_CLASS clsAviCompress = new AVICOMPRESSOPTIONS_CLASS();
                    clsAviCompress.fccType = (uint)StreamtypeVIDEO;
                    clsAviCompress.lpParms = IntPtr.Zero;
                    clsAviCompress.lpFormat = IntPtr.Zero;

                    MessageBox.Show("Please select the Intel IYUV Video Codec... Or play with other options at your own risk.");
                    bool cont = AVISaveOptions(IntPtr.Zero, 3, 1, ref ppavi, ref clsAviCompress);

                    if (cont == false)
                        throw new Exception("Operation Cancelled by User.");

                    AVICOMPRESSOPTIONS options2 = clsAviCompress.ToStruct();
                    result = AVIMakeCompressedStream(out ppcompressedavi, ppavi, ref options2, 0);
                    uint err_code = (uint)result;
                    if (result != 0)
                        throw new Exception("Error Writing AVI: Could Not Make Compressed Stream.");

                    BITMAPINFOHEADER bmp_header = new BITMAPINFOHEADER();
                    bmp_header.biSize = (uint)Marshal.SizeOf(bmp_header);
                    bmp_header.biWidth = (Int32)prevF.Size.Width;
                    bmp_header.biHeight = (Int32)prevF.Size.Height;
                    bmp_header.biPlanes = 1;
                    bmp_header.biBitCount = 24;
                    bmp_header.biCompression = (uint)BMPCompressionType.BI_RGB;
                    bmp_header.biSizeImage = (uint)(bmpdata.Stride * bmpdata.Height);
                    bmp_header.biXPelsPerMeter = 0;
                    bmp_header.biYPelsPerMeter = 0;
                    bmp_header.biClrUsed = 0;
                    bmp_header.biClrImportant = 0;

                    result = AVIStreamSetFormat(ppcompressedavi, 0, ref bmp_header, Marshal.SizeOf(bmp_header));
                    if (result != 0)
                        throw new Exception("Error Writing AVI: Could Not Set Format.");

                    frmProgress.TopMost = true;
                    frmProgress.Show();
                    Frame currentF;
                    for (int i = start + 1; i <= end; i++)
                    {
                        currentF = video.GetFrame_Sync(i);
                        Bitmap test = currentF.ToBitmap();
                        Bitmap redF = Frame.ToRedDotMovie(prevF, currentF, threshold);
                        BitmapData bdata = redF.LockBits(new Rectangle(0, 0, redF.Width, redF.Height), ImageLockMode.ReadOnly, redF.PixelFormat);

                        result = AVIStreamWrite(ppcompressedavi, i - 2, 1,
                            bdata.Scan0, (Int32)(bdata.Stride * bdata.Height), 0, 0, 0);

                        if (result != 0)
                            throw new Exception("Error Writing AVI: Frame " + i);

                        redF.UnlockBits(bdata);

                        if (frmProgress.IsCancelled) throw new Exception("Cancelled.");

                        int pct_complete = (i - start) * 100 / (end - start + 1);
                        frmProgress.UpdateProgress(pct_complete);

                        Application.DoEvents();
                        prevF = currentF;
                    }
                }
            }
            catch (Exception exc)
            {
                if (exc.Message == "Cancelled.")
                {
                    frmProgress.IsCancelled = false;
                    frmProgress.Close();
                }
                MessageBox.Show(exc.Message, "Error Writing Avi");
            }
            finally
            {
                if (ppavi.ToInt32() != 0)
                    AVIStreamRelease(ppavi);

                if (ppcompressedavi.ToInt32() != 0)
                    AVIStreamRelease(ppcompressedavi);

                if (root != 0)
                    AVIFileRelease(root);

                AVIFileExit();

                frmProgress.Close();
            }
        }
    }
}
