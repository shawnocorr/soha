﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using BaseObjects;

namespace VideoInteropDLL
{
    public interface IVideo
    {
        void Open();
        void Close();
        Bitmap GetBitmap(int p_frame);
        
        void GetFrame(int p_frame);
        Frame GetFrameSync(int p_frame);

        void GetMMode(int p_x, MModeType type);
        void GetMMode(int p_x, int start, int end);
        MMode GetMMode(int p_x);

        void GetProprocess(List<Roi> rois);
        void GetFrameTimes();
    }
}
