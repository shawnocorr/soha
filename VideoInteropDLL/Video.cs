﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseObjects;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using System.Drawing;
using System.Threading;

namespace VideoInteropDLL
{
    public delegate void MModeEventHandler(object sender, MMode mmode);
    public delegate void FrameEventHandler(object sender, Frame frame);
    public delegate void PreprocessEventHandler(object sender, PreprocessData[] ppData);
    public delegate void PreprocessProgressHandler(Video sender, int progress);
    public delegate void VideoOpenEventHandler(object sender, bool error);
    public delegate void VideoCloseEventHandler(object sender, bool error);

    public enum VideoFileType
    {
        CXD = 0,
        AVI
    }

    public enum MModeType
    {
        Snapshot,
        Full
    }

    public enum STREAM_SEEK : int
    {
        STREAM_SEEK_SET = 0,
        STREAM_SEEK_CUR = 1,
        STREAM_SEEK_END = 2
    }

    [XmlInclude(typeof(Video))]
    public class Video : Processable, IXmlSerializable
    {
        protected const int FRAMES_TO_SKIP = 0;

        protected Camera m_camera;
        protected string m_video_path;
        protected int m_fps;
        protected int m_total_frames;
        protected int m_frame_width;
        protected int m_frame_height;
        protected double[] m_frame_times;
        protected bool m_opened;
        protected bool m_cancel_process = false;

        public Camera Camera
        {
            get
            {
                return m_camera;
            }
            set
            {
                m_camera = value;
            }
        }
        public string VideoPath 
        { 
            get 
            { 
                return m_video_path; 
            }
            set
            {
                m_video_path = value;
            }
        }
        public int Fps 
        { 
            get 
            { 
                return m_fps; 
            }
            set
            {
                m_fps = value;
            }
        }
        public int Frames 
        { 
            get 
            { 
                return m_total_frames; 
            }
            set
            {
                m_total_frames = value;
            }
        }
        public int Width 
        { 
            get 
            { 
                return m_frame_width; 
            }
            set
            {
                m_frame_width = value;
            }
        }
        public int Height 
        { 
            get 
            { 
                return m_frame_height; 
            }
            set
            {
                m_frame_height = value;
            }
        }
        public double[] FrameTimes
        {
            get
            {
                return m_frame_times;
            }
            set
            {
                m_frame_times = value;
            }
        }


        public event MModeEventHandler MModeComplete;
        public event FrameEventHandler FrameComplete;
        public event PreprocessEventHandler PreprocessComplete;
        public event PreprocessProgressHandler ProgressUpdate;
        public event VideoOpenEventHandler VideoOpenComplete;
        public event VideoCloseEventHandler VideoCloseComplete;

        public Video() { }

        public void CancelProcess()
        {
            m_cancel_process = true;
        }

        public bool IsFrameTimesValid()
        {
            if (m_frame_times == null ||
                m_frame_times.Count() != m_total_frames)
                return false;

            for (int i = 1; i < m_frame_times.Count(); i++)
            {
                if (m_frame_times[i] <= m_frame_times[i - 1])
                    return false;
            }

            return true;
        }

        public virtual void Open() { throw new VideoFileNotFoundException(); }

        public virtual void Close() { throw new VideoFileNotFoundException(); }

        public virtual Bitmap GetBitmap(int p_frame) { throw new NotImplementedException(); }

        public virtual void GetFrame(int p_frame) { throw new NotImplementedException(); }

        public virtual Frame GetFrame_Sync(int p_frame) { throw new NotImplementedException(); }

        public virtual void GetMMode(int p_x_coordinate, MModeType type) { throw new NotImplementedException(); }

        public virtual void GetMMode(int p_x_coordinate, int start_frame, int end_frame) { throw new NotImplementedException(); }

        public virtual MMode GetMMode(int p_x_coordinate) { throw new VideoFileNotFoundException(); }

        public virtual void GetPreprocess(List<Roi> rois) { throw new NotImplementedException(); }

        public virtual void GetFrameTimes() { throw new VideoFileNotFoundException(); }

        protected virtual void OnMModeComplete(object sender, MMode mmode)
        {
            if (MModeComplete != null)
                MModeComplete(sender, mmode);
        }

        protected virtual void OnFrameComplete(object sender, Frame frame)
        {
            if (FrameComplete != null)
                FrameComplete(sender, frame);
        }

        protected virtual void OnPreprocessComplete(object sender, PreprocessData[] ppData)
        {
            if (PreprocessComplete != null)
                PreprocessComplete(sender, ppData);
        }

        protected virtual void OnVideoOpenComplete(object sender, bool error)
        {
            if (VideoOpenComplete != null)
                VideoOpenComplete(sender, error);
        }

        protected virtual void OnVideoCloseComplete(object sender, bool error)
        {
            if (VideoCloseComplete != null)
                VideoCloseComplete(sender, error);
        }

        protected override void OnBegin(EventArgs e)
        {
            base.OnBegin(e);
        }

        protected void OnProgressUpdate(Video sender, int progress)
        {
            if (ProgressUpdate != null)
                ProgressUpdate(sender, progress);
        }

        #region IXmlSerializable Members

        public virtual XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            try
            {
                //reader.Read();
                m_total_frames = Int32.Parse(reader.GetAttribute("Frames"));
                m_frame_height = Int32.Parse(reader.GetAttribute("Height"));
                m_frame_width = Int32.Parse(reader.GetAttribute("Width"));
                m_fps = Int32.Parse(reader.GetAttribute("Fps"));
                m_video_path = reader.GetAttribute("Path");

                reader.ReadToDescendant("Camera");
                XmlSerializer serializer = new XmlSerializer(typeof(Camera));
                m_camera = (Camera)serializer.Deserialize(reader);

                //reader.Read();
                serializer = new XmlSerializer(typeof(double[]));
                m_frame_times = (double[])serializer.Deserialize(reader);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Frames", m_total_frames.ToString());
            writer.WriteAttributeString("Height", m_frame_height.ToString());
            writer.WriteAttributeString("Width", m_frame_width.ToString());
            writer.WriteAttributeString("Fps", m_fps.ToString());
            writer.WriteAttributeString("Path", m_video_path.ToString());

            XmlSerializer serializer = new XmlSerializer(typeof(Camera));
            serializer.Serialize(writer, m_camera);

            serializer = new XmlSerializer(typeof(double[]));
            serializer.Serialize(writer, m_frame_times);
        }

        #endregion

        public override void Run(object arg, CancellationToken cancel_token)
        {
            throw new NotImplementedException();
        }
    }

    [Serializable]
    public class VideoFileNotFoundException : Exception
    {
        public VideoFileNotFoundException() { }
        public VideoFileNotFoundException(string message) : base(message) { }
        public VideoFileNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected VideoFileNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class VideoFileOpenException : Exception
    {
        public VideoFileOpenException() { }
        public VideoFileOpenException(string message) : base(message) { }
        public VideoFileOpenException(string message, Exception inner) : base(message, inner) { }
        protected VideoFileOpenException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
