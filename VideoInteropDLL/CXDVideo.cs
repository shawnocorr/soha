﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Runtime.InteropServices.ComTypes;
using BaseObjects;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Mathematics;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.Threading;

namespace VideoInteropDLL
{

    public class CXDVideo : Video
    {
        #region CXD DECLARES

        private static Guid IID_IStorage = new Guid("0000000B-0000-0000-C000-000000000046");
        private static Guid IID_IPropertySetStorage = new Guid("0000013A-0000-0000-C000-000000000046");
        private static Guid FMTID_Intshcut = new Guid(0x000214A0, 0x0000, 0x0000, new byte[] { 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46 });
        private static Guid IID_IPropertyStorage = new Guid("00000138-0000-0000-C000-000000000046");

        private const int OPEN = (int)(STGM.READ | STGM.PRIORITY);
        private const int READ = (int)(STGM.DIRECT | STGM.READ | STGM.SHARE_EXCLUSIVE);

        #region DLLImports/Enums

        [DllImport("ole32.dll", ExactSpelling = true, PreserveSig = false)]
        static extern Guid ReadClassStg(IStorage pStg);

        [DllImport("ole32.dll")]
        static extern int StgOpenStorage(
            [MarshalAs(UnmanagedType.LPWStr)] string pwcsName,
            IStorage pstgPriority,
            STGM grfMode,
            IntPtr snbExclude,
            uint reserved,
            out IStorage ppstgOpen);

        [DllImport("ole32.dll", CharSet = CharSet.Unicode)]
        public static extern uint StgOpenStorageEx(
            [MarshalAs(UnmanagedType.LPWStr)] string name,
            int accessMode,
            int storageFileFormat,
            int fileBuffering,
            IntPtr options,
            IntPtr reserved,
            ref System.Guid riid,
            [MarshalAs(UnmanagedType.Interface)] out object propertySetStorage);

        [DllImport("ole32.dll")]
        static extern void CoTaskMemAlloc(
            int size);

        [DllImport("ole32.dll")]
        private static extern int StgIsStorageFile(
            [MarshalAs(UnmanagedType.LPWStr)] string pwcsName);

        [StructLayout(LayoutKind.Explicit, Size = 8, CharSet = CharSet.Unicode)]
        public struct PropSpec
        {
            [FieldOffset(0)]
            public int ulKind;
            [FieldOffset(4)]
            public IntPtr Name_Or_ID;
        }

        [StructLayout(LayoutKind.Explicit, Size = 16)]
        public struct PropVariant
        {
            [FieldOffset(0)]
            public short variantType;
            [FieldOffset(8)]
            public IntPtr pointerValue;
            [FieldOffset(8)]
            public byte byteValue;
            [FieldOffset(8)]
            public long longValue;

            public void FromObject(object obj)
            {
                if (obj.GetType() == typeof(string))
                {
                    this.variantType = (short)VarEnum.VT_LPWSTR;
                    this.pointerValue = Marshal.StringToHGlobalUni((string)obj);
                }
            }
        }

        [ComVisible(true), ComImport(),
        Guid("0000013A-0000-0000-C000-000000000046"),
        InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IPropertySetStorage
        {
            uint Create(
                [In]   ref Guid fmtid,
                [In]   IntPtr pclsid,
                [In]   int grfFlags,
                [In]   int grfMode,
                [Out]  out object ppProgStg
                );

            uint Open(
                  [In]   ref Guid fmtid,
                  [In]   int grfMode,
                  [Out]  out object[] ppPropStg
                );

            uint Enum(
                [Out]  out IEnumSTATPROPSETSTG[] ppenum
                );

            //uint Create(
            //[In, MarshalAs(UnmanagedType.Struct)] ref System.Guid rfmtid,
            //[In] IntPtr pclsid,
            //[In] int grfFlags,
            //[In] int grfMode,
            //ref IPropertyStorage propertyStorage);

            //int Open(
            //[In, MarshalAs(UnmanagedType.Struct)] ref System.Guid rfmtid,
            //[In] int grfMode,
            //[Out] out IPropertySetStorage propertyStorage);
        }

        [ComVisible(true), ComImport(),
        Guid("0000013B-0000-0000-C000-000000000046"),
        InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IEnumSTATPROPSETSTG
        {
            uint Next(
                [In]   int celt,
                [Out]  out STATPROPSETSTG rgelt,
                [Out]  out int pceltFetched
                );

            uint Skip(
                [In] int celt
                );

            uint Reset();

            uint Clone(
                [Out]  out IEnumSTATPROPSETSTG ppenum
            );

        }

        [ComVisible(true), ComImport(),
        Guid("00000138-0000-0000-C000-000000000046"),
        InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IPropertyStorage
        {
            int ReadMultiple(
            uint numProperties,
            PropSpec[] propertySpecifications,
            PropVariant[] propertyValues);

            int WriteMultiple(
            uint numProperties,
            [MarshalAs(UnmanagedType.Struct)] ref PropSpec
            propertySpecification,
            ref PropVariant propertyValues,
            int propIDNameFirst);

            uint Commit(
            int commitFlags);
        }

        public struct STATPROPSETSTG
        {
            Guid fmtid;
            IntPtr clsid;
            int grfFlags;
            System.Runtime.InteropServices.FILETIME mtime;
            System.Runtime.InteropServices.FILETIME ctime;
            System.Runtime.InteropServices.FILETIME atime;
        };

        public enum STATFLAG : uint
        {
            STATFLAG_DEFAULT = 0,
            STATFLAG_NONAME = 1,
            STATFLAG_NOOPEN = 2
        }

        public enum STGFMT : uint
        {
            STGFMT_STORAGE = 0,
            STGFMT_FILE = 3,
            STGFMT_ANY = 4,
            STGFMT_DOCFILE = 5
        }

        [Flags]
        public enum STGM : int
        {
            DIRECT = 0x00000000,
            TRANSACTED = 0x00010000,
            SIMPLE = 0x08000000,
            READ = 0x00000000,
            WRITE = 0x00000001,
            READWRITE = 0x00000002,
            SHARE_DENY_NONE = 0x00000040,
            SHARE_DENY_READ = 0x00000030,
            SHARE_DENY_WRITE = 0x00000020,
            SHARE_EXCLUSIVE = 0x00000010,
            PRIORITY = 0x00040000,
            DELETEONRELEASE = 0x04000000,
            NOSCRATCH = 0x00100000,
            CREATE = 0x00001000,
            CONVERT = 0x00020000,
            FAILIFTHERE = 0x00000000,
            NOSNAPSHOT = 0x00200000,
            DIRECT_SWMR = 0x00400000,
        }

        public enum STGTY : int
        {
            STGTY_STORAGE = 1,
            STGTY_STREAM = 2,
            STGTY_LOCKBYTES = 3,
            STGTY_PROPERTY = 4
        }

        public enum STGC
        {
            DEFAULT = 0,
            OVERWRITE = 1,
            ONLYIFCURRENT = 2,
            DANGEROUSLYCOMMITMERELYTODISKCACHE = 4,
            CONSOLIDATE = 8
        }


        [ComImport]
        [Guid("0000000b-0000-0000-C000-000000000046")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IStorage
        {
            void CreateStream(
                /* [string][in] */ string pwcsName,
                /* [in] */ uint grfMode,
                /* [in] */ uint reserved1,
                /* [in] */ uint reserved2,
                /* [out] */ out System.Runtime.InteropServices.ComTypes.IStream ppstm);

            void OpenStream(
                /* [string][in] */ string pwcsName,
                /* [unique][in] */ IntPtr reserved1,
                /* [in] */ uint grfMode,
                /* [in] */ uint reserved2,
                /* [out] */ out System.Runtime.InteropServices.ComTypes.IStream ppstm);

            void CreateStorage(
                /* [string][in] */ string pwcsName,
                /* [in] */ uint grfMode,
                /* [in] */ uint reserved1,
                /* [in] */ uint reserved2,
                /* [out] */ out IStorage ppstg);

            void OpenStorage(
                /* [string][unique][in] */ string pwcsName,
                /* [unique][in] */ IStorage pstgPriority,
                /* [in] */ uint grfMode,
                /* [unique][in] */ IntPtr snbExclude,
                /* [in] */ uint reserved,
                /* [out] */ out IStorage ppstg);

            void CopyTo(
                /* [in] */ uint ciidExclude,
                /* [size_is][unique][in] */ Guid rgiidExclude, // should this be an array?
                /* [unique][in] */ IntPtr snbExclude,
                /* [unique][in] */ IStorage pstgDest);

            void MoveElementTo(
                /* [string][in] */ string pwcsName,
                /* [unique][in] */ IStorage pstgDest,
                /* [string][in] */ string pwcsNewName,
                /* [in] */ uint grfFlags);

            void Commit(
                /* [in] */ uint grfCommitFlags);

            void Release();

            void Revert();

            void EnumElements(
                /* [in] */ uint reserved1,
                /* [size_is][unique][in] */ IntPtr reserved2,
                /* [in] */ uint reserved3,
                /* [out] */ out IEnumSTATSTG ppenum);


            void DestroyElement(
                /* [string][in] */ string pwcsName);

            void RenameElement(
                /* [string][in] */ string pwcsOldName,
                /* [string][in] */ string pwcsNewName);

            void SetElementTimes(
                /* [string][unique][in] */ string pwcsName,
                /* [unique][in] */ System.Runtime.InteropServices.ComTypes.FILETIME pctime,
                /* [unique][in] */ System.Runtime.InteropServices.ComTypes.FILETIME patime,
                /* [unique][in] */ System.Runtime.InteropServices.ComTypes.FILETIME pmtime);

            void SetClass(
                /* [in] */ Guid clsid);

            void SetStateBits(
                /* [in] */ uint grfStateBits,
                /* [in] */ uint grfMask);

            [SecurityPermission(SecurityAction.Assert, Unrestricted = true)]
            void Stat(
                /* [out] */ out System.Runtime.InteropServices.ComTypes.STATSTG pstatstg,
                /* [in] */ uint grfStatFlag);


        }

        struct STGOPTIONS
        {
            public ushort usVersion;
            public ushort reserved;
            public ulong ulSectorSize;
            public char[] pwcsTemplateFile;
        }

        [ComImport]
        [Guid("0000000d-0000-0000-C000-000000000046")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IEnumSTATSTG
        {
            // The user needs to allocate an STATSTG array whose size is celt.
            [PreserveSig]
            uint Next(
                uint celt,
                [MarshalAs(UnmanagedType.LPArray), Out]
            System.Runtime.InteropServices.ComTypes.STATSTG[] rgelt,
                out uint pceltFetched
            );

            void Skip(uint celt);

            void Reset();

            [return: MarshalAs(UnmanagedType.Interface)]
            IEnumSTATSTG Clone();
        }
        #endregion

        #endregion

        private IStorage root;
        private IStorage fields;

        public CXDVideo() { }

        public CXDVideo(string path)
        {
            try
            {
                m_video_path = path;

                object storage = null;
                IStorage root, fields;
                IStorage field, image, node_details;
                IStream data_stream;

                StgOpenStorageEx(m_video_path, OPEN, (int)STGFMT.STGFMT_DOCFILE, 0, IntPtr.Zero, IntPtr.Zero, ref IID_IPropertySetStorage, out storage);
                root = (storage as IStorage);
                root.OpenStorage("Field Data", null, READ, IntPtr.Zero, 0, out fields);

                byte[] data;

                #region Get Frames

                int increment = 10000;
                int i = 1;
                bool done = false;
                while (!done)
                {
                    try
                    {
                        fields.OpenStorage("Field " + i, null, READ, IntPtr.Zero, 0, out field);
                        Marshal.ReleaseComObject(field);
                    }
                    catch (Exception exc)
                    {
                        i -= increment;

                        if (increment != 1)
                        {
                            increment /= 10;
                        }
                        else
                        {
                            m_total_frames = i;
                            done = true;
                        }
                    }
                    finally
                    {
                        i += increment;
                    }
                }

                #endregion

                fields.OpenStorage("Field " + m_total_frames, null, READ, IntPtr.Zero, 0, out field);
                field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out image);
                image.OpenStorage("Details", null, READ, IntPtr.Zero, 0, out node_details);

                #region Get Height
                try
                {
                    //Height
                    node_details.OpenStream("Image_Height", IntPtr.Zero, READ, 0, out data_stream);
                }
                catch (Exception exc)
                {
                    try
                    {
                        node_details.OpenStream("d_02_Image_Height        ", IntPtr.Zero, READ, 0, out data_stream);
                    }
                    catch (Exception exc2)
                    {
                        throw new Exception("Error getting Stream", exc2);
                    }
                }
                data = new byte[8];
                data_stream.Read(data, 8, IntPtr.Zero);
                double height = BitConverter.ToDouble(data, 0);
                m_frame_height = (int)height;
                Marshal.ReleaseComObject(data_stream);

                #endregion

                #region Get Width
                try
                {
                    //Width
                    node_details.OpenStream("Image_Width", IntPtr.Zero, READ, 0, out data_stream);
                }
                catch (Exception exc)
                {
                    try
                    {
                        node_details.OpenStream("d_01_Image_Width         ", IntPtr.Zero, READ, 0, out data_stream);
                    }
                    catch (Exception exc2)
                    {
                        throw new Exception("Error getting Stream", exc2);
                    }
                }
                data = new byte[8];
                data_stream.Read(data, 8, IntPtr.Zero);
                double width = BitConverter.ToDouble(data, 0);
                m_frame_width = (int)width;
                Marshal.ReleaseComObject(data_stream);

                #endregion

                #region Get FPS

                #region Get Time

                double time = 30;
                //data_stream = null;
                //try
                //{
                //    node_details.OpenStream("Time_From_Start", IntPtr.Zero, READ, 0, out data_stream);
                //}
                //catch (Exception exc)
                //{
                //    try
                //    {
                //        node_details.OpenStream("d_06_Time_From_Start     ", IntPtr.Zero, READ, 0, out data_stream);
                //    }
                //    catch (Exception exc2)
                //    {
                //        time = Double.NaN;
                //    }
                //    //throw new CxdContainerException("Error getting Stream", exc2);
                //}
                //data = new byte[8];
                //data_stream.Read(data, 8, IntPtr.Zero);
                //time = BitConverter.ToDouble(data, 0);

                #endregion

                m_fps = (int)(m_total_frames / time);

                #endregion

                Marshal.ReleaseComObject(image);
                Marshal.ReleaseComObject(field);
                Marshal.ReleaseComObject(fields);
                Marshal.ReleaseComObject(root);
            }
            catch (Exception exc)
            {
                throw new VideoFileOpenException("Error Opening '" + Path.GetFileName(path) + "'", exc);
            }
        }

        public static CXDVideo FromVideo(Video video)
        {
            CXDVideo cxd = new CXDVideo();
            cxd.m_fps = video.Fps;
            cxd.m_total_frames = video.Frames;
            cxd.m_frame_height = video.Height;
            cxd.m_frame_width = video.Width;
            cxd.m_video_path = video.VideoPath;
            cxd.m_camera = video.Camera;
            cxd.m_frame_times = video.FrameTimes;

            return cxd;
        }

        public override void Open()
        {
            if (!m_opened)
            {
                if (!File.Exists(m_video_path)) throw new VideoFileNotFoundException("File Not Found: " + m_video_path);

                object storage;
                uint ret = StgOpenStorageEx(m_video_path, OPEN, (int)STGFMT.STGFMT_DOCFILE, 0, IntPtr.Zero, IntPtr.Zero, ref IID_IPropertySetStorage, out storage);

                if (ret != 0)
                    throw new Exception("Stg File Error: Could not open file.");

                root = (storage as IStorage);

                root.OpenStorage("Field Data", null, READ, IntPtr.Zero, 0, out fields);

                if (root == null && fields == null)
                    throw new Exception("CXD File Failed to Initialize.");

                m_opened = true;
            }
        }

        public override void Close()
        {
            if(root != null)
                Marshal.FinalReleaseComObject(root);

            m_opened = false;
        }

        public override void GetFrame(int p_frame)
        {
            if (!m_opened)
                throw new Exception("Video not opened.");

            IStorage field, image, node_details;
            IStream picStream, data_stream;
            byte[] data;

            fields.OpenStorage("Field " + p_frame, null, READ, IntPtr.Zero, 0, out field);
            field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out image);


            field.OpenStorage("Details", null, READ, IntPtr.Zero, 0, out node_details);

            #region Get Time

            double time = 0;
            data_stream = null;
            try
            {
                node_details.OpenStream("Time_From_Start", IntPtr.Zero, READ, 0, out data_stream);
            }
            catch (Exception exc)
            {
                try
                {
                    node_details.OpenStream("d_06_Time_From_Start     ", IntPtr.Zero, READ, 0, out data_stream);
                }
                catch (Exception exc2)
                {
                    time = Double.NaN;
                }
                //throw new CxdContainerException("Error getting Stream", exc2);
            }
            data = new byte[8];
            data_stream.Read(data, 8, IntPtr.Zero);
            time = BitConverter.ToDouble(data, 0);

            #endregion

            try
            {
                image.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out picStream);
            }
            catch (Exception exc)
            {
                image.OpenStream("Data", IntPtr.Zero, READ, 0, out picStream);
            }

            Frame fr = new Frame(m_frame_width, m_frame_height, time, ref picStream, p_frame);

            /* Clean-up */
            Marshal.ReleaseComObject(picStream);
            Marshal.ReleaseComObject(image);
            Marshal.ReleaseComObject(field);

            OnFrameComplete(this, fr);
        }

        public override Frame GetFrame_Sync(int p_frame)
        {
            IStorage field, image, node_details;
            IStream picStream, data_stream;
            byte[] data;

            fields.OpenStorage("Field " + p_frame, null, READ, IntPtr.Zero, 0, out field);
            field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out image);


            field.OpenStorage("Details", null, READ, IntPtr.Zero, 0, out node_details);

            #region Get Time

            double time = 0;
            data_stream = null;
            try
            {
                node_details.OpenStream("Time_From_Start", IntPtr.Zero, READ, 0, out data_stream);
            }
            catch (Exception exc)
            {
                try
                {
                    node_details.OpenStream("d_06_Time_From_Start     ", IntPtr.Zero, READ, 0, out data_stream);
                }
                catch (Exception exc2)
                {
                    time = Double.NaN;
                }
                //throw new CxdContainerException("Error getting Stream", exc2);
            }
            data = new byte[8];
            data_stream.Read(data, 8, IntPtr.Zero);
            time = BitConverter.ToDouble(data, 0);

            #endregion

            try
            {
                image.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out picStream);
            }
            catch (Exception exc)
            {
                image.OpenStream("Data", IntPtr.Zero, READ, 0, out picStream);
            }

            Frame fr = new Frame(m_frame_width, m_frame_height, time, ref picStream, p_frame);

            /* Clean-up */
            Marshal.ReleaseComObject(picStream);
            Marshal.ReleaseComObject(image);
            Marshal.ReleaseComObject(field);

            return fr;
        }

        public override Bitmap GetBitmap(int p_frame)
        {
            if (!m_opened)
                throw new Exception("Video not opened.");

            IStorage field, image, node_details;
            IStream picStream, data_stream;
            byte[] data;

            fields.OpenStorage("Field " + p_frame, null, READ, IntPtr.Zero, 0, out field);
            field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out image);


            field.OpenStorage("Details", null, READ, IntPtr.Zero, 0, out node_details);

            #region Get Time

            double time = 0;
            data_stream = null;
            try
            {
                node_details.OpenStream("Time_From_Start", IntPtr.Zero, READ, 0, out data_stream);
            }
            catch (Exception exc)
            {
                try
                {
                    node_details.OpenStream("d_06_Time_From_Start     ", IntPtr.Zero, READ, 0, out data_stream);
                }
                catch (Exception exc2)
                {
                    time = Double.NaN;
                }
                //throw new CxdContainerException("Error getting Stream", exc2);
            }
            data = new byte[8];
            data_stream.Read(data, 8, IntPtr.Zero);
            time = BitConverter.ToDouble(data, 0);

            #endregion

            try
            {
                image.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out picStream);
            }
            catch (Exception exc)
            {
                image.OpenStream("Data", IntPtr.Zero, READ, 0, out picStream);
            }

            double hi = 0;
            byte[] pixel = new byte[2];
            picStream.Seek(0, (int)STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);
            for (int i = 0; i < m_frame_height * m_frame_width; i++)
            {
                picStream.Read(pixel, pixel.Length, IntPtr.Zero);
                ushort pix_val = BitConverter.ToUInt16(pixel, 0);
                if (pix_val > hi)
                    hi = pix_val;
            }

            int stride = 3 * this.Width;
            while (stride % 4 != 0)
                stride++;

            IntPtr scan = new IntPtr();
            Bitmap bmp = new Bitmap(this.Width, this.Height, stride, PixelFormat.Format24bppRgb, scan);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            picStream.Seek(0, (int)STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);
            unsafe
            {
                byte* imgPtr = (byte*)(bmpdata.Scan0.ToPointer());
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = 0; j < bmpdata.Width; j++)
                    {
                        picStream.Read(pixel, pixel.Length, IntPtr.Zero);
                        ushort pix_val = BitConverter.ToUInt16(pixel, 0);
                        int p = (int)(pix_val / hi * 255);
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                    }

                    imgPtr += stride - 3 * bmpdata.Width;
                }
            }

            bmp.UnlockBits(bmpdata);

            Marshal.ReleaseComObject(picStream);
            Marshal.ReleaseComObject(image);
            Marshal.ReleaseComObject(field);

            return bmp;

            /* Clean-up */
            

            //return 
        }

        protected static MemoryStream GetMModeSliver(IStream data, int x, Size framesize)
        {
            MemoryStream mmode_sliver = new MemoryStream(new byte[framesize.Height * 2]);

            //Create New Stream From Roi

            long w = framesize.Width;

            long l = x;
            long r = x;

            long t = 0;
            long b = framesize.Height;

            long skip_top = 2 * (w * t + l);
            long skip_line = 2 * (w - 1);

            //Skip Top
            data.Seek(skip_top, (int)STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);

            byte[] line = new byte[2];
            for (int i = 0; i < framesize.Height; i++)
            {
                data.Read(line, line.Length, IntPtr.Zero);
                data.Seek(skip_line, (int)STREAM_SEEK.STREAM_SEEK_CUR, IntPtr.Zero);
                mmode_sliver.Write(line, 0, line.Length);
            }

            mmode_sliver.Position = 0;

            //Create Frame From New Stream
            return mmode_sliver;
        }

        protected static MemoryStream GetFrame(IStream data, Size framesize, Roi roi)
        {
            MemoryStream roi_stream = new MemoryStream(new byte[roi.Area() * 2]);

            //Create New Stream From Roi

            long w = framesize.Width;

            long l = roi.X;
            long r = roi.X + roi.Width;

            long t = roi.Y;
            long b = roi.Y + roi.Height;

            long skip_top = 2 * (w * t + l);
            long read_length = 2 * roi.Width;
            long skip_line = 2 * (w - roi.Width);

            //Skip Top
            data.Seek(skip_top, (int)STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);

            byte[] line = new byte[read_length];
            for (int i = 0; i < roi.Height; i++)
            {
                data.Read(line, line.Length, IntPtr.Zero);
                data.Seek(skip_line, (int)STREAM_SEEK.STREAM_SEEK_CUR, IntPtr.Zero);
                roi_stream.Write(line, 0, line.Length);
            }

            roi_stream.Position = 0;

            //Create Frame From New Stream
            return roi_stream;
        }

        //protected static void ProcessStream(IStream data, Size framesize, Roi roi, int x, ref MemoryStream roi_stream, ref MemoryStream mmode_stream)
        //{

        //}

        protected IStream GetFrameStream(int frame)
        {
            try
            {
                IStorage field, bmpStore;
                IStream bmpStream;

                fields.OpenStorage("Field " + frame, null, READ, IntPtr.Zero, 0, out field);

                field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out bmpStore);
                bmpStore.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out bmpStream);

                return bmpStream;
            }
            catch (Exception exc)
            {

            }

            return null;
        }

        protected static MemoryStream GetRoiStream(IStream data, Size framesize, Roi roi)
        {
            try
            {
                MemoryStream roi_stream = new MemoryStream(new byte[roi.Area() * 2]);

                //Create New Stream From Roi

                long w = framesize.Width;

                long l = roi.X;
                long r = roi.X + roi.Width;

                long t = roi.Y;
                long b = roi.Y + roi.Height;

                long skip_top = 2 * (w * t + l);
                long read_length = 2 * roi.Width;
                long skip_line = 2 * (w - roi.Width);

                //Skip Top
                data.Seek(skip_top, (int)STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);

                byte[] line = new byte[read_length];
                for (int i = 0; i < roi.Height; i++)
                {
                    data.Read(line, line.Length, IntPtr.Zero);
                    data.Seek(skip_line, (int)STREAM_SEEK.STREAM_SEEK_CUR, IntPtr.Zero);
                    roi_stream.Write(line, 0, line.Length);
                }

                roi_stream.Position = 0;

                //Create Frame From New Stream
                return roi_stream;
            }
            catch (Exception exc)
            {

            }

            return null;
        }

        public override void GetMMode(int p_x_coordinate, MModeType type)
        {
            frmProcessNotify frmProgress = new frmProcessNotify("Gathering MMode...","Gathering MMode");
            frmProgress.TopMost = true;
            frmProgress.Show();

            Open();

            MMode ret_mmode = new MMode();

            ret_mmode.PixelValues = new double[m_total_frames, this.Height];
            ret_mmode.XCoordinate = p_x_coordinate;

            int n = 1;
            switch (type)
            {
                case MModeType.Snapshot:
                    n = 1000;
                    break;
                case MModeType.Full:
                    n = m_total_frames;
                    break;
                default:
                    break;
            }

            for (int i = 1; i < n; i++)
            {

                #region Cancellation

                Application.DoEvents();

                if (frmProgress.IsCancelled)
                {
                    frmProgress.IsCancelled = false;
                    frmProgress.Close();
                    Close();
                    OnMModeComplete(this, null);
                    return;
                }

                #endregion

                IStorage field, bmpStore, details;
                IStream bmpStream = null, data_stream;

                fields.OpenStorage("Field " + i, null, READ, IntPtr.Zero, 0, out field);

                field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out bmpStore);
                bmpStore.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out bmpStream);

                MemoryStream mmode_sliver = CXDVideo.GetMModeSliver(bmpStream, ret_mmode.XCoordinate, new Size(m_frame_width, m_frame_height));
                for (int y = 0; y < ret_mmode.Size.Height * 2; y += 2)
                {
                    byte[] pix = new byte[2];
                    mmode_sliver.Read(pix, 0, pix.Length);

                    ushort p1 = BitConverter.ToUInt16(pix, 0);
                    ret_mmode[i - 1, y / 2] = p1;
                }

                int pct_complete = i * 100 / n;
                frmProgress.UpdateProgress(pct_complete);
            }

            frmProgress.Close();
            Close();

            OnMModeComplete(this, ret_mmode);
        }

        public override void GetMMode(int p_x_coordinate, int start_frame, int end_frame)
        {
            if (start_frame < 1 || end_frame > m_total_frames) throw new Exception("Invalid MMode Interval.");

            int N = end_frame - start_frame + 1;

            frmProcessNotify frmProgress = new frmProcessNotify("Gathering MMode...","Gathering MMode");
            frmProgress.TopMost = true;
            frmProgress.Show();

            Open();

            MMode ret_mmode = new MMode();

            ret_mmode.PixelValues = new double[m_total_frames, this.Height];
            ret_mmode.XCoordinate = p_x_coordinate;
            
            for (int i = start_frame; i <= end_frame; i++)
            {

                #region Cancellation

                Application.DoEvents();

                if (frmProgress.IsCancelled)
                {
                    frmProgress.IsCancelled = false;
                    frmProgress.Close();
                    Close();
                    OnMModeComplete(this, null);
                    return;
                }

                #endregion

                IStorage field, bmpStore;
                IStream bmpStream = null;

                fields.OpenStorage("Field " + i, null, READ, IntPtr.Zero, 0, out field);

                field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out bmpStore);
                bmpStore.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out bmpStream);

                MemoryStream mmode_sliver = CXDVideo.GetMModeSliver(bmpStream, ret_mmode.XCoordinate, new Size(m_frame_width, m_frame_height));
                for (int y = 0; y < ret_mmode.Size.Height * 2; y += 2)
                {
                    byte[] pix = new byte[2];
                    mmode_sliver.Read(pix, 0, pix.Length);

                    ushort p1 = BitConverter.ToUInt16(pix, 0);
                    ret_mmode[i - 1, y / 2] = p1;
                }

                int pct_complete = (i - start_frame) * 100 / (end_frame - start_frame + 1);

                frmProgress.UpdateProgress(pct_complete);
            }

            frmProgress.Close();
            Close();

            OnMModeComplete(this, ret_mmode);
        }

        public override MMode GetMMode(int p_x_coordinate)
        {
            Open();

            MMode ret_mmode = new MMode();

            ret_mmode.PixelValues = new double[m_total_frames, this.Height];
            ret_mmode.XCoordinate = p_x_coordinate;

            int n = m_total_frames;

            for (int i = 1; i < n; i++)
            {
                IStorage field, bmpStore, details;
                IStream bmpStream = null, data_stream;

                fields.OpenStorage("Field " + i, null, READ, IntPtr.Zero, 0, out field);

                field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out bmpStore);
                bmpStore.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out bmpStream);

                MemoryStream mmode_sliver = CXDVideo.GetMModeSliver(bmpStream, ret_mmode.XCoordinate, new Size(m_frame_width, m_frame_height));
                for (int y = 0; y < ret_mmode.Size.Height * 2; y += 2)
                {
                    byte[] pix = new byte[2];
                    mmode_sliver.Read(pix, 0, pix.Length);

                    ushort p1 = BitConverter.ToUInt16(pix, 0);
                    ret_mmode[i - 1, y / 2] = p1;
                }

                Marshal.FinalReleaseComObject(field);
                Marshal.FinalReleaseComObject(bmpStore);
                Marshal.FinalReleaseComObject(bmpStream);
            }

            Close();

            return ret_mmode;
        }

        public override void GetPreprocess(List<Roi> rois)
        {
            #region Working Preprocess
            /*int last_progress = 0;
            PreprocessData[] data_out = new PreprocessData[1];
            try
            {
                Frame[] frame_cache = new Frame[2];

                if (rois.Count == 0)
                {
                    rois.Add(new Roi(0, 0, new Size(this.Width, this.Height)));
                }

                data_out = new PreprocessData[rois.Count];
                for (int i = 0; i < data_out.Length; i++)
                {
                    data_out[i] = new PreprocessData(this.Frames);
                }

                Open();

                for (int n = 0; n < data_out.Length; n++)
                {
                    data_out[n].MMode.PixelValues = new double[m_total_frames, this.Height];
                    data_out[n].MMode.XCoordinate = rois[n].X + rois[n].Width / 2;

                    m_frame_times = new double[m_total_frames + 1];

                    frame_cache[1] = GetFrame_Sync(1);
                    for (int i = 1; i <= m_total_frames; i++)
                    {

                        #region Cancellation

                        if (m_cancel_process)
                        {
                            m_cancel_process = false;
                            OnPreprocessComplete(this, null);
                            return;
                        }

                        #endregion

                        frame_cache[0] = frame_cache[1];

                        if (i < m_total_frames)
                            frame_cache[1] = GetFrame_Sync(i + 1);

                        for (int y = 0; y < data_out[n].MMode.Size.Height; y++)
                        {
                            data_out[n].MMode[i - 1, y] = frame_cache[0][data_out[n].MMode.XCoordinate, y];
                        }

                        double area = frame_cache[0].PixelValues.Length;
                        double sum_bright = 0;
                        double sum_intensity = 0;
                        for (int j = 0; j < frame_cache[0].PixelValues.GetLength(0); j++)
                        {
                            for (int k = 0; k < frame_cache[0].PixelValues.GetLength(1); k++)
                            {
                                #region FrameBrightness
                                sum_bright += -frame_cache[0][j, k];
                                #endregion

                                #region 2nd Order Functions
                                if (i < m_total_frames)
                                {
                                    #region PixelIntensityChange

                                    double delta = 0;
                                    delta += Math.Abs(frame_cache[1][j, k] - frame_cache[0][j, k]);
                                    sum_intensity += delta;

                                    #endregion
                                }
                                #endregion
                            }
                        }

                        m_frame_times[i - 1] = frame_cache[0].TimeFromStart;
                        data_out[n].Darkness[i - 1] = (double)(sum_bright);
                        if (i < m_total_frames)
                        {
                            data_out[n].Intensity[i - 1] = (double)(sum_intensity);
                        }                        

                        //Send Progress
                        
                        int progress = (int)(i / (double)m_total_frames * 100 * ((double)(n + 1) / data_out.Length));
                        if (progress != last_progress)
                        {
                            last_progress = progress;
                            base.OnProgressUpdate(this, progress);
                        }

                    }

                    #region Eliminate Intensity +/- 2 Std Dev

                    data_out[n].Intensity = ArrayFunctions.PlusMinusWindow(data_out[n].Intensity, 3);

                    #endregion

                }
                Close();

                OnPreprocessComplete(this, data_out);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error in CXD:PreProcess(): " + exc.Message);

                Close();

                OnPreprocessComplete(this, null);
            }*/
            #endregion


            DateTime start = DateTime.Now;
            int FRAMES =  m_total_frames;
            MemoryStream frame_a = null, frame_b = null;

            PreprocessData[] data_out = new PreprocessData[1];
            try
            {
                if (rois.Count == 0)
                {
                    rois.Add(new Roi(0, 0, new Size(this.Width, this.Height)));
                }

                data_out = new PreprocessData[rois.Count];
                for (int i = 0; i < data_out.Length; i++)
                {
                    data_out[i] = new PreprocessData(FRAMES - FRAMES_TO_SKIP);
                }

                Open();

                for (int n = 0; n < data_out.Length; n++)
                {
                    data_out[n].MMode.PixelValues = new double[FRAMES, this.Height];
                    data_out[n].MMode.XCoordinate = rois[n].X + rois[n].Width / 2;

                    m_frame_times = new double[FRAMES];

                    IStorage field, bmpStore, details;
                    IStream bmpStream = null, data_stream;
                    byte[] data = new byte[2];
                    double time;

                    int index_counter = 0;
                    bool FIRST_PASS = true;
                    
                    for (int i = FRAMES_TO_SKIP + 1; i <= FRAMES; index_counter = i++)
                    {
                        #region Cancellation

                        if (m_cancel_process)
                        {
                            m_cancel_process = false;
                            OnPreprocessComplete(this, null);
                            return;
                        }

                        #endregion

                        if (!FIRST_PASS)
                        {
                            frame_b = frame_a;
                            frame_b.Position = 0;
                        }

                        fields.OpenStorage("Field " + i, null, READ, IntPtr.Zero, 0, out field);

                        #region Get Time

                        field.OpenStorage("Details", null, READ, IntPtr.Zero, 0, out details);
                        try
                        {
                            details.OpenStream("Time_From_Start", IntPtr.Zero, READ, 0, out data_stream);
                        }
                        catch (Exception exc)
                        {
                            try
                            {
                                details.OpenStream("d_06_Time_From_Start     ", IntPtr.Zero, READ, 0, out data_stream);
                            }
                            catch (Exception exc2)
                            {
                                throw new CxdFrameTimeException("Error getting stream", exc2);
                            }
                        }
                        data = new byte[8];
                        data_stream.Read(data, 8, IntPtr.Zero);
                        time = BitConverter.ToDouble(data, 0);

                        this.m_frame_times[index_counter] = time;

                        #endregion

                        field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out bmpStore);
                        bmpStore.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out bmpStream);

                        frame_a = CXDVideo.GetRoiStream(bmpStream, new Size(m_frame_width, m_frame_height), rois[n]);

                        MemoryStream mmode_sliver = CXDVideo.GetMModeSliver(bmpStream, data_out[n].MMode.XCoordinate, new Size(m_frame_width, m_frame_height));
                        for (int y = 0; y < data_out[n].MMode.Size.Height * 2; y+=2)
                        {
                            byte[] pix = new byte[2];
                            mmode_sliver.Read(pix, 0, pix.Length);

                            ushort p1 = BitConverter.ToUInt16(pix, 0);
                            data_out[n].MMode[i - 1, y / 2] = p1;
                        }

                        double area = rois[n].Area();
                        double sum_bright = 0;
                        double sum_intensity = 0;

                        byte[] z1 = new byte[2]; byte[] z2 = new byte[2];
                        while (frame_a.Read(z1, 0, z1.Length) == z1.Length)
                        {
                            ushort p1 = BitConverter.ToUInt16(z1, 0);
                            sum_bright += -p1;

                            if (!FIRST_PASS)
                            {
                                frame_b.Read(z2, 0, z2.Length);
                                ushort p2 = BitConverter.ToUInt16(z2, 0);
                                sum_intensity += Math.Abs(p2 - p1);
                            }
                            
                        }

                        data_out[n].Darkness[index_counter] = (double)(sum_bright);
                        if (!FIRST_PASS)
                        {
                            data_out[n].Intensity[index_counter - 1] = (double)(sum_intensity);
                        }

                        FIRST_PASS = false;
                    }

                    #region Eliminate Intensity +/- 2 Std Dev

                    //data_out[n].Intensity = ArrayFunctions.PlusMinusWindow(data_out[n].Intensity, 3);

                    #endregion

                }
                Close();

                Logs.LogTrace("Processing took " + (DateTime.Now - start).Seconds
                    + " seconds. [" + m_total_frames + " Frames, " + m_total_frames * rois[0].Area() / (DateTime.Now - start).Seconds + " pps]");

                OnPreprocessComplete(this, data_out);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error in CXD:PreProcess(): " + exc.Message + "\n\n" + exc.ToString());
                Logs.LogException(exc);

                Close();

                OnPreprocessComplete(this, null);
            }
        }

        public override void GetFrameTimes()
        {
            try
            {
                Open();

                this.m_frame_times = new double[m_total_frames];

                IStorage field, details;
                IStream data_stream;
                byte[] data = new byte[2];
                double time;

                for (int i = 1; i <= m_total_frames; i++)
                {
                    fields.OpenStorage("Field " + i, null, READ, IntPtr.Zero, 0, out field);

                    #region Get Time

                    field.OpenStorage("Details", null, READ, IntPtr.Zero, 0, out details);
                    try
                    {
                        details.OpenStream("Time_From_Start", IntPtr.Zero, READ, 0, out data_stream);
                    }
                    catch (Exception exc)
                    {
                        try
                        {
                            details.OpenStream("d_06_Time_From_Start     ", IntPtr.Zero, READ, 0, out data_stream);
                        }
                        catch (Exception exc2)
                        {
                            throw new CxdFrameTimeException("Error getting stream", exc2);
                        }
                    }
                    data = new byte[8];
                    data_stream.Read(data, 8, IntPtr.Zero);
                    time = BitConverter.ToDouble(data, 0);

                    #endregion

                    this.m_frame_times[i - 1] = time;

                    Marshal.FinalReleaseComObject(field);
                    Marshal.FinalReleaseComObject(details);
                    Marshal.FinalReleaseComObject(data_stream);
                }

                if (!IsFrameTimesValid())
                {
                    for (int i = 1; i <= m_total_frames; i++)
                    {

                        this.m_frame_times[i - 1] = (i-1)/(double)m_fps;

                    }
                }
            }
            catch (Exception exc)
            {

            }
            finally
            {
                Close();
            }
        }

        public unsafe override void Run(object arg, CancellationToken cancel_token)
        {
            OnBegin(new EventArgs());

            List<double> access_times = new List<double>();
            List<double> roi_times = new List<double>();
            List<double> mmode_times = new List<double>();
            List<double> process_times = new List<double>();

            Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;
            Exception exc_return = null;
            bool cancelled = false;
            bool error = false;
            Logs.LogTrace("Began Run(): [" + Thread.CurrentThread.ManagedThreadId + "] " + this.VideoPath);
            Stopwatch stopwatch = Stopwatch.StartNew();

            BindingList<Roi> rois = arg as BindingList<Roi>;
            DateTime start = DateTime.Now;
            int FRAMES = m_total_frames;
            int progress = 0;
            MemoryStream frame_a = null, frame_b = null;
            IntPtr frm_a, frm_b;

            PreprocessData[] data_out = new PreprocessData[1];
            try
            {
                if (rois.Count == 0)
                {
                    rois.Add(new Roi(0, 0, new Size(this.Width, this.Height)));
                }

                data_out = new PreprocessData[rois.Count];
                for (int i = 0; i < data_out.Length; i++)
                {
                    data_out[i] = new PreprocessData(FRAMES - FRAMES_TO_SKIP);
                }

                for (int n = 0; n < data_out.Length; n++)
                {
                    //Allocate Frame Pointer Structures
                    int total_bytes_per_roi = rois[n].Height * rois[n].Width * 2;
                    frm_a = Marshal.AllocHGlobal(total_bytes_per_roi);
                    frm_b = Marshal.AllocHGlobal(total_bytes_per_roi);

                    STGOPTIONS options = new STGOPTIONS();
                    options.usVersion = 1;
                    options.reserved = 0;
                    options.ulSectorSize = 4096;

                    IntPtr opt_ptr = Marshal.AllocHGlobal(Marshal.SizeOf(options));
                    Marshal.StructureToPtr(options, opt_ptr, false);

                    #region Open Storage

                    object storage;
                    uint ret = StgOpenStorageEx(m_video_path, OPEN, (int)STGFMT.STGFMT_DOCFILE, 0, opt_ptr, IntPtr.Zero, ref IID_IPropertySetStorage, out storage);

                    if (ret != 0)
                        throw new Exception("Stg File Error: Could not open file.");

                    root = (storage as IStorage);

                    root.OpenStorage("Field Data", null, READ, IntPtr.Zero, 0, out fields);

                    if (root == null && fields == null)
                        throw new Exception("CXD File Failed to Initialize.");

                    #endregion

                    data_out[n].MMode.PixelValues = new double[FRAMES, this.Height];
                    data_out[n].MMode.XCoordinate = rois[n].X + rois[n].Width / 2;

                    m_frame_times = new double[FRAMES];

                    IStorage field, bmpStore, details;
                    IStream bmpStream = null, data_stream;
                    byte[] data = new byte[2];
                    double time;

                    int index_counter = 0;
                    bool FIRST_PASS = true;

                    for (int i = FRAMES_TO_SKIP + 1; i <= FRAMES; index_counter = i++)
                    {
                        #region Cancellation

                        if (cancel_token.IsCancellationRequested)
                        {
                            cancel_token.ThrowIfCancellationRequested();
                            //m_cancel_process = false;
                            //OnPreprocessComplete(this, null);
                            //return;
                        }

                        #endregion

                        if (!FIRST_PASS)
                        {
                            frame_b = frame_a;
                            frame_b.Position = 0;
                        }

                        Stopwatch access_watch = new Stopwatch();
                        access_watch.Start();
                        fields.OpenStorage("Field " + i, null, READ, IntPtr.Zero, 0, out field);

                        #region Get Time

                        field.OpenStorage("Details", null, READ, IntPtr.Zero, 0, out details);
                        try
                        {
                            details.OpenStream("Time_From_Start", IntPtr.Zero, READ, 0, out data_stream);
                        }
                        catch (Exception exc)
                        {
                            try
                            {
                                details.OpenStream("d_06_Time_From_Start     ", IntPtr.Zero, READ, 0, out data_stream);
                            }
                            catch (Exception exc2)
                            {
                                throw new CxdFrameTimeException("Error getting stream", exc2);
                            }
                        }
                        data = new byte[8];
                        data_stream.Read(data, 8, IntPtr.Zero);
                        time = BitConverter.ToDouble(data, 0);

                        this.m_frame_times[index_counter] = time;

                        #endregion

                        field.OpenStorage("i_Image1", null, READ, IntPtr.Zero, 0, out bmpStore);
                        bmpStore.OpenStream("Bitmap 1", IntPtr.Zero, READ, 0, out bmpStream);

                        access_times.Add(access_watch.ElapsedMilliseconds);

                        frame_a = CXDVideo.GetRoiStream(bmpStream, new Size(m_frame_width, m_frame_height), rois[n]);
                        roi_times.Add(access_watch.ElapsedMilliseconds);

                        MemoryStream mmode_sliver = CXDVideo.GetMModeSliver(bmpStream, data_out[n].MMode.XCoordinate, new Size(m_frame_width, m_frame_height));
                        mmode_times.Add(access_watch.ElapsedMilliseconds);


                        for (int y = 0; y < data_out[n].MMode.Size.Height * 2; y += 2)
                        {
                            byte[] pix = new byte[2];
                            mmode_sliver.Read(pix, 0, pix.Length);

                            ushort p1 = BitConverter.ToUInt16(pix, 0);
                            data_out[n].MMode[i - 1, y / 2] = p1;
                        }

                        double area = rois[n].Area();
                        double sum_bright = 0;
                        double sum_intensity = 0;

                        byte[] z1 = new byte[2]; byte[] z2 = new byte[2];
                        while (frame_a.Read(z1, 0, z1.Length) == z1.Length)
                        {
                            ushort p1 = BitConverter.ToUInt16(z1, 0);
                            sum_bright += -p1;

                            if (!FIRST_PASS)
                            {
                                frame_b.Read(z2, 0, z2.Length);
                                ushort p2 = BitConverter.ToUInt16(z2, 0);
                                sum_intensity += Math.Abs(p2 - p1);
                            }

                        }

                        data_out[n].Darkness[index_counter] = (double)(sum_bright);
                        if (!FIRST_PASS)
                        {
                            data_out[n].Intensity[index_counter - 1] = (double)(sum_intensity);
                        }

                        FIRST_PASS = false;

                        process_times.Add(access_watch.ElapsedMilliseconds);

                        int new_progress = i * 100 / FRAMES;
                        if (progress < new_progress)
                        {
                            progress = new_progress;
                            OnProgressUpdate(this, progress / data_out.Length);
                        }
                    }

                    #region Eliminate Intensity +/- 2 Std Dev

                    //data_out[n].Intensity = ArrayFunctions.PlusMinusWindow(data_out[n].Intensity, 3);

                    #endregion


                    Marshal.FinalReleaseComObject(storage);
                }

                //OnComplete(new ProgressEventArgs(data_out, false,false,null));
                //OnPreprocessComplete(this, data_out);
            }
            catch (AggregateException agg_exc)
            {
                cancelled = true;
            }
            catch (OperationCanceledException op_cancel_exc)
            {
                cancelled = true;
                data_out = new PreprocessData[1];
                error = false;
                exc_return = op_cancel_exc;
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error in CXD:PreProcess(): " + exc.Message);
                Logs.LogException(exc);
                exc_return = exc;
                error = true;
            }

            if (root != null)
                root = null;

            if (fields != null)
                fields = null;

            m_opened = false;

            Logs.LogTrace("[" + Thread.CurrentThread.ManagedThreadId + "] Processing took " + stopwatch.ElapsedMilliseconds
                    + " milliseconds. [" + m_total_frames + " Frames, " + m_total_frames * rois[0].Area() / stopwatch.ElapsedMilliseconds * 1000 + " pps]");

            OnComplete(new ProgressEventArgs(data_out, cancelled, error, exc_return));
        }
    }

    [Serializable]
    public class CxdFrameTimeException : Exception
    {
        public CxdFrameTimeException() { }
        public CxdFrameTimeException(string message) : base(message) { }
        public CxdFrameTimeException(string message, Exception inner) : base(message, inner) { }
        protected CxdFrameTimeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
