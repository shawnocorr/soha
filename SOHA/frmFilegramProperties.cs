﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Video;
using System.IO;
using SOHA.Library.Xml;
using SOHA.Library.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library;

namespace SOHA
{
    public partial class frmFilegramProperties : frmPhase
    {
        public string NewVideoPath { get; set; }
        private Filegram m_filegram;

        private readonly string m_initial_video_path;
        private readonly double[] m_initial_frame_times;

        public frmFilegramProperties()
        {
            InitializeComponent();
        }

        public frmFilegramProperties(Filegram fgram)
        {
            InitializeComponent();

            //this.ControlBox = true;
            this.WindowState = FormWindowState.Normal;

            this.Text = fgram.Name;
            m_filegram = fgram;
            //m_filegram.Status = FilegramStatus.UserInterface;

            propGrid.SelectedObject = m_filegram;

            m_initial_frame_times = m_filegram.Video.FrameTimes;
            m_initial_video_path = m_filegram.Video.VideoPath;

            if (m_filegram.Video.VideoPath == null || m_filegram.Video.VideoPath == "")
            {
                lblCurrentVideo.Text = "NO VIDEO FILE";
            }
            else
            {
                lblCurrentVideo.Text = m_filegram.Video.VideoPath;
            }

            if(m_filegram.Video.FrameTimes == null)
            {   
                lblFrameTimeStatus.Text = "NOT CALCULATED";
            }
            else
            {
                if (m_filegram.Video.IsFrameTimesValid())
                {
                    lblFrameTimeStatus.Text = "Appears valid; " + m_filegram.Video.FrameTimes.Count() + " values";
                }
                else
                {
                    lblFrameTimeStatus.Text = "Appears INVALID; " + m_filegram.Video.FrameTimes.Count() + " values";
                }
            }

            if(m_filegram.Phases[PhaseName.Preprocess].Options.Count > 0)
            {
                chkMirror.DataBindings.Add("Checked", (m_filegram.Phases[PhaseName.Preprocess].Options[0] as PreprocessPhaseOptions), "EnableMirror",false, DataSourceUpdateMode.OnPropertyChanged);
                chkEnableFilters.DataBindings.Add("Checked", (m_filegram.Phases[PhaseName.Preprocess].Options[0] as PreprocessPhaseOptions), "EnableFilters", false, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            m_filegram.Video.FrameTimes = m_initial_frame_times;
            m_filegram.Video.VideoPath = m_initial_video_path;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog file_browser = new OpenFileDialog();
            file_browser.Filter = "CXD Files|*.cxd|AVI Files|*.avi";
            file_browser.Title = "Select a Video File";

            if (file_browser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string new_video = Path.GetFileNameWithoutExtension(file_browser.FileName);

                if (new_video != m_filegram.Name)
                {
                    if (MessageBox.Show("The new video file does not appear to match.\r\nAre you sure you want to link this video to this trial?", "Continue?", MessageBoxButtons.YesNo)
                        == DialogResult.No)
                        return;

                    m_filegram.Video.VideoPath = file_browser.FileName;
                }
            }

            lblCurrentVideo.Text = (m_filegram.Video.VideoPath == null || m_filegram.Video.VideoPath == "") ? "NO VIDEO FILE" : m_filegram.Video.VideoPath;
        }

        public static frmFilegramProperties Spawn(Filegram fgram)
        {
            return new frmFilegramProperties(fgram);
        }

        private void cmdCalcFrameTimes_Click(object sender, EventArgs e)
        {
            try
            {
                m_filegram.Video.GetFrameTimes();

                if (m_filegram.Video.FrameTimes == null)
                {
                    lblFrameTimeStatus.Text = "NOT CALCULATED";
                }
                else
                {
                    if (m_filegram.Video.IsFrameTimesValid())
                    {
                        lblFrameTimeStatus.Text = "Appears valid; " + m_filegram.Video.FrameTimes.Count() + " values";
                    }
                    else
                    {
                        lblFrameTimeStatus.Text = "Appears INVALID; " + m_filegram.Video.FrameTimes.Count() + " values";
                    }
                }
            }
            catch (VideoFileNotFoundException exc)
            {
                MessageBox.Show("No Video File Present.\r\n\r\nPlease assign a Video File first.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //Calculate Shift

                //Ensure 2 ROI's
                if(m_filegram.Phases[2].Data.Count < 2)
                    throw new Exception("More than 2 ROIs required.");

                IntervalPhaseData out1 = m_filegram.Phases[2].Data[0] as IntervalPhaseData;
                IntervalPhaseData out2 = m_filegram.Phases[2].Data[1] as IntervalPhaseData;

                if(out1.Intervals.Count != out2.Intervals.Count)
                    throw new Exception("Interval Counts Must Match.");

                IntervalType inttype = IntervalType.MaxVelocity;

                //Calculate Shift
                //Get Systoles
                List<int> sys1 = out1.Intervals
                    .Where(x => x.IntervalType == inttype)
                    .Select(x => x.Frame)
                    .ToList<int>();
                List<int> sys2 = out2.Intervals
                    .Where(x => x.IntervalType == inttype)
                    .Select(x => x.Frame)
                    .ToList<int>();
                Vector S1 = new Vector(sys1);
                Vector S2 = new Vector(sys2);
                Vector A = S2 - S1;

                //Calculate Distance in um/s
                MarkPhaseData roi = m_filegram.Phases[0].Data[0] as MarkPhaseData;
                double dist = roi.Rois[1].X - roi.Rois[0].X;
                dist *= m_filegram.Video.Camera.ScaleFactor;
                Vector speed = (1/dist) * A;
            }
            catch(Exception exC)
            {

            }
        }
    }
}
