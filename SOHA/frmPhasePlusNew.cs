﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHAControls;
using SOHA.Library.Video;
using SOHA.Library;
using SOHA.Library.Xml;
using SOHAControls.DisplayControls;
using SOHA.Library.Forms;

namespace SOHA
{
    public partial class frmPhasePlusNew : frmPhase
    {
        private readonly Frame m_mmode_display_frame;

        private bool m_collecting_mmode = false;
        private Video m_video;
        private VideoFrameRange m_current_frame_range = new VideoFrameRange();
        private IntervalProperties m_current_frame_interval = new IntervalProperties();

        private readonly IntervalPhaseData m_interval_data;
        private readonly IntervalPhaseSettings m_interval_settings;
        private readonly IntervalPhaseOutput m_interval_output;
        private readonly PreprocessPhaseData m_preprocess_data;
        private PhasePhaseData m_phase_data;
        private PhasePhaseSettings m_phase_settings;
        private PhasePhaseOutput m_phase_output;

        private int N = 1;
        private List<double> m_initial_function;
        private IntervalCollection m_initial_intervals;
        private IntervalCollection m_last_phase_intervals_baseline;

        private bool m_phase_data_present = false;

        public frmPhasePlusNew()
        {
            InitializeComponent();
        }

        public frmPhasePlusNew(Filegram fgram)
            :base(fgram)
        {
            //base constructor run first

            InitializeComponent();

            if(!(fgram.Phases[2] is IntervalPhaseOutput) 
                || (fgram.Phases[2] as IntervalPhaseOutput).Status == PhaseStatus.NA 
                || (fgram.Phases[2] as IntervalPhaseOutput).Status == PhaseStatus.Reject)
            {
                throw new PhaseInitiationException("No Interval Data Present.");
            }

            m_phase_type = PhaseFormType.Phase;
            m_video = m_filegram.Video;
            this.Text = m_filegram.Name;

            this.KeyDown += new KeyEventHandler(frmPhasePlusNew_KeyDown);
            //this.Resize += new EventHandler(frmPhasePlusNew_Resize);
            //this.FormClosing += new FormClosingEventHandler(frmPhasePlusNew_FormClosing);

            ucPhaseFunctionControlDisplay1.SuspendGraph();

            //Read-Only Variables
            m_interval_output = (fgram.Phases[2] as IntervalPhaseOutput);
            m_interval_data = m_interval_output.Data[0] as IntervalPhaseData;
            m_interval_settings = m_interval_output.Settings[0] as IntervalPhaseSettings;
            m_phase_output = (fgram.Phases[3] as PhasePhaseOutput);

            m_phase_settings = m_phase_output.Settings[0] as PhasePhaseSettings;
            m_phase_settings.SettingsChanged += new EventHandler(m_phase_settings_SettingsChanged);

            m_preprocess_data = (fgram.Phases[1] as PreprocessPhaseOutput).Data[0] as PreprocessPhaseData;

            m_phase_data = (m_phase_output.Data[0] as PhasePhaseData);

            //Set Functions
            ucPhaseFunctionControlDisplay1.SetXValuesFromY(m_preprocess_data.Intensity);
            ucPhaseFunctionControlDisplay1.InitialIntervals = m_interval_data.Intervals;

            m_current_frame_range = new VideoFrameRange(m_filegram.Video, Globals.Options.FramesPerRange);

            ucPhaseFunctionControlDisplay1.SetSettings(ref m_phase_settings);
            ucPhaseFunctionControlDisplay1.Range = m_current_frame_range;

            mModeDisplay1.Range = m_current_frame_range;
            mModeDisplay1.MMode = m_preprocess_data.MMode;
            mModeDisplay1.List = m_phase_data.Intervals;
            //mModeDisplay1.MouseMove += new MouseEventHandler(mModeDisplay_MouseMove);

            if(IsLocked)
            {
                ucPhaseFunctionControlDisplay1.Enabled = false;
                mModeDisplay1.Enabled = false;
                mModeThumbnail.Enabled = false;
            }
            else
            {
                try
                {
                    m_video.Open();
                    m_mmode_display_frame = m_video.GetFrameSync(1);
                    m_video_present = true;
                }
                catch(VideoFileNotFoundException vnfe)
                {
                    //m_video_present = false;
                    //scrollDarknessHigh.Enabled = false;
                    //scrollDarknessLow.Enabled = false;
                    //scrollIntensity.Enabled = false;
                    //cmdAccept.Enabled = false;
                    //cmdDiscard.Enabled = false;
                    //cmdReset.Enabled = false;
                    //mModeDisplay.Enabled = false;
                    //mModeThumbnail.Enabled = false;
                    //chkInvert.Enabled = false;
                    //chkUseDarkness.Enabled = false;
                    //bpFilterBright.Enabled = false;
                    //bpFilterIntensity.Enabled = false;
                }

                m_video.MModeComplete += new MModeEventHandler(m_video_MModeComplete);

                if(m_video_present)
                {
                    mModeThumbnail.XCoord = m_preprocess_data.MMode.XCoordinate;
                    mModeThumbnail.Thumbnail = m_mmode_display_frame.ToBitmap();
                    mModeThumbnail.XCoordChanged += new MouseEventHandler(mModeThumbnail_XCoordChanged);
                }
            }
            //ucPhaseFunctionControlDisplay1.VideoFrameRangeChanged += new EventHandler(ucPhaseFunctionControlDisplay1_VideoFrameRangeChanged);

            ucPhaseFunctionControlDisplay1.ResumeGraph();
            ucPhaseFunctionControlDisplay1.DrawGraph();
            
            OnPhaseBegin(this, new PhaseCompleteEventArgs(true, m_phase_type, m_output));
            OnSizeChanged(new EventArgs());
        }

        void ucPhaseFunctionControlDisplay1_VideoFrameRangeChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public static frmPhasePlusNew Spawn(Filegram fgram)
        {
            return new frmPhasePlusNew(fgram);
        }

        void m_phase_settings_SettingsChanged(object sender, EventArgs e)
        {
            RecalculateIntervals(sender, e);
        }

        void RecalculateIntervals(object sender, EventArgs e)
        {
            List<Interval> phase_intervals = ucPhaseFunctionControlDisplay1.GetIntervals();
            //phase_intervals = phase_intervals.Where(x => x.IntervalType == IntervalType.MaxVelocity).ToList<Interval>();

            IntervalCollection init_list = (IntervalCollection)(ucPhaseFunctionControlDisplay1.InitialIntervals.Clone());
            //init_list = IntervalCollection.XExtrapolate(init_list, zoom);

            init_list.Union<Interval>(phase_intervals).ToList<Interval>();

            init_list = new IntervalCollection(init_list.Union<Interval>(phase_intervals).ToList<Interval>());
            init_list.Sort();

            //IntervalCollection first_five = IntervalCollection.Take(init_list, 5);
            //first_five = IntervalCollection.XExtrapolate(first_five, (int)numZoom.Value);

            mModeDisplay1.List = init_list;
            //m_last_phase_intervals_baseline = IntervalCollection.XExtrapolateInverse(first_five, N);
        }

        void mModeThumbnail_XCoordChanged(object sender, MouseEventArgs e)
        {
            if(m_collecting_mmode) return;

            m_collecting_mmode = true;

            int new_x_coord = (sender as MModeThumbnail).XCoord;

            m_video.GetMMode(new_x_coord, MModeType.Full);
        }

        void m_video_MModeComplete(object sender, MMode mmode)
        {
            m_collecting_mmode = false;

            if(mmode == null) return;

            (m_filegram.Phases[1].Data[0] as PreprocessPhaseData).MMode = mmode;
            mModeDisplay1.MMode = mmode;
        }

        void frmPhasePlusNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            //throw new NotImplementedException();
        }

        void frmPhasePlusNew_Resize(object sender, EventArgs e)
        {
            this.Invalidate(true);
        }

        private int m_frames_with_zoom;
        void frmPhasePlusNew_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.A:
                case Keys.Left:
                    m_current_frame_range.PreviousRange();
                    break;

                case Keys.D:
                case Keys.Right:
                    m_current_frame_range.NextRange();
                    break;
                default:
                    break;
            }
        }

        protected override void OnComplete(object sender, PhaseCompleteEventArgs args)
        {

            args.Type = PhaseFormType.Phase;

            if(args.Accept)
            {
                //Query User about resetting all next phases in phaseoutputlist
                //if(m_filegram.Phases.CheckPostPhases(m_interval_output))
                //{
                //    string query = "Data (Phases) that depend on the data (phase) you are changing have already been completed.\n\nWould you like to reset these phases as well?";

                //    switch(MessageBox.Show(this, query, "Data Modified", MessageBoxButtons.YesNoCancel))
                //    {
                //        case System.Windows.Forms.DialogResult.Yes:
                //            m_filegram.Phases.Clear(m_interval_output);
                //            break;
                //        case System.Windows.Forms.DialogResult.No:
                //            break;
                //        case System.Windows.Forms.DialogResult.Cancel:
                //            return;
                //    }
                //}

                //if(m_interval_output.Data.ElementAt(m_current_roi) == null)
                //    m_interval_output.Data.Insert(m_current_roi, new IntervalPhaseData());

                //Check for alternating Intervals
                //if(mModeDisplay.List != null && !mModeDisplay.List.IsAlternating)
                //{
                //    this.WindowState = FormWindowState.Minimized;
                //    string text = "The accepted Interval Collection [" + mModeDisplay.List.Count + " elements] does not appear to alternate systole/diastole."
                //        + "\n\nWould you like to continue?";
                //    DialogResult result = MessageBox.Show(Globals.MainInstance, text, "Possible Error Detected", MessageBoxButtons.OKCancel);
                //    if(result == DialogResult.Cancel)
                //    {
                //        this.WindowState = FormWindowState.Maximized;
                //        return;
                //    }
                //}


                (m_phase_output.Data[0] as PhasePhaseData).Intervals = mModeDisplay1.List;
                PhasePhaseSettings phase_settings = new PhasePhaseSettings();
                mModeDisplay1.List.Run(m_video.FrameTimes);

                phase_settings = ucPhaseFunctionControlDisplay1.GetSettings();
                //IntervalPhaseSettings intensity_settings = ucIntensityFunctionControlDisplay1.GetSettings();

                //interval_settings.BrightThresholdHi = ucBrightFunctionControlDisplay1.HiThreshold;
                //interval_settings.BrightThresholdLo = ucBrightFunctionControlDisplay1.LoThreshold;
                //interval_settings.Invert = ucBrightFunctionControlDisplay1.Invert;
                //interval_settings.UseBright = ucBrightFunctionControlDisplay1.UseDarkness;
                //interval_settings.BrightLoCutoff = ucBrightFunctionControlDisplay1.LoCutoff;
                //interval_settings.BrightHiCutoff = ucBrightFunctionControlDisplay1.HiCutoff;
                //interval_settings.ShowPhase = mModeDisplay.ShowChangeIntervals;
                //interval_settings.PhaseOffset = mModeDisplay.PhaseOffset;

                //interval_settings.IntensityThreshold = ucIntensityFunctionControl1.Threshold;
                //interval_settings.IntensityMinInterval = ucIntensityFunctionControl1.MinIntervals;
                //interval_settings.IntensityLoCutoff = ucIntensityFunctionControl1.LoCutoff;
                //interval_settings.IntensityHiCutoff = ucIntensityFunctionControl1.HiCutoff;

                /* Repetitive Function w/SelectedIndexChanged of ROI ComboBox*/

                //m_interval_settings.BrightThresholdHi = (int)ucBrightFunctionControlDisplay1.HiThreshold;
                //m_interval_settings.BrightThresholdLo = (int)ucBrightFunctionControlDisplay1.LoThreshold;
                //m_interval_settings.Invert = ucBrightFunctionControlDisplay1.Invert;
                //m_interval_settings.UseBright = ucBrightFunctionControlDisplay1.UseDarkness;
                //m_interval_settings.BrightFilter = ucBrightFunctionControlDisplay1.Filter;
                //m_interval_settings.BrightLoCutoff = (int)ucBrightFunctionControlDisplay1.LoCutoff;
                //m_interval_settings.BrightHiCutoff = (int)ucBrightFunctionControlDisplay1.HiCutoff;

                //m_interval_settings.IntensityThreshold = (int)ucIntensityFunctionControl1.Threshold;
                //m_interval_settings.IntensityMinInterval = ucIntensityFunctionControl1.MinIntervals;
                //m_interval_settings.IntensityFilter = ucIntensityFunctionControl1.Filter;
                //m_interval_settings.IntensityLoCutoff = ucIntensityFunctionControl1.LoCutoff;
                //m_interval_settings.IntensityHiCutoff = ucIntensityFunctionControl1.HiCutoff;

                m_phase_output.Settings[0] = phase_settings;//interval_settings;

                PhasePhaseOutput output = new PhasePhaseOutput();
                PhasePhaseData data = new PhasePhaseData();

                //IntervalCollection.CurveFitIntervals(mModeDisplay.List, ucIntensityFunctionControl1.LastData);

                //Globals.SaveMMode(mModeDisplay1.MMode.ToBitmap(), m_filegram.Name + "_Full.bmp");
            }

            m_filegram.Phases[3].Status = args.Accept ? PhaseStatus.OK : PhaseStatus.Reject;

            base.OnComplete(sender, args);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            int control_width = ucPhaseFunctionControlDisplay1.Width;
            int left = (int)(0.04 * control_width);
            int right = (int)(0.02 * control_width);
            mModeDisplay1.Margin = new Padding(left, 3, right, 3);
            base.OnSizeChanged(e);
        }
    }
}
