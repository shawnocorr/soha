﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Video;
using SOHAControls;

using SOHA.Library;
using SOHA.Library.Mathematics;
using SOHA.Library.Xml;
using SOHA.Library.Forms;

namespace SOHA
{
    public partial class frmPhasePlus : frmPhase
    {
        private Video m_video;
        private IntervalProperties m_current_frame_interval = new IntervalProperties();

        private readonly IntervalPhaseData m_interval_data;
        private readonly IntervalPhaseSettings m_interval_settings;
        private readonly IntervalPhaseOutput m_interval_output;
        private readonly PreprocessPhaseData m_preprocess_data;
        private PhasePhaseData m_phase_data;
        private PhasePhaseSettings m_phase_settings;
        private PhasePhaseOutput m_phase_output;

        private int N = 1;
        private List<double> m_initial_function;
        private IntervalCollection m_initial_intervals;
        private IntervalCollection m_last_phase_intervals_baseline;

        private bool m_phase_data_present = false;

        public frmPhasePlus()
        {
            InitializeComponent();
        }

        public frmPhasePlus(Filegram fgram)
            : base(fgram)
        {
            InitializeComponent();

            if (!(fgram.Phases[2] is IntervalPhaseOutput) || (fgram.Phases[2] as IntervalPhaseOutput).Status == PhaseStatus.NA || (fgram.Phases[2] as IntervalPhaseOutput).Status == PhaseStatus.Reject)
            {
                throw new PhaseInitiationException("No Interval Data Present.");
            }

            m_phase_type = PhaseFormType.Phase;
            m_filegram = fgram;
            m_video = m_filegram.Video;
            m_filegram.Status = FilegramStatus.UserInterface;
            this.Text = m_filegram.Name;

            this.KeyDown += new KeyEventHandler(frmInterval_KeyDown);
            this.Resize += new EventHandler(frmInterval_Resize);
            this.FormClosing += new FormClosingEventHandler(frmInterval_FormClosing);

            m_interval_output = (fgram.Phases[2] as IntervalPhaseOutput);
            m_interval_data = m_interval_output.Data[0] as IntervalPhaseData;
            m_interval_settings = m_interval_output.Settings[0] as IntervalPhaseSettings;
            m_preprocess_data = (fgram.Phases[1] as PreprocessPhaseOutput).Data[0] as PreprocessPhaseData;
            m_phase_output = (fgram.Phases[3] as PhasePhaseOutput);

            if (m_phase_output.Settings.Count == 0)
                m_phase_output.Settings.Add(new PhasePhaseSettings());

            m_phase_settings = m_phase_output.Settings[0] as PhasePhaseSettings;// != null ? m_phase_output.Settings[0] as PhasePhaseSettings : new PhasePhaseSettings() ;

            if(m_phase_output.Data.Count < 1) m_phase_output.Data.Add(new PhasePhaseData());
            m_phase_data = m_phase_output.Data[0] as PhasePhaseData;


            phaseDisplay1.Interval = mModeDisplay1.Interval = m_current_frame_interval;

            mModeDisplay1.MMode = m_preprocess_data.MMode;// MMode.XExtrapolate(m_preprocess_data.MMode, N);
            //mModeDisplay1.Phase = PhaseType.Phase;
            mModeDisplay1.List = m_phase_data.Intervals;
            mModeDisplay1.ShowChangeIntervals = true;
            mModeDisplay1.MouseMove += new MouseEventHandler(mModeDisplay1_MouseMove);

            phaseDisplay1.InitialData = m_preprocess_data.Intensity;
            m_initial_function = m_preprocess_data.Intensity;// ImageProcessing.XExtrapolate(m_preprocess_data.Intensity, N);
            m_initial_intervals = m_interval_data.Intervals;// IntervalCollection.XExtrapolate(m_interval_data.Intervals, N);
            if (m_phase_data.Intervals != null && m_phase_data.Intervals.Count != 0)
            {
                IntervalCollection init_list = (IntervalCollection)(m_interval_data.Intervals.Clone());
                IntervalCollection init_phase_list = (IntervalCollection)(m_phase_data.Intervals.Clone());

                init_list.Union(init_phase_list);
                init_list.Sort();
                m_last_phase_intervals_baseline = init_list;

                //mModeDisplay1.List = IntervalCollection.XExtrapolate(init_list, N);

                m_phase_data_present = true;
            }
            phaseDisplay1.Data = m_initial_function;
            phaseDisplay1.InitialList = m_initial_intervals;
            phaseDisplay1.Fps = m_video.Fps;

            numZoom.Value = m_phase_settings.Zoom;
            m_frames_with_zoom = m_video.Frames * m_phase_settings.Zoom;
            numOffset.Value = phaseDisplay1.Offset = m_phase_settings.Offset;
            bpFilter.LoCutoff = phaseDisplay1.LoCutoff = m_phase_settings.LoCutoff;
            bpFilter.HiCutoff = phaseDisplay1.HiCutoff = m_phase_settings.HiCutoff;
            chkThreshold.Checked = phaseDisplay1.UseThreshold = m_phase_settings.UseThreshold;
            scrollIntensity.Value = phaseDisplay1.Threshold = m_phase_settings.Threshold;
            phaseDisplay1.Zoom = mModeDisplay1.Zoom = (int)numZoom.Value;
            //chkThreshold.Checked = m_phase_settings.UseThreshold;
            //numZoom_ValueChanged(this, new EventArgs());

            numOffset.DataBindings.Add(new Binding("Value", phaseDisplay1, "Offset", false, DataSourceUpdateMode.OnPropertyChanged));
            bpFilter.DataBindings.Add(new Binding("HiCutoff", phaseDisplay1, "HiCutoff", false, DataSourceUpdateMode.OnPropertyChanged));
            bpFilter.DataBindings.Add(new Binding("LoCutoff", phaseDisplay1, "LoCutoff", false, DataSourceUpdateMode.OnPropertyChanged));
            chkThreshold.DataBindings.Add(new Binding("Checked", phaseDisplay1, "UseThreshold", false, DataSourceUpdateMode.OnPropertyChanged));
            scrollIntensity.DataBindings.Add(new Binding("Value", phaseDisplay1, "Threshold", false, DataSourceUpdateMode.OnPropertyChanged));

            phaseDisplay1.IntervalsChanged += new EventHandler(IntervalsChanged);
            phaseDisplay1.ThresholdsChanged += new EventHandler(RecalculateIntervals);
            phaseDisplay1.MouseMove += new MouseEventHandler(phaseDisplay1_MouseMove);

            numZoom.ValueChanged += new EventHandler(numZoom_ValueChanged);
            mModeDisplay1.ListChanged += new EventHandler(mModeDisplay1_ListChanged);

            mModeDisplay1.Invalidate();

            if(m_phase_data.Intervals == null || m_phase_data.Intervals.Count == 0) RecalculateIntervals(this, new EventArgs());


            m_initialized = true;


            if (!m_phase_data_present)
                RecalculateIntervals(this, new EventArgs());
        }
        
        void mModeDisplay1_ListChanged(object sender, EventArgs e)
        {
            m_last_phase_intervals_baseline = IntervalCollection.XExtrapolateInverse(mModeDisplay1.List, (int)numZoom.Value);
        }

        void mModeDisplay1_MouseMove(object sender, MouseEventArgs e)
        {
            lblCoordinates.Text = "X: " + (mModeDisplay1.GetXValue(e.X) + (m_current_frame_interval.Interval - 1) * (1000/(int)numZoom.Value)) + "; Y:" + (mModeDisplay1.Height - e.Y);
        }

        void phaseDisplay1_MouseMove(object sender, MouseEventArgs e)
        {
            double[] point = phaseDisplay1.GetValues(e.X,m_current_frame_interval.Interval,m_current_frame_interval.IntervalSize);
            lblCoordinates.Text = "Frame: " + point[0] + "; Value: " + point[1];
        }

        void numZoom_ValueChanged(object sender, EventArgs e)
        {
            N = (int)numZoom.Value;
            phaseDisplay1.Zoom = N;
            mModeDisplay1.Zoom = N;
            //m_frames_with_zoom = m_video.Frames * N;
            //m_current_frame_interval.Interval = 1;
            //mModeDisplay1.MMode = MMode.XExtrapolate(m_preprocess_data.MMode, N); 
            //m_initial_function = ImageProcessing.XExtrapolate(m_preprocess_data.Intensity, N);
            ////m_initial_intervals = IntervalCollection.XExtrapolate(mModeDisplay1.List, N);
            //IntervalCollection new_intervals = IntervalCollection.XExtrapolate(m_last_phase_intervals_baseline, N);
            //mModeDisplay1.List = new_intervals;
            //phaseDisplay1.Data = m_initial_function;
            //phaseDisplay1.InitialList = m_initial_intervals;

            //mModeDisplay1.Invalidate();
            //phaseDisplay1.Invalidate();

            //RecalculateIntervals(sender, e);
        }

        void IntervalsChanged(object sender, EventArgs e)
        {
            
        }

        void RecalculateIntervals(object sender, EventArgs e)
        {
            if (!m_initialized)
                return;

            int zoom = 1;// (int)numZoom.Value;

            List<Interval> phase_intervals = phaseDisplay1.GetIntervals();
            //phase_intervals = phase_intervals.Where(x => x.IntervalType == IntervalType.MaxVelocity).ToList<Interval>();

            IntervalCollection init_list = (IntervalCollection)(m_initial_intervals.Clone());
            //init_list = IntervalCollection.XExtrapolate(init_list, zoom);

            init_list.Union<Interval>(phase_intervals).ToList<Interval>();

            init_list = new IntervalCollection(init_list.Union<Interval>(phase_intervals).ToList<Interval>());
            init_list.Sort();

            IntervalCollection first_five = IntervalCollection.Take(init_list, 5);
            //first_five = IntervalCollection.XExtrapolate(first_five, (int)numZoom.Value);

            mModeDisplay1.List = first_five;
            //m_last_phase_intervals_baseline = IntervalCollection.XExtrapolateInverse(first_five, N);
        }

        void frmInterval_FormClosing(object sender, FormClosingEventArgs e)
        {
            //throw new NotImplementedException();
        }

        void frmInterval_Resize(object sender, EventArgs e)
        {
            this.Invalidate(true);
        }

        private int m_frames_with_zoom;
        void frmInterval_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                //case Keys.Escape:
                //    OnComplete(new PhaseCompleteEventArgs(false, PhaseType.Mark, m_interval_output));
                //    break;
                //case Keys.Space:
                //    OnComplete(new PhaseCompleteEventArgs(true, PhaseType.Mark, m_interval_output));
                //    break;
                case Keys.A:
                case Keys.Left:
                    if (m_current_frame_interval.Interval > 1)
                        m_current_frame_interval.Interval--;
                    break;

                case Keys.D:
                case Keys.Right:
                    if (m_current_frame_interval.Interval < m_frames_with_zoom / m_current_frame_interval.IntervalSize + 1)
                        m_current_frame_interval.Interval++;
                    break;
                default:
                    break;
            }
        }

        protected override void OnComplete(object sender, PhaseCompleteEventArgs args)
        {
            args.Type = PhaseFormType.Phase;

            if (args.Accept)
            {
                m_filegram.Phases[3].Data.Clear();
                m_filegram.Phases[3].Settings.Clear();

                m_phase_settings.Offset = phaseDisplay1.Offset;
                m_phase_settings.HiCutoff = phaseDisplay1.HiCutoff;
                m_phase_settings.LoCutoff = phaseDisplay1.LoCutoff;
                m_phase_settings.Zoom = (int)numZoom.Value;
                m_phase_settings.Threshold = phaseDisplay1.Threshold;
                m_phase_settings.UseThreshold = chkThreshold.Checked;

                //IntervalCollection list_output = IntervalCollection.XExtrapolateInverse(mModeDisplay1.List, N);
                m_phase_data.Intervals = mModeDisplay1.List;

                m_filegram.Phases[3].Data.Add(m_phase_data);
                m_filegram.Phases[3].Settings.Add(m_phase_settings);
            }
            else
            {

            }

            m_filegram.Phases[3].Status = args.Accept ? PhaseStatus.OK : PhaseStatus.Reject;

            base.OnComplete(this, args);
        }

        public static frmPhasePlus Spawn(Filegram fgram)
        {
            return new frmPhasePlus(fgram);
        }

        private void chkThreshold_CheckedChanged(object sender, EventArgs e)
        {
            phaseDisplay1.UseThreshold = chkThreshold.Checked;
        }
    }
}
