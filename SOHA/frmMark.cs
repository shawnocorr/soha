﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SOHA.Library;
using SOHA.Library.Video;
using SOHA.Library.Xml;
using SOHA.Library.GPU;
using SOHA.Library.Mathematics;
using SOHA.Library.Vision;
using System.Threading;
using SOHA.Library.Utilities;
using SOHA.Library.Forms;

namespace SOHA
{
    public partial class frmMark : frmPhase
    {
        private readonly string ORIGINAL_FRAME_SIZE;

        private Frame m_frame;
        private Bitmap m_bmp;
        private Video m_video;
        private MarkPhaseOutput m_output;
        private bool m_mouseover = false;
        private MouseEventArgs m_mouse_event;

        private frmFilterToolbox frmFilters = new frmFilterToolbox();

        public frmMark()
        {
            InitializeComponent();
        }

        public frmMark(Filegram fgram)
            : base(fgram)
        {

            //base constructor run first

            InitializeComponent();
            PHASE = PhaseName.Mark;

            m_phase_type = SOHA.Library.PhaseFormType.Mark;
            this.Text = m_filegram.Name;

            this.KeyDown += new KeyEventHandler(frmMark_KeyDown);
            //this.Resize += new EventHandler(frmMark_Resize);
            //this.FormClosing += new FormClosingEventHandler(frmMark_FormClosing);

            m_output = m_filegram.Phases[PHASE] as MarkPhaseOutput;

            ORIGINAL_FRAME_SIZE = "Original Frame Size: " + frameDisplay1.CurrentFrame.Size.ToString();

            frameDisplay1.Marks = (m_output.Data[0] as MarkPhaseData).Marks;
            frameDisplay1.ROI = (m_output.Data[0] as MarkPhaseData).Rois;
            
            frameDisplay1.Paint += new PaintEventHandler(frameDisplay1_Paint);
            frameDisplay1.FrameChanged += new EventHandler<FrameEventArgs>(frameDisplay1_FrameChanged);
            frameDisplay1.CoordinatesChanged += new EventHandler(frameDisplay1_CoordinatesChanged);

            cmbCamera.DataSource = CameraCollection.List;
            //cmbCamera.SelectedIndex = m_video.Camera == null ? CameraCollection.List.IndexOf(Globals.DefaultCamera()) : CameraCollection.List.IndexOf(m_video.Camera);
            cmbCamera.SelectedIndexChanged += new EventHandler(cmbCamera_SelectedIndexChanged);
            cmbCamera.MouseWheel += new MouseEventHandler(cmbCamera_MouseWheel);

            dgvMark.DataSource = frameDisplay1.Marks;
            dgvRoi.DataSource = frameDisplay1.ROI;
            frameDisplay1.Marks.ListChanged += new ListChangedEventHandler(Marks_ListChanged);
            frameDisplay1.ROI.ListChanged += new ListChangedEventHandler(ROI_ListChanged);
            //dgvMark.DataUpdated += new EventHandler(grid_DataUpdated);
            //dgvRoi.DataUpdated += new EventHandler(grid_DataUpdated);

            //if((m_output.Options[0].Count as MarkPhaseOptions)
            //frmFilters.SetAllFilters((m_output.Options[0] as MarkPhaseOptions).Filters);
            frmFilters.Update += new EventHandler(frmFilters_Update);

            //Append label to status strip
            statusStrip1.Items.Add(ORIGINAL_FRAME_SIZE.ToString());

            //Lock Form
            if(IsLocked)
            {
                dgvMark.Enabled = false;
                dgvRoi.Enabled = false;
                frameDisplay1.Enabled = false;
                cmbCamera.Enabled = false;
            }

            m_video_present = m_filegram.Video.IsVideoPresent;
            if(m_video_present)
            {
                m_video = m_filegram.Video;
                frameDisplay1.VideoFile = m_video;
            }
            
            OnPhaseBegin(this, new PhaseCompleteEventArgs(true, m_phase_type, m_output));
        }

        void ROI_ListChanged(object sender, ListChangedEventArgs e)
        {
            dgvRoi.Refresh();
            //throw new NotImplementedException();
        }

        void Marks_ListChanged(object sender, ListChangedEventArgs e)
        {
            dgvMark.Refresh();
            //throw new NotImplementedException();
        }

        void grid_DataUpdated(object sender, EventArgs e)
        {
            frameDisplay1.Invalidate(true);
        }

        void frameDisplay1_CoordinatesChanged(object sender, EventArgs e)
        {
            lblCoordinates.Text = "Frame: " + frameDisplay1.CurrentFrame.FrameNumber + " " + frameDisplay1.Coordinates.ToString();// "X=" + frameDisplay1.Coordinates.X + "; Y=" + frameDisplay1.Coordinates.Y;
            lblCoordinates.Invalidate();
            statusStrip1.Update();
        }

        void frameDisplay1_FrameChanged(object sender, EventArgs e)
        {
            lblCoordinates.Text = "Frame: " + frameDisplay1.CurrentFrame.FrameNumber + " " + frameDisplay1.Coordinates.ToString();// "X=" + frameDisplay1.Coordinates.X + "; Y=" + frameDisplay1.Coordinates.Y;
            lblCoordinates.Invalidate();
            //frameDisplay1.Invalidate();
            statusStrip1.Update();
        }

        void frameDisplay1_Paint(object sender, PaintEventArgs e)
        {
            //dgvMark.Update();
            //dgvRoi.Update();
            //this.Update();
        }

        void frmFilters_Update(object sender, EventArgs e)
        {
            frameDisplay1.Filters = frmFilters.GetAllFilters();
        }
        
        void cmbCamera_MouseWheel(object sender, MouseEventArgs e)
        {

        }

        void ListChanged(object sender, ListChangedEventArgs e)
        {
            //dgvMark.Update();
            //dgvRoi.Update();
            //this.Refresh();
        }

        void cmbCamera_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_video.Camera = CameraCollection.List[cmbCamera.SelectedIndex];
        }

        void frmMark_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_video_present)
            {
                m_video.Close();
            }
        }

        void frmMark_Resize(object sender, EventArgs e)
        {
            //this.Invalidate(true);
            this.Refresh();
            //frameDisplay1.Invalidate();
            //dgvMark.Refresh();
            //dgvRoi.Refresh();
        }

        void frmMark_KeyDown(object sender, KeyEventArgs e)
        {
            frameDisplay1.KeyPress(e);
        }

        protected override void OnComplete(object sender, PhaseCompleteEventArgs args)
        {

            if(!IsLocked)
            {
                /* Verify valid marking
                 * - 0 marks -> OK
                 * - sys >= 1 && dias >= 1 -> OK
                 */
                MarkPhaseData data = new MarkPhaseData();// = m_output.Data[0] as MarkPhaseData;

                data.Marks = frameDisplay1.Marks;
                data.Rois = frameDisplay1.ROI;

                int sys = data.Marks.Where<MarkPair>(mp => mp.Beat == BeatType.Systole).Count();
                int dias = data.Marks.Where<MarkPair>(mp => mp.Beat == BeatType.Diastole).Count();

                if(args.Accept && ((sys >= 1 && dias == 0) || (sys == 0 && dias >= 1)))
                {
                    this.WindowState = FormWindowState.Minimized;
                    string text = "You have marked " + sys + " systolic diameters.\nYou have marked "
                        + dias + " diastolic intervals.\n\nIf this is correct, click OK."
                        + "\nTo go back and re-mark, click Cancel.";
                    DialogResult result = MessageBox.Show(Globals.MainInstance, text, "Possible Error Detected", MessageBoxButtons.OKCancel);
                    if(result == DialogResult.Cancel)
                    {
                        this.WindowState = FormWindowState.Maximized;
                        return;
                    }
                }

                MarkPhaseOptions options = new MarkPhaseOptions();
                options.Filters = frmFilters.GetAllFilters();

                PreprocessPhaseOptions process_options = new PreprocessPhaseOptions();
                if(options.Filters.Count > 0) process_options.EnableFilters = true;

                if(args.Accept)
                {
                    m_filegram.Video.Camera = (Camera)CameraCollection.List[cmbCamera.SelectedIndex];
                    m_filegram.Phases[PHASE].Data.Clear();
                    m_filegram.Phases[PHASE].Data.Add(data);

                    m_filegram.Phases[PHASE].Options.Clear();
                    m_filegram.Phases[PHASE].Options.Add(options);

                    m_filegram.Phases[1].Options.Clear();
                    m_filegram.Phases[1].Options.Add(process_options);
                }

                m_filegram.Phases[0].Status = args.Accept ? PhaseStatus.OK : PhaseStatus.Reject;
            }

            base.OnComplete(this, args);
        }

        public static frmMark Spawn(Filegram fgram)
        {
            return new frmMark(fgram);
        }

        private void cmdFilter_Click(object sender, EventArgs e)
        {
            frmFilters.ShowDialog(this);
        }

        private void numBrightScale_ValueChanged(object sender, EventArgs e)
        {
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if(m_video is AVIVideo)
            {
                //m_video.GetSnapshot("");
            }
        }
    }
}
