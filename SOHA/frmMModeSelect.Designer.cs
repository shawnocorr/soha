﻿namespace SOHA
{
    partial class frmMModeSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMModeSelect));
            SOHAControls.IntervalProperties intervalProperties2 = new SOHAControls.IntervalProperties();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTimeCreate = new System.Windows.Forms.Button();
            this.txtTimeFinish = new System.Windows.Forms.TextBox();
            this.txtTimeStart = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnFrameCreate = new System.Windows.Forms.Button();
            this.txtFrameFinish = new System.Windows.Forms.TextBox();
            this.txtFrameStart = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.scrollXCoord = new System.Windows.Forms.HScrollBar();
            this.mModeThumbnail1 = new SOHAControls.MModeThumbnail(this.components);
            this.mModeDisplay1 = new Forms.MModeDisplay(this.components);
            this.toolStripContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(780, 360);
            this.toolStripContainer1.Size = new System.Drawing.Size(780, 360);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mModeDisplay1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(780, 360);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(533, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(244, 174);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnTimeCreate);
            this.groupBox1.Controls.Add(this.txtTimeFinish);
            this.groupBox1.Controls.Add(this.txtTimeStart);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 81);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MMode by Time";
            // 
            // btnTimeCreate
            // 
            this.btnTimeCreate.Location = new System.Drawing.Point(157, 37);
            this.btnTimeCreate.Name = "btnTimeCreate";
            this.btnTimeCreate.Size = new System.Drawing.Size(75, 23);
            this.btnTimeCreate.TabIndex = 4;
            this.btnTimeCreate.Text = "Create";
            this.btnTimeCreate.UseVisualStyleBackColor = true;
            this.btnTimeCreate.Click += new System.EventHandler(this.btnTimeCreate_Click);
            // 
            // txtTimeFinish
            // 
            this.txtTimeFinish.Location = new System.Drawing.Point(85, 37);
            this.txtTimeFinish.Name = "txtTimeFinish";
            this.txtTimeFinish.Size = new System.Drawing.Size(66, 20);
            this.txtTimeFinish.TabIndex = 3;
            // 
            // txtTimeStart
            // 
            this.txtTimeStart.Location = new System.Drawing.Point(10, 37);
            this.txtTimeStart.Name = "txtTimeStart";
            this.txtTimeStart.Size = new System.Drawing.Size(69, 20);
            this.txtTimeStart.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Finish";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Start";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnFrameCreate);
            this.groupBox2.Controls.Add(this.txtFrameFinish);
            this.groupBox2.Controls.Add(this.txtFrameStart);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 90);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 81);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MMode by Frame";
            // 
            // btnFrameCreate
            // 
            this.btnFrameCreate.Location = new System.Drawing.Point(157, 35);
            this.btnFrameCreate.Name = "btnFrameCreate";
            this.btnFrameCreate.Size = new System.Drawing.Size(75, 23);
            this.btnFrameCreate.TabIndex = 4;
            this.btnFrameCreate.Text = "Create";
            this.btnFrameCreate.UseVisualStyleBackColor = true;
            this.btnFrameCreate.Click += new System.EventHandler(this.btnFrameCreate_Click);
            // 
            // txtFrameFinish
            // 
            this.txtFrameFinish.Location = new System.Drawing.Point(85, 37);
            this.txtFrameFinish.Name = "txtFrameFinish";
            this.txtFrameFinish.Size = new System.Drawing.Size(66, 20);
            this.txtFrameFinish.TabIndex = 3;
            // 
            // txtFrameStart
            // 
            this.txtFrameStart.Location = new System.Drawing.Point(10, 37);
            this.txtFrameStart.Name = "txtFrameStart";
            this.txtFrameStart.Size = new System.Drawing.Size(69, 20);
            this.txtFrameStart.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(82, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Finish";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Start";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.scrollXCoord, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.mModeThumbnail1, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(524, 174);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // scrollXCoord
            // 
            this.scrollXCoord.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollXCoord.Location = new System.Drawing.Point(0, 155);
            this.scrollXCoord.Name = "scrollXCoord";
            this.scrollXCoord.Size = new System.Drawing.Size(524, 17);
            this.scrollXCoord.TabIndex = 0;
            // 
            // mModeThumbnail1
            // 
            this.mModeThumbnail1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mModeThumbnail1.BackgroundImage")));
            this.mModeThumbnail1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mModeThumbnail1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mModeThumbnail1.Location = new System.Drawing.Point(3, 3);
            this.mModeThumbnail1.Name = "mModeThumbnail1";
            this.mModeThumbnail1.Size = new System.Drawing.Size(518, 148);
            this.mModeThumbnail1.TabIndex = 1;
            this.mModeThumbnail1.TabStop = false;
            this.mModeThumbnail1.Thumbnail = ((System.Drawing.Bitmap)(resources.GetObject("mModeThumbnail1.Thumbnail")));
            this.mModeThumbnail1.XCoord = 0;
            // 
            // mModeDisplay1
            // 
            this.mModeDisplay1.DisplayCoordinates = false;
            this.mModeDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            intervalProperties2.Interval = 1;
            intervalProperties2.IntervalSize = 1000;
            this.mModeDisplay1.Interval = intervalProperties2;
            this.mModeDisplay1.List = null;
            this.mModeDisplay1.Location = new System.Drawing.Point(3, 183);
            this.mModeDisplay1.MMode = null;
            this.mModeDisplay1.Name = "mModeDisplay1";
            this.mModeDisplay1.Phase = SOHA.Library.PhaseFormType.Interval;
            this.mModeDisplay1.PhaseOffset = 0;
            this.mModeDisplay1.ShowChangeIntervals = false;
            this.mModeDisplay1.Size = new System.Drawing.Size(524, 174);
            this.mModeDisplay1.TabIndex = 4;
            this.mModeDisplay1.TabStop = false;
            this.mModeDisplay1.Zoom = 1;
            // 
            // frmMModeSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 382);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "frmMModeSelect";
            this.Text = "frmMModeSelect";
            this.Controls.SetChildIndex(this.toolStripContainer1, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnTimeCreate;
        private System.Windows.Forms.TextBox txtTimeFinish;
        private System.Windows.Forms.TextBox txtTimeStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnFrameCreate;
        private System.Windows.Forms.TextBox txtFrameFinish;
        private System.Windows.Forms.TextBox txtFrameStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.HScrollBar scrollXCoord;
        private SOHAControls.MModeThumbnail mModeThumbnail1;
        private Forms.MModeDisplay mModeDisplay1;
    }
}