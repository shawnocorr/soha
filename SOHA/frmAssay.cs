﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using SOHA.Library;
using SOHAControls;
using System.Threading;
using SOHA.Library.Video;
using SOHA.Library.Forms;
using SOHA.Library.Xml;
using WeifenLuo.WinFormsUI.Docking;

namespace SOHA
{
    public partial class frmAssay : DockContent
    {
        public event EventHandler SelectedFilegramChanged;

        private object syncLock = new object();
        private ProcessQueue PQueue = new ProcessQueue(false);

        public Filegram SelectedFilegram
        {
            get
            {
                return Globals.FilegramList[lstFilegrams.SelectedIndices[0]];
            }
        }

        public frmAssay()
        {
            InitializeComponent();

            //Comment out 8/8/16
            //Initialize();
        }

        public void Initialize()
        {
            foreach(Filegram f in Globals.FilegramList)
            {
                f.StatusChanged += new EventHandler(f_StatusChanged);
                f.PhaseComplete += new PhaseCompleteEventHandler(f_PhaseComplete);
                f.ProcessComplete += new ProcessComplete(f_ProcessComplete);
            }

            this.Load += new EventHandler(frmAssay_Load);
            this.FormClosing += new FormClosingEventHandler(frmAssay_FormClosing);
            this.Paint += new PaintEventHandler(frmAssay_Paint);
            this.Resize += new EventHandler(frmAssay_Resize);

            lstFilegrams.MouseClick += new MouseEventHandler(lstFilegrams_MouseClick);

            markToolStripMenuItem.Click += new EventHandler(PhaseSelected);
            preprocessToolStripMenuItem.Click += new EventHandler(PhaseSelected);
            intervalToolStripMenuItem.Click += new EventHandler(PhaseSelected);
            phaseToolStripMenuItem.Click += new EventHandler(PhaseSelected);
            redDotMovieToolStripMenuItem.Click += new EventHandler(PhaseSelected);
            mModeSelectToolStripMenuItem.Click += new EventHandler(PhaseSelected);
            propertiesToolStripMenuItem.Click += new EventHandler(PhaseSelected);
            workBenchToolStripMenuItem.Click += new EventHandler(PhaseSelected);
            intervalWorkbenchToolStripMenuItem.Click += new EventHandler(PhaseSelected);

            //this.WindowState = FormWindowState.Maximized;

            //lstFilegrams.
            lstFilegrams.Font = new Font(FontFamily.GenericSansSerif, 12.0f);
            lstFilegrams.BeginUpdate();
            lstFilegrams.Items.Clear();
            
            AddColumns();

            foreach(Filegram f in Globals.FilegramList)
            {
                lstFilegrams.Items.Add(f.ToListViewItem());
            }

            lstFilegrams.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            lstFilegrams.Columns[0].Width = 100;

            lstFilegrams.EndUpdate();
            lstFilegrams.Dock = DockStyle.Fill;

            this.Text = Globals.CurrentDirectory;
        }

        void f_ProcessComplete(object sender, ProgressEventArgs e)
        {
            (sender as Filegram).Serialize(sender as Filegram);
            //Preprocess Complete
        }

        void f_PhaseComplete(object sender, PhaseCompleteEventArgs e)
        {
            Filegram fg = (sender as Filegram);
            fg.Serialize(fg);

            
            bool isLastFilegram = false;
            int index_complete_filegram = Globals.FilegramList.IndexOf(fg);
            int index_next_filegram = index_complete_filegram + 1;

            isLastFilegram = (index_next_filegram == Globals.FilegramList.Count);

            if(e.Type == PhaseFormType.Mark &&
                Options.AutoProcess)
            {
                BeginPhase(Globals.FilegramList[index_complete_filegram], PhaseFormType.Preprocess);
            }

            if(Enums.UI_FORMS.Contains(e.Type))
            {
                if(Options.AutoGUI)
                {
                    if(!isLastFilegram)
                    {
                        BeginPhase(Globals.FilegramList[index_next_filegram], e.Type);
                    }
                }
            }
        }

        void frmAssay_Resize(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void Queue_ProgressUpdate(SOHA.Library.Video.Video sender, int progress)
        {
            Filegram temp_fg = Globals.FilegramList.Where(x => x.Video == sender).First();
            int index = Globals.FilegramList.IndexOf(temp_fg);
            Globals.FilegramList[index].SetProgress(progress);
        }

        void outputIntervalsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i = lstFilegrams.SelectedIndices[0];
            //Globals.FilegramList[i].
        }

        void lstFilegrams_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListView.SelectedIndexCollection index = (sender as ListView).SelectedIndices;

                List<Filegram> selection = Globals.FilegramList.GetRange(index[0], index.Count);

                bool processing = selection.Where(x => x.Status == FilegramStatus.Processing || x.Status == FilegramStatus.Queued).Count() > 0;

                this.preprocessToolStripMenuItem.Text = (processing ? "Cancel Process" : "Process");

                //Deprecated 3/7/2015
                //Single Index Selected
                //if (index.Count == 1)
                //{
                //    int i = index[0];

                //    if (Globals.FilegramList[i].Status == FilegramStatus.Processing || Globals.FilegramList[i].Status == FilegramStatus.Queued)
                //    {
                //        this.preprocessToolStripMenuItem.Text = "Cancel Process";
                //    }
                //    else
                //    {
                //        this.preprocessToolStripMenuItem.Text = "Process";
                //    }
                //}

                //if(selection.Count > 1)
                //{
                //    mnuPhase.Items
                //}

                mnuPhase.Show(lstFilegrams, e.Location);
            }
        }

        void PhaseSelected(object sender, EventArgs e)
        {
            ListView.SelectedIndexCollection index = lstFilegrams.SelectedIndices;
            PhaseFormType phase = (sender as PhaseToolStripItem).Phase;

            List<Filegram> selection = Globals.FilegramList.GetRange(index[0], index.Count);

            bool processing = selection.Where(x => x.Status == FilegramStatus.Processing || x.Status == FilegramStatus.Queued).Count() > 0;

            if(index.Count == 1 && Globals.FilegramList[index[0]].Status == FilegramStatus.Error)
                return;

            if(phase == PhaseFormType.Preprocess)
            {
                if(processing)
                {
                    ProcessCancelMessageBox box = new ProcessCancelMessageBox();
                    int ind = index[0];

                    switch(box.Show("Cancel " + (Globals.FilegramList[ind] as Filegram).Name + "?"))
                    {
                        case DialogResult.Yes:
                            Filegram f = Globals.FilegramList[ind] as Filegram;

                            PQueue.Cancel(f.Video);
                            f.Status = FilegramStatus.Idle;

                            break;
                        case DialogResult.No:
                            //Do Nothing
                            break;
                        case DialogResult.Abort:

                            PQueue.Cancel(selection.Select(x => x.Video).AsEnumerable<IProcessable>());

                            break;
                    }

                    box.Dispose();
                }
                else
                {
                    foreach(int i in index)
                    {
                        Globals.FilegramList[i].Preprocess(PQueue, true);
                    }
                }
            }
            else
            {
                foreach(Filegram fg in selection)
                {
                    BeginPhase(fg, phase);
                }
            }

            
        }

        private void BeginPhase(Filegram fg, PhaseFormType type)
        {
            try
            {
                frmPhase frm = new frmPhase();
                switch(type)
                {
                    case PhaseFormType.New:
                        break;
                    case PhaseFormType.Mark:
                        frm = new frmMark(fg);
                        break;
                    case PhaseFormType.Preprocess:
                        //frm = new frm(fg);
                        break;
                    case PhaseFormType.Interval:
                        frm = new frmIntervalNew(fg);
                        break;
                    case PhaseFormType.Phase:
                        frm = new frmPhasePlusNew(fg);
                        break;
                    case PhaseFormType.RedDot:
                        frm = new frmRedDotMovie(fg);
                        break;
                    case PhaseFormType.MModeSelect:
                        frm = new frmMModeSelect(fg);
                        break;
                    case PhaseFormType.Properties:
                        frm = new frmFilegramProperties(fg);
                        break;
                    case PhaseFormType.WorkBench:
                        frm = new frmWorkBench(fg);
                        break;
                    case PhaseFormType.IntervalWorkbench:
                        frm = new frmIntervalWorkbench(fg);
                        break;
                    case PhaseFormType.Reset:
                        //frm = new frmFilegramProperties(fg);
                        break;
                    default:
                        break;
                }
                fg.CurrentForm = frm;
                (Globals.MainInstance as frmMain).AddDocument(frm);
                //frm.MdiParent = Globals.MainInstance;
                //frm.Text = "Test";
                //frm.Show();
            }
            catch(Exception exc)
            {
                Logs.LogException(exc);

                //Ask if user would like a reset, if its Mark, Interval or Phase
                switch(type)
                {
                    case PhaseFormType.Mark:
                    case PhaseFormType.Interval:
                    case PhaseFormType.Phase:

                        string msg = "Error opening phase.\n\n" +
                            "Would like to reset the phase output and settings?\n\n"
                            + "This should make the phase able to be opened but will reset all data";
                        DialogResult res = MessageBox.Show(Globals.MainInstance, msg, "Error Opening", MessageBoxButtons.YesNo);

                        if(res == System.Windows.Forms.DialogResult.Yes)
                        {
                            string phasename = type.ToString();
                            fg.Phases[phasename].Clear();
                        }

                        break;
                    default:
                        break;
                }
            }
        }

        void frmAssay_Paint(object sender, PaintEventArgs e)
        {

            lstFilegrams.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            //lstFilegrams.Font = new Font(FontFamily.GenericSansSerif, 12.0f);
            //lstFilegrams.BeginUpdate();
            //lstFilegrams.Items.Clear();

            //foreach (Filegram f in Globals.FilegramList)
            //{
            //    lstFilegrams.Items.Add(f.ToListViewItem());
            //}

            //lstFilegrams.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            //lstFilegrams.EndUpdate();

            //Logs.LogTrace("Assay Form Repainted");
        }

        void frmAssay_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(PQueue != null)
            {
                int queue_count = PQueue.Count;
                bool processing = PQueue.Status == ProcessingQueueStatus.Running;

                //Commented out 3/7/2015
                //if (!Globals.PreprocessQueue.IsEmpty || Globals.PreprocessQueue.RunningTasks > 0)
                //{
                //    string msg = "Preprocessing Queue is not empty.\nYou must cancel all queued and running tasks before you can exit.\n"
                //        + "-Processing: " + Globals.PreprocessQueue.RunningTasks + "\n"
                //        + "-Queued:     " + Globals.PreprocessQueue.Count;

                //    Globals.MainInstance.ShowMessage(msg, "Files Processing", MessageBoxButtons.OK);

                //    e.Cancel = true;
                //}

                if(!PQueue.IsEmpty || PQueue.Tasks > 0)
                {
                    string msg = "Preprocessing Queue is not empty.\nYou must cancel all queued and running tasks before you can exit.\n"
                        + "-Processing: " + PQueue.Tasks + "\n"
                        + "-Queued:     " + PQueue.Count;

                    //Globals.MainInstance.ShowMessage(msg, "Files Processing", MessageBoxButtons.OK);

                    e.Cancel = true;
                }

                Globals.Save();
            }
        }

        void frmAssay_Load(object sender, EventArgs e)
        {
            if (Globals.CurrentDirectory == null || !Directory.Exists(Globals.CurrentDirectory))
            {
                MessageBox.Show("Invalid Working Directory");
                this.Close();
            }

            AddColumns();
            //LoadFiles();
            //throw new NotImplementedException();
        }

        protected virtual void OnSelectedFilegramChanged(EventArgs e)
        {
            if(SelectedFilegramChanged != null)
                SelectedFilegramChanged(this, e);
        }

        private void AddColumns()
        {
            lstFilegrams.GridLines = true;
            lstFilegrams.TabIndex = 0;
            lstFilegrams.UseCompatibleStateImageBehavior = false;
            lstFilegrams.View = System.Windows.Forms.View.Details;


            lstFilegrams.BeginUpdate();

            lstFilegrams.Columns.Add("Filename");
            lstFilegrams.Columns.Add("Status");
            lstFilegrams.Columns.Add("Camera");
            lstFilegrams.Columns.Add("Mark", 20);
            lstFilegrams.Columns.Add("Process", 20);
            lstFilegrams.Columns.Add("Interval", 20);
            lstFilegrams.Columns.Add("Phase", 20);

            lstFilegrams.EndUpdate();
        }

        void f_StatusChanged(object sender, EventArgs e)
        {
            //Logs.LogTrace((sender as Filegram).Name + " status change fired to Assay Form");

            if (InvokeRequired)
            {
                EventHandler status_change = new EventHandler(f_StatusChanged);
                Invoke(status_change, new object[] { sender, e });
                return;
            }

            Filegram fg = sender as Filegram;
            int index = Globals.FilegramList.IndexOf(fg);

            ListViewItem lvi = lstFilegrams.Items[index];

            if (fg.Status == FilegramStatus.Processing)
            {
                lvi.SubItems[1].Text = fg.GetProgress() + "%";
            }
            else
            {
                lvi.SubItems[1].Text = fg.Status.ToString();
            }

            if (fg.Video.Camera != null)
            {
                lvi.SubItems[2].Text = fg.Video.Camera.ToString();
            }
            else
            {
                lvi.SubItems[2].Text = "N/A";
            }

            lvi.SubItems[3].Text = fg.Phases[0].Status.ToString();
            lvi.SubItems[4].Text = fg.Phases[1].Status.ToString();
            lvi.SubItems[5].Text = fg.Phases[2].Status.ToString();
            lvi.SubItems[6].Text = fg.Phases[3].Status.ToString();

            for (int i = 3; i <= 6; i++)
            {
                switch(lvi.SubItems[i].Text)
                {
                    case "OK":
                        lvi.SubItems[i].BackColor = Color.Green;
                        break;

                    case "NA":
                        lvi.SubItems[i].BackColor = Color.Yellow;
                        break;

                    default:
                        lvi.SubItems[i].BackColor = Color.Red;
                        break;
                }
            }

            //lstFilegrams.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void lstFilegrams_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnSelectedFilegramChanged(e);
        }

        private void cmdProcessAll_Click(object sender, EventArgs e)
        {
            foreach(Filegram f in Globals.FilegramList)
            {
                BeginPhase(f, PhaseFormType.Preprocess);
            }
        }

        private void cmdCancelAll_Click(object sender, EventArgs e)
        {
            PQueue.CancelAll();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            lstFilegrams.Margin = new System.Windows.Forms.Padding(3, 25, 3, 3);
            base.OnSizeChanged(e);
        }
    }
}
