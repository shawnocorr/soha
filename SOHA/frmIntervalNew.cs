﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;

using SOHA.Library.Video;
using SOHAControls;
using SOHA.Library.Xml;
using SOHAControls.DisplayControls;
using SOHA.Library.Forms;

namespace SOHA
{
    public partial class frmIntervalNew : frmPhase
    {
        private Frame m_mmode_display_frame;
        private Video m_video;
        private bool m_collecting_mmode = false;
        private VideoFrameRange m_current_frame_range = new VideoFrameRange();
        private double Max = 0;
        private double Min = 0;
        private readonly int m_total_roi = 0;

        //private IntervalPhaseOutput m_output;
        private IntervalPhaseData m_interval_data;
        private IntervalPhaseSettings m_interval_settings;
        private IntervalPhaseOutput m_output;
        private PreprocessPhaseData m_preprocess_data;

        private int m_current_roi = 0;

        public frmIntervalNew()
        {
            InitializeComponent();
        }

        public frmIntervalNew(Filegram fgram)
            : base(fgram)
        {
            //base constructor run first

            InitializeComponent();

            OnSizeChanged(new EventArgs());

            if (!(fgram.Phases[1] is PreprocessPhaseOutput) 
                || (fgram.Phases[1] as PreprocessPhaseOutput).Status == PhaseStatus.NA 
                || (fgram.Phases[1] as PreprocessPhaseOutput).Status == PhaseStatus.Reject)
            {
                throw new PhaseInitiationException("No Mark Data Present.");
            }

            //this.ResizeEnd += new EventHandler(frmInterval_ResizeEnd);

            m_phase_type = SOHA.Library.PhaseFormType.Interval;
            this.Text = m_filegram.Name;

            this.KeyDown += new KeyEventHandler(frmInterval_KeyDown);
            //this.Resize += new EventHandler(frmInterval_Resize);
            //this.FormClosing += new FormClosingEventHandler(frmInterval_FormClosing);

            ucBrightFunctionControlDisplay1.SuspendGraph();
            ucIntensityFunctionControlDisplay1.SuspendGraph();

            //Read-Only Variables
            m_total_roi = (fgram.Phases[1] as PreprocessPhaseOutput).Data.Count;
            m_preprocess_data = (fgram.Phases[1] as PreprocessPhaseOutput).Data[m_current_roi] as PreprocessPhaseData;

            //Populate ComboBox
            for(int i = 1; i <= m_total_roi; i++)
            {
                ddROI.Items.Add(i);
            }
            ddROI.SelectedIndex = 0;

            //Set Functions
            ucBrightFunctionControlDisplay1.SetXValuesFromY(m_preprocess_data.Brightness);
            ucIntensityFunctionControlDisplay1.SetXValuesFromY(m_preprocess_data.Intensity);

            m_output = (fgram.Phases[2] as IntervalPhaseOutput);            
            m_interval_data = m_output.Data[m_current_roi] as IntervalPhaseData;
            m_interval_settings = m_output.Settings[m_current_roi] as IntervalPhaseSettings;
            m_current_frame_range = new VideoFrameRange(m_filegram.Video, Globals.Options.FramesPerRange);
            
            m_interval_settings.SettingsChanged += new EventHandler(m_interval_settings_SettingsChanged);

            ucBrightFunctionControlDisplay1.SetSettings(ref m_interval_settings);
            ucBrightFunctionControlDisplay1.Range = m_current_frame_range;

            ucIntensityFunctionControlDisplay1.SetSettings(ref m_interval_settings);
            ucIntensityFunctionControlDisplay1.Range = m_current_frame_range;

            mModeDisplay.Range = m_current_frame_range;

            //Initialize Primary Preprocess Output

            mModeDisplay.MMode = m_preprocess_data.MMode;
            mModeDisplay.List = m_interval_data.Intervals;
            mModeDisplay.MouseMove += new MouseEventHandler(mModeDisplay_MouseMove);

            m_video = m_filegram.Video;

            if(IsLocked)
            {
                ucBrightFunctionControlDisplay1.Enabled = false;
                ucIntensityFunctionControlDisplay1.Enabled = false;
                mModeDisplay.Enabled = false;
                mModeThumbnail.Enabled = false;
            }
            else
            {

                try
                {
                    m_video.Open();
                    m_mmode_display_frame = m_video.GetFrameSync(1);
                    m_video_present = true;
                    //m_video.Close();
                }
                catch(VideoFileNotFoundException vnfe)
                {
                    //m_video_present = false;
                    //scrollDarknessHigh.Enabled = false;
                    //scrollDarknessLow.Enabled = false;
                    //scrollIntensity.Enabled = false;
                    //cmdAccept.Enabled = false;
                    //cmdDiscard.Enabled = false;
                    //cmdReset.Enabled = false;
                    //mModeDisplay.Enabled = false;
                    //mModeThumbnail.Enabled = false;
                    //chkInvert.Enabled = false;
                    //chkUseDarkness.Enabled = false;
                    //bpFilterBright.Enabled = false;
                    //bpFilterIntensity.Enabled = false;
                }

                m_video.MModeComplete += new MModeEventHandler(m_video_MModeComplete);

                if(m_video_present)
                {
                    mModeThumbnail.XCoord = m_preprocess_data.MMode.XCoordinate;
                    mModeThumbnail.Thumbnail = m_mmode_display_frame.ToBitmap();
                    mModeThumbnail.XCoordChanged += new MouseEventHandler(mModeThumbnail_XCoordChanged);
                }
            }
            //ucBrightFunctionControlDisplay1.VideoFrameRangeChanged += new EventHandler(IntervalsChanged);
            //ucIntensityFunctionControlDisplay1.VideoFrameRangeChanged += new EventHandler(IntervalsChanged);
            //ucBrightFunctionControlDisplay1.ThresholdsChanged += new EventHandler(RecalculateIntervals);
            //ucIntensityFunctionControlDisplay1.ThresholdsChanged += new EventHandler(RecalculateIntervals);
            //ucIntensityFunctionControlDisplay1.MouseMove += new MouseEventHandler(ucIntensityFunctionControl1_MouseMove);
            //ucBrightFunctionControlDisplay1.MouseMove += new MouseEventHandler(ucBrightFunctionControlDisplay1_MouseMove);

            #region Resize MModeDisplay

            Padding new_margin = mModeDisplay.Margin;
            new_margin.Left = (int)ucBrightFunctionControlDisplay1.PlotRectangle.Left + ucBrightFunctionControlDisplay1.Left;
            mModeDisplay.Margin = new_margin;

            mModeDisplay.Width = (int)ucBrightFunctionControlDisplay1.PlotRectangle.Width;

            #endregion

            ucBrightFunctionControlDisplay1.ResumeGraph();
            ucBrightFunctionControlDisplay1.DrawGraph();

            ucIntensityFunctionControlDisplay1.ResumeGraph();
            ucIntensityFunctionControlDisplay1.DrawGraph();

            if (m_interval_data.Intervals == null || m_interval_data.Intervals.Count == 0) RecalculateIntervals(this, new EventArgs());

            OnPhaseBegin(this, new PhaseCompleteEventArgs(true, m_phase_type, m_output));
            OnSizeChanged(new EventArgs());
        }

        void m_interval_settings_SettingsChanged(object sender, EventArgs e)
        {
            RecalculateIntervals(sender, e);
        }

        void frmInterval_ResizeEnd(object sender, EventArgs e)
        {
            this.Invalidate();

            //Padding new_margin = mModeDisplay.Margin;
            //new_margin.Left = (int)ucBrightFunctionControlDisplay1.PlotRectangle.Left;
            //mModeDisplay.Margin = new_margin;

            //mModeDisplay.Width = (int)ucBrightFunctionControlDisplay1.PlotRectangle.Width;
        }

        void mModeDisplay_MouseMove(object sender, MouseEventArgs e)
        {
            //lblCoordinates.Text = "X: " + (mModeDisplay.GetXValue(e.X) + (m_current_frame_interval.Interval - 1) * 1000) + "; Y:" + (mModeDisplay.Height - e.Y);
        }

        void ucIntensityFunctionControl1_MouseMove(object sender, MouseEventArgs e)
        {
            double[] point = { 0, 0 }; //ucIntensityFunctionControl1.GetValues(e.X, m_current_frame_interval.Interval, m_current_frame_interval.IntervalSize);
            lblCoordinates.Text = "Frame: " + point[0] + "; Value: " + point[1];
        }
        
        void ucBrightFunctionControlDisplay1_MouseMove(object sender, MouseEventArgs e)
        {
            double[] point = { 0, 0 };// ucBrightFunctionControlDisplay1.GetValues(e.X, m_current_frame_interval.Interval, m_current_frame_interval.IntervalSize);
            lblCoordinates.Text = "Frame: " + point[0] + "; Value: " + point[1];
        }

        void cmbROI_SelectedIndexChanged(object sender, EventArgs e)
        {
            //NumeralStats ROI1_NormalizationFactors = ucBrightFunctionControlDisplay1.m_stats;

            //If the "New" ROI is the same as the old, do nothing and exit
            //if (m_current_roi == cmbROI.SelectedIndex)
            //    return;

            #region Save Current ROI Interval Data and Settings

            if(m_output.Data.ElementAt(m_current_roi) == null)
                m_output.Data.Insert(m_current_roi,new IntervalPhaseData());
                
            (m_output.Data[m_current_roi] as IntervalPhaseData).Intervals = mModeDisplay.List;


            //m_interval_settings.BrightThresholdHi = (int)ucBrightFunctionControlDisplay1.HiThreshold;
            //m_interval_settings.BrightThresholdLo = (int)ucBrightFunctionControlDisplay1.LoThreshold;
            //m_interval_settings.Invert = ucBrightFunctionControlDisplay1.Invert;
            //m_interval_settings.UseBright = ucBrightFunctionControlDisplay1.UseDarkness;
            //m_interval_settings.BrightFilter = ucBrightFunctionControlDisplay1.Filter;
            //m_interval_settings.BrightLoCutoff = (int)ucBrightFunctionControlDisplay1.LoCutoff;
            //m_interval_settings.BrightHiCutoff = (int)ucBrightFunctionControlDisplay1.HiCutoff;

            //m_interval_settings.IntensityThreshold = (int)ucIntensityFunctionControl1.Threshold;
            //m_interval_settings.IntensityMinInterval = ucIntensityFunctionControl1.MinIntervals;
            //m_interval_settings.IntensityFilter = ucIntensityFunctionControl1.Filter;
            //m_interval_settings.IntensityLoCutoff = ucIntensityFunctionControl1.LoCutoff;
            //m_interval_settings.IntensityHiCutoff = ucIntensityFunctionControl1.HiCutoff;
            
            
            m_output.Settings[m_current_roi] = m_interval_settings;

            #endregion

            //m_current_roi = cmbROI.SelectedIndex;

            #region Load New ROI Interval Data and Settings

            //m_interval_data = m_interval_output.Data[m_current_roi] as IntervalPhaseData;
            //m_interval_settings = m_interval_output.Settings[m_current_roi] as IntervalPhaseSettings;
            //m_preprocess_data = (m_filegram.Phases[1] as PreprocessPhaseOutput).Data[m_current_roi] as PreprocessPhaseData;

            //mModeThumbnail.XCoord = m_preprocess_data.MMode.XCoordinate;

            ////Set Defaults
            //ucBrightFunctionControlDisplay1.HiThreshold = m_interval_settings.BrightThresholdHi;
            //scrollDarknessHigh.Value = m_interval_settings.BrightThresholdHi;
            //ucBrightFunctionControlDisplay1.LoThreshold = m_interval_settings.BrightThresholdLo;
            //scrollDarknessLow.Value = m_interval_settings.BrightThresholdLo;
            //chkInvert.Checked = ucBrightFunctionControlDisplay1.Invert = m_interval_settings.Invert;
            //chkUseDarkness.Checked = ucBrightFunctionControlDisplay1.UseDarkness = m_interval_settings.UseBright;
            //bpFilterBright.Filter = ucBrightFunctionControlDisplay1.Filter = m_interval_settings.BrightFilter;
            ////bpFilterBright.LoCutoff = ucBrightFunctionControlDisplay1.LoCutoff = m_interval_settings.BrightLoCutoff;
            ////bpFilterBright.HiCutoff = ucBrightFunctionControlDisplay1.HiCutoff = m_interval_settings.BrightHiCutoff;

            //ucIntensityFunctionControl1.Threshold = m_interval_settings.IntensityThreshold;
            //scrollIntensity.Value = m_interval_settings.IntensityThreshold;
            //numMinInterval.Value = ucIntensityFunctionControl1.MinIntervals = m_interval_settings.IntensityMinInterval;
            //bpFilterIntensity.Filter = ucIntensityFunctionControl1.Filter = m_interval_settings.IntensityFilter;
            ////bpFilterIntensity.LoCutoff = ucIntensityFunctionControl1.LoCutoff = m_interval_settings.IntensityLoCutoff;
            ////bpFilterIntensity.HiCutoff = ucIntensityFunctionControl1.HiCutoff = m_interval_settings.IntensityHiCutoff;

            //ucBrightFunctionControlDisplay1.SetXValuesFromY(m_preprocess_data.Brightness);
            //ucIntensityFunctionControl1.SetXValuesFromY(m_preprocess_data.Intensity);
            //mModeDisplay.MMode = m_preprocess_data.MMode;

            ////ucBrightFunctionControlDisplay1.m_stats = ROI1_NormalizationFactors;
            ////ucBrightFunctionControlDisplay1.m_stats.Set = true;

            //ucBrightFunctionControlDisplay1.Invalidate();
            //ucIntensityFunctionControl1.Invalidate();
            //mModeDisplay.Invalidate();

            #endregion
        }

        void ScrollMouseEnter(object sender, EventArgs e)
        {
            (sender as ScrollBar).Focus();
        }

        void IntervalsChanged(object sender, EventArgs e)
        {
        }

        void RecalculateIntervals(object sender, EventArgs e)
        {
            OnSizeChanged(new EventArgs());
            //combine settings objects

            int thresh = ucIntensityFunctionControlDisplay1.GetSettings().IntensityMinInterval;

            List<Interval> intensity_intervals = ucIntensityFunctionControlDisplay1.GetIntervals();
            List<Interval> bright_intervals = ucBrightFunctionControlDisplay1.GetIntervals();

            if(intensity_intervals == null || bright_intervals == null)
                return;

            //mModeDisplay.List = new IntervalCollection(bright_intervals);
            //return;

            IntervalCollection l_list = new IntervalCollection(intensity_intervals);
            double[] l_darkness = ucBrightFunctionControlDisplay1.YValues.ToArray();// new double[ucBrightFunctionControlDisplay1.YValues.Count];
            double max = l_darkness.Max();
            double min = l_darkness.Min();
            double spread = max - min;
            double bright_lo = m_interval_settings.BrightThresholdLo / 100.0 * spread + min;
            double bright_hi = m_interval_settings.BrightThresholdHi / 100.0 * spread + min;
            //ucBrightFunctionControlDisplay1.Data.CopyTo(l_darkness);
            //List<double> tempBright = l_darkness.ToList();
            ////bpFilterControl1.RunFilter(ref tempBright);
            //ucBrightFunctionControlDisplay1.PreFilter(ref tempBright);
            //tempBright.CopyTo(l_darkness);

            double[] l_intensity = ucIntensityFunctionControlDisplay1.YValues.ToArray();//new double[ucIntensityFunctionControl1.YValues.Count];
            max = l_darkness.Max();
            min = l_darkness.Min();
            spread = max - min;
            double intensity = m_interval_settings.IntensityThreshold / 100.0 * spread + min;
            //ucIntensityFunctionControl1.Data.CopyTo(l_intensity);
            //List<double> tempIntensity = l_intensity.ToList();
            //ucIntensityFunctionControl1.PreFilter(ref tempIntensity);
            //tempIntensity.CopyTo(l_intensity);

            #region If use darkness...

            if(m_interval_settings.UseBright) //(ucBrightFunctionControlDisplay1.UseDarkness)
            {
                /*  Remove all diastoles whose mean brightness value (MBV) is greater than the low threshold
                 * 
                 * 
                 */

                for (int i = 0; i < (l_list.Count - 1); i++)
                {
                    if (l_list[i].IntervalType == IntervalType.Diastole)
                    {
                        double MBV = 0; int ct = 0;
                        for (int j = l_list[i].Frame; j <= l_list[i + 1].Frame; j++)
                        {
                            ct++;
                            MBV += l_darkness[j];// (1 - l_darkness[j]);
                        }
                        MBV /= ct;

                        if(MBV > bright_lo)
                        {
                            l_list.RemoveAt(i);
                            l_list.RemoveAt(i);
                            i--;
                        }
                    }
                }

                /*  Remove all systoles whose mean brightness value (MBV) is less than the high threshold
                 * 
                 * 
                 */

                for (int i = 0; i < (l_list.Count - 1); i++)
                {
                    if (l_list[i].IntervalType == IntervalType.Systole)
                    {
                        double MBV = 0; int ct = 0;
                        for (int j = l_list[i].Frame; j <= l_list[i + 1].Frame; j++)
                        {
                            ct++;
                            MBV += l_darkness[j];// (1 - l_darkness[j]);
                        }
                        MBV /= ct;

                        if(MBV < bright_hi)
                        {
                            l_list.RemoveAt(i);
                            l_list.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
            #endregion
            else
            {
                //Remove all intervals with length less than minimum interval
                for (int j = 0; j < l_list.Count - 1; j++)
                {
                    if (l_list[j + 1].Frame - l_list[j].Frame < thresh)
                    {
                        if (l_list[j + 1].IntervalType == IntervalType.Systole &&
                            l_list[j].IntervalType == IntervalType.Diastole)
                        {
                            //Remove both frames involved in the interval//
                            l_list.RemoveAt(j);
                            l_list.RemoveAt(j);
                            j--;
                        }
                    }
                }
            }

            int min_interval_length = (int)(0.05 * (double)(m_video.Fps));
            for (int j = 0; j < l_list.Count - 1; j++)
            {
                if (l_list[j + 1].Frame - l_list[j].Frame < min_interval_length)
                {
                    //Remove both frames involved in the interval//
                    l_list.RemoveAt(j);
                    l_list.RemoveAt(j);
                    j--;
                }
            }

            mModeDisplay.List = l_list;
            //(m_interval_data[m_cur_roi] as IntervalPhaseData).Intervals = l_list;

            #region Append Phase Intervals
            /* Taken out 2/13/2015. Not sure why it was here to begin with
             * 
            List<Interval> changes = new List<Interval>();


            IntervalCollection m_intervallist = l_list;
            //List<double> data = ImageProcessing.XExtrapolate(InitialData, Zoom);
            //PreFilter(ref data);
            //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, Zoom);

            bool m_use_threshold = false;
            int m_threshold = ucIntensityFunctionControl1.Threshold;

            if(!m_use_threshold && m_intervallist.Count > 1)
            {
                //List<double> data = InitialData;
                //PreFilter(ref data);
                //IntervalCollection tmp_intervallist = IntervalCollection.XExtrapolateInverse(m_intervallist, this.Zoom);
                //List<double> temp_data = ImageProcessing.XExtrapolateInverse(data, this.Zoom);
                int start = m_intervallist.IndexOf(m_intervallist.First(x => x.IntervalType == IntervalType.Systole));

                // Start at 1; skip first interval; ensure accuracy 
                for(int i = start; i < m_intervallist.Count; i++)
                {
                    if(m_intervallist[i].IntervalType == IntervalType.Systole)
                    {
                        double lo = 1.0f; double hi = 0.0f; int index_lo = 0; int index_hi = 0;
                        for(int j = m_intervallist[i].Frame + (int)numericUpDown1.Value; j <= m_intervallist[i + 1].Frame - (int)numericUpDown1.Value; j++)
                        {

                            //Find Minimum
                            if(l_intensity[j] < lo)
                            {
                                lo = l_intensity[j];
                                index_lo = j + 1;
                            }
                        }

                        for(int j = m_intervallist[i].Frame; j <= m_intervallist[i + 1].Frame; j++)
                        {
                            //Find First Maximum
                            if(l_intensity[j] > hi)
                            {
                                hi = l_intensity[j];
                                index_hi = j;
                            }
                        }

                        if(index_lo != 0)
                        {
                            changes.Add(new Interval(index_lo / 1, IntervalType.Change));
                            //changes.Add(new Interval(index_hi / 1, IntervalType.MaxVelocity));
                        }
                    }
                }

                //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, this.Zoom);
            }
            else
            {
                //List<double> data = InitialData;
                //PreFilter(ref data);
                //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, Zoom);
                double threshold = (100 - m_threshold) / 100.0;
                int offset = (int)numericUpDown1.Value;

                int total_beats = (from interval in m_intervallist
                                   where interval.IntervalType == IntervalType.Systole
                                   select interval).Count();

                IEnumerable<Interval> systoles;
                if(total_beats >= 5)
                {
                    systoles = (from interval in m_intervallist
                                where interval.IntervalType == IntervalType.Systole
                                select interval).Take(5);
                }
                else
                {
                    systoles = (from interval in m_intervallist
                                where interval.IntervalType == IntervalType.Systole
                                select interval);
                }

                foreach(Interval systole in systoles)
                {
                    Interval diastole = (from dias in m_intervallist
                                         where dias.IntervalType == IntervalType.Diastole && dias.Frame > systole.Frame
                                         select dias).First();

                    int max_index = systole.Frame + offset;

                    //if (max_index > data.Count)
                    //    max_index = data.Count - 1;

                    double max = l_intensity[max_index];
                    for(int i = systole.Frame + offset; i <= diastole.Frame - offset; i++)
                    {
                        double left = l_intensity[i - 1];
                        double right = l_intensity[i];
                        if((left < threshold && right > threshold) ||
                            (left > threshold && right < threshold))
                        {
                            //Interval new_interval = new SOHA.Library.Interval(
                            changes.Add(new Interval(i / 1, IntervalType.Change));
                        }

                        if(right > max)
                        {
                            max_index = i;
                            max = l_intensity[max_index];
                        }
                    }
                    changes.Add(new Interval(max_index / 1, IntervalType.MaxVelocity));
                }

                //changes = IntervalCollection.XExtrapolate(new IntervalCollection(changes), Zoom).ToList();
            }

            l_list.Union(new IntervalCollection(changes));
            l_list.Sort();
            */
            #endregion

            m_interval_data.Intervals = l_list;
            mModeDisplay.List = l_list;
        }

        void mModeThumbnail_XCoordChanged(object sender, MouseEventArgs e)
        {
            if (m_collecting_mmode) return;

            m_collecting_mmode = true;

            int new_x_coord = (sender as MModeThumbnail).XCoord;

            m_video.GetMMode(new_x_coord, MModeType.Full);
        }

        void m_video_MModeComplete(object sender, MMode mmode)
        {
            m_collecting_mmode = false;

            if (mmode == null) return;

            (m_filegram.Phases[1].Data[0] as PreprocessPhaseData).MMode = mmode;
            mModeDisplay.MMode = mmode;
        }

        void frmInterval_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_video.MModeComplete -= m_video_MModeComplete;
        }

        void frmInterval_Resize(object sender, EventArgs e)
        {
            this.Invalidate(true);

            //Padding new_margin = mModeDisplay.Margin;
            //new_margin.Left = (int)ucBrightFunctionControlDisplay1.PlotRectangle.Left;
            //mModeDisplay.Margin = new_margin;

            //mModeDisplay.Width = (int)ucBrightFunctionControlDisplay1.PlotRectangle.Width;
        }

        void frmInterval_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.A:
                case Keys.Left:
                    m_current_frame_range.PreviousRange();
                    break;

                case Keys.D:
                case Keys.Right:
                    m_current_frame_range.NextRange();
                    break;
                default:
                    break;
            }
        }

        protected override void OnComplete(object sender, PhaseCompleteEventArgs args)
        {

            args.Type = PhaseFormType.Interval;
            args.Output = m_output;

            //if(args.Accept)
            //{
            //    //Query User about resetting all next phases in phaseoutputlist
            //    if(m_filegram.Phases.CheckPostPhases(m_interval_output))
            //    {
            //        string query = "Data (Phases) that depend on the data (phase) you are changing have already been completed.\n\nWould you like to reset these phases as well?";

            //        switch(MessageBox.Show(this, query, "Data Modified", MessageBoxButtons.YesNoCancel))
            //        {
            //            case System.Windows.Forms.DialogResult.Yes:
            //                m_filegram.Phases.Clear(m_interval_output);
            //                break;
            //            case System.Windows.Forms.DialogResult.No:
            //                break;
            //            case System.Windows.Forms.DialogResult.Cancel:
            //                return;
            //        }
            //    }

            //    if(m_interval_output.Data.ElementAt(m_current_roi) == null)
            //        m_interval_output.Data.Insert(m_current_roi, new IntervalPhaseData());
                
            //    //Check for alternating Intervals
            //    if(mModeDisplay.List != null && !mModeDisplay.List.IsAlternating)
            //    {
            //        this.WindowState = FormWindowState.Minimized;
            //        string text = "The accepted Interval Collection [" + mModeDisplay.List.Count + " elements] does not appear to alternate systole/diastole."
            //            + "\n\nWould you like to continue?";
            //        DialogResult result = MessageBox.Show(Globals.MainInstance, text, "Possible Error Detected", MessageBoxButtons.OKCancel);
            //        if(result == DialogResult.Cancel)
            //        {
            //            this.WindowState = FormWindowState.Maximized;
            //            return;
            //        }
            //    }

            //    (m_interval_output.Data[m_current_roi] as IntervalPhaseData).Intervals = mModeDisplay.List;
            //    IntervalPhaseSettings interval_settings = new IntervalPhaseSettings();
            //    mModeDisplay.List.Run(m_video.FrameTimes);
                                
            //    interval_settings = ucBrightFunctionControlDisplay1.GetSettings();
            //    //IntervalPhaseSettings intensity_settings = ucIntensityFunctionControlDisplay1.GetSettings();

            //    //interval_settings.BrightThresholdHi = ucBrightFunctionControlDisplay1.HiThreshold;
            //    //interval_settings.BrightThresholdLo = ucBrightFunctionControlDisplay1.LoThreshold;
            //    //interval_settings.Invert = ucBrightFunctionControlDisplay1.Invert;
            //    //interval_settings.UseBright = ucBrightFunctionControlDisplay1.UseDarkness;
            //    //interval_settings.BrightLoCutoff = ucBrightFunctionControlDisplay1.LoCutoff;
            //    //interval_settings.BrightHiCutoff = ucBrightFunctionControlDisplay1.HiCutoff;
            //    //interval_settings.ShowPhase = mModeDisplay.ShowChangeIntervals;
            //    //interval_settings.PhaseOffset = mModeDisplay.PhaseOffset;

            //    //interval_settings.IntensityThreshold = ucIntensityFunctionControl1.Threshold;
            //    //interval_settings.IntensityMinInterval = ucIntensityFunctionControl1.MinIntervals;
            //    //interval_settings.IntensityLoCutoff = ucIntensityFunctionControl1.LoCutoff;
            //    //interval_settings.IntensityHiCutoff = ucIntensityFunctionControl1.HiCutoff;

            //    /* Repetitive Function w/SelectedIndexChanged of ROI ComboBox*/

            //    //m_interval_settings.BrightThresholdHi = (int)ucBrightFunctionControlDisplay1.HiThreshold;
            //    //m_interval_settings.BrightThresholdLo = (int)ucBrightFunctionControlDisplay1.LoThreshold;
            //    //m_interval_settings.Invert = ucBrightFunctionControlDisplay1.Invert;
            //    //m_interval_settings.UseBright = ucBrightFunctionControlDisplay1.UseDarkness;
            //    //m_interval_settings.BrightFilter = ucBrightFunctionControlDisplay1.Filter;
            //    //m_interval_settings.BrightLoCutoff = (int)ucBrightFunctionControlDisplay1.LoCutoff;
            //    //m_interval_settings.BrightHiCutoff = (int)ucBrightFunctionControlDisplay1.HiCutoff;

            //    //m_interval_settings.IntensityThreshold = (int)ucIntensityFunctionControl1.Threshold;
            //    //m_interval_settings.IntensityMinInterval = ucIntensityFunctionControl1.MinIntervals;
            //    //m_interval_settings.IntensityFilter = ucIntensityFunctionControl1.Filter;
            //    //m_interval_settings.IntensityLoCutoff = ucIntensityFunctionControl1.LoCutoff;
            //    //m_interval_settings.IntensityHiCutoff = ucIntensityFunctionControl1.HiCutoff;

            //    m_interval_output.Settings[m_current_roi] = interval_settings;//interval_settings;

            //    IntervalPhaseOutput output = new IntervalPhaseOutput();
            //    IntervalPhaseData data = new IntervalPhaseData();

            //    //IntervalCollection.CurveFitIntervals(mModeDisplay.List, ucIntensityFunctionControl1.LastData);

            //    Globals.SaveMMode(mModeDisplay.MMode.ToBitmap(), m_filegram.Name + "_Full.bmp");
            //}

            //m_filegram.Phases[2].Status = args.Accept ? PhaseStatus.OK : PhaseStatus.Reject;

            base.OnComplete(sender, args);
        }

        public static frmIntervalNew Spawn(Filegram fgram)
        {
            return new frmIntervalNew(fgram);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            RecalculateIntervals(sender, e);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            RecalculateIntervals(sender, e);
        }

        private void cmdAccept_Click(object sender, EventArgs e)
        {

        }

        protected override void OnSizeChanged(EventArgs e)
        {
            double lt = ucBrightFunctionControlDisplay1.PlotRectangle.Left + 10;
            double rt = ucBrightFunctionControlDisplay1.DisplayRectangle.Right - ucBrightFunctionControlDisplay1.PlotRectangle.Right - ucBrightFunctionControlDisplay1.PlotRectangle.Left;
            int control_width = ucBrightFunctionControlDisplay1.Width;
            int left = (int)(0.045 * control_width);
            int right = (int)(0.025 * control_width);
            mModeDisplay.Margin = new Padding((int)lt, 3, right, 3);
            base.OnSizeChanged(e);
        }

        private void ddROI_SelectedIndexChanged(object sender, EventArgs e)
        {
            int roi_selected = (sender as ToolStripComboBox).SelectedIndex + 1;

            if(roi_selected - 1 == m_current_roi)
                return;

            m_current_roi = roi_selected - 1;
            m_preprocess_data = (m_filegram.Phases[1] as PreprocessPhaseOutput).Data[m_current_roi] as PreprocessPhaseData;
            
            ucBrightFunctionControlDisplay1.SuspendGraph();
            ucIntensityFunctionControlDisplay1.SuspendGraph();

            //Set Functions
            ucBrightFunctionControlDisplay1.SetXValuesFromY(m_preprocess_data.Brightness);
            ucIntensityFunctionControlDisplay1.SetXValuesFromY(m_preprocess_data.Intensity);

            m_output = (m_filegram.Phases[2] as IntervalPhaseOutput);

            if(m_output.Data.Count < m_total_roi)
            {
                m_output.Data.Add(new IntervalPhaseData());
                m_output.Settings.Add(new IntervalPhaseSettings());
            }

            m_interval_data = m_output.Data[m_current_roi] as IntervalPhaseData;
            m_interval_settings = m_output.Settings[m_current_roi] as IntervalPhaseSettings;
            m_current_frame_range = new VideoFrameRange(m_filegram.Video, Globals.Options.FramesPerRange);

            m_interval_settings.SettingsChanged += new EventHandler(m_interval_settings_SettingsChanged);

            ucBrightFunctionControlDisplay1.SetSettings(ref m_interval_settings);
            ucBrightFunctionControlDisplay1.Range = m_current_frame_range;

            ucIntensityFunctionControlDisplay1.SetSettings(ref m_interval_settings);
            ucIntensityFunctionControlDisplay1.Range = m_current_frame_range;

            mModeDisplay.Range = m_current_frame_range;

            //Initialize Primary Preprocess Output

            mModeDisplay.MMode = m_preprocess_data.MMode;
            mModeDisplay.List = m_interval_data.Intervals;
            mModeDisplay.MouseMove += new MouseEventHandler(mModeDisplay_MouseMove);

            m_video = m_filegram.Video;

            if(IsLocked)
            {
                ucBrightFunctionControlDisplay1.Enabled = false;
                ucIntensityFunctionControlDisplay1.Enabled = false;
                mModeDisplay.Enabled = false;
                mModeThumbnail.Enabled = false;
            }
            else
            {

                try
                {
                    m_video.Open();
                    m_mmode_display_frame = m_video.GetFrameSync(1);
                    m_video_present = true;
                    //m_video.Close();
                }
                catch(VideoFileNotFoundException vnfe)
                {
                    //m_video_present = false;
                    //scrollDarknessHigh.Enabled = false;
                    //scrollDarknessLow.Enabled = false;
                    //scrollIntensity.Enabled = false;
                    //cmdAccept.Enabled = false;
                    //cmdDiscard.Enabled = false;
                    //cmdReset.Enabled = false;
                    //mModeDisplay.Enabled = false;
                    //mModeThumbnail.Enabled = false;
                    //chkInvert.Enabled = false;
                    //chkUseDarkness.Enabled = false;
                    //bpFilterBright.Enabled = false;
                    //bpFilterIntensity.Enabled = false;
                }

                m_video.MModeComplete += new MModeEventHandler(m_video_MModeComplete);

                if(m_video_present)
                {
                    mModeThumbnail.XCoord = m_preprocess_data.MMode.XCoordinate;
                    mModeThumbnail.Thumbnail = m_mmode_display_frame.ToBitmap();
                    mModeThumbnail.XCoordChanged += new MouseEventHandler(mModeThumbnail_XCoordChanged);
                }
            }
            //ucBrightFunctionControlDisplay1.VideoFrameRangeChanged += new EventHandler(IntervalsChanged);
            //ucIntensityFunctionControlDisplay1.VideoFrameRangeChanged += new EventHandler(IntervalsChanged);
            //ucBrightFunctionControlDisplay1.ThresholdsChanged += new EventHandler(RecalculateIntervals);
            //ucIntensityFunctionControlDisplay1.ThresholdsChanged += new EventHandler(RecalculateIntervals);
            //ucIntensityFunctionControlDisplay1.MouseMove += new MouseEventHandler(ucIntensityFunctionControl1_MouseMove);
            //ucBrightFunctionControlDisplay1.MouseMove += new MouseEventHandler(ucBrightFunctionControlDisplay1_MouseMove);

            #region Resize MModeDisplay

            Padding new_margin = mModeDisplay.Margin;
            new_margin.Left = (int)ucBrightFunctionControlDisplay1.PlotRectangle.Left + ucBrightFunctionControlDisplay1.Left;
            mModeDisplay.Margin = new_margin;

            mModeDisplay.Width = (int)ucBrightFunctionControlDisplay1.PlotRectangle.Width;

            #endregion

            ucBrightFunctionControlDisplay1.ResumeGraph();
            ucBrightFunctionControlDisplay1.DrawGraph();

            ucIntensityFunctionControlDisplay1.ResumeGraph();
            ucIntensityFunctionControlDisplay1.DrawGraph();

            if(m_interval_data.Intervals == null || m_interval_data.Intervals.Count == 0) RecalculateIntervals(this, new EventArgs());

            OnPhaseBegin(this, new PhaseCompleteEventArgs(true, m_phase_type, m_output));
            OnSizeChanged(new EventArgs());
        }
    }
}
