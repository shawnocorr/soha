﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using System.IO.Compression;
using System.Threading;

using System.Collections;
using SOHA.Library;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Ionic.Zip;
using SOHA.Library.Xml;
using SOHA.SecondaryForms;

namespace SOHA
{
    public partial class mdiMain : Form
    {
        private int childFormNumber = 0;
        private frmAssay m_Assay = new frmAssay();

        private static ProcessingQueue LoadQueue = new ProcessingQueue(false);

        private static frmProcessNotify m_process_progress = new frmProcessNotify();
        private static Thread t;
        private static readonly object sync = new object();
        private static int total_files = 0;
        private static int files_loaded = 0;

        private static frmMessageBox m_message_box = new frmMessageBox();
        private static bool m_cancel_process = false;

        public static mdiMain Instance
        {
            get;
            private set;
        }

        public mdiMain()
        {
            InitializeComponent();
            AddRecentAssays();

            Globals.MainInstance = this;
            mdiMain.Instance = this;

            this.FormClosing += new FormClosingEventHandler(mdiMain_FormClosing);
            //Filegram.LoadComplete += new FilegramLoadEventHandler(Filegram_LoadComplete);

            LoadQueue.ProcessComplete += new ProcessComplete(LoadQueue_ProcessComplete);
            LoadQueue.ProgressUpdate += new ProcessProgressUpdate(LoadQueue_ProgressUpdate);
        }

        void LoadQueue_ProgressUpdate(object sender, int percent)
        {
            Filegram fg = sender as Filegram;

            if (fg.Status != FilegramStatus.Error)
                Globals.FilegramList.Add(fg);
            else
                Globals.ErrorFilegrams.Add(fg.Name);
            
            m_process_progress.UpdateProgress(percent);
        }

        void LoadQueue_ProcessComplete(object sender, ProgressEventArgs e)
        {
            if (InvokeRequired)
            {
                ProcessComplete complete = new ProcessComplete(LoadQueue_ProcessComplete);
                Invoke(complete, new object[] { sender, e });
            }
            else
            {
                if (e.Cancelled)
                {
                    Globals.FilegramList.Clear();
                    m_process_progress.Close();
                    return;
                }

                FinishLoadFiles();
            }
        }

        private void AddRecentAssays()
        {
            List<string> assays = Globals.RecentAssays;

            foreach (string s in assays)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(s,null,RecentAssay_Click);
                recentAssaysToolStripMenuItem.DropDownItems.Add(item); 
            }
        }

        void RecentAssay_Click(object sender, EventArgs e)
        {
            string path = (sender as ToolStripMenuItem).Text;
            if (Directory.Exists(path))
            {
                if (m_Assay != null)
                {
                    m_Assay.Dispose();
                }

                Globals.CurrentDirectory = path;
                Globals.XmlArchiveDirectory = path + @"\XMLArchive\";
                WriteRegistry(path);
                LoadFiles();
                //m_Assay = new frmAssay();
                //m_Assay.MdiParent = this;
                //m_Assay.Show();
                //WriteRegistry(path);
            }
        }

        void mdiMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e = ShowExitVerificationForm(e);
            //Globals.WriteCamFile();

            Globals.WriteRecentAssays();
            Globals.FreeAllResources();
        }

        #region Inherited Functions

        private void ShowNewForm(object sender, EventArgs e)
        {
            //Form childForm = new Form();
            //childForm.MdiParent = this;
            //childForm.Text = "Window " + childFormNumber++;
            //childForm.Show();

            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.SelectedPath = ReadRegistry();
            folderBrowser.ShowNewFolderButton = false;
            if (folderBrowser.ShowDialog(this) == DialogResult.Cancel)
                return;

            string path = folderBrowser.SelectedPath;

            if (Directory.Exists(path))
            {
                if (m_Assay != null)
                {
                    m_Assay.Dispose();
                }

                Globals.CurrentDirectory = path;
                Globals.XmlArchiveDirectory = path + @"\XMLArchive\";
                Globals.FilegramList = new FilegramCollection(path);
                FilegramCollection.LoadComplete += new EventHandler(FilegramCollection_LoadComplete);
                //m_Assay = new frmAssay();
                //m_Assay.MdiParent = this;
                //m_Assay.Show();
                WriteRegistry(path);
                Globals.AddNewAssay(path);
                //LoadFiles();
            }
        }

        void FilegramCollection_LoadComplete(object sender, EventArgs e)
        {
            this.Invoke(((MethodInvoker)delegate { this.Assay(); }));
        }

        public void Assay()
        {
            m_Assay = new frmAssay();
            m_Assay.MdiParent = this;
            m_Assay.Show();
            m_Assay.Text = Globals.CurrentDirectory;
            m_Assay.WindowState = FormWindowState.Maximized;
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        #endregion

        #region User-Defined Functions

        private void WriteRegistry(string path)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey("OpticalHeartBeatAnalysis");
            key.SetValue("Folder", path);
        }

        private string ReadRegistry()
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey("OpticalHeartBeatAnalysis");
            return (string)key.GetValue("Folder");
        }

        #endregion

        public void LoadFiles()
        {
            DirectoryInfo directory_info = new DirectoryInfo(Globals.CurrentDirectory);
            //this.Text = Globals.CurrentDirectory;

            string[] valid_exts = new string[] { ".cxd", ".avi", ".tif"};
            ArrayList files_added = new ArrayList();

            IEnumerable<string> ohba_files = from FileInfo fi in directory_info.GetFiles("*.xml") select Path.GetFileNameWithoutExtension(fi.Name);

            #region Archive

            StringBuilder archive_message = new StringBuilder();
            archive_message.AppendLine(ohba_files.Count() + " Files Found:");

            bool archive_today_found = directory_info.GetFiles("*" + DateTime.Today.ToString("yyyyMMdd") + "*.zip").Count() > 0;

            archive_message.AppendLine("\nWould you like to archive(ZIP) these files?");

            if(ohba_files.Count() > 0 && !archive_today_found)
            {
                if(MessageBox.Show(this, archive_message.ToString(), "Archive?", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    string zip_path = Globals.CurrentDirectory + @"\Archive_" + DateTime.Today.ToString("yyyyMMdd") + ".zip";
                    DialogResult zip_continue = System.Windows.Forms.DialogResult.Yes;
                    if(File.Exists(zip_path))
                        zip_continue = MessageBox.Show(this, "You have created an Archive already today. Overwrite?", "Overwrite?", MessageBoxButtons.YesNo);

                    if(zip_continue == System.Windows.Forms.DialogResult.Yes)
                    {
                        using(ZipFile zip = new ZipFile())
                        {
                            foreach(string s in ohba_files)
                                zip.AddFile(Globals.CurrentDirectory + @"\" + s + ".xml", "");
                            zip.Save(Globals.CurrentDirectory + @"\Archive_" + DateTime.Today.ToString("yyyyMMdd") + ".zip");
                        }
                    }
                }
            }


            #endregion

            #region Convert Old Xml Files

            IEnumerable<string> xml_files = from FileInfo fi in directory_info.GetFiles("*.xml") select Path.GetFileNameWithoutExtension(fi.Name);
            IEnumerable<string> files_to_convert = xml_files.Where(x => !ohba_files.Contains(x));
            List<string> failed_conversions = new List<string>();

            List<string> files_convert = new List<string>();
            foreach (string s in files_to_convert)
            {
                string file = Globals.CurrentDirectory + "\\" + s + ".xml";
                if (File.Exists(file))
                    files_convert.Add(file);
            }

            if (files_convert.Count() > 0)
            {
                if (MessageBox.Show("XML Files found. Would you like to update these files?", "XML Files Found.", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int success = 0;
                    int fail = 0;
                    foreach (string s in files_convert)
                    {
                        try
                        {
                            //Filegram.ConvertFromXml(s);
                            success++;
                        }
                        catch (Exception e)
                        {
                            failed_conversions.Add(s);
                            fail++;
                        }
                    }

                    //Message Box Results
                    StringBuilder sbuild = new StringBuilder();
                    sbuild.AppendLine("Successful Conversions: " + success);
                    sbuild.AppendLine("Failed Conversions: " + fail);
                    foreach (string s in failed_conversions)
                        sbuild.AppendLine("\t" + s);

                    MessageBox.Show(Globals.MainInstance, sbuild.ToString(), "XML Conversion Results", MessageBoxButtons.OKCancel);
                }
            }

            #endregion

            #region Populate XML and CXD/AVI/TIF files to be loaded

            IEnumerable<string> non_ohba_files = from FileInfo fi in directory_info.GetFiles("*").Where<FileInfo>(x => x.Extension.Equals(".cxd") || x.Extension.Equals(".avi") || x.Extension.Equals(".tif")) select Path.GetFileNameWithoutExtension(fi.Name);
            IEnumerable<string> files_to_add = ohba_files.Union(non_ohba_files).Except(ohba_files.Intersect(non_ohba_files));

            List<string> files = new List<string>();
            foreach (string s in ohba_files)
                files.Add(Globals.CurrentDirectory + "\\" + s + ".xml");

            foreach (string s in files_to_add)
            {
                for (int i = 0; i < valid_exts.Length; i++)
                {
                    string file = Globals.CurrentDirectory + "\\" + s + valid_exts[i];
                    if (File.Exists(file))
                        files.Add(file);
                }
            }

            #endregion

            ////List<Filegram> fg_files = (from string s in files select new Filegram(s)).ToList();

            Globals.FilegramList.Clear();
            Globals.ErrorFilegrams.Clear();

            //ConcurrentQueue<Filegram> file_queue = new ConcurrentQueue<Filegram>();
            //var results
            
            //try
            //{
            //    foreach (string s in files)
            //        file_queue.Enqueue(new Filegram(s));

            //    //ParallelOptions options = new ParallelOptions();
            //    //options.
            //    ParallelLoopResult plr = Parallel.ForEach<Filegram>(loads, Load());
            //}
            //catch (AggregateException agg_exc)
            //{

            //}

            if(files.Count == 0)
            {
                MessageBox.Show("No Files Found.");
                return;
            }

            foreach (string s in files)
                LoadQueue.Enqueue(new Filegram(s), "Load");

            m_process_progress = new frmProcessNotify("Loading Files...", "Loading Files");
            m_process_progress.Cancel += new EventHandler(m_process_progress_Cancel);
            m_process_progress.Show();
            LoadQueue.Start();

            //frmLoadFilegrams frmLoad = new frmLoadFilegrams();
            //frmLoad.Show(this);
            //frmLoad.LoadFilegrams(files);
            //LoadFilegrams(files);
            //Globals.Save();

            //while (!frmLoad.Completed)
            //{
            //    Application.DoEvents();
            //    //Thread.Sleep(100);
            //}

            //m_process_progress = new frmProcessNotify("Loading Files...");
            //m_process_progress.TopMost = true;
            //m_process_progress.Show();

            //total_files = files.Count;
            //files_loaded = 0;
            //foreach (string file in files)
            //{
            //    while (t != null && t.ThreadState == System.Threading.ThreadState.Running) { }

            //    if (m_process_progress.IsCancelled)
            //    {
            //        m_process_progress.IsCancelled = false;
            //        Filegram_LoadComplete(null, new EventArgs());
            //        break;
            //    }

            //    t = new Thread(() => Filegram.FromFilepath(file));
            //    t.Start();
            //    //if(m_load_task != null && m_load_task.Status == TaskStatus.Running)
            //    //    m_load_task.Wait(5000);

            //    //m_load_task = new Task(() => Filegram.FromFilepath(file));
            //    //m_load_task.Start();
            //}

            //if (!frmLoad.Cancelled)
            //{

            //    foreach (Filegram f in Globals.FilegramList)
            //    {
            //        f.StatusChanged += new EventHandler(fgram_StatusChanged);
            //    }

            //    IEnumerable<Filegram> errors = from f in Globals.FilegramList where f.Status == FilegramStatus.Error select f;
            //    IEnumerable<Filegram> non_errors = from f in Globals.FilegramList where f.Status != FilegramStatus.Error select f;
            //    IEnumerable<Filegram> sorted = from f in non_errors orderby f.CultureProperties.Genotype, f.CultureProperties.ID select f;
            //    IEnumerable<Filegram> complete = sorted.Union<Filegram>(errors);
            //    Globals.FilegramList = complete.ToList<Filegram>();
            //}
        }

        void m_process_progress_Cancel(object sender, EventArgs e)
        {
            if(LoadQueue.Status == ProcessingQueueStatus.Running)
                LoadQueue.CancelAll();
        }

        public void FinishLoadFiles()
        {
            m_process_progress.End();

            if(m_process_progress.IsCancelled)
            {
                Globals.FilegramList.Clear();
                return;
            }

            bool errors_present = false;
            StringBuilder errors = new StringBuilder();
            errors.AppendLine("The following files were unable to be processed:");
            foreach (string s in Globals.ErrorFilegrams)
            {
                errors.AppendLine("\t" + s);
                errors_present = true;
            }

            if(errors_present)
                MessageBox.Show(errors.ToString());

            m_Assay = new frmAssay();
            m_Assay.MdiParent = this;
            m_Assay.Show();
            m_Assay.Text = Globals.CurrentDirectory;
            m_Assay.WindowState = FormWindowState.Maximized;
        }

        void Filegram_LoadComplete(Filegram fgram, EventArgs e)
        {//DEPRECATED
            if (fgram == null)
            {
                Globals.FilegramList.Clear();
                Logs.LogTrace("Load Cancelled by User");
                return;
            }

            if (fgram.Status == FilegramStatus.Error)
                return;

            files_loaded++;
            Globals.FilegramList.Add(fgram);
            Logs.LogTrace("Loaded " + fgram.Name);

            int percent = (int)(files_loaded * 100 / (double)total_files);

            if (InvokeRequired)
            {
                UpdateProgressCallback update = new UpdateProgressCallback(m_process_progress.UpdateProgress);
                Invoke(update, new object[] { percent });
            }

            if (percent == 100)
            {
                Invoke(new MethodInvoker(FinishLoad));
            }
            //throw new NotImplementedException();
        }

        public void ShowProcessNotify()
        {
            m_process_progress.ShowDialog(this);
        }

        public void FinishLoad()
        {
            IEnumerable<Filegram> errors = from f in Globals.FilegramList where f.Status == FilegramStatus.Error select f;
            IEnumerable<Filegram> non_errors = from f in Globals.FilegramList where f.Status != FilegramStatus.Error select f;
            IEnumerable<Filegram> sorted = from f in non_errors orderby f.CultureProperties.Genotype, f.CultureProperties.ID select f;
            IEnumerable<Filegram> complete = sorted.Union<Filegram>(errors);
            //Globals.FilegramList = complete.ToList<Filegram>();

            Globals.Save();

            m_process_progress.Close();
            m_Assay = new frmAssay();
            m_Assay.MdiParent = this;
            m_Assay.Show();
            Logs.LogTrace("Load Complete");
        }

        public DialogResult ShowMessage(string msg, string title, MessageBoxButtons buttons)
        {
            m_message_box = new frmMessageBox(msg, title, buttons);
            return m_message_box.ShowDialog(this);
        }

        public static void ShowForm(Form form)
        {
            form.ShowDialog(mdiMain.Instance);
        }

        private void camerasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCameraSelect form_cams = new frmCameraSelect();
            form_cams.ShowDialog(this);
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGlobalOptions frmOptions = new frmGlobalOptions();
            frmOptions.Show(this);
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            Globals.Save();
        }

        private void cSVFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Globals.FilegramList.WriteCsv();
            }
            catch(IOException ioexc)
            {
                MessageBox.Show("File is already opened. You must close the file and re-output.");
            }
            catch(Win32Exception win_exc)
            {
                MessageBox.Show("Could not open CSV. Please associate (default open) CSV files with a program.");
            }
            catch(SohaException soha_exc)
            {
                MessageBox.Show("There are no files present to output.");
            }

            #region Deprecated

            //if (Globals.FilegramList.Count <= 0)
            //{
            //    MessageBox.Show("There are no files present to output.");
            //    return;
            //}

            //int n = Globals.FilegramList.Count;

            //List<object[]> output_data = new List<object[]>(n + 1);
            //output_data.Add(CreateHeader());
            //for (int i = 0; i < n; i++)
            //{
            //    List<object[]> objs = Globals.FilegramList[i].ToDataRow();
            //    foreach (object[] o_array in objs)
            //        output_data.Add(o_array);
            //}

            //string file = Globals.CurrentDirectory + "\\complete_data_output.csv";

            //string DelimiterChar = ";";
            //try
            //{
            //    using(StreamWriter writer = new StreamWriter(file))
            //    {
            //        writer.WriteLine("sep=;");
            //        for(int i = 0; i < output_data.Count; i++)
            //        {
            //            for(int j = 0; j < (output_data[i] as object[]).Length; j++)
            //            {
            //                writer.Write("\"" + (output_data[i] as object[])[j] + "\"" + DelimiterChar);
            //            }
            //            writer.WriteLine();
            //        }
            //        writer.Close();
            //    }

            //    Process.Start(file);
            //}
            //catch(IOException ioexc)
            //{
            //    MessageBox.Show("File is already opened. You must close the file and re-output.");
            //}
            //catch(Win32Exception win_exc)
            //{
            //    MessageBox.Show("Could not open CSV. Please associate (default open) CSV files with a program.");
            //}

            #endregion
        }

        public FormClosingEventArgs ShowExitVerificationForm(FormClosingEventArgs e)
        {
            //if (Globals.Queue != null)
            //{
            //    int queue_count = Globals.Queue.Count;
            //    bool processing = Globals.Queue.Processing;

            //    DialogResult dialog_result = MessageBox.Show(this,
            //        "Are you sure you would like to exit?\n\n" +
            //        (processing ? "-File currently processing" : "-No files are processing") + "\n" +
            //        "-There are currently " + queue_count + " items in the queue.\n\n" +
            //        "Any current running processes WILL be damaged.\n" +
            //        "All queue items currently idle will NOT be processed.",
            //        "Verify Application Termination", MessageBoxButtons.OKCancel);

            //    if (dialog_result == DialogResult.Cancel)
            //    {
            //        e.Cancel = true;
            //    }
            //    else if (dialog_result == DialogResult.OK)
            //    {
            //        Globals.Queue.RemoveAll();
            //        Globals.Save();

            //        //e.CloseReason = CloseReason.
            //        //this.Close();
            //    }
            //}

            return e;
        }

        private void discreteIntervalsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                var genotypes = Globals.FilegramList.GroupBy(x => x.CultureProperties.Genotype.ToUpper());

                foreach (IGrouping<string, Filegram> geno_group in genotypes)
                {
                    var ages = geno_group.GroupBy(x => x.CultureProperties.Age);

                    ArrayList out_data = new ArrayList();
                    foreach (IGrouping<int, Filegram> age_group in ages)
                    {
                        List<object> systoles = new List<object>();
                        List<object> diastoles = new List<object>();
                        List<object> periods = new List<object>();

                        systoles.Add("");
                        diastoles.Add("Age: " + age_group.Key.ToString());
                        periods.Add("");

                        systoles.Add("Systoles");
                        diastoles.Add("Diastoles");
                        periods.Add("Periods");

                        foreach (Filegram f in age_group)
                        {
                            foreach (PhaseData intervaldata in f.Phases[2].Data)
                            {
                                IntervalCollection intervals = (intervaldata as IntervalPhaseData).Intervals;
                                if (intervals.Count == 0)
                                    break;

                                double[] frame_times = f.Video.FrameTimes;

                                IEnumerable<int> SystolicIntervals =
                                from Interval interval in intervals
                                where (interval.IntervalType == IntervalType.Systole)
                                select interval.Frame;

                                foreach (int a in SystolicIntervals)
                                {
                                    //Get Next Diastole
                                    var next_diastole = (from Interval interval in intervals
                                                         where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                                                         select interval.Frame);
                                    if (next_diastole.Count() == 0)
                                        break;

                                    int b = next_diastole.First();

                                    if (b > 0)
                                        systoles.Add(frame_times[b] - frame_times[a]);


                                    //Get Next Systole
                                    //The last systolic interval has no corresponding diastolic interval, so exit
                                    var next_systole = (from Interval interval in intervals
                                                        where (interval.IntervalType == IntervalType.Systole && interval.Frame > a)
                                                        select interval.Frame);
                                    if (next_systole.Count() == 0)
                                        break;

                                    int a1 = next_systole.First();

                                    if (a1 > 0)
                                    {
                                        diastoles.Add(frame_times[a1] - frame_times[b]);
                                        periods.Add(frame_times[a1] - frame_times[a]);
                                    }
                                }
                            }
                        }

                        //Find longest dataset
                        int rows = systoles.Count > diastoles.Count ? systoles.Count : diastoles.Count;

                        #region Fill 2D object matrix

                        object[,] data = new object[rows, 3];
                        for (int j = 0; j < systoles.Count; j++)
                        {
                            data[j, 2] = systoles[j];
                        }

                        for (int j = 0; j < diastoles.Count; j++)
                        {
                            data[j, 1] = diastoles[j];
                        }

                        for (int j = 0; j < periods.Count; j++)
                        {
                            data[j, 0] = periods[j];
                        }

                        out_data.Add(data);

                        #endregion
                    }

                    #region Write Age File

                    string file = Globals.CurrentDirectory + "\\interval_output_" + geno_group.Key + ".csv";
                    using (StreamWriter writer = new StreamWriter(file))
                    {
                        int r = 0;
                        bool done = false;
                        while (!done)
                        {
                            for (int i = 0; i < out_data.Count; i++)
                            {
                                for (int j = 0; j < (out_data[i] as object[,]).GetLength(1); j++)
                                {
                                    writer.Write((out_data[i] as object[,])[r, j] + ",");
                                }
                            }
                            writer.WriteLine();
                            r++;

                            done = true;
                            for (int i = 0; i < out_data.Count; i++)
                            {
                                if (r < (out_data[i] as object[,]).GetLength(0))
                                {
                                    done = false;
                                    break;
                                }
                            }
                        }
                    }

                    #endregion

                    Process.Start(file);
                }

                /*
                List<string> genos = new List<string>();
                List<Filegram> filegrams = new List<Filegram>();
                List<double> systolic_intervals = new List<double>();
                List<double> diastolic_intervals = new List<double>();
                List<double> heart_periods = new List<double>();
                List<double> heart_rate = new List<double>();

                //Fill genotypes and filegram lists
                foreach (Filegram f in Globals.FilegramList)
                {
                    if (f.Status != FilegramStatus.Error && (f.Phases[2].Data[0] is IntervalPhaseData) && (f.Phases[2].Status == PhaseStatus.OK))
                    {
                        genos.Add(f.CultureProperties.Genotype);
                        filegrams.Add(f);
                    }
                }

                //Find UNIQUE genotypes
                IEnumerable<string> genotypes = genos.Distinct();

                int n = 0;
                foreach (string s in genotypes)
                {
                    //Fill the current set of genotype filegrams
                    IEnumerable<Filegram> current_set = filegrams.Where<Filegram>(x => x.CultureProperties.Genotype.ToLower().Equals(s.ToLower()));

                    //Fill age groups of current set
                    List<int> ages = new List<int>();
                    foreach (Filegram f in current_set)
                        ages.Add(f.CultureProperties.Age);

                    //Select UNIQUE ages
                    IEnumerable<int> age_groups = ages.Distinct();

                    ArrayList out_data = new ArrayList();
                    foreach (int i in age_groups)
                    {
                        List<object> systoles = new List<object>();
                        List<object> diastoles = new List<object>();
                        List<object> periods = new List<object>();

                        systoles.Add("");
                        diastoles.Add("Age: " + i.ToString());
                        periods.Add("");

                        systoles.Add("Systoles");
                        diastoles.Add("Diastoles");
                        periods.Add("Periods");

                        IEnumerable<Filegram> current_age_set = current_set.Where<Filegram>(x => x.CultureProperties.Age == i);

                        foreach (Filegram f in current_age_set)
                        {
                            foreach (PhaseData intervaldata in f.Phases[2].Data)
                            {
                                IntervalCollection intervals = (intervaldata as IntervalPhaseData).Intervals;
                                if (intervals.Count == 0)
                                    break;

                                double[] frame_times = f.Video.FrameTimes;

                                IEnumerable<int> SystolicIntervals =
                                from Interval interval in intervals
                                where (interval.IntervalType == IntervalType.Systole)
                                select interval.Frame;

                                foreach (int a in SystolicIntervals)
                                {
                                    //Get Next Diastole
                                    var next_diastole = (from Interval interval in intervals
                                                         where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                                                         select interval.Frame);
                                    if (next_diastole.Count() == 0)
                                        break;

                                    int b = next_diastole.First();

                                    if (b > 0)
                                        systoles.Add(frame_times[b] - frame_times[a]);
                                    

                                    //Get Next Systole
                                    //The last systolic interval has no corresponding diastolic interval, so exit
                                    var next_systole = (from Interval interval in intervals
                                                        where (interval.IntervalType == IntervalType.Systole && interval.Frame > a)
                                                        select interval.Frame);
                                    if(next_systole.Count() == 0)
                                        break;

                                    int a1 = next_systole.First();

                                    if (a1 > 0)
                                    {
                                        diastoles.Add(frame_times[a1] - frame_times[b]);
                                        periods.Add(frame_times[a1] - frame_times[a]);
                                        //heart_rate.Add(1 / (frame_times[a1] - frame_times[a]));
                                    }
                                }

                                //RegionList rlist = ilist.ToRegionList();
                                //HeartBeatParameters hbparams = rlist.ToParameters(f.Cxd);

                                //systoles.AddRange((IEnumerable<object>)hbparams.m_systoles.Cast<object>());
                                //diastoles.AddRange((IEnumerable<object>)hbparams.m_diastoles.Cast<object>());
                                //periods.AddRange((IEnumerable<object>)hbparams.m_heartperiods.Cast<object>());
                            }
                        }

                        //Find longest dataset
                        int rows = systoles.Count > diastoles.Count ? systoles.Count : diastoles.Count;

                        //Fill 2D object matrix
                        object[,] data = new object[rows, 3];
                        for (int j = 0; j < systoles.Count; j++)
                        {
                            data[j, 2] = systoles[j];
                        }

                        for (int j = 0; j < diastoles.Count; j++)
                        {
                            data[j, 1] = diastoles[j];
                        }

                        for (int j = 0; j < periods.Count; j++)
                        {
                            data[j, 0] = periods[j];
                        }

                        out_data.Add(data);
                    }

                    string file = Globals.CurrentDirectory + "\\interval_output_" + s + "_" + n.ToString() + ".csv";
                    using (StreamWriter writer = new StreamWriter(file))
                    {
                        int r = 0;
                        bool done = false;
                        while (!done)
                        {
                            for (int i = 0; i < out_data.Count; i++)
                            {
                                for (int j = 0; j < (out_data[i] as object[,]).GetLength(1); j++)
                                {
                                    writer.Write((out_data[i] as object[,])[r, j] + ",");
                                }
                            }
                            writer.WriteLine();
                            r++;

                            done = true;
                            for (int i = 0; i < out_data.Count; i++)
                            {
                                if (r < (out_data[i] as object[,]).GetLength(0))
                                {
                                    done = false;
                                    break;
                                }
                            }
                        }
                    }

                    n++;
                    Process.Start(file);
                }*/
            }
            catch (Exception exc)
            {
                MessageBox.Show(this, exc.ToString()); 
                Logs.LogException(exc);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout about = new frmAbout();
            about.ShowDialog(this);
        }

        private void phaseIntervalsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{

            //    foreach (Filegram f in Globals.FilegramList)
            //    {                      

            //        string file = Globals.CurrentDirectory + "\\phase_output_" + f.Name.ToString() + ".csv";
            //        using (StreamWriter writer = new StreamWriter(file))
            //        {
            //            foreach()
            //            {

            //            }
            //        }

            //        n++;
            //        Process.Start(file);
            //    }
            //}
            //catch (Exception exc)
            //{
            //    MessageBox.Show(this, exc.ToString());
            //    Globals.LogException(exc, new string[] { "Discrete Output" });
            //}
        }

        private void filesForGraphingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Globals.FilegramList.Count <= 0)
            {
                MessageBox.Show("There are no files present to output.");
                return;
            }

            FolderBrowserDialog folder_browse = new FolderBrowserDialog();
            if (folder_browse.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;

            string folder = folder_browse.SelectedPath;

            var genotypes = Globals.FilegramList.GroupBy(x => x.CultureProperties.Genotype.ToUpper());
            foreach (IGrouping<string, Filegram> geno_group in genotypes)
            {
                List<object[]> output_data = new List<object[]>();
                output_data.Add(FilegramCollection.CreateHeader());
                foreach (Filegram f in geno_group)
                {
                    List<object[]> objs = f.ToDataRow();
                    foreach (object[] o_array in objs)
                        output_data.Add(o_array);
                }

                string file = folder + "\\" + geno_group.Key + ".csv";

                using (StreamWriter writer = new StreamWriter(file))
                {
                    for (int i = 0; i < output_data.Count; i++)
                    {
                        for (int j = 0; j < (output_data[i] as object[]).Length; j++)
                        {
                            writer.Write((output_data[i] as object[])[j] + ",");
                        }
                        writer.WriteLine();
                    }
                    writer.Close();
                }
            }
        }

        private void filterToolboxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFilterToolbox frmFilter = new frmFilterToolbox();
            frmFilter.MdiParent = this;
            frmFilter.Show();
        }

        private void supportBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://sohasoftware.com/support/report-bugs/");
        }

        private void recentIssuesAndResolutionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://sohasoftware.com/SOHA-known-issues.html");
        }

        private void createLogFileZipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Globals.CreateLogFileZip();
        }

        private void xMLConverterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmXMLConvert frmXMLConvert = new frmXMLConvert();
            frmXMLConvert.MdiParent = this;
            frmXMLConvert.Show();
        }

        private void saveLayoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
    }
}
