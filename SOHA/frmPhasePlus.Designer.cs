﻿using SOHA.SohaControls;
namespace SOHA
{
    partial class frmPhasePlus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SOHAControls.IntervalProperties intervalProperties1 = new SOHAControls.IntervalProperties();
            SOHAControls.IntervalProperties intervalProperties2 = new SOHAControls.IntervalProperties();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdQuit = new System.Windows.Forms.Button();
            this.cmdReset = new System.Windows.Forms.Button();
            this.cmdDiscard = new System.Windows.Forms.Button();
            this.cmdAccept = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.numOffset = new SOHA.SohaControls.SilentNumericUpDown(this.components);
            this.numZoom = new SOHA.SohaControls.SilentNumericUpDown(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.scrollIntensity = new System.Windows.Forms.VScrollBar();
            this.bpFilter = new SOHAControls.BandPassFilterControl();
            this.chkThreshold = new System.Windows.Forms.CheckBox();
            this.phaseDisplay1 = new SOHAControls.PhaseDisplay();
            this.mModeDisplay1 = new Forms.MModeDisplay(this.components);
            this.toolStripContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZoom)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(964, 490);
            this.toolStripContainer1.Size = new System.Drawing.Size(964, 490);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.phaseDisplay1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mModeDisplay1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(964, 490);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.cmdQuit, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmdReset, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmdDiscard, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmdAccept, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(767, 238);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(194, 229);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cmdQuit
            // 
            this.cmdQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdQuit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdQuit.Location = new System.Drawing.Point(100, 117);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(91, 109);
            this.cmdQuit.TabIndex = 3;
            this.cmdQuit.Text = "Quit";
            this.cmdQuit.UseVisualStyleBackColor = true;
            // 
            // cmdReset
            // 
            this.cmdReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdReset.Location = new System.Drawing.Point(3, 117);
            this.cmdReset.Name = "cmdReset";
            this.cmdReset.Size = new System.Drawing.Size(91, 109);
            this.cmdReset.TabIndex = 2;
            this.cmdReset.Text = "Reset";
            this.cmdReset.UseVisualStyleBackColor = true;
            // 
            // cmdDiscard
            // 
            this.cmdDiscard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDiscard.Location = new System.Drawing.Point(100, 3);
            this.cmdDiscard.Name = "cmdDiscard";
            this.cmdDiscard.Size = new System.Drawing.Size(91, 108);
            this.cmdDiscard.TabIndex = 1;
            this.cmdDiscard.Text = "Discard";
            this.cmdDiscard.UseVisualStyleBackColor = true;
            // 
            // cmdAccept
            // 
            this.cmdAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAccept.Location = new System.Drawing.Point(3, 3);
            this.cmdAccept.Name = "cmdAccept";
            this.cmdAccept.Size = new System.Drawing.Size(91, 108);
            this.cmdAccept.TabIndex = 0;
            this.cmdAccept.Text = "Accept";
            this.cmdAccept.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.numOffset, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.numZoom, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(767, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(194, 204);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Offset";
            // 
            // numOffset
            // 
            this.numOffset.Location = new System.Drawing.Point(100, 3);
            this.numOffset.Name = "numOffset";
            this.numOffset.Size = new System.Drawing.Size(91, 20);
            this.numOffset.TabIndex = 2;
            // 
            // numZoom
            // 
            this.numZoom.Location = new System.Drawing.Point(100, 33);
            this.numZoom.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numZoom.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numZoom.Name = "numZoom";
            this.numZoom.Size = new System.Drawing.Size(91, 20);
            this.numZoom.TabIndex = 4;
            this.numZoom.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Zoom";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel5, 2);
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.scrollIntensity, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.bpFilter, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.chkThreshold, 1, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 63);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(188, 138);
            this.tableLayoutPanel5.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Thresh.";
            // 
            // scrollIntensity
            // 
            this.scrollIntensity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrollIntensity.Location = new System.Drawing.Point(16, 49);
            this.scrollIntensity.Maximum = 111;
            this.scrollIntensity.Name = "scrollIntensity";
            this.tableLayoutPanel5.SetRowSpan(this.scrollIntensity, 3);
            this.scrollIntensity.Size = new System.Drawing.Size(17, 89);
            this.scrollIntensity.TabIndex = 1;
            // 
            // bpFilter
            // 
            this.bpFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bpFilter.Filter = null;
            this.bpFilter.HiCutoff = 0D;
            this.bpFilter.Location = new System.Drawing.Point(53, 3);
            this.bpFilter.LoCutoff = 0D;
            this.bpFilter.Name = "bpFilter";
            this.tableLayoutPanel5.SetRowSpan(this.bpFilter, 2);
            this.bpFilter.Size = new System.Drawing.Size(132, 92);
            this.bpFilter.TabIndex = 4;
            // 
            // chkThreshold
            // 
            this.chkThreshold.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkThreshold.AutoSize = true;
            this.chkThreshold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkThreshold.Location = new System.Drawing.Point(53, 101);
            this.chkThreshold.Name = "chkThreshold";
            this.tableLayoutPanel5.SetRowSpan(this.chkThreshold, 2);
            this.chkThreshold.Size = new System.Drawing.Size(132, 34);
            this.chkThreshold.TabIndex = 5;
            this.chkThreshold.Text = "Use Threshold";
            this.chkThreshold.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkThreshold.UseVisualStyleBackColor = true;
            this.chkThreshold.CheckedChanged += new System.EventHandler(this.chkThreshold_CheckedChanged);
            // 
            // phaseDisplay1
            // 
            this.phaseDisplay1.Data = null;
            this.phaseDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.phaseDisplay1.Fps = 0D;
            this.phaseDisplay1.HiCutoff = 0D;
            this.phaseDisplay1.InitialList = null;
            intervalProperties1.Interval = 1;
            intervalProperties1.IntervalSize = 1000;
            this.phaseDisplay1.Interval = intervalProperties1;
            this.phaseDisplay1.Location = new System.Drawing.Point(3, 3);
            this.phaseDisplay1.LoCutoff = 0D;
            this.phaseDisplay1.Name = "phaseDisplay1";
            this.phaseDisplay1.Offset = 0;
            this.phaseDisplay1.Size = new System.Drawing.Size(758, 229);
            this.phaseDisplay1.TabIndex = 3;
            this.phaseDisplay1.Threshold = 0;
            this.phaseDisplay1.UseThreshold = false;
            this.phaseDisplay1.Zoom = 0;
            // 
            // mModeDisplay1
            // 
            this.mModeDisplay1.BackColor = System.Drawing.Color.White;
            this.mModeDisplay1.DisplayCoordinates = false;
            this.mModeDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            intervalProperties2.Interval = 1;
            intervalProperties2.IntervalSize = 1000;
            this.mModeDisplay1.Interval = intervalProperties2;
            this.mModeDisplay1.List = null;
            this.mModeDisplay1.Location = new System.Drawing.Point(3, 238);
            this.mModeDisplay1.MMode = null;
            this.mModeDisplay1.Name = "mModeDisplay1";
            this.mModeDisplay1.Phase = SOHA.Library.PhaseFormType.Phase;
            this.mModeDisplay1.PhaseOffset = 0;
            this.mModeDisplay1.Range = null;
            this.mModeDisplay1.ShowChangeIntervals = false;
            this.mModeDisplay1.Size = new System.Drawing.Size(758, 229);
            this.mModeDisplay1.TabIndex = 4;
            this.mModeDisplay1.TabStop = false;
            this.mModeDisplay1.Zoom = 1;
            // 
            // frmPhasePlus
            // 
            this.AcceptButton = this.cmdAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdQuit;
            this.ClientSize = new System.Drawing.Size(964, 512);
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.Name = "frmPhasePlus";
            this.RejectButton = this.cmdDiscard;
            this.Text = "frmPhasePlus";
            this.Controls.SetChildIndex(this.toolStripContainer1, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZoom)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button cmdQuit;
        private System.Windows.Forms.Button cmdReset;
        private System.Windows.Forms.Button cmdDiscard;
        private System.Windows.Forms.Button cmdAccept;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label1;
        private SilentNumericUpDown numOffset;
        private SOHAControls.PhaseDisplay phaseDisplay1;
        private Forms.MModeDisplay mModeDisplay1;
        private SilentNumericUpDown numZoom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.VScrollBar scrollIntensity;
        private SOHAControls.BandPassFilterControl bpFilter;
        private System.Windows.Forms.CheckBox chkThreshold;
    }
}