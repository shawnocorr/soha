﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SOHA
{
    public delegate void ProgressUpdateHandler(int percent, EventArgs e);

    public class LoadQueue
    {
        Queue<string> m_queue = new Queue<string>();
        string m_filegram;
        int m_start_total;
        bool was_cancelled = false;

        public bool Cancelled
        {
            get
            {
                return was_cancelled;
            }
        }

        public event EventHandler QueueFinished;
        public event ProgressUpdateHandler ProgressUpdate;

        public LoadQueue(List<string> strings)
        {
            Filegram.LoadComplete += new FilegramLoadEventHandler(Filegram_LoadComplete);
            m_start_total = strings.Count;
            foreach (string s in strings)
                this.Enqueue(s);

            Dequeue();
        }

        public int Count
        {
            get
            {
                return m_queue.Count;
            }
        }

        public void Enqueue(string str)
        {
            m_queue.Enqueue(str);
        }

        void Filegram_LoadComplete(Filegram fgram, EventArgs e)
        {
            Globals.FilegramList.Add(fgram);

            if (m_queue.Count > 0)
            {
                if (ProgressUpdate != null)
                    ProgressUpdate((int)((double)(m_start_total - m_queue.Count) / m_start_total * 100), new EventArgs());

                Dequeue();
            }
            else
            {
                OnFinished();
            }

            if (was_cancelled)
                Globals.FilegramList.Clear();
        }

        public void Dequeue()
        {
            if (m_queue.Count != 0)
            {
                m_filegram = m_queue.Dequeue();
                //Thread t = new Thread(() => { Filegram.FromFilepath(m_filegram); });
                //t.Start();
            }
            else
            {
                OnFinished();
            }
        }

        public void OnFinished()
        {
            if (QueueFinished != null)
                    QueueFinished(this, new EventArgs());
        }

        public void Cancel()
        {
            was_cancelled = true;
            m_queue.Clear();
        }
    }
}
