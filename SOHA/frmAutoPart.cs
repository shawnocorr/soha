﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Video;
using System.IO;
using SOHA.Library.Mathematics;
using SOHA.Library.GPU;
using SOHA.Library.Imaging;

namespace SOHA
{

    public struct FrameVariations
    {
        private Frame m_frame;
        private CLImage m_clImage;
        private Bitmap m_bmp;

        public Frame Frm 
        { 
            get 
            { 
                return m_frame; 
            } 
        }
        public CLImage CLImg 
        { 
            get 
            { 
                return m_clImage; 
            }
            set
            {
                m_clImage = value;
            }
        }
        public Bitmap Bmp 
        { 
            get 
            { 
                return m_bmp; 
            }
            set
            {
                m_bmp = value;
                m_frame = new Frame(m_bmp, m_frame.FrameNumber, 0);
            }
        }

        public FrameVariations(Frame Frm)
        {
            m_frame = Frm;
            m_bmp = m_frame.ToBitmap();
            m_clImage = new CLImage(m_bmp);
        }
    }

    public partial class frmAutoPart : frmPhase
    {
        private readonly double BASE_SPEED;
        //private Frame[] m_frame_list = new Frame[2];
        FrameVariations[] m_frame_list = new FrameVariations[2];

        private Timer tmrPlay = new Timer();

        private Video m_video;

        private bool m_isPlaying = false;
        private double m_speed = 1;
        private double m_smooth_variance = 0;
        private double m_threshold = 10;

        public frmAutoPart()
        {
            InitializeComponent();
        }

        public static frmAutoPart Spawn(Filegram fg)
        {
            return new frmAutoPart(fg);
        }

        public frmAutoPart(Filegram fg)
            : base()
        {
            InitializeComponent();

            m_filegram = fg;
            m_filegram.Status = FilegramStatus.UserInterface;
            m_video = m_filegram.Video;
            m_video.Open();
            BASE_SPEED = (double)Math.Round(1000.0 / m_video.Fps, 0) * 10;

            this.FormClosing += new FormClosingEventHandler(frmAutoPart_FormClosing);

            tmrPlay.Interval = (int)BASE_SPEED;
            tmrPlay.Tick += new EventHandler(tmrPlay_Tick);

            cmbSpeed.SelectedIndex = cmbSpeed.Items.IndexOf("1x");
            cmbSpeed.SelectedIndexChanged += new EventHandler(cmbSpeed_SelectedIndexChanged);
            numSigma.ValueChanged += new EventHandler(numSigma_ValueChanged);
            numThreshold.Value = (decimal)m_threshold;
            numThreshold.ValueChanged += new EventHandler(numThreshold_ValueChanged);
            scrollFrame.Minimum = 2;
            scrollFrame.Maximum = m_video.Frames;
            scrollFrame.Value = 2;
            scrollFrame.Scroll += new ScrollEventHandler(scrollFrame_Scroll);

            cmdPlay.Click += new EventHandler(cmdPlay_Click);
            cmdToAvi.Click += new EventHandler(cmdToAvi_Click);

            m_frame_list[0] = new FrameVariations(m_video.GetFrameSync(1));
            m_frame_list[1] = new FrameVariations(m_video.GetFrameSync(2));
        }

        void cmdToAvi_Click(object sender, EventArgs e)
        {
            if (m_isPlaying)
                cmdPlay_Click("ExportAVI", new EventArgs());

            string parent_directory = Globals.CurrentDirectory;

            if (!Directory.Exists(parent_directory + @"\RedDotMovies"))
                Directory.CreateDirectory(parent_directory + @"\RedDotMovies");

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = parent_directory + @"\RedDotMovies";
            sfd.OverwritePrompt = true;
            sfd.Title = "Save Red Dot Movie";
            sfd.DefaultExt = ".avi";
            sfd.FileName = "RedDot_" + m_filegram.Name;
            sfd.Filter = "Avi Files (*.avi)|*.avi";

            if (sfd.ShowDialog(this) == DialogResult.OK)
            {
                string path = sfd.FileName;
                //mdiMain.ShowProcessNotify("Writing AVI...");

                #region Get Recorded Framerate

                frmUserInputFramerate frm = new frmUserInputFramerate(m_video.Fps, m_video.Frames);

                FrameRateSettings settings = new FrameRateSettings();
                if(frm.ShowDialog() == DialogResult.OK)
                {
                    settings.NewFrameRate = frm.NewFrameRate;
                    settings.Start = frm.Start;
                    settings.End = frm.End;
                }
                else
                {
                    return;
                }

                #endregion

                AVIVideo.WriteAvi(m_video, m_threshold / 100, path, settings);

                //mdiMain.EndProcessNotify();
            }
        }

        void frmAutoPart_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_isPlaying)
                cmdPlay_Click(sender, e);

            m_video.Close();
        }

        void cmdPlay_Click(object sender, EventArgs e)
        {
            m_isPlaying = !m_isPlaying;
            tmrPlay.Enabled = m_isPlaying;
            cmdPlay.Text = m_isPlaying ? "Stop" : "Play";
        }

        void scrollFrame_Scroll(object sender, ScrollEventArgs e)
        {
            if (m_isPlaying)
                tmrPlay.Enabled = false;

            m_frame_list[1] = new FrameVariations(m_video.GetFrameSync(e.NewValue));
            m_frame_list[0] = new FrameVariations(m_video.GetFrameSync(e.NewValue - 1));
            //timerPlay_Tick("Scroll", new EventArgs());

            if (m_smooth_variance > 0)
            {
                DateTime s = DateTime.Now;
                //m_frame_list[1] = m_frame_list[1].GaussSmooth((float)m_smooth_variance);
                //m_frame_list[0] = m_frame_list[0].GaussSmooth((float)m_smooth_variance);
                TimeSpan t = DateTime.Now - s;
            }

            Bitmap B = Frame.ToRedDotMovie(m_frame_list[0].Frm, m_frame_list[1].Frm, m_threshold / 100);

            picFrame.BackgroundImage = B;
            picFrame.BackgroundImageLayout = ImageLayout.Stretch;

            if (m_isPlaying)
                tmrPlay.Enabled = true;
        }

        void numThreshold_ValueChanged(object sender, EventArgs e)
        {
            m_threshold = (double)numThreshold.Value;
        }

        void numSigma_ValueChanged(object sender, EventArgs e)
        {
            m_smooth_variance = (double)numSigma.Value;
        }

        void cmbSpeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            string n = cmbSpeed.Text.Substring(0, cmbSpeed.Text.Length - 1);
            m_speed = Double.Parse(n);

            tmrPlay.Interval = (int)(BASE_SPEED / m_speed) * 10;
        }

        void tmrPlay_Tick(object sender, EventArgs e)
        {
            Bitmap B = null;
            int next_frame = m_frame_list[1].Frm.FrameNumber + 1;

            if (next_frame <= m_video.Frames)
            {
                m_frame_list[0] = m_frame_list[1];
                m_frame_list[1] = new FrameVariations(m_video.GetFrameSync(next_frame));

                scrollFrame.Value = next_frame;
            }
            else
            {
                cmdPlay_Click("MovieEnd", new EventArgs());

                m_frame_list[0] = new FrameVariations(m_video.GetFrameSync(1));
                m_frame_list[1] = new FrameVariations(m_video.GetFrameSync(2));

                scrollFrame.Value = 2;
            }
            
            if (m_smooth_variance > 0)
            {
                ImageFilter gauss_smooth = ImageFilter.Gaussian2D((float)m_smooth_variance, 3);
                List<ImageFilter> filters = new List<ImageFilter>();//m_filters;//frmFilters.GetAllFilters();
                filters.Add(gauss_smooth);

                GpuFrame gFrame = null;
                Bitmap bmpa = m_frame_list[1].Frm.ToBitmap();
                CLImage img = new CLImage(bmpa);
                if(filters.Count > 0)
                {
                    m_frame_list[1].Frm.Filter(filters);
                    //List<CLFilter> clfilters = new List<CLFilter>();
                    //foreach(ImageFilter imgFilter in filters)
                    //    clfilters.Add((CLFilter)imgFilter);

                    //GPUInterop.ApplyFilters(img, clfilters);
                    //B = img.GetStoredBitmap(bmpa);
                    B = m_frame_list[1].Frm.ToBitmap();
                }

                //m_frame_list[1] = new FrameVariations(new Frame(B, 1, 1));
            }

            //Frame f1 = new Frame(m_frame_list[0], 0,0);
            //B = Frame.ToRedDotMovie(m_frame_list[0].Frm, m_frame_list[1].Frm, m_threshold / 100);

            picFrame.BackgroundImage = B;
            picFrame.BackgroundImageLayout = ImageLayout.Stretch;
        }

        //private static FrameVariations Functions(FrameVariations frm)
        //{

        //}
    }
}
