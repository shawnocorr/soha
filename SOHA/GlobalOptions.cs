﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using SOHA.Library.Xml;
using SOHA.Library;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing.Design;
using System.Xml.Serialization;
using System.Xml;
using System.Security.Principal;
using System.Security.AccessControl;
using System.IO;

namespace SOHA
{

    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class GlobalOptions
    {
        private static GlobalOptions m_instance = null;
        private static readonly object _padlock = new object();

        [EditorBrowsable()]
        [Description("Number of Frames to Skip for preprocessing and viewing within each Phase.")]
        [XmlAttribute()]
        public int SkipFrames { get; set; }

        [EditorBrowsable()]
        [Description("Size of processing Queue. Should not be greater than number of Processors.")]
        [XmlAttribute()]
        public int ProcessQueueSize { get; set; }

        [EditorBrowsable()]
        [Description("Automatically open next video's Phase GUI.")]
        [XmlAttribute()]
        public bool AutoGUI { get; set; }

        [EditorBrowsable()]
        [Description("Automatically begin preprocess after Mark Phase completion")]
        [XmlAttribute()]
        public bool AutoProcess { get; set; }

        [EditorBrowsable()]
        [Description("Affects the Interval and Phase Forms. Determines the number of frames in a window in these phases.")]
        [XmlAttribute()]
        public int FramesPerRange { get; set; }

        [EditorBrowsable()]
        [Description("Output is ON or OFF")]
        [XmlAttribute()]
        public bool OutputOn { get; set; }
        
        public GlobalOptions() 
        {
            SkipFrames = 10;
            ProcessQueueSize = 2;
            AutoGUI = true;
            AutoProcess = true;
            FramesPerRange = 1000;
            OutputOn = true;
        }

        public static GlobalOptions ReadFile()
        {
            string path = Path.GetDirectoryName(Application.ExecutablePath) + @"\options.xml";
            //try
            //{
            //    using(XmlTextReader reader = new XmlTextReader(path))
            //    {
            //        XmlSerializer deserializer = new XmlSerializer(typeof(GlobalOptions));
            //        return (GlobalOptions)deserializer.Deserialize(reader);
            //    }
            //}
            //catch(Exception exc)
            //{

            //}

            return new GlobalOptions();
        }

        public void WriteFile()
        {
            string path = Path.GetDirectoryName(Application.ExecutablePath) + @"\options.xml";

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(GlobalOptions));
                using(XmlTextWriter writer = new XmlTextWriter(path, Encoding.UTF8))
                {
                    serializer.Serialize(writer, this);
                }

                //NTAccount act_everyone = new NTAccount("Everyone");
                //SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);

                //FileSecurity sec = File.GetAccessControl(path);
                //sec.AddAccessRule(new FileSystemAccessRule(act_everyone, FileSystemRights.FullControl, AccessControlType.Allow));
                //File.SetAccessControl(path, sec);
            }
            catch(Exception exc)
            {

            }
        }
    }
}
