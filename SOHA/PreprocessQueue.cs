﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library;
using System.Threading;
using SOHA.Library.Video;
using System.Threading.Tasks;
using SOHA.Library.Xml;

namespace SOHA
{
    public class PreprocessQueue
    {
        Queue<Filegram> m_queue = new Queue<Filegram>();
        Filegram m_filegram;

        public event PreprocessProgressHandler ProgressUpdate;

        public PreprocessQueue()
        {
        }

        public int Count
        {
            get
            {
                return m_queue.Count;
            }
        }

        public bool Processing
        {
            get
            {
                if (m_filegram == null)
                    return false;

                return m_filegram.Status == FilegramStatus.Processing;
            }
        }

        public void Enqueue(Filegram fg)
        {
            m_queue.Enqueue(fg);
            fg.Status = FilegramStatus.Queued;
            //Video
            fg.Video.PreprocessComplete += new PreprocessEventHandler(Video_PreprocessComplete);
            //fg.Video.ProgressUpdate += new PreprocessProgressHandler(Video_ProgressUpdate);

            if (m_filegram == null)
                Dequeue();
        }

        void Video_ProgressUpdate(Video sender, int progress)
        {
            if (ProgressUpdate != null)
                ProgressUpdate(sender, progress);
        }

        void Video_PreprocessComplete(object sender, PreprocessData[] ppData)
        {
            if (ppData != null)
            {
                PreprocessPhaseOutput process_output = m_filegram.Phases[1] as PreprocessPhaseOutput;
                process_output.Status = PhaseStatus.OK;
                process_output.Data.Clear();

                foreach (PreprocessData data in ppData)
                {
                    PreprocessPhaseData out_data = new PreprocessPhaseData();
                    out_data.Brightness = data.Darkness.ToList();
                    out_data.Intensity = data.Intensity.ToList();
                    out_data.MMode = data.MMode;

                    process_output.Data.Add(out_data);
                }

                m_filegram.Serialize(m_filegram);
                //Filegram.Serialize(m_filegram);
            }
            else
            {
                PreprocessPhaseOutput process_output = m_filegram.Phases[1] as PreprocessPhaseOutput;
                process_output.Status = PhaseStatus.Reject;
                process_output.Data.Clear();
            }

            if (m_filegram != null)
            {
                m_filegram.Status = FilegramStatus.Idle;
            }

            if (m_queue.Count > 0)
            {
                Dequeue();
            }
            else
            {
                m_filegram = null;
            }
        }

        public void Dequeue()
        {
            m_filegram = m_queue.Dequeue();

            List<Roi> rois = (m_filegram.Phases[0].Data[0] as MarkPhaseData).Rois.ToList();

            m_filegram.Status = FilegramStatus.Processing;
            var task = new Task(() => { m_filegram.Video.GetPreprocess(rois); });
            task.Start();
            //Thread t = new Thread(() => { m_filegram.Video.GetPreprocess(rois); });
            //ThreadPool.QueueUserWorkItem(delegate { m_filegram.Video.GetPreprocess(rois); });
            //t.Start();
        }

        public void Remove(Filegram fg)
        {
            if (m_queue.Contains(fg))
            {
                Queue<Filegram> new_queue = new Queue<Filegram>();

                while (m_queue.Count > 0)
                {
                    Filegram f = m_queue.Dequeue();

                    if (f != fg)
                        new_queue.Enqueue(f);
                }

                fg.Status = FilegramStatus.Idle;
                m_queue = new_queue;
            }
        }

        public void RemoveAll()
        {
            if(m_filegram != null)
                m_filegram.Video.CancelProcess();

            foreach (Filegram f in m_queue)
            {
                f.Status = FilegramStatus.Idle;
            }
            m_queue.Clear();

            //foreach (Filegram f in m_processing_list)
            //    f.CancelPreProcess();

            //if (m_filegram != null)
                //m_filegram.CancelPreProcess();
        }
    }
}
