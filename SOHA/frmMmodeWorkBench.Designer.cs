﻿namespace SOHA
{
    partial class frmMmodeWorkBench
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMmodeWorkBench));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ucEqualize1 = new SOHAControls.FilterControls.ucEqualize();
            this.ucNormalize1 = new SOHAControls.FilterControls.ucSegment();
            this.ucGaussianSmooth1 = new SOHAControls.FilterControls.ucGaussianSmooth();
            this.ucSobel1 = new SOHAControls.FilterControls.ucSobel();
            this.ucSequentialFilterLayout1 = new SOHAControls.FilterControls.ucSequentialFilterLayout();
            this.ucHistogram1 = new SOHAControls.DisplayControls.ucHistogram();
            this.mModeFrameDisplay1 = new SOHAControls.DisplayControls.MModeFrameDisplay();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tableLayoutPanel1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(809, 484);
            this.toolStripContainer1.Size = new System.Drawing.Size(809, 484);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucSequentialFilterLayout1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucHistogram1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.mModeFrameDisplay1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(809, 484);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.checkBox1, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.ucEqualize1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.ucNormalize1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ucGaussianSmooth1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.ucSobel1, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 245);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(603, 236);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(204, 121);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 23);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "Filter Enable";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // ucEqualize1
            // 
            this.ucEqualize1.Location = new System.Drawing.Point(3, 62);
            this.ucEqualize1.Name = "ucEqualize1";
            this.ucEqualize1.Size = new System.Drawing.Size(195, 51);
            this.ucEqualize1.TabIndex = 11;
            // 
            // ucNormalize1
            // 
            this.ucNormalize1.Location = new System.Drawing.Point(3, 3);
            this.ucNormalize1.Name = "ucNormalize1";
            this.ucNormalize1.Size = new System.Drawing.Size(195, 51);
            this.ucNormalize1.TabIndex = 9;
            // 
            // ucGaussianSmooth1
            // 
            this.ucGaussianSmooth1.Location = new System.Drawing.Point(204, 3);
            this.ucGaussianSmooth1.Name = "ucGaussianSmooth1";
            this.ucGaussianSmooth1.Size = new System.Drawing.Size(195, 51);
            this.ucGaussianSmooth1.TabIndex = 10;
            // 
            // ucSobel1
            // 
            this.ucSobel1.Location = new System.Drawing.Point(204, 62);
            this.ucSobel1.Name = "ucSobel1";
            this.ucSobel1.Size = new System.Drawing.Size(195, 51);
            this.ucSobel1.TabIndex = 12;
            // 
            // ucSequentialFilterLayout1
            // 
            this.ucSequentialFilterLayout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucSequentialFilterLayout1.Location = new System.Drawing.Point(612, 245);
            this.ucSequentialFilterLayout1.Name = "ucSequentialFilterLayout1";
            this.ucSequentialFilterLayout1.Size = new System.Drawing.Size(194, 236);
            this.ucSequentialFilterLayout1.TabIndex = 6;
            // 
            // ucHistogram1
            // 
            this.ucHistogram1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucHistogram1.Location = new System.Drawing.Point(612, 3);
            this.ucHistogram1.Name = "ucHistogram1";
            this.ucHistogram1.Size = new System.Drawing.Size(194, 236);
            this.ucHistogram1.TabIndex = 5;
            // 
            // mModeFrameDisplay1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.mModeFrameDisplay1, 2);
            this.mModeFrameDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mModeFrameDisplay1.EnableFilters = false;
            this.mModeFrameDisplay1.Filters = ((System.Collections.Generic.List<SOHA.Library.Imaging.IFrameFunction>)(resources.GetObject("mModeFrameDisplay1.Filters")));
            this.mModeFrameDisplay1.Location = new System.Drawing.Point(3, 3);
            this.mModeFrameDisplay1.Name = "mModeFrameDisplay1";
            this.mModeFrameDisplay1.NormalPen = null;
            this.mModeFrameDisplay1.Size = new System.Drawing.Size(603, 236);
            this.mModeFrameDisplay1.TabIndex = 8;
            this.mModeFrameDisplay1.UserInteractive = false;
            this.mModeFrameDisplay1.XCoord = 0;
            this.mModeFrameDisplay1.CreateMMode += new System.EventHandler<SOHAControls.DisplayControls.CreateMmodeEventArgs>(this.mModeFrameDisplay1_CreateMMode);
            // 
            // frmMmodeWorkBench
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 506);
            this.Name = "frmMmodeWorkBench";
            this.Text = "frmMmodeWorkBench";
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private SOHAControls.FilterControls.ucSequentialFilterLayout ucSequentialFilterLayout1;
        private SOHAControls.DisplayControls.ucHistogram ucHistogram1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox checkBox1;
        private SOHAControls.FilterControls.ucEqualize ucEqualize1;
        private SOHAControls.FilterControls.ucSegment ucNormalize1;
        private SOHAControls.FilterControls.ucGaussianSmooth ucGaussianSmooth1;
        private SOHAControls.FilterControls.ucSobel ucSobel1;
        private SOHAControls.DisplayControls.MModeFrameDisplay mModeFrameDisplay1;
    }
}