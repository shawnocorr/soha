﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHA
{
    public partial class frmMessageBox : Form
    {
        public frmMessageBox()
        {
            InitializeComponent();
        }

        public frmMessageBox(string msg, string title, MessageBoxButtons buttons)
        {
            InitializeComponent();

            label1.Text = msg;
            this.Text = title;

            int n = 1;
            int w = 0;

            switch (buttons)
            {
                case MessageBoxButtons.AbortRetryIgnore:
                    break;
                case MessageBoxButtons.OK:
                    n = 1;
                    w = (flowLayoutPanel1.Width - 50) / n;

                    flowLayoutPanel1.SuspendLayout();
                    flowLayoutPanel1.Controls.Add(new Button() { DialogResult = DialogResult.OK, Text = "OK", Width = w });
                    flowLayoutPanel1.ResumeLayout(true);
                    break;
                case MessageBoxButtons.OKCancel:
                    n = 2;
                    w = (flowLayoutPanel1.Width - 50) / n;

                    flowLayoutPanel1.SuspendLayout();
                    flowLayoutPanel1.Controls.Add(new Button() { DialogResult = DialogResult.OK, Text = "OK", Width = w });
                    flowLayoutPanel1.Controls.Add(new Button() { DialogResult = DialogResult.Cancel, Text = "Cancel", Width = w });
                    flowLayoutPanel1.ResumeLayout(true);

                    break;
                case MessageBoxButtons.RetryCancel:
                    break;
                case MessageBoxButtons.YesNo:
                    break;
                case MessageBoxButtons.YesNoCancel:
                    break;
                default:
                    break;
            }
        }

        //public DialogResult ShowDialog(string text, string caption, MessageBoxButtons buttons)
        //{
            
        //}
    }
}
