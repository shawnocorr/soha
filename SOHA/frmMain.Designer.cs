﻿namespace SOHA
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analyzeDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.camerasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xMLConverterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeCSVFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeDiscreteIntervalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeFileForGraphingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supportBugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recentIssuesAndResolutionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createLogFileZipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.layoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vS2005Theme1 = new WeifenLuo.WinFormsUI.Docking.VS2005Theme();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dockPanel1
            // 
            this.dockPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel1.Location = new System.Drawing.Point(0, 24);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Size = new System.Drawing.Size(882, 600);
            this.dockPanel1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.outputToolStripMenuItem,
            this.helpMenu,
            this.layoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(882, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analyzeDataToolStripMenuItem,
            this.toolStripMenuItem2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // analyzeDataToolStripMenuItem
            // 
            this.analyzeDataToolStripMenuItem.Name = "analyzeDataToolStripMenuItem";
            this.analyzeDataToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.analyzeDataToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.analyzeDataToolStripMenuItem.Text = "&Analyze Data";
            this.analyzeDataToolStripMenuItem.Click += new System.EventHandler(this.analyzeDataToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(182, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.camerasToolStripMenuItem,
            this.xMLConverterToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.optionsToolStripMenuItem.Text = "&Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // camerasToolStripMenuItem
            // 
            this.camerasToolStripMenuItem.Name = "camerasToolStripMenuItem";
            this.camerasToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.camerasToolStripMenuItem.Text = "&Cameras";
            this.camerasToolStripMenuItem.Click += new System.EventHandler(this.camerasToolStripMenuItem_Click);
            // 
            // xMLConverterToolStripMenuItem
            // 
            this.xMLConverterToolStripMenuItem.Name = "xMLConverterToolStripMenuItem";
            this.xMLConverterToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.xMLConverterToolStripMenuItem.Text = "&XML Converter";
            this.xMLConverterToolStripMenuItem.Click += new System.EventHandler(this.xMLConverterToolStripMenuItem_Click);
            // 
            // outputToolStripMenuItem
            // 
            this.outputToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeCSVFileToolStripMenuItem,
            this.writeDiscreteIntervalsToolStripMenuItem,
            this.writeFileForGraphingToolStripMenuItem});
            this.outputToolStripMenuItem.Name = "outputToolStripMenuItem";
            this.outputToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.outputToolStripMenuItem.Text = "&Output";
            // 
            // writeCSVFileToolStripMenuItem
            // 
            this.writeCSVFileToolStripMenuItem.Name = "writeCSVFileToolStripMenuItem";
            this.writeCSVFileToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.writeCSVFileToolStripMenuItem.Text = "Write CS&V File";
            this.writeCSVFileToolStripMenuItem.Click += new System.EventHandler(this.writeCSVFileToolStripMenuItem_Click);
            // 
            // writeDiscreteIntervalsToolStripMenuItem
            // 
            this.writeDiscreteIntervalsToolStripMenuItem.Name = "writeDiscreteIntervalsToolStripMenuItem";
            this.writeDiscreteIntervalsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.writeDiscreteIntervalsToolStripMenuItem.Text = "Write &Discrete Intervals";
            this.writeDiscreteIntervalsToolStripMenuItem.Click += new System.EventHandler(this.writeDiscreteIntervalsToolStripMenuItem_Click);
            // 
            // writeFileForGraphingToolStripMenuItem
            // 
            this.writeFileForGraphingToolStripMenuItem.Name = "writeFileForGraphingToolStripMenuItem";
            this.writeFileForGraphingToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.writeFileForGraphingToolStripMenuItem.Text = "Write File For Graphing";
            this.writeFileForGraphingToolStripMenuItem.Click += new System.EventHandler(this.writeFileForGraphingToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.supportBugsToolStripMenuItem,
            this.recentIssuesAndResolutionsToolStripMenuItem,
            this.createLogFileZipToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // supportBugsToolStripMenuItem
            // 
            this.supportBugsToolStripMenuItem.Name = "supportBugsToolStripMenuItem";
            this.supportBugsToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.supportBugsToolStripMenuItem.Text = "Support/Bugs";
            this.supportBugsToolStripMenuItem.Click += new System.EventHandler(this.supportBugsToolStripMenuItem_Click);
            // 
            // recentIssuesAndResolutionsToolStripMenuItem
            // 
            this.recentIssuesAndResolutionsToolStripMenuItem.Name = "recentIssuesAndResolutionsToolStripMenuItem";
            this.recentIssuesAndResolutionsToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.recentIssuesAndResolutionsToolStripMenuItem.Text = "Known Issues";
            this.recentIssuesAndResolutionsToolStripMenuItem.Click += new System.EventHandler(this.recentIssuesAndResolutionsToolStripMenuItem_Click);
            // 
            // createLogFileZipToolStripMenuItem
            // 
            this.createLogFileZipToolStripMenuItem.Name = "createLogFileZipToolStripMenuItem";
            this.createLogFileZipToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.createLogFileZipToolStripMenuItem.Text = "Create Log File Zip";
            this.createLogFileZipToolStripMenuItem.Click += new System.EventHandler(this.createLogFileZipToolStripMenuItem_Click);
            // 
            // layoutToolStripMenuItem
            // 
            this.layoutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.layoutToolStripMenuItem.Name = "layoutToolStripMenuItem";
            this.layoutToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.layoutToolStripMenuItem.Text = "Layout";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 624);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "SOHA";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analyzeDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem camerasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xMLConverterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeCSVFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeDiscreteIntervalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeFileForGraphingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supportBugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recentIssuesAndResolutionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createLogFileZipToolStripMenuItem;
        private WeifenLuo.WinFormsUI.Docking.VS2005Theme vS2005Theme1;
        private System.Windows.Forms.ToolStripMenuItem layoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
    }
}