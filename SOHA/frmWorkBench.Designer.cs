﻿using SOHA.Library;
namespace SOHA
{
    partial class frmWorkBench
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ucSequentialFilterLayout1 = new SOHAControls.FilterControls.ucSequentialFilterLayout();
            this.ucHistogram1 = new SOHAControls.DisplayControls.ucHistogram();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ucEqualize1 = new SOHAControls.FilterControls.ucEqualize();
            this.ucNormalize1 = new SOHAControls.FilterControls.ucSegment();
            this.ucGaussianSmooth1 = new SOHAControls.FilterControls.ucGaussianSmooth();
            this.ucSobel1 = new SOHAControls.FilterControls.ucSobel();
            this.button1 = new System.Windows.Forms.Button();
            this.ucCanny1 = new SOHAControls.FilterControls.ucCanny();
            this.frameDisplay1 = new SOHAControls.DisplayControls.FrameDisplay();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tableLayoutPanel1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(911, 467);
            this.toolStripContainer1.Size = new System.Drawing.Size(911, 467);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.Controls.Add(this.ucSequentialFilterLayout1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucHistogram1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.frameDisplay1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(911, 467);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ucSequentialFilterLayout1
            // 
            this.ucSequentialFilterLayout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucSequentialFilterLayout1.Location = new System.Drawing.Point(664, 236);
            this.ucSequentialFilterLayout1.Name = "ucSequentialFilterLayout1";
            this.ucSequentialFilterLayout1.Size = new System.Drawing.Size(244, 228);
            this.ucSequentialFilterLayout1.TabIndex = 6;
            this.ucSequentialFilterLayout1.Updated += new System.EventHandler(this.ucSequentialFilterLayout1_Updated);
            // 
            // ucHistogram1
            // 
            this.ucHistogram1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucHistogram1.Location = new System.Drawing.Point(664, 3);
            this.ucHistogram1.Name = "ucHistogram1";
            this.ucHistogram1.Size = new System.Drawing.Size(244, 227);
            this.ucHistogram1.TabIndex = 5;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.checkBox1, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.ucEqualize1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.ucNormalize1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ucGaussianSmooth1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.ucSobel1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.button1, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.ucCanny1, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 236);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(655, 228);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(221, 117);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 23);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "Filter Enable";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // ucEqualize1
            // 
            this.ucEqualize1.Location = new System.Drawing.Point(3, 60);
            this.ucEqualize1.Name = "ucEqualize1";
            this.ucEqualize1.Size = new System.Drawing.Size(210, 51);
            this.ucEqualize1.TabIndex = 11;
            // 
            // ucNormalize1
            // 
            this.ucNormalize1.Location = new System.Drawing.Point(3, 3);
            this.ucNormalize1.Name = "ucNormalize1";
            this.ucNormalize1.Size = new System.Drawing.Size(210, 51);
            this.ucNormalize1.TabIndex = 9;
            // 
            // ucGaussianSmooth1
            // 
            this.ucGaussianSmooth1.Location = new System.Drawing.Point(221, 3);
            this.ucGaussianSmooth1.Name = "ucGaussianSmooth1";
            this.ucGaussianSmooth1.Size = new System.Drawing.Size(210, 51);
            this.ucGaussianSmooth1.TabIndex = 10;
            // 
            // ucSobel1
            // 
            this.ucSobel1.Location = new System.Drawing.Point(221, 60);
            this.ucSobel1.Name = "ucSobel1";
            this.ucSobel1.Size = new System.Drawing.Size(210, 51);
            this.ucSobel1.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(439, 117);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ucCanny1
            // 
            this.ucCanny1.Location = new System.Drawing.Point(3, 117);
            this.ucCanny1.Name = "ucCanny1";
            this.ucCanny1.Size = new System.Drawing.Size(212, 51);
            this.ucCanny1.TabIndex = 14;
            // 
            // frameDisplay1
            // 
            this.frameDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frameDisplay1.EnableFilters = false;
            this.frameDisplay1.Location = new System.Drawing.Point(3, 3);
            this.frameDisplay1.MmodeDisplay = false;
            this.frameDisplay1.Name = "frameDisplay1";
            this.frameDisplay1.Size = new System.Drawing.Size(655, 227);
            this.frameDisplay1.TabIndex = 8;
            this.frameDisplay1.UserInteractive = false;
            this.frameDisplay1.FrameChanged += new System.EventHandler<SOHA.Library.FrameEventArgs>(this.frameDisplay1_FrameChanged);
            this.frameDisplay1.HistogramUpdated += new System.EventHandler(this.frameDisplay1_HistogramUpdated);
            this.frameDisplay1.Paint += new System.Windows.Forms.PaintEventHandler(this.frameDisplay1_Paint);
            // 
            // frmWorkBench
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 489);
            this.KeyPreview = true;
            this.Name = "frmWorkBench";
            this.Text = "WorkBench";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private SOHAControls.DisplayControls.ucHistogram ucHistogram1;
        private SOHAControls.FilterControls.ucSequentialFilterLayout ucSequentialFilterLayout1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private SOHAControls.FilterControls.ucSegment ucNormalize1;
        private SOHAControls.FilterControls.ucGaussianSmooth ucGaussianSmooth1;
        private SOHAControls.FilterControls.ucEqualize ucEqualize1;
        private SOHAControls.FilterControls.ucSobel ucSobel1;
        private System.Windows.Forms.CheckBox checkBox1;
        private SOHAControls.DisplayControls.FrameDisplay frameDisplay1;
        private System.Windows.Forms.Button button1;
        private SOHAControls.FilterControls.ucCanny ucCanny1;
    }
}