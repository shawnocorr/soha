﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using SOHA.Library.Xml;
using System.Reflection;
using SOHA.Library;
using SOHA.Library.Video;
using System.Drawing;
using System.IO;

namespace SOHA
{

    public enum SohaXmlFileType
    {
        NA,
        OHBA,
        Filegram,
        Filegram_v3p5
    }

    public struct SohaXmlFileReadState
    {
        public string Path { get; set; }
        public string NodeName { get; set; }
    }

    public class SohaXmlConvert
    {
        /// <summary>
        /// Returns the expected version of the SOHA XML file.
        /// 
        /// OHBA:
        ///     -Find an element with the name <i>OHBAFile</i>
        /// Filegram:
        ///     -Find an element with the name <i>Filegram</i>
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static SohaXmlFileType FilegramType(string filepath)
        {

            using(XmlTextReader reader = new XmlTextReader(filepath))
            {
                reader.WhitespaceHandling = WhitespaceHandling.None;

                while(reader.Read())
                {
                    if(reader.IsStartElement())
                    {
                        if(reader.Name == "OHBAFile")
                            return SohaXmlFileType.OHBA;

                        if(reader.Name == "Filegram")
                            return SohaXmlFileType.Filegram;
                    }
                }
            }

            return SohaXmlFileType.NA;
        }

        /// <summary>
        /// 2016/07/31
        /// Converts older OHBAFile types to SOHAVersion 3.0.0.5.
        /// </summary>
        /// <param name="ohba_file"></param>
        /// <param name="new_file"></param>
        public static void ConvertOHBAFile(string ohba_file, string new_file)
        {
            SohaXmlFileReadState read_state = new SohaXmlFileReadState();
            read_state.Path = ohba_file;

            try
            {
                Filegram new_filegram = new Filegram();

                //XmlDocument infile = new XmlDocument();
                //XmlDocument outfile = new XmlDocument();

                //infile.Load(ohba_file);

                using(XmlTextReader reader = new XmlTextReader(ohba_file))
                {
                    reader.WhitespaceHandling = WhitespaceHandling.None;

                    CultureProperties properties = new CultureProperties();
                    CXDVideo video = new CXDVideo();
                    Camera camera = new Camera();

                    PhaseOutputList phase_list = new PhaseOutputList();
                    MarkPhaseOutput mark_output = new MarkPhaseOutput();
                    PreprocessPhaseOutput preprocess_output = new PreprocessPhaseOutput();
                    IntervalPhaseOutput interval_output = new IntervalPhaseOutput();

                    MarkPhaseData mark_data = new MarkPhaseData();
                    PreprocessPhaseData preprocess_data = new PreprocessPhaseData();
                    IntervalPhaseData interval_data = new IntervalPhaseData();
                    IntervalPhaseSettings interval_settings = new IntervalPhaseSettings();

                    while(reader.Read())
                    {
                        read_state.NodeName = reader.Name;

                        if(reader.IsStartElement())
                        {

                            #region Read and Convert Culture Properties

                            if(reader.Name == "Culture")
                            {
                                foreach(PropertyInfo prop_info in (properties.GetType().GetProperties()))
                                {
                                    string prop_name = prop_info.Name;
                                    Type prop_type = prop_info.PropertyType;
                                    if(reader.GetAttribute(prop_name) != null)
                                    {
                                        string ret = reader.GetAttribute(prop_name);
                                        prop_info.SetValue(properties, Convert.ChangeType(reader.GetAttribute(prop_name), prop_type), null);
                                    }
                                }
                                //new_filegram.CultureProperties = properties;
                            }

                            #endregion

                            #region Read and Convert Video

                            if(reader.Name == "Video")
                            {
                                foreach(PropertyInfo prop_info in (video.GetType().GetProperties()))
                                {
                                    string prop_name = prop_info.Name;
                                    Type prop_type = prop_info.PropertyType;
                                    if(reader.GetAttribute(prop_name) != null)
                                    {
                                        string ret = reader.GetAttribute(prop_name);
                                        prop_info.SetValue(video, Convert.ChangeType(reader.GetAttribute(prop_name), prop_type), null);
                                    }
                                }
                                //new_filegram.Video = video;
                            }

                            #endregion

                            #region Read and Convert Camera

                            if(reader.Name == "Camera:camera")
                            {
                                foreach(PropertyInfo prop_info in (camera.GetType().GetProperties()))
                                {
                                    string prop_name = prop_info.Name;
                                    Type prop_type = prop_info.PropertyType;
                                    //Iterate Child Elements not attributes
                                    if(reader.GetAttribute(prop_name) != null)
                                    {
                                        string ret = reader.GetAttribute(prop_name);
                                        prop_info.SetValue(camera, Convert.ChangeType(reader.GetAttribute(prop_name), prop_type), null);
                                    }
                                }
                                video.Camera = camera;
                            }

                            #endregion

                            #region Read and Convert Mark Phase Data

                            if(reader.Name == "Mark")
                            {
                                mark_output.Status = (reader.GetAttribute("Use") == "yes" ? PhaseStatus.OK : PhaseStatus.Reject);
                            }

                            if(reader.Name == "Measure:measure")
                            {
                                MarkPair mp = new MarkPair();
                                foreach(PropertyInfo prop_info in (mp.GetType().GetProperties()))
                                {
                                    string prop_name = prop_info.Name;
                                    Type prop_type = prop_info.PropertyType;
                                    if(reader.GetAttribute(prop_name) != null)
                                    {
                                        string ret = reader.GetAttribute(prop_name);

                                        if(!prop_type.IsEnum)
                                        {
                                            prop_info.SetValue(mp, Convert.ChangeType(reader.GetAttribute(prop_name), prop_type), null);
                                        }
                                        else
                                        {
                                            prop_info.SetValue(mp, Enum.Parse(prop_type, ret), null);
                                        }
                                    }
                                    if(prop_name == "Mark1")
                                    {
                                        Point p1 = new Point(int.Parse(reader.GetAttribute("x1")), int.Parse(reader.GetAttribute("y1")));
                                        mp.Mark1 = p1;
                                    }
                                    if(prop_name == "Mark2")
                                    {
                                        Point p2 = new Point(int.Parse(reader.GetAttribute("x2")), int.Parse(reader.GetAttribute("y2")));
                                        mp.Mark2 = p2;
                                    }
                                    if(prop_name == "Beat")
                                    {
                                        mp.Beat = (BeatType)Enum.Parse(typeof(BeatType), reader.GetAttribute("BeatType"));
                                    }
                                    if(prop_name == "Orientation")
                                    {
                                        mp.Orientation = (OrientationType)Enum.Parse(typeof(OrientationType), reader.GetAttribute("Orientation"));
                                    }
                                }
                                mark_data.Marks.Add(mp);
                            }
                            if(reader.Name == "Roi:roi")
                            {
                                Roi roi = new Roi();
                                foreach(PropertyInfo prop_info in (roi.GetType().GetProperties()))
                                {
                                    string prop_name = prop_info.Name;
                                    Type prop_type = prop_info.PropertyType;
                                    //if(reader.GetAttribute(prop_name.ToLower()) != null)
                                    //{
                                    //    string ret = reader.GetAttribute(prop_name);
                                    //    prop_info.SetValue(roi, Convert.ChangeType(reader.GetAttribute(prop_name), prop_type), null);
                                    //}
                                    if(prop_name == "X")
                                    {
                                        roi.X = int.Parse(reader.GetAttribute("x"));
                                    }
                                    if(prop_name == "Y")
                                    {
                                        roi.Y = int.Parse(reader.GetAttribute("y"));
                                    }
                                    if(prop_name == "Width")
                                    {
                                        roi.Width = int.Parse(reader.GetAttribute("w"));
                                    }
                                    if(prop_name == "Height")
                                    {
                                        roi.Height = int.Parse(reader.GetAttribute("h"));
                                    }
                                }
                                mark_data.Rois.Add(roi);
                            }

                            #endregion

                            #region Read and Convert Preprocess Phase Data

                            if(reader.Name == "darkness")
                            {
                                preprocess_data.Brightness = reader.GetDoubles();
                                preprocess_output.Status = PhaseStatus.OK;
                            }

                            if(reader.Name == "intensity")
                            {
                                preprocess_data.Intensity = reader.GetDoubles();
                                preprocess_output.Status = PhaseStatus.OK;
                            }

                            #endregion

                            #region Read and Convert Interval Phase Data

                            if(reader.Name == "Interval")
                            {
                                interval_output.Status = (reader.GetAttribute("Use") == "True" ? PhaseStatus.OK : PhaseStatus.Reject);


                                //If the phase was rejected skip settings, unnecessary
                                if(interval_output.Status == PhaseStatus.OK)
                                {
                                    foreach(PropertyInfo prop_info in (interval_settings.GetType().GetProperties()))
                                    {
                                        string prop_name = prop_info.Name;
                                        Type prop_type = prop_info.PropertyType;

                                        //interval_settings.UseBright

                                        if(prop_name == "BrightThresholdHi")
                                        {
                                            interval_settings.BrightThresholdHi = int.Parse(reader.GetAttribute("Darkness_Hi"));
                                        }
                                        if(prop_name == "BrightThresholdLo")
                                        {
                                            interval_settings.BrightThresholdLo = int.Parse(reader.GetAttribute("Darkness_Lo"));
                                        }
                                        if(prop_name == "IntensityMinInterval")
                                        {
                                            interval_settings.IntensityMinInterval = int.Parse(reader.GetAttribute("Intensity_Thresh"));
                                        }
                                        if(prop_name == "UseBright")
                                        {
                                            interval_settings.UseBright = bool.Parse(reader.GetAttribute("UseDarkness"));
                                        }
                                    }

                                }

                                reader.ReadToDescendant("Intervals");
                                interval_data.Intervals = new IntervalCollection(reader.GetIntervals());
                            }

                            if(reader.Name == "Intervals")
                            {
                            }

                            #endregion
                        }
                    }

                    mark_output.Data[0] = mark_data;
                    preprocess_output.Data[0] = preprocess_data;
                    interval_output.Data[0] = interval_data;

                    phase_list[0] = mark_output;
                    phase_list[1] = preprocess_output;
                    phase_list[2] = interval_output;

                    new_filegram.Phases = phase_list;
                    new_filegram.CultureProperties = properties;
                    new_filegram.Video = video;
                    new_filegram.Name = Path.GetFileNameWithoutExtension(new_file);

                    #region Fix Individual Elements

                    video.VideoPath = "";

                    #endregion

                    new_filegram.Serialize(new_filegram);
                    //Filegram.Serialize(new_filegram);
                }
            }
            catch(Exception exc)
            {
                SohaXmlConvertException soha_xml_read_exc = new SohaXmlConvertException("Error converting XML file.", exc);
                soha_xml_read_exc.ReadState = read_state;
                throw soha_xml_read_exc;
            }
        }

    }

    [Serializable]
    public class SohaXmlConvertException : Exception
    {
        public SohaXmlFileReadState ReadState { get; set; }

        public SohaXmlConvertException() { }
        public SohaXmlConvertException(string message) : base(message) { }
        public SohaXmlConvertException(string message, Exception inner) : base(message, inner) { }
        protected SohaXmlConvertException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
