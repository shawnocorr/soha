﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Video;
using SOHA.Library;
using SOHAControls.DisplayControls;

namespace SOHA
{
    public partial class frmMmodeWorkBench : frmPhase
    {
        private Video m_video;

        public frmMmodeWorkBench()
        {
            InitializeComponent();
        }

        public static frmMmodeWorkBench Spawn(Filegram fg)
        {
            return new frmMmodeWorkBench(fg);
        }

        public frmMmodeWorkBench(Filegram fg)
            : base()
        {
            InitializeComponent();
            
            m_filegram = fg;
            m_filegram.Status = FilegramStatus.UserInterface;
            m_video = m_filegram.Video;
            m_video.Open();

            //this.FormClosing += new FormClosingEventHandler(frmAutoPart_FormClosing);

            ucSequentialFilterLayout1.Updated += new EventHandler(ucSequentialFilterLayout1_Updated);
            
            mModeFrameDisplay1.VideoFile = m_video;
            mModeFrameDisplay1.FrameChanged += new EventHandler(frameDisplay1_FrameChanged);
            mModeFrameDisplay1.HistogramUpdated += new EventHandler(frameDisplay1_HistogramUpdated);

            //if(mModeOptions.AutoPreAnalyze)
            //    m_video.PreAnalyze(new ProcessEventArgs(), null);

        }

        void frameDisplay1_HistogramUpdated(object sender, EventArgs e)
        {
            //Temp return, takes too long
            return;

            if(mModeFrameDisplay1.PostOpFrame != null)
            {
                ucHistogram1.SetFrame(mModeFrameDisplay1.PostOpFrame);
            }
            else
            {
                ucHistogram1.SetFrame(mModeFrameDisplay1.CurrentFrame);
            }
        }

        void ucSequentialFilterLayout1_Updated(object sender, EventArgs e)
        {
            mModeFrameDisplay1.Filters = ucSequentialFilterLayout1.GetFilters;
        }

        void frameDisplay1_FrameChanged(object sender, EventArgs e)
        {
            //ucHistogram1.SetFrame(frameDisplay1.PostOpFrame);
            OnUpdateStatus();
        }

        private void OnUpdateStatus()
        {
            statusStrip1.Items.Clear();

            string FRAME_NUMBER = "Frame #" + mModeFrameDisplay1.CurrentFrame.FrameNumber;
            string LAST_FILTER_TIME = " Filter Time [" + mModeFrameDisplay1.LastFilterWatch.ElapsedMilliseconds + " ms]";

            statusStrip1.Items.Add(FRAME_NUMBER + "; " + LAST_FILTER_TIME);
        }

        private void mModeFrameDisplay1_CreateMMode(object sender, SOHAControls.DisplayControls.CreateMmodeEventArgs e)
        {
            try
            {
                //Ensure creation parameters are within range
                if(!e.FullVideo)
                {

                    if(e.Start >= e.End)
                        throw new CreateMmodeException("Can't create Mmode: Start is greater than End.");

                    if(e.Start < 0)
                        throw new CreateMmodeException("Can't create Mmode: Start is less than 0.");

                    switch(e.MmodeType)
                    {
                        case CreateMmodeType.ByTime:

                            if(e.End > m_video.TotalTime)
                                throw new CreateMmodeException("Can't create Mmode: End is beyond total time, select 'Full Video'");

                            break;
                        case CreateMmodeType.ByFrame:

                            if(e.End > m_video.Frames)
                                throw new CreateMmodeException("Can't create Mmode: End is beyond total frames, select 'Full Video'");

                            break;
                        default:
                            break;
                    }
                }

                //All Checks Complete, Continue Firing Create Mmode
                string filename = m_video.Name + "_x=" + mModeFrameDisplay1.XCoord + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");

                SaveFileDialog sveMmode = new SaveFileDialog();
                sveMmode.InitialDirectory = Globals.CurrentDirectory + @"\MModes\";
                sveMmode.AddExtension = true;
                sveMmode.DefaultExt = ".bmp";
                sveMmode.FileName = filename;
                sveMmode.OverwritePrompt = true;

                if(sveMmode.ShowDialog(this.Parent) == System.Windows.Forms.DialogResult.OK)
                {

                    Frame l_frame = (Frame)mModeFrameDisplay1.CurrentFrame.Clone();

                    if(!e.FullVideo)
                    {

                        int start_frame = e.Start;
                        int end_frame = e.End;

                        switch(e.MmodeType)
                        {
                            case CreateMmodeType.ByTime:

                                for(int i = 0; i < m_video.Frames; i++)
                                {
                                    if(start_frame >= m_video.FrameTimes[i])
                                    {
                                        start_frame = i;
                                    }

                                    if(end_frame < m_video.FrameTimes[i])
                                    {
                                        end_frame = i;
                                    }
                                }

                                break;
                            case CreateMmodeType.ByFrame:
                                break;
                            default:
                                break;
                        }

                        Frame.Crop(ref l_frame, new Rectangle(start_frame, 0, end_frame - start_frame, l_frame.Height));
                    }

                    l_frame.Save(sveMmode.FileName);
                }
            }
            catch(Exception exc)
            {
                MessageBox.Show("Error Creating Mmode: " + exc.ToString());
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            mModeFrameDisplay1.EnableFilters = checkBox1.Checked;
        }
    }
}
