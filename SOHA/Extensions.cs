﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using SOHA.Library;
using System.Xml.Serialization;
using System.Security.Principal;
using System.Security.AccessControl;
using System.IO;

namespace SOHA
{
    public static class Extensions
    {

        public static Dictionary<double,double> ToDictionary(this List<double> plist)
        {
            Dictionary<double,double> ret = new Dictionary<double,double>();
            for(int i = 0; i < plist.Count; i++)
                ret.Add(i, plist[i]);

            return ret;
        }

        public static void ProcessAllControls(this Control control, Action<Control> action)
        {
            foreach(Control ctrl in control.Controls)
            {
                ProcessAllControls(ctrl, action);
                action(ctrl);
            }
        }

        public static void ProcessAllControls(this Control control, Action<Control> action, Type type, bool doAction)
        {
            foreach(Control ctrl in control.Controls)
            {
                ProcessAllControls(ctrl, action);
                if(doAction && ctrl.GetType() == type)
                    action(ctrl);

                if(!doAction && ctrl.GetType() != type)
                    action(ctrl);
            }
        }

        public static void ProcessAllControls(this Control control, Action<Control> action, List<Control> exceptions)
        {
            foreach(Control ctrl in control.Controls)
            {
                ProcessAllControls(ctrl, action, exceptions);
                if(!exceptions.Contains(ctrl))
                    action(ctrl);
            }
        }

        public static IEnumerable<Control> GetAllControls(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                .Concat(controls)
                .Where(c => c.GetType() == type);
        }

        public static T Converts<T>(String value)
        {
            if(typeof(T).IsEnum)
                return (T)Enum.Parse(typeof(T), value);

            return (T)Convert.ChangeType(value, typeof(T));
        }

        public static List<double> GetDoubles(this XmlTextReader reader)
        {
            List<double> elements = new List<double>();

            reader.Read();

            while(reader.Read())
            {
                if(reader.NodeType == XmlNodeType.EndElement)//(reader.Name != "Datapoint:darkness")
                {
                    reader.ReadEndElement();
                    break;
                }
                else
                {
                    elements.Add(double.Parse(reader["value"]));
                }
            }

            return elements;
        }

        public static List<Interval> GetIntervals(this XmlTextReader reader)
        {
            List<Interval> elements = new List<Interval>();

            //reader.Read();

            while(reader.Read())
            {

                if(reader.NodeType == XmlNodeType.EndElement)//(reader.Name != "Datapoint:darkness")
                {
                    reader.ReadEndElement();
                    break;
                }
                else if(reader.Name != "Interval:interval")
                {
                    break;
                }
                else
                {
                    Interval interval = new Interval();
                    interval.Frame = int.Parse(reader.GetAttribute("frame"));
                    interval.IntervalType = (IntervalType)Enum.Parse(typeof(IntervalType), reader.GetAttribute("intervaltype"));
                    elements.Add(interval);
                }
            }

            return elements;
        }


        #region Filegram Extension Methods

        public static void Serialize(this Filegram fg, object o)
        {
            try
            {
                //SohaXml.Serialize<Filegram>((Filegram)o);

                Filegram f = (Filegram)o;
                XmlSerializer serializer = new XmlSerializer(typeof(Filegram));
                string path = Globals.CurrentDirectory + "\\" + f.Name + ".xml";
                using(XmlTextWriter writer = new XmlTextWriter(path, Encoding.UTF8))
                {
                    serializer.Serialize(writer, f);
                }

                using(XmlTextReader reader = new XmlTextReader(path))
                {

                }

                NTAccount act_everyone = new NTAccount("Everyone");
                SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);

                FileSecurity sec = File.GetAccessControl(path);
                sec.AddAccessRule(new FileSystemAccessRule(act_everyone, FileSystemRights.FullControl, AccessControlType.Allow));
                File.SetAccessControl(path, sec);
            }
            catch(Exception exc)
            {
                MessageBox.Show("Error Serializing Xml File.\r\n\r\n" + exc.Message);
                Logs.LogException(exc);
            }
        }

        #endregion
    }
}
