﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Video;
using SOHAControls;
using SOHA.Library.Forms;

namespace SOHA
{
    public partial class frmMModeSelect : frmPhase
    {
        private frmProcessNotify frmProgress;
        private Frame m_mmode_display_frame;
        private bool m_collecting_mmode = false;

        private int m_start = 0; int m_end = 0;
        private bool m_save_mmode = false;

        public frmMModeSelect()
        {
            InitializeComponent();
        }

        public frmMModeSelect(Filegram fgram)
        {
            InitializeComponent();

            m_filegram = fgram;
            m_phase_type = PhaseFormType.MModeSelect;

            fgram.Video.Open();
            m_mmode_display_frame = fgram.Video.GetFrameSync(10);
            mModeThumbnail1.Thumbnail = m_mmode_display_frame.ToBitmap();
            fgram.Video.Close();

            mModeDisplay1.Interval.IntervalSize = 500;

            scrollXCoord.Minimum = 1;
            scrollXCoord.Maximum = m_mmode_display_frame.Width;
            scrollXCoord.Scroll += new ScrollEventHandler(scrollXCoord_Scroll);

            mModeThumbnail1.XCoordChanged += new MouseEventHandler(mModeThumbnail1_XCoordChanged);

            m_filegram.Video.MModeComplete += new MModeEventHandler(this.Video_MModeComplete);

            txtTimeFinish.KeyPress += new KeyPressEventHandler(NumbersOnly);
            txtTimeStart.KeyPress += new KeyPressEventHandler(NumbersOnly);
            txtFrameFinish.KeyPress += new KeyPressEventHandler(NumbersOnly);
            txtFrameStart.KeyPress += new KeyPressEventHandler(NumbersOnly);

            btnFrameCreate.Click += new EventHandler(CreateMMode);
            btnTimeCreate.Click += new EventHandler(CreateMMode);

            this.FormClosing += new FormClosingEventHandler(frmMModeSelect_FormClosing);
        }

        void frmMModeSelect_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_filegram.Video.MModeComplete -= Video_MModeComplete;
        }

        void scrollXCoord_Scroll(object sender, ScrollEventArgs e)
        {
            mModeThumbnail1.XCoord = e.NewValue;
            mModeThumbnail1_XCoordChanged(mModeThumbnail1, null);
        }

        void CreateMMode(object sender, EventArgs e)
        {
            m_save_mmode = true;

            if ((sender as Button).Name == "btnFrameCreate")
            {
                m_start = int.Parse(txtFrameStart.Text);
                m_end = int.Parse(txtFrameFinish.Text);
                if (m_start < 1 || m_end > m_filegram.Video.Frames)
                {
                    MessageBox.Show("Invalid Frame Start and/or End.");
                    return;
                }
            }
            else
            {
                m_start = int.Parse(txtTimeStart.Text) * m_filegram.Video.Fps;
                m_end = int.Parse(txtTimeFinish.Text) * m_filegram.Video.Fps;

                if (m_start == 0) m_start = 1;

                if (m_start < 1 || m_end > m_filegram.Video.Frames)
                {
                    MessageBox.Show("Invalid Time Start and/or End.");
                    return;
                }
            }

            m_filegram.Video.GetMMode(mModeThumbnail1.XCoord, m_start, m_end);
            
        }

        void NumbersOnly(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) 
                && !char.IsDigit(e.KeyChar) 
                && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        void Video_MModeComplete(object sender, MMode mmode)
        {
            if (m_collecting_mmode)
            {
                m_collecting_mmode = false;

                if (mmode == null) return;

                mModeDisplay1.MMode = mmode;
            }
            else if (m_save_mmode)
            {
                m_save_mmode = false;

                try
                {
                    Globals.SaveMMode(mmode.ToBitmap(m_start,0, (m_end - m_start),mmode.Height),
                        m_filegram.Name + "x=" + mmode.XCoordinate
                        + "_Length=" + (m_end - m_start) + ".bmp");
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Error saving MMode.");
                }
            }

        }

        void mModeThumbnail1_XCoordChanged(object sender, MouseEventArgs e)
        {
            if (m_collecting_mmode) return;

            m_collecting_mmode = true;
            m_filegram.Video.GetMMode((sender as MModeThumbnail).XCoord,1,500);

            scrollXCoord.Value = (sender as MModeThumbnail).XCoord;
        }

        public static frmMModeSelect Spawn(Filegram fgram)
        {
            return new frmMModeSelect(fgram);
        }

        private void btnFrameCreate_Click(object sender, EventArgs e)
        {

        }

        private void btnTimeCreate_Click(object sender, EventArgs e)
        {

        }
    }
}
