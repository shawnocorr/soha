﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHA
{
    public partial class frmActivation : Form
    {
        public string Code { get; private set; }

        public frmActivation()
        {
            InitializeComponent();

            label1.Text = "You have not yet activated this application.\n\nPlease enter your activation code below:";
        }

        public frmActivation(string MAC)
        {
            InitializeComponent();

            label1.Text = "You have not yet activated this application.\n\nMAC: " + MAC + "\n\nPlease enter your activation code below:";
        }

        private void cmdActivate_Click(object sender, EventArgs e)
        {
            Code = textBox1.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
