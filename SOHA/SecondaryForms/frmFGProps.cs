﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SOHA.SecondaryForms
{
    public partial class frmFGProps : DockContent
    {

        public Filegram SelectedFilegram
        {
            get
            {
                return (propGrid.SelectedObject as Filegram);
            }
            set
            {
                propGrid.SelectedObject = value;
                OnSelectedFilegramChanged();
            }
        }

        public frmFGProps()
        {
            InitializeComponent();
        }

        private void OnSelectedFilegramChanged()
        {
            this.Text = SelectedFilegram.Name;
        }

    }
}
