﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SOHA.SecondaryForms
{
    public partial class frmXMLConvert : Form
    {

        private bool IsFolderSelected { get; set; }

        public frmXMLConvert()
        {
            InitializeComponent();
            textBox1.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                IsFolderSelected = true;
                textBox1.Text = folderBrowserDialog1.SelectedPath;
                AnalyzePath(folderBrowserDialog1.SelectedPath);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if(IsFolderSelected)
            {
                //Convert

                //Iterate all XML Files and convert from OHBAFile to New XML File

                string old_directory = Globals.CurrentDirectory;
                Globals.CurrentDirectory = folderBrowserDialog1.SelectedPath;
                var xml_files = Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*?.xml", SearchOption.TopDirectoryOnly);
                foreach(string path in xml_files)
                {
                    SohaXmlFileType type = SohaXmlConvert.FilegramType(path);

                    switch(type)
                    {
                        case SohaXmlFileType.NA:
                            break;
                        case SohaXmlFileType.OHBA:
                            string new_path = Path.GetDirectoryName(path) + @"\" + Path.GetFileNameWithoutExtension(path) + @"_new.xml";
                            string backup_path = Path.GetDirectoryName(path) + @"\" + Path.GetFileNameWithoutExtension(path) + @".oldxml";
                            SohaXmlConvert.ConvertOHBAFile(path, new_path);
                            File.Replace(new_path, path, backup_path);
                            break;
                        case SohaXmlFileType.Filegram:
                            break;
                        case SohaXmlFileType.Filegram_v3p5:
                            break;
                        default:
                            break;
                    }
                }
                Globals.CurrentDirectory = old_directory;
            }
            Cursor.Current = Cursors.Default;
        }

        private void AnalyzePath(string path)
        {
            Dictionary<SohaXmlFileType, int> files_analyzed = new Dictionary<SohaXmlFileType, int>();
            var files_to_analyze = Directory.GetFiles(path, "*.xml", SearchOption.TopDirectoryOnly);

            lblAnalysis.Text = files_to_analyze.Count() + " files found\n";

            //Count SohaXmlFileType
            foreach(string strFile in files_to_analyze )
            {
                //files_analyzed.Add(strFile, SohaXmlConvert.FilegramType(strFile));
                SohaXmlFileType type = SohaXmlConvert.FilegramType(strFile);
                if(!files_analyzed.ContainsKey(type))
                {
                    files_analyzed.Add(type, 1);
                }
                else
                {
                    files_analyzed[type]++;
                }
            }

            foreach(KeyValuePair<SohaXmlFileType, int> type in files_analyzed)
            {
                lblAnalysis.Text += type.Value + " " + type.Key.ToString() + " files\n";
            }
        }
    }
}
