﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHAControls;
using WeifenLuo.WinFormsUI.Docking;
using Microsoft.Win32;
using System.IO;
using SOHA.SecondaryForms;
using SOHA.Library;
using System.Diagnostics;
using SOHA.Library.Xml;
using System.Collections;

namespace SOHA
{
    public partial class frmMain : Form
    {
        public static FilegramCollection FilegramList = new FilegramCollection();

        frmAssay dockAssay;
        frmFGProps propGrid;

        public frmOutput output { get; private set; }

        public frmMain()
        {
            InitializeComponent();

            Globals.MainInstance = this;
            this.FormClosing += new FormClosingEventHandler(frmMain_FormClosing);

            output = new frmOutput();
            //output.Show(dockPanel1, DockState.DockBottom);


            dockAssay = new frmAssay();
            dockAssay.Show(dockPanel1, DockState.DockLeft);
            dockAssay.SelectedFilegramChanged += new EventHandler(dockAssay_SelectedFilegramChanged);
            dockAssay.DockChanged += new EventHandler(dockAssay_DockChanged);
            dockAssay.DockStateChanged += new EventHandler(dockAssay_DockStateChanged);
            
            propGrid = new frmFGProps();
            propGrid.Show(dockPanel1.Panes[0], DockAlignment.Bottom,0.5);

            this.WindowState = FormWindowState.Maximized;
        }

        void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Globals.Options.WriteFile();
        }

        void dockAssay_SelectedFilegramChanged(object sender, EventArgs e)
        {
            propGrid.SelectedFilegram = dockAssay.SelectedFilegram;
        }

        public void AddDocument(DockContent frm)
        {
            frm.Show(dockPanel1, DockState.Document);
        }

        void dockAssay_DockStateChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void dockAssay_DockChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void analyzeDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.SelectedPath = ReadRegistry();
            folderBrowser.ShowNewFolderButton = false;
            if (folderBrowser.ShowDialog(this) == DialogResult.Cancel)
                return;

            string path = folderBrowser.SelectedPath;

            if (Directory.Exists(path))
            {
                Globals.CurrentDirectory = path;
                Globals.XmlArchiveDirectory = path + @"\XMLArchive\";
                FilegramList = new FilegramCollection(path);
                FilegramCollection.LoadComplete += new EventHandler(FilegramCollection_LoadComplete);
                WriteRegistry(path);
                //Globals.AddNewAssay(path);
            }
        }
        
        void FilegramCollection_LoadComplete(object sender, EventArgs e)
        {
            //new_assay.Initialize();
            this.Invoke(((MethodInvoker)delegate { dockAssay.Initialize(); }));
        }

        private void WriteRegistry(string path)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey("OpticalHeartBeatAnalysis");
            key.SetValue("Folder", path);
        }

        private string ReadRegistry()
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey("OpticalHeartBeatAnalysis");
            return (string)key.GetValue("Folder");
        }

        #region Menu Selection Handling

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            frmGlobalOptions frmOptions = new frmGlobalOptions();
            frmOptions.Show(this);
        }

        private void camerasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCameraSelect form_cams = new frmCameraSelect();
            form_cams.ShowDialog(this);
        }

        private void xMLConverterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmXMLConvert frmXMLConvert = new frmXMLConvert();
            frmXMLConvert.ShowDialog(this);
        }

        private void writeCSVFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Globals.FilegramList.WriteCsv();
            }
            catch(IOException ioexc)
            {
                MessageBox.Show("File is already opened. You must close the file and re-output.");
            }
            catch(Win32Exception win_exc)
            {
                MessageBox.Show("Could not open CSV. Please associate (default open) CSV files with a program.");
            }
            catch(SohaException soha_exc)
            {
                MessageBox.Show("There are no files present to output.");
            }
        }

        private void GetDiscreteIntervals()
        {            

            Dictionary<string, Dictionary<int, List<Filegram>>> l_geno_age_filegram = new Dictionary<string, Dictionary<int, List<Filegram>>>();
            //Brute Force Dictionary Creation
            try
            {
                foreach(Filegram fg in Globals.FilegramList)
                {
                    string geno = fg.CultureProperties.Genotype.ToUpper();
                    int age = fg.CultureProperties.Age;

                    if(!l_geno_age_filegram.ContainsKey(geno))
                        l_geno_age_filegram.Add(geno, new Dictionary<int, List<Filegram>>());
                    if(!l_geno_age_filegram[geno].ContainsKey(age))
                        l_geno_age_filegram[geno].Add(age, new List<Filegram>());

                    l_geno_age_filegram[geno][age].Add(fg);
                }
            }
            catch(Exception exc)
            {
                
            }

            //Output => File per Genotype

            foreach(KeyValuePair<string, Dictionary<int, List<Filegram>>> kvp in l_geno_age_filegram)
            {
                //Genotype Loop
                List<object> col1 = new List<object>();
                List<object> col2 = new List<object>();
                List<object> col3 = new List<object>();

                string genofile = Globals.CurrentDirectory + "\\interval_output_" + kvp.Key + ".csv";

                ArrayList out_data = new ArrayList();
                object[,] data = null;
                foreach(KeyValuePair<int, List<Filegram>> kvp2 in kvp.Value)
                {
                    //Age Loop
                    genofile = Globals.CurrentDirectory + "\\interval_output_" + kvp.Key + "_Age" + kvp2.Key + ".csv";

                    List<double> systoles = new List<double>();
                    List<double> diastoles = new List<double>();
                    List<double> periods = new List<double>();

                    foreach(Filegram fg in kvp2.Value)
                    {
                        List<Dictionary<string, List<double>>> stats = fg.GetIntervalsPeriods();

                        foreach(Dictionary<string,List<double>> stat_kvp in stats)
                        {
                            foreach(double d in stat_kvp["systoles"])
                                systoles.Add(d);

                            foreach(double d in stat_kvp["diastoles"])
                                diastoles.Add(d);

                            foreach(double d in stat_kvp["periods"])
                                periods.Add(d);
                        }
                    }

                    //Columns to arrays
                    int rows = systoles.Count > diastoles.Count ? systoles.Count : diastoles.Count;
                    if(systoles.Count > diastoles.Count)
                    {
                        while(diastoles.Count < systoles.Count)
                        {
                            diastoles.Add(0);
                            periods.Add(0);
                        }
                    }
                    else if(diastoles.Count > systoles.Count)
                    {
                        while(systoles.Count < diastoles.Count)
                        {
                            systoles.Add(0);
                            periods.Add(0);
                        }
                    }

                    int j = 0;
                    data = new object[rows + 2, 3];
                    data[0,0] = "";
                    data[0,1] = "Age: " + kvp2.Key;
                    data[0,2] = "";

                    data[1,0] = "Systoles";
                    data[1,1] = "Diastoles";
                    data[1,2] = "Periods";

                    for(j = 0; j < systoles.Count; j++)
                    {
                        data[j + 2, 0] = systoles[j];
                    }

                    for(j = 0; j < diastoles.Count; j++)
                    {
                        data[j + 2, 1] = diastoles[j];
                    }

                    for(j = 0; j < periods.Count; j++)
                    {
                        data[j + 2, 2] = periods[j];
                    }

                    //out_data.Add(data);
                    //End of Age, 1 line spacing

                    //col1.Add("");
                    //col2.Add("");
                    //col3.Add("");

                    using(StreamWriter writer = new StreamWriter(genofile))
                    {
                        for(int i = 0; i < data.GetLength(0); i++)
                        {
                            for(j = 0; j < 3; j++)
                            {
                                writer.Write(data[i, j] + ",");
                            }
                            writer.WriteLine();
                        }
                        writer.Close();
                    }

                    Process.Start(genofile);
                }
                
            }

            //var genotypes = Globals.FilegramList.GroupBy(x => x.CultureProperties.Genotype.ToUpper(), x => x.CultureProperties.Age);

            //Dictionary<string, Filegram> l_geno_groups = Globals.FilegramList.GroupBy(x => x.CultureProperties.Genotype.ToUpper())
            //    .ToDictionary(x => x.CultureProperties.Genotype.ToUpper(), x => x.Select());
        }

        private void writeDiscreteIntervalsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            GetDiscreteIntervals();
            return;

            int i = 0;
            int r = 0;
            int j = 0;
            try
            {

                var genotypes = Globals.FilegramList.GroupBy(x => x.CultureProperties.Genotype.ToUpper());

                foreach (IGrouping<string, Filegram> geno_group in genotypes)
                {
                    var ages = geno_group.GroupBy(x => x.CultureProperties.Age);

                    ArrayList out_data = new ArrayList();
                    foreach (IGrouping<int, Filegram> age_group in ages)
                    {
                        List<object> systoles = new List<object>();
                        List<object> diastoles = new List<object>();
                        List<object> periods = new List<object>();

                        systoles.Add("");
                        diastoles.Add("Age: " + age_group.Key.ToString());
                        periods.Add("");

                        systoles.Add("Systoles");
                        diastoles.Add("Diastoles");
                        periods.Add("Periods");

                        foreach (Filegram f in age_group)
                        {
                            foreach (PhaseData intervaldata in f.Phases[2].Data)
                            {
                                IntervalCollection intervals = (intervaldata as IntervalPhaseData).Intervals;
                                if (intervals.Count == 0)
                                    break;

                                double[] frame_times = f.Video.FrameTimes;

                                IEnumerable<int> SystolicIntervals =
                                from Interval interval in intervals
                                where (interval.IntervalType == IntervalType.Systole)
                                select interval.Frame;

                                foreach (int a in SystolicIntervals)
                                {
                                    //Get Next Diastole
                                    var next_diastole = (from Interval interval in intervals
                                                         where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                                                         select interval.Frame);
                                    if (next_diastole.Count() == 0)
                                        break;

                                    int b = next_diastole.First();

                                    if (b > 0)
                                        systoles.Add(frame_times[b] - frame_times[a]);


                                    //Get Next Systole
                                    //The last systolic interval has no corresponding diastolic interval, so exit
                                    var next_systole = (from Interval interval in intervals
                                                        where (interval.IntervalType == IntervalType.Systole && interval.Frame > a)
                                                        select interval.Frame);
                                    if (next_systole.Count() == 0)
                                        break;

                                    int a1 = next_systole.First();

                                    if (a1 > 0)
                                    {
                                        diastoles.Add(frame_times[a1] - frame_times[b]);
                                        periods.Add(frame_times[a1] - frame_times[a]);
                                    }
                                }
                            }
                        }

                        //Find longest dataset
                        int rows = systoles.Count > diastoles.Count ? systoles.Count : diastoles.Count;

                        #region Fill 2D object matrix

                        object[,] data = new object[rows, 3];
                        for (j = 0; j < systoles.Count; j++)
                        {
                            data[j, 2] = systoles[j];
                        }

                        for (j = 0; j < diastoles.Count; j++)
                        {
                            data[j, 1] = diastoles[j];
                        }

                        for (j = 0; j < periods.Count; j++)
                        {
                            data[j, 0] = periods[j];
                        }

                        out_data.Add(data);

                        #endregion
                    }

                    #region Write Age File

                    i = 0;
                    string file = Globals.CurrentDirectory + "\\interval_output_" + geno_group.Key + ".csv";
                    using (StreamWriter writer = new StreamWriter(file))
                    {
                        r = 0;
                        bool done = false;
                        while (!done)
                        {
                            for (i = 0; i < out_data.Count; i++)
                            {
                                for (j = 0; j < (out_data[i] as object[,]).GetLength(1); j++)
                                {
                                    writer.Write((out_data[i] as object[,])[r, j] + ",");
                                }
                            }
                            writer.WriteLine();
                            r++;

                            done = true;
                            for (i = 0; i < out_data.Count; i++)
                            {
                                if (r < (out_data[i] as object[,]).GetLength(0))
                                {
                                    done = false;
                                    break;
                                }
                            }
                        }
                    }

                    #endregion

                    Process.Start(file);
                }
            }
            catch(Exception exc)
            {
                MessageBox.Show(this, "i = " + i + "; r = " + r + "; j = " + j + "\r\n\r\n" + exc.ToString());
                Logs.LogException(exc);
            }
        }

        private void writeFileForGraphingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(Globals.FilegramList.Count <= 0)
            {
                MessageBox.Show("There are no files present to output.");
                return;
            }

            FolderBrowserDialog folder_browse = new FolderBrowserDialog();
            if(folder_browse.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;

            string folder = folder_browse.SelectedPath;

            var genotypes = Globals.FilegramList.GroupBy(x => x.CultureProperties.Genotype.ToUpper());
            foreach(IGrouping<string, Filegram> geno_group in genotypes)
            {
                List<object[]> output_data = new List<object[]>();
                output_data.Add(FilegramCollection.CreateHeader());
                foreach(Filegram f in geno_group)
                {
                    List<object[]> objs = f.ToDataRow();
                    foreach(object[] o_array in objs)
                        output_data.Add(o_array);
                }

                string file = folder + "\\" + geno_group.Key + ".csv";

                using(StreamWriter writer = new StreamWriter(file))
                {
                    for(int i = 0; i < output_data.Count; i++)
                    {
                        for(int j = 0; j < (output_data[i] as object[]).Length; j++)
                        {
                            writer.Write((output_data[i] as object[])[j] + ",");
                        }
                        writer.WriteLine();
                    }
                    writer.Close();
                }
            }
        }
        
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout about = new frmAbout();
            about.ShowDialog(this);
        }

        private void supportBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://sohasoftware.com/support/report-bugs/");
        }

        private void recentIssuesAndResolutionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://sohasoftware.com/SOHA-known-issues.html");
        }

        private void createLogFileZipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Globals.CreateLogFileZip();
        }

        #endregion

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string xml_file = @"C:\test_layout.xml";
            dockPanel1.SaveAsXml(xml_file);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string xml_file = @"C:\test_layout.xml";
        }

    }
}
