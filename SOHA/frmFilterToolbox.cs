﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library.GPU;
using SOHAControls.FilterControls;
using SOHA.Library.Imaging;

namespace SOHA
{
    public partial class frmFilterToolbox : Form
    {

        public event EventHandler Update;

        private List<CLFilter> m_filters = new List<CLFilter>();

        public frmFilterToolbox()
        {
            InitializeComponent();
            this.ProcessAllControls(ctrl => ctrl.MouseHover += new EventHandler(ShowToolTip),typeof(BFilterControl), true);
            flwPanelFilters.FilterUpdate += new EventHandler(flwPanelFilters_FilterUpdate);
        }

        public frmFilterToolbox(List<CLFilter> filters)
        {
            InitializeComponent();

            this.ProcessAllControls(ctrl => ctrl.MouseHover += new EventHandler(ShowToolTip), typeof(BFilterControl), true);
            flwPanelFilters.FilterUpdate += new EventHandler(flwPanelFilters_FilterUpdate);

            m_filters = filters;
        }

        void flwPanelFilters_FilterUpdate(object sender, EventArgs e)
        {
            OnUpdate(e);
        }

        void ShowToolTip(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void FilterDragDrop(object sender, DragEventArgs e)
        {
            ImageFilter filter = (ImageFilter)e.Data.GetData(DataFormats.Serializable);
            m_filters.Add((CLFilter)filter);
            ucFilterDisplay display = new ucFilterDisplay(filter);
            display.Width = flwPanelFilters.Width - display.Margin.Left - display.Margin.Right;
            display.Height = 100;
            display.Tag = flwPanelFilters.Controls.Count + 1;
            display.Text = filter.ToString();

            flwPanelFilters.Controls.Add(display);
        }

        void FilterDragEnter(object sender, DragEventArgs e)
        {
            if(e.Data.GetDataPresent(DataFormats.Serializable))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        protected void OnUpdate(EventArgs e)
        {
            if(Update != null)
                Update(this, e);
        }

        public List<IFrameFunction> GetAllFilters()
        {
            return flwPanelFilters.GetFilters();
            //List<CLFilter> filters = new List<CLFilter>();
            //foreach(ucFilterDisplay filterDisplay in flwPanelFilters.Controls)
            //{
            //    filters.Add((CLFilter)filterDisplay.Filter);
            //}
            //return filters;
        }

        public void SetAllFilters(List<IFrameFunction> filters)
        {
            flwPanelFilters.SetFilters(filters);
        }

        public IEnumerable<Control> GetAllControls(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                .Concat(controls)
                .Where(c => c.GetType() == type);
        }
    }
}
