﻿namespace SOHA
{
    partial class frmFilterToolbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpFilters = new System.Windows.Forms.GroupBox();
            this.flwPanelFilters = new SOHAControls.FilterDisplayPanel(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ucSobel1 = new SOHAControls.FilterControls.ucSobel();
            this.ucGaussianSmooth = new SOHAControls.FilterControls.ucGaussianSmooth();
            this.grpFilters.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpFilters
            // 
            this.grpFilters.Controls.Add(this.flwPanelFilters);
            this.grpFilters.Location = new System.Drawing.Point(404, 12);
            this.grpFilters.Name = "grpFilters";
            this.grpFilters.Size = new System.Drawing.Size(376, 732);
            this.grpFilters.TabIndex = 1;
            this.grpFilters.TabStop = false;
            this.grpFilters.Text = "Filters (Sequential)";
            // 
            // flwPanelFilters
            // 
            this.flwPanelFilters.AllowDrop = true;
            this.flwPanelFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flwPanelFilters.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flwPanelFilters.Location = new System.Drawing.Point(3, 16);
            this.flwPanelFilters.Name = "flwPanelFilters";
            this.flwPanelFilters.Size = new System.Drawing.Size(370, 713);
            this.flwPanelFilters.TabIndex = 0;
            // 
            // ucSobel1
            // 
            this.ucSobel1.Location = new System.Drawing.Point(12, 89);
            this.ucSobel1.Name = "ucSobel1";
            this.ucSobel1.Size = new System.Drawing.Size(220, 70);
            this.ucSobel1.TabIndex = 5;
            // 
            // ucGaussianSmooth
            // 
            this.ucGaussianSmooth.AllowDrop = true;
            this.ucGaussianSmooth.Location = new System.Drawing.Point(12, 12);
            this.ucGaussianSmooth.Name = "ucGaussianSmooth";
            this.ucGaussianSmooth.Size = new System.Drawing.Size(221, 71);
            this.ucGaussianSmooth.TabIndex = 4;
            // 
            // frmFilterToolbox
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 742);
            this.Controls.Add(this.ucSobel1);
            this.Controls.Add(this.ucGaussianSmooth);
            this.Controls.Add(this.grpFilters);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFilterToolbox";
            this.Text = "Filter Toolbox";
            this.grpFilters.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpFilters;
        private SOHAControls.FilterControls.ucGaussianSmooth ucGaussianSmooth1;
        private SOHAControls.FilterControls.ucGaussianSmooth ucGaussianSmooth;
        private SOHAControls.FilterDisplayPanel flwPanelFilters;
        private System.Windows.Forms.ToolTip toolTip1;
        private SOHAControls.FilterControls.ucSobel ucSobel1;

    }
}