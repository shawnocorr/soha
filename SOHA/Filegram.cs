﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using SOHA.Library.Video;
using SOHA.Library;
using SOHA.Library.Mathematics;

using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Reflection;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Threading;
using SOHA.Library.Xml;
using SOHA.Library.GPU;
using SOHA.Library.Forms;

namespace SOHA.Library.Forms
{

    public delegate void FilegramLoadEventHandler(Filegram fgram, EventArgs e);

    public enum FilegramStatus
    {
        Idle = 0,
        UserInterface,
        Processing,
        Queued,
        Error
    }

    public class Filegram : Processable, IXmlSerializable
    {
        private const int VERSION = 3;
        public const int EXCEL_ROW_LENGTH = 107;

        public List<CLFilter> Filters = new List<CLFilter>();

        private FilegramStatus m_status;
        private string m_name;
        private string m_load_file;
        private string m_xml_path;
        private int m_progress;
        private Task m_task_preprocess;
        //private string m_video_path;

        private IPhase m_current_form;

        [Browsable(false)]
        public FilegramStatus Status
        {
            get
            {
                return m_status;
            }
            set
            {
                m_status = value;
                OnStatusChanged();
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        [XmlAttribute("XML_Version")]
        public string XML_Version
        {
            get
            {
                return VERSION.ToString();
                //return Globals.XML_VERSION.ToString();
            }
        }

        private CultureProperties m_culture_properties;
        [XmlElement("CultureProperties")]
        [Browsable(false)]
        [ReadOnly(true)]
        public CultureProperties CultureProperties
        {
            get
            {
                return m_culture_properties;
            }
            set
            {
                m_culture_properties = value;
            }
        }

        private Video m_video;
        [Browsable(true)]
        [ReadOnly(true)]
        public Video Video
        {
            get
            {
                return m_video;
            }
            set
            {
                m_video = value;
            }
        }

        private PhaseOutputList m_phases = new PhaseOutputList();
        [XmlElement("Phases")]
        [Browsable(true)]
        [ReadOnly(true)]
        public PhaseOutputList Phases
        {
            get
            {
                return m_phases;
            }
            set
            {
                m_phases = value;
            }
        }

        public event EventHandler StatusChanged;
        public static event FilegramLoadEventHandler LoadComplete;

        public Filegram() { }

        public Filegram(string filename)
        {
            m_load_file = filename;
            m_name = Path.GetFileNameWithoutExtension(filename);
            m_culture_properties = new CultureProperties(m_name);
        }

        public void SetProgress(int new_progress)
        {
            m_progress = new_progress;
            OnStatusChanged();
        }

        private void OnStatusChanged()
        {
            Logs.LogTrace(this.Name + " status changed to " + this.Status);
            if (StatusChanged != null)
                StatusChanged(this, new EventArgs());
        }

        private static readonly object syncObject = new object();
        public static void FromFilepath(string file)
        {
            lock (syncObject)
            {
                Logs.LogTrace("Loading " + file);

                Filegram fgram = new Filegram(file);
                string extension = Path.GetExtension(file);
                extension = extension.Substring(1);

                try
                {
                    switch (extension.ToUpper())
                    {
                        case "XML":
                            fgram = Filegram.Deserialize(file);
                            break;

                        case "CXD":
                            fgram.Video = new CXDVideo(file);
                            //Filegram.Serialize(fgram);
                            break;

                        case "AVI":
                            fgram.Video = new AVIVideo(file);
                            //Filegram.Serialize(fgram);
                            break;
                    }

                    fgram.Status = FilegramStatus.Idle;
                }
                catch (Exception exc)
                {
                    fgram.Status = FilegramStatus.Error;
                }
                finally
                {
                    fgram.Name = Path.GetFileNameWithoutExtension(file);
                }

                //return fgram;
                if (LoadComplete != null)
                    LoadComplete(fgram, new EventArgs());
            }
        }

        public void ViewPhase(PhaseFormType p_phase)
        {
            if (Status == FilegramStatus.Idle)
            {

                if(p_phase == PhaseFormType.Properties)
                {
                    frmPhase.SpawnForm(this, PhaseFormType.Properties);
                    return;
                }

                try
                {
                    m_current_form = frmPhase.SpawnForm(this, p_phase);
                    m_current_form.PhaseComplete += new PhaseCompleteEventHandler(m_current_form_Complete);
                    (m_current_form as frmPhase).MdiParent = Globals.MainInstance;
                    (m_current_form as frmPhase).Text = this.Name;
                    (m_current_form as frmPhase).Show();
                }
                catch (PhaseInitiationException phase_exc)
                {
                    MessageBox.Show(Globals.MainInstance, phase_exc.Message, "Error Opening Phase - " + this.Name, MessageBoxButtons.OK);
                    Status = FilegramStatus.Idle;
                    m_current_form = null;
                }
                catch (Exception exc)
                {
                    StringBuilder error_msg = new StringBuilder();
                    error_msg.AppendLine(Enum.GetName(typeof(PhaseFormType), p_phase));
                    error_msg.AppendLine(exc.ToString());

                    MessageBox.Show(Globals.MainInstance, error_msg.ToString(), "Error Opening Phase");
                    Status = FilegramStatus.Idle;
                    m_current_form = null;
                }
            }
        }

        //NEEDS TO GO UP A LEVEL 7/31/2016
        void m_current_form_Complete(object sender, PhaseCompleteEventArgs e)
        {
            if (m_current_form != null)
            {
                m_current_form.PhaseComplete -= new PhaseCompleteEventHandler(m_current_form_Complete);
                (m_current_form as frmPhase).Dispose();

                if (e.Type == PhaseFormType.Mark && Options.AutoProcess)
                {
                    if ((Phases[1] as PreprocessPhaseOutput).Status == PhaseStatus.NA && (Phases[0] as MarkPhaseOutput).Status == PhaseStatus.OK)
                    {
                        this.Preprocess(true);
                        //Globals.PreprocessQueue.Enqueue(this.Video);
                        //Globals.Queue.Enqueue(this);
                    }
                    else
                    {
                        /* Created Problems; re-processed files would not start and errors in CXD.PreProcess() came up 3/27/2013*/
                        //if (MessageBox.Show(Globals.MainInstance, "Would you like to re-process this file?\r\n[" + Name + "]", "Re-Process",MessageBoxButtons.YesNo) == DialogResult.Yes)
                        //{
                        //    Globals.Queue.Enqueue(this);
                        //}
                    }
                }

                if((e.Type == PhaseFormType.Mark || e.Type == PhaseFormType.Interval || e.Type == PhaseFormType.Phase) 
                    && Options.AutoGUI)
                {
                    int i = Globals.FilegramList.IndexOf(this);

                    if (i < Globals.FilegramList.Count - 1)
                    {
                        Globals.FilegramList[i + 1].ViewPhase(e.Type);
                    }
                    else
                    {
                        //MessageBox.Show(Globals.MainInstance, "End of file list.", "End of List");
                    }
                }
            }

            if(Status != FilegramStatus.Processing && Status != FilegramStatus.Queued)
                Status = FilegramStatus.Idle;

            if(e.Accept)
                Filegram.Serialize(this);
        }

        public delegate List<object[]> DataDelegate();
        public List<object[]> ToDataRow()
        {
            List<object[]> data = new List<object[]>();

            for (int i = 0; i < (Phases[1].Data as List<PhaseData>).Count; i++)
            {
                data.Add(ToRowData(i));
            }

            return data;
        }

        public object[] ToRowData(int n)
        {

            object[] ret = new object[EXCEL_ROW_LENGTH];

            try
            {
                double diastole_max = 1.0;
                double systole_max = 0.35;
                double systole_min = 0.06;

                double[] m_diastoles = new double[1], m_systoles = new double[1], m_diastoles_hor = new double[1], m_systoles_hor = new double[1], m_intervals_diastoles = new double[1],
                    m_intervals_systoles = new double[1], m_intervals_heartrate = new double[1], m_intervals_heartperiod = new double[1], m_intervals_contract = new double[1],
                    m_intervals_relax = new double[1], m_intervals_isotonic = new double[1], m_intervals_max = new double[1], m_intervals_normalized = new double[1];
                double m_intervals_systoles_fibrillations = 0, m_intervals_diastoles_long = 0, m_intervals_systoles_long = 0;

                IntervalType first_beat = IntervalType.Diastole;

                DateTime begin = DateTime.Now;

                List<double> systolic_intervals = new List<double>();
                List<double> diastolic_intervals = new List<double>();
                List<double> heart_periods = new List<double>();
                List<double> heart_rate = new List<double>();
                List<double> norm_intervals = new List<double>();
                List<double> contraction_intervals = new List<double>();
                List<double> relaxation_intervals = new List<double>();
                List<double> isotonic_intervals = new List<double>();
                List<double> maxvelocity_intervals = new List<double>();

                try
                {

                    #region Parse Data

                    #region PostProcessMarkPhase
                    
                    BindingList<MarkPair> l_marklist = (m_phases[0].Data[0] as MarkPhaseData).Marks;//m_marklist;

                    if (l_marklist.Count > 0)
                    {

                        List<double> l_systole = new List<double>();
                        List<double> l_diastole = new List<double>();
                        List<double> l_systole_hor = new List<double>();
                        List<double> l_diastole_hor = new List<double>();

                        foreach (MarkPair l_markpair in l_marklist)
                        {
                            double l_measurement = l_markpair.Euclidean();

                            //Scaling Factor
                            l_measurement *= this.Video.Camera.ScaleFactor;

                            switch (l_markpair.Beat)
                            {
                                case BeatType.Systole:
                                    l_systole.Add(l_measurement);
                                    break;
                                case BeatType.Diastole:
                                    l_diastole.Add(l_measurement);
                                    break;
                                //case BeatType.XSystole:
                                //    l_systole_hor.Add(l_measurement);
                                //    break;
                                //case BeatType.XDiastole:
                                //    l_diastole_hor.Add(l_measurement);
                                //    break;
                                //case BeatType.RegionOfInterest:
                                //    //this.Video.Roi = new Rectangle(l_markpair.Mark1.X, l_markpair.Mark1.Y, Math.Abs(l_markpair.Mark2.X - l_markpair.Mark1.X), Math.Abs(l_markpair.Mark2.Y - l_markpair.Mark1.Y));
                                //    break;
                            }
                        }

                        m_diastoles = l_diastole.ToArray();
                        m_systoles = l_systole.ToArray();
                        m_systoles_hor = l_systole_hor.ToArray();
                        m_diastoles_hor = l_diastole_hor.ToArray();
                    }
                    #endregion

                    
                    if (m_phases[2].Status == PhaseStatus.OK)
                    {
                        try
                        {
                            #region PostProcessIntervalPhase

                            IntervalCollection intervals = (IntervalCollection)(m_phases[2].Data[n] as IntervalPhaseData).Intervals.Clone();
                            double[] frame_times = Video.FrameTimes;

                            if(!intervals.IsSorted())
                                intervals.Sort();

                            #region Systolic Intervals

                            IEnumerable<int> SystolicIntervals =
                                from Interval interval in intervals
                                where (interval.IntervalType == IntervalType.Systole)
                                select interval.Frame;

                            foreach (int a in SystolicIntervals)
                            {
                                //Get Next Diastole
                                int b = 0;
                                try
                                {
                                    b =
                                        (from Interval interval in intervals
                                         where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                                         select interval.Frame).First();

                                    systolic_intervals.Add(frame_times[b - 1] - frame_times[a - 1]);
                                }
                                catch (Exception exc)
                                {

                                }


                                //Get Next Systole
                                try
                                {
                                    int a1 =
                                        (from Interval interval in intervals
                                         where (interval.IntervalType == IntervalType.Systole && interval.Frame > a)
                                         select interval.Frame).First();

                                    //diastolic_intervals.Add(frame_times[a1] - frame_times[b]);
                                    heart_periods.Add(frame_times[a1 - 1] - frame_times[a - 1]);
                                    heart_rate.Add(1 / (frame_times[a1 - 1] - frame_times[a - 1]));
                                }
                                catch (Exception exc)
                                {

                                }
                            }

                            #endregion

                            IEnumerable<int> DiastolicIntervals =
                                 from Interval interval in intervals
                                 where (interval.IntervalType == IntervalType.Diastole)
                                 select interval.Frame;

                            foreach (int a in DiastolicIntervals)
                            {
                                int b = 0;
                                try
                                {
                                    b =
                                        (from Interval interval in intervals
                                         where (interval.IntervalType == IntervalType.Systole && interval.Frame > a)
                                         select interval.Frame).First();

                                    diastolic_intervals.Add(frame_times[b - 1] - frame_times[a - 1]);
                                }
                                catch (Exception exc)
                                {

                                }
                            }

                            int offset = (intervals[0].IntervalType == IntervalType.Diastole ? 0 : 1);
                            for (int i = offset; i < systolic_intervals.Count; i++)
                            {
                                try
                                {
                                    norm_intervals.Add(systolic_intervals[i] / diastolic_intervals[i - offset]);
                                }
                                catch (IndexOutOfRangeException ioor_exc)
                                {

                                }
                            }

                            #endregion
                        }
                        catch (Exception exc)
                        {
                            Logs.LogException(exc);
                        }
                    }
                    else
                    {
                        //Globals.LogTrace(Name + ": No Interval Data Present");
                    }
                    

                    if (m_phases[3].Status == PhaseStatus.OK)
                    {
                        try
                        {
                            int NumberOfIntervalsToRecord = 5;

                            #region PostProcessPhase

                            IntervalCollection intervals = (m_phases[3].Data[n] as PhasePhaseData).Intervals;
                            double[] frame_times = Video.FrameTimes;

                            IEnumerable<int> SystolicIntervals =
                                (from Interval interval in intervals
                                where (interval.IntervalType == IntervalType.Systole)
                                select interval.Frame).Take(NumberOfIntervalsToRecord);

                            foreach (int a in SystolicIntervals)
                            {
                                int b =
                                    (from Interval interval in intervals
                                    where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                                    select interval.Frame).First();

                                IEnumerable<int> MaxVelocities =
                                    from Interval interval in intervals
                                    where (interval.IntervalType == IntervalType.MaxVelocity && interval.Frame > a && interval.Frame < b)
                                    select interval.Frame;

                                foreach (int i in MaxVelocities)
                                    maxvelocity_intervals.Add(frame_times[i] - frame_times[a]);


                                IEnumerable<int> PhaseChanges =
                                    from Interval interval in intervals
                                    where (interval.IntervalType == IntervalType.Change && interval.Frame > a && interval.Frame < b)
                                    select interval.Frame;

                                switch (PhaseChanges.Count())
                                {
                                    case 1:
                                        
                                        // Add Contraction and Relaxation

                                        contraction_intervals.Add(frame_times[PhaseChanges.ElementAt(0)] - frame_times[a]);
                                        relaxation_intervals.Add(frame_times[b] - frame_times[PhaseChanges.ElementAt(0)]);

                                        break;

                                    case 2:
                                        
                                        // Add Contraction, Isotonic and Relaxation

                                        contraction_intervals.Add(frame_times[PhaseChanges.ElementAt(0)] - frame_times[a]);
                                        isotonic_intervals.Add(frame_times[PhaseChanges.ElementAt(1)] - frame_times[PhaseChanges.ElementAt(0)]);
                                        relaxation_intervals.Add(frame_times[b] - frame_times[PhaseChanges.ElementAt(1)]);

                                        break;

                                    default:
                                        break;
                                }
                            }


                            #endregion
                        }
                        catch (Exception exc)
                        {
                            Logs.LogException(exc);
                        }
                    }
                    else
                    {
                        //Globals.LogTrace(Name + ": No Phase Data Present");
                    }
                    #endregion
                }
                catch (Exception exc)
                {
                    Logs.LogException(exc);
                }

                #region Stat Output

                //Initial Properties
                ret[0] = Name;
                ret[1] = Video.Camera != null ? Video.Camera.ScaleFactor : 0.0;
                object[] culture = CultureProperties.ToArray();
                for (int i = 2; i < 7; i++)
                {
                    ret[i] = culture[i - 2];
                }

                int c = 7;

                //Heartrate Statistics
                //m_intervals_heartrate = heart_rate.ToArray();
                //Stats<double> stats_heartrate = new Stats<double>(m_intervals_heartrate);

                //double hr_mean = StatisticalAnalysis.Mean(m_intervals_heartrate);
                //double hr_median = StatisticalAnalysis.Median(m_intervals_heartrate);
                //double hr_sd = StatisticalAnalysis.StandardDeviation(m_intervals_heartrate);
                //ret[c++] = hr_mean;
                //ret[c++] = hr_median;
                //ret[c++] = hr_sd;
                //ret[c++] = (hr_sd != Double.NaN && hr_median != Double.NaN) ? hr_sd / hr_median : Double.NaN;
                //c += 3;

                IntervalCollection l_intervals = (this.Phases[PhaseName.Interval].Data[n] as IntervalPhaseData).Intervals;
                l_intervals.Run(m_video.FrameTimes);

                ret[c++] = l_intervals.HeartRate.Mean;
                ret[c++] = l_intervals.HeartRate.Median;
                ret[c++] = l_intervals.HeartRate.StdDev;
                ret[c++] = l_intervals.HeartRate.SDMedian;
                c += 3;

                //Heartperiod Statistics
                m_intervals_heartperiod = heart_periods.ToArray();
                double hp_mean = StatisticalAnalysis.Mean(m_intervals_heartperiod);
                double hp_median = StatisticalAnalysis.Median(m_intervals_heartperiod);
                double hp_sd = StatisticalAnalysis.StandardDeviation(m_intervals_heartperiod);
                ret[c++] = hp_mean;
                ret[c++] = hp_median;
                ret[c++] = hp_sd;
                ret[c++] = (hp_sd != Double.NaN && hp_median != Double.NaN) ? hp_sd / hp_median : Double.NaN;
                c += 3;

                //Diastolic Interval
                m_intervals_diastoles = diastolic_intervals.ToArray();
                double di_mean = StatisticalAnalysis.Mean(m_intervals_diastoles);
                double di_median = StatisticalAnalysis.Median(m_intervals_diastoles);
                double di_sd = StatisticalAnalysis.StandardDeviation(m_intervals_diastoles);
                ret[c++] = di_mean;
                ret[c++] = di_median;
                ret[c++] = di_sd;
                ret[c++] = (di_sd != Double.NaN && di_median != Double.NaN) ? di_sd / di_median : Double.NaN;
                ret[c++] = di_mean / hp_mean;
                ret[c++] = StatisticalAnalysis.Sum(m_intervals_diastoles);
                c += 3;

                //Systolic Interval
                m_intervals_systoles = systolic_intervals.ToArray();
                double si_mean = StatisticalAnalysis.Mean(m_intervals_systoles);
                double si_median = StatisticalAnalysis.Median(m_intervals_systoles);
                double si_sd = StatisticalAnalysis.StandardDeviation(m_intervals_systoles);
                ret[c++] = si_mean;
                ret[c++] = si_median;
                ret[c++] = si_sd;
                ret[c++] = (si_sd != Double.NaN && si_median != Double.NaN) ? si_sd / si_median : Double.NaN;
                ret[c++] = si_mean / hp_mean;
                ret[c++] = StatisticalAnalysis.Sum(m_intervals_systoles);
                c += 2;

                //Heart Period SD/Median Heart Period (Arrythmia Index)
                ret[c++] = hp_sd / hp_median;

                //SI[mean]/DI[mean]
                ret[c++] = si_mean / di_mean;

                //Diastolic Diameter
                for (int i = 0; i < 2; i++)
                {
                    ret[c++] = (m_diastoles.Length > i) ? m_diastoles[i] : 0;
                }
                ret[c++] = (m_diastoles.Length > 0) ? StatisticalAnalysis.Mean(m_diastoles) : 0;

                //Systolic Diameter
                for (int i = 0; i < 2; i++)
                {
                    ret[c++] = (m_systoles.Length > i) ? m_systoles[i] : 0;
                }
                ret[c++] = (m_systoles.Length > 0) ? StatisticalAnalysis.Mean(m_systoles) : 0;

                //Fractional Shortening
                ret[c++] = (StatisticalAnalysis.Mean(m_diastoles) - StatisticalAnalysis.Mean(m_systoles)) / StatisticalAnalysis.Mean(m_diastoles);

                //1st Diastole Horizontal
                ret[c++] = m_diastoles_hor.Length != 0 ? m_diastoles_hor[0] : 0;

                //Diastolic SA
                double dias_area = 0;
                if (m_diastoles != null && m_diastoles.Length > 0 && m_diastoles_hor != null && m_diastoles_hor.Length > 0)
                {
                    //Vertical Axis
                    double a = m_diastoles[0] / 2;

                    //Horizontal Axis
                    double b = m_diastoles_hor[0] / 2;

                    //Calculate Area
                    dias_area = Math.PI * a * b;
                }
                ret[c++] = dias_area;

                //1st Systole Horizontal
                ret[c++] = m_systoles_hor.Length != 0 ? m_systoles_hor[0] : 0;

                //Systole SA
                double sys_area = 0;
                if (m_systoles != null && m_systoles.Length > 0 && m_systoles_hor != null && m_systoles_hor.Length > 0)
                {
                    //Vertical Axis
                    double a = m_systoles[0] / 2;

                    //Horizontal Axis
                    double b = m_systoles_hor[0] / 2;

                    //Calculate Area
                    sys_area = Math.PI * a * b;
                }
                ret[c++] = sys_area;

                //Fractional Area Change
                ret[c++] = (dias_area - sys_area) / dias_area;
                c += 3;

                //Contract Intervals
                m_intervals_contract = contraction_intervals.ToArray();
                double ci_mean = StatisticalAnalysis.Mean(m_intervals_contract);
                double ci_median = StatisticalAnalysis.Median(m_intervals_contract);
                double ci_sd = StatisticalAnalysis.StandardDeviation(m_intervals_contract);
                ret[c++] = ci_mean;
                ret[c++] = ci_median;
                ret[c++] = ci_sd;
                ret[c++] = (ci_sd != Double.NaN && ci_median != Double.NaN) ? ci_sd / ci_median : Double.NaN;
                ret[c++] = StatisticalAnalysis.Sum(m_intervals_contract);
                c += 3;

                //Relax Intervals
                m_intervals_relax = relaxation_intervals.ToArray();
                double ri_mean = StatisticalAnalysis.Mean(m_intervals_relax);
                double ri_median = StatisticalAnalysis.Median(m_intervals_relax);
                double ri_sd = StatisticalAnalysis.StandardDeviation(m_intervals_relax);
                ret[c++] = ri_mean;
                ret[c++] = ri_median;
                ret[c++] = ri_sd;
                ret[c++] = (ri_sd != Double.NaN && ri_median != Double.NaN) ? ri_sd / ri_median : Double.NaN;
                ret[c++] = StatisticalAnalysis.Sum(m_intervals_relax);
                c += 3;

                //MaxVelocity Intervals
                m_intervals_max = maxvelocity_intervals.ToArray();
                double mv_mean = StatisticalAnalysis.Mean(m_intervals_max);
                double mv_median = StatisticalAnalysis.Median(m_intervals_max);
                double mv_sd = StatisticalAnalysis.StandardDeviation(m_intervals_max);
                ret[c++] = mv_mean;
                ret[c++] = mv_median;
                ret[c++] = mv_sd;
                ret[c++] = (mv_sd != Double.NaN && mv_median != Double.NaN) ? mv_sd / mv_median : Double.NaN;
                ret[c++] = StatisticalAnalysis.Sum(m_intervals_max);
                //c += 3;

                //Jerome Additions (DI > 3)
                double m_diastoles_greater_3 = diastolic_intervals.FindAll(x => x > 3).Count;
                c++;//ret[c++] = (double)m_diastoles_greater_3;
                ret[c++] = (double)m_diastoles_greater_3;
                ret[c++] = ((m_diastoles_greater_3 / m_intervals_diastoles.Length) * 100);

                #region DI and SI Stats

                double total_diastoles = (double)m_intervals_diastoles.Length;
                double long_diastoles = (double)(m_intervals_diastoles.Where(x => x > diastole_max).Count());

                ret[c++] = total_diastoles;
                ret[c++] = long_diastoles;
                ret[c++] = (long_diastoles/total_diastoles * 100);

                double total_systoles = (double)m_intervals_systoles.Length;
                double long_systoles = (double)(m_intervals_systoles.Where(x => x > systole_max).Count());

                ret[c++] = total_systoles;
                ret[c++] = long_systoles;
                ret[c++] = (long_systoles / total_systoles * 100);
                ret[c++] = m_intervals_systoles_fibrillations;

                #endregion

                //Normalized Intervals
                m_intervals_normalized = norm_intervals.ToArray();
                double norm_mean = StatisticalAnalysis.Mean(m_intervals_normalized);
                double norm_median = StatisticalAnalysis.Median(m_intervals_normalized);
                double norm_sd = StatisticalAnalysis.StandardDeviation(m_intervals_normalized);
                ret[c++] = norm_mean;
                ret[c++] = norm_median;
                ret[c++] = norm_sd;
                ret[c++] = (norm_sd != Double.NaN && norm_median != Double.NaN) ? norm_sd / norm_median : Double.NaN;
                c += 3;

                //Isotonic Intervals
                m_intervals_isotonic = isotonic_intervals.ToArray();
                double iso_mean = StatisticalAnalysis.Mean(m_intervals_isotonic);
                double iso_median = StatisticalAnalysis.Median(m_intervals_isotonic);
                double iso_sd = StatisticalAnalysis.StandardDeviation(m_intervals_isotonic);
                ret[c++] = iso_mean;
                ret[c++] = iso_median;
                ret[c++] = iso_sd;
                ret[c++] = (iso_sd != Double.NaN && iso_median != Double.NaN) ? iso_sd / iso_median : Double.NaN;
                ret[c++] = StatisticalAnalysis.Sum(m_intervals_isotonic);



                //Shortening Velocity
                c += 2;
                double mean_dd = StatisticalAnalysis.Mean(m_diastoles);
                double mean_sd = StatisticalAnalysis.Mean(m_systoles);
                if (ci_mean != 0 && (mean_dd - mean_sd) != 0)
                    ret[c++] = (mean_dd - mean_sd) / ci_mean;

                //Lengthening Velocity
                if(ci_mean != 0 && (mean_dd - mean_sd) != 0)
                    ret[c++] = (mean_dd - mean_sd) / ri_mean;

                #endregion
                
                for (int j = 0; j < ret.Length; j++)
                {
                    try
                    {
                        if (j == 44 || j == 67)
                        {
                            string t = ret[j].ToString();
                            Type r = ret[j].GetType();
                        }

                        if (ret[j] == null || (double)ret[j] == 65535 || (double)ret[j] == 0 || ret[j].ToString().Equals("NaN"))
                            ret[j] = "";
                    }
                    catch (Exception exc)
                    {

                    }
                }
            }
            catch (Exception exc)
            {
                Logs.LogException(exc);
            }
            finally
            {

            }

            return ret;
        }

        public ListViewItem ToListViewItem()
        {
            ListViewItem item = new ListViewItem(m_name);

            if (Status == FilegramStatus.Processing)
            {
                item.SubItems.Add(Status.ToString());// + "(" + m_progress + "%)");
            }
            else
            {
                item.SubItems.Add(Status.ToString());
            }

            if(m_video != null)
                item.SubItems.Add(m_video.Camera != null ? m_video.Camera.ToString() : "N/A");

            item.UseItemStyleForSubItems = false;
            foreach (PhaseOutput po in m_phases)
            {
                System.Windows.Forms.ListViewItem.ListViewSubItem subitem = new ListViewItem.ListViewSubItem();
                subitem.Text = Enum.GetName(typeof(PhaseStatus), po.Status);

                Color bgColor = Color.White;
                switch (po.Status)
                {
                    case PhaseStatus.NA:
                        bgColor = Color.Yellow;
                        break;
                    case PhaseStatus.OK:
                        bgColor = Color.Green;
                        break;
                    case PhaseStatus.Reject:
                        bgColor = Color.Red;
                        break;
                    default:
                        bgColor = Color.White;
                        break;
                }
                subitem.BackColor = bgColor;
                item.SubItems.Add(subitem);
            }

            if (Status == FilegramStatus.Error)
            {
                for (int i = 0; i < item.SubItems.Count; i++)
                {
                    item.SubItems[i].BackColor = Color.Red;
                }
            }

            return item;
        }

        public static Filegram Deserialize(string path)
        {
            Filegram f = new Filegram();
            try
            {
                XmlTextReader reader = new XmlTextReader(path);
                reader.ReadToDescendant("Filegram");
                string version = reader.GetAttribute("Version");
                reader.Close();
                UpdateXml(path, version);

                using(reader = new XmlTextReader(path))
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(Filegram));
                    f = (Filegram)deserializer.Deserialize(reader);
                }

                //Clean IntervalCollection; added as result of v3.2.0.0 error where Change intervals got saved with Systole/Diastole
                foreach(PhaseData data in f.Phases[PhaseName.Interval].Data)
                    (data as IntervalPhaseData).Intervals.Clean(PhaseName.Interval);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
                f.Status = FilegramStatus.Error;
            }

            return f;
        }

        public static void Serialize(object o)
        {
            try
            {
                //SohaXml.Serialize<Filegram>((Filegram)o);

                Filegram f = (Filegram)o;
                XmlSerializer serializer = new XmlSerializer(typeof(Filegram));
                string path = Globals.CurrentDirectory + "\\" + f.Name + ".xml";
                using(XmlTextWriter writer = new XmlTextWriter(path, Encoding.UTF8))
                {
                    serializer.Serialize(writer, f);
                }

                using(XmlTextReader reader = new XmlTextReader(path))
                {

                }

                NTAccount act_everyone = new NTAccount("Everyone");
                SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);

                FileSecurity sec = File.GetAccessControl(path);
                sec.AddAccessRule(new FileSystemAccessRule(act_everyone, FileSystemRights.FullControl, AccessControlType.Allow));
                File.SetAccessControl(path, sec);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error Serializing Xml File.\r\n\r\n" + exc.Message); 
                Logs.LogException(exc);
            }
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            try
            {
                m_status = (FilegramStatus)Enum.Parse(typeof(FilegramStatus), reader["Status"].ToString());

                if (m_status != FilegramStatus.Error)
                {

                    reader.Read();
                    XmlSerializer ser = new XmlSerializer(typeof(CultureProperties));
                    m_culture_properties = (CultureProperties)ser.Deserialize(reader.ReadSubtree());

                    reader.Read();
                    Type type = Type.GetType("SOHA.Library.Video." + reader.Name + ", SOHA.Library");
                    ser = new XmlSerializer(type);
                    m_video = (Video)ser.Deserialize(reader.ReadSubtree());

                    
                    reader.Read();
                    ser = new XmlSerializer(typeof(PhaseOutputList));
                    m_phases = (PhaseOutputList)ser.Deserialize(reader.ReadSubtree());
                }
                else
                {

                }
            }
            catch (Exception exc)
            {
                Logs.LogException(exc);
                throw exc;
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Status", Status.ToString());
            writer.WriteAttributeString("Version", XML_Version);
            //writer.WriteAttributeString("Filepath", m_filepath);

            XmlSerializer ser = new XmlSerializer(typeof(CultureProperties));
            ser.Serialize(writer, m_culture_properties);

            ser = new XmlSerializer(m_video.GetType());
            ser.Serialize(writer, m_video);

            ser = new XmlSerializer(typeof(PhaseOutputList));
            ser.Serialize(writer, m_phases);

        }

        private static void UpdateXml(string path, string version)
        {
            if(version == null)
            {
                //Determine if OHBAFile version
                XmlTextReader reader = new XmlTextReader(path);
                reader.ReadToDescendant("OHBAFile");

                if(reader != null)
                {
                    reader.Close();
                    string new_path = Path.GetDirectoryName(path) + @"\" + Path.GetFileNameWithoutExtension(path) + @".xmlold";
                    File.Copy(path, new_path);
                    SohaXmlConvert.ConvertOHBAFile(path, path);
                }
                reader.Close();

                //XmlUpdated, return
                return;
            }

            while(version.Split('.').Length < 4)
                version += ".0";

            Version appVersion = new Version(version);

            if(appVersion < new Version(3, 0, 0, 2))
            {
                #region Replace '<Video' and '</Video>'

                /* Added 4/17/2014 */

                string text = File.ReadAllText(path);
                int index_of_video_node = text.IndexOf("Video");
                int index_of_video_path = text.IndexOf("Path=",index_of_video_node);
                int index_end_video_path = text.IndexOf("\">",index_of_video_path);

                int start_path = index_of_video_path + 6;
                int length = index_end_video_path - start_path;
                string video_path = text.Substring(start_path, length);
                string NEW_VIDEO = Path.GetExtension(video_path).ToUpper().Substring(1);

                text = text.Replace("<Video", "<" + NEW_VIDEO + "Video");
                text = text.Replace("</Video>", "</" + NEW_VIDEO + "Video>");
                File.WriteAllText(path, text);

                #endregion
            }
        }

        #endregion

        private static void XmlUpgrade(string path, int fromVersion, int toVersion)
        {
            Filegram new_fgram = new Filegram(path);
            bool read_success = true;
            FileInfo file = new FileInfo(path);
            if (file.Length <= (2 * Math.Pow(2,10)))
            {
                Filegram.Serialize(new_fgram);
                return;
            }

            try
            {
                if (fromVersion == 1)
                {

                    XmlDocument xml = new XmlDocument();
                    xml.Load(path);                   
                    

                    try
                    {
                        XmlElement culture = xml["OHBAFile"]["Properties"]["Culture"];
                        new_fgram.CultureProperties.Sex = culture.Attributes["Sex"].Value;
                        new_fgram.CultureProperties.Age = int.Parse(culture.Attributes["Age"].Value);
                        new_fgram.CultureProperties.ID = int.Parse(culture.Attributes["Id"].Value);
                        new_fgram.CultureProperties.Genotype = culture.Attributes["Genotype"].Value;
                        new_fgram.CultureProperties.CameraId = culture.Attributes["Camera"].Value;
                        new_fgram.CultureProperties.Comments = culture.Attributes["Comments"].Value;
                        new_fgram.CultureProperties.Time = culture.Attributes["Length"].Value;
                    }
                    catch (Exception exc)
                    {
                        new_fgram.CultureProperties = new CultureProperties(Path.GetFileNameWithoutExtension(path));
                    }

                    try
                    {
                        XmlElement video = xml["OHBAFile"]["Properties"]["Video"];
                        new_fgram.Video = new CXDVideo();
                        new_fgram.Video.Frames = Int32.Parse(video.GetAttribute("Frames"));
                        new_fgram.Video.Height = Int32.Parse(video.GetAttribute("Height"));
                        new_fgram.Video.Width = Int32.Parse(video.GetAttribute("Width"));
                        new_fgram.Video.Fps = Int32.Parse(video.GetAttribute("Fps"));

                    }
                    catch (Exception exc)
                    {
                        XmlElement video = xml["OHBAFile"];
                        new_fgram.Video = new CXDVideo();
                        new_fgram.Video.Frames = Int32.Parse(video.GetAttribute("Frames"));
                        new_fgram.Video.Height = Int32.Parse(video.GetAttribute("Height"));
                        new_fgram.Video.Width = Int32.Parse(video.GetAttribute("Width"));
                        new_fgram.Video.Fps = Int32.Parse(video.GetAttribute("Fps"));
                    }

                    string directory = Path.GetDirectoryName(path);
                    string filename = Path.GetFileNameWithoutExtension(path);
                    if (File.Exists(directory + @"\" + filename + ".cxd"))
                    {
                        new_fgram.Video.VideoPath = directory + @"\" + filename + ".cxd";
                    }
                    else if (File.Exists(directory + @"\" + filename + ".avi"))
                    {
                        new_fgram.Video.VideoPath = directory + @"\" + filename + ".avi";
                    }
                    else
                    {
                        new_fgram.Video.VideoPath = "";
                    }

                    try
                    {
                        XmlElement camera = xml["OHBAFile"]["Mark"]["Camera"]["Camera:camera"];
                        new_fgram.Video.Camera = new Camera();
                        new_fgram.Video.Camera.Name = camera.Attributes["Name"].Value;
                        new_fgram.Video.Camera.ScaleFactor = double.Parse(camera.Attributes["Factor"].Value);
                    }
                    catch (Exception exc)
                    {
                        //MessageBox.Show(exc.ToString());
                    }

                    try
                    {
                        XmlElement marks = xml["OHBAFile"]["Mark"]["Measures"];
                        MarkPhaseOutput mark_phase = new MarkPhaseOutput();
                        mark_phase.Data.Clear();
                        MarkPhaseData mark_data = new MarkPhaseData();
                        mark_phase.Status = xml["OHBAFile"]["Mark"].Attributes["Use"].Value == "yes" ? PhaseStatus.OK : PhaseStatus.Reject;
                        foreach (XmlElement mark in marks.ChildNodes)
                        {
                            MarkPair pair = new MarkPair();
                            pair.Beat = (BeatType)Enum.Parse(typeof(BeatType), mark.Attributes["BeatType"].Value);
                            pair.Orientation = (OrientationType)Enum.Parse(typeof(OrientationType), mark.Attributes["Orientation"].Value);
                            pair.Mark1 = new Point(int.Parse(mark.Attributes["x1"].Value), int.Parse(mark.Attributes["y1"].Value));
                            pair.Mark2 = new Point(int.Parse(mark.Attributes["x2"].Value), int.Parse(mark.Attributes["y2"].Value));
                            mark_data.Marks.Add(pair);
                        }
                        XmlElement rois = xml["OHBAFile"]["Mark"]["Rois"];
                        foreach (XmlElement roi in rois.ChildNodes)
                        {
                            Roi new_roi = new Roi(
                                int.Parse(roi.Attributes["x"].Value),
                                int.Parse(roi.Attributes["y"].Value),
                                int.Parse(roi.Attributes["w"].Value),
                                int.Parse(roi.Attributes["h"].Value));
                            mark_data.Rois.Add(new_roi);
                        }
                        mark_phase.Data.Add(mark_data);
                        new_fgram.Phases[0] = mark_phase;
                    }
                    catch (Exception exc)
                    {
                        //MessageBox.Show(exc.ToString());
                    }

                    try
                    {
                        XmlElement interval = xml["OHBAFile"]["Interval"];
                        IntervalPhaseOutput interval_phase = new IntervalPhaseOutput();
                        interval_phase.Data.Clear();
                        interval_phase.Settings.Clear();
                        interval_phase.Status = interval.Attributes["Use"].Value == "True" ? PhaseStatus.OK : PhaseStatus.Reject;

                        IntervalPhaseSettings interval_settings = new IntervalPhaseSettings();
                        interval_settings.BrightHiCutoff = 100;
                        interval_settings.BrightLoCutoff = 0;
                        interval_settings.BrightThresholdHi = int.Parse(interval.Attributes["Darkness_Hi"].Value);
                        interval_settings.BrightThresholdLo = int.Parse(interval.Attributes["Darkness_Lo"].Value);
                        interval_settings.IntensityHiCutoff = 100;
                        interval_settings.IntensityLoCutoff = 0;
                        interval_settings.IntensityMinInterval = int.Parse(interval.Attributes["Intensity_MinInterval"].Value);
                        interval_settings.IntensityThreshold = int.Parse(interval.Attributes["Intensity_Thresh"].Value);
                        interval_settings.Invert = false;
                        interval_settings.UseBright = bool.Parse(interval.Attributes["UseDarkness"].Value);
                        interval_phase.Settings.Add(interval_settings);


                        IntervalPhaseData interval_data = new IntervalPhaseData();
                        interval_data.Intervals = new IntervalCollection();
                        foreach (XmlElement i in interval["Intervals"].ChildNodes)
                        {
                            Interval new_interval = new Interval(
                                int.Parse(i.Attributes["frame"].Value),
                                (IntervalType)Enum.Parse(typeof(IntervalType), i.Attributes["intervaltype"].Value));
                            interval_data.Intervals.Add(new_interval);
                        }
                        interval_phase.Data.Add(interval_data);
                        new_fgram.Phases[2] = interval_phase;
                    }
                    catch (Exception exc)
                    {
                        //MessageBox.Show(exc.ToString());
                    }

                    try
                    {
                        PreprocessPhaseOutput process_output = new PreprocessPhaseOutput();
                        process_output.Data.Clear();
                        PreprocessPhaseData process_data = new PreprocessPhaseData();
                        XmlElement preprocess = xml["OHBAFile"]["Processing"]["ProcessResults"];
                        process_output.Status = PhaseStatus.OK;
                        foreach (XmlElement val in preprocess["darkness"].ChildNodes)
                        {
                            process_data.Brightness.Add(double.Parse(val.Attributes["value"].Value));
                        }
                        foreach (XmlElement val in preprocess["intensity"].ChildNodes)
                        {
                            process_data.Intensity.Add(double.Parse(val.Attributes["value"].Value));
                        }
                        process_output.Data.Add(process_data);
                        new_fgram.Phases[1] = process_output;
                    }
                    catch (Exception exc)
                    {
                        try
                        {
                            PreprocessPhaseOutput process_output = new PreprocessPhaseOutput();
                            process_output.Data.Clear();
                            PreprocessPhaseData process_data = new PreprocessPhaseData();
                            XmlElement preprocess = xml["OHBAFile"]["Processing"];
                            process_output.Status = PhaseStatus.OK;
                            foreach (XmlElement val in preprocess["Darkness"].ChildNodes)
                            {
                                process_data.Brightness.Add(double.Parse(val.Attributes["value"].Value));
                            }
                            foreach (XmlElement val in preprocess["Intensity"].ChildNodes)
                            {
                                process_data.Intensity.Add(double.Parse(val.Attributes["value"].Value));
                            }
                            process_output.Data.Add(process_data);
                            new_fgram.Phases[1] = process_output;

                            if (process_data.Brightness.Count() == 0 ||
                                process_data.Intensity.Count() == 0)
                                new_fgram.Phases[1].Status = PhaseStatus.NA;
                        }
                        catch (Exception exc2)
                        {
                            //MessageBox.Show(exc.ToString());
                            new_fgram.Phases[1].Status = PhaseStatus.NA;
                        }
                    }

                    try
                    {
                        XmlElement phase = xml["OHBAFile"]["Phase"];
                        PhasePhaseOutput phase_phase = new PhasePhaseOutput();
                        phase_phase.Data.Clear();
                        phase_phase.Settings.Clear();
                        phase_phase.Status = phase.Attributes["Use"].Value == "True" ? PhaseStatus.OK : PhaseStatus.Reject;

                        PhasePhaseSettings phase_settings = new PhasePhaseSettings();
                        phase_settings.HiCutoff = 100;
                        phase_settings.LoCutoff = 0;
                        phase_settings.Offset = int.Parse(phase.Attributes["Offset"].Value);
                        phase_settings.Zoom = 2;
                        phase_phase.Settings.Add(phase_settings);

                        PhasePhaseData phase_data = new PhasePhaseData();
                        phase_data.Intervals = new IntervalCollection();
                        foreach (XmlElement i in phase["Intervals"].ChildNodes)
                        {
                            Interval new_interval = new Interval(
                                int.Parse(i.Attributes["frame"].Value),
                                (IntervalType)Enum.Parse(typeof(IntervalType), i.Attributes["intervaltype"].Value));
                            phase_data.Intervals.Add(new_interval);
                        }
                        phase_phase.Data.Add(phase_data);
                        new_fgram.Phases[3] = phase_phase;
                    }
                    catch (Exception exc)
                    {
                        //MessageBox.Show(exc.ToString());
                    }

                    if (new_fgram.Video.FrameTimes == null)
                        new_fgram.Video.GetFrameTimes();

                    //Include Offset in Preprocessing and intervals
                    int shift = 10;
                    if ((new_fgram.Phases[1].Data[0] as PreprocessPhaseData) != null)
                    {
                        (new_fgram.Phases[1].Data[0] as PreprocessPhaseData).Brightness = ArrayFunctions.RShift((new_fgram.Phases[1].Data[0] as PreprocessPhaseData).Brightness, shift);
                        (new_fgram.Phases[1].Data[0] as PreprocessPhaseData).Intensity = ArrayFunctions.RShift((new_fgram.Phases[1].Data[0] as PreprocessPhaseData).Intensity, shift);
                    }

                    if ((new_fgram.Phases[2].Data[0] as IntervalPhaseData) != null && (new_fgram.Phases[2].Data[0] as IntervalPhaseData).Intervals != null)
                        (new_fgram.Phases[2].Data[0] as IntervalPhaseData).Intervals.Shift(shift);
                    if ((new_fgram.Phases[3].Data[0] as PhasePhaseData) != null && (new_fgram.Phases[3].Data[0] as PhasePhaseData).Intervals != null)
                        (new_fgram.Phases[3].Data[0] as PhasePhaseData).Intervals.Shift(shift);

                    ////Recalculate MMode
                    //if ((new_fgram.Phases[1].Data[0] as PreprocessPhaseData) != null || (new_fgram.Phases[1].Data[0] as PreprocessPhaseData).MMode != null)
                    //{
                    //    try
                    //    {
                    //        new_fgram.Preprocess(false);
                    //        //int count = 0;
                    //        //if ((new_fgram.Phases[0].Data[0] as MarkPhaseData) != null)
                    //        //{
                    //        //    foreach (Roi r in (new_fgram.Phases[0].Data[0] as MarkPhaseData).Rois)
                    //        //    {
                    //        //        int midpoint = (r.Width - r.X) / 2;
                    //        //        MMode mmode = new_fgram.Video.GetMMode(midpoint);

                    //        //        (new_fgram.Phases[1].Data[count] as PreprocessPhaseData).MMode = mmode;

                    //        //        count++;
                    //        //    }
                    //        //}
                    //    }
                    //    catch (Exception exc)
                    //    {
                    //        MessageBox.Show(exc.ToString());
                    //    }
                    //}
                    //Roi r = (new_fgram.Phases[0].Data[0] as MarkPhaseData).Rois[0].Width - 
                    //new_fgram.Video.GetMMode(,MModeType.Full)
                    //(new_fgram.Phases[1].Data

                    Filegram.Serialize(new_fgram);


                    //XmlElement marks = xml["OHBAFile"]["Mark"]["Measures"];
                    //MarkPhaseOutput mark_phase = new MarkPhaseOutput();
                    //mark_phase.Status = xml["OHBAFile"]["Mark"].Attributes["Use"].Value == "yes" ? PhaseStatus.OK : PhaseStatus.Reject;
                    //foreach (XmlElement mark in marks.ChildNodes)
                    //{
                    //    MarkPair pair = new MarkPair();
                    //    pair.Beat = (BeatType)Enum.Parse(typeof(BeatType), mark.Attributes["BeatType"].Value);
                    //    pair.Orientation = (OrientationType)Enum.Parse(typeof(OrientationType), mark.Attributes["Orientation"].Value);
                    //    pair.Mark1 = new Point(int.Parse(mark.Attributes["x1"].Value), int.Parse(mark.Attributes["y1"].Value));
                    //    pair.Mark2 = new Point(int.Parse(mark.Attributes["x2"].Value), int.Parse(mark.Attributes["y2"].Value));
                    //}
                    //new_fgram.Phases[0] = mark_phase;



                    //new_fgram.Video.VideoPath = video.GetAttribute("Path");

                    /*
                    XmlTextReader reader = new XmlTextReader(path);

                    read_success = reader.ReadToFollowing("CultureProperties");
                    XmlSerializer serializer = new XmlSerializer(typeof(CultureProperties));
                    new_fgram.CultureProperties = (CultureProperties)serializer.Deserialize(reader.ReadSubtree());

                    read_success = reader.ReadToFollowing("VideoInterop");
                    new_fgram.Video = new Video();
                    new_fgram.Video.Frames = Int32.Parse(reader.GetAttribute("Frames"));
                    new_fgram.Video.Height = Int32.Parse(reader.GetAttribute("Height"));
                    new_fgram.Video.Width = Int32.Parse(reader.GetAttribute("Width"));
                    new_fgram.Video.Fps = Int32.Parse(reader.GetAttribute("Fps"));
                    new_fgram.Video.VideoPath = reader.GetAttribute("Path");

                    reader.ReadToFollowing("Camera");
                    serializer = new XmlSerializer(typeof(Camera));
                    new_fgram.Video.Camera = (Camera)serializer.Deserialize(reader);

                    serializer = new XmlSerializer(typeof(double[]));
                    new_fgram.Video.FrameTimes = (double[])serializer.Deserialize(reader);

                    reader.ReadToFollowing("MarkPhaseOutput");
                    serializer = new XmlSerializer(typeof(MarkPhaseOutput));
                    new_fgram.Phases[0] = (MarkPhaseOutput)serializer.Deserialize(reader);


                    // Process 
                    PreprocessPhaseOutput preprocess = new PreprocessPhaseOutput();
                    preprocess.Status = (PhaseStatus)Enum.Parse(typeof(PhaseStatus), reader.GetAttribute("Status"));
                    //reader.ReadToFollowing("ProcessPhaseOutput");
                    reader.ReadToDescendant("ProcessPhaseData");

                    do
                    {
                        PreprocessPhaseData preprocess_data = new PreprocessPhaseData();
                        reader.Read();
                        serializer = new XmlSerializer(typeof(List<double>));
                        preprocess_data.Brightness = (List<double>)serializer.Deserialize(reader);
                        preprocess_data.Intensity = (List<double>)serializer.Deserialize(reader);

                        //serializer = new XmlSerializer(typeof(MMode));
                        //m_mmode = (MMode)serializer.Deserialize(reader);

                        preprocess.Data.Add(preprocess_data);
                    } while (reader.ReadToNextSibling("ProcessPhaseData"));

                    new_fgram.Phases[1] = preprocess;


                    // Interval 
                    IntervalPhaseOutput interval = new IntervalPhaseOutput();
                    reader.ReadToFollowing("IntervalPhaseOutput");
                    interval.Status = (PhaseStatus)Enum.Parse(typeof(PhaseStatus), reader.GetAttribute("Status"));
                    reader.ReadToDescendant("IntervalPhaseData");

                    do
                    {
                        IntervalPhaseData interval_data = new IntervalPhaseData();
                        reader.Read();
                        serializer = new XmlSerializer(typeof(IntervalCollection));
                        interval_data.Intervals = (IntervalCollection)serializer.Deserialize(reader);

                        interval.Data.Add(interval_data);
                        interval.Settings.Add(new IntervalPhaseSettings());
                    } while (reader.ReadToNextSibling("IntervalPhaseData"));

                    new_fgram.Phases[2] = interval;

                    // Phase 
                    PhasePhaseOutput phase = new PhasePhaseOutput();
                    reader.ReadToFollowing("PhasePhaseOutput");
                    phase.Status = (PhaseStatus)Enum.Parse(typeof(PhaseStatus), reader.GetAttribute("Status"));
                    reader.ReadToDescendant("PhasePhaseData");

                    do
                    {
                        PhasePhaseData phase_data = new PhasePhaseData();
                        reader.Read();
                        serializer = new XmlSerializer(typeof(IntervalCollection));
                        phase_data.Intervals = (IntervalCollection)serializer.Deserialize(reader);

                        phase.Data.Add(phase_data);
                        phase.Settings.Add(new PhasePhaseSettings());
                    } while (reader.ReadToNextSibling("PhasePhaseData"));

                    new_fgram.Phases[3] = phase;

                    reader.Close();
                    //File.Delete(path);
                    Filegram.Serialize(new_fgram);
                     * */
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }

        private void XmlVersionRead(XmlReader reader, string version)
        {
            try
            {
                if (version == "2")
                {
                    #region 2

                    m_status = (FilegramStatus)Enum.Parse(typeof(FilegramStatus), reader["Status"].ToString());

                    if (m_status != FilegramStatus.Error)
                    {

                        reader.Read();
                        XmlSerializer ser = new XmlSerializer(typeof(CultureProperties));
                        m_culture_properties = (CultureProperties)ser.Deserialize(reader.ReadSubtree());

                        reader.Read();
                        ser = new XmlSerializer(typeof(Video));
                        m_video = (Video)ser.Deserialize(reader.ReadSubtree());

                        string ext = Path.GetExtension(m_video.VideoPath);

                        switch (ext.ToUpper())
                        {
                            case ".CXD":
                                m_video = CXDVideo.FromVideo(m_video);
                                break;
                            case ".AVI":
                                m_video = AVIVideo.FromVideo(m_video);
                                break;
                        }

                        //m_filepath = Cxd.FilePath;

                        reader.Read();
                        ser = new XmlSerializer(typeof(PhaseOutputList));
                        m_phases = (PhaseOutputList)ser.Deserialize(reader.ReadSubtree());
                    }
                    else
                    {

                    }

                    #endregion
                }
            }
            catch (Exception exc)
            {
                Logs.LogException(exc);
                throw exc;
            }
        }

        public void Load()
        {
            string file = m_load_file;
            Logs.LogTrace("Loading " + file);

            Filegram fgram = new Filegram(file);
            string extension = Path.GetExtension(file);
            extension = extension.Substring(1);

            try
            {
                switch (extension.ToUpper())
                {
                    case "XML":
                        fgram = Filegram.Deserialize(file);
                        this.Name = fgram.Name;
                        this.Phases = fgram.Phases;
                        this.Video = fgram.Video;
                        break;

                    case "CXD":
                        this.Video = new CXDVideo(file);
                        //Filegram.Serialize(fgram);
                        break;

                    case "AVI":
                        this.Video = new AVIVideo(file);
                        //Filegram.Serialize(fgram);
                        break;
                }

                fgram.Status = FilegramStatus.Idle;
            }
            catch (Exception exc)
            {
                fgram.Status = FilegramStatus.Error;
            }
            finally
            {
                fgram.Name = Path.GetFileNameWithoutExtension(file);
            }
        }

        public override void Run(ProcessEventArgs arg, CancellationToken cancel_token)
        {
            bool error_return = false;
            Exception exception_return = null;

            string file = m_load_file;
            Logs.LogTrace("Loading " + file);

            Filegram fgram = new Filegram(file);
            string extension = Path.GetExtension(file);
            extension = extension.Substring(1);

            try
            {
                switch (extension.ToUpper())
                {
                    case "XML":
                        fgram = Filegram.Deserialize(file);

                        //If original Video Path DNE then find one locally in the Current Working Directory
                        if (!File.Exists(fgram.Video.VideoPath))
                        {
                            string cwd = Globals.CurrentDirectory;
                            string name = Path.GetFileNameWithoutExtension(file);

                            if (File.Exists(cwd + "\\" + name + ".cxd"))
                            {
                                fgram.Video.VideoPath = cwd + "\\" + name + ".cxd";
                            }
                            else if (File.Exists(cwd + name + ".avi"))
                            {
                                fgram.Video.VideoPath = cwd + "\\" + name + ".avi";
                            }
                            else
                            {

                            }
                        }
                        break;

                    case "CXD":
                        fgram.Video = new CXDVideo(file);
                        //Filegram.Serialize(fgram);
                        break;

                    case "AVI":
                        fgram.Video = new AVIVideo(file);
                        //Filegram.Serialize(fgram);
                        break;
                }

                fgram.Status = FilegramStatus.Idle;
            }
            catch (Exception exc)
            {
                exception_return = exc;
                fgram.Status = FilegramStatus.Error;
            }
            finally
            {
                fgram.Name = Path.GetFileNameWithoutExtension(file);
            }

            OnComplete(new ProgressEventArgs(fgram, false, error_return,exception_return));
        }

        public override void Run(ProcessEventArgs arg, CancellationTokenSource cancel_token)
        {
            bool error_return = false;
            Exception exception_return = null;

            string file = m_load_file;
            Logs.LogTrace("Loading " + file);

            Filegram fgram = new Filegram(file);
            string extension = Path.GetExtension(file);
            extension = extension.Substring(1);

            try
            {
                switch (extension.ToUpper())
                {
                    case "XML":
                        fgram = Filegram.Deserialize(file);

                        //If original Video Path DNE then find one locally in the Current Working Directory
                        if (!File.Exists(fgram.Video.VideoPath))
                        {
                            string cwd = Globals.CurrentDirectory;
                            string name = Path.GetFileNameWithoutExtension(file);

                            if (File.Exists(cwd + "\\" + name + ".cxd"))
                            {
                                fgram.Video.VideoPath = cwd + "\\" + name + ".cxd";
                            }
                            else if (File.Exists(cwd + name + ".avi"))
                            {
                                fgram.Video.VideoPath = cwd + "\\" + name + ".avi";
                            }
                            else
                            {

                            }
                        }
                        break;

                    case "CXD":
                        fgram.Video = new CXDVideo(file);
                        //Filegram.Serialize(fgram);
                        break;

                    case "AVI":
                        fgram.Video = new AVIVideo(file);
                        //Filegram.Serialize(fgram);
                        break;
                }

                fgram.Status = FilegramStatus.Idle;
            }
            catch (Exception exc)
            {
                exception_return = exc;
                fgram.Status = FilegramStatus.Error;
            }
            finally
            {
                fgram.Name = Path.GetFileNameWithoutExtension(file);
            }

            //OnComplete(new ProgressEventArgs(fgram, false, error_return, exception_return));
            OnEnd(new AsyncCompletedEventArgs(exception_return, cancel_token.Token.IsCancellationRequested, fgram));
        }

        public override void TestRun(ProcessEventArgs arg, CancellationTokenSource cancel_token)
        {
            bool error_return = false;
            Exception exception_return = null;

            string file = m_load_file;
            Logs.LogTrace("Loading " + file);

            Filegram fgram = new Filegram(file);
            string extension = Path.GetExtension(file);
            extension = extension.Substring(1);

            try
            {
                switch(extension.ToUpper())
                {
                    case "XML":
                        fgram = Filegram.Deserialize(file);

                        //If original Video Path DNE then find one locally in the Current Working Directory
                        if(!File.Exists(fgram.Video.VideoPath))
                        {
                            string cwd = Globals.CurrentDirectory;
                            string name = Path.GetFileNameWithoutExtension(file);

                            if(File.Exists(cwd + "\\" + name + ".cxd"))
                            {
                                fgram.Video.VideoPath = cwd + "\\" + name + ".cxd";
                            }
                            else if(File.Exists(cwd + name + ".avi"))
                            {
                                fgram.Video.VideoPath = cwd + "\\" + name + ".avi";
                            }
                            else
                            {

                            }
                        }
                        break;

                    case "CXD":
                        fgram.Video = new CXDVideo(file);
                        //Filegram.Serialize(fgram);
                        break;

                    case "AVI":
                        fgram.Video = new AVIVideo(file);
                        //Filegram.Serialize(fgram);
                        break;
                }

                fgram.Status = FilegramStatus.Idle;
            }
            catch(Exception exc)
            {
                exception_return = exc;
                fgram.Status = FilegramStatus.Error;
            }
            finally
            {
                fgram.Name = Path.GetFileNameWithoutExtension(file);
            }

            OnComplete(new ProgressEventArgs(fgram, false, error_return, exception_return));
        }

        //public override void OnEnd(AsyncCompletedEventArgs e)
        //{
        //    if(EndProcess != null)
        //        EndProcess(this, e);
        //}

        public void Preprocess(bool async)
        {
            if (async)
            {
                lock (syncObject)
                {
                    this.Video.EndProcess += new AsyncEventHandler(Video_EndProcess);
                    this.Video.BeginProcess += new EventHandler(Video_ProgressBegin);
                    this.Video.UpdateProcess += new AsyncEventHandler(Video_UpdateProcess);
                    this.Status = FilegramStatus.Queued;

                    PreprocessArguments args =
                        new PreprocessArguments(
                            (Phases[PhaseName.Mark].Data[0] as MarkPhaseData).Rois,
                            (Phases[PhaseName.Mark].Options[0] as MarkPhaseOptions).Filters,
                            (Phases[PhaseName.Preprocess].Options[0] as PreprocessPhaseOptions).EnableFilters
                            );

                    Globals.PQueue.Enqueue(this.Video, new ProcessEventArgs(ProcessEventArgsType.Preprocess, args));
                }
            }
            else
            {
                this.Video.GetPreprocess((this.Phases[0].Data[0] as MarkPhaseData).Rois.ToList());
            }
        }

        void Video_UpdateProcess(object sender, AsyncCompletedEventArgs e)
        {
            m_progress = (int)e.UserState;

            OnStatusChanged();
        }

        void Video_EndProcess(object sender, AsyncCompletedEventArgs e)
        {
            this.Status = FilegramStatus.Idle;
            try
            {
                lock (syncObject)
                {
                    PreprocessPhaseOutput process_output = this.Phases[1] as PreprocessPhaseOutput;
                    if (e.Cancelled)
                    {

                    }
                    else if (e.Error != null)
                    {
                        //process_output.Status = PhaseStatus.Reject;
                        //process_output.Data.Clear();
                    }
                    else
                    {
                        process_output.Status = PhaseStatus.OK;
                        process_output.Data.Clear();

                        PreprocessData[] data_out = e.UserState as PreprocessData[];
                        foreach (PreprocessData data in data_out)
                        {
                            PreprocessPhaseData out_data = new PreprocessPhaseData();
                            out_data.Brightness = data.Darkness.ToList();
                            out_data.Intensity = data.Intensity.ToList();
                            out_data.MMode = data.MMode;

                            process_output.Data.Add(out_data);
                        }

                        Filegram.Serialize(this);
                    }

                    //OnEnd(new AsyncCompletedEventArgs(e.Error, e.Cancelled, this));
                    OnComplete(new ProgressEventArgs(this, e.Cancelled, (e.Error != null), e.Error));
                }
            }
            catch (Exception exc)
            {

            }

            OnStatusChanged();
        }

        public int GetProgress()
        {
            return m_progress;
        }

        void Video_ProgressUpdate(Video sender, int progress)
        {
            m_progress = progress;

            OnStatusChanged();
        }

        void Video_ProgressBegin(object sender, EventArgs e)
        {
            this.Status = FilegramStatus.Processing;
        }

        void Video_ProcessComplete(object sender, ProgressEventArgs e)
        {
            this.Status = FilegramStatus.Idle;
            try
            {
                lock (syncObject)
                {
                    PreprocessPhaseOutput process_output = this.Phases[1] as PreprocessPhaseOutput;
                    if (e.Cancelled)
                    {

                    }
                    else if (e.Error)
                    {
                        //process_output.Status = PhaseStatus.Reject;
                        //process_output.Data.Clear();
                    }
                    else
                    {
                        process_output.Status = PhaseStatus.OK;
                        process_output.Data.Clear();

                        foreach (PreprocessData data in e.Return as PreprocessData[])
                        {
                            PreprocessPhaseData out_data = new PreprocessPhaseData();
                            out_data.Brightness = data.Darkness.ToList();
                            out_data.Intensity = data.Intensity.ToList();
                            out_data.MMode = data.MMode;

                            process_output.Data.Add(out_data);
                        }

                        Filegram.Serialize(this);
                    }

                    OnComplete(new ProgressEventArgs(this, e.Cancelled, e.Error, e.Exception));
                }
            }
            catch (Exception exc)
            {

            }

            OnStatusChanged();
        }
    }
}
