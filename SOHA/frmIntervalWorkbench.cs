﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Forms;
using SOHA.Library;
using SOHA.Library.Xml;

namespace SOHA
{
    public partial class frmIntervalWorkbench : frmPhase
    {
        private readonly IntervalCollection intervals = new IntervalCollection();
        private readonly RegionCollection regions;
        private readonly List<double> intensity = new List<double>();


        public frmIntervalWorkbench()
        {
            InitializeComponent();
        }

        public frmIntervalWorkbench(Filegram fg)
            : base(fg)
        {
            InitializeComponent();

            intervals = ((m_filegram.Phases[PhaseName.Interval] as IntervalPhaseOutput).Data[0] as IntervalPhaseData).Intervals;
            intensity = ((m_filegram.Phases[PhaseName.Preprocess] as PreprocessPhaseOutput).Data[0] as PreprocessPhaseData).Intensity;

            regions = new RegionCollection(intervals);

            numericUpDown1.Minimum = 0;
            numericUpDown1.Maximum = regions.Count;
            numericUpDown1.ValueChanged += new EventHandler(numericUpDown1_ValueChanged);
        }

        void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void frmIntervalWorkbench_Load(object sender, EventArgs e)
        {
            Dictionary<double,double> function = new Dictionary<double,double>();

            int start = regions[(int)numericUpDown1.Value].Start;
            int end = regions[(int)numericUpDown1.Value].End;

            //ucRegionFunctionControl1.Function = new 
        }
    }
}
