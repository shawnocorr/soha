﻿using SOHAControls;
namespace SOHA
{
    partial class frmAssay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lstFilegrams = new System.Windows.Forms.ListView();
            this.mnuPhase = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.markToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.preprocessToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.intervalToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.phaseToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.redDotMovieToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.mModeSelectToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.propertiesToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.workBenchToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.intervalWorkbenchToolStripMenuItem = new SOHAControls.PhaseToolStripItem();
            this.mnuPhase.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstFilegrams
            // 
            this.lstFilegrams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstFilegrams.FullRowSelect = true;
            this.lstFilegrams.Location = new System.Drawing.Point(0, 0);
            this.lstFilegrams.Name = "lstFilegrams";
            this.lstFilegrams.Size = new System.Drawing.Size(478, 282);
            this.lstFilegrams.TabIndex = 0;
            this.lstFilegrams.UseCompatibleStateImageBehavior = false;
            this.lstFilegrams.SelectedIndexChanged += new System.EventHandler(this.lstFilegrams_SelectedIndexChanged);
            // 
            // mnuPhase
            // 
            this.mnuPhase.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markToolStripMenuItem,
            this.preprocessToolStripMenuItem,
            this.intervalToolStripMenuItem,
            this.phaseToolStripMenuItem,
            this.toolStripMenuItem2,
            this.redDotMovieToolStripMenuItem,
            this.mModeSelectToolStripMenuItem,
            this.toolStripMenuItem3,
            this.propertiesToolStripMenuItem,
            this.workBenchToolStripMenuItem,
            this.intervalWorkbenchToolStripMenuItem});
            this.mnuPhase.Name = "mnuPhase";
            this.mnuPhase.Size = new System.Drawing.Size(178, 214);
            // 
            // markToolStripMenuItem
            // 
            this.markToolStripMenuItem.Name = "markToolStripMenuItem";
            this.markToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.Mark;
            this.markToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.markToolStripMenuItem.Text = "Mark";
            // 
            // preprocessToolStripMenuItem
            // 
            this.preprocessToolStripMenuItem.Name = "preprocessToolStripMenuItem";
            this.preprocessToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.Preprocess;
            this.preprocessToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.preprocessToolStripMenuItem.Text = "Preprocess";
            // 
            // intervalToolStripMenuItem
            // 
            this.intervalToolStripMenuItem.Name = "intervalToolStripMenuItem";
            this.intervalToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.Interval;
            this.intervalToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.intervalToolStripMenuItem.Text = "Interval";
            // 
            // phaseToolStripMenuItem
            // 
            this.phaseToolStripMenuItem.Name = "phaseToolStripMenuItem";
            this.phaseToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.Phase;
            this.phaseToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.phaseToolStripMenuItem.Text = "Phase";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(174, 6);
            // 
            // redDotMovieToolStripMenuItem
            // 
            this.redDotMovieToolStripMenuItem.Name = "redDotMovieToolStripMenuItem";
            this.redDotMovieToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.RedDot;
            this.redDotMovieToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.redDotMovieToolStripMenuItem.Text = "Red Dot Movie";
            // 
            // mModeSelectToolStripMenuItem
            // 
            this.mModeSelectToolStripMenuItem.Name = "mModeSelectToolStripMenuItem";
            this.mModeSelectToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.MModeSelect;
            this.mModeSelectToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.mModeSelectToolStripMenuItem.Text = "MMode Select";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(174, 6);
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.Properties;
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.propertiesToolStripMenuItem.Text = "Properties";
            // 
            // workBenchToolStripMenuItem
            // 
            this.workBenchToolStripMenuItem.Name = "workBenchToolStripMenuItem";
            this.workBenchToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.WorkBench;
            this.workBenchToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.workBenchToolStripMenuItem.Text = "WorkBench";
            // 
            // intervalWorkbenchToolStripMenuItem
            // 
            this.intervalWorkbenchToolStripMenuItem.Name = "intervalWorkbenchToolStripMenuItem";
            this.intervalWorkbenchToolStripMenuItem.Phase = SOHA.Library.PhaseFormType.IntervalWorkbench;
            this.intervalWorkbenchToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.intervalWorkbenchToolStripMenuItem.Text = "Interval Workbench";
            // 
            // frmAssay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 282);
            this.Controls.Add(this.lstFilegrams);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmAssay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assay [Folder]";
            this.mnuPhase.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstFilegrams;
        private System.Windows.Forms.ContextMenuStrip mnuPhase;
        private PhaseToolStripItem markToolStripMenuItem;
        private PhaseToolStripItem preprocessToolStripMenuItem;
        private PhaseToolStripItem intervalToolStripMenuItem;
        private PhaseToolStripItem phaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private PhaseToolStripItem redDotMovieToolStripMenuItem;
        private PhaseToolStripItem mModeSelectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private PhaseToolStripItem propertiesToolStripMenuItem;
        private PhaseToolStripItem workBenchToolStripMenuItem;
        private PhaseToolStripItem intervalWorkbenchToolStripMenuItem;
    }
}