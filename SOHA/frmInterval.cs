﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;

using SOHA.Library.Video;
using SOHAControls;
using SOHA.Library.Xml;
using SOHA.Library.Forms;

namespace SOHA
{
    public partial class frmInterval : frmPhase
    {
        private readonly Frame m_mmode_display_frame;
        private Video m_video;
        private bool m_collecting_mmode = false;
        private IntervalProperties m_current_frame_interval = new IntervalProperties();
        private double Max = 0;
        private double Min = 0;

        //private IntervalPhaseOutput m_output;
        private IntervalPhaseData m_interval_data;
        private IntervalPhaseSettings m_interval_settings;
        private IntervalPhaseOutput m_interval_output;
        private PreprocessPhaseData m_preprocess_data;

        private int m_current_roi = 0;

        public frmInterval()
        {
            InitializeComponent();
        }

        public frmInterval(Filegram fgram)
        {
            InitializeComponent();

            if (!(fgram.Phases[1] is PreprocessPhaseOutput) 
                || (fgram.Phases[1] as PreprocessPhaseOutput).Status == PhaseStatus.NA 
                || (fgram.Phases[1] as PreprocessPhaseOutput).Status == PhaseStatus.Reject)
            {
                throw new PhaseInitiationException("No Mark Data Present.");
            }

            this.ResizeEnd += new EventHandler(frmInterval_ResizeEnd);

            m_phase_type = SOHA.Library.PhaseFormType.Interval;
            m_filegram = fgram;
            m_filegram.Status = FilegramStatus.UserInterface;
            this.Text = m_filegram.Name;

            this.KeyDown += new KeyEventHandler(frmInterval_KeyDown);
            this.Resize += new EventHandler(frmInterval_Resize);
            this.FormClosing += new FormClosingEventHandler(frmInterval_FormClosing);

            m_interval_output = (fgram.Phases[2] as IntervalPhaseOutput);
            m_interval_data = m_interval_output.Data[m_current_roi] as IntervalPhaseData;
            m_interval_settings = m_interval_output.Settings[m_current_roi] as IntervalPhaseSettings;
            m_preprocess_data = (fgram.Phases[1] as PreprocessPhaseOutput).Data[m_current_roi] as PreprocessPhaseData;

            #region Setup Multiple ROI Selections

            int num_rois = (fgram.Phases[1] as PreprocessPhaseOutput).Data.Count;
            for (int i = 1; i < num_rois; i++)
            {
                m_interval_output.Data.Add(new IntervalPhaseData());
                m_interval_output.Settings.Add(new IntervalPhaseSettings());
            }

            if (num_rois > 1)
            {
                BindingList<Roi> rois = ((fgram.Phases[0] as MarkPhaseOutput).Data[0] as MarkPhaseData).Rois;
                foreach (Roi roi in rois)
                {
                    if (roi.Name != null && roi.Name.Trim() != "")
                    {
                        cmbROI.Items.Add(roi.Name);
                    }
                    else
                    {
                        cmbROI.Items.Add(rois.IndexOf(roi));
                    }
                }

                cmbROI.SelectedIndex = 0;
                cmbROI.SelectedIndexChanged += new EventHandler(cmbROI_SelectedIndexChanged);
            }
            else
            {
                cmbROI.Enabled = false;
            }

            #endregion

            //Initialize Primary Preprocess Output

            mModeDisplay.MMode = m_preprocess_data.MMode;
            //mModeDisplay.PhaseType = PhaseType.Interval;
            mModeDisplay.List = m_interval_data.Intervals;
            mModeDisplay.MouseMove += new MouseEventHandler(mModeDisplay_MouseMove);

            m_video = m_filegram.Video;

            try
            {
                m_video.Open();
                m_mmode_display_frame = m_video.GetFrameSync(1);
                m_video_present = true;
                //m_video.Close();
            }
            catch (VideoFileNotFoundException vnfe)
            {
                m_video_present = false;
                scrollDarknessHigh.Enabled = false;
                scrollDarknessLow.Enabled = false;
                scrollIntensity.Enabled = false;
                //cmdAccept.Enabled = false;
                //cmdDiscard.Enabled = false;
                //cmdReset.Enabled = false;
                //mModeDisplay.Enabled = false;
                mModeThumbnail.Enabled = false;
                chkInvert.Enabled = false;
                chkUseDarkness.Enabled = false;
                bpFilterBright.Enabled = false;
                bpFilterIntensity.Enabled = false;
            }

            m_video.MModeComplete += new MModeEventHandler(m_video_MModeComplete);

            if (m_video_present)
            {
                mModeThumbnail.XCoord = m_preprocess_data.MMode.XCoordinate;
                mModeThumbnail.Thumbnail = m_mmode_display_frame.ToBitmap();
                mModeThumbnail.XCoordChanged += new MouseEventHandler(mModeThumbnail_XCoordChanged);
            }

            //Set Defaults
            scrollDarknessHigh.Value = brightDisplay1.HiThreshold = m_interval_settings.BrightThresholdHi;
            scrollDarknessLow.Value = brightDisplay1.LoThreshold = m_interval_settings.BrightThresholdLo;
            chkInvert.Checked = brightDisplay1.Invert = m_interval_settings.Invert;
            chkUseDarkness.Checked = brightDisplay1.UseDarkness = m_interval_settings.UseBright;
            bpFilterBright.LoCutoff = brightDisplay1.LoCutoff = m_interval_settings.BrightLoCutoff;
            bpFilterBright.HiCutoff = brightDisplay1.HiCutoff = m_interval_settings.BrightHiCutoff;
            checkBox1.Checked = mModeDisplay.ShowChangeIntervals = m_interval_settings.ShowPhase;
            numericUpDown1.Value = mModeDisplay.PhaseOffset = m_interval_settings.PhaseOffset;

            scrollIntensity.Value = intensityDisplay1.Threshold = m_interval_settings.IntensityThreshold;
            numMinInterval.Value = intensityDisplay1.MinIntervals = m_interval_settings.IntensityMinInterval;
            bpFilterIntensity.LoCutoff = intensityDisplay1.LoCutoff = m_interval_settings.IntensityLoCutoff;
            bpFilterIntensity.HiCutoff = intensityDisplay1.HiCutoff = m_interval_settings.IntensityHiCutoff;

            brightDisplay1.Fps = intensityDisplay1.Fps = m_video.Fps;
            brightDisplay1.Interval = intensityDisplay1.Interval = mModeDisplay.Interval = m_current_frame_interval;

            brightDisplay1.Data = m_preprocess_data.Brightness;
            intensityDisplay1.Data = m_preprocess_data.Intensity;

            scrollDarknessHigh.DataBindings.Add(new Binding("Value", brightDisplay1, "HiThreshold", false, DataSourceUpdateMode.OnPropertyChanged));
            scrollDarknessLow.DataBindings.Add(new Binding("Value", brightDisplay1, "LoThreshold", false, DataSourceUpdateMode.OnPropertyChanged));
            chkUseDarkness.DataBindings.Add(new Binding("Checked", brightDisplay1, "UseDarkness", false, DataSourceUpdateMode.OnPropertyChanged));
            chkInvert.DataBindings.Add(new Binding("Checked", brightDisplay1, "Invert", false, DataSourceUpdateMode.OnPropertyChanged));
            bpFilterBright.DataBindings.Add(new Binding("HiCutoff", brightDisplay1, "HiCutoff", false, DataSourceUpdateMode.OnPropertyChanged));
            bpFilterBright.DataBindings.Add(new Binding("LoCutoff", brightDisplay1, "LoCutoff", false, DataSourceUpdateMode.OnPropertyChanged));

            numMinInterval.DataBindings.Add(new Binding("Value", intensityDisplay1, "MinIntervals", false, DataSourceUpdateMode.OnPropertyChanged));
            scrollIntensity.DataBindings.Add(new Binding("Value", intensityDisplay1, "Threshold", false, DataSourceUpdateMode.OnPropertyChanged));
            bpFilterIntensity.DataBindings.Add(new Binding("HiCutoff", intensityDisplay1, "HiCutoff", false, DataSourceUpdateMode.OnPropertyChanged));
            bpFilterIntensity.DataBindings.Add(new Binding("LoCutoff", intensityDisplay1, "LoCutoff", false, DataSourceUpdateMode.OnPropertyChanged));

            checkBox1.DataBindings.Add(new Binding("Checked", mModeDisplay, "ShowChangeIntervals", false, DataSourceUpdateMode.OnPropertyChanged));
            numericUpDown1.DataBindings.Add(new Binding("Value", mModeDisplay, "PhaseOffset", false, DataSourceUpdateMode.OnPropertyChanged));

            brightDisplay1.IntervalsChanged += new EventHandler(IntervalsChanged);
            intensityDisplay1.IntervalsChanged += new EventHandler(IntervalsChanged);
            brightDisplay1.ThresholdsChanged += new EventHandler(RecalculateIntervals);
            intensityDisplay1.ThresholdsChanged += new EventHandler(RecalculateIntervals);
            intensityDisplay1.MouseMove += new MouseEventHandler(intensityDisplay1_MouseMove);
            brightDisplay1.MouseMove += new MouseEventHandler(brightDisplay1_MouseMove);
            numericUpDown1.ValueChanged += new EventHandler(numericUpDown1_ValueChanged);
            checkBox1.CheckedChanged += new EventHandler(checkBox1_CheckedChanged);

            scrollDarknessHigh.MouseEnter += new EventHandler(ScrollMouseEnter);
            scrollDarknessLow.MouseEnter += new EventHandler(ScrollMouseEnter);
            scrollIntensity.MouseEnter += new EventHandler(ScrollMouseEnter);

            mModeDisplay.Invalidate();

            if (m_interval_data.Intervals == null || m_interval_data.Intervals.Count == 0) RecalculateIntervals(this, new EventArgs());
        }

        void frmInterval_ResizeEnd(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        void mModeDisplay_MouseMove(object sender, MouseEventArgs e)
        {
            lblCoordinates.Text = "X: " + (mModeDisplay.GetXValue(e.X) + (m_current_frame_interval.Interval - 1) * 1000) + "; Y:" + (mModeDisplay.Height - e.Y);
        }

        void intensityDisplay1_MouseMove(object sender, MouseEventArgs e)
        {
            double[] point = intensityDisplay1.GetValues(e.X, m_current_frame_interval.Interval, m_current_frame_interval.IntervalSize);
            lblCoordinates.Text = "Frame: " + point[0] + "; Value: " + point[1];
        }
        
        void brightDisplay1_MouseMove(object sender, MouseEventArgs e)
        {
            double[] point = brightDisplay1.GetValues(e.X, m_current_frame_interval.Interval, m_current_frame_interval.IntervalSize);
            lblCoordinates.Text = "Frame: " + point[0] + "; Value: " + point[1];
        }

        void cmbROI_SelectedIndexChanged(object sender, EventArgs e)
        {
            //NumeralStats ROI1_NormalizationFactors = brightDisplay1.m_stats;

            //If the "New" ROI is the same as the old, do nothing and exit
            if (m_current_roi == cmbROI.SelectedIndex)
                return;

            #region Save Current ROI Interval Data and Settings

            if(m_interval_output.Data.ElementAt(m_current_roi) == null)
                m_interval_output.Data.Insert(m_current_roi,new IntervalPhaseData());
                
            (m_interval_output.Data[m_current_roi] as IntervalPhaseData).Intervals = mModeDisplay.List;


            m_interval_settings.BrightThresholdHi = brightDisplay1.HiThreshold;
            m_interval_settings.BrightThresholdLo = brightDisplay1.LoThreshold;
            m_interval_settings.Invert = brightDisplay1.Invert;
            m_interval_settings.UseBright = brightDisplay1.UseDarkness;
            m_interval_settings.BrightLoCutoff = brightDisplay1.LoCutoff;
            m_interval_settings.BrightHiCutoff = brightDisplay1.HiCutoff;

            m_interval_settings.IntensityThreshold = intensityDisplay1.Threshold;
            m_interval_settings.IntensityMinInterval = intensityDisplay1.MinIntervals;
            m_interval_settings.IntensityLoCutoff = intensityDisplay1.LoCutoff;
            m_interval_settings.IntensityHiCutoff = intensityDisplay1.HiCutoff;
            
            
            m_interval_output.Settings[m_current_roi] = m_interval_settings;

            #endregion

            m_current_roi = cmbROI.SelectedIndex;

            #region Load New ROI Interval Data and Settings

            m_interval_data = m_interval_output.Data[m_current_roi] as IntervalPhaseData;
            m_interval_settings = m_interval_output.Settings[m_current_roi] as IntervalPhaseSettings;
            m_preprocess_data = (m_filegram.Phases[1] as PreprocessPhaseOutput).Data[m_current_roi] as PreprocessPhaseData;

            mModeThumbnail.XCoord = m_preprocess_data.MMode.XCoordinate;

            //Set Defaults
            scrollDarknessHigh.Value = brightDisplay1.HiThreshold = m_interval_settings.BrightThresholdHi;
            scrollDarknessLow.Value = brightDisplay1.LoThreshold = m_interval_settings.BrightThresholdLo;
            chkInvert.Checked = brightDisplay1.Invert = m_interval_settings.Invert;
            chkUseDarkness.Checked = brightDisplay1.UseDarkness = m_interval_settings.UseBright;
            bpFilterBright.LoCutoff = brightDisplay1.LoCutoff = m_interval_settings.BrightLoCutoff;
            bpFilterBright.HiCutoff = brightDisplay1.HiCutoff = m_interval_settings.BrightHiCutoff;

            scrollIntensity.Value = intensityDisplay1.Threshold = m_interval_settings.IntensityThreshold;
            numMinInterval.Value = intensityDisplay1.MinIntervals = m_interval_settings.IntensityMinInterval;
            bpFilterIntensity.LoCutoff = intensityDisplay1.LoCutoff = m_interval_settings.IntensityLoCutoff;
            bpFilterIntensity.HiCutoff = intensityDisplay1.HiCutoff = m_interval_settings.IntensityHiCutoff;

            brightDisplay1.Data = m_preprocess_data.Brightness;
            intensityDisplay1.Data = m_preprocess_data.Intensity;
            mModeDisplay.MMode = m_preprocess_data.MMode;

            //brightDisplay1.m_stats = ROI1_NormalizationFactors;
            //brightDisplay1.m_stats.Set = true;

            brightDisplay1.Invalidate();
            intensityDisplay1.Invalidate();
            mModeDisplay.Invalidate();

            #endregion
        }

        void ScrollMouseEnter(object sender, EventArgs e)
        {
            (sender as ScrollBar).Focus();
        }

        void IntervalsChanged(object sender, EventArgs e)
        {
        }

        void RecalculateIntervals(object sender, EventArgs e)
        {
            int thresh = intensityDisplay1.MinIntervals;

            List<Interval> intensity_intervals = intensityDisplay1.GetIntervals();

            List<Interval> bright_intervals = brightDisplay1.GetIntervals();

            //mModeDisplay.List = new IntervalCollection(bright_intervals);
            //return;

            IntervalCollection l_list = new IntervalCollection(intensity_intervals);
            double[] l_darkness = new double[brightDisplay1.Data.Count];
            brightDisplay1.Data.CopyTo(l_darkness);
            List<double> tempBright = l_darkness.ToList();
            //bpFilterControl1.RunFilter(ref tempBright);
            brightDisplay1.PreFilter(ref tempBright);
            tempBright.CopyTo(l_darkness);

            double[] l_intensity = new double[intensityDisplay1.Data.Count];
            intensityDisplay1.Data.CopyTo(l_intensity);
            List<double> tempIntensity = l_intensity.ToList();
            intensityDisplay1.PreFilter(ref tempIntensity);
            tempIntensity.CopyTo(l_intensity);

            #region If use darkness...
            if (brightDisplay1.UseDarkness)
            {
                double lo = 1 - brightDisplay1.HiThreshold / 100.0;
                double hi = 1 - brightDisplay1.LoThreshold / 100.0;

                /*  Remove all diastoles whose mean brightness value (MBV) is greater than the low threshold
                 * 
                 * 
                 */

                for (int i = 0; i < (l_list.Count - 1); i++)
                {
                    if (l_list[i].IntervalType == IntervalType.Diastole)
                    {
                        double MBV = 0; int ct = 0;
                        for (int j = l_list[i].Frame; j <= l_list[i + 1].Frame; j++)
                        {
                            ct++;
                            MBV += l_darkness[j];// (1 - l_darkness[j]);
                        }
                        MBV /= ct;

                        if (MBV > lo)
                        {
                            l_list.RemoveAt(i);
                            l_list.RemoveAt(i);
                            i--;
                        }
                    }
                }

                /*  Remove all systoles whose mean brightness value (MBV) is less than the high threshold
                 * 
                 * 
                 */

                for (int i = 0; i < (l_list.Count - 1); i++)
                {
                    if (l_list[i].IntervalType == IntervalType.Systole)
                    {
                        double MBV = 0; int ct = 0;
                        for (int j = l_list[i].Frame; j <= l_list[i + 1].Frame; j++)
                        {
                            ct++;
                            MBV += l_darkness[j];// (1 - l_darkness[j]);
                        }
                        MBV /= ct;

                        if (MBV < hi)
                        {
                            l_list.RemoveAt(i);
                            l_list.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
            #endregion
            else
            {
                //Remove all intervals with length less than minimum interval
                for (int j = 0; j < l_list.Count - 1; j++)
                {
                    if (l_list[j + 1].Frame - l_list[j].Frame < thresh)
                    {
                        if (l_list[j + 1].IntervalType == IntervalType.Systole &&
                            l_list[j].IntervalType == IntervalType.Diastole)
                        {
                            //Remove both frames involved in the interval//
                            l_list.RemoveAt(j);
                            l_list.RemoveAt(j);
                            j--;
                        }
                    }
                }
            }

            int min_interval_length = (int)(0.05 * (double)(m_video.Fps));
            for (int j = 0; j < l_list.Count - 1; j++)
            {
                if (l_list[j + 1].Frame - l_list[j].Frame < min_interval_length)
                {
                    //Remove both frames involved in the interval//
                    l_list.RemoveAt(j);
                    l_list.RemoveAt(j);
                    j--;
                }
            }

            mModeDisplay.List = l_list;
            //(m_interval_data[m_cur_roi] as IntervalPhaseData).Intervals = l_list;

            #region Append Phase Intervals
            /* Taken out 2/13/2015. Not sure why it was here to begin with
             * 
            List<Interval> changes = new List<Interval>();


            IntervalCollection m_intervallist = l_list;
            //List<double> data = ImageProcessing.XExtrapolate(InitialData, Zoom);
            //PreFilter(ref data);
            //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, Zoom);

            bool m_use_threshold = false;
            int m_threshold = intensityDisplay1.Threshold;

            if(!m_use_threshold && m_intervallist.Count > 1)
            {
                //List<double> data = InitialData;
                //PreFilter(ref data);
                //IntervalCollection tmp_intervallist = IntervalCollection.XExtrapolateInverse(m_intervallist, this.Zoom);
                //List<double> temp_data = ImageProcessing.XExtrapolateInverse(data, this.Zoom);
                int start = m_intervallist.IndexOf(m_intervallist.First(x => x.IntervalType == IntervalType.Systole));

                // Start at 1; skip first interval; ensure accuracy 
                for(int i = start; i < m_intervallist.Count; i++)
                {
                    if(m_intervallist[i].IntervalType == IntervalType.Systole)
                    {
                        double lo = 1.0f; double hi = 0.0f; int index_lo = 0; int index_hi = 0;
                        for(int j = m_intervallist[i].Frame + (int)numericUpDown1.Value; j <= m_intervallist[i + 1].Frame - (int)numericUpDown1.Value; j++)
                        {

                            //Find Minimum
                            if(l_intensity[j] < lo)
                            {
                                lo = l_intensity[j];
                                index_lo = j + 1;
                            }
                        }

                        for(int j = m_intervallist[i].Frame; j <= m_intervallist[i + 1].Frame; j++)
                        {
                            //Find First Maximum
                            if(l_intensity[j] > hi)
                            {
                                hi = l_intensity[j];
                                index_hi = j;
                            }
                        }

                        if(index_lo != 0)
                        {
                            changes.Add(new Interval(index_lo / 1, IntervalType.Change));
                            //changes.Add(new Interval(index_hi / 1, IntervalType.MaxVelocity));
                        }
                    }
                }

                //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, this.Zoom);
            }
            else
            {
                //List<double> data = InitialData;
                //PreFilter(ref data);
                //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, Zoom);
                double threshold = (100 - m_threshold) / 100.0;
                int offset = (int)numericUpDown1.Value;

                int total_beats = (from interval in m_intervallist
                                   where interval.IntervalType == IntervalType.Systole
                                   select interval).Count();

                IEnumerable<Interval> systoles;
                if(total_beats >= 5)
                {
                    systoles = (from interval in m_intervallist
                                where interval.IntervalType == IntervalType.Systole
                                select interval).Take(5);
                }
                else
                {
                    systoles = (from interval in m_intervallist
                                where interval.IntervalType == IntervalType.Systole
                                select interval);
                }

                foreach(Interval systole in systoles)
                {
                    Interval diastole = (from dias in m_intervallist
                                         where dias.IntervalType == IntervalType.Diastole && dias.Frame > systole.Frame
                                         select dias).First();

                    int max_index = systole.Frame + offset;

                    //if (max_index > data.Count)
                    //    max_index = data.Count - 1;

                    double max = l_intensity[max_index];
                    for(int i = systole.Frame + offset; i <= diastole.Frame - offset; i++)
                    {
                        double left = l_intensity[i - 1];
                        double right = l_intensity[i];
                        if((left < threshold && right > threshold) ||
                            (left > threshold && right < threshold))
                        {
                            //Interval new_interval = new SOHA.Library.Interval(
                            changes.Add(new Interval(i / 1, IntervalType.Change));
                        }

                        if(right > max)
                        {
                            max_index = i;
                            max = l_intensity[max_index];
                        }
                    }
                    changes.Add(new Interval(max_index / 1, IntervalType.MaxVelocity));
                }

                //changes = IntervalCollection.XExtrapolate(new IntervalCollection(changes), Zoom).ToList();
            }

            l_list.Union(new IntervalCollection(changes));
            l_list.Sort();
            */
            #endregion


            mModeDisplay.List = l_list;
        }

        void mModeThumbnail_XCoordChanged(object sender, MouseEventArgs e)
        {
            if (m_collecting_mmode) return;

            m_collecting_mmode = true;

            int new_x_coord = (sender as MModeThumbnail).XCoord;

            m_video.GetMMode(new_x_coord, MModeType.Full);
        }

        void m_video_MModeComplete(object sender, MMode mmode)
        {
            m_collecting_mmode = false;

            if (mmode == null) return;

            (m_filegram.Phases[1].Data[0] as PreprocessPhaseData).MMode = mmode;
            mModeDisplay.MMode = mmode;
        }

        void frmInterval_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_video.MModeComplete -= m_video_MModeComplete;
        }

        void frmInterval_Resize(object sender, EventArgs e)
        {
            this.Invalidate(true);
        }

        void frmInterval_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                //case Keys.Escape:
                //    OnComplete(new PhaseCompleteEventArgs(false, PhaseType.Interval, m_interval_output));
                //    break;
                //case Keys.Space:
                //    OnComplete(new PhaseCompleteEventArgs(true, PhaseType.Interval, m_interval_output));
                //    break;
                case Keys.A:
                case Keys.Left:
                    if (m_current_frame_interval.Interval > 1)
                        m_current_frame_interval.Interval--;
                    break;

                case Keys.D:
                case Keys.Right:
                    if (m_current_frame_interval.Interval < m_video.Frames / m_current_frame_interval.IntervalSize + 1)
                        m_current_frame_interval.Interval++;
                    break;
                default:
                    break;
            }
        }

        protected override void OnComplete(object sender, PhaseCompleteEventArgs args)
        {

            args.Type = PhaseFormType.Interval;

            if(args.Accept)
            {
                //Query User about resetting all next phases in phaseoutputlist
                if(m_filegram.Phases.CheckPostPhases(m_interval_output))
                {
                    string query = "Data (Phases) that depend on the data (phase) you are changing have already been completed.\n\nWould you like to reset these phases as well?";

                    switch(MessageBox.Show(this, query, "Data Modified", MessageBoxButtons.YesNoCancel))
                    {
                        case System.Windows.Forms.DialogResult.Yes:
                            m_filegram.Phases.Clear(m_interval_output);
                            break;
                        case System.Windows.Forms.DialogResult.No:
                            break;
                        case System.Windows.Forms.DialogResult.Cancel:
                            return;
                            break;
                    }
                }

                if(m_interval_output.Data.ElementAt(m_current_roi) == null)
                    m_interval_output.Data.Insert(m_current_roi, new IntervalPhaseData());
                
                //Check for alternating Intervals
                if(!mModeDisplay.List.IsAlternating)
                {
                    this.WindowState = FormWindowState.Minimized;
                    string text = "The accepted Interval Collection [" + mModeDisplay.List.Count + " elements] does not appear to alternate systole/diastole."
                        + "\n\nWould you like to continue?";
                    DialogResult result = MessageBox.Show(Globals.MainInstance, text, "Possible Error Detected", MessageBoxButtons.OKCancel);
                    if(result == DialogResult.Cancel)
                    {
                        this.WindowState = FormWindowState.Maximized;
                        return;
                    }
                }

                (m_interval_output.Data[m_current_roi] as IntervalPhaseData).Intervals = mModeDisplay.List;
                IntervalPhaseSettings interval_settings = new IntervalPhaseSettings();
                mModeDisplay.List.Run(m_video.FrameTimes);


                interval_settings.BrightThresholdHi = brightDisplay1.HiThreshold;
                interval_settings.BrightThresholdLo = brightDisplay1.LoThreshold;
                interval_settings.Invert = brightDisplay1.Invert;
                interval_settings.UseBright = brightDisplay1.UseDarkness;
                interval_settings.BrightLoCutoff = brightDisplay1.LoCutoff;
                interval_settings.BrightHiCutoff = brightDisplay1.HiCutoff;
                interval_settings.ShowPhase = mModeDisplay.ShowChangeIntervals;
                interval_settings.PhaseOffset = mModeDisplay.PhaseOffset;

                interval_settings.IntensityThreshold = intensityDisplay1.Threshold;
                interval_settings.IntensityMinInterval = intensityDisplay1.MinIntervals;
                interval_settings.IntensityLoCutoff = intensityDisplay1.LoCutoff;
                interval_settings.IntensityHiCutoff = intensityDisplay1.HiCutoff;


                m_interval_output.Settings[m_current_roi] = interval_settings;

                IntervalPhaseOutput output = new IntervalPhaseOutput();
                IntervalPhaseData data = new IntervalPhaseData();

                //IntervalCollection.CurveFitIntervals(mModeDisplay.List, intensityDisplay1.LastData);

                Globals.SaveMMode(mModeDisplay.MMode.ToBitmap(), m_filegram.Name + "_Full.bmp");
            }

            m_filegram.Phases[2].Status = args.Accept ? PhaseStatus.OK : PhaseStatus.Reject;

            base.OnComplete(sender, args);
        }

        public static frmIntervalNew Spawn(Filegram fgram)
        {
            return new frmIntervalNew(fgram);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            RecalculateIntervals(sender, e);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            RecalculateIntervals(sender, e);
        }

        private void cmdAccept_Click(object sender, EventArgs e)
        {

        }
    }
}
