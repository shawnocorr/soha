﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SOHA.Library;
using System.Globalization;
using System.Threading;

namespace SOHA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            InputLanguage l = Application.CurrentInputLanguage;
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            try
            {
                //Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
                Globals.Initialize();
            }
            catch (Exception exc)
            {
                //Logs.LogTrace("Initialize FAILED.");
                //Logs.LogException(exc);
                MessageBox.Show(exc.ToString());
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new frmIntervalNew());
            Application.Run(new frmMain());
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logs.LogTrace("Unhandled Exception.");
            Logs.LogException(e.ExceptionObject as Exception);
        }
    }
}
