﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;

namespace SOHA
{
    public partial class frmCameraSelect : Form
    {

        private Camera[] original_cameras;

        public frmCameraSelect()
        {
            InitializeComponent();

            original_cameras = new Camera[CameraCollection.List.Count];
            CameraCollection.List.CopyTo(original_cameras, 0);
            dataGridView1.DataSource = CameraCollection.List;
            dataGridView1.CellBeginEdit += new DataGridViewCellCancelEventHandler(dataGridView1_CellBeginEdit);
            dataGridView1.Columns["ScaleFactor"].HeaderText = "Pixel to Micron Conversion";
            cmdOk.Click += new EventHandler(cmdOk_Click);
        }

        void cmdOk_Click(object sender, EventArgs e)
        {
            bool default_chosen = false;
            foreach (Camera cam in CameraCollection.List)
            {
                if (cam.Default)
                    default_chosen = true;
            }

            if (!default_chosen)
            {
                MessageBox.Show("You Must Select a Default Camera.", "Default Not Selected", MessageBoxButtons.OK);
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }

            CameraCollection.Write();
        }

        void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                foreach (Camera cam in CameraCollection.List)
                {
                    cam.Default = false;
                }
            }

            this.BindingContext[this.dataGridView1.DataSource].EndCurrentEdit();
            //this.BindingContext[this.dataGridView1.DataSource, "MemberName"].EndCurrentEdit();
            this.dataGridView1.Refresh(); // Make sure this comes first
            this.dataGridView1.Parent.Refresh(); // Make sure this comes second

        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            //CameraCollection.List = new BindingList<Camera>(original_cameras.ToList());
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
