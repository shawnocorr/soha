﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using SOHA.Library;

namespace SOHA
{
    public class Conversions
    {

        /// <summary>
        /// Creates Bitmap from Frame object
        /// </summary>
        /// <param name="fr"><code>Frame</code> object</param>
        /// <param name="scale_to_minimum">(0,1): defines multiplier of minimum in pixel scaling</param>
        /// <returns></returns>
        public static Bitmap FromFrame(Frame fr, double scale_to_minimum)
        {
            if (fr == null)
                throw new Exception("Frame object is null.");

            if (fr.PixelValues == null)
                throw new Exception("Frame object Values are null.");

            if(scale_to_minimum <= 0) scale_to_minimum = 1.0;

            Bitmap bmp = new Bitmap(fr.PixelValues.GetLength(0), fr.PixelValues.GetLength(1), PixelFormat.Format24bppRgb);

            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            double m = fr.Max();
            double min = fr.Min();
            min *= scale_to_minimum;

            for (int y = 0; y < bmpdata.Height; y++)
            {
                unsafe
                {
                    byte* row = (byte*)bmpdata.Scan0 + (y * bmpdata.Stride);
                    for (int x = 0; x < bmpdata.Width; x++)
                    {
                        int val = (int)((fr[x, y] - min) / (m - min) * 255);
                        byte b = (byte)val;
                        row[x * 3] = (byte)val;
                        row[x * 3 + 1] = (byte)val;
                        row[x * 3 + 2] = (byte)val;
                    }
                }
            }

            bmp.UnlockBits(bmpdata);

            return bmp;
        }
    }
}
