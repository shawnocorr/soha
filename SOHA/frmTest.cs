﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Video;
using SOHA.Library;
using SOHA.Library.Mathematics;
using SOHA.Library.Imaging;

namespace SOHA
{
    public partial class frmTest : Form
    {
        public frmTest()
        {
            InitializeComponent();
        }

        private void filterDisplayPanel1_FilterUpdate(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void frmTest_Load(object sender, EventArgs e)
        {

            List<ImageFilter> filters = new List<ImageFilter>();
            filters.Add(new ImageFilter(Matrix.TestMatrix));
            //filters.Add(new ImageFilter(Matrix.TestMatrix));

            //filterDisplayPanel1.SetFilters(filters);

        }
    }
}
