﻿using SOHAControls;
using SOHA.SohaControls;
namespace SOHA
{
    partial class frmInterval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInterval));
            SOHAControls.IntervalProperties intervalProperties1 = new SOHAControls.IntervalProperties();
            SOHAControls.IntervalProperties intervalProperties2 = new SOHAControls.IntervalProperties();
            SOHAControls.IntervalProperties intervalProperties3 = new SOHAControls.IntervalProperties();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.mModeThumbnail = new SOHAControls.MModeThumbnail(this.components);
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.scrollDarknessHigh = new System.Windows.Forms.VScrollBar();
            this.scrollDarknessLow = new System.Windows.Forms.VScrollBar();
            this.chkInvert = new System.Windows.Forms.CheckBox();
            this.chkUseDarkness = new System.Windows.Forms.CheckBox();
            this.bpFilterBright = new SOHAControls.BandPassFilterControl();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.scrollIntensity = new System.Windows.Forms.VScrollBar();
            this.label4 = new System.Windows.Forms.Label();
            this.numMinInterval = new SOHA.SohaControls.SilentNumericUpDown(this.components);
            this.bpFilterIntensity = new SOHAControls.BandPassFilterControl();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdQuit = new System.Windows.Forms.Button();
            this.cmdReset = new System.Windows.Forms.Button();
            this.cmdDiscard = new System.Windows.Forms.Button();
            this.cmdAccept = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbROI = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.numericUpDown1 = new SOHA.SohaControls.SilentNumericUpDown(this.components);
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.mModeDisplay = new Forms.MModeDisplay(this.components);
            this.brightDisplay1 = new SOHAControls.BrightDisplay();
            this.intensityDisplay1 = new SOHAControls.IntensityDisplay();
            this.toolStripContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinInterval)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1272, 559);
            this.toolStripContainer1.Size = new System.Drawing.Size(1272, 559);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1272, 559);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.mModeThumbnail, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel6, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel7, 0, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(975, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(294, 553);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // mModeThumbnail
            // 
            this.mModeThumbnail.BackColor = System.Drawing.Color.White;
            this.mModeThumbnail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mModeThumbnail.BackgroundImage")));
            this.mModeThumbnail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mModeThumbnail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mModeThumbnail.Location = new System.Drawing.Point(3, 3);
            this.mModeThumbnail.Name = "mModeThumbnail";
            this.mModeThumbnail.Size = new System.Drawing.Size(288, 104);
            this.mModeThumbnail.TabIndex = 0;
            this.mModeThumbnail.TabStop = false;
            this.mModeThumbnail.Thumbnail = ((System.Drawing.Bitmap)(resources.GetObject("mModeThumbnail.Thumbnail")));
            this.mModeThumbnail.XCoord = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.scrollDarknessHigh, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.scrollDarknessLow, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.chkInvert, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.chkUseDarkness, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.bpFilterBright, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 113);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(288, 104);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Low";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "High";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scrollDarknessHigh
            // 
            this.scrollDarknessHigh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrollDarknessHigh.Location = new System.Drawing.Point(11, 20);
            this.scrollDarknessHigh.Maximum = 111;
            this.scrollDarknessHigh.Name = "scrollDarknessHigh";
            this.tableLayoutPanel4.SetRowSpan(this.scrollDarknessHigh, 3);
            this.scrollDarknessHigh.Size = new System.Drawing.Size(17, 84);
            this.scrollDarknessHigh.TabIndex = 2;
            // 
            // scrollDarknessLow
            // 
            this.scrollDarknessLow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrollDarknessLow.Location = new System.Drawing.Point(51, 20);
            this.scrollDarknessLow.Maximum = 111;
            this.scrollDarknessLow.Name = "scrollDarknessLow";
            this.tableLayoutPanel4.SetRowSpan(this.scrollDarknessLow, 3);
            this.scrollDarknessLow.Size = new System.Drawing.Size(17, 84);
            this.scrollDarknessLow.TabIndex = 3;
            // 
            // chkInvert
            // 
            this.chkInvert.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkInvert.AutoSize = true;
            this.chkInvert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkInvert.Location = new System.Drawing.Point(83, 47);
            this.chkInvert.Name = "chkInvert";
            this.chkInvert.Size = new System.Drawing.Size(202, 24);
            this.chkInvert.TabIndex = 4;
            this.chkInvert.Text = "Invert";
            this.chkInvert.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chkInvert.UseVisualStyleBackColor = true;
            // 
            // chkUseDarkness
            // 
            this.chkUseDarkness.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUseDarkness.AutoSize = true;
            this.chkUseDarkness.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkUseDarkness.Location = new System.Drawing.Point(83, 77);
            this.chkUseDarkness.Name = "chkUseDarkness";
            this.chkUseDarkness.Size = new System.Drawing.Size(202, 24);
            this.chkUseDarkness.TabIndex = 5;
            this.chkUseDarkness.Text = "Use Darkness";
            this.chkUseDarkness.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chkUseDarkness.UseVisualStyleBackColor = true;
            // 
            // bpFilterBright
            // 
            this.bpFilterBright.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bpFilterBright.Filter = null;
            this.bpFilterBright.HiCutoff = 0D;
            this.bpFilterBright.Location = new System.Drawing.Point(83, 3);
            this.bpFilterBright.LoCutoff = 0D;
            this.bpFilterBright.Name = "bpFilterBright";
            this.tableLayoutPanel4.SetRowSpan(this.bpFilterBright, 2);
            this.bpFilterBright.Size = new System.Drawing.Size(202, 38);
            this.bpFilterBright.TabIndex = 6;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.scrollIntensity, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label4, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.numMinInterval, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.bpFilterIntensity, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 223);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(288, 104);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Thresh.";
            // 
            // scrollIntensity
            // 
            this.scrollIntensity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrollIntensity.Location = new System.Drawing.Point(16, 32);
            this.scrollIntensity.Maximum = 111;
            this.scrollIntensity.Name = "scrollIntensity";
            this.tableLayoutPanel5.SetRowSpan(this.scrollIntensity, 3);
            this.scrollIntensity.Size = new System.Drawing.Size(17, 72);
            this.scrollIntensity.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(138, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Min Interval";
            // 
            // numMinInterval
            // 
            this.numMinInterval.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numMinInterval.Location = new System.Drawing.Point(149, 87);
            this.numMinInterval.Name = "numMinInterval";
            this.numMinInterval.Size = new System.Drawing.Size(40, 20);
            this.numMinInterval.TabIndex = 3;
            // 
            // bpFilterIntensity
            // 
            this.bpFilterIntensity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bpFilterIntensity.Filter = null;
            this.bpFilterIntensity.HiCutoff = 0D;
            this.bpFilterIntensity.Location = new System.Drawing.Point(53, 3);
            this.bpFilterIntensity.LoCutoff = 0D;
            this.bpFilterIntensity.Name = "bpFilterIntensity";
            this.tableLayoutPanel5.SetRowSpan(this.bpFilterIntensity, 2);
            this.bpFilterIntensity.Size = new System.Drawing.Size(232, 58);
            this.bpFilterIntensity.TabIndex = 4;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.Controls.Add(this.cmdQuit, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.cmdReset, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdDiscard, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdAccept, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 333);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(288, 104);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // cmdQuit
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.cmdQuit, 3);
            this.cmdQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdQuit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdQuit.Location = new System.Drawing.Point(3, 55);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(282, 46);
            this.cmdQuit.TabIndex = 3;
            this.cmdQuit.Text = "Quit";
            this.cmdQuit.UseVisualStyleBackColor = true;
            // 
            // cmdReset
            // 
            this.cmdReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdReset.Location = new System.Drawing.Point(195, 3);
            this.cmdReset.Name = "cmdReset";
            this.cmdReset.Size = new System.Drawing.Size(90, 46);
            this.cmdReset.TabIndex = 2;
            this.cmdReset.Text = "Reset";
            this.cmdReset.UseVisualStyleBackColor = true;
            // 
            // cmdDiscard
            // 
            this.cmdDiscard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDiscard.Location = new System.Drawing.Point(99, 3);
            this.cmdDiscard.Name = "cmdDiscard";
            this.cmdDiscard.Size = new System.Drawing.Size(90, 46);
            this.cmdDiscard.TabIndex = 1;
            this.cmdDiscard.Text = "Discard";
            this.cmdDiscard.UseVisualStyleBackColor = true;
            // 
            // cmdAccept
            // 
            this.cmdAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAccept.Location = new System.Drawing.Point(3, 3);
            this.cmdAccept.Name = "cmdAccept";
            this.cmdAccept.Size = new System.Drawing.Size(90, 46);
            this.cmdAccept.TabIndex = 0;
            this.cmdAccept.Text = "Accept";
            this.cmdAccept.UseVisualStyleBackColor = true;
            this.cmdAccept.Click += new System.EventHandler(this.cmdAccept_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.cmbROI, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.checkBox1, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.numericUpDown1, 1, 1);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 443);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(288, 107);
            this.tableLayoutPanel7.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(115, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "ROI";
            // 
            // cmbROI
            // 
            this.cmbROI.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cmbROI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbROI.FormattingEnabled = true;
            this.cmbROI.Location = new System.Drawing.Point(147, 16);
            this.cmbROI.Name = "cmbROI";
            this.cmbROI.Size = new System.Drawing.Size(121, 21);
            this.cmbROI.TabIndex = 1;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(3, 56);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(147, 56);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.mModeDisplay, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.brightDisplay1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.intensityDisplay1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(966, 553);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // mModeDisplay
            // 
            this.mModeDisplay.BackColor = System.Drawing.Color.White;
            this.mModeDisplay.DisplayCoordinates = false;
            this.mModeDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            intervalProperties1.Interval = 1;
            intervalProperties1.IntervalSize = 1000;
            this.mModeDisplay.Interval = intervalProperties1;
            this.mModeDisplay.List = null;
            this.mModeDisplay.Location = new System.Drawing.Point(3, 371);
            this.mModeDisplay.MMode = null;
            this.mModeDisplay.Name = "mModeDisplay";
            this.mModeDisplay.Phase = SOHA.Library.PhaseFormType.Interval;
            this.mModeDisplay.PhaseOffset = 0;
            this.mModeDisplay.ShowChangeIntervals = false;
            this.mModeDisplay.Size = new System.Drawing.Size(960, 179);
            this.mModeDisplay.TabIndex = 0;
            this.mModeDisplay.TabStop = false;
            this.mModeDisplay.Zoom = 1;
            // 
            // brightDisplay1
            // 
            this.brightDisplay1.Data = null;
            this.brightDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.brightDisplay1.Filter = null;
            this.brightDisplay1.FilterFrequency = 0D;
            this.brightDisplay1.Fps = 0D;
            this.brightDisplay1.HiCutoff = 0D;
            this.brightDisplay1.HiThreshold = 0;
            intervalProperties2.Interval = 1;
            intervalProperties2.IntervalSize = 1000;
            this.brightDisplay1.Interval = intervalProperties2;
            this.brightDisplay1.Invert = false;
            this.brightDisplay1.Location = new System.Drawing.Point(3, 3);
            this.brightDisplay1.LoCutoff = 0D;
            this.brightDisplay1.LoThreshold = 0;
            this.brightDisplay1.Name = "brightDisplay1";
            this.brightDisplay1.Size = new System.Drawing.Size(960, 178);
            this.brightDisplay1.TabIndex = 1;
            this.brightDisplay1.UseDarkness = false;
            // 
            // intensityDisplay1
            // 
            this.intensityDisplay1.Data = null;
            this.intensityDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.intensityDisplay1.Fps = 0D;
            this.intensityDisplay1.HiCutoff = 0D;
            intervalProperties3.Interval = 1;
            intervalProperties3.IntervalSize = 1000;
            this.intensityDisplay1.Interval = intervalProperties3;
            this.intensityDisplay1.Location = new System.Drawing.Point(3, 187);
            this.intensityDisplay1.LoCutoff = 0D;
            this.intensityDisplay1.MinIntervals = 0;
            this.intensityDisplay1.Name = "intensityDisplay1";
            this.intensityDisplay1.Size = new System.Drawing.Size(960, 178);
            this.intensityDisplay1.TabIndex = 2;
            this.intensityDisplay1.Threshold = 0;
            // 
            // frmInterval
            // 
            this.AcceptButton = this.cmdAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdQuit;
            this.ClientSize = new System.Drawing.Size(1272, 581);
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.Name = "frmInterval";
            this.RejectButton = this.cmdDiscard;
            this.Text = "frmInterval";
            this.Controls.SetChildIndex(this.toolStripContainer1, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinInterval)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MModeThumbnail mModeThumbnail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.VScrollBar scrollDarknessHigh;
        private System.Windows.Forms.VScrollBar scrollDarknessLow;
        private System.Windows.Forms.CheckBox chkInvert;
        private System.Windows.Forms.CheckBox chkUseDarkness;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.VScrollBar scrollIntensity;
        private Forms.MModeDisplay mModeDisplay;
        private SOHAControls.BrightDisplay brightDisplay1;
        private SOHAControls.IntensityDisplay intensityDisplay1;
        private System.Windows.Forms.Label label4;
        private SilentNumericUpDown numMinInterval;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button cmdQuit;
        private System.Windows.Forms.Button cmdReset;
        private System.Windows.Forms.Button cmdDiscard;
        private System.Windows.Forms.Button cmdAccept;
        private BandPassFilterControl bpFilterBright;
        private BandPassFilterControl bpFilterIntensity;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbROI;
        private System.Windows.Forms.CheckBox checkBox1;
        private SilentNumericUpDown numericUpDown1;
    }
}