﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Video;
using SOHA.Library.Forms;
using SOHA.Library.Xml;

namespace SOHA
{
    public partial class frmWorkBench : frmPhase
    {
        private Video m_video;

        public frmWorkBench()
        {
            InitializeComponent();
        }

        public static frmWorkBench Spawn(Filegram fg)
        {
            return new frmWorkBench(fg);
        }

        public frmWorkBench(Filegram fg)
            : base()
        {
            InitializeComponent();
            
            m_filegram = fg;
            m_filegram.Status = FilegramStatus.UserInterface;
            m_video = m_filegram.Video;
            m_video.Open();

            //this.FormClosing += new FormClosingEventHandler(frmAutoPart_FormClosing);

            ucSequentialFilterLayout1.Updated += new EventHandler(ucSequentialFilterLayout1_Updated);

            frameDisplay1.VideoFile = m_video;
            frameDisplay1.Paint += new PaintEventHandler(frameDisplay1_Paint);
            frameDisplay1.FrameChanged += new EventHandler<FrameEventArgs>(frameDisplay1_FrameChanged);
            frameDisplay1.HistogramUpdated += new EventHandler(frameDisplay1_HistogramUpdated);

        }

        void frameDisplay1_HistogramUpdated(object sender, EventArgs e)
        {
            if(frameDisplay1.PostOpFrame != null)
            {
                ucHistogram1.SetFrame(frameDisplay1.PostOpFrame);
            }
            else
            {
                ucHistogram1.SetFrame(frameDisplay1.CurrentFrame);
            }
        }

        void ucSequentialFilterLayout1_Updated(object sender, EventArgs e)
        {
            frameDisplay1.Filters = ucSequentialFilterLayout1.GetFilters;
        }

        void frameDisplay1_FrameChanged(object sender, EventArgs e)
        {
            //frameDisplay1.ReDraw();
            ucHistogram1.SetFrame(frameDisplay1.CurrentFrame);
            //frameDisplay1.Invalidate(true);
            //this.Update();

            OnUpdateStatus();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            frameDisplay1.KeyPress(e);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            frameDisplay1.EnableFilters = checkBox1.Checked;
        }

        private void OnUpdateStatus()
        {
            statusStrip1.Items.Clear();

            string FRAME_NUMBER = "Frame #" + frameDisplay1.CurrentFrame.FrameNumber;
            string LAST_FILTER_TIME = " Filter Time [" + frameDisplay1.LastFilterWatch.ElapsedMilliseconds + " ms]";

            statusStrip1.Items.Add(FRAME_NUMBER + "; " + LAST_FILTER_TIME);
        }

        private void frameDisplay1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            (m_filegram.Phases[PhaseName.Preprocess].Options[0] as PreprocessPhaseOptions).Filters = ucSequentialFilterLayout1.GetFilters;
            (m_filegram.Phases[PhaseName.Preprocess].Options[0] as PreprocessPhaseOptions).EnableFilters = true;
        }
    }
}
