﻿namespace SOHA
{
    partial class frmTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.filterDisplayPanel1 = new SOHAControls.FilterDisplayPanel(this.components);
            this.SuspendLayout();
            // 
            // filterDisplayPanel1
            // 
            this.filterDisplayPanel1.AllowDrop = true;
            this.filterDisplayPanel1.Location = new System.Drawing.Point(633, 12);
            this.filterDisplayPanel1.Name = "filterDisplayPanel1";
            this.filterDisplayPanel1.Size = new System.Drawing.Size(200, 358);
            this.filterDisplayPanel1.TabIndex = 0;
            this.filterDisplayPanel1.FilterUpdate += new System.EventHandler(this.filterDisplayPanel1_FilterUpdate);
            // 
            // frmTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 415);
            this.Controls.Add(this.filterDisplayPanel1);
            this.Name = "frmTest";
            this.Text = "frmTest";
            this.Load += new System.EventHandler(this.frmTest_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SOHAControls.DisplayControls.FrameDisplay frameDisplay1;
        private SOHAControls.FilterDisplayPanel filterDisplayPanel1;


    }
}