﻿using SOHAControls;
using SOHA.SohaControls;
namespace SOHA
{
    partial class frmIntervalNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIntervalNew));
            SOHAControls.IntervalProperties intervalProperties1 = new SOHAControls.IntervalProperties();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.mModeThumbnail = new SOHAControls.MModeThumbnail(this.components);
            this.mModeDisplay = new Forms.MModeDisplay(this.components);
            this.ucBrightFunctionControlDisplay1 = new SOHAControls.DisplayControls.ucBrightFunctionControlDisplay();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdReset = new System.Windows.Forms.Button();
            this.cmdDiscard = new System.Windows.Forms.Button();
            this.cmdAccept = new System.Windows.Forms.Button();
            this.cmdQuit = new System.Windows.Forms.Button();
            this.ucIntensityFunctionControlDisplay1 = new SOHAControls.DisplayControls.ucIntensityFunctionControlDisplay();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.ddROI = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tableLayoutPanel3);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1100, 577);
            this.toolStripContainer1.Size = new System.Drawing.Size(1100, 577);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.Controls.Add(this.mModeThumbnail, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.mModeDisplay, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.ucBrightFunctionControlDisplay1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.ucIntensityFunctionControlDisplay1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1100, 577);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // mModeThumbnail
            // 
            this.mModeThumbnail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mModeThumbnail.BackColor = System.Drawing.Color.White;
            this.mModeThumbnail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mModeThumbnail.BackgroundImage")));
            this.mModeThumbnail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mModeThumbnail.Location = new System.Drawing.Point(903, 362);
            this.mModeThumbnail.Name = "mModeThumbnail";
            this.mModeThumbnail.Size = new System.Drawing.Size(194, 150);
            this.mModeThumbnail.TabIndex = 0;
            this.mModeThumbnail.TabStop = false;
            this.mModeThumbnail.Thumbnail = ((System.Drawing.Bitmap)(resources.GetObject("mModeThumbnail.Thumbnail")));
            this.mModeThumbnail.XCoord = 0;
            // 
            // mModeDisplay
            // 
            this.mModeDisplay.BackColor = System.Drawing.Color.White;
            this.mModeDisplay.DisplayCoordinates = false;
            this.mModeDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            intervalProperties1.Interval = 1;
            intervalProperties1.IntervalSize = 1000;
            this.mModeDisplay.Interval = intervalProperties1;
            this.mModeDisplay.List = null;
            this.mModeDisplay.Location = new System.Drawing.Point(40, 362);
            this.mModeDisplay.Margin = new System.Windows.Forms.Padding(40, 3, 15, 3);
            this.mModeDisplay.MMode = null;
            this.mModeDisplay.Name = "mModeDisplay";
            this.mModeDisplay.Phase = SOHA.Library.PhaseFormType.Interval;
            this.mModeDisplay.PhaseOffset = 0;
            this.mModeDisplay.Range = null;
            this.mModeDisplay.ShowChangeIntervals = false;
            this.mModeDisplay.Size = new System.Drawing.Size(845, 161);
            this.mModeDisplay.TabIndex = 0;
            this.mModeDisplay.TabStop = false;
            this.mModeDisplay.Zoom = 1;
            // 
            // ucBrightFunctionControlDisplay1
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.ucBrightFunctionControlDisplay1, 2);
            this.ucBrightFunctionControlDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucBrightFunctionControlDisplay1.Location = new System.Drawing.Point(3, 28);
            this.ucBrightFunctionControlDisplay1.Name = "ucBrightFunctionControlDisplay1";
            this.ucBrightFunctionControlDisplay1.Range = null;
            this.ucBrightFunctionControlDisplay1.Size = new System.Drawing.Size(1094, 161);
            this.ucBrightFunctionControlDisplay1.TabIndex = 5;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.Controls.Add(this.cmdReset, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdDiscard, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdAccept, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdQuit, 3, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 529);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(894, 45);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // cmdReset
            // 
            this.cmdReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdReset.Location = new System.Drawing.Point(449, 3);
            this.cmdReset.Name = "cmdReset";
            this.cmdReset.Size = new System.Drawing.Size(217, 39);
            this.cmdReset.TabIndex = 2;
            this.cmdReset.Text = "Reset";
            this.cmdReset.UseVisualStyleBackColor = true;
            // 
            // cmdDiscard
            // 
            this.cmdDiscard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDiscard.Location = new System.Drawing.Point(226, 3);
            this.cmdDiscard.Name = "cmdDiscard";
            this.cmdDiscard.Size = new System.Drawing.Size(217, 39);
            this.cmdDiscard.TabIndex = 1;
            this.cmdDiscard.Text = "Discard";
            this.cmdDiscard.UseVisualStyleBackColor = true;
            // 
            // cmdAccept
            // 
            this.cmdAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAccept.Location = new System.Drawing.Point(3, 3);
            this.cmdAccept.Name = "cmdAccept";
            this.cmdAccept.Size = new System.Drawing.Size(217, 39);
            this.cmdAccept.TabIndex = 0;
            this.cmdAccept.Text = "Accept";
            this.cmdAccept.UseVisualStyleBackColor = true;
            this.cmdAccept.Click += new System.EventHandler(this.cmdAccept_Click);
            // 
            // cmdQuit
            // 
            this.cmdQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdQuit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdQuit.Location = new System.Drawing.Point(672, 3);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(219, 39);
            this.cmdQuit.TabIndex = 3;
            this.cmdQuit.Text = "Quit";
            this.cmdQuit.UseVisualStyleBackColor = true;
            // 
            // ucIntensityFunctionControlDisplay1
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.ucIntensityFunctionControlDisplay1, 2);
            this.ucIntensityFunctionControlDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucIntensityFunctionControlDisplay1.Location = new System.Drawing.Point(3, 195);
            this.ucIntensityFunctionControlDisplay1.Name = "ucIntensityFunctionControlDisplay1";
            this.ucIntensityFunctionControlDisplay1.Range = null;
            this.ucIntensityFunctionControlDisplay1.Size = new System.Drawing.Size(1094, 161);
            this.ucIntensityFunctionControlDisplay1.TabIndex = 6;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.ddROI});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1100, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(26, 22);
            this.toolStripLabel1.Text = "ROI";
            // 
            // ddROI
            // 
            this.ddROI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddROI.Name = "ddROI";
            this.ddROI.Size = new System.Drawing.Size(121, 25);
            this.ddROI.SelectedIndexChanged += new System.EventHandler(this.ddROI_SelectedIndexChanged);
            // 
            // frmIntervalNew
            // 
            this.AcceptButton = this.cmdAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdQuit;
            this.ClientSize = new System.Drawing.Size(1100, 599);
            this.Controls.Add(this.toolStrip1);
            this.KeyPreview = true;
            this.Name = "frmIntervalNew";
            this.RejectButton = this.cmdDiscard;
            this.Text = "frmInterval";
            this.Controls.SetChildIndex(this.toolStripContainer1, 0);
            this.Controls.SetChildIndex(this.toolStrip1, 0);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button cmdQuit;
        private System.Windows.Forms.Button cmdReset;
        private System.Windows.Forms.Button cmdDiscard;
        private System.Windows.Forms.Button cmdAccept;
        private MModeThumbnail mModeThumbnail;
        private Forms.MModeDisplay mModeDisplay;
        private SOHAControls.DisplayControls.ucBrightFunctionControlDisplay ucBrightFunctionControlDisplay1;
        private SOHAControls.DisplayControls.ucIntensityFunctionControlDisplay ucIntensityFunctionControlDisplay1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox ddROI;

    }
}