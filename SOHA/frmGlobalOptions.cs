﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Video;
using System.IO;

namespace SOHA
{
    public partial class frmGlobalOptions : Form
    {
        private const string AutoAnalyzeDesc = "Option to automatically gather all frames during loading of WorkBenches. Load is slower; frame access time is better. (Recommended RAM: 8+ GB.)";
        public frmGlobalOptions()
        {
            InitializeComponent();
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            Globals.Options.WriteFile();

            this.Close();
        }

        private void frmGlobalOptions_Load(object sender, EventArgs e)
        {
            //chkAutoAnalyze
            propertyGrid1.SelectedObject = Globals.Options;
        }
    }
}
