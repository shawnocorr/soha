﻿namespace SOHA
{
    partial class frmPhasePlusNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SOHAControls.IntervalProperties intervalProperties2 = new SOHAControls.IntervalProperties();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPhasePlusNew));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdReset = new System.Windows.Forms.Button();
            this.cmdDiscard = new System.Windows.Forms.Button();
            this.cmdAccept = new System.Windows.Forms.Button();
            this.cmdQuit = new System.Windows.Forms.Button();
            this.mModeDisplay1 = new Forms.MModeDisplay(this.components);
            this.ucPhaseFunctionControlDisplay1 = new SOHAControls.DisplayControls.ucPhaseFunctionControlDisplay();
            this.mModeThumbnail = new SOHAControls.MModeThumbnail(this.components);
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tableLayoutPanel1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1065, 617);
            this.toolStripContainer1.Size = new System.Drawing.Size(1065, 617);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.mModeDisplay1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucPhaseFunctionControlDisplay1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mModeThumbnail, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1065, 617);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel6, 2);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.Controls.Add(this.cmdReset, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdDiscard, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdAccept, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdQuit, 3, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 569);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1059, 45);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // cmdReset
            // 
            this.cmdReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdReset.Location = new System.Drawing.Point(531, 3);
            this.cmdReset.Name = "cmdReset";
            this.cmdReset.Size = new System.Drawing.Size(258, 39);
            this.cmdReset.TabIndex = 2;
            this.cmdReset.Text = "Reset";
            this.cmdReset.UseVisualStyleBackColor = true;
            // 
            // cmdDiscard
            // 
            this.cmdDiscard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDiscard.Location = new System.Drawing.Point(267, 3);
            this.cmdDiscard.Name = "cmdDiscard";
            this.cmdDiscard.Size = new System.Drawing.Size(258, 39);
            this.cmdDiscard.TabIndex = 1;
            this.cmdDiscard.Text = "Discard";
            this.cmdDiscard.UseVisualStyleBackColor = true;
            // 
            // cmdAccept
            // 
            this.cmdAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAccept.Location = new System.Drawing.Point(3, 3);
            this.cmdAccept.Name = "cmdAccept";
            this.cmdAccept.Size = new System.Drawing.Size(258, 39);
            this.cmdAccept.TabIndex = 0;
            this.cmdAccept.Text = "Accept";
            this.cmdAccept.UseVisualStyleBackColor = true;
            // 
            // cmdQuit
            // 
            this.cmdQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdQuit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdQuit.Location = new System.Drawing.Point(795, 3);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(261, 39);
            this.cmdQuit.TabIndex = 3;
            this.cmdQuit.Text = "Quit";
            this.cmdQuit.UseVisualStyleBackColor = true;
            // 
            // mModeDisplay1
            // 
            this.mModeDisplay1.BackColor = System.Drawing.Color.White;
            this.mModeDisplay1.DisplayCoordinates = false;
            this.mModeDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            intervalProperties2.Interval = 1;
            intervalProperties2.IntervalSize = 1000;
            this.mModeDisplay1.Interval = intervalProperties2;
            this.mModeDisplay1.List = null;
            this.mModeDisplay1.Location = new System.Drawing.Point(3, 286);
            this.mModeDisplay1.MMode = null;
            this.mModeDisplay1.Name = "mModeDisplay1";
            this.mModeDisplay1.Phase = SOHA.Library.PhaseFormType.Phase;
            this.mModeDisplay1.PhaseOffset = 0;
            this.mModeDisplay1.Range = null;
            this.mModeDisplay1.ShowChangeIntervals = true;
            this.mModeDisplay1.Size = new System.Drawing.Size(859, 277);
            this.mModeDisplay1.TabIndex = 1;
            this.mModeDisplay1.TabStop = false;
            this.mModeDisplay1.Zoom = 1;
            // 
            // ucPhaseFunctionControlDisplay1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.ucPhaseFunctionControlDisplay1, 2);
            this.ucPhaseFunctionControlDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPhaseFunctionControlDisplay1.InitialIntervals = null;
            this.ucPhaseFunctionControlDisplay1.Location = new System.Drawing.Point(3, 3);
            this.ucPhaseFunctionControlDisplay1.Name = "ucPhaseFunctionControlDisplay1";
            this.ucPhaseFunctionControlDisplay1.Range = null;
            this.ucPhaseFunctionControlDisplay1.Size = new System.Drawing.Size(1059, 277);
            this.ucPhaseFunctionControlDisplay1.TabIndex = 0;
            // 
            // mModeThumbnail
            // 
            this.mModeThumbnail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mModeThumbnail.BackColor = System.Drawing.Color.White;
            this.mModeThumbnail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mModeThumbnail.BackgroundImage")));
            this.mModeThumbnail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mModeThumbnail.Location = new System.Drawing.Point(868, 286);
            this.mModeThumbnail.Name = "mModeThumbnail";
            this.mModeThumbnail.Size = new System.Drawing.Size(194, 150);
            this.mModeThumbnail.TabIndex = 2;
            this.mModeThumbnail.TabStop = false;
            this.mModeThumbnail.Thumbnail = ((System.Drawing.Bitmap)(resources.GetObject("mModeThumbnail.Thumbnail")));
            this.mModeThumbnail.XCoord = 0;
            // 
            // frmPhasePlusNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 639);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "frmPhasePlusNew";
            this.Text = "frmPhasePlusNew";
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private SOHAControls.DisplayControls.ucPhaseFunctionControlDisplay ucPhaseFunctionControlDisplay1;
        private Forms.MModeDisplay mModeDisplay1;
        private SOHAControls.MModeThumbnail mModeThumbnail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button cmdReset;
        private System.Windows.Forms.Button cmdDiscard;
        private System.Windows.Forms.Button cmdAccept;
        private System.Windows.Forms.Button cmdQuit;
    }
}