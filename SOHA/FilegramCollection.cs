﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using SOHA.Library.Xml;
using System.IO;
using Ionic.Zip;
using System.Windows.Forms;
using SOHA.Library;
using System.ComponentModel;
using System.Diagnostics;
using SOHA.Library.Forms;

namespace SOHA
{
    [XmlRoot("FilegramCollection")]
    public class FilegramCollection : IList<Filegram>, ICollection<Filegram>
    {
        public static string CSV_DELIMITER = ";";
        public static event EventHandler LoadComplete;

        private static List<Filegram> m_list = new List<Filegram>();
        private static ProcessQueue load_queue = new ProcessQueue(true);
        private static frmProcessNotify m_process_progress = null;

        private static double TOTAL_QUEUE_SIZE = 0;

        public FilegramCollection() { }
        public FilegramCollection(string path) 
        {
            Load(path);
        }

        public static void Load(string path)
        {
            if(Globals.FilegramList.Count > 0)
            {
                
                //foreach(Filegram fg in Globals.FilegramList)
                    //fg
            }

            if(Globals.Options.OutputOn)
                (Globals.MainInstance as frmMain).output.WriteLine("Opening Folder: '" + path + "'");

            DirectoryInfo directory_info = new DirectoryInfo(Globals.CurrentDirectory);

            string[] valid_exts = new string[] { ".cxd", ".avi", ".tif" };
            List<string> files_added = new List<string>();

            IEnumerable<string> ohba_files = from FileInfo fi in directory_info.GetFiles("*.xml") select Path.GetFileNameWithoutExtension(fi.Name);

            #region Archive

            StringBuilder archive_message = new StringBuilder();
            archive_message.AppendLine(ohba_files.Count() + " Files Found:");

            bool archive_today_found = directory_info.GetFiles("*" + DateTime.Today.ToString("yyyyMMdd") + "*.zip").Count() > 0;

            archive_message.AppendLine("\nWould you like to archive(ZIP) these files?");

            if(ohba_files.Count() > 0 && !archive_today_found)
            {
                if(MessageBox.Show(mdiMain.Instance, archive_message.ToString(), "Archive?", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    string zip_path = Globals.CurrentDirectory + @"\Archive_" + DateTime.Today.ToString("yyyyMMdd") + ".zip";
                    DialogResult zip_continue = System.Windows.Forms.DialogResult.Yes;
                    if(File.Exists(zip_path))
                        zip_continue = MessageBox.Show(mdiMain.Instance, "You have created an Archive already today. Overwrite?", "Overwrite?", MessageBoxButtons.YesNo);

                    if(zip_continue == System.Windows.Forms.DialogResult.Yes)
                    {
                        using(ZipFile zip = new ZipFile())
                        {
                            foreach(string s in ohba_files)
                                zip.AddFile(Globals.CurrentDirectory + @"\" + s + ".xml", "");
                            zip.Save(Globals.CurrentDirectory + @"\Archive_" + DateTime.Today.ToString("yyyyMMdd") + ".zip");
                        }
                    }
                }
            }


            #endregion

            #region Convert Old Xml Files

            IEnumerable<string> xml_files = from FileInfo fi in directory_info.GetFiles("*.xml") select Path.GetFileNameWithoutExtension(fi.Name);
            IEnumerable<string> files_to_convert = xml_files.Where(x => !ohba_files.Contains(x));
            List<string> failed_conversions = new List<string>();

            List<string> files_convert = new List<string>();
            foreach(string s in files_to_convert)
            {
                string file = Globals.CurrentDirectory + "\\" + s + ".xml";
                if(File.Exists(file))
                    files_convert.Add(file);
            }

            if(files_convert.Count() > 0)
            {
                if(MessageBox.Show("XML Files found. Would you like to update these files?", "XML Files Found.", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int success = 0;
                    int fail = 0;
                    foreach(string s in files_convert)
                    {
                        try
                        {
                            //Filegram.ConvertFromXml(s);
                            success++;
                        }
                        catch(Exception e)
                        {
                            failed_conversions.Add(s);
                            fail++;
                        }
                    }

                    //Message Box Results
                    StringBuilder sbuild = new StringBuilder();
                    sbuild.AppendLine("Successful Conversions: " + success);
                    sbuild.AppendLine("Failed Conversions: " + fail);
                    foreach(string s in failed_conversions)
                        sbuild.AppendLine("\t" + s);

                    MessageBox.Show(Globals.MainInstance, sbuild.ToString(), "XML Conversion Results", MessageBoxButtons.OKCancel);
                }
            }

            #endregion

            #region Populate XML and CXD/AVI/TIF files to be loaded

            IEnumerable<string> non_ohba_files = from FileInfo fi in directory_info.GetFiles("*").Where<FileInfo>(x => x.Extension.Equals(".cxd") || x.Extension.Equals(".avi") || x.Extension.Equals(".tif")) select Path.GetFileNameWithoutExtension(fi.Name);
            IEnumerable<string> files_to_add = ohba_files.Union(non_ohba_files).Except(ohba_files.Intersect(non_ohba_files));

            List<string> files = new List<string>();
            foreach(string s in ohba_files)
                files.Add(Globals.CurrentDirectory + "\\" + s + ".xml");

            foreach(string s in files_to_add)
            {
                for(int i = 0; i < valid_exts.Length; i++)
                {
                    string file = Globals.CurrentDirectory + "\\" + s + valid_exts[i];
                    if(File.Exists(file))
                        files.Add(file);
                }
            }

            #endregion


            Globals.FilegramList.Clear();
            Globals.ErrorFilegrams.Clear();

            if(files.Count == 0)
            {
                MessageBox.Show("No Files Found.");
                return;
            }

            ProcessQueue.MaximumTasks = 1;
            m_process_progress = new frmProcessNotify();

            foreach(string s in files)
                load_queue.Enqueue(new Filegram(s), new ProcessEventArgs(ProcessEventArgsType.Load));

            TOTAL_QUEUE_SIZE = load_queue.Count;

            m_process_progress = new frmProcessNotify("Loading Files...", "Loading Files");
            m_process_progress.Cancel += new EventHandler(m_process_progress_Cancel);
            m_process_progress.Show();
            load_queue.Start();
            load_queue.Complete += new EventHandler(load_queue_Complete);
            load_queue.AllComplete += new EventHandler(load_queue_AllComplete);
        }

        static void load_queue_Complete(object sender, EventArgs e)
        {
            int progress = (int)((m_list.Count + 1) / TOTAL_QUEUE_SIZE * 100);
            m_list.Add((Filegram)(e as AsyncCompletedEventArgs).UserState);
            m_process_progress.UpdateProgress(progress);
        }

        static void load_queue_AllComplete(object sender, EventArgs e)
        {
            ProcessQueue.MaximumTasks = 2;
            m_process_progress.Invoke(((MethodInvoker)delegate { m_process_progress.Close(); }));
            OnLoadComplete();
        }

        static void m_process_progress_Cancel(object sender, EventArgs e)
        {
            load_queue.CancelAll();
            m_process_progress.Close();
        }

        public static void Save()
        {

        }

        static void OnLoadComplete()
        {
            Globals.PQueue = new ProcessQueue(false);

            foreach(Filegram fg in m_list)
            {
                //Subscribe to events
                fg.PhaseComplete += new PhaseCompleteEventHandler(fg_PhaseComplete);
                fg.PhaseBegin += new PhaseCompleteEventHandler(fg_PhaseBegin);
            }

            if(LoadComplete != null)
                LoadComplete(new object(), new EventArgs());
        }

        static void fg_PhaseBegin(object sender, EventArgs e)
        {
            frmPhase new_form = (sender as frmPhase);
            //throw new NotImplementedException();
        }

        static void fg_PhaseComplete(object sender, Library.Forms.PhaseCompleteEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public List<Filegram> GetRange(int index, int count)
        {
            return m_list.GetRange(index, count);
        }

        public void WriteCsv()
        {
            if(this.Count <= 0)
                throw new SohaException("Can't write Csv: No Filegrams in List.");

            string l_csv_filepath = Globals.CurrentDirectory + "\\complete_data_output.csv";

            #region Create List of objects

            List<object[]> output_data = new List<object[]>(this.Count + 1);
            output_data.Add(CreateHeader());
            for(int i = 0; i < this.Count; i++)
            {
                List<object[]> objs = Globals.FilegramList[i].ToDataRow();
                foreach(object[] o_array in objs)
                    output_data.Add(o_array);
            }

            #endregion

            using(StreamWriter writer = new StreamWriter(l_csv_filepath))
            {
                writer.WriteLine("sep=;");
                for(int i = 0; i < output_data.Count; i++)
                {
                    for(int j = 0; j < (output_data[i] as object[]).Length; j++)
                    {
                        writer.Write("\"" + (output_data[i] as object[])[j] + "\"" + CSV_DELIMITER);
                    }
                    writer.WriteLine();
                }
                writer.Close();
            }

            Process.Start(l_csv_filepath);

        }


        public static object[] CreateHeader()
        {
            object[] header = new object[Filegram.EXCEL_ROW_LENGTH];
            int i = 0;

            header[i++] = "Filename";
            header[i++] = "Scale Factor";
            header[i++] = "Genotype";
            header[i++] = "ID";
            header[i++] = "Comments";
            header[i++] = "Age";
            header[i++] = "Sex";

            header[i++] = "Heartrate: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            i += 3;

            header[i++] = "Heartperiod: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            i += 3;

            header[i++] = "Diastolic Intervals: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            header[i++] = "DI/Hp [Mean]";
            header[i++] = "Total DI Time";
            i += 3;

            header[i++] = "Systolic Intervals: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            header[i++] = "SI/Hp [Mean]";
            header[i++] = "Total SI Time";
            i += 2;

            header[i++] = "SD/Median [Heartperiod]";
            header[i++] = "SI/DI [Mean]";

            header[i++] = "Diastolic Diameters";
            i++;
            header[i++] = "Diastolic Mean Diameter";
            header[i++] = "Systolic Diameters";
            i++;
            header[i++] = "Systolic Mean Diameter";

            header[i++] = "Fractional Shortening";

            header[i++] = "Horizontal Diastole";

            header[i++] = "Diastolic SA";

            header[i++] = "Horizontal Systole";

            header[i++] = "Systolic SA";

            header[i++] = "Fractional Area Change";

            i += 3;

            header[i++] = "Contraction Intervals: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            header[i++] = "Total CI Time";
            i += 3;

            header[i++] = "Relax Intervals: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            header[i++] = "Total RI Time";
            i += 3;

            header[i++] = "MaxVelocity Intervals: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            header[i++] = "Total MVI Time";

            //Jerome Adds
            header[i++] = "Total DI > 3sec";
            header[i++] = "Long DI > 3sec";
            header[i++] = "% DI > 3sec";

            header[i++] = "Total DI";
            header[i++] = "Long DI";
            header[i++] = "% Long DI";

            header[i++] = "Total SI";
            header[i++] = "Long SI";
            header[i++] = "% Long SI";
            header[i++] = "";

            header[i++] = "Normalized Intervals: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            i += 3;

            header[i++] = "Isometric Intervals: Mean";
            header[i++] = "Median";
            header[i++] = "Std Dev";
            header[i++] = "StdDev/Median";
            header[i++] = "Total Iso Time";

            i += 2;
            header[i++] = "Shortening Velocity";
            header[i++] = "Lengthening Velocity";
            return header;
        }

        #region IList<Filegram> Members

        public int IndexOf(Filegram item)
        {
            return m_list.IndexOf(item);
        }

        public void Insert(int index, Filegram item)
        {
            m_list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            m_list.RemoveAt(index);
        }

        public Filegram this[int index]
        {
            get
            {
                return m_list[index];
            }
            set
            {
                m_list[index] = value;
            }
        }

        #endregion

        #region ICollection<Filegram> Members

        public void Add(Filegram item)
        {
            m_list.Add(item);
        }

        public void Clear()
        {

            foreach(Filegram fg in m_list)
            {
                fg.Close();
            }

            m_list.Clear();
        }

        public bool Contains(Filegram item)
        {
            return m_list.Contains(item);
        }

        public void CopyTo(Filegram[] array, int arrayIndex)
        {
            m_list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return m_list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Filegram item)
        {
            return m_list.Remove(item);
        }

        #endregion

        #region IEnumerable<Filegram> Members

        public IEnumerator<Filegram> GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        #endregion
    }
}
