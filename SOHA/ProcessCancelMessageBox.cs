﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHA
{
    public partial class ProcessCancelMessageBox : Form
    {
        private bool m_option_chosen = false;
        private DialogResult m_option;

        public ProcessCancelMessageBox()
        {
            InitializeComponent();

            this.LostFocus += new EventHandler(ProcessCancelMessageBox_LostFocus);
        }

        void ProcessCancelMessageBox_LostFocus(object sender, EventArgs e)
        {
            this.Focus();
        }

        public DialogResult Show(string text)
        {
            //Globals.MainInstance.Enabled = false;

            label1.Text = text;

            this.Visible = true;

            while (m_option == DialogResult.None)
            {
                Application.DoEvents();
            }

            //Globals.MainInstance.Enabled = true;

            return m_option;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            m_option = System.Windows.Forms.DialogResult.Yes;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            m_option = System.Windows.Forms.DialogResult.No;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            m_option = System.Windows.Forms.DialogResult.Abort;
        }
    }
}
