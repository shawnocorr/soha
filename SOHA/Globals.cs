﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using System.ComponentModel;
using SOHA.Library.GPU;
using Ionic.Zip;
using SOHA.Library.Utilities;
using System.Xml.Serialization;
using System.Xml;
using System.Web.Script.Serialization;

namespace SOHA
{
    public static class Globals
    {
        public const int XML_VERSION = 2;

        public static string CAMERA_PATH { get; set; }
        public static string RECENT_ASSAY_PATH { get; set; }

        private static int m_max_queue_size;
        public static int MaxProcessingQueueSize 
        {
            get
            {
                return m_max_queue_size;
            }
            set
            {
                m_max_queue_size = value;
                ProcessingQueue.MaxRunningTasks = m_max_queue_size;
            }
        }

        public static string CurrentDirectory { get; set; }
        public static string XmlArchiveDirectory { get; set; }

        public static List<string> RecentAssays { get; set; }
        public static Form MainInstance { get; set; }
        public static ProcessQueue PQueue = new ProcessQueue();
        public static FilegramCollection FilegramList { get; set; }
        public static GlobalOptions Options { get; set; }


        public static List<string> ErrorFilegrams { get; set; }
        public static string AppDataFolder { get; set; }

        public static void Initialize()
        {

            //Initialize OpenCL
            //Gpu.Initialize();
            //if(!GpuFilter.Initialized)
            //    GpuFilter.Init(7);

            #region Verify Activation

            //if (!Web.Verify())
            //{
            //    MessageBox.Show("You do not have access.");
            //    throw new Exception("You do not have access to SOHA.");
            //}

            //string current_activation_code = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Oaktree\SOHA", "ActivationNumber", "");
            //string mac = NICAccess.GetMACAddress();
            //string code = GetSerialActivation(mac);

            //if (!(code == current_activation_code))
            //{
            //    frmActivation activation = new frmActivation(mac);
            //    activation.ShowDialog();

            //    string new_code = activation.Code.Replace("-","");

            //    if ((code == new_code))
            //    {
            //        MessageBox.Show("Thank you for activating the SOHA Application.\n\nPlease Restart the Application to continue.");

            //        Registry.SetValue(@"HKEY_LOCAL_MACHINE\Software\Oaktree\SOHA", "ActivationNumber", new_code);
                    
            //        throw new InvalidDataException("Activation Code Valid... Restart Required");
            //    }
            //    else
            //    {
            //        MessageBox.Show("Invalid Activation Code.\n\nExiting Application....");
            //        throw new InvalidDataException("Activation Code Invalid");
            //    }
            //}

            #endregion

            //if(Registry

            //string max_queue_size = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Oaktree\SOHA", "MaxProcessingQueueSize", "");

            int queue_size = 2;// (max_queue_size == null ? 2 : int.Parse(max_queue_size));

            m_max_queue_size = ProcessingQueue.MaxRunningTasks = queue_size;

            AppDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\SOHA\";
            Directory.CreateDirectory(AppDataFolder);
            Logs.AppDataFolder = AppDataFolder;

            RECENT_ASSAY_PATH = AppDataFolder + "assays.txt";

            //A simple get accessor ensures cameras are loaded
            var temp = CameraCollection.List;

            RecentAssays = new List<string>();
            ReadRecentAssays();

            Options = GlobalOptions.ReadFile();

            FilegramList = new FilegramCollection();
            ErrorFilegrams = new List<string>();
            //Queue = new PreprocessQueue();
        }

        static void PreprocessQueue_ProcessBegun(Processable process)
        {
            foreach (Filegram f in FilegramList)
            {
                if (f.Video.Equals(process))
                    f.Status = FilegramStatus.Processing;
            }
        }

        public static void ReadRecentAssays()
        {
            try
            {
                if (!System.IO.File.Exists(RECENT_ASSAY_PATH))
                {
                    return;
                }

                using (StreamReader reader = new StreamReader(RECENT_ASSAY_PATH))
                {
                    RecentAssays.Clear();
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        RecentAssays.Add(line);
                    }
                    reader.Close();
                }
            }
            catch (FileNotFoundException fnfe)
            {

            }
            catch (Exception exc)
            {

            }
        }

        public static void WriteRecentAssays()
        {
            using (StreamWriter writer = new StreamWriter(RECENT_ASSAY_PATH))
            {
                foreach (string assay in RecentAssays)
                {
                    writer.WriteLine(assay);
                }
                writer.Close();
            }
        }

        public static void AddNewAssay(string assay_path)
        {
            if (RecentAssays.Contains(assay_path)) return;

            if (RecentAssays.Count <= 5)
            {
                RecentAssays.Add(assay_path);
            }
            else
            {
                RecentAssays.RemoveAt(0);
                RecentAssays.Add(assay_path);
            }
        }

        public static bool ArchiveXml(string file)
        {
            try
            {
                Directory.CreateDirectory(XmlArchiveDirectory);

                string filename = Path.GetFileName(file);

                System.IO.File.Copy(file, XmlArchiveDirectory + filename);

                return true;
            }
            catch (IOException io_exc)
            {
                //MessageBox.Show("XML file has already been archived.");
            }
            catch (Exception exc)
            {

            }

            return false;
        }

        public static void CreateLogFileZip()
        {

            #region Get/Write PC Properties
            
            PCProperties.GetProperties();

            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\SOHA\pcproperties.txt";
            using(StreamWriter writer = new StreamWriter(path))
            {
                writer.WriteLine(path, new JavaScriptSerializer().Serialize(PCProperties.AllObjects));
            }

            #endregion

            string directory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\SOHA\Traces";

            //Get files for last 7 days
            var files = Directory.GetFiles(directory).Where(x => (new FileInfo(x).CreationTime.AddDays(7) > DateTime.Today));

            using(ZipFile zip = new ZipFile())
            {
                //Add Properties File
                zip.AddFile(path);

                foreach(string s in files)
                    zip.AddFile(s, "");
                zip.Save(directory + @"\..\TraceArchive_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".zip");
            }
        }

        private static bool File(string x)
        {
            throw new NotImplementedException();
        }

        public static void Save()
        {
            foreach(Filegram fgram in FilegramList)
            {
                fgram.Serialize(fgram);
                //Filegram.Serialize(fgram);
            }
        }

        public static void FreeAllResources()
        {
            foreach (Filegram fgram in FilegramList)
            {
                fgram.Video.Close();
            }

            GC.Collect();
        }

        public static void SaveMMode(Bitmap mmode, string filename)
        {
            if (!Directory.Exists(CurrentDirectory + @"\MModes"))
            {
                Directory.CreateDirectory(CurrentDirectory + @"\MModes");
            }

            string filepath = CurrentDirectory + @"\MModes\" + filename;

            if(System.IO.File.Exists(filepath))
            {
                System.IO.File.Delete(filepath);
            }

            mmode.Save(filepath, ImageFormat.Bmp);
        }

        //public static void LogTrace(string message)
        //{
        //    if (!Directory.Exists(AppDataFolder + @"Traces\"))
        //        Directory.CreateDirectory(AppDataFolder + @"Traces\");

        //    string trace_file_path = AppDataFolder + @"Traces\" + DateTime.Today.ToShortDateString().Replace("/","_") + ".txt";


        //    using (StreamWriter writer = new StreamWriter(trace_file_path, true))
        //    {
        //        writer.WriteLine(DateTime.Now.ToLongTimeString() + ": " + message);
        //    }
        //}

        //public static void LogException(Exception exc, string[] extra_info)
        //{
        //    if (!Directory.Exists(AppDataFolder + @"Exceptions\"))
        //        Directory.CreateDirectory(AppDataFolder + @"Exceptions\");

        //    string filename = DateTime.Today.ToShortDateString().Replace("/", "_") + "_" + DateTime.Now.ToString("HHmmss");
        //    string trace_file_path = AppDataFolder + @"Exceptions\" + filename + ".txt";

        //    using (StreamWriter writer = new StreamWriter(trace_file_path, true))
        //    {
        //        foreach (string s in extra_info)
        //            writer.WriteLine(s);

        //        writer.WriteLine(exc.Message);
        //        writer.WriteLine(exc.StackTrace);
        //    }
        //}

        //public static void LogException(Exception exc)
        //{
        //    if (!Directory.Exists(AppDataFolder + @"Exceptions\"))
        //        Directory.CreateDirectory(AppDataFolder + @"Exceptions\");

        //    string filename = DateTime.Today.ToShortDateString().Replace("/", "_") + "_" + DateTime.Now.ToString("HHmmss");
        //    string trace_file_path = AppDataFolder + @"Exceptions\" + filename + ".txt";

        //    using (StreamWriter writer = new StreamWriter(trace_file_path, true))
        //    {
        //        writer.WriteLine(exc.Message);
        //        writer.WriteLine(exc.StackTrace);
        //    }
        //}

        public static bool Validate(string serial)
        {
            serial = serial.ToUpper();
            if (!Regex.IsMatch(serial, "[A-Z]{4}-[A-Z]{4}-[A-Z]{4}-[A-Z]{4}"))
                return false;

            string[] segments = serial.Split('-');

            for (int i = 0; i < 4; i++)
            {
                int sum = 0;
                for (int j = 0; j < 3; j++)
                {
                    sum += (int)segments[i][j] - 65;
                }

                int mod = (i + 1) * 5;

                if (sum % mod != (int)segments[i][3] - 65)
                    return false;
            }

            return true;
        }

        public static string GetSerialActivation(string mac)
        {
            if (mac.Contains(":"))
                mac = mac.Replace(":", "");

            string code = "";

            int sum = 0;
            int prod = 1;
            for (int i = 0; i < 12; )
            {
                int c1 = (int)mac[i++];
                int c2 = (int)mac[i++];

                sum = c1 + c2;
                prod = c1 * c2;

                code += (char)(sum % 26 + 65);
                code += (char)(prod % 26 + 65);
            }

            return code;
        }
    }
}

