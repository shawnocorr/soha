﻿using SOHAControls.DataDisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Mathematics;
using System.Windows.Forms;
using System.Drawing;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for MatrixDataGridViewTest and is intended
    ///to contain all MatrixDataGridViewTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MatrixDataGridViewTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for TheData
        ///</summary>
        [TestMethod()]
        public void TheDataTest()
        {
            MatrixDataGridView target = new MatrixDataGridView(); // TODO: Initialize to an appropriate value
            double[,] expected = null; // TODO: Initialize to an appropriate value
            double[,] actual;
            target.TheData = expected;
            actual = target.TheData;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Matrix
        ///</summary>
        [TestMethod()]
        public void MatrixTest()
        {
            MatrixDataGridView target = new MatrixDataGridView(); // TODO: Initialize to an appropriate value
            Matrix expected = null; // TODO: Initialize to an appropriate value
            Matrix actual;
            target.Matrix = expected;
            actual = target.Matrix;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ResizeOurself
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void ResizeOurselfTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            int r = 0; // TODO: Initialize to an appropriate value
            int c = 0; // TODO: Initialize to an appropriate value
            target.ResizeOurself(r, c);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MatrixIO_KeyUp
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MatrixIO_KeyUpTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = null; // TODO: Initialize to an appropriate value
            target.MatrixIO_KeyUp(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MatrixIO_KeyPress
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MatrixIO_KeyPressTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyPressEventArgs e = null; // TODO: Initialize to an appropriate value
            target.MatrixIO_KeyPress(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MatrixIO_KeyDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MatrixIO_KeyDownTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = null; // TODO: Initialize to an appropriate value
            target.MatrixIO_KeyDown(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MatrixIO_EditingControlShowing
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MatrixIO_EditingControlShowingTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            DataGridViewEditingControlShowingEventArgs e = null; // TODO: Initialize to an appropriate value
            target.MatrixIO_EditingControlShowing(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MakeMatrixTitle
        ///</summary>
        [TestMethod()]
        public void MakeMatrixTitleTest()
        {
            MatrixDataGridView target = new MatrixDataGridView(); // TODO: Initialize to an appropriate value
            string Title = string.Empty; // TODO: Initialize to an appropriate value
            target.MakeMatrixTitle(Title);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeDataGridView
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeDataGridViewTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            int rows = 0; // TODO: Initialize to an appropriate value
            int columns = 0; // TODO: Initialize to an appropriate value
            target.InitializeDataGridView(rows, columns);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for FillTextboxes
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void FillTextboxesTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            target.FillTextboxes();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ExtractTextboxes
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void ExtractTextboxesTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            target.ExtractTextboxes();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CheckKey
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void CheckKeyTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            Keys K = new Keys(); // TODO: Initialize to an appropriate value
            bool isDecimalPoint = false; // TODO: Initialize to an appropriate value
            bool isMinus = false; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.CheckKey(K, isDecimalPoint, isMinus);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for AddARow
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void AddARowTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            int i = 0; // TODO: Initialize to an appropriate value
            target.AddARow(i);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddAColumn
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void AddAColumnTest()
        {
            MatrixDataGridView_Accessor target = new MatrixDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            int i = 0; // TODO: Initialize to an appropriate value
            target.AddAColumn(i);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MatrixDataGridView Constructor
        ///</summary>
        [TestMethod()]
        public void MatrixDataGridViewConstructorTest()
        {
            MatrixDataGridView target = new MatrixDataGridView();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for MatrixDataGridView Constructor
        ///</summary>
        [TestMethod()]
        public void MatrixDataGridViewConstructorTest1()
        {
            int nRows = 0; // TODO: Initialize to an appropriate value
            int nCols = 0; // TODO: Initialize to an appropriate value
            MatrixDataGridView target = new MatrixDataGridView(nRows, nCols);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for MatrixDataGridView Constructor
        ///</summary>
        [TestMethod()]
        public void MatrixDataGridViewConstructorTest2()
        {
            Point location = new Point(); // TODO: Initialize to an appropriate value
            int nRows = 0; // TODO: Initialize to an appropriate value
            int nCols = 0; // TODO: Initialize to an appropriate value
            MatrixDataGridView target = new MatrixDataGridView(location, nRows, nCols);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
