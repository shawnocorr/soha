﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SOHA.Library.Mathematics;
using System.Drawing;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for GraphEventArgsTest and is intended
    ///to contain all GraphEventArgsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GraphEventArgsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Y
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void YTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            GraphEventArgs_Accessor target = new GraphEventArgs_Accessor(param0); // TODO: Initialize to an appropriate value
            List<double> expected = null; // TODO: Initialize to an appropriate value
            List<double> actual;
            target.Y = expected;
            actual = target.Y;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for X
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void XTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            GraphEventArgs_Accessor target = new GraphEventArgs_Accessor(param0); // TODO: Initialize to an appropriate value
            List<double> expected = null; // TODO: Initialize to an appropriate value
            List<double> actual;
            target.X = expected;
            actual = target.X;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Filter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void FilterTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            GraphEventArgs_Accessor target = new GraphEventArgs_Accessor(param0); // TODO: Initialize to an appropriate value
            Filter expected = null; // TODO: Initialize to an appropriate value
            Filter actual;
            target.Filter = expected;
            actual = target.Filter;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GraphEventArgs Constructor
        ///</summary>
        [TestMethod()]
        public void GraphEventArgsConstructorTest()
        {
            List<double> y = null; // TODO: Initialize to an appropriate value
            List<double> t = null; // TODO: Initialize to an appropriate value
            Filter f = null; // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            Rectangle r = new Rectangle(); // TODO: Initialize to an appropriate value
            GraphEventArgs target = new GraphEventArgs(y, t, f, g, r);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
