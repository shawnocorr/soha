﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Video;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for VideoFrameRangeTest and is intended
    ///to contain all VideoFrameRangeTest Unit Tests
    ///</summary>
    [TestClass()]
    public class VideoFrameRangeTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for StartFrame
        ///</summary>
        [TestMethod()]
        public void StartFrameTest()
        {
            VideoFrameRange target = new VideoFrameRange(); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.StartFrame;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Range
        ///</summary>
        [TestMethod()]
        public void RangeTest()
        {
            VideoFrameRange target = new VideoFrameRange(); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Range;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EndFrame
        ///</summary>
        [TestMethod()]
        public void EndFrameTest()
        {
            VideoFrameRange target = new VideoFrameRange(); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.EndFrame;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PreviousRange
        ///</summary>
        [TestMethod()]
        public void PreviousRangeTest()
        {
            VideoFrameRange target = new VideoFrameRange(); // TODO: Initialize to an appropriate value
            target.PreviousRange();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnRangeChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnRangeChangedTest()
        {
            VideoFrameRange_Accessor target = new VideoFrameRange_Accessor(); // TODO: Initialize to an appropriate value
            VideoFrameRangeEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnRangeChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for NextRange
        ///</summary>
        [TestMethod()]
        public void NextRangeTest()
        {
            VideoFrameRange target = new VideoFrameRange(); // TODO: Initialize to an appropriate value
            target.NextRange();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ChangeRange
        ///</summary>
        [TestMethod()]
        public void ChangeRangeTest()
        {
            VideoFrameRange target = new VideoFrameRange(); // TODO: Initialize to an appropriate value
            int newrange = 0; // TODO: Initialize to an appropriate value
            target.ChangeRange(newrange);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for VideoFrameRange Constructor
        ///</summary>
        [TestMethod()]
        public void VideoFrameRangeConstructorTest()
        {
            VideoFrameRange target = new VideoFrameRange();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for VideoFrameRange Constructor
        ///</summary>
        [TestMethod()]
        public void VideoFrameRangeConstructorTest1()
        {
            Video video = null; // TODO: Initialize to an appropriate value
            VideoFrameRange target = new VideoFrameRange(video);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for VideoFrameRange Constructor
        ///</summary>
        [TestMethod()]
        public void VideoFrameRangeConstructorTest2()
        {
            Video video = null; // TODO: Initialize to an appropriate value
            int range = 0; // TODO: Initialize to an appropriate value
            VideoFrameRange target = new VideoFrameRange(video, range);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
