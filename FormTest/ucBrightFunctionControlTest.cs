﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library;
using System.Collections.Generic;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucBrightFunctionControlTest and is intended
    ///to contain all ucBrightFunctionControlTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucBrightFunctionControlTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for OnLoad
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnLoadTest()
        {
            ucBrightFunctionControl_Accessor target = new ucBrightFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnLoad(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            ucBrightFunctionControl_Accessor target = new ucBrightFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        public void GetIntervalsTest()
        {
            ucBrightFunctionControl target = new ucBrightFunctionControl(); // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DrawThresholds
        ///</summary>
        [TestMethod()]
        public void DrawThresholdsTest()
        {
            ucBrightFunctionControl target = new ucBrightFunctionControl(); // TODO: Initialize to an appropriate value
            target.DrawThresholds();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawGraph
        ///</summary>
        [TestMethod()]
        public void DrawGraphTest()
        {
            ucBrightFunctionControl target = new ucBrightFunctionControl(); // TODO: Initialize to an appropriate value
            target.DrawGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            ucBrightFunctionControl_Accessor target = new ucBrightFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucBrightFunctionControl Constructor
        ///</summary>
        [TestMethod()]
        public void ucBrightFunctionControlConstructorTest()
        {
            ucBrightFunctionControl target = new ucBrightFunctionControl();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
