﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Mathematics;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for BandPassFilterControlTest and is intended
    ///to contain all BandPassFilterControlTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BandPassFilterControlTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for LoCutoff
        ///</summary>
        [TestMethod()]
        public void LoCutoffTest()
        {
            BandPassFilterControl target = new BandPassFilterControl(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.LoCutoff = expected;
            actual = target.LoCutoff;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HiCutoff
        ///</summary>
        [TestMethod()]
        public void HiCutoffTest()
        {
            BandPassFilterControl target = new BandPassFilterControl(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.HiCutoff = expected;
            actual = target.HiCutoff;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Filter
        ///</summary>
        [TestMethod()]
        public void FilterTest()
        {
            BandPassFilterControl target = new BandPassFilterControl(); // TODO: Initialize to an appropriate value
            Filter expected = null; // TODO: Initialize to an appropriate value
            Filter actual;
            target.Filter = expected;
            actual = target.Filter;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for OnPropertyChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPropertyChangedTest()
        {
            BandPassFilterControl_Accessor target = new BandPassFilterControl_Accessor(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            target.OnPropertyChanged(name);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            BandPassFilterControl_Accessor target = new BandPassFilterControl_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            BandPassFilterControl_Accessor target = new BandPassFilterControl_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BandPassFilterControl Constructor
        ///</summary>
        [TestMethod()]
        public void BandPassFilterControlConstructorTest()
        {
            BandPassFilterControl target = new BandPassFilterControl();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
