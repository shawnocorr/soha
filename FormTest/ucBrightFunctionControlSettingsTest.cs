﻿using SOHAControls.DisplayControls.FunctionControlSettings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucBrightFunctionControlSettingsTest and is intended
    ///to contain all ucBrightFunctionControlSettingsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucBrightFunctionControlSettingsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for OnSettingsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnSettingsChangedTest()
        {
            ucBrightFunctionControlSettings_Accessor target = new ucBrightFunctionControlSettings_Accessor(); // TODO: Initialize to an appropriate value
            target.OnSettingsChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPropertyChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPropertyChangedTest()
        {
            ucBrightFunctionControlSettings_Accessor target = new ucBrightFunctionControlSettings_Accessor(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            target.OnPropertyChanged(name);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            ucBrightFunctionControlSettings_Accessor target = new ucBrightFunctionControlSettings_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetSettings
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void GetSettingsTest()
        {
            ucBrightFunctionControlSettings_Accessor target = new ucBrightFunctionControlSettings_Accessor(); // TODO: Initialize to an appropriate value
            BrightFunctionControlSettings expected = null; // TODO: Initialize to an appropriate value
            BrightFunctionControlSettings actual;
            actual = target.GetSettings();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            ucBrightFunctionControlSettings_Accessor target = new ucBrightFunctionControlSettings_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucBrightFunctionControlSettings Constructor
        ///</summary>
        [TestMethod()]
        public void ucBrightFunctionControlSettingsConstructorTest()
        {
            ucBrightFunctionControlSettings target = new ucBrightFunctionControlSettings();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
