﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using SOHA.Library.Xml;
using SOHA.Library;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucBrightFunctionControlDisplayTest and is intended
    ///to contain all ucBrightFunctionControlDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucBrightFunctionControlDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for YValues
        ///</summary>
        [TestMethod()]
        public void YValuesTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            List<double> actual;
            actual = target.YValues;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Range
        ///</summary>
        [TestMethod()]
        public void RangeTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            VideoFrameRange expected = null; // TODO: Initialize to an appropriate value
            VideoFrameRange actual;
            target.Range = expected;
            actual = target.Range;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PlotRectangle
        ///</summary>
        [TestMethod()]
        public void PlotRectangleTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            RectangleF actual;
            actual = target.PlotRectangle;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ucBrightFunctionControl1_ThresholdsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void ucBrightFunctionControl1_ThresholdsChangedTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.ucBrightFunctionControl1_ThresholdsChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucBrightFunctionControl1_MouseClickZedGraph
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void ucBrightFunctionControl1_MouseClickZedGraphTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.ucBrightFunctionControl1_MouseClickZedGraph(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for scrollDarknessHigh_ValueChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void scrollDarknessHigh_ValueChangedTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.scrollDarknessHigh_ValueChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for scrollDarknessHigh_Scroll
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void scrollDarknessHigh_ScrollTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            ScrollEventArgs e = null; // TODO: Initialize to an appropriate value
            target.scrollDarknessHigh_Scroll(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for numValueChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void numValueChangedTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.numValueChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for m_settings_SettingsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void m_settings_SettingsChangedTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.m_settings_SettingsChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SuspendGraph
        ///</summary>
        [TestMethod()]
        public void SuspendGraphTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            target.SuspendGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetXValuesFromY
        ///</summary>
        [TestMethod()]
        public void SetXValuesFromYTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            List<double> y = null; // TODO: Initialize to an appropriate value
            target.SetXValuesFromY(y);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetSettings
        ///</summary>
        [TestMethod()]
        public void SetSettingsTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            IntervalPhaseSettings settings = null; // TODO: Initialize to an appropriate value
            IntervalPhaseSettings settingsExpected = null; // TODO: Initialize to an appropriate value
            target.SetSettings(ref settings);
            Assert.AreEqual(settingsExpected, settings);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ResumeGraph
        ///</summary>
        [TestMethod()]
        public void ResumeGraphTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            target.ResumeGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnVideoFrameRangeChanged
        ///</summary>
        [TestMethod()]
        public void OnVideoFrameRangeChangedTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnVideoFrameRangeChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnThresholdChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnThresholdChangedTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnThresholdChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnLoad
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnLoadTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnLoad(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetSettings
        ///</summary>
        [TestMethod()]
        public void GetSettingsTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            IntervalPhaseSettings expected = null; // TODO: Initialize to an appropriate value
            IntervalPhaseSettings actual;
            actual = target.GetSettings();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        public void GetIntervalsTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DrawGraph
        ///</summary>
        [TestMethod()]
        public void DrawGraphTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            target.DrawGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            ucBrightFunctionControlDisplay_Accessor target = new ucBrightFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucBrightFunctionControlDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void ucBrightFunctionControlDisplayConstructorTest()
        {
            ucBrightFunctionControlDisplay target = new ucBrightFunctionControlDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
