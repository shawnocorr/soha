﻿using SOHAControls.FilterControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Mathematics;
using System.Windows.Forms;
using SOHA.Library.Imaging;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucFilterPanelDisplayTest and is intended
    ///to contain all ucFilterPanelDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucFilterPanelDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Text
        ///</summary>
        [TestMethod()]
        public void TextTest()
        {
            ucFilterPanelDisplay target = new ucFilterPanelDisplay(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.Text = expected;
            actual = target.Text;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Matrix
        ///</summary>
        [TestMethod()]
        public void MatrixTest()
        {
            ucFilterPanelDisplay target = new ucFilterPanelDisplay(); // TODO: Initialize to an appropriate value
            Matrix expected = null; // TODO: Initialize to an appropriate value
            Matrix actual;
            target.Matrix = expected;
            actual = target.Matrix;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsSelected
        ///</summary>
        [TestMethod()]
        public void IsSelectedTest()
        {
            ucFilterPanelDisplay target = new ucFilterPanelDisplay(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsSelected;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsHighlighted
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void IsHighlightedTest()
        {
            ucFilterPanelDisplay_Accessor target = new ucFilterPanelDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.IsHighlighted = expected;
            actual = target.IsHighlighted;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for label1_MouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void label1_MouseClickTest()
        {
            ucFilterPanelDisplay_Accessor target = new ucFilterPanelDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.label1_MouseClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ToggleSelected
        ///</summary>
        [TestMethod()]
        public void ToggleSelectedTest()
        {
            ucFilterPanelDisplay target = new ucFilterPanelDisplay(); // TODO: Initialize to an appropriate value
            target.ToggleSelected();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ToggleHighlight
        ///</summary>
        [TestMethod()]
        public void ToggleHighlightTest()
        {
            ucFilterPanelDisplay target = new ucFilterPanelDisplay(); // TODO: Initialize to an appropriate value
            target.ToggleHighlight();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            ucFilterPanelDisplay_Accessor target = new ucFilterPanelDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            ucFilterPanelDisplay_Accessor target = new ucFilterPanelDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            ucFilterPanelDisplay_Accessor target = new ucFilterPanelDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucFilterPanelDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void ucFilterPanelDisplayConstructorTest()
        {
            IFrameFunction filter = null; // TODO: Initialize to an appropriate value
            ucFilterPanelDisplay target = new ucFilterPanelDisplay(filter);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ucFilterPanelDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void ucFilterPanelDisplayConstructorTest1()
        {
            ucFilterPanelDisplay target = new ucFilterPanelDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
