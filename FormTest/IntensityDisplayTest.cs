﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using SOHA.Library;
using System.Drawing;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for IntensityDisplayTest and is intended
    ///to contain all IntensityDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IntensityDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Threshold
        ///</summary>
        [TestMethod()]
        public void ThresholdTest()
        {
            IntensityDisplay target = new IntensityDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Threshold = expected;
            actual = target.Threshold;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for MinIntervals
        ///</summary>
        [TestMethod()]
        public void MinIntervalsTest()
        {
            IntensityDisplay target = new IntensityDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.MinIntervals = expected;
            actual = target.MinIntervals;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LoCutoff
        ///</summary>
        [TestMethod()]
        public void LoCutoffTest()
        {
            IntensityDisplay target = new IntensityDisplay(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.LoCutoff = expected;
            actual = target.LoCutoff;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HiCutoff
        ///</summary>
        [TestMethod()]
        public void HiCutoffTest()
        {
            IntensityDisplay target = new IntensityDisplay(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.HiCutoff = expected;
            actual = target.HiCutoff;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for plot_MouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void plot_MouseClickTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.plot_MouseClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PreFilter
        ///</summary>
        [TestMethod()]
        public void PreFilterTest()
        {
            IntensityDisplay target = new IntensityDisplay(); // TODO: Initialize to an appropriate value
            List<double> func = null; // TODO: Initialize to an appropriate value
            List<double> funcExpected = null; // TODO: Initialize to an appropriate value
            target.PreFilter(ref func);
            Assert.AreEqual(funcExpected, func);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnThresholdsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnThresholdsChangedTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnThresholdsChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPropertyChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPropertyChangedTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            target.OnPropertyChanged(name);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaintBackground
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintBackgroundTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaintBackground(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnIntervalsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnIntervalsChangedTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnIntervalsChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeToolStripMenu
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeToolStripMenuTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeToolStripMenu();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetValues
        ///</summary>
        [TestMethod()]
        public void GetValuesTest()
        {
            IntensityDisplay target = new IntensityDisplay(); // TODO: Initialize to an appropriate value
            int x = 0; // TODO: Initialize to an appropriate value
            int interval = 0; // TODO: Initialize to an appropriate value
            int interval_size = 0; // TODO: Initialize to an appropriate value
            double[] expected = null; // TODO: Initialize to an appropriate value
            double[] actual;
            actual = target.GetValues(x, interval, interval_size);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        public void GetIntervalsTest()
        {
            IntensityDisplay target = new IntensityDisplay(); // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            IntensityDisplay_Accessor target = new IntensityDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddThresholds
        ///</summary>
        [TestMethod()]
        public void AddThresholdsTest()
        {
            IntensityDisplay target = new IntensityDisplay(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            Bitmap b = null; // TODO: Initialize to an appropriate value
            target.AddThresholds(g, b);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for IntensityDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void IntensityDisplayConstructorTest()
        {
            IntensityDisplay target = new IntensityDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
