﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Windows.Forms;
using SOHA.Library.Mathematics;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for DisplayTest and is intended
    ///to contain all DisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        internal virtual Display CreateDisplay()
        {
            // TODO: Instantiate an appropriate concrete class.
            Display target = null;
            return target;
        }

        /// <summary>
        ///A test for WindowSize
        ///</summary>
        [TestMethod()]
        public void WindowSizeTest()
        {
            Display target = CreateDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.WindowSize = expected;
            actual = target.WindowSize;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CurrentWindow
        ///</summary>
        [TestMethod()]
        public void CurrentWindowTest()
        {
            Display target = CreateDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.CurrentWindow = expected;
            actual = target.CurrentWindow;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PreviousWindow
        ///</summary>
        [TestMethod()]
        public void PreviousWindowTest()
        {
            Display target = CreateDisplay(); // TODO: Initialize to an appropriate value
            target.PreviousWindow();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        internal virtual Display_Accessor CreateDisplay_Accessor()
        {
            // TODO: Instantiate an appropriate concrete class.
            Display_Accessor target = null;
            return target;
        }

        /// <summary>
        ///A test for PreGraph
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void PreGraphTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Display_Accessor target = new Display_Accessor(param0); // TODO: Initialize to an appropriate value
            double[] data = null; // TODO: Initialize to an appropriate value
            double[] dataExpected = null; // TODO: Initialize to an appropriate value
            target.PreGraph(ref data);
            Assert.AreEqual(dataExpected, data);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PostGraph
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void PostGraphTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Display_Accessor target = new Display_Accessor(param0); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            Bitmap b = null; // TODO: Initialize to an appropriate value
            target.PostGraph(g, b);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnWindowChanged
        ///</summary>
        [TestMethod()]
        public void OnWindowChangedTest()
        {
            Display target = CreateDisplay(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnWindowChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Display_Accessor target = new Display_Accessor(param0); // TODO: Initialize to an appropriate value
            PaintEventArgs pe = null; // TODO: Initialize to an appropriate value
            target.OnPaint(pe);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnMouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnMouseClickTest()
        {
            // Private Accessor for OnMouseClick is not found. Please rebuild the containing project or run the Publicize.exe manually.
            Assert.Inconclusive("Private Accessor for OnMouseClick is not found. Please rebuild the containing pro" +
                    "ject or run the Publicize.exe manually.");
        }

        /// <summary>
        ///A test for NextWindow
        ///</summary>
        [TestMethod()]
        public void NextWindowTest()
        {
            Display target = CreateDisplay(); // TODO: Initialize to an appropriate value
            target.NextWindow();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeDisplay
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeDisplayTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Display_Accessor target = new Display_Accessor(param0); // TODO: Initialize to an appropriate value
            target.InitializeDisplay();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Display_Accessor target = new Display_Accessor(param0); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Filter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void FilterTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Display_Accessor target = new Display_Accessor(param0); // TODO: Initialize to an appropriate value
            double[] data = null; // TODO: Initialize to an appropriate value
            double[] dataExpected = null; // TODO: Initialize to an appropriate value
            Filter filter = null; // TODO: Initialize to an appropriate value
            target.Filter(ref data, filter);
            Assert.AreEqual(dataExpected, data);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Display_Accessor target = new Display_Accessor(param0); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
