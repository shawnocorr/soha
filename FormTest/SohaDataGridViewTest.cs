﻿using SOHAControls.DataDisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;
using System.ComponentModel;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for SohaDataGridViewTest and is intended
    ///to contain all SohaDataGridViewTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SohaDataGridViewTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for DataSource
        ///</summary>
        [TestMethod()]
        public void DataSourceTest()
        {
            SohaDataGridView target = new SohaDataGridView(); // TODO: Initialize to an appropriate value
            object expected = null; // TODO: Initialize to an appropriate value
            object actual;
            target.DataSource = expected;
            actual = target.DataSource;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DataGridType
        ///</summary>
        [TestMethod()]
        public void DataGridTypeTest()
        {
            SohaDataGridView target = new SohaDataGridView(); // TODO: Initialize to an appropriate value
            SohaDataGridViewType expected = new SohaDataGridViewType(); // TODO: Initialize to an appropriate value
            SohaDataGridViewType actual;
            target.DataGridType = expected;
            actual = target.DataGridType;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for OnUserDeletedRow
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnUserDeletedRowTest()
        {
            SohaDataGridView_Accessor target = new SohaDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            DataGridViewRowEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnUserDeletedRow(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnDataUpdated
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnDataUpdatedTest()
        {
            SohaDataGridView_Accessor target = new SohaDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnDataUpdated(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnCellMouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnCellMouseClickTest()
        {
            SohaDataGridView_Accessor target = new SohaDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            DataGridViewCellMouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnCellMouseClick(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            SohaDataGridView_Accessor target = new SohaDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            SohaDataGridView_Accessor target = new SohaDataGridView_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SohaDataGridView Constructor
        ///</summary>
        [TestMethod()]
        public void SohaDataGridViewConstructorTest()
        {
            IContainer container = null; // TODO: Initialize to an appropriate value
            SohaDataGridView target = new SohaDataGridView(container);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for SohaDataGridView Constructor
        ///</summary>
        [TestMethod()]
        public void SohaDataGridViewConstructorTest1()
        {
            SohaDataGridView target = new SohaDataGridView();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
