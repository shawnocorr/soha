﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Video;
using SOHA.Library;
using SOHA.Library.Xml;
using System.Drawing;
using System.Diagnostics;
using SOHA.Library.Imaging;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for FrameDisplayTest and is intended
    ///to contain all FrameDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FrameDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for VideoFile
        ///</summary>
        [TestMethod()]
        public void VideoFileTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            Video expected = null; // TODO: Initialize to an appropriate value
            Video actual;
            target.VideoFile = expected;
            actual = target.VideoFile;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for UserInteractive
        ///</summary>
        [TestMethod()]
        public void UserInteractiveTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.UserInteractive = expected;
            actual = target.UserInteractive;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ROI
        ///</summary>
        [TestMethod()]
        public void ROITest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            SohaCollection<Roi> expected = null; // TODO: Initialize to an appropriate value
            SohaCollection<Roi> actual;
            target.ROI = expected;
            actual = target.ROI;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PostOpFrame
        ///</summary>
        [TestMethod()]
        public void PostOpFrameTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            Frame actual;
            actual = target.PostOpFrame;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for NormalPen
        ///</summary>
        [TestMethod()]
        public void NormalPenTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            Pen expected = null; // TODO: Initialize to an appropriate value
            Pen actual;
            target.NormalPen = expected;
            actual = target.NormalPen;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for MmodeDisplay
        ///</summary>
        [TestMethod()]
        public void MmodeDisplayTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.MmodeDisplay = expected;
            actual = target.MmodeDisplay;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Marks
        ///</summary>
        [TestMethod()]
        public void MarksTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            SohaCollection<MarkPair> expected = null; // TODO: Initialize to an appropriate value
            SohaCollection<MarkPair> actual;
            target.Marks = expected;
            actual = target.Marks;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LastFilterWatch
        ///</summary>
        [TestMethod()]
        public void LastFilterWatchTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            Stopwatch actual;
            actual = target.LastFilterWatch;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HighlightPen
        ///</summary>
        [TestMethod()]
        public void HighlightPenTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            Pen expected = null; // TODO: Initialize to an appropriate value
            Pen actual;
            target.HighlightPen = expected;
            actual = target.HighlightPen;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Filters
        ///</summary>
        [TestMethod()]
        public void FiltersTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            List<IFrameFunction> expected = null; // TODO: Initialize to an appropriate value
            List<IFrameFunction> actual;
            target.Filters = expected;
            actual = target.Filters;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EnableFilters
        ///</summary>
        [TestMethod()]
        public void EnableFiltersTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.EnableFilters = expected;
            actual = target.EnableFilters;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CurrentFrame
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void CurrentFrameTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            Frame expected = null; // TODO: Initialize to an appropriate value
            Frame actual;
            target.CurrentFrame = expected;
            actual = target.CurrentFrame;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Coordinates
        ///</summary>
        [TestMethod()]
        public void CoordinatesTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            Point expected = new Point(); // TODO: Initialize to an appropriate value
            Point actual;
            target.Coordinates = expected;
            actual = target.Coordinates;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for imgFrame_Paint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void imgFrame_PaintTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.imgFrame_Paint(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for imgFrame_MouseUp
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void imgFrame_MouseUpTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.imgFrame_MouseUp(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for imgFrame_MouseMove
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void imgFrame_MouseMoveTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.imgFrame_MouseMove(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for imgFrame_MouseLeave
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void imgFrame_MouseLeaveTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.imgFrame_MouseLeave(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for imgFrame_MouseEnter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void imgFrame_MouseEnterTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.imgFrame_MouseEnter(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for imgFrame_MouseDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void imgFrame_MouseDownTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.imgFrame_MouseDown(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for hscrollFrame_Scroll
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void hscrollFrame_ScrollTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            ScrollEventArgs e = null; // TODO: Initialize to an appropriate value
            target.hscrollFrame_Scroll(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ReDraw
        ///</summary>
        [TestMethod()]
        public void ReDrawTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            target.ReDraw();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnVideoChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnVideoChangedTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnVideoChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnUserInteractiveChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnUserInteractiveChangedTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnUserInteractiveChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnResize
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnResizeTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnResize(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnHistogramUpdated
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnHistogramUpdatedTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnHistogramUpdated();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnFrameChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnFrameChangedTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnFrameChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnFiltersChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnFiltersChangedTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnFiltersChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnDataChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnDataChangedTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnDataChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnCoordinatesChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnCoordinatesChangedTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnCoordinatesChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for KeyPress
        ///</summary>
        [TestMethod()]
        public void KeyPressTest()
        {
            FrameDisplay target = new FrameDisplay(); // TODO: Initialize to an appropriate value
            KeyEventArgs key_event = null; // TODO: Initialize to an appropriate value
            target.KeyPress(key_event);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Initialize
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.Initialize();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            FrameDisplay_Accessor target = new FrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for FrameDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void FrameDisplayConstructorTest()
        {
            FrameDisplay target = new FrameDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
