﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ExtensionsTest and is intended
    ///to contain all ExtensionsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ExtensionsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ProcessAllControls
        ///</summary>
        [TestMethod()]
        public void ProcessAllControlsTest()
        {
            Control control = null; // TODO: Initialize to an appropriate value
            Action<Control> action = null; // TODO: Initialize to an appropriate value
            Type type = null; // TODO: Initialize to an appropriate value
            bool doAction = false; // TODO: Initialize to an appropriate value
            Extensions.ProcessAllControls(control, action, type, doAction);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ProcessAllControls
        ///</summary>
        [TestMethod()]
        public void ProcessAllControlsTest1()
        {
            Control control = null; // TODO: Initialize to an appropriate value
            Action<Control> action = null; // TODO: Initialize to an appropriate value
            List<Control> exceptions = null; // TODO: Initialize to an appropriate value
            Extensions.ProcessAllControls(control, action, exceptions);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ProcessAllControls
        ///</summary>
        [TestMethod()]
        public void ProcessAllControlsTest2()
        {
            Control control = null; // TODO: Initialize to an appropriate value
            Action<Control> action = null; // TODO: Initialize to an appropriate value
            Extensions.ProcessAllControls(control, action);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetAllControls
        ///</summary>
        [TestMethod()]
        public void GetAllControlsTest()
        {
            Control control = null; // TODO: Initialize to an appropriate value
            Type type = null; // TODO: Initialize to an appropriate value
            IEnumerable<Control> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<Control> actual;
            actual = Extensions.GetAllControls(control, type);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
