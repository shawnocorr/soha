﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SOHA.Library;
using System.Windows.Forms;
using SOHA.Library.Xml;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucPhaseFunctionControlDisplayTest and is intended
    ///to contain all ucPhaseFunctionControlDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucPhaseFunctionControlDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for YValues
        ///</summary>
        [TestMethod()]
        public void YValuesTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            List<double> actual;
            actual = target.YValues;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Range
        ///</summary>
        [TestMethod()]
        public void RangeTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            VideoFrameRange expected = null; // TODO: Initialize to an appropriate value
            VideoFrameRange actual;
            target.Range = expected;
            actual = target.Range;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitialIntervals
        ///</summary>
        [TestMethod()]
        public void InitialIntervalsTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            IntervalCollection expected = null; // TODO: Initialize to an appropriate value
            IntervalCollection actual;
            target.InitialIntervals = expected;
            actual = target.InitialIntervals;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ucPhaseFunctionControl1_MouseClickZedGraph
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void ucPhaseFunctionControl1_MouseClickZedGraphTest()
        {
            ucPhaseFunctionControlDisplay_Accessor target = new ucPhaseFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.ucPhaseFunctionControl1_MouseClickZedGraph(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for numZoom_ValueChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void numZoom_ValueChangedTest()
        {
            ucPhaseFunctionControlDisplay_Accessor target = new ucPhaseFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.numZoom_ValueChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for numValueChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void numValueChangedTest()
        {
            ucPhaseFunctionControlDisplay_Accessor target = new ucPhaseFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.numValueChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for m_settings_SettingsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void m_settings_SettingsChangedTest()
        {
            ucPhaseFunctionControlDisplay_Accessor target = new ucPhaseFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.m_settings_SettingsChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SuspendGraph
        ///</summary>
        [TestMethod()]
        public void SuspendGraphTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            target.SuspendGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetXValuesFromY
        ///</summary>
        [TestMethod()]
        public void SetXValuesFromYTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            List<double> y = null; // TODO: Initialize to an appropriate value
            target.SetXValuesFromY(y);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetSettings
        ///</summary>
        [TestMethod()]
        public void SetSettingsTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            PhasePhaseSettings settings = null; // TODO: Initialize to an appropriate value
            PhasePhaseSettings settingsExpected = null; // TODO: Initialize to an appropriate value
            target.SetSettings(ref settings);
            Assert.AreEqual(settingsExpected, settings);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ResumeGraph
        ///</summary>
        [TestMethod()]
        public void ResumeGraphTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            target.ResumeGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnVideoFrameRangeChanged
        ///</summary>
        [TestMethod()]
        public void OnVideoFrameRangeChangedTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnVideoFrameRangeChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnLoad
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnLoadTest()
        {
            ucPhaseFunctionControlDisplay_Accessor target = new ucPhaseFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnLoad(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            ucPhaseFunctionControlDisplay_Accessor target = new ucPhaseFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetSettings
        ///</summary>
        [TestMethod()]
        public void GetSettingsTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            PhasePhaseSettings expected = null; // TODO: Initialize to an appropriate value
            PhasePhaseSettings actual;
            actual = target.GetSettings();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        public void GetIntervalsTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DrawGraph
        ///</summary>
        [TestMethod()]
        public void DrawGraphTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay(); // TODO: Initialize to an appropriate value
            target.DrawGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            ucPhaseFunctionControlDisplay_Accessor target = new ucPhaseFunctionControlDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucPhaseFunctionControlDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void ucPhaseFunctionControlDisplayConstructorTest()
        {
            ucPhaseFunctionControlDisplay target = new ucPhaseFunctionControlDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
