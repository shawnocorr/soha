﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for PhaseDisplayTest and is intended
    ///to contain all PhaseDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PhaseDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Zoom
        ///</summary>
        [TestMethod()]
        public void ZoomTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Zoom = expected;
            actual = target.Zoom;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for UseThreshold
        ///</summary>
        [TestMethod()]
        public void UseThresholdTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.UseThreshold = expected;
            actual = target.UseThreshold;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Threshold
        ///</summary>
        [TestMethod()]
        public void ThresholdTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Threshold = expected;
            actual = target.Threshold;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Offset
        ///</summary>
        [TestMethod()]
        public void OffsetTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Offset = expected;
            actual = target.Offset;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LoCutoff
        ///</summary>
        [TestMethod()]
        public void LoCutoffTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.LoCutoff = expected;
            actual = target.LoCutoff;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitialList
        ///</summary>
        [TestMethod()]
        public void InitialListTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            IntervalCollection expected = null; // TODO: Initialize to an appropriate value
            IntervalCollection actual;
            target.InitialList = expected;
            actual = target.InitialList;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HiCutoff
        ///</summary>
        [TestMethod()]
        public void HiCutoffTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.HiCutoff = expected;
            actual = target.HiCutoff;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for plot_Paint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void plot_PaintTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.plot_Paint(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for plot_MouseMove
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void plot_MouseMoveTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.plot_MouseMove(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for plot_MouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void plot_MouseClickTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.plot_MouseClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PreFilter
        ///</summary>
        [TestMethod()]
        public void PreFilterTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            List<double> func = null; // TODO: Initialize to an appropriate value
            List<double> funcExpected = null; // TODO: Initialize to an appropriate value
            target.PreFilter(ref func);
            Assert.AreEqual(funcExpected, func);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnZoomChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnZoomChangedTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnZoomChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnThresholdsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnThresholdsChangedTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnThresholdsChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPropertyChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPropertyChangedTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            target.OnPropertyChanged(name);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaintBackground
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintBackgroundTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaintBackground(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnIntervalsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnIntervalsChangedTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnIntervalsChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeToolStripMenu
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeToolStripMenuTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeToolStripMenu();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetValues
        ///</summary>
        [TestMethod()]
        public void GetValuesTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            int x = 0; // TODO: Initialize to an appropriate value
            int interval = 0; // TODO: Initialize to an appropriate value
            int interval_size = 0; // TODO: Initialize to an appropriate value
            double[] expected = null; // TODO: Initialize to an appropriate value
            double[] actual;
            actual = target.GetValues(x, interval, interval_size);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        public void GetIntervalsTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            int zoom = 0; // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals(zoom);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        public void GetIntervalsTest1()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            PhaseDisplay_Accessor target = new PhaseDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddThresholds
        ///</summary>
        [TestMethod()]
        public void AddThresholdsTest()
        {
            PhaseDisplay target = new PhaseDisplay(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            Bitmap b = null; // TODO: Initialize to an appropriate value
            target.AddThresholds(g, b);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PhaseDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void PhaseDisplayConstructorTest()
        {
            PhaseDisplay target = new PhaseDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
