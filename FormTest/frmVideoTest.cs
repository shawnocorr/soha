﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Video;
using SOHA;

namespace FormTest
{
    public partial class frmVideoTest : Form
    {
        private CFVideo m_video;

        public frmVideoTest()
        {
            InitializeComponent();

            m_video = new CFVideo(@"D:\SOHA\Good\10mCS_05_4wf_9300_60s_hypoxia122310 10m.cxd");
            frameDisplay1.VideoFile = m_video;
        }
    }
}
