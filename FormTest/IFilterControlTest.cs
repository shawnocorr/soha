﻿using SOHAControls.FilterControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Imaging;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for IFilterControlTest and is intended
    ///to contain all IFilterControlTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IFilterControlTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        internal virtual IFilterControl CreateIFilterControl()
        {
            // TODO: Instantiate an appropriate concrete class.
            IFilterControl target = null;
            return target;
        }

        /// <summary>
        ///A test for Filter
        ///</summary>
        [TestMethod()]
        public void FilterTest()
        {
            IFilterControl target = CreateIFilterControl(); // TODO: Initialize to an appropriate value
            IFrameFunction actual;
            actual = target.Filter;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
