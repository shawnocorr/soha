﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for CreateMmodeEventArgsTest and is intended
    ///to contain all CreateMmodeEventArgsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CreateMmodeEventArgsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Start
        ///</summary>
        [TestMethod()]
        public void StartTest()
        {
            CreateMmodeEventArgs target = new CreateMmodeEventArgs(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Start = expected;
            actual = target.Start;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for MmodeType
        ///</summary>
        [TestMethod()]
        public void MmodeTypeTest()
        {
            CreateMmodeEventArgs target = new CreateMmodeEventArgs(); // TODO: Initialize to an appropriate value
            CreateMmodeType expected = new CreateMmodeType(); // TODO: Initialize to an appropriate value
            CreateMmodeType actual;
            target.MmodeType = expected;
            actual = target.MmodeType;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FullVideo
        ///</summary>
        [TestMethod()]
        public void FullVideoTest()
        {
            CreateMmodeEventArgs target = new CreateMmodeEventArgs(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.FullVideo = expected;
            actual = target.FullVideo;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for End
        ///</summary>
        [TestMethod()]
        public void EndTest()
        {
            CreateMmodeEventArgs target = new CreateMmodeEventArgs(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.End = expected;
            actual = target.End;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CreateMmodeEventArgs Constructor
        ///</summary>
        [TestMethod()]
        public void CreateMmodeEventArgsConstructorTest()
        {
            CreateMmodeEventArgs target = new CreateMmodeEventArgs();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for CreateMmodeEventArgs Constructor
        ///</summary>
        [TestMethod()]
        public void CreateMmodeEventArgsConstructorTest1()
        {
            bool isFullVideo = false; // TODO: Initialize to an appropriate value
            CreateMmodeType type = new CreateMmodeType(); // TODO: Initialize to an appropriate value
            int start = 0; // TODO: Initialize to an appropriate value
            int end = 0; // TODO: Initialize to an appropriate value
            CreateMmodeEventArgs target = new CreateMmodeEventArgs(isFullVideo, type, start, end);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
