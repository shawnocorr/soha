﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library;
using System.Collections.Generic;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for IIntervalableTest and is intended
    ///to contain all IIntervalableTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IIntervalableTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        internal virtual IIntervalable_Accessor CreateIIntervalable_Accessor()
        {
            // TODO: Instantiate an appropriate concrete class.
            IIntervalable_Accessor target = null;
            return target;
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void GetIntervalsTest()
        {
            IIntervalable_Accessor target = CreateIIntervalable_Accessor(); // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
