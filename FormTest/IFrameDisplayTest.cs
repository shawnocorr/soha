﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Video;
using SOHA.Library;
using SOHA.Library.Xml;
using System.Drawing;
using System.Diagnostics;
using SOHA.Library.Imaging;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for IFrameDisplayTest and is intended
    ///to contain all IFrameDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IFrameDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        internal virtual IFrameDisplay_Accessor CreateIFrameDisplay_Accessor()
        {
            // TODO: Instantiate an appropriate concrete class.
            IFrameDisplay_Accessor target = null;
            return target;
        }

        /// <summary>
        ///A test for VideoFile
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void VideoFileTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            Video expected = null; // TODO: Initialize to an appropriate value
            Video actual;
            target.VideoFile = expected;
            actual = target.VideoFile;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for UserInteractive
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void UserInteractiveTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.UserInteractive = expected;
            actual = target.UserInteractive;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ROI
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void ROITest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            SohaCollection<Roi> expected = null; // TODO: Initialize to an appropriate value
            SohaCollection<Roi> actual;
            target.ROI = expected;
            actual = target.ROI;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PostOpFrame
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void PostOpFrameTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            Frame actual;
            actual = target.PostOpFrame;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for NormalPen
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void NormalPenTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            Pen expected = null; // TODO: Initialize to an appropriate value
            Pen actual;
            target.NormalPen = expected;
            actual = target.NormalPen;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for MmodeDisplay
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MmodeDisplayTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.MmodeDisplay = expected;
            actual = target.MmodeDisplay;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Marks
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MarksTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            SohaCollection<MarkPair> expected = null; // TODO: Initialize to an appropriate value
            SohaCollection<MarkPair> actual;
            target.Marks = expected;
            actual = target.Marks;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LastFilterWatch
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void LastFilterWatchTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            Stopwatch actual;
            actual = target.LastFilterWatch;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HighlightPen
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void HighlightPenTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            Pen expected = null; // TODO: Initialize to an appropriate value
            Pen actual;
            target.HighlightPen = expected;
            actual = target.HighlightPen;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Filters
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void FiltersTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            List<IFrameFunction> expected = null; // TODO: Initialize to an appropriate value
            List<IFrameFunction> actual;
            target.Filters = expected;
            actual = target.Filters;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EnableFilters
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void EnableFiltersTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.EnableFilters = expected;
            actual = target.EnableFilters;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CurrentFrame
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void CurrentFrameTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            Frame actual;
            actual = target.CurrentFrame;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Coordinates
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void CoordinatesTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            Point expected = new Point(); // TODO: Initialize to an appropriate value
            Point actual;
            target.Coordinates = expected;
            actual = target.Coordinates;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for KeyPress
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void KeyPressTest()
        {
            IFrameDisplay_Accessor target = CreateIFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            KeyEventArgs key_event = null; // TODO: Initialize to an appropriate value
            target.KeyPress(key_event);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
