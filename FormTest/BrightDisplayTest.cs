﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Mathematics;
using System.Windows.Forms;
using System.Collections.Generic;
using SOHA.Library.Xml;
using SOHA.Library;
using System.Drawing;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for BrightDisplayTest and is intended
    ///to contain all BrightDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BrightDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for UseDarkness
        ///</summary>
        [TestMethod()]
        public void UseDarknessTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.UseDarkness = expected;
            actual = target.UseDarkness;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LoThreshold
        ///</summary>
        [TestMethod()]
        public void LoThresholdTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.LoThreshold = expected;
            actual = target.LoThreshold;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LoCutoff
        ///</summary>
        [TestMethod()]
        public void LoCutoffTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.LoCutoff = expected;
            actual = target.LoCutoff;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Invert
        ///</summary>
        [TestMethod()]
        public void InvertTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Invert = expected;
            actual = target.Invert;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HiThreshold
        ///</summary>
        [TestMethod()]
        public void HiThresholdTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.HiThreshold = expected;
            actual = target.HiThreshold;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HiCutoff
        ///</summary>
        [TestMethod()]
        public void HiCutoffTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.HiCutoff = expected;
            actual = target.HiCutoff;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FilterFrequency
        ///</summary>
        [TestMethod()]
        public void FilterFrequencyTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.FilterFrequency = expected;
            actual = target.FilterFrequency;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Filter
        ///</summary>
        [TestMethod()]
        public void FilterTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            Filter expected = null; // TODO: Initialize to an appropriate value
            Filter actual;
            target.Filter = expected;
            actual = target.Filter;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for plot_MouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void plot_MouseClickTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.plot_MouseClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PreFilter
        ///</summary>
        [TestMethod()]
        public void PreFilterTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            List<double> func = null; // TODO: Initialize to an appropriate value
            List<double> funcExpected = null; // TODO: Initialize to an appropriate value
            target.PreFilter(ref func);
            Assert.AreEqual(funcExpected, func);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnThresholdsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnThresholdsChangedTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnThresholdsChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPropertyChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPropertyChangedTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            target.OnPropertyChanged(name);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaintBackground
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintBackgroundTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaintBackground(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnIntervalsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnIntervalsChangedTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnIntervalsChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for LoadSettings
        ///</summary>
        [TestMethod()]
        public void LoadSettingsTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            IntervalPhaseSettings settings = null; // TODO: Initialize to an appropriate value
            target.LoadSettings(settings);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeToolStripMenu
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeToolStripMenuTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeToolStripMenu();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetValues
        ///</summary>
        [TestMethod()]
        public void GetValuesTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            int x = 0; // TODO: Initialize to an appropriate value
            int interval = 0; // TODO: Initialize to an appropriate value
            int interval_size = 0; // TODO: Initialize to an appropriate value
            double[] expected = null; // TODO: Initialize to an appropriate value
            double[] actual;
            actual = target.GetValues(x, interval, interval_size);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        public void GetIntervalsTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            BrightDisplay_Accessor target = new BrightDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddThresholds
        ///</summary>
        [TestMethod()]
        public void AddThresholdsTest()
        {
            BrightDisplay target = new BrightDisplay(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            Bitmap b = null; // TODO: Initialize to an appropriate value
            target.AddThresholds(g, b);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BrightDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void BrightDisplayConstructorTest()
        {
            BrightDisplay target = new BrightDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
