﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Xml;
using SOHA.Library;
using System.Collections.Generic;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucPhaseFunctionControlTest and is intended
    ///to contain all ucPhaseFunctionControlTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucPhaseFunctionControlTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Settings
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void SettingsTest()
        {
            ucPhaseFunctionControl_Accessor target = new ucPhaseFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            PhasePhaseSettings actual;
            actual = target.Settings;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitialIntervals
        ///</summary>
        [TestMethod()]
        public void InitialIntervalsTest()
        {
            ucPhaseFunctionControl target = new ucPhaseFunctionControl(); // TODO: Initialize to an appropriate value
            IntervalCollection expected = null; // TODO: Initialize to an appropriate value
            IntervalCollection actual;
            target.InitialIntervals = expected;
            actual = target.InitialIntervals;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            ucPhaseFunctionControl_Accessor target = new ucPhaseFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetIntervals
        ///</summary>
        [TestMethod()]
        public void GetIntervalsTest()
        {
            ucPhaseFunctionControl target = new ucPhaseFunctionControl(); // TODO: Initialize to an appropriate value
            List<Interval> expected = null; // TODO: Initialize to an appropriate value
            List<Interval> actual;
            actual = target.GetIntervals();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DrawThresholds
        ///</summary>
        [TestMethod()]
        public void DrawThresholdsTest()
        {
            ucPhaseFunctionControl target = new ucPhaseFunctionControl(); // TODO: Initialize to an appropriate value
            target.DrawThresholds();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawGraph
        ///</summary>
        [TestMethod()]
        public void DrawGraphTest()
        {
            ucPhaseFunctionControl target = new ucPhaseFunctionControl(); // TODO: Initialize to an appropriate value
            target.DrawGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            ucPhaseFunctionControl_Accessor target = new ucPhaseFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucPhaseFunctionControl Constructor
        ///</summary>
        [TestMethod()]
        public void ucPhaseFunctionControlConstructorTest()
        {
            ucPhaseFunctionControl target = new ucPhaseFunctionControl();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
