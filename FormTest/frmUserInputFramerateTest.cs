﻿using SOHA.Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for frmUserInputFramerateTest and is intended
    ///to contain all frmUserInputFramerateTest Unit Tests
    ///</summary>
    [TestClass()]
    public class frmUserInputFramerateTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Start
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void StartTest()
        {
            frmUserInputFramerate_Accessor target = new frmUserInputFramerate_Accessor(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Start = expected;
            actual = target.Start;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for NewRate
        ///</summary>
        [TestMethod()]
        public void NewRateTest()
        {
            frmUserInputFramerate target = new frmUserInputFramerate(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.NewRate = expected;
            actual = target.NewRate;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for NewFrameRate
        ///</summary>
        [TestMethod()]
        public void NewFrameRateTest()
        {
            frmUserInputFramerate target = new frmUserInputFramerate(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.NewFrameRate = expected;
            actual = target.NewFrameRate;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for End
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void EndTest()
        {
            frmUserInputFramerate_Accessor target = new frmUserInputFramerate_Accessor(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.End = expected;
            actual = target.End;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for rdoTime_CheckedChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void rdoTime_CheckedChangedTest()
        {
            frmUserInputFramerate_Accessor target = new frmUserInputFramerate_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.rdoTime_CheckedChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for rdoFull_CheckedChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void rdoFull_CheckedChangedTest()
        {
            frmUserInputFramerate_Accessor target = new frmUserInputFramerate_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.rdoFull_CheckedChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for cmbMultiplier_SelectedIndexChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void cmbMultiplier_SelectedIndexChangedTest()
        {
            frmUserInputFramerate_Accessor target = new frmUserInputFramerate_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.cmbMultiplier_SelectedIndexChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for button1_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void button1_ClickTest()
        {
            frmUserInputFramerate_Accessor target = new frmUserInputFramerate_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.button1_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            frmUserInputFramerate_Accessor target = new frmUserInputFramerate_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            frmUserInputFramerate_Accessor target = new frmUserInputFramerate_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for frmUserInputFramerate Constructor
        ///</summary>
        [TestMethod()]
        public void frmUserInputFramerateConstructorTest()
        {
            frmUserInputFramerate target = new frmUserInputFramerate();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for frmUserInputFramerate Constructor
        ///</summary>
        [TestMethod()]
        public void frmUserInputFramerateConstructorTest1()
        {
            int orig_rate = 0; // TODO: Initialize to an appropriate value
            int total_frames = 0; // TODO: Initialize to an appropriate value
            frmUserInputFramerate target = new frmUserInputFramerate(orig_rate, total_frames);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
