﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SOHA.Library.Xml;
using System.Drawing;
using ZedGraph;
using System.Windows.Forms;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucFunctionControlTest and is intended
    ///to contain all ucFunctionControlTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucFunctionControlTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for YValues
        ///</summary>
        [TestMethod()]
        public void YValuesTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            List<double> actual;
            actual = target.YValues;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Settings
        ///</summary>
        [TestMethod()]
        public void SettingsTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            PhaseSettings expected = null; // TODO: Initialize to an appropriate value
            PhaseSettings actual;
            target.Settings = expected;
            actual = target.Settings;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for RightClickThresholdEnable
        ///</summary>
        [TestMethod()]
        public void RightClickThresholdEnableTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.RightClickThresholdEnable = expected;
            actual = target.RightClickThresholdEnable;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Range
        ///</summary>
        [TestMethod()]
        public void RangeTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            VideoFrameRange expected = null; // TODO: Initialize to an appropriate value
            VideoFrameRange actual;
            target.Range = expected;
            actual = target.Range;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PlotRectangle
        ///</summary>
        [TestMethod()]
        public void PlotRectangleTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            RectangleF actual;
            actual = target.PlotRectangle;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsAlwaysNormalize
        ///</summary>
        [TestMethod()]
        public void IsAlwaysNormalizeTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.IsAlwaysNormalize = expected;
            actual = target.IsAlwaysNormalize;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GraphPane
        ///</summary>
        [TestMethod()]
        public void GraphPaneTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            GraphPane actual;
            actual = target.GraphPane;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Function
        ///</summary>
        [TestMethod()]
        public void FunctionTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            Dictionary<double, double> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<double, double> actual;
            target.Function = expected;
            actual = target.Function;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for zedGraphControl1_MouseLeave
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void zedGraphControl1_MouseLeaveTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.zedGraphControl1_MouseLeave(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for zedGraphControl1_MouseEnter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void zedGraphControl1_MouseEnterTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.zedGraphControl1_MouseEnter(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for zedGraphControl1_MouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void zedGraphControl1_MouseClickTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.zedGraphControl1_MouseClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for zedGraphControl1_CursorChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void zedGraphControl1_CursorChangedTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.zedGraphControl1_CursorChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for m_settings_SettingsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void m_settings_SettingsChangedTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.m_settings_SettingsChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for m_range_RangeChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void m_range_RangeChangedTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            VideoFrameRangeEventArgs e = null; // TODO: Initialize to an appropriate value
            target.m_range_RangeChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SuspendGraph
        ///</summary>
        [TestMethod()]
        public void SuspendGraphTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            target.SuspendGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetXValuesFromY
        ///</summary>
        [TestMethod()]
        public void SetXValuesFromYTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            List<double> y = null; // TODO: Initialize to an appropriate value
            target.SetXValuesFromY(y);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetSettings
        ///</summary>
        [TestMethod()]
        public void SetSettingsTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            PhaseSettings settings = null; // TODO: Initialize to an appropriate value
            PhaseSettings settingsExpected = null; // TODO: Initialize to an appropriate value
            target.SetSettings(ref settings);
            Assert.AreEqual(settingsExpected, settings);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ResumeGraph
        ///</summary>
        [TestMethod()]
        public void ResumeGraphTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            target.ResumeGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnVideoFrameRangeChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnVideoFrameRangeChangedTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnVideoFrameRangeChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnThresholdsChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnThresholdsChangedTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnThresholdsChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPropertyChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPropertyChangedTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            target.OnPropertyChanged(name);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnMouseClickZedGraph
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnMouseClickZedGraphTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnMouseClickZedGraph(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnLoad
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnLoadTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnLoad(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnFunctionChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnFunctionChangedTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            target.OnFunctionChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GraphFunction
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void GraphFunctionTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            VideoFrameRange range = null; // TODO: Initialize to an appropriate value
            target.GraphFunction(range);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetSettings
        ///</summary>
        [TestMethod()]
        public void GetSettingsTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            PhaseSettings expected = null; // TODO: Initialize to an appropriate value
            PhaseSettings actual;
            actual = target.GetSettings();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DrawThresholds
        ///</summary>
        [TestMethod()]
        public void DrawThresholdsTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            target.DrawThresholds();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawGraph
        ///</summary>
        [TestMethod()]
        public void DrawGraphTest()
        {
            ucFunctionControl target = new ucFunctionControl(); // TODO: Initialize to an appropriate value
            target.DrawGraph();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            ucFunctionControl_Accessor target = new ucFunctionControl_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucFunctionControl Constructor
        ///</summary>
        [TestMethod()]
        public void ucFunctionControlConstructorTest()
        {
            ucFunctionControl target = new ucFunctionControl();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
