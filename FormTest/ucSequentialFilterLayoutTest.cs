﻿using SOHAControls.FilterControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Imaging;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucSequentialFilterLayoutTest and is intended
    ///to contain all ucSequentialFilterLayoutTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucSequentialFilterLayoutTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetFilters
        ///</summary>
        [TestMethod()]
        public void GetFiltersTest()
        {
            ucSequentialFilterLayout target = new ucSequentialFilterLayout(); // TODO: Initialize to an appropriate value
            List<IFrameFunction> actual;
            actual = target.GetFilters;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for grpFilters_DragEnter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void grpFilters_DragEnterTest()
        {
            ucSequentialFilterLayout_Accessor target = new ucSequentialFilterLayout_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            DragEventArgs e = null; // TODO: Initialize to an appropriate value
            target.grpFilters_DragEnter(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for grpFilters_DragDrop
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void grpFilters_DragDropTest()
        {
            ucSequentialFilterLayout_Accessor target = new ucSequentialFilterLayout_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            DragEventArgs e = null; // TODO: Initialize to an appropriate value
            target.grpFilters_DragDrop(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for flwPanelFilters_FilterUpdate
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void flwPanelFilters_FilterUpdateTest()
        {
            ucSequentialFilterLayout_Accessor target = new ucSequentialFilterLayout_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.flwPanelFilters_FilterUpdate(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnUpdate
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnUpdateTest()
        {
            ucSequentialFilterLayout_Accessor target = new ucSequentialFilterLayout_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnUpdate(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            ucSequentialFilterLayout_Accessor target = new ucSequentialFilterLayout_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Initialize
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeTest()
        {
            ucSequentialFilterLayout_Accessor target = new ucSequentialFilterLayout_Accessor(); // TODO: Initialize to an appropriate value
            target.Initialize();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            ucSequentialFilterLayout_Accessor target = new ucSequentialFilterLayout_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucSequentialFilterLayout Constructor
        ///</summary>
        [TestMethod()]
        public void ucSequentialFilterLayoutConstructorTest()
        {
            ucSequentialFilterLayout target = new ucSequentialFilterLayout();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
