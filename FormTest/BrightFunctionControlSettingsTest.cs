﻿using SOHAControls.DisplayControls.FunctionControlSettings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for BrightFunctionControlSettingsTest and is intended
    ///to contain all BrightFunctionControlSettingsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BrightFunctionControlSettingsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Invert
        ///</summary>
        [TestMethod()]
        public void InvertTest()
        {
            BrightFunctionControlSettings target = new BrightFunctionControlSettings(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Invert = expected;
            actual = target.Invert;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for OnPropertyChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPropertyChangedTest()
        {
            BrightFunctionControlSettings_Accessor target = new BrightFunctionControlSettings_Accessor(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            target.OnPropertyChanged(name);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BrightFunctionControlSettings Constructor
        ///</summary>
        [TestMethod()]
        public void BrightFunctionControlSettingsConstructorTest()
        {
            BrightFunctionControlSettings target = new BrightFunctionControlSettings();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
