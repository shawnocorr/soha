﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;
using System.ComponentModel;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for NumericTextboxTest and is intended
    ///to contain all NumericTextboxTest Unit Tests
    ///</summary>
    [TestClass()]
    public class NumericTextboxTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for IntValue
        ///</summary>
        [TestMethod()]
        public void IntValueTest()
        {
            NumericTextbox target = new NumericTextbox(); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.IntValue;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DecimalValue
        ///</summary>
        [TestMethod()]
        public void DecimalValueTest()
        {
            NumericTextbox target = new NumericTextbox(); // TODO: Initialize to an appropriate value
            Decimal actual;
            actual = target.DecimalValue;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for AllowNulls
        ///</summary>
        [TestMethod()]
        public void AllowNullsTest()
        {
            NumericTextbox target = new NumericTextbox(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.AllowNulls = expected;
            actual = target.AllowNulls;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for AllowNegatives
        ///</summary>
        [TestMethod()]
        public void AllowNegativesTest()
        {
            NumericTextbox target = new NumericTextbox(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.AllowNegatives = expected;
            actual = target.AllowNegatives;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for OnValueChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnValueChangedTest()
        {
            NumericTextbox_Accessor target = new NumericTextbox_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnValueChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnLostFocus
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnLostFocusTest()
        {
            NumericTextbox_Accessor target = new NumericTextbox_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnLostFocus(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnKeyPress
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnKeyPressTest()
        {
            NumericTextbox_Accessor target = new NumericTextbox_Accessor(); // TODO: Initialize to an appropriate value
            KeyPressEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnKeyPress(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            NumericTextbox_Accessor target = new NumericTextbox_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            NumericTextbox_Accessor target = new NumericTextbox_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for NumericTextbox Constructor
        ///</summary>
        [TestMethod()]
        public void NumericTextboxConstructorTest()
        {
            IContainer container = null; // TODO: Initialize to an appropriate value
            NumericTextbox target = new NumericTextbox(container);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for NumericTextbox Constructor
        ///</summary>
        [TestMethod()]
        public void NumericTextboxConstructorTest1()
        {
            NumericTextbox target = new NumericTextbox();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
