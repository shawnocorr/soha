﻿using SOHAControls.FilterControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ISelectableTest and is intended
    ///to contain all ISelectableTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ISelectableTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        internal virtual ISelectable CreateISelectable()
        {
            // TODO: Instantiate an appropriate concrete class.
            ISelectable target = null;
            return target;
        }

        /// <summary>
        ///A test for IsSelected
        ///</summary>
        [TestMethod()]
        public void IsSelectedTest()
        {
            ISelectable target = CreateISelectable(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsSelected;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsHighlighted
        ///</summary>
        [TestMethod()]
        public void IsHighlightedTest()
        {
            ISelectable target = CreateISelectable(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsHighlighted;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ToggleSelected
        ///</summary>
        [TestMethod()]
        public void ToggleSelectedTest()
        {
            ISelectable target = CreateISelectable(); // TODO: Initialize to an appropriate value
            target.ToggleSelected();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ToggleHighlight
        ///</summary>
        [TestMethod()]
        public void ToggleHighlightTest()
        {
            ISelectable target = CreateISelectable(); // TODO: Initialize to an appropriate value
            target.ToggleHighlight();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
