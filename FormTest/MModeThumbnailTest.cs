﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for MModeThumbnailTest and is intended
    ///to contain all MModeThumbnailTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MModeThumbnailTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for XCoord
        ///</summary>
        [TestMethod()]
        public void XCoordTest()
        {
            MModeThumbnail target = new MModeThumbnail(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.XCoord = expected;
            actual = target.XCoord;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Thumbnail
        ///</summary>
        [TestMethod()]
        public void ThumbnailTest()
        {
            MModeThumbnail target = new MModeThumbnail(); // TODO: Initialize to an appropriate value
            Bitmap expected = null; // TODO: Initialize to an appropriate value
            Bitmap actual;
            target.Thumbnail = expected;
            actual = target.Thumbnail;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Paint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void PaintTest()
        {
            MModeThumbnail_Accessor target = new MModeThumbnail_Accessor(); // TODO: Initialize to an appropriate value
            target.Paint();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MModeThumbnail_MouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MModeThumbnail_MouseClickTest()
        {
            MModeThumbnail_Accessor target = new MModeThumbnail_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.MModeThumbnail_MouseClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            MModeThumbnail_Accessor target = new MModeThumbnail_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            MModeThumbnail_Accessor target = new MModeThumbnail_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MModeThumbnail Constructor
        ///</summary>
        [TestMethod()]
        public void MModeThumbnailConstructorTest()
        {
            MModeThumbnail target = new MModeThumbnail();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for MModeThumbnail Constructor
        ///</summary>
        [TestMethod()]
        public void MModeThumbnailConstructorTest1()
        {
            IContainer container = null; // TODO: Initialize to an appropriate value
            MModeThumbnail target = new MModeThumbnail(container);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
