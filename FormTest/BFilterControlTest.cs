﻿using SOHAControls.FilterControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Imaging;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for BFilterControlTest and is intended
    ///to contain all BFilterControlTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BFilterControlTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SOHAControls.FilterControls.IFilterControl.Filter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void FilterTest()
        {
            IFilterControl target = new BFilterControl(); // TODO: Initialize to an appropriate value
            IFrameFunction actual;
            actual = target.Filter;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsSelected
        ///</summary>
        [TestMethod()]
        public void IsSelectedTest()
        {
            BFilterControl target = new BFilterControl(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsSelected;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsHighlighted
        ///</summary>
        [TestMethod()]
        public void IsHighlightedTest()
        {
            BFilterControl target = new BFilterControl(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsHighlighted;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Filter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void FilterTest1()
        {
            BFilterControl_Accessor target = new BFilterControl_Accessor(); // TODO: Initialize to an appropriate value
            IFrameFunction expected = null; // TODO: Initialize to an appropriate value
            IFrameFunction actual;
            target.Filter = expected;
            actual = target.Filter;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ToggleSelected
        ///</summary>
        [TestMethod()]
        public void ToggleSelectedTest()
        {
            BFilterControl target = new BFilterControl(); // TODO: Initialize to an appropriate value
            target.ToggleSelected();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ToggleHighlight
        ///</summary>
        [TestMethod()]
        public void ToggleHighlightTest()
        {
            BFilterControl target = new BFilterControl(); // TODO: Initialize to an appropriate value
            target.ToggleHighlight();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            BFilterControl_Accessor target = new BFilterControl_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            BFilterControl_Accessor target = new BFilterControl_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BFilterControl Constructor
        ///</summary>
        [TestMethod()]
        public void BFilterControlConstructorTest()
        {
            BFilterControl target = new BFilterControl();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
