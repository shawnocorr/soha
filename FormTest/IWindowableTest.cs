﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for IWindowableTest and is intended
    ///to contain all IWindowableTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IWindowableTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        internal virtual IWindowable CreateIWindowable()
        {
            // TODO: Instantiate an appropriate concrete class.
            IWindowable target = null;
            return target;
        }

        /// <summary>
        ///A test for WindowSize
        ///</summary>
        [TestMethod()]
        public void WindowSizeTest()
        {
            IWindowable target = CreateIWindowable(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.WindowSize = expected;
            actual = target.WindowSize;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CurrentWindow
        ///</summary>
        [TestMethod()]
        public void CurrentWindowTest()
        {
            IWindowable target = CreateIWindowable(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.CurrentWindow = expected;
            actual = target.CurrentWindow;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PreviousWindow
        ///</summary>
        [TestMethod()]
        public void PreviousWindowTest()
        {
            IWindowable target = CreateIWindowable(); // TODO: Initialize to an appropriate value
            target.PreviousWindow();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnWindowChanged
        ///</summary>
        [TestMethod()]
        public void OnWindowChangedTest()
        {
            IWindowable target = CreateIWindowable(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnWindowChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for NextWindow
        ///</summary>
        [TestMethod()]
        public void NextWindowTest()
        {
            IWindowable target = CreateIWindowable(); // TODO: Initialize to an appropriate value
            target.NextWindow();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
