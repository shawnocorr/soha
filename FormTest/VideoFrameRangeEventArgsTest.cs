﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for VideoFrameRangeEventArgsTest and is intended
    ///to contain all VideoFrameRangeEventArgsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class VideoFrameRangeEventArgsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Range
        ///</summary>
        [TestMethod()]
        public void RangeTest()
        {
            VideoFrameRange range = null; // TODO: Initialize to an appropriate value
            VideoFrameRangeEventArgs target = new VideoFrameRangeEventArgs(range); // TODO: Initialize to an appropriate value
            VideoFrameRange actual;
            actual = target.Range;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for VideoFrameRangeEventArgs Constructor
        ///</summary>
        [TestMethod()]
        public void VideoFrameRangeEventArgsConstructorTest()
        {
            VideoFrameRange range = null; // TODO: Initialize to an appropriate value
            VideoFrameRangeEventArgs target = new VideoFrameRangeEventArgs(range);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
