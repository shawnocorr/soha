﻿using SOHAControls.DisplayControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Video;
using SOHA.Library;
using SOHA.Library.Xml;
using System.Drawing;
using System.Diagnostics;
using SOHA.Library.Imaging;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for MModeFrameDisplayTest and is intended
    ///to contain all MModeFrameDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MModeFrameDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for XCoord
        ///</summary>
        [TestMethod()]
        public void XCoordTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.XCoord = expected;
            actual = target.XCoord;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for VideoFile
        ///</summary>
        [TestMethod()]
        public void VideoFileTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            Video expected = null; // TODO: Initialize to an appropriate value
            Video actual;
            target.VideoFile = expected;
            actual = target.VideoFile;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for UserInteractive
        ///</summary>
        [TestMethod()]
        public void UserInteractiveTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.UserInteractive = expected;
            actual = target.UserInteractive;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ROI
        ///</summary>
        [TestMethod()]
        public void ROITest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            SohaCollection<Roi> expected = null; // TODO: Initialize to an appropriate value
            SohaCollection<Roi> actual;
            target.ROI = expected;
            actual = target.ROI;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PostOpFrame
        ///</summary>
        [TestMethod()]
        public void PostOpFrameTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            Frame actual;
            actual = target.PostOpFrame;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for NormalPen
        ///</summary>
        [TestMethod()]
        public void NormalPenTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            Pen expected = null; // TODO: Initialize to an appropriate value
            Pen actual;
            target.NormalPen = expected;
            actual = target.NormalPen;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for MmodeDisplay
        ///</summary>
        [TestMethod()]
        public void MmodeDisplayTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.MmodeDisplay = expected;
            actual = target.MmodeDisplay;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Marks
        ///</summary>
        [TestMethod()]
        public void MarksTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            SohaCollection<MarkPair> expected = null; // TODO: Initialize to an appropriate value
            SohaCollection<MarkPair> actual;
            target.Marks = expected;
            actual = target.Marks;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LastFilterWatch
        ///</summary>
        [TestMethod()]
        public void LastFilterWatchTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            Stopwatch actual;
            actual = target.LastFilterWatch;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HighlightPen
        ///</summary>
        [TestMethod()]
        public void HighlightPenTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            Pen expected = null; // TODO: Initialize to an appropriate value
            Pen actual;
            target.HighlightPen = expected;
            actual = target.HighlightPen;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Filters
        ///</summary>
        [TestMethod()]
        public void FiltersTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            List<IFrameFunction> expected = null; // TODO: Initialize to an appropriate value
            List<IFrameFunction> actual;
            target.Filters = expected;
            actual = target.Filters;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EnableFilters
        ///</summary>
        [TestMethod()]
        public void EnableFiltersTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.EnableFilters = expected;
            actual = target.EnableFilters;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CurrentFrame
        ///</summary>
        [TestMethod()]
        public void CurrentFrameTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            Frame actual;
            actual = target.CurrentFrame;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Coordinates
        ///</summary>
        [TestMethod()]
        public void CoordinatesTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            Point expected = new Point(); // TODO: Initialize to an appropriate value
            Point actual;
            target.Coordinates = expected;
            actual = target.Coordinates;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ucMmodeSelection1_CreateMmode
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void ucMmodeSelection1_CreateMmodeTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            CreateMmodeEventArgs e = null; // TODO: Initialize to an appropriate value
            target.ucMmodeSelection1_CreateMmode(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for scrollXCoord_Scroll
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void scrollXCoord_ScrollTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            ScrollEventArgs e = null; // TODO: Initialize to an appropriate value
            target.scrollXCoord_Scroll(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for mModeThumbnail1_XCoordChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void mModeThumbnail1_XCoordChangedTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.mModeThumbnail1_XCoordChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for mModeDisplay1_Paint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void mModeDisplay1_PaintTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.mModeDisplay1_Paint(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnVideoChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnVideoChangedTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnVideoChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnSetXCoordinate
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnSetXCoordinateTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnSetXCoordinate();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnHistogramUpdated
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnHistogramUpdatedTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnHistogramUpdated();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnFrameChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnFrameChangedTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnFrameChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnFiltersChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnFiltersChangedTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnFiltersChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MModeFrameDisplay_Resize
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MModeFrameDisplay_ResizeTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.MModeFrameDisplay_Resize(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for KeyPress
        ///</summary>
        [TestMethod()]
        public void KeyPressTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay(); // TODO: Initialize to an appropriate value
            KeyEventArgs key_event = null; // TODO: Initialize to an appropriate value
            target.KeyPress(key_event);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Initialize
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.Initialize();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            MModeFrameDisplay_Accessor target = new MModeFrameDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MModeFrameDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void MModeFrameDisplayConstructorTest()
        {
            MModeFrameDisplay target = new MModeFrameDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
