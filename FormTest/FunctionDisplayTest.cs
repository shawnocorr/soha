﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for FunctionDisplayTest and is intended
    ///to contain all FunctionDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FunctionDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for LastData
        ///</summary>
        [TestMethod()]
        public void LastDataTest()
        {
            FunctionDisplay target = new FunctionDisplay(); // TODO: Initialize to an appropriate value
            double[] actual;
            actual = target.LastData;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Interval
        ///</summary>
        [TestMethod()]
        public void IntervalTest()
        {
            FunctionDisplay target = new FunctionDisplay(); // TODO: Initialize to an appropriate value
            IntervalProperties expected = null; // TODO: Initialize to an appropriate value
            IntervalProperties actual;
            target.Interval = expected;
            actual = target.Interval;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitialData
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitialDataTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            List<double> expected = null; // TODO: Initialize to an appropriate value
            List<double> actual;
            target.InitialData = expected;
            actual = target.InitialData;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Fps
        ///</summary>
        [TestMethod()]
        public void FpsTest()
        {
            FunctionDisplay target = new FunctionDisplay(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            target.Fps = expected;
            actual = target.Fps;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Data
        ///</summary>
        [TestMethod()]
        public void DataTest()
        {
            FunctionDisplay target = new FunctionDisplay(); // TODO: Initialize to an appropriate value
            List<double> expected = null; // TODO: Initialize to an appropriate value
            List<double> actual;
            target.Data = expected;
            actual = target.Data;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for m_interval_IntervalChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void m_interval_IntervalChangedTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.m_interval_IntervalChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PreFilter
        ///</summary>
        [TestMethod()]
        public void PreFilterTest()
        {
            FunctionDisplay target = new FunctionDisplay(); // TODO: Initialize to an appropriate value
            List<double> func = null; // TODO: Initialize to an appropriate value
            List<double> funcExpected = null; // TODO: Initialize to an appropriate value
            target.PreFilter(ref func);
            Assert.AreEqual(funcExpected, func);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Paint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void PaintTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.Paint();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnResize
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnResizeTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnResize(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaintBackground
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintBackgroundTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaintBackground(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnInvalidated
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnInvalidatedTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            InvalidateEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnInvalidated(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnDataChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnDataChangedTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnDataChanged(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Graph
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void GraphTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            List<double> f = null; // TODO: Initialize to an appropriate value
            List<double> t = null; // TODO: Initialize to an appropriate value
            target.Graph(f, t);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            FunctionDisplay_Accessor target = new FunctionDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddThresholds
        ///</summary>
        [TestMethod()]
        public void AddThresholdsTest()
        {
            FunctionDisplay target = new FunctionDisplay(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            Bitmap b = null; // TODO: Initialize to an appropriate value
            target.AddThresholds(g, b);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for FunctionDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void FunctionDisplayConstructorTest()
        {
            FunctionDisplay target = new FunctionDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
