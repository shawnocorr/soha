﻿using SOHAControls.FilterControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Imaging;
using System.Windows.Forms;
using System.ComponentModel;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for ucFilterDisplayTest and is intended
    ///to contain all ucFilterDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ucFilterDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for IsSelected
        ///</summary>
        [TestMethod()]
        public void IsSelectedTest()
        {
            IFrameFunction filter = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay target = new ucFilterDisplay(filter); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsSelected;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsHighlighted
        ///</summary>
        [TestMethod()]
        public void IsHighlightedTest()
        {
            IFrameFunction filter = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay target = new ucFilterDisplay(filter); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsHighlighted;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FilterName
        ///</summary>
        [TestMethod()]
        public void FilterNameTest()
        {
            IFrameFunction filter = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay target = new ucFilterDisplay(filter); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.FilterName = expected;
            actual = target.FilterName;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ToggleSelected
        ///</summary>
        [TestMethod()]
        public void ToggleSelectedTest()
        {
            IFrameFunction filter = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay target = new ucFilterDisplay(filter); // TODO: Initialize to an appropriate value
            target.ToggleSelected();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ToggleHighlight
        ///</summary>
        [TestMethod()]
        public void ToggleHighlightTest()
        {
            IFrameFunction filter = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay target = new ucFilterDisplay(filter); // TODO: Initialize to an appropriate value
            target.ToggleHighlight();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay_Accessor target = new ucFilterDisplay_Accessor(param0); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnMouseLeave
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnMouseLeaveTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay_Accessor target = new ucFilterDisplay_Accessor(param0); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnMouseLeave(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnMouseEnter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnMouseEnterTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay_Accessor target = new ucFilterDisplay_Accessor(param0); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnMouseEnter(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnMouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnMouseClickTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay_Accessor target = new ucFilterDisplay_Accessor(param0); // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnMouseClick(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeFilterDisplay
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeFilterDisplayTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay_Accessor target = new ucFilterDisplay_Accessor(param0); // TODO: Initialize to an appropriate value
            IFrameFunction filter = null; // TODO: Initialize to an appropriate value
            target.InitializeFilterDisplay(filter);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay_Accessor target = new ucFilterDisplay_Accessor(param0); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay_Accessor target = new ucFilterDisplay_Accessor(param0); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ucFilterDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void ucFilterDisplayConstructorTest()
        {
            IFrameFunction filter = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay target = new ucFilterDisplay(filter);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ucFilterDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void ucFilterDisplayConstructorTest1()
        {
            IContainer container = null; // TODO: Initialize to an appropriate value
            ucFilterDisplay target = new ucFilterDisplay(container);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
