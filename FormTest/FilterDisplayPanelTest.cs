﻿using SOHAControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHA.Library.Imaging;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for FilterDisplayPanelTest and is intended
    ///to contain all FilterDisplayPanelTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FilterDisplayPanelTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SetFilters
        ///</summary>
        [TestMethod()]
        public void SetFiltersTest()
        {
            FilterDisplayPanel target = new FilterDisplayPanel(); // TODO: Initialize to an appropriate value
            List<IFrameFunction> filters = null; // TODO: Initialize to an appropriate value
            target.SetFilters(filters);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPreviewKeyDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPreviewKeyDownTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            PreviewKeyDownEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPreviewKeyDown(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnPaint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnMouseLeave
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnMouseLeaveTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnMouseLeave(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnMouseEnter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnMouseEnterTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnMouseEnter(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnFilterUpdate
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnFilterUpdateTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnFilterUpdate(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnDragEnter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnDragEnterTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            DragEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnDragEnter(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnDragDrop
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnDragDropTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            DragEventArgs e = null; // TODO: Initialize to an appropriate value
            target.OnDragDrop(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetFilters
        ///</summary>
        [TestMethod()]
        public void GetFiltersTest()
        {
            FilterDisplayPanel target = new FilterDisplayPanel(); // TODO: Initialize to an appropriate value
            List<IFrameFunction> expected = null; // TODO: Initialize to an appropriate value
            List<IFrameFunction> actual;
            actual = target.GetFilters();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FiltersChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void FiltersChangedTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            ControlEventArgs e = null; // TODO: Initialize to an appropriate value
            target.FiltersChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            FilterDisplayPanel_Accessor target = new FilterDisplayPanel_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for FilterDisplayPanel Constructor
        ///</summary>
        [TestMethod()]
        public void FilterDisplayPanelConstructorTest()
        {
            IContainer container = null; // TODO: Initialize to an appropriate value
            FilterDisplayPanel target = new FilterDisplayPanel(container);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for FilterDisplayPanel Constructor
        ///</summary>
        [TestMethod()]
        public void FilterDisplayPanelConstructorTest1()
        {
            FilterDisplayPanel target = new FilterDisplayPanel();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
