﻿using Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SOHAControls.DisplayControls;
using SOHA.Library;
using SOHAControls;
using System.Windows.Forms;
using System.ComponentModel;

namespace FormTest
{
    
    
    /// <summary>
    ///This is a test class for MModeDisplayTest and is intended
    ///to contain all MModeDisplayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MModeDisplayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Zoom
        ///</summary>
        [TestMethod()]
        public void ZoomTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.Zoom = expected;
            actual = target.Zoom;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ShowChangeIntervals
        ///</summary>
        [TestMethod()]
        public void ShowChangeIntervalsTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.ShowChangeIntervals = expected;
            actual = target.ShowChangeIntervals;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Range
        ///</summary>
        [TestMethod()]
        public void RangeTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            VideoFrameRange expected = null; // TODO: Initialize to an appropriate value
            VideoFrameRange actual;
            target.Range = expected;
            actual = target.Range;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PhaseOffset
        ///</summary>
        [TestMethod()]
        public void PhaseOffsetTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.PhaseOffset = expected;
            actual = target.PhaseOffset;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Phase
        ///</summary>
        [TestMethod()]
        public void PhaseTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            PhaseFormType expected = new PhaseFormType(); // TODO: Initialize to an appropriate value
            PhaseFormType actual;
            target.Phase = expected;
            actual = target.Phase;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for MMode
        ///</summary>
        [TestMethod()]
        public void MModeTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            MMode expected = null; // TODO: Initialize to an appropriate value
            MMode actual;
            target.MMode = expected;
            actual = target.MMode;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for List
        ///</summary>
        [TestMethod()]
        public void ListTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            IntervalCollection expected = null; // TODO: Initialize to an appropriate value
            IntervalCollection actual;
            target.List = expected;
            actual = target.List;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Interval
        ///</summary>
        [TestMethod()]
        public void IntervalTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            IntervalProperties expected = null; // TODO: Initialize to an appropriate value
            IntervalProperties actual;
            target.Interval = expected;
            actual = target.Interval;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DisplayCoordinates
        ///</summary>
        [TestMethod()]
        public void DisplayCoordinatesTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.DisplayCoordinates = expected;
            actual = target.DisplayCoordinates;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for m_range_RangeChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void m_range_RangeChangedTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            VideoFrameRangeEventArgs e = null; // TODO: Initialize to an appropriate value
            target.m_range_RangeChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for m_interval_IntervalChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void m_interval_IntervalChangedTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.m_interval_IntervalChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for addVelocityToolStripMenuItem_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void addVelocityToolStripMenuItem_ClickTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.addVelocityToolStripMenuItem_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for addSystoleToolStripMenuItem_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void addSystoleToolStripMenuItem_ClickTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.addSystoleToolStripMenuItem_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for addDiastoleToolStripMenuItem_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void addDiastoleToolStripMenuItem_ClickTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.addDiastoleToolStripMenuItem_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for addChangeToolStripMenuItem_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void addChangeToolStripMenuItem_ClickTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.addChangeToolStripMenuItem_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SuspendPaint
        ///</summary>
        [TestMethod()]
        public void SuspendPaintTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            target.SuspendPaint();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ResumePaint
        ///</summary>
        [TestMethod()]
        public void ResumePaintTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            bool resume = false; // TODO: Initialize to an appropriate value
            target.ResumePaint(resume);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Paint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void PaintTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.Paint();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Paint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void PaintTest1()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            VideoFrameRangeEventArgs e = null; // TODO: Initialize to an appropriate value
            target.Paint(e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnToggleChangeIntervals
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnToggleChangeIntervalsTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnToggleChangeIntervals();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPhaseTypeChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPhaseTypeChangedTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnPhaseTypeChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaintBackground
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintBackgroundTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs pevent = null; // TODO: Initialize to an appropriate value
            target.OnPaintBackground(pevent);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnPaint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnPaintTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            PaintEventArgs pe = null; // TODO: Initialize to an appropriate value
            target.OnPaint(pe);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for OnListChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void OnListChangedTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.OnListChanged();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MModeDisplay_PreviewKeyDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MModeDisplay_PreviewKeyDownTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            PreviewKeyDownEventArgs e = null; // TODO: Initialize to an appropriate value
            target.MModeDisplay_PreviewKeyDown(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MModeDisplay_MouseEnter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MModeDisplay_MouseEnterTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.MModeDisplay_MouseEnter(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MModeDisplay_MouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void MModeDisplay_MouseClickTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.MModeDisplay_MouseClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for List_ListChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void List_ListChangedTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            ListChangedEventArgs e = null; // TODO: Initialize to an appropriate value
            target.List_ListChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void InitializeComponentTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetXValue
        ///</summary>
        [TestMethod()]
        public void GetXValueTest()
        {
            MModeDisplay target = new MModeDisplay(); // TODO: Initialize to an appropriate value
            int x = 0; // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.GetXValue(x);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void DisposeTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CreatePopupMenu
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHAControls.dll")]
        public void CreatePopupMenuTest()
        {
            MModeDisplay_Accessor target = new MModeDisplay_Accessor(); // TODO: Initialize to an appropriate value
            target.CreatePopupMenu();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MModeDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void MModeDisplayConstructorTest()
        {
            MModeDisplay target = new MModeDisplay();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for MModeDisplay Constructor
        ///</summary>
        [TestMethod()]
        public void MModeDisplayConstructorTest1()
        {
            IContainer container = null; // TODO: Initialize to an appropriate value
            MModeDisplay target = new MModeDisplay(container);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
