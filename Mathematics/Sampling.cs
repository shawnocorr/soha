﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpticalHeartBeatAnalysis.Mathematics
{
    public class Sampling
    {

        public static double[] Downsample(double[] x, int n)
        {
            double[] y = new double[x.Length];

            for (int i = 0; i < x.Length; i++)
            {
                if (i % n == 0)
                {
                    y[i] = x[i];
                }
                else
                {
                    y[i] = 0;
                }
            }

            return y;
        }
    }
}
