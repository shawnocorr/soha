﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpticalHeartBeatAnalysis.Mathematics.Filtering
{
    public enum FilterType
    {
        Low,
        High,
        Bandpass
    }

    public class Filter
    {
        /* Denominator, Feedback */
        protected double[] A;

        /* Numerator, Feedforward */
        protected double[] B;

        public double[] Denominator
        {
            get
            {
                return A;
            }
            set
            {
                A = value;
            }
        }

        public double[] Numerator
        {
            get
            {
                return B;
            }
            set
            {
                B = value;
            }
        }

        public Filter()
        {
            A = new double[1];
            A[0] = 1;

            B = new double[1];
            B[0] = 1;
        }

        /// <summary>
        /// Non-Linear Phase Default Digital Filtering
        /// </summary>
        /// <param name="x">Input sequence</param>
        /// <returns>Filtered sequence</returns>
        public virtual double[] Filt(double[] x)
        {
            double[] y = new double[x.Length];

            for (int i = 0; i < x.Length; i++)
            {
                double val = B[0] * x[i];

                for (int j = 1; j < A.Length; j++)
                {
                    if (i - j < 0)
                        break;

                    val += B[j] * x[i - j];
                    val -= A[j] * y[i - j];
                }

                y[i] = val;
            }

            return y;
        }

        /// <summary>
        /// Zero-Phase Filter. Filter, Reverse, Filter, Reverse, Output
        /// </summary>
        /// <param name="x">Input Sequence</param>
        /// <returns>Output Sequence</returns>
        public virtual double[] ZeroPhaseFilter(double[] x)
        {
            double[] y = Filt(x);
            y = y.Reverse().ToArray();
            return Filt(y).Reverse().ToArray();
        }

        public virtual double[] DualInputZeroPhaseFilter(double[] x)
        {
            double[] new_x = new double[3*x.Length];

            for (int i = 0; i < x.Length; i++)
            {
                new_x[i] = new_x[i + x.Length] = new_x[i + 2*x.Length] = x[i];
            }

            double[] y = ZeroPhaseFilter(new_x);

            double[] new_y = new double[x.Length];

            for (int i = 0; i < new_y.Length; i++)
            {
                new_y[i] = y[i + x.Length];
            }

            return new_y;
        }
    }
}
