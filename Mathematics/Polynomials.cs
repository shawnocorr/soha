﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mathematics;

namespace OpticalHeartBeatAnalysis.Mathematics
{
    public class Polynomials
    {
        public static double[] PolynomialMultiplication(double[] v1, double[] v2)
        {
            //int length = v1.Length > v2.Length ? v1.Length : v2.Length;
            int length = (v1.Length-1) + (v2.Length-1) + 1;
            double[] ret_vector = new double[length];

            for (int i = 0; i < v1.Length; i++)
            {
                for (int j = 0; j < v2.Length; j++)
                {
                    ret_vector[i + j] += v1[i] * v2[j];
                }
            }

            return ret_vector;
        }

        public static double[] Scale(double A, double[] v)
        {
            double[] ret_v = (double[])v.Clone();
            for (int i = 0; i < v.Length; i++)
            {
                ret_v[i] = v[i]*A;
            }
            return ret_v;
        }

        /// <summary>
        /// Returns the roots of the polynomial with given coefficients.
        /// </summary>
        /// <param name="poly_coeffs"></param>
        /// <returns></returns>
        public static double[] Roots(double[] poly_coeffs)
        {
            throw new NotImplementedException();
        }

        public static double[] Poly(List<Complex> roots)
        {
            if (roots.Count == 2)
            {
                return new double[] { 1, -roots[0] - roots[1], roots[0] * roots[1] };
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static double[] Poly(List<double> roots)
        {
            int n = roots.Count;
            Vector v_ret = new Vector(n + 1);
            v_ret = v_ret.LeftShift(1);
            v_ret = v_ret.LeftShift(-roots[0]);

            for (int i = 1; i < n; i++)
            {
                v_ret = -roots[i] * v_ret + v_ret.LeftShift();
            }

            return v_ret;
        }

        public static Vector Poly(Vector roots)
        {
            int n = roots.Size;
            Vector v_ret = new Vector(n + 1);
            v_ret = v_ret.LeftShift(1);
            v_ret = v_ret.LeftShift(-roots[0]);

            for (int i = 1; i < n; i++)
            {
                v_ret = -roots[i] * v_ret + v_ret.LeftShift();
            }

            return v_ret;
        }

        /// <summary>
        /// Determines the characteristic equation of the input matrix using
        /// Leverrier's Algorithm *Extend to inverse determination*
        /// </summary>
        /// <param name="roots"></param>
        public static Vector Poly(Matrix roots)
        {
            List<double> s = new List<double>(roots.Rows);

            //Calculate S values
            Matrix power_matrix = (Matrix)roots.Clone();
            s.Add(power_matrix.Trace());
            for (int i = 1; i < roots.Rows; i++)
            {
                power_matrix *= roots;
                s.Add(power_matrix.Trace());
            }

            //Calculate P values
            List<double> p = new List<double>(roots.Rows);
            p.Add(-s[0]);
            for (int i = 1; i < roots.Rows; i++)
            {                
                double sum =0;
                for(int j = 0; j < i;j++)
                {
                    sum += s[i - j - 1]*p[j];
                }
                sum += s[i];
                sum *= -(1.0 / (i + 1));
                p.Add(sum);
            }

            p.Insert(0,1);

            return new Vector(p.ToArray());
        }

        public static double[] FindCoefficients(int width, double[] function)
        {
            //http://www.ce.ufl.edu/~kgurl/Classes/Lect3421/NM5_curve_s02.pdf

            if (width != function.Length)
                throw new MathematicsException("Error in FindCoefficients: width does not match function array length.");

            double[] x = new double[width];
            for(int i = 0; i < width;i++)
                x[i] = i;

            Matrix A = new Matrix(5, 5);
            Matrix B = new Matrix(5, 1);

            A[0, 0] = width;
            A[0, 1] = A[1, 0] = x.Aggregate((total, value) => total + Math.Pow(value, 1));
            A[0, 2] = A[1, 1] = A[2, 0] = x.Aggregate((total, value) => total + Math.Pow(value, 2));
            A[0, 3] = A[1, 2] = A[2, 1] = A[0, 3] = x.Aggregate((total, value) => total + Math.Pow(value, 3));
            A[0, 4] = A[1, 3] = A[2, 2] = A[3, 1] = A[4, 0] = x.Aggregate((total, value) => total + Math.Pow(value, 4));
            A[1, 4] = A[2, 3] = A[3, 2] = A[4, 1] = x.Aggregate((total, value) => total + Math.Pow(value, 5));
            A[2, 4] = A[3, 3] = A[4, 2] = x.Aggregate((total, value) => total + Math.Pow(value, 6));
            A[3, 4] = A[4, 3] = x.Aggregate((total, value) => total + Math.Pow(value, 7));
            A[4, 4] = x.Aggregate((total, value) => total + Math.Pow(value, 8));

            for (int i = 0; i < width; i++)
                B[0,0] += Math.Pow(x[i], 0) * function[i];

            for (int i = 0; i < width; i++)
                B[1, 0] += Math.Pow(x[i], 1) * function[i];

            for (int i = 0; i < width; i++)
                B[2, 0] += Math.Pow(x[i], 2) * function[i];

            for (int i = 0; i < width; i++)
                B[3, 0] += Math.Pow(x[i], 3) * function[i];

            for (int i = 0; i < width; i++)
                B[4, 0] += Math.Pow(x[i], 4) * function[i];

            Matrix X = ~A * B;

            return new double[] { 1.0 };
        }
    }
}
