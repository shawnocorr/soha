﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mathematics
{
    public class ArrayFunctions
    {

        public static List<double> Normalize(List<double> data)
        {
            double H = data[0]; double L = data[0];

            foreach (double d in data)
            {
                if (d > H)
                    H = d;

                if (d < L)
                    L = d;
            }

            double scale = H - L;

            for (int i = 0; i < data.Count; i++)
            {
                data[i] -= L;
                data[i] /= scale;
            }

            return data;
        }

        public static void Invert(ref List<double> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                data[i] = 1 - data[i];
            }
        }

        public static double[] PlusMinusWindow(double[] data, int sd)
        {
            double mean = Statistics.Mean(data);
            double window = sd * Statistics.StdDev(data);
            double hi = mean + window;
            double lo = mean - window;

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] > hi)
                    data[i] = hi;

                if (data[i] < lo)
                    data[i] = lo;
            }

            return data;
        }

        public static List<double> RShift(List<double> func,int shift)
        {
            List<double> new_func = new List<double>();

            for(int i =0;i < func.Count + shift;i++)
            {
                if (i < shift)
                {
                    new_func.Add(func[0]);
                }
                else
                {
                    new_func.Add(func[i - shift]);
                }
            }

            return new_func;
        }
    }
}
