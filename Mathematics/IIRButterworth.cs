﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpticalHeartBeatAnalysis.Mathematics;
using OpticalHeartBeatAnalysis.Mathematics.Filtering;

namespace OpticalHeartBeatAnalysis.Mathematics
{

    public class IIRButterworth
    {
        private const double pi = 3.14156892;

        public struct Direct_FormII_Transposed_Filter
        {
            public double[] A; //Denominator
            public double[] B; //Numerator

            /// <summary>
            /// Zero-phase forward and reverse digital filtering
            /// </summary>
            /// <param name="input"></param>
            /// <returns></returns>
            public double[] Filter(double[] x)
            {
                int extra_dp = 1000 * A.Length;
                double[] new_x = new double[x.Length + 2 * extra_dp];
                double[] y = new double[new_x.Length];

                for (int i = 0; i < extra_dp; i++)
                {
                    double avg = 0;
                    for (int j = i; j < i + extra_dp; j++)
                    {
                        avg += x[j];
                    }
                    new_x[i] = avg / extra_dp;
                    new_x[new_x.Length - i - 1] = avg / extra_dp;
                    y[i] = avg / extra_dp;
                }
                for (int i = 0; i < x.Length; i++)
                {
                    new_x[i + extra_dp] = x[i];
                }

                //Forward

                for (int i = extra_dp; i < y.Length - extra_dp; i++)
                {
                    double val = B[0] * new_x[i];

                    for (int j = 1; j < A.Length; j++)
                    {
                        if (i - j < 0)
                            break;

                        val += B[j] * new_x[i - j];
                        val -= A[j] * y[i - j];
                    }

                    y[i] = val;
                }

                //Reverse output; re-filter
                new_x = y.Reverse<double>().ToArray();
                y = new double[new_x.Length];

                for (int i = extra_dp; i < y.Length - extra_dp; i++)
                {
                    double val = B[0] * new_x[i];

                    for (int j = 1; j < A.Length; j++)
                    {
                        if (i - j < 0)
                            break;

                        val += B[j] * new_x[i - j];
                        val -= A[j] * y[i - j];
                    }

                    y[i] = val;
                }

                y = y.Reverse<double>().ToArray();

                double[] ret = new double[x.Length];
                for (int i = 0; i < x.Length; i++)
                {
                    ret[i] = y[i + extra_dp];
                }

                //Remove excess datapoints
                double[] output = new double[x.Length];
                for (int i = extra_dp; i < output.Length + extra_dp; i++)
                {
                    output[i - extra_dp] = y[i];
                }

                return output;
            }
        }

        struct ZeroPoleGain
        {
            public List<Complex> z;
            public List<Complex> p;
            public double k;

            public void Initialize()
            {
                z = new List<Complex>();
                p = new List<Complex>();
                k = 1;
            }
        }

        struct StateSpace
        {
            public Matrix A;
            public Matrix B;
            public Matrix C;
            public Matrix D;

            public void Initialize()
            {
                A = new Matrix();
                B = new Matrix();
                C = new Matrix();
                D = new Matrix(1,1,new double[]{1});
            }

            public void Initialize(int n, List<Matrix> a_matrices)
            {
                B = new Matrix(n, 1); B[0, 0] = 1;
                C = new Matrix(1, n); C[0, n - 1] = 1;
                D = new Matrix();

                A = new Matrix(n, n);
                int r = 0;
                int c = 0;
                for (int k = 0; k < a_matrices.Count; k++)
                {
                    for (int i = 0; i < a_matrices[k].Columns; i++)
                    {
                        for (int j = 0; j < a_matrices[k].Rows; j++)
                        {
                            A[r + j, c + i] = a_matrices[k][j, i];
                        }
                    }

                    r += a_matrices[k].Rows;
                    c += a_matrices[k].Columns;

                    if (k < n / 2 - 1)
                    {
                        A[r, c - 1] = 1;
                    }
                }
            }

            public void Transform(FilterType type, double bw, double wn)
            {
                int n = A.Size(0);
                switch (type)
                {
                    case FilterType.Low:
                        A = wn * A;
                        B = wn * B;
                        break;
                    case FilterType.High:
                        Matrix newA = wn * ~A;
                        Matrix newB = -wn * (~A * B);
                        Matrix newC = C / A;
                        Matrix newD = -1 * C / A * B;

                        A = newA;
                        B = newB;
                        C = newC;
                        D = newD;
                        break;
                    case FilterType.Bandpass:
                        double q = wn / bw;
                        
                        Matrix new_A = Matrix.Augment((1/q) * A, n, n);
                        new_A = wn * new_A;
                        for (int i = n; i < n * 2; i++)
                        {
                            new_A[i, i - n] = -wn;
                            new_A[i - n, i] = wn;
                        }
                        A = new_A;

                        B = new Matrix(n * 2, 1); B[0, 0] = wn / q;
                        C = new Matrix(1, n * 2); C[0, n - 1] = 1;
                        break;
                    default:
                        break;
                }
            }

            public void Bilinear(double fs)
            {
                double t = 1 / fs;
                double r = Math.Sqrt(t);
                int n = A.Size(0);
                Matrix t1 = Matrix.Identity(n) + (0.5) * (t * A);
                Matrix t2 = Matrix.Identity(n) - (0.5) * (t * A);

                int i = 0;

                Matrix inv_t2 = ~t2;
                Matrix nA = inv_t2 * t1;
                Matrix nB = t / r * (inv_t2 * B);
                Matrix nC = r * C * inv_t2;
                Matrix nD = C * inv_t2 * B * t / 2 + D;

                A = nA;
                B = nB;
                C = nC;
                D = nD;
            }
        }

        public static Filter Butter(int n, double[] Wn, FilterType type)
        {
            #region Precondition Checks

            if (n > 100)
                throw new ButterworthException("Filter Order too large.");

            //if (Wn.Length != 2)
            //    throw new ButterworthException("Invalid poles.");

            #endregion

            double[] u = new double[2];
            for (int i = 0; i < Wn.Length; i++)
            {
                int fs = 2;
                u[i] = 2 * fs * Math.Tan(pi * Wn[i] / fs);
            }

            double Bw = 0;
            double center_freq = 0;
            switch (type)
            {
                case FilterType.Low:
                    center_freq = u[0];
                    break;
                case FilterType.High:
                    center_freq = u[0];
                    break;
                case FilterType.Bandpass:
                    Bw = u[1] - u[0];
                    center_freq = Math.Sqrt(u[1] * u[0]);
                    break;
                default:
                    break;
            }

            ZeroPoleGain zpk = buttap(n);
            StateSpace ss = zpk2ss(zpk);
            ss.Transform(type, Bw, center_freq);
            ss.Bilinear(2);
            Vector den = Polynomials.Poly(ss.A);
            Vector num = Polynomials.Poly(ss.A - ss.B * ss.C) + (ss.D - 1) * den;

            //Direct_FormII_Transposed_Filter filter = new Direct_FormII_Transposed_Filter();
            //filter.B = num;
            //filter.A = den;
            Filter filter = new Filter();
            filter.Denominator = den;
            filter.Numerator = num;
            return filter;
        }

        private static ZeroPoleGain buttap(int n)
        {
            ZeroPoleGain zpk = new ZeroPoleGain();
            zpk.Initialize();

            for (int i = 1; i <= n - 1; i += 2)
            {
                double a = 0;
                double b = pi * i / (2*n) + a;
                double e = Math.Exp(a);
                Complex c = new Complex((float)(-e*Math.Cos(b)), (float)(e*Math.Sin(b)));
                zpk.p.Add(c);
                zpk.p.Add(c.Conj());
            }

            if (n % 2 == 1)
            {

            }

            zpk.k = (double)prod(zpk.p).Real;

            return zpk;
        }

        private static StateSpace zpk2ss(ZeroPoleGain zpk)
        {
            //Strip Infinites

            //Calculate Lengths
            int np = zpk.p.Count;
            int nz = zpk.z.Count;
            
            /* Quick and Dirty Method */

            StateSpace ss = new StateSpace();

            List<Matrix> a_matrices = new List<Matrix>();
            int i = 0;
            while (i < np)
            {
                double[] den = Polynomials.Poly(zpk.p.GetRange(i, 2));

                //Skipping... doesn't seem necessary
                //wn = 1;

                Matrix t = Matrix.Identity(2);
                Matrix a1 = new Matrix(2, 2, new double[] { -den[1], -den[2], 1, 0 });
                Matrix inv_t = ~t;
                a1 = inv_t * a1 * t;
                a_matrices.Add(a1);
                //Matrix b1 = inv_t * new Matrix(2, 1, new double[] { 1, 0 });
                //Matrix c1 = new Matrix(1, 2, new double[] { 0, 1 }) * t;
                //double d1 = 0;

                //int ma1 = ss.A.Size(0);
                //int na2 = a1.Size(1);

                //ss.A = new Matrix(new Matrix[] { ss.A, Matrix.Zeros(ma1, na2), b1 * ss.C, a1 }, "xx;xx");
                //ss.B = new Matrix(new Matrix[] { ss.B, b1 * ss.D }, "x;x");
                //ss.C = new Matrix(new Matrix[] { d1 * c1, c1 }, "xx");
                //ss.D = d1 * ss.D;

                i += 2;
            }

            ss.Initialize(a_matrices.Count * 2, a_matrices);

            #region Long Method

            ////Switch Real/Imaj Parts
            //int i = 0;
            //for (i = 0; i < np; i++)
            //{
            //    float tmp = zpk.p[i].Imaginary;
            //    //zpk.p[i].Imaginary = zpk.p[i].Real;
            //    //zpk.p[i].Real = tmp;
            //}

            ////Initialize SS Matrices
            //StateSpace ss = new StateSpace();
            //ss.Initialize();

            ////Odd/Even Pole Pairs

            ////Iterate Zeros
            //i = 0;
            //while (i < nz)
            //{
            //    i++;
            //}

            ////Iterate Poles
            //while (i < np)
            //{
            //    double[] den = Polynomials.Poly(zpk.p.GetRange(i, 2));

            //    //Skipping... doesn't seem necessary
            //    //wn = 1;

            //    Matrix t = Matrix.Identity(2);
            //    Matrix a1 = new Matrix(2, 2, new double[] { -den[1], -den[2], 1, 0 });
            //    Matrix inv_t = ~t;
            //    a1 = inv_t * a1 * t;
            //    Matrix b1 = inv_t * new Matrix(2, 1, new double[] { 1, 0 });
            //    Matrix c1 = new Matrix(1, 2, new double[] { 0, 1 }) * t;
            //    double d1 = 0;

            //    int ma1 = ss.A.Size(0);
            //    int na2 = a1.Size(1);

            //    ss.A = new Matrix(new Matrix[] { ss.A, Matrix.Zeros(ma1, na2), b1 * ss.C, a1 }, "xx;xx");
            //    ss.B = new Matrix(new Matrix[] { ss.B, b1 * ss.D }, "x;x");
            //    ss.C = new Matrix(new Matrix[] { d1 * c1, c1 }, "xx");
            //    ss.D = d1 * ss.D;

            //    i += 2;
            //}

            #endregion

            return ss;
        }

        private static Complex prod(List<Complex> list)
        {
            Complex product = new Complex(1.0f, 0.0f);
            foreach (Complex c in list)
            {
                product *= c;
            }

            return product;
        }

        //private static double[] poly(List<Complex> list)
        //{
        //    double[] polynomial = new double[list.Count + 1];

        //    for (int i = 0; i < polynomial.Length; i++)
        //    {
        //        int reals = list.Count - i;

        //        double prod = 1;

        //        for (int a = 0; a < reals; a++)
        //        {
        //            prod *= (double)list[a].Real;
        //        }
        //        for (int b = reals; b < list.Count; b++)
        //        {
        //            prod *= (double)list[b].Imaginary;
        //        }

        //        polynomial[i] = prod;
        //    }

        //    return polynomial;
        //}

        private static Direct_FormII_Transposed_Filter bilinear(double[] polynomial)
        {
            Direct_FormII_Transposed_Filter filter = new Direct_FormII_Transposed_Filter();

            //Calculate Numerator
            filter.A = Binomials.BinomialExpansion(polynomial.Length);

            //Calculate Denominator
            double[] z_minus = new double[]{1,-1};
            double[] z_plus = new double[]{1,1};
            int n = polynomial.Length;
            double[] den = Polynomials.Scale(polynomial[0],Binomials.BinomialPower(z_plus,n));

            for(int i = 1; i < n;i++)
            {
                double[] p1 = Binomials.BinomialPower(z_minus, i);
                double[] p2 = Binomials.BinomialPower(z_plus, n - i);
                double[] P = Polynomials.PolynomialMultiplication(p1, p2);
                P = Polynomials.Scale(polynomial[i], P);

                for (int j = 0; j < den.Length; j++)
                {
                    den[j] += P[j];
                }
            }
            filter.B = den;

            return filter;
        }
    }

    [global::System.Serializable]
    public class ButterworthException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public ButterworthException() { }
        public ButterworthException(string message) : base(message) { }
        public ButterworthException(string message, Exception inner) : base(message, inner) { }
        protected ButterworthException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
