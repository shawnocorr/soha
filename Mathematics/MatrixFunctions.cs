﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpticalHeartBeatAnalysis
{
    public class MatrixFunctions
    {
        public static int Maximum(int[,] inMatrix)
        {
            if (inMatrix == null)
                return 0;

            int hi = inMatrix[0, 0];

            foreach (int i in inMatrix)
            {
                if (i > hi)
                    hi = i;
            }

            return hi;
        }

        public static int Minimum(int[,] inMatrix)
        {
            if (inMatrix == null)
                return 0;

            int lo = inMatrix[0, 0];
            foreach (int i in inMatrix)
            {
                if (i < lo)
                    lo = i;
            }

            return lo;
        }

        public static double[] MaxMin(double[,] m_values)
        {
            if (m_values == null)
                return new double[] { 0, 0 };

            double hi=0,lo=0;
            foreach(double i in m_values)
            {
                if (i < lo)
                    lo = i;

                if (i > hi)
                    hi = i;
            }

            return new double[] { hi, lo };
        }

        public static double Maximum(double[,] inMatrix)
        {
            if (inMatrix == null)
                return 0;

            double hi = inMatrix[0, 0];

            foreach (double i in inMatrix)
            {
                if (i > hi)
                    hi = i;
            }

            return hi;
        }

        public static double Minimum(double[,] inMatrix)
        {
            if (inMatrix == null)
                return 0;

            double lo = inMatrix[0, 0];

            foreach (double i in inMatrix)
            {
                if (i < lo)
                    lo = i;
            }

            return lo;
        }

        public static double[,] Normalize(double[,] inMatrix)
        {
            double max = Maximum(inMatrix);
            double min = Minimum(inMatrix);

            for (int i = 0; i < inMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < inMatrix.GetLength(1); j++)
                {
                    inMatrix[i, j] -= min;
                    inMatrix[i, j] /= (max - min);
                }
            }
            
            return inMatrix;
        }

        public static double[,] Denormalize(double[,] inMatrix)
        {
            for (int i = 0; i < inMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < inMatrix.GetLength(1); j++)
                {
                    inMatrix[i, j] *= 255;
                }
            }

            return inMatrix;
        }

        public static int[,] To8Bit(double[,] inMatrix)
        {
            int[,] ret = new int[inMatrix.GetLength(0), inMatrix.GetLength(1)];

            for (int i = 0; i < ret.GetLength(0); i++)
            {
                for (int j = 0; j < ret.GetLength(1); j++)
                {
                    ret[i, j] = (int)inMatrix[i, j];
                }
            }

            return ret;
        }

        public static double[,] Convolute(double[,] f1, double[,] f2)
        {
            int center_x = f2.GetLength(0)/2;
            int center_y = f2.GetLength(1)/2;

            double[,] ret = new double[f1.GetLength(0), f1.GetLength(1)];

            for (int i = 0; i < f1.GetLength(0); i++)
            {
                for (int j = 0; j < f1.GetLength(1); j++)
                {
                    double sum = 0;

                    for (int x = 0; x < f2.GetLength(0); x++)
                    {
                        int xx = f2.GetLength(0) - 1 - x;

                        for (int y = 0; y < f2.GetLength(1); y++)
                        {
                            int yy = f2.GetLength(1) - 1 - y;

                            int ii = i + (x - center_x);
                            int jj = j + (y - center_y);

                            if (ii >= 0 && ii < f1.GetLength(0) && jj >= 0 && jj < f1.GetLength(1))
                            {
                                sum += f1[ii, jj] * f2[xx, yy];
                            }
                        }
                    }

                    ret[i, j] = sum;
                }
            }

            return ret;
        }

        //public static object[] CreateFilter(float fp, float fb, float Ap, float As, float Fs)
        //{
        //    //Normalize Frequencies
        //    float Wp = (double)(2 * Math.PI * fp / Fs);
        //    float Ws = 2 * Math.PI * fb / Fs;

        //    //Analog to Digital
        //    float T = 2;
        //    float Op = (2 / T) * Math.Atan(Wp / 2);
        //    float Os = (2 / T) * Math.Atan(Ws / 2);

        //    //Calculate Filter Length
        //    int N = (Math.Log10(Math.Pow(10, (double)(0.1 * Ap)) - 1) - (Math.Log10(Math.Pow(10, (double)(0.1 * Ap)) - 1))) / (2 * Math.Log10(Op / Os)) + 1;

        //    //Calculate Cutoff Frequencies
        //    float Oc = Op / Math.Pow(Math.Pow(10, (double)(0.1 * Ap)) - 1, (double)(1 / (2 * N)));

        //    float[] p = new float[2 * N];
        //    for (int i = 0; i < p.Length; i++)
        //    {
        //        //double s = Math.Exp(
        //    }
        //     return new object[3]();
        //}

        #region Simple Arithmetic

        public static double[,] Add(double[,] A, double[,] B)
        {
            if (A.GetLength(0) != B.GetLength(0) && A.GetLength(1) != B.GetLength(1))
                throw new ArgumentException("Matrix Dimensions must match");

            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    A[i, j] = A[i, j] + B[i, j];
                }
            }

            return A;
        }

        public static double[,] Divide(double[,] A, double[,] B)
        {
            if (A.GetLength(0) != B.GetLength(0) && A.GetLength(1) != B.GetLength(1))
                throw new ArgumentException("Matrix Dimensions must match");

            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    A[i, j] = A[i, j] / (B[i, j]+1e-6);
                }
            }

            return A;
        }

        public static double[,] Pow(double[,] A, double exp)
        {
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    A[i, j] = Math.Pow(A[i, j],exp);
                }
            }

            return A;
        }

        public static double[,] Sqrt(double[,] A)
        {
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    A[i, j] = Math.Sqrt(A[i, j]);
                }
            }

            return A;
        }

        public static double[,] Atan(double[,] A)
        {
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    A[i, j] = Math.Atan(A[i, j]);
                }
            }

            return A;
        }

        #endregion
    }
}
