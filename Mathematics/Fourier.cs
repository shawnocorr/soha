﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpticalHeartBeatAnalysis.Mathematics
{
    public class Fourier
    {

        public struct ImageFFT
        {
            Matrix[] m_matrix;
            double m_drs;
            public int m_original_rows;
            public int m_original_columns;

            public Matrix Magnitude
            {
                get
                {
                    return m_matrix[0];
                }
            }
            public Matrix Phase
            {
                get
                {
                    return m_matrix[1];
                }
            }
            public double DRS
            {
                get
                {
                    return m_drs;
                }
            }
            public Matrix this[int index]
            {
                get
                {
                    return m_matrix[index];
                }
                set
                {
                    m_matrix[index] = value;
                }
            }

            public ImageFFT(Matrix[] M, double drs, Matrix Original)
            {
                m_matrix = M;
                m_drs = drs;
                m_original_columns = Original.Columns;
                m_original_rows = Original.Rows;
            }
        }

        public struct FunctionFFT
        {
            double[] M;
            double[] P;

            public double[] Magnitude
            {
                get { return M; }
                set { M = value; }
            }
            public double[] Phase
            {
                get { return P; }
                set { P = value; }
            }

            public double[] GetReal()
            {
                double[] a = new double[M.Length];

                for (int i = 0; i < M.Length; i++)
                {
                    a[i] = M[i] * Math.Cos(P[i]);
                }

                return a;
            }

            public double[] GetImaj()
            {
                double[] a = new double[M.Length];

                for (int i = 0; i < M.Length; i++)
                {
                    a[i] = M[i] * Math.Sin(P[i]);
                }

                return a;
            }

            public FunctionFFT(double[] x, double[] y)
            {
                M = new double[x.Length];
                P = new double[y.Length];

                for (int i = 0; i < x.Length; i++)
                {
                    M[i] = Math.Sqrt(x[i] * x[i] + y[i] * y[i]);
                    P[i] = Math.Atan2(y[i], x[i]);
                }
            }
        }

        public static ImageFFT DFFT(Matrix M)
        {
            #region Ensure 2^N Sizing

            int n = NextPower2(M.Columns);
            int m = NextPower2(M.Rows);

            #endregion

            #region Create Complex Matrix

            Complex[,] d = new Complex[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (i < M.Columns && j < M.Rows)
                    {
                        d[i, j] = new Complex(M[j, i], 0);
                    }
                    else
                    {
                        d[i, j] = new Complex(0, 0);
                    }
                }
            }

            #endregion

            Complex[,] dfft = FFT2D(d, n, m, 1);

            #region Create Magnitude Matrix

            Matrix Y = new Matrix(n, m);
            Matrix Z = new Matrix(n, m);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Y[i, j] = (double)dfft[i, j].Magnitude;
                    Z[i, j] = (double)dfft[i, j].Phase;
                }
            }

            #endregion

            #region Dynamic Range Suppression

            double[] max_min = MatrixFunctions.MaxMin(Y);
            double c = 255.0 / (1 + Math.Log10(max_min[0]));

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    //if (Y[i, j] > 100)
                    //    Y[i, j] = 100;

                    //f[i, j] = Math.Log(1 + f[i, j],100);
                    Y[i, j] = c*Math.Log10(1 + Y[i, j]);
                }
            }

            #endregion

            Y = ShiftQuadrants(Y);
            Z = ShiftQuadrants(Z);

            ImageFFT fft = new ImageFFT(new Matrix[] { Y, Z }, c, M);

            return fft;
        }

        public static Matrix IDFFT(ImageFFT F)
        {
            #region Ensure 2^N Sizing

            int n = F[0].Columns;
            int m = F[1].Rows;

            #endregion

            F[0] = ShiftQuadrants(F[0]);
            F[1] = ShiftQuadrants(F[1]);

            #region Create Complex Matrix

            Complex[,] d = new Complex[n, m];

            #region Invert Dynamic Range Suppression

            //double[] max_min = MatrixFunctions.MaxMin(Y);
            //double c = 255.0 / (1 + Math.Log10(max_min[0]));

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    F[0][j, i] = Math.Pow(10, (F[0][j, i] / F.DRS)) - 1;

                    //if (Y[i, j] > 100)
                    //    Y[i, j] = 100;

                    //f[i, j] = Math.Log(1 + f[i, j],100);
                    //Y[i, j] = Math.Log10(1 + Y[i, j]);
                }
            }

            #endregion

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    double A = F[0][j, i] * Math.Cos(F[1][j, i]);
                    double B = F[0][j, i] * Math.Sin(F[1][j, i]);
                    d[i,j] = new Complex(A,B);
                    //if (i < M.Columns && j < M.Rows)
                    //{
                    //    d[i, j] = new Complex(M[j, i], 0);
                    //}
                    //else
                    //{
                    //    d[i, j] = new Complex(0, 0);
                    //}
                }
            }

            #endregion

            Complex[,] dfft = FFT2D(d, n, m, -1);

            #region Create Magnitude Matrix

            Matrix Y = new Matrix(n, m);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Y[i, j] = (double)dfft[i, j].Real;
                }
            }

            #endregion

            Matrix M = new Matrix(F.m_original_rows, F.m_original_columns);
            for (int i = 0; i < M.Rows; i++)
            {
                for (int j = 0; j < M.Columns; j++)
                {
                    M[i, j] = Y[i, j];
                }
            }

            //Y = ShiftQuadrants(Y);

            return M;
        }

        public static FunctionFFT DFFT(double[] f)
        {
            #region Ensure 2^N Sizing

            int n = NextPower2(f.Count());

            #endregion

            #region Create Complex Matrix

            double[] x = new double[n];
            double[] y = new double[n];

            for (int i = 0; i < n; i++)
            {
                x[i] = i < f.Count() ? f[i] : 0;
                y[i] = 0;
            }

            #endregion

            int m = (int)Math.Log((double)n, 2);

            FFT1D(1, m, ref x, ref y);

            x = ShiftQuadrants(x);
            y = ShiftQuadrants(y);

            FunctionFFT fft = new FunctionFFT(x, y);

            return fft;
        }

        public static double[] IDFFT(FunctionFFT fft)
        {
            double[] x = fft.GetReal();
            double[] y = fft.GetImaj();

            x = ShiftQuadrants(x);
            y = ShiftQuadrants(y);

            int m = (int)Math.Log((double)x.Length, 2);

            FFT1D(-1, m, ref x, ref y);

            return x;
        }

        /// <summary>
        /// Shifts Quadrants of the 2D Fourier to proper orientation
        /// </summary>
        /// <param name="forward">True: to 2D-FT; False: from 2D-FT </param>
        /// <returns>Matrix</returns>
        private static Matrix ShiftQuadrants(Matrix M)
        {
            Matrix Y = M.CloneEmpty();
            int col_2 = Y.Columns / 2;
            int row_2 = Y.Rows / 2;

            //Quadrant I -> IV
            for (int i = 0; i < col_2; i++)
            {
                for (int j = 0; j < row_2; j++)
                {
                    Y[row_2 + j, col_2 + i] = M[j, i];
                }
            }

            //Quadrant II -> III
            for (int i = col_2; i < M.Columns; i++)
            {
                for (int j = 0; j < row_2; j++)
                {
                    Y[row_2 + j, i - col_2] = M[j, i];
                }
            }

            //Quadrant III -> II
            for (int i = 0; i < col_2; i++)
            {
                for (int j = row_2; j < M.Rows; j++)
                {
                    Y[j - row_2, col_2 + i] = M[j, i];
                }
            }

            //Quadrant IV -> I
            for (int i = col_2; i < M.Columns; i++)
            {
                for (int j = row_2; j < M.Rows; j++)
                {
                    Y[j - row_2, i - col_2] = M[j, i];
                }
            }

            return Y;
        }

        private static double[] ShiftQuadrants(double[] f)
        {
            double[] y = new double[f.Length];

            int di = f.Length / 2;

            y[di] = f[0];

            for (int i = 1; i < di; i++)
            {
                y[di + i] = f[i];
                y[di - i] = f[f.Length - i];
            }

            return y;
        }

        private static double[] EliminateNegatives(double[] f)
        {
            int di = f.Length / 2;
            double[] y = new double[di];

            for (int i = 0; i < di; i++)
            {
                y[i] = f[di + i];
            }

            return y;

        }

        private static int NextPower2(int x)
        {
            return (int)Math.Pow(2,(double)Math.Ceiling(Math.Log(x, 2)));
            //int i = 0;

            //while (true)
            //{
            //    if (Math.Pow(2, i) >= x)
            //    {
            //        return i;
            //    }
            //    i++;
            //}
        }

        /*-------------------------------------------------------------------------
            Perform a 2D FFT inplace given a complex 2D array
            The direction dir, 1 for forward, -1 for reverse
            The size of the array (nx,ny)
            Return false if there are memory problems or
            the dimensions are not powers of 2
        */
        public static Complex[,] FFT2D(Complex[,] c, int nx, int ny, int dir)
        {
            int i, j;
            int m;//Power of 2 for current number of points
            double[] real;
            double[] imag;
            Complex[,] output;//=new COMPLEX [nx,ny];
            output = c; // Copying Array
            // Transform the Rows 
            real = new double[nx];
            imag = new double[nx];

            for (j = 0; j < ny; j++)
            {
                for (i = 0; i < nx; i++)
                {
                    real[i] = c[i, j].Real;
                    imag[i] = c[i, j].Imaginary;
                }


                // Calling 1D FFT Function for Rows
                m = (int)Math.Log((double)nx, 2);//Finding power of 2 for current number of points e.g. for nx=512 m=9
                FFT1D(dir, m, ref real, ref imag);

                for (i = 0; i < nx; i++)
                {
                    //  c[i,j].real = real[i];
                    //  c[i,j].imag = imag[i];
                    output[i, j].Real = real[i];
                    output[i, j].Imaginary = imag[i];
                }
            }

            // Transform the columns  
            real = new double[ny];
            imag = new double[ny];

            for (i = 0; i < nx; i++)
            {
                for (j = 0; j < ny; j++)
                {
                    //real[j] = c[i,j].real;
                    //imag[j] = c[i,j].imag;
                    real[j] = output[i, j].Real;
                    imag[j] = output[i, j].Imaginary;
                }
                // Calling 1D FFT Function for Columns
                m = (int)Math.Log((double)ny, 2);//Finding power of 2 for current number of points e.g. for nx=512 m=9
                FFT1D(dir, m, ref real, ref imag);
                for (j = 0; j < ny; j++)
                {
                    //c[i,j].real = real[j];
                    //c[i,j].imag = imag[j];
                    output[i, j].Real = real[j];
                    output[i, j].Imaginary = imag[j];
                }
            }

            // return(true);
            return (output);
        }

        /*-------------------------------------------------------------------------
            This computes an in-place complex-to-complex FFT
            x and y are the real and imaginary arrays of 2^m points.
            dir = 1 gives forward transform
            dir = -1 gives reverse transform
            Formula: forward
                     N-1
                      ---
                    1 \         - j k 2 pi n / N
            X(K) = --- > x(n) e                  = Forward transform
                    N /                            n=0..N-1
                      ---
                     n=0
            Formula: reverse
                     N-1
                     ---
                     \          j k 2 pi n / N
            X(n) =    > x(k) e                  = Inverse transform
                     /                             k=0..N-1
                     ---
                     k=0
            */
        public static void FFT1D(int dir, int m, ref double[] x, ref double[] y)
        {
            long nn, i, i1, j, k, i2, l, l1, l2;
            double c1, c2, tx, ty, t1, t2, u1, u2, z;
            /* Calculate the number of points */
            nn = 1;
            for (i = 0; i < m; i++)
                nn *= 2;
            /* Do the bit reversal */
            i2 = nn >> 1;
            j = 0;
            for (i = 0; i < nn - 1; i++)
            {
                if (i < j)
                {
                    tx = x[i];
                    ty = y[i];
                    x[i] = x[j];
                    y[i] = y[j];
                    x[j] = tx;
                    y[j] = ty;
                }
                k = i2;
                while (k <= j)
                {
                    j -= k;
                    k >>= 1;
                }
                j += k;
            }
            /* Compute the FFT */
            c1 = -1.0;
            c2 = 0.0;
            l2 = 1;
            for (l = 0; l < m; l++)
            {
                l1 = l2;
                l2 <<= 1;
                u1 = 1.0;
                u2 = 0.0;
                for (j = 0; j < l1; j++)
                {
                    for (i = j; i < nn; i += l2)
                    {
                        i1 = i + l1;
                        t1 = u1 * x[i1] - u2 * y[i1];
                        t2 = u1 * y[i1] + u2 * x[i1];
                        x[i1] = x[i] - t1;
                        y[i1] = y[i] - t2;
                        x[i] += t1;
                        y[i] += t2;
                    }
                    z = u1 * c1 - u2 * c2;
                    u2 = u1 * c2 + u2 * c1;
                    u1 = z;
                }
                c2 = Math.Sqrt((1.0 - c1) / 2.0);
                if (dir == 1)
                    c2 = -c2;
                c1 = Math.Sqrt((1.0 + c1) / 2.0);
            }
            /* Scaling for forward transform */
            if (dir == 1)
            {
                for (i = 0; i < nn; i++)
                {
                    x[i] /= (double)nn;
                    y[i] /= (double)nn;

                }
            }



            //  return(true) ;
            return;
        }
    }
}
