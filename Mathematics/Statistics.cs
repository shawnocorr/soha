﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mathematics
{
    public class Statistics
    {
        public static double Mean(double[] f)
        {
            double sum = 0;
            for (int i = 0; i < f.Length; i++)
                sum += f[i];

            return sum / f.Length;
        }

        public static double Median(double[] f)
        {
            var sorted = f.OrderBy(x => x);

            if (f.Length % 2 == 0)
            {
                double sum = sorted.ElementAt<double>(f.Length / 2) + sorted.ElementAt<double>(f.Length / 2 + 1);
                sum /= 2;
                return sum;
            }
            else
            {
                return sorted.ElementAt<double>(f.Length / 2);
            }
        }

        //public static double Mode(double[] f)
        //{

        //}

        public static double Variance(double[] f)
        {
            double u = Mean(f);
            double sum = 0;
            for (int i = 0; i < f.Length; i++)
                sum += Math.Pow(f[i] - u, 2);

            return sum / f.Length;

        }

        public static double StdDev(double[] f)
        {
            return Math.Sqrt(Variance(f));
        }
    }
}
