﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseObjects;
using System.Xml.Serialization;

namespace XMLInteropDLL
{
    public class PhasePhaseData : PhaseData, IXmlSerializable
    {
        private IntervalList m_intervals;

        public IntervalList Intervals
        {
            get
            {
                return m_intervals;
            }
            set
            {
                m_intervals = value;
            }
        }

        public PhasePhaseData()
            : base()
        {

        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            while (reader.Read())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(IntervalList));
                m_intervals = (IntervalList)serializer.Deserialize(reader);

                reader.ReadEndElement();
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(IntervalList));
            serializer.Serialize(writer, m_intervals);
        }
    }
}
