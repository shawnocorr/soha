﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XMLInteropDLL
{
    [Serializable]
    public class XmlDeserializationException : Exception
    {
        public XmlDeserializationException() { }
        public XmlDeserializationException(string message) : base(message) { }
        public XmlDeserializationException(string message, Exception inner) : base(message, inner) { }
        protected XmlDeserializationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
