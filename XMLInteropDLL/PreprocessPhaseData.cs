﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BaseObjects;

namespace XMLInteropDLL
{
    public class PreprocessPhaseData : PhaseData, IXmlSerializable
    {
        private List<double> m_bright = new List<double>();
        private List<double> m_intensity = new List<double>();
        private MMode m_mmode;

        public List<double> Brightness
        {
            get
            {
                return m_bright;
            }
            set
            {
                m_bright = value;
            }
        }
        public List<double> Intensity
        {
            get
            {
                return m_intensity;
            }
            set
            {
                m_intensity = value;
            }
        }
        public MMode MMode
        {
            get
            {
                return m_mmode;
            }
            set
            {
                m_mmode = value;
            }
        }

        public PreprocessPhaseData()
            : base()
        {
            m_mmode = new MMode();
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            while (reader.Read())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<double>));
                m_bright = (List<double>)serializer.Deserialize(reader);
                m_intensity = (List<double>)serializer.Deserialize(reader);

                serializer = new XmlSerializer(typeof(MMode));
                m_mmode = (MMode)serializer.Deserialize(reader);

                reader.ReadEndElement();
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<double>));
                serializer.Serialize(writer, m_bright);
                serializer.Serialize(writer, m_intensity);

                serializer = new XmlSerializer(typeof(MMode));
                serializer.Serialize(writer, m_mmode);
            }
            catch (Exception exc)
            {

            }
        }
    }
}
