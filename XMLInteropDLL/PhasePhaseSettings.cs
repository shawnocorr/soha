﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XMLInteropDLL
{
    public class PhasePhaseSettings : PhaseSettings//, IXmlSerializable
    {
        public double LoCutoff { get; set; }
        public double HiCutoff { get; set; }
        public int Offset { get; set; }
        public int Zoom { get; set; }
        public bool UseThreshold { get; set; }
        public int Threshold{ get; set; }

        public PhasePhaseSettings()
            : base()
        {
            Offset = 4;
            LoCutoff = 0.0;
            HiCutoff = 100.0;
            Zoom = 2;
            UseThreshold = false;
            Threshold = 80;
        }

        //public System.Xml.Schema.XmlSchema GetSchema()
        //{
        //    return null;
        //}

        //public void ReadXml(System.Xml.XmlReader reader)
        //{
        //    try
        //    {
        //        reader.Read();

        //        LoCutoff = double.Parse(reader.ReadElementContentAsString());
        //        HiCutoff = double.Parse(reader.ReadElementContentAsString());
        //        Offset = int.Parse(reader.ReadElementContentAsString());

        //        if (reader.Name == "Zoom")
        //            Zoom = int.Parse(reader.ReadElementContentAsString());

        //        reader.ReadEndElement();
        //    }
        //    catch (Exception exc)
        //    {

        //    }
        //}

        //public void WriteXml(System.Xml.XmlWriter writer)
        //{
        //    try
        //    {
        //        writer.WriteElementString("LoCutoff", LoCutoff.ToString());
        //        writer.WriteElementString("HiCutoff", HiCutoff.ToString());
        //        writer.WriteElementString("Offset", Offset.ToString());
        //        writer.WriteElementString("Zoom", Zoom.ToString());
        //        writer.WriteElementString("UseThreshold", UseThreshold.ToString());
        //        writer.WriteElementString("Threshold", Threshold.ToString());
        //    }
        //    catch (Exception exc)
        //    {

        //    }
        //}
    }
}
