﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XMLInteropDLL
{

    public enum PhaseStatus
    {
        NA = 0,
        OK,
        Reject
    }

    public enum PhaseName
    {
        Mark = 1,
        Preprocess,
        Interval,
        Phase
    }

    public class PhaseOutput : IXmlSerializable
    {
        protected PhaseStatus m_phase_status;
        protected PhaseName m_phase_name;
        protected List<PhaseData> m_phase_data = new List<PhaseData>();
        protected List<PhaseSettings> m_phase_settings = new List<PhaseSettings>();

        public PhaseStatus Status
        {
            get { return m_phase_status; }
            set { m_phase_status = value; }
        }
        public PhaseName PhaseName
        {
            get { return m_phase_name; }
            set { m_phase_name = value; }
        }
        public List<PhaseData> Data
        {
            get { return m_phase_data; }
        }
        public List<PhaseSettings> Settings
        {
            get { return m_phase_settings; }
        }

        public PhaseOutput()
        {
            m_phase_status = PhaseStatus.NA;
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            string status = reader["Status"];
            m_phase_status = (PhaseStatus)Enum.Parse(typeof(PhaseStatus), status);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("Status", m_phase_status.ToString());
        }
    }
}
