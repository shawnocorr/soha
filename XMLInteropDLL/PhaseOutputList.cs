﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XMLInteropDLL
{
    [XmlRoot("PhaseOutputList")]

    [XmlInclude(typeof(MarkPhaseOutput))]
    [XmlInclude(typeof(PreprocessPhaseOutput))]
    [XmlInclude(typeof(IntervalPhaseOutput))]
    [XmlInclude(typeof(PhasePhaseOutput))]
    public class PhaseOutputList : IXmlSerializable
    {

        private List<PhaseOutput> m_phases = new List<PhaseOutput>();

        [XmlElement("PhaseList")]
        public List<PhaseOutput> Phases
        {
            get
            {
                return m_phases;
            }
            set
            {
                m_phases = value;
            }
        }

        public PhaseOutput this[int index]
        {
            get
            {
                return m_phases[index];
            }
            set
            {
                m_phases[index] = value;
            }
        }

        public PhaseOutputList()
        {
            m_phases = new List<PhaseOutput>();
            m_phases.Add(new MarkPhaseOutput());
            m_phases.Add(new PreprocessPhaseOutput());
            m_phases.Add(new IntervalPhaseOutput());
            m_phases.Add(new PhasePhaseOutput());

            m_phases[0].PhaseName = PhaseName.Mark;
            m_phases[1].PhaseName = PhaseName.Preprocess;
            m_phases[2].PhaseName = PhaseName.Interval;
            m_phases[3].PhaseName = PhaseName.Phase;
        }

        public IEnumerator<PhaseOutput> GetEnumerator()
        {
            return m_phases.GetEnumerator();
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            try
            {
                while (reader.Read())
                {
                    Type t = Type.GetType("XMLInteropDLL." + reader.Name);
                    XmlSerializer serializer = new XmlSerializer(t);
                    object o = serializer.Deserialize(reader.ReadSubtree());
                    Type t1 = o.GetType();

                    if (o is MarkPhaseOutput)
                    {
                        m_phases[0] = o as MarkPhaseOutput;
                    }
                    else if (o is PreprocessPhaseOutput)
                    {
                        m_phases[1] = o as PreprocessPhaseOutput;
                    }
                    else if (o is IntervalPhaseOutput)
                    {
                        m_phases[2] = o as IntervalPhaseOutput;
                    }
                    else if (o is PhasePhaseOutput)
                    {
                        m_phases[3] = o as PhasePhaseOutput;
                    }

                    //reader.ReadEndElement();
                }
            }
            catch (Exception exc)
            {

            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            foreach (PhaseOutput phase_out in m_phases)
            {
                XmlSerializer serializer = new XmlSerializer(phase_out.GetType());
                serializer.Serialize(writer, phase_out);
            }
        }
    }
}
