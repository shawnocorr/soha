﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XMLInteropDLL
{
    [XmlInclude(typeof(PhasePhaseOutput))]
    public class PhasePhaseOutput : PhaseOutput, IXmlSerializable
    {

        public PhasePhaseOutput()
        {
            Data.Add(new PhasePhaseData());
            Settings.Add(new PhasePhaseSettings());
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            try
            {
                base.ReadXml(reader);
                Data.Clear();
                Settings.Clear();

                while (reader.Read())
                {
                    Type t = Type.GetType("XMLInteropDLL." + reader.Name);
                    XmlSerializer serializer = new XmlSerializer(t);
                    object o = serializer.Deserialize(reader.ReadSubtree());

                    if (o is PhaseData)
                    {
                        Data.Add(o as PhaseData);
                    }
                    else if (o is PhaseSettings)
                    {
                        Settings.Add(o as PhaseSettings);
                    }

                    //reader.ReadEndElement();
                }
            }
            catch (Exception exc)
            {

            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            base.WriteXml(writer);

            foreach (PhaseData data in m_phase_data)
            {
                XmlSerializer serializer = new XmlSerializer(data.GetType());
                serializer.Serialize(writer, data);
            }

            foreach (PhaseSettings settings in m_phase_settings)
            {
                XmlSerializer serializer = new XmlSerializer(settings.GetType());
                serializer.Serialize(writer, settings);
            }
        }
    }
}
