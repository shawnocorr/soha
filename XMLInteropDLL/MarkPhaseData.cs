﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;
using BaseObjects;

namespace XMLInteropDLL
{
    [XmlRoot("MarkPhaseData")]
    public class MarkPhaseData : PhaseData
    {
        private BindingList<MarkPair> m_marks = new BindingList<MarkPair>();
        private BindingList<Roi> m_rois = new BindingList<Roi>();

        public BindingList<MarkPair> Marks
        {
            get
            {
                return m_marks;
            }
            set
            {
                m_marks = value;
            }
        }
        public BindingList<Roi> Rois
        {
            get
            {
                return m_rois;
            }
            set
            {
                m_rois = value;
            }
        }

        public MarkPhaseData()
            : base()
        {

        }
    }
}
