﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Threading;

namespace XMLInteropDLL
{
    public class XmlUpdate
    {
        public static void Update(string path, int fromVersion, int toVersion)
        {
            
            // 1 -> 2
            for (int i = fromVersion; i < toVersion; i++)
            {
                switch (i)
                {
                    case 1:
                        XmlDocument doc = new XmlDocument();
                        doc.Load(path);
                        
                        //XmlElement root = doc.DocumentElement;

                        //// Update Filegram Element
                        ////XmlNode filegram = root.ChildNodes[0];
                        ////root.RemoveAttribute("Filepath");

                        //XmlAttribute version = doc.CreateAttribute("Version"); version.Value = "2";
                        //XmlAttribute status = doc.CreateAttribute("Status"); status.Value = "Idle";

                        //root.Attributes.Append(version);
                        //root.Attributes.Append(status);

                        ////Update VideoInterop
                        ////XmlElement video_interop = root.ChildNodes[1] as XmlElement;
                        ////video_interop.RemoveAttribute("Name");
                                                
                        //XmlElement output_list = root.ChildNodes[2] as XmlElement;

                        ////Update Preprocess
                        //XmlElement preprocess_output = output_list.ChildNodes[1] as XmlElement;
                        //foreach (XmlElement data in preprocess_output.ChildNodes)
                        //{
                        //    //data.RemoveChild(data.ChildNodes[2]);
                        //}

                        ////Update Interval
                        //XmlElement interval_output = output_list.ChildNodes[2] as XmlElement;
                        //foreach (XmlElement data in interval_output.ChildNodes)
                        //{
                        //    if (data.Name == "IntervalPhaseSettings")
                        //    {
                        //        //data.RemoveChild(data.ChildNodes[2]);
                        //        //data.RemoveChild(data.ChildNodes[5]);
                        //        //data.RemoveChild(data.ChildNodes[7]);
                        //        //data.RemoveChild(data.ChildNodes[7]);

                        //        XmlElement bright_lo_cutoff = doc.CreateElement("BrightLoCutoff"); bright_lo_cutoff.InnerText = "0";
                        //        XmlElement bright_hi_cutoff = doc.CreateElement("BrightHiCutoff"); bright_lo_cutoff.InnerText = "100";
                        //        XmlElement intensity_lo_cutoff = doc.CreateElement("IntensityLoCutoff"); bright_lo_cutoff.InnerText = "0";
                        //        XmlElement intensity_hi_cutoff = doc.CreateElement("IntensityHiCutoff"); bright_lo_cutoff.InnerText = "100";
                        //        XmlElement invert = doc.CreateElement("Invert"); bright_lo_cutoff.InnerText = "false";
                        //        data.AppendChild(bright_lo_cutoff);
                        //        data.AppendChild(bright_hi_cutoff);
                        //        data.AppendChild(intensity_lo_cutoff);
                        //        data.AppendChild(intensity_hi_cutoff);
                        //        data.AppendChild(invert);
                        //    }
                        //}

                        ////Update Phase
                        //XmlElement phase_output = output_list.ChildNodes[3] as XmlElement;
                        //foreach (XmlElement data in phase_output.ChildNodes)
                        //{
                        //    if (data.Name == "PhasePhaseSettings")
                        //    {
                        //        //data.RemoveChild(data.ChildNodes[0]);

                        //        XmlElement lo_cutoff = doc.CreateElement("LoCutoff"); lo_cutoff.InnerText = "0";
                        //        XmlElement hi_cutoff = doc.CreateElement("HiCutoff"); hi_cutoff.InnerText = "100";
                        //        data.AppendChild(lo_cutoff);
                        //        data.AppendChild(hi_cutoff);
                        //    }
                        //}

                        string new_name = path.Insert(path.Length - 4, "A");
                        doc.Save(new_name);

                        using (StreamReader reader = new StreamReader(new_name))
                        using (StreamWriter writer = new StreamWriter(path, false))
                        {

                            string all_text = reader.ReadToEnd();
                            all_text = all_text.Replace("VideoInterop", "Video");
                            all_text = all_text.Replace("ProcessPhaseOutput", "PreprocessPhaseOutput");
                            all_text = all_text.Replace("ProcessPhaseData", "PreprocessPhaseData");
                            //all_text = all_text.Replace("\r\n", "");
                            writer.Write(all_text);
                            //string line;
                            //while ((line = reader.ReadLine()) != null)
                            //{
                            //    line = line.Replace("VideoInterop", "Video");
                            //    line = line.Replace("ProcessPhaseOutput", "PreprocessPhaseOutput");
                            //    line = line.Replace("ProcessPhaseData", "PreprocessPhaseData");
                            //    line = line.Trim();
                            //    writer.WriteLine(line);
                            //}
                        }

                        File.Delete(path);
                        File.Move(new_name, path);

                        break;

                    default:
                        throw new Exception("ERROR NO VERSION VALID.");
                }
            }
        }
    }
}
