﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XMLInteropDLL
{
    [XmlInclude(typeof(PreprocessPhaseData))]
    public class PreprocessPhaseOutput : PhaseOutput, IXmlSerializable
    {
        public PreprocessPhaseOutput()
        {
            Data.Add(new PreprocessPhaseData());
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            try
            {
                base.ReadXml(reader);
                Data.Clear();
                Settings.Clear();

                while (reader.ReadToFollowing("PreprocessPhaseData"))
                {
                    Type t = Type.GetType("XMLInteropDLL." + reader.Name);
                    XmlSerializer serializer = new XmlSerializer(t);
                    object o = serializer.Deserialize(reader.ReadSubtree());
                    Data.Add(o as PhaseData);
                }
            }
            catch (Exception exc)
            {

            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            base.WriteXml(writer);

            foreach (PhaseData data in m_phase_data)
            {
                XmlSerializer serializer = new XmlSerializer(data.GetType());
                serializer.Serialize(writer, data);
            }
        }
    }
}
