﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace XMLInteropDLL
{
    public class IntervalPhaseSettings : PhaseSettings, INotifyPropertyChanged
    {

        public int BrightThresholdHi { get; set; }
        public int BrightThresholdLo { get; set; }
        public double BrightLoCutoff { get; set; }
        public double BrightHiCutoff { get; set; }
        public bool Invert { get; set; }
        public bool UseBright { get; set; }

        public int IntensityThreshold { get; set; }
        public int IntensityMinInterval { get; set; }
        public double IntensityLoCutoff { get; set; }
        public double IntensityHiCutoff { get; set; }


        /// <summary>
        /// Declares a default Settings Object for Intervals
        ///     -Thresholds must be set at [100 - x] in a (0,100) range
        /// </summary>
        public IntervalPhaseSettings()
            : base()
        {
            BrightThresholdHi = 67;
            BrightThresholdLo = 80;
            BrightLoCutoff = 0.0;
            BrightHiCutoff = 100.0;
            Invert = false;
            UseBright = false;

            IntensityMinInterval = 8;
            IntensityThreshold = 80;
            IntensityHiCutoff = 100.0;
            IntensityLoCutoff = 0.0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
