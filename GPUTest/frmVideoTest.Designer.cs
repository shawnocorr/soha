﻿namespace GPUTest
{
    partial class frmVideoTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.frameDisplay1 = new SOHAControls.DisplayControls.FrameDisplay();
            this.SuspendLayout();
            // 
            // frameDisplay1
            // 
            this.frameDisplay1.EnableFilters = false;
            this.frameDisplay1.Location = new System.Drawing.Point(165, 39);
            this.frameDisplay1.MmodeDisplay = false;
            this.frameDisplay1.Name = "frameDisplay1";
            this.frameDisplay1.Size = new System.Drawing.Size(706, 404);
            this.frameDisplay1.TabIndex = 0;
            this.frameDisplay1.UserInteractive = false;
            // 
            // frmVideoTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 639);
            this.Controls.Add(this.frameDisplay1);
            this.Name = "frmVideoTest";
            this.Text = "frmVideoTest";
            this.ResumeLayout(false);

        }

        #endregion

        private SOHAControls.DisplayControls.FrameDisplay frameDisplay1;


    }
}