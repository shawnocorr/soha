﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Video;
using SOHA;
using SOHA.Library;
using System.Diagnostics;

namespace GPUTest
{
    public partial class frmVideoTest : Form
    {
        private Video m_video;

        public frmVideoTest()
        {
            InitializeComponent();

            //m_video = new CFVideo(@"D:\SOHA\Good\10mCS_05_4wf_9300_60s_hypoxia122310 10m.cxd");
            //List<double> times_cf = new List<double>();
            //for(int i = 1; i <= m_video.Frames; i++)
            //{
            //    Stopwatch watch = new Stopwatch();
            //    watch.Start();
            //    m_video.GetFrameSync(i);
            //    watch.Stop();
            //    times_cf.Add(watch.ElapsedMilliseconds);
            //}
            //m_video.Close();

            m_video = new CXDVideo(@"D:\SOHA\Good\10mCS_05_4wf_9300_60s_hypoxia122310 10m.cxd");
            m_video.Open();
            List<double> times_cxd = new List<double>();
            for(int i = 1; i <= m_video.Frames; i++)
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                m_video.GetFrameSync(i);
                watch.Stop();
                times_cxd.Add(watch.ElapsedMilliseconds);
            }
            m_video.Close();

            frameDisplay1.VideoFile = m_video;
            //pictureBox1.BackgroundImage = m_video.GetFrameSync(1).ToBitmap();
        }
    }
}
