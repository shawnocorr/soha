﻿namespace GPUTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ucNormalize1 = new SOHAControls.FilterControls.ucSegment();
            this.ucSobel1 = new SOHAControls.FilterControls.ucSobel();
            this.ucGaussianSmooth1 = new SOHAControls.FilterControls.ucGaussianSmooth();
            this.ucSequentialFilterLayout1 = new SOHAControls.FilterControls.ucSequentialFilterLayout();
            this.frameDisplay1 = new SOHAControls.DisplayControls.FrameDisplay();
            this.ucHistogram1 = new SOHAControls.DisplayControls.ucHistogram();
            this.ucEqualize1 = new SOHAControls.FilterControls.ucEqualize();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(643, 422);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 23);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Filter Enable";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // ucNormalize1
            // 
            this.ucNormalize1.Location = new System.Drawing.Point(272, 451);
            this.ucNormalize1.Name = "ucNormalize1";
            this.ucNormalize1.Size = new System.Drawing.Size(220, 70);
            this.ucNormalize1.TabIndex = 8;
            // 
            // ucSobel1
            // 
            this.ucSobel1.Location = new System.Drawing.Point(498, 527);
            this.ucSobel1.Name = "ucSobel1";
            this.ucSobel1.Size = new System.Drawing.Size(220, 70);
            this.ucSobel1.TabIndex = 7;
            // 
            // ucGaussianSmooth1
            // 
            this.ucGaussianSmooth1.Location = new System.Drawing.Point(498, 451);
            this.ucGaussianSmooth1.Name = "ucGaussianSmooth1";
            this.ucGaussianSmooth1.Size = new System.Drawing.Size(220, 70);
            this.ucGaussianSmooth1.TabIndex = 6;
            // 
            // ucSequentialFilterLayout1
            // 
            this.ucSequentialFilterLayout1.Location = new System.Drawing.Point(724, 236);
            this.ucSequentialFilterLayout1.Name = "ucSequentialFilterLayout1";
            this.ucSequentialFilterLayout1.Size = new System.Drawing.Size(336, 439);
            this.ucSequentialFilterLayout1.TabIndex = 5;
            // 
            // frameDisplay1
            // 
            this.frameDisplay1.EnableFilters = false;
            this.frameDisplay1.Location = new System.Drawing.Point(12, 12);
            this.frameDisplay1.MmodeDisplay = false;
            this.frameDisplay1.Name = "frameDisplay1";
            this.frameDisplay1.Size = new System.Drawing.Size(706, 404);
            this.frameDisplay1.TabIndex = 3;
            this.frameDisplay1.UserInteractive = false;
            // 
            // ucHistogram1
            // 
            this.ucHistogram1.Location = new System.Drawing.Point(724, 12);
            this.ucHistogram1.Name = "ucHistogram1";
            this.ucHistogram1.Size = new System.Drawing.Size(336, 218);
            this.ucHistogram1.TabIndex = 2;
            // 
            // ucEqualize1
            // 
            this.ucEqualize1.Location = new System.Drawing.Point(272, 527);
            this.ucEqualize1.Name = "ucEqualize1";
            this.ucEqualize1.Size = new System.Drawing.Size(220, 70);
            this.ucEqualize1.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1467, 687);
            this.Controls.Add(this.frameDisplay1);
            this.Controls.Add(this.ucEqualize1);
            this.Controls.Add(this.ucNormalize1);
            this.Controls.Add(this.ucSobel1);
            this.Controls.Add(this.ucGaussianSmooth1);
            this.Controls.Add(this.ucSequentialFilterLayout1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.ucHistogram1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SOHAControls.DisplayControls.ucHistogram ucHistogram1;
        private SOHAControls.DisplayControls.FrameDisplay frameDisplay1;
        private System.Windows.Forms.CheckBox checkBox1;
        private SOHAControls.FilterControls.ucSequentialFilterLayout ucSequentialFilterLayout1;
        private SOHAControls.FilterControls.ucGaussianSmooth ucGaussianSmooth1;
        private SOHAControls.FilterControls.ucSobel ucSobel1;
        private SOHAControls.FilterControls.ucSegment ucNormalize1;
        private SOHAControls.FilterControls.ucEqualize ucEqualize1;
    }
}

