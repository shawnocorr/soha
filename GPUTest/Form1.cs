﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.GPU;
using SOHA.Library.Mathematics;
using SOHA.Library.Video;
using SOHA.Library;

namespace GPUTest
{
    public partial class Form1 : Form
    {

        private string TEST_PATH = @"C:\Users\Public\Pictures\Sample Pictures\Desert.jpg";

        private CXDVideo m_video = new CXDVideo(@"D:\SOHA\Good\HIGMH5xP38_09_3wf_93001_30s.cxd");
        private Frame m_frame = null;
        private Bitmap m_bmp;

        public Form1()
        {
            InitializeComponent();

            Logs.AppDataFolder = @"C:\test\logs";

            ucSequentialFilterLayout1.Updated += new EventHandler(ucSequentialFilterLayout1_Updated);

            frameDisplay1.VideoFile = m_video;
            frameDisplay1.FrameChanged += new EventHandler<FrameEventArgs>(frameDisplay1_FrameChanged);
            frameDisplay1.HistogramUpdated += new EventHandler(frameDisplay1_HistogramUpdated);

            if(Options.AutoPreAnalyze)
                m_video.PreAnalyze(new ProcessEventArgs(), null);

            //m_bmp = m_frame.ToBitmap();
        }

        void frameDisplay1_HistogramUpdated(object sender, EventArgs e)
        {
            if(frameDisplay1.PostOpFrame != null)
            {
                ucHistogram1.SetFrame(frameDisplay1.PostOpFrame);
            }
            else
            {
                ucHistogram1.SetFrame(frameDisplay1.CurrentFrame);
            }
        }

        void ucSequentialFilterLayout1_Updated(object sender, EventArgs e)
        {
            frameDisplay1.Filters = ucSequentialFilterLayout1.GetFilters;
        }

        void frameDisplay1_FrameChanged(object sender, EventArgs e)
        {
            ucHistogram1.SetFrame(frameDisplay1.PostOpFrame);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            frameDisplay1.KeyPress(e);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            frameDisplay1.EnableFilters = checkBox1.Checked;
        }
    }
}
