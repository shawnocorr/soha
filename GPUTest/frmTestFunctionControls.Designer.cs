﻿namespace GPUTest
{
    partial class frmTestFunctionControls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucBrightFunctionControlDisplay1 = new SOHAControls.DisplayControls.ucBrightFunctionControlDisplay();
            this.SuspendLayout();
            // 
            // ucBrightFunctionControlDisplay1
            // 
            this.ucBrightFunctionControlDisplay1.Location = new System.Drawing.Point(12, 38);
            this.ucBrightFunctionControlDisplay1.Name = "ucBrightFunctionControlDisplay1";
            this.ucBrightFunctionControlDisplay1.Range = null;
            this.ucBrightFunctionControlDisplay1.Size = new System.Drawing.Size(925, 299);
            this.ucBrightFunctionControlDisplay1.TabIndex = 0;
            // 
            // frmTestFunctionControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 420);
            this.Controls.Add(this.ucBrightFunctionControlDisplay1);
            this.Name = "frmTestFunctionControls";
            this.Text = "frmTestFunctionControls";
            this.ResumeLayout(false);

        }

        #endregion

        private SOHAControls.DisplayControls.ucBrightFunctionControlDisplay ucBrightFunctionControlDisplay1;



    }
}