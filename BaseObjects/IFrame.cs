﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseObjects
{
    public interface IFrame<T> where T : struct
    {
        T Max();
        int Width { get; }
        int Height { get; }
    }
}
