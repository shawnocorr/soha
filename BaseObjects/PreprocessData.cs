﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseObjects
{
    public class PreprocessData
    {
        protected int m_total_frames;
        protected double[] m_times_from_start;
        protected double[] m_darkness;
        protected double[] m_intensity;
        protected MMode m_mmode;

        public double[] TimesFromStart { get { return m_times_from_start; } }
        public double[] Darkness { get { return m_darkness; } }
        public double[] Intensity { get { return m_intensity; } set { m_intensity = value; } }
        public int TotalFrames { get { return m_total_frames; } }
        public MMode MMode { get { return m_mmode; } }

        public PreprocessData()
        {
            m_total_frames = 0;

            m_times_from_start = new double[1];
            m_darkness = new double[1];
            m_intensity = new double[1];

            m_times_from_start[0] = 0;
            m_darkness[0] = 0;
            m_intensity[0] = 0;
        }

        public PreprocessData(int total_frames)
        {
            m_total_frames = total_frames;
            int i = m_total_frames;

            m_times_from_start = new double[i];
            m_darkness = new double[i];
            m_intensity = new double[i - 1];
            m_mmode = new MMode();
        }

    }
}
