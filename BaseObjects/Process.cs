﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BaseObjects
{
    public abstract class Processable : IProcess
    {
        public event EventHandler ProgressBegin;
        public event ProcessProgressUpdate ProgressUpdate;
        public event ProcessComplete ProcessComplete;

        public abstract void Run(object arg, CancellationToken cancel_token);

        protected virtual void OnUpdate(object obj, int pct)
        {
            if (ProgressUpdate != null)
                ProgressUpdate(obj, pct);
        }

        protected virtual void OnComplete(ProgressEventArgs e)
        {
            if (ProcessComplete != null)
                ProcessComplete(this, e);
        }

        protected virtual void OnBegin(EventArgs e)
        {
            if (ProgressBegin != null)
                ProgressBegin(this, e);
        }
    }
}
