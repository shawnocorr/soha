﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseObjects
{
    public enum PhaseType
    {
        New = 0,
        Mark,
        Preprocess,
        Interval,
        Phase,
        RedDot,
        MModeSelect,
        Properties
    }

    class Enums
    {
    }
}
