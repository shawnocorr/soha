﻿namespace BaseObjects
{
    partial class frmUserInputFramerate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOriginalRate = new System.Windows.Forms.Label();
            this.cmbMultiplier = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblNewRate = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoFull = new System.Windows.Forms.RadioButton();
            this.rdoTime = new System.Windows.Forms.RadioButton();
            this.numStart = new System.Windows.Forms.NumericUpDown();
            this.numEnd = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnd)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOriginalRate
            // 
            this.lblOriginalRate.AutoSize = true;
            this.lblOriginalRate.Location = new System.Drawing.Point(12, 9);
            this.lblOriginalRate.Name = "lblOriginalRate";
            this.lblOriginalRate.Size = new System.Drawing.Size(35, 13);
            this.lblOriginalRate.TabIndex = 0;
            this.lblOriginalRate.Text = "label1";
            // 
            // cmbMultiplier
            // 
            this.cmbMultiplier.FormattingEnabled = true;
            this.cmbMultiplier.Items.AddRange(new object[] {
            ".25x",
            ".5x",
            "1x",
            "2x",
            "4x"});
            this.cmbMultiplier.Location = new System.Drawing.Point(180, 42);
            this.cmbMultiplier.Name = "cmbMultiplier";
            this.cmbMultiplier.Size = new System.Drawing.Size(121, 21);
            this.cmbMultiplier.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Select New Frame Rate Multipler";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(163, 205);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Create AVI";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblNewRate
            // 
            this.lblNewRate.AutoSize = true;
            this.lblNewRate.Location = new System.Drawing.Point(12, 87);
            this.lblNewRate.Name = "lblNewRate";
            this.lblNewRate.Size = new System.Drawing.Size(35, 13);
            this.lblNewRate.TabIndex = 4;
            this.lblNewRate.Text = "label3";
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(15, 205);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(142, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numEnd);
            this.groupBox1.Controls.Add(this.numStart);
            this.groupBox1.Controls.Add(this.rdoTime);
            this.groupBox1.Controls.Add(this.rdoFull);
            this.groupBox1.Location = new System.Drawing.Point(5, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(301, 100);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Length";
            // 
            // rdoFull
            // 
            this.rdoFull.AutoSize = true;
            this.rdoFull.Location = new System.Drawing.Point(10, 19);
            this.rdoFull.Name = "rdoFull";
            this.rdoFull.Size = new System.Drawing.Size(73, 17);
            this.rdoFull.TabIndex = 0;
            this.rdoFull.Text = "Full Movie";
            this.rdoFull.UseVisualStyleBackColor = true;
            this.rdoFull.CheckedChanged += new System.EventHandler(this.rdoFull_CheckedChanged);
            // 
            // rdoTime
            // 
            this.rdoTime.AutoSize = true;
            this.rdoTime.Checked = true;
            this.rdoTime.Location = new System.Drawing.Point(10, 42);
            this.rdoTime.Name = "rdoTime";
            this.rdoTime.Size = new System.Drawing.Size(86, 17);
            this.rdoTime.TabIndex = 1;
            this.rdoTime.TabStop = true;
            this.rdoTime.Text = "Time Interval";
            this.rdoTime.UseVisualStyleBackColor = true;
            this.rdoTime.CheckedChanged += new System.EventHandler(this.rdoTime_CheckedChanged);
            // 
            // numStart
            // 
            this.numStart.Location = new System.Drawing.Point(102, 39);
            this.numStart.Name = "numStart";
            this.numStart.Size = new System.Drawing.Size(37, 20);
            this.numStart.TabIndex = 2;
            // 
            // numEnd
            // 
            this.numEnd.Location = new System.Drawing.Point(167, 39);
            this.numEnd.Name = "numEnd";
            this.numEnd.Size = new System.Drawing.Size(37, 20);
            this.numEnd.TabIndex = 3;
            this.numEnd.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(145, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "to";
            // 
            // frmUserInputFramerate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 240);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lblNewRate);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbMultiplier);
            this.Controls.Add(this.lblOriginalRate);
            this.Name = "frmUserInputFramerate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Choose Frame Rate";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOriginalRate;
        private System.Windows.Forms.ComboBox cmbMultiplier;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblNewRate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numEnd;
        private System.Windows.Forms.NumericUpDown numStart;
        private System.Windows.Forms.RadioButton rdoTime;
        private System.Windows.Forms.RadioButton rdoFull;
    }
}