﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseObjects
{
    public delegate void ProcessProgressUpdate(object sender, int percent);
    public delegate void ProcessComplete(object sender, ProgressEventArgs e);

    public interface IProcess
    {
        event ProcessProgressUpdate ProgressUpdate;
        event ProcessComplete ProcessComplete;

        //void OnUpdate(object obj, int pct);
        //void OnComplete(ProgressEventArgs e);
    }

    public class ProgressEventArgs : EventArgs
    {

        public bool Cancelled
        {
            get;
            private set;
        }
        public object Return
        {
            get;
            private set;
        }
        public bool Error
        {
            get;
            private set;
        }
        public Exception Exception
        {
            get;
            private set;
        }
        
        public ProgressEventArgs()
            : base()
        {

        }

        public ProgressEventArgs(object ret, bool WasCancelled, bool error, Exception exc)
            : base()
        {
            Return = ret;
            Cancelled = WasCancelled;
            Error = error;
            Exception = exc;
        }
    }
}
