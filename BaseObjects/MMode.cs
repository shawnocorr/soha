﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace BaseObjects
{
    [XmlInclude(typeof(MMode))]
    public class MMode : Frame, IXmlSerializable
    {

        protected int m_x_coordinate;

        public int XCoordinate
        {
            get
            {
                return m_x_coordinate;
            }
            set
            {
                m_x_coordinate = value;
            }
        }
        public byte[] MModeStream
        {
            get
            {
                if (m_pixel_values != null && m_pixel_values.Length != 0)
                {
                    MemoryStream mem_stream = new MemoryStream();
                    foreach (double d in m_pixel_values)
                    {
                        Int16 val = (Int16)d;
                        byte[] byte_array = BitConverter.GetBytes(val);
                        mem_stream.Write(byte_array, 0, byte_array.Length);
                    }
                    return mem_stream.ToArray();
                }
                else
                {
                    return new byte[] { new byte() };
                }
            }
            set
            {
                if (value != null)
                    DeserialzizeMMode(value);
            }
        }

        public MMode()
            : base()
        {

        }

        public Bitmap PartialMMode(int start, int end)
        {
            int width = end - start;

            int stride = 3 * width;
            while (stride % 4 != 0)
                stride++;

            double max = this.Max();

            IntPtr scan = new IntPtr();
            Bitmap bmp = new Bitmap(width, this.Height, stride, PixelFormat.Format24bppRgb, scan);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            unsafe
            {
                byte* imgPtr = (byte*)(bmpdata.Scan0.ToPointer());
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = start; j < end; j++)
                    {
                        int p = (int)(this.m_pixel_values[j, i] / max * 255);
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                    }

                    imgPtr += stride - 3 * bmpdata.Width;
                }
            }

            bmp.UnlockBits(bmpdata);
            return bmp;
        }

        public static MMode XExtrapolate(MMode mmode, int n)
        {
            MMode new_mmode = new MMode();
            new_mmode.PixelValues = new double[mmode.Width * n, mmode.Height];
            new_mmode.XCoordinate = mmode.XCoordinate;

            for (int i = 0; i < mmode.Width; i++)
            {
                for (int j = 0; j < mmode.Height; j++)
                {
                    for (int k = 0; k < n; k++)
                    {
                        new_mmode[i * n + k, j] = mmode[i, j];
                    }
                }
            }

            return new_mmode;
        }

        public unsafe void DeserialzizeMMode(byte[] stream)
        {
            if (base.Size != new Size(0, 0))
            {
                MemoryStream ms = new MemoryStream(stream);

                m_pixel_values = new double[base.Size.Width, base.Size.Height];

                byte[] val = new byte[2];
                for (int x = 0; x < base.Size.Width; x++)
                {
                    for (int y = 0; y < base.Size.Height; y++)
                    {
                        ms.Read(val, 0, val.Length);
                        m_pixel_values[x, y] = BitConverter.ToInt16(val, 0);
                    }
                }
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            m_x_coordinate = Int32.Parse(reader.GetAttribute("XCoord"));
            int width = Int32.Parse(reader.GetAttribute("Width"));
            int height = Int32.Parse(reader.GetAttribute("Height"));

            m_pixel_values = new double[width, height];

            reader.ReadToDescendant("base64Binary");
            XmlSerializer serializer = new XmlSerializer(typeof(byte[]));
            MModeStream = (byte[])serializer.Deserialize(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            try
            {
                writer.WriteAttributeString("XCoord", m_x_coordinate.ToString());
                writer.WriteAttributeString("Height", m_pixel_values.GetLength(1).ToString());
                writer.WriteAttributeString("Width", m_pixel_values.GetLength(0).ToString());

                XmlSerializer serializer = new XmlSerializer(typeof(byte[]));
                serializer.Serialize(writer, MModeStream);
            }
            catch (Exception exc)
            {

            }
        }
    }
}
