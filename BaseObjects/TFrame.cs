﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.InteropServices.ComTypes;
using System.Drawing;
using System.Drawing.Imaging;

namespace BaseObjects
{
    public class TFrame<T> where T:struct
    {
        protected int m_frame_number;
        protected T[,] m_pixel_values;
        protected T m_time_from_start;

        [XmlIgnore]
        public T[,] PixelValues
        {
            get
            {
                return m_pixel_values;
            }
            set
            {
                m_pixel_values = value;
            }
        }
        [XmlIgnore]
        public T this[int x, int y]
        {
            get
            {
                return m_pixel_values[x, y];
            }
            set
            {
                m_pixel_values[x,y] = value;
            }
        }
        [XmlIgnore]
        public int FrameNumber
        {
            get
            {
                return m_frame_number;
            }
            set
            {
                m_frame_number = value;
            }
        }
        [XmlIgnore]
        public T TimeFromStart
        {
            get
            {
                return m_time_from_start;
            }
            set
            {
                m_time_from_start = value;
            }
        }
        public int Width
        {
            get
            {
                return m_pixel_values.GetLength(0);
            }
        }
        public int Height
        {
            get
            {
                return m_pixel_values.GetLength(1);
            }
        }
        [XmlIgnore]
        public Size Size
        {
            get
            {
                return new Size(Width, Height);
            }
        }
        [XmlIgnore]
        public bool IsBlank
        {
            get;
            protected set;
        }

        public TFrame()
        {
            m_frame_number = 0;
            m_time_from_start = default(T);
            m_pixel_values = new T[1, 1];
            m_pixel_values[0, 0] = default(T);
        }

        public TFrame(int width, int height, T time, ref IStream inStream, int frameNumber)
        {
            #region Initialize Values
            m_pixel_values = new T[width, height];
            m_frame_number = frameNumber;
            m_time_from_start = time;
            #endregion

            byte[] px = new byte[width * height * 2];
            inStream.Read(px, px.Length, IntPtr.Zero);
            int ct = 0;

            //StreamWriter sw = new StreamWriter(@"C:\test_pix_values.txt");

            #region Assign Pixel Values

            ushort H = ushort.MinValue;
            ushort L = ushort.MaxValue;
            for (int i = 0; i < m_pixel_values.GetLength(1); i++)
            {
                for (int j = 0; j < m_pixel_values.GetLength(0); j++)
                {
                    byte[] pix = new byte[2];
                    pix[0] = px[ct++];
                    pix[1] = px[ct++];

                    ushort testpix = BitConverter.ToUInt16(pix, 0);

                    if (testpix > H)
                        H = testpix;

                    if (testpix < L)
                        L = testpix;

                    m_pixel_values[j, i] = (T)Convert.ChangeType(testpix, typeof(T));//(T)(int)testpix;// / scaling * 255.0;
                }
            }

            #endregion
        }

        public TFrame(Bitmap bmp, int frameNumber, int fps)
        {
            m_frame_number = frameNumber;
            m_time_from_start = (T)Convert.ChangeType(m_frame_number / (double)fps, typeof(T));
            m_pixel_values = new T[bmp.Width, bmp.Height];

            BitmapData bmpdata = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);

            int PixelSize = 0;
            switch (bmp.PixelFormat)
            {
                case PixelFormat.Alpha:
                    break;
                case PixelFormat.Canonical:
                    break;
                case PixelFormat.DontCare:
                    break;
                case PixelFormat.Extended:
                    break;
                case PixelFormat.Format16bppArgb1555:
                    break;
                case PixelFormat.Format16bppGrayScale:
                    break;
                case PixelFormat.Format16bppRgb555:
                    break;
                case PixelFormat.Format16bppRgb565:
                    break;
                case PixelFormat.Format1bppIndexed:
                    break;
                case PixelFormat.Format24bppRgb:
                    break;
                case PixelFormat.Format32bppArgb:
                    PixelSize = 4;
                    break;
                case PixelFormat.Format32bppPArgb:
                    break;
                case PixelFormat.Format32bppRgb:
                    break;
                case PixelFormat.Format48bppRgb:
                    break;
                case PixelFormat.Format4bppIndexed:
                    break;
                case PixelFormat.Format64bppArgb:
                    break;
                case PixelFormat.Format64bppPArgb:
                    break;
                case PixelFormat.Format8bppIndexed:
                    break;
                case PixelFormat.Gdi:
                    break;
                case PixelFormat.Indexed:
                    break;
                case PixelFormat.Max:
                    break;
                case PixelFormat.PAlpha:
                    break;
                default:
                    break;
            }

            unsafe
            {
                for (int y = 0; y < bmpdata.Height; y++)
                {
                    byte* row = (byte*)bmpdata.Scan0 + (y * bmpdata.Stride);
                    for (int x = 0; x < bmpdata.Width; x++)
                    {
                        m_pixel_values[x, y] = (T)Convert.ChangeType(row[x * PixelSize], typeof(T));
                    }
                }
            }
        }

        public static TFrame<T> BlankFrame(int width, int height)
        {
            TFrame<T> f = new TFrame<T>();
            f.m_pixel_values = new T[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    f.m_pixel_values[i, j] = default(T);
                }
            }

            f.IsBlank = true;
            return f;
        }

        public static bool IsBlankFrame(TFrame<T> f)
        {
            return f.IsBlank;
        }

        public static void Crop(ref TFrame<T> frame, Rectangle rect)
        {
            T[,] new_values = new T[rect.Width, rect.Height];
            //Frame ret_frame = new Frame(rect.Size, m_number);

            for (int i = rect.Left; i <= rect.Left + rect.Width - 1; i++)
            {
                for (int j = rect.Top; j < rect.Top + rect.Height - 1; j++)
                {
                    new_values[i - rect.Left, j - rect.Top] = frame.m_pixel_values[i, j];
                }
            }

            frame.m_pixel_values = new_values;
        }

        public Size TrimSize()
        {
            int real_x = 0;
            int real_y = 0;

            for (int x = this.Size.Width - 1; x >= 0; x--)
            {
                bool col_blank = true;
                for (int y = this.Size.Height - 1; y >= 0; y--)
                {
                    if (!this[x, y].Equals(0) && !this[x, y].Equals(255))
                    {
                        col_blank = false;
                        real_y = y + 1;
                        break;
                    }
                }

                if (!col_blank)
                    break;
            }

            for (int y = this.Size.Height - 1; y >= 0; y--)
            {
                bool col_blank = true;
                for (int x = this.Size.Width - 1; x >= 0; x--)
                {
                    if (!this[x, y].Equals(0) && !this[x, y].Equals(255))
                    {
                        col_blank = false;
                        real_x = x + 1;
                        break;
                    }
                }

                if (!col_blank)
                    break;
            }

            return new Size(real_x, real_y);
        }

        public T Max()
        {
            T mx = default(T);
            for (int i = 0; i < m_pixel_values.GetLength(0); i++)
            {
                for (int j = 0; j < m_pixel_values.GetLength(1); j++)
                {
                    if ((double)Convert.ChangeType(m_pixel_values[i, j],typeof(double))
                        > (double)Convert.ChangeType(mx, typeof(double)))
                        mx = m_pixel_values[i, j];
                }
            }
            return mx;
        }

        public unsafe Bitmap ToBitmap()
        {
            int stride = 3 * this.Width;
            while (stride % 4 != 0)
                stride++;

            T max = this.Max();

            IntPtr scan = new IntPtr();
            Bitmap bmp = new Bitmap(this.Width, this.Height, stride, PixelFormat.Format24bppRgb, scan);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            unsafe
            {
                byte* imgPtr = (byte*)(bmpdata.Scan0.ToPointer());
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = 0; j < bmpdata.Width; j++)
                    {
                        double tmp = (double)Convert.ChangeType(this.m_pixel_values[j, i],typeof(double))
                            / (double)Convert.ChangeType(max, typeof(double));
                        int p = (int)(tmp * 255);
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                    }

                    imgPtr += stride - 3 * bmpdata.Width;
                }
            }

            bmp.UnlockBits(bmpdata);
            return bmp;
        }

        public unsafe Bitmap ToBitmap(int x, int y, int w, int h)
        {
            //if (x + w > m_pixel_values.GetLength(0))
            //    throw new Exception("Frame.ToBitmap(): Invalid width.");

            //if (y + h > m_pixel_values.GetLength(1))
            //    throw new Exception("Frame.ToBitmap(): Invalid height.");

            int stride = 3 * w;
            while (stride % 4 != 0)
                stride++;

            T max = this.Max();

            IntPtr scan = new IntPtr();
            Bitmap bmp = new Bitmap(w, h, stride, PixelFormat.Format24bppRgb, scan);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            unsafe
            {
                byte* imgPtr = (byte*)(bmpdata.Scan0.ToPointer());
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = 0; j < bmpdata.Width; j++)
                    {
                        int p;

                        if (j + x >= m_pixel_values.GetLength(0))
                        {
                            p = 0;
                        }
                        else
                        {
                            double tmp = (double)Convert.ChangeType(this.m_pixel_values[j, i], typeof(double))
                            / (double)Convert.ChangeType(max, typeof(double));
                            p = (int)(tmp * 255);
                        }

                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                    }

                    imgPtr += stride - 3 * bmpdata.Width;
                }
            }

            bmp.UnlockBits(bmpdata);
            return bmp;
        }

        public static unsafe Bitmap ToRedDotMovie(TFrame<T> f1, TFrame<T> f2, double threshold)
        {
            Bitmap bmp = new Bitmap(f1.m_pixel_values.GetLength(0), f1.m_pixel_values.GetLength(1), PixelFormat.Format24bppRgb);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            T m = f1.Max();

            for (int y = 0; y < bmpdata.Height; y++)
            {
                byte* row = (byte*)bmpdata.Scan0 + (y * bmpdata.Stride);
                //byte[] row = new byte[bmpdata.Stride];
                for (int x = 0; x < bmpdata.Width; x++)
                {
                    double tmp = (double)Convert.ChangeType(f2.m_pixel_values[x, y], typeof(double))
                               - (double)Convert.ChangeType(f1.m_pixel_values[x, y], typeof(double));
                    double dI = Math.Abs(tmp) / (double)Convert.ChangeType(f1[x, y],typeof(double));

                    if (dI > threshold)
                    {
                        /* Make Red Dot */
                        row[x * 3] = 0;
                        row[x * 3 + 1] = 0;
                        row[x * 3 + 2] = (byte)255;
                    }
                    else
                    {
                        tmp = (double)Convert.ChangeType(f1.m_pixel_values[x, y], typeof(double))
                            / (double)Convert.ChangeType(m, typeof(double));
                        byte b = (byte)(int)(tmp * 255);
                        row[x * 3] = b;
                        row[x * 3 + 1] = b;
                        row[x * 3 + 2] = b;
                    }
                }
            }

            bmp.UnlockBits(bmpdata);
            return bmp;
        }
    }
}
