﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseObjects
{
    public delegate void UpdateProgressCallback(int percent);

    public partial class frmProcessNotify : Form
    {
        public event EventHandler Cancel;

        public bool IsCancelled
        {
            get;
            set;
        }

        public frmProcessNotify()
        {
            InitializeComponent();
        }

        public frmProcessNotify(string label, string title)
        {
            InitializeComponent();

            this.Shown += new EventHandler(frmProcessNotify_Shown);
            this.Text = title;
            lblTitle.Text = label;

            IsCancelled = false;

            this.BringToFront();
            this.CenterToParent();
            this.TopMost = true;
            //this.ShowDialog();
        }

        void frmProcessNotify_Shown(object sender, EventArgs e)
        {
            this.Activate();
        }

        public void UpdateProgress(int percent)
        {
            if (InvokeRequired)
            {
                UpdateProgressCallback update = new UpdateProgressCallback(UpdateProgress);
                Invoke(update, new object[] { percent });
            }
            else
            {
                barProgress.Value = percent;
            }
        }

        public void End()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(this.Close));
            }
            else
            {
                this.Close();
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Cancelling...";
            IsCancelled = true;

            if (Cancel != null)
                Cancel(this, new EventArgs());
        }
    }
}
