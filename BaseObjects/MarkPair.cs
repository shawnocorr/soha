﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BaseObjects
{
    #region Enums

    [Flags()]
    public enum BeatType
    {
        Systole = 0,
        Diastole,
        XSystole,
        XDiastole,
        RegionOfInterest
    }

    [Flags()]
    public enum OrientationType
    {
        Vertical = 0,
        Horizontal,
        Diagonal
    }

    public enum MeasurementType
    {
        Standard = 0,
        Diagonal
    }
    #endregion

    public class MarkPair
    {
        private Point[] m_marks = new Point[2];
        private BeatType m_beat = BeatType.Systole;
        private OrientationType m_orientation = OrientationType.Vertical;
        private bool m_highlight = false;

        public Point Mark1
        {
            get
            {
                return m_marks[0];
            }
            set
            {
                m_marks[0] = value;
            }
        }
        public Point Mark2
        {
            get
            {
                return m_marks[1];
            }
            set
            {
                m_marks[1] = value;
            }
        }
        public BeatType Beat
        {
            get
            {
                return m_beat;
            }
            set
            {
                m_beat = value;
            }
        }
        public OrientationType Orientation
        {
            get
            {
                return m_orientation;
            }
            set
            {
                if (value != OrientationType.Diagonal && value != OrientationType.Horizontal && value != OrientationType.Vertical)
                    throw new Exception("InvalidValue");
                m_orientation = value;
            }
        }
        public bool Highlight
        {
            get
            {
                return m_highlight;
            }
            set
            {
                m_highlight = value;
            }
        }

        public override string ToString()
        {
            return Mark1.ToString() + "\t" + Mark2.ToString();
        }

        public MarkPair() { }

        public MarkPair(Point a)
        {
            m_marks[0] = a;
        }

        public MarkPair(Point a, Point b)
        {
            m_marks[0] = a;
            m_marks[1] = b;
        }

        public void Add(Point b)
        {
            if (!m_marks[1].Equals(new Point(0, 0)))
                throw new Exception();

            m_marks[1] = b;
        }

        #region Distance Measurement Functions
        public double Euclidean()
        {
            double dx = m_marks[1].X - m_marks[0].X;
            double dy = m_marks[1].Y - m_marks[0].Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }

        public double HorizontalDistance()
        {
            return (double)Math.Abs(m_marks[1].X - m_marks[0].X);
        }

        public double VerticalDistance()
        {
            return (double)Math.Abs(m_marks[1].Y - m_marks[0].Y);
        }
        #endregion
    }
}
