﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace BaseObjects
{
    public class CultureProperties : IXmlSerializable
    {
        private string m_genotype;
        private int m_id;
        private int m_age;
        private string m_sex;
        private string m_cameraid;
        private string m_time;
        private string m_comments;
        private string m_initials;

        public string Genotype
        {
            get
            {
                return m_genotype;
            }
            set
            {
                m_genotype = value;
            }
        }
        public int ID
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public int Age
        {
            get
            {
                return m_age;
            }
            set
            {
                m_age = value;
            }
        }
        public string Sex
        {
            get
            {
                return m_sex;
            }
            set
            {
                m_sex = value;
            }
        }
        public string CameraId
        {
            get
            {
                return m_cameraid;
            }
            set
            {
                m_cameraid = value;
            }
        }
        public string Comments
        {
            get
            {
                return m_comments;
            }
            set
            {
                m_comments = value;
            }
        }
        public string Time
        {
            get
            {
                return m_time;
            }
            set
            {
                m_time = value;
            }
        }
        public string Initials
        {
            get
            {
                return m_initials;
            }
            set
            {
                m_initials = value;
            }
        }

        public CultureProperties() { }

        public CultureProperties(string name)
        {
            try
            {
                //if (!File.Exists(filepath))
                //    throw new FileNotFoundException();

                //Retreive filename without filepath and extension
                //string shortname = filepath.Substring(filepath.LastIndexOf("\\") + 1);
                //shortname = shortname.Substring(0, shortname.Length - 4);

                //Parse filename: try by "_" then try by "-"
                string[] tokens = name.Split('_');
                if (tokens.Length <= 2)
                    tokens = name.Split('-');

                if (tokens.Length >= 7)
                    throw new FormatException("Invalid number of tokens.");

                m_genotype = tokens[0];

                m_id = 0;
                //remove leading zeros
                string id = tokens[1];
                for (int i = 0; i < tokens[1].Length; i++)
                {
                    if (Int16.Parse(id) == 0)
                    {
                        id.Remove(0, 1);
                    }
                }
                m_id = Int16.Parse(id);

                //Parse age and sex
                int pos = tokens[2].IndexOfAny(new char[] { 'm', 'f', 'w', 'd', 'h' });
                if (pos == 0)
                {
                    throw new FormatException("Could not parse sex.");
                }
                else
                {
                    m_age = Int32.Parse(tokens[2].Substring(0, pos));
                    m_sex = ((tokens[2])[tokens[2].Length - 1]).ToString();
                }

                m_cameraid = tokens[3];
                m_time = tokens[4];

                if (tokens.Length > 5)
                {
                    m_comments = tokens[5];
                }
                else
                {
                    m_comments = "";
                }

                if (tokens.Length == 7)
                {
                    m_initials = tokens[6];
                }
                else
                {
                    m_initials = "";
                }
            }
            catch (Exception exc)
            {
                //frmCultureProperties frm = new frmCultureProperties(filepath);

                //while (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                //    Application.DoEvents();

            }
        }

        public object[] ToArray()
        {
            return new object[]{
                m_genotype,
                (int)m_id,
                m_comments,
                (int)m_age,
                m_sex
            };
        }

        public override string ToString()
        {
            string str = m_genotype + "_" +
                m_id + "_" +
                m_age + "_" +
                m_sex + "_" +
                m_cameraid + "_" +
                m_time + "_" +
                m_comments + "_" +
                m_initials;

            while (str[str.Length - 1] == '_')
                str.Remove(str.Length - 1, 1);

            return str;
        }

        internal string ToCSV()
        {
            return m_genotype + ","
                + m_id + ","
                + m_comments + ","
                + m_age + ","
                + m_sex + ",";
        }
        
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            try
            {
                m_genotype = reader["Genotype"];
                m_id = Int32.Parse(reader["ID"]);
                m_age = Int32.Parse(reader["Age"]);
                m_sex = reader["Sex"];
                m_cameraid = reader["CameraID"];
                m_comments = reader["Comments"];
                m_time = reader["Time"];
                m_initials = reader["Initials"];
            }
            catch (Exception exc)
            {
                throw new Exception("Error reading XML.");
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Genotype", m_genotype);
            writer.WriteAttributeString("ID", m_id.ToString());
            writer.WriteAttributeString("Age", m_age.ToString());
            writer.WriteAttributeString("Sex", m_sex);
            writer.WriteAttributeString("CameraID", m_cameraid);
            writer.WriteAttributeString("Comments", m_comments);
            writer.WriteAttributeString("Time", m_time);
            writer.WriteAttributeString("Initials", m_initials);
        }
    }
}
