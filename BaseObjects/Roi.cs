﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BaseObjects
{
    public class Roi
    {
        private string m_name;
        private int m_x;
        private int m_y;
        private int m_width;
        private int m_height;
        private bool m_highlight;

        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }
        public int X
        {
            get
            {
                return m_x;
            }

            set { m_x = value; }
        }
        public int Y
        {
            get
            {
                return m_y;
            }

            set { m_y = value; }
        }
        public int Width
        {
            get
            {
                return m_width;
            }

            set { m_width = value; }
        }
        public int Height
        {
            get
            {
                return m_height;
            }

            set { m_height = value; }
        }
        public bool Highlight
        {
            get
            {
                return m_highlight;
            }
            set
            {
                m_highlight = value;
            }
        }

        public Roi()
        {
            m_name = "";
            m_x = 0;
            m_y = 0;
            m_width = 0;
            m_height = 0;
            m_highlight = false;
        }

        public Roi(int x, int y, int width, int height)
        {
            m_name = "";
            m_x = x;
            m_y = y;
            m_width = width;
            m_height = height;
        }

        public Roi(int x, int y, Size size)
        {
            m_name = "";
            m_x = x;
            m_y = y;
            m_width = size.Width;
            m_height = size.Height;
        }

        public Rectangle ScaledRectangle(Size originalsize, Size actualsize)
        {
            double xf = (double)originalsize.Width / actualsize.Width;
            double yf = (double)originalsize.Height / actualsize.Height;
            return new Rectangle((int)(m_x / xf), (int)(m_y / yf), (int)(m_width / xf), (int)(m_height / yf));
        }

        public int Area()
        {
            if (m_height == null || m_height == 0 || m_width == null || m_width == 0)
                return -1;

            return m_width * m_height;
        }

        public static explicit operator Rectangle(Roi roi)
        {
            return new Rectangle(roi.m_x, roi.m_y, roi.m_width, roi.m_height);
        }
    }
}
