﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BaseObjects
{
    public enum ProcessingQueueStatus
    {
        Empty = 0,
        Running,
        Paused,
        Idle
    }

    public delegate void ProcessEventHandler(Processable process);

    public class ProcessingQueue : Processable
    {
        public event ProcessEventHandler ProcessBegun;

        private static object cancelLock = new object();
        private static object syncLock = new object();

        //private static TaskFactory m_factory = new TaskFactory(null,TaskCreationOptions.LongRunning,PriorityScheduler
        
        public static int MaxRunningTasks;
        Queue<Processable> m_queue = new Queue<Processable>();
        Dictionary<Processable, object> m_process_arg = new Dictionary<Processable, object>();
        Dictionary<Processable, CancellationTokenSource> m_task_cancel = new Dictionary<Processable, CancellationTokenSource>();

        List<Processable> m_running_tasks = new List<Processable>(MaxRunningTasks);

        int running_tasks = 0;
        int m_total_threads = 0;
        int m_threads_completed = 0;
        bool cancel = false;

        public int Count
        {
            get { return m_queue.Count; }
        }
        public bool IsEmpty
        {
            get { return m_queue.Count == 0; }
        }
        public int RunningTasks
        {
            get
            {
                return m_running_tasks.Count;
            }
        }
        public bool IsImmediate
        {
            get;
            private set;
        }
        public ProcessingQueueStatus Status
        {
            get;
            private set;
        }

        public ProcessingQueue(bool isImmediateQueue)
        {
            IsImmediate = isImmediateQueue;
        }

        ~ProcessingQueue()
        {
            CancelAll();
        }

        public void Start()
        {
            if (IsEmpty)
                throw new ProcessingQueueException("Start called when queue is empty.");

            m_threads_completed = 0;
            m_total_threads = m_queue.Count;
            Dequeue();
        }

        public void Cancel()
        {

        }

        public void Cancel(Processable process)
        {
            //Processable Item is in the queue but processing has not yet begun
            if (m_queue.Contains(process))
            {
                m_queue = new Queue<Processable>(from Processable p in m_queue where p != process select p);
            }
            else
            {
                //Processable Item is being processed
                if (m_task_cancel.ContainsKey(process))
                {
                    CancellationTokenSource token = m_task_cancel[process];

                    if (token != null)
                    {
                        token.Cancel();
                    }
                }
            }
        }

        public void CancelAll()
        {
            while (m_queue.Count > 0)
            {
                Processable process = m_queue.Dequeue();
                Cancel(process);
            }

            foreach (Processable process in m_running_tasks)
                Cancel(process);

            //Clear Queued 
            //m_queue.Clear();

            if (!this.IsEmpty)
                throw new ProcessingQueueException("Queue has not been fully cancelled.");
        }

        public void Enqueue(Processable process, object arg)
        {
            Monitor.Enter(syncLock);

            m_queue.Enqueue(process);
            Logs.LogTrace("Enqueue");

            if (!m_process_arg.Keys.Contains(process))
                m_process_arg.Add(process, arg);

            //if (IsImmediate && running_tasks < MaxRunningTasks)
            //{
            //    Dequeue();
            //}

            Monitor.Exit(syncLock);
            if (IsImmediate && m_running_tasks.Count() < MaxRunningTasks)
            {
                Dequeue();
            }
        }

        //public void Enqueue(List<Processable> items)
        //{
        //    if (!IsEmpty)
        //        throw new ProcessingQueueException("Queue is not empty; cannot add multiple items.");
        //    //m_queue = (Queue<Process>)items;

        //    Start();
        //}

        private void Dequeue()
        {
            Monitor.Enter(syncLock);

            Processable process = m_queue.Dequeue();
            Logs.LogTrace("Dequeue");

            process.ProgressUpdate += new ProcessProgressUpdate(process_ProgressUpdate);
            process.ProcessComplete += new ProcessComplete(process_ProcessComplete);

            OnProcessBegun(process);

            object arg = m_process_arg[process];

            CancellationTokenSource token_source = new CancellationTokenSource();
            CancellationToken cancel_token = token_source.Token;

            Task new_task = new Task(() => process.Run(arg,cancel_token), cancel_token, TaskCreationOptions.LongRunning);
            m_running_tasks.Add(process);
            m_task_cancel.Add(process, token_source);

            new_task.Start();

            Status = ProcessingQueueStatus.Running;

            Monitor.Exit(syncLock);
        }

        protected void OnProcessBegun(Processable process)
        {
            if (ProcessBegun != null)
                ProcessBegun(process);
        }
        
        void process_ProcessComplete(object sender, ProgressEventArgs e)
        {
            int percent = 0;
            Monitor.Enter(syncLock);

            m_running_tasks.Remove(sender as Processable);
            m_task_cancel.Remove(sender as Processable);

            if (IsEmpty)
            {
                //Ensure queue is empty and threads are stopped
                m_threads_completed++;

                percent = 0;
                if (m_total_threads != 0)
                    percent = (int)(m_threads_completed * 100 / m_total_threads);

                OnUpdate(e.Return, percent);
                OnComplete(e);
            }
            else
            {
                if (e.Cancelled)
                {
                    OnComplete(e);
                }

                m_threads_completed++;

                percent = 0;
                if (m_total_threads != 0)
                    percent = (int)(m_threads_completed * 100 / m_total_threads);

                OnUpdate(e.Return, percent);
                Dequeue();
            }

            Monitor.Exit(syncLock);
        }

        void process_ProgressUpdate(object sender, int percent)
        {
            OnUpdate(sender, percent);
        }

        public override void Run(object arg, CancellationToken cancel_token)
        {
            throw new NotImplementedException();
        }
    }

    [Serializable]
    public class ProcessingQueueException : Exception
    {
        public ProcessingQueueException() { }
        public ProcessingQueueException(string message) : base(message) { }
        public ProcessingQueueException(string message, Exception inner) : base(message, inner) { }
        protected ProcessingQueueException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
