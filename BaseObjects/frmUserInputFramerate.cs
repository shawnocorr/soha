﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BaseObjects
{
    public partial class frmUserInputFramerate : Form
    {

        private int m_original_rate;
        private int m_total_frames;

        public double NewRate
        {
            get;
            set;
        }

        public int NewFrameRate
        {
            get;
            set;
        }

        public int Start { get; private set; }
        public int End { get; private set; }

        public frmUserInputFramerate()
        {
            InitializeComponent();
        }

        public frmUserInputFramerate(int orig_rate, int total_frames)
        {
            InitializeComponent();

            m_original_rate = orig_rate;
            m_total_frames = total_frames;

            numStart.Minimum = numEnd.Minimum = 0;
            numStart.Maximum = numEnd.Maximum = m_total_frames / m_original_rate;

            lblOriginalRate.Text = "Original Frame Rate: " + orig_rate.ToString() + " fps";

            cmbMultiplier.SelectedIndex = 2;
            NewRate = 1;
            NewFrameRate = (int)(orig_rate * NewRate);
            cmbMultiplier.SelectedIndexChanged += new EventHandler(cmbMultiplier_SelectedIndexChanged);

            lblNewRate.Text = "New Frame Rate: " + NewFrameRate + " fps";
        }

        void cmbMultiplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rate = (string)cmbMultiplier.SelectedItem;

            NewRate = double.Parse(rate.Replace("x", ""));

            NewFrameRate = (int)(m_original_rate * NewRate);

            lblNewRate.Text = "New Frame Rate: " + NewFrameRate + " fps";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (numStart.Value >= numEnd.Value)
            {
                MessageBox.Show("End value must be greater than start value.");
                this.DialogResult = System.Windows.Forms.DialogResult.None;
                return;
            }

            if (rdoTime.Checked)
            {
                Start = (int)numStart.Value * m_original_rate == 0 ? 1 : (int)numStart.Value * m_original_rate;
                End = (int)numEnd.Value * m_original_rate;
            }

            if (rdoFull.Checked)
            {
                Start = 1;
                End = m_total_frames;
            }
        }

        private void rdoTime_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoTime.Checked)
            {
                numStart.Enabled = numEnd.Enabled = true;
            }
        }

        private void rdoFull_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoFull.Checked)
            {
                numStart.Enabled = numEnd.Enabled = false;
            }
        }
    }
}
