﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace BaseObjects
{
    //[XmlInclude(typeof(TMmode))]
    public class TMmode<T> where T : struct, IXmlSerializable, IFrame<T>
    {
        private TFrame<T> m_frame;
        protected int m_x_coordinate;

        public int Width
        {
            get
            {
                return m_frame.PixelValues.GetLength(0);
            }
        }
        public int Height
        {
            get
            {
                return m_frame.PixelValues.GetLength(1);
            }
        }
        public int XCoordinate
        {
            get
            {
                return m_x_coordinate;
            }
            set
            {
                m_x_coordinate = value;
            }
        }
        public byte[] MModeStream
        {
            get
            {
                if (m_frame.PixelValues != null && m_frame.PixelValues.Length != 0)
                {
                    MemoryStream mem_stream = new MemoryStream();
                    foreach (T d in m_frame.PixelValues)
                    {
                        short val = (short)Convert.ChangeType(d,typeof(short));
                        byte[] byte_array = BitConverter.GetBytes(val);
                        mem_stream.Write(byte_array, 0, byte_array.Length);
                    }
                    return mem_stream.ToArray();
                }
                else
                {
                    return new byte[] { new byte() };
                }
            }
            set
            {
                if (value != null)
                    DeserialzizeMMode(value);
            }
        }

        public TMmode()
        {
            m_frame = new TFrame<T>();
        }

        public Bitmap PartialMMode(int start, int end)
        {
            //Rectangle rect = new Rectangle(0, 0, end - start, this.Height);
            //return Crop(ref this, rect);

            int width = end - start;

            int stride = 3 * width;
            while (stride % 4 != 0)
                stride++;

            T max = Max();

            IntPtr scan = new IntPtr();
            Bitmap bmp = new Bitmap(width, this.Height, stride, PixelFormat.Format24bppRgb, scan);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            unsafe
            {
                byte* imgPtr = (byte*)(bmpdata.Scan0.ToPointer());
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = start; j < end; j++)
                    {
                        double tmp = (double)Convert.ChangeType(m_frame.PixelValues[j, i], typeof(double))
                               / (double)Convert.ChangeType(max, typeof(double));
                        int p = (int)(tmp * 255);
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                    }

                    imgPtr += stride - 3 * bmpdata.Width;
                }
            }

            bmp.UnlockBits(bmpdata);
            return bmp;
        }

        public T Max()
        {
            T mx = default(T);
            for (int i = 0; i < m_frame.PixelValues.GetLength(0); i++)
            {
                for (int j = 0; j < m_frame.PixelValues.GetLength(1); j++)
                {
                    if ((double)Convert.ChangeType(m_frame.PixelValues[i, j], typeof(double))
                        > (double)Convert.ChangeType(mx, typeof(double)))
                        mx = m_frame.PixelValues[i, j];
                }
            }
            return mx;
        }

        public static MMode XExtrapolate(MMode mmode, int n)
        {
            MMode new_mmode = new MMode();
            new_mmode.PixelValues = new double[mmode.Width * n, mmode.Height];
            new_mmode.XCoordinate = mmode.XCoordinate;

            for (int i = 0; i < mmode.Width; i++)
            {
                for (int j = 0; j < mmode.Height; j++)
                {
                    for (int k = 0; k < n; k++)
                    {
                        new_mmode[i * n + k, j] = mmode[i, j];
                    }
                }
            }

            return new_mmode;
        }

        public unsafe void DeserialzizeMMode(byte[] stream)
        {
            if (m_frame.Size != new Size(0, 0))
            {
                MemoryStream ms = new MemoryStream(stream);

                m_frame.PixelValues = new T[m_frame.Size.Width, m_frame.Size.Height];

                byte[] val = new byte[2];
                for (int x = 0; x < m_frame.Size.Width; x++)
                {
                    for (int y = 0; y < m_frame.Size.Height; y++)
                    {
                        ms.Read(val, 0, val.Length);
                        m_frame.PixelValues[x, y] = (T)Convert.ChangeType(BitConverter.ToInt16(val, 0), typeof(T));
                    }
                }
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            m_x_coordinate = Int32.Parse(reader.GetAttribute("XCoord"));
            int width = Int32.Parse(reader.GetAttribute("Width"));
            int height = Int32.Parse(reader.GetAttribute("Height"));

            m_frame.PixelValues = new T[width, height];

            reader.ReadToDescendant("m_frame64Binary");
            XmlSerializer serializer = new XmlSerializer(typeof(byte[]));
            MModeStream = (byte[])serializer.Deserialize(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            try
            {
                writer.WriteAttributeString("XCoord", m_x_coordinate.ToString());
                writer.WriteAttributeString("Height", m_frame.PixelValues.GetLength(1).ToString());
                writer.WriteAttributeString("Width", m_frame.PixelValues.GetLength(0).ToString());

                XmlSerializer serializer = new XmlSerializer(typeof(byte[]));
                serializer.Serialize(writer, MModeStream);
            }
            catch (Exception exc)
            {

            }
        }
    }
}
