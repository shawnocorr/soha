﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using OpticalHeartBeatAnalysis.Mathematics;

namespace BaseObjects
{

    public class IntervalList : IList<Interval>, IList
    {

        private List<Interval> m_list = new List<Interval>();

        public int Count
        {
            get
            {
                return m_list.Count;
            }
        }

        public Interval this[int index]
        {
            get
            {
                if (index == -1 || index == m_list.Count)
                {
                    return new Interval();
                }

                return m_list[index] as Interval;
            }
            set
            {
                m_list[index] = value;
            }
        }

        public IntervalList()
        {
            
        }

        public IntervalList(List<Interval> list)
        {
            m_list = list;
        }

        public void Union(IntervalList list2)
        {
            int end = list2.Count;
            for (int i = 0; i < end; i++)
            {
                this.Add(list2[i]);
            }
        }

        public static IntervalList Union(IntervalList list1, IntervalList list2)
        {
            IntervalList ret = new IntervalList();

            foreach (Interval i in list1)
            {
                ret.Add(i);
            }

            foreach (Interval i in list2)
            {
                ret.Add(i);
            }

            return ret;
        }

        public void Sort()
        {
            IEnumerable<Interval> sortedIntervals =
            from interval in m_list
            orderby interval.Frame ascending
            select interval;

            m_list = sortedIntervals.ToList<Interval>();
        }

        public static IntervalList Take(IntervalList init_list, int first_n)
        {
            IntervalList ret_list = new IntervalList();

            IEnumerable<int> SystolicIntervals =
                                (from Interval interval in init_list
                                 where (interval.IntervalType == IntervalType.Systole)
                                 select interval.Frame).Take(first_n);

            if (SystolicIntervals.Count() >= first_n)
            {

                int a = SystolicIntervals.ElementAt(first_n - 1);

                int b = (from Interval interval in init_list
                         where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                         select interval.Frame).First();

                return new IntervalList(init_list.TakeWhile(x => x.Frame <= b).ToList());
            }
            else
            {
                return init_list;
            }
        }

        public static IntervalList XExtrapolate(IntervalList list, int n)
        {
            IntervalList new_list = new IntervalList();

            for (int i = 0; i < list.Count; i++)
            {
                Interval interval = list[i].Clone();
                interval.Frame *= n;
                new_list.Add(interval);
            }

            return new_list;
        }

        public static IntervalList XExtrapolateInverse(IntervalList list, int n)
        {
            IntervalList new_list = new IntervalList();

            for (int i = 0; i < list.Count; i++)
            {
                Interval interval = list[i].Clone();
                interval.Frame /= n;
                new_list.Add(interval);
            }

            return new_list;
        }

        public static List<double[]> CurveFitIntervals(IntervalList list, double[] intensity)
        {
            var systoles = list.Where(i => i.IntervalType == IntervalType.Systole);

            foreach (Interval systole in systoles)
            {
                Interval diastole = list.First(x => x.IntervalType == IntervalType.Diastole && x.Frame > systole.Frame);

                double[] function = new double[diastole.Frame - systole.Frame];

                for (int i = systole.Frame; i < diastole.Frame; i++)
                {
                    function[i - systole.Frame] = intensity[i];
                }

                double[] polynomial = Polynomials.FindCoefficients(function.Length, function);
            }

            return new List<double[]>();
        }

        public void Shift(int shift)
        {
            foreach (Interval i in m_list)
            {
                i.Frame += shift;
            }
        }

        public bool IsSorted()
        {
            for (int i = 1; i < m_list.Count; i++)
            {
                if (m_list[i].Frame < m_list[i - 1].Frame)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Cleans up the interval list.
        /// Two systoles in a row: first systole removed
        /// Two diastoles in a row: second diastole removed
        /// </summary>
        public void Clean()
        {
            for (int i = 0; i < m_list.Count - 1; i++)
            {
                if (m_list[i].IntervalType == m_list[i + 1].IntervalType)
                {
                    if (m_list[i].IntervalType == IntervalType.Diastole)
                    {
                        m_list.RemoveAt(i + 1);
                    }
                    else
                    {
                        m_list.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        public object Clone()
        {
            IntervalList ret = new IntervalList();

            for (int i = 0; i < this.Count; i++)
            {
                Interval interval = new Interval();
                interval.Frame = this[i].Frame;
                interval.IntervalType = this[i].IntervalType;
                ret.Add(interval);
            }

            return ret;
        }

        public int IndexOfClosest(int frameNumber, PhaseType mmodeType)
        {
            int range = 5; //+ or -

            for (int i = 0; i < m_list.Count; i++)
            {

                if (mmodeType == PhaseType.Interval)
                {
                    if ((m_list[i] as Interval).IntervalType != IntervalType.Systole
                        && (m_list[i] as Interval).IntervalType != IntervalType.Diastole)
                        continue;
                }

                if (mmodeType == PhaseType.Phase)
                {
                    if ((m_list[i] as Interval).IntervalType != IntervalType.MaxVelocity
                        && (m_list[i] as Interval).IntervalType != IntervalType.Change)
                        continue;
                }

                if (frameNumber >= (m_list[i] as Interval).Frame - range &&
                    frameNumber <= (m_list[i] as Interval).Frame + range)
                {
                    return i;
                }
            }

            return -1;
        }

        public void ClearHighlights()
        {
            foreach (Interval i in m_list)
                i.Highlight = false;
        }

        public void DeleteHighlights()
        {
            for (int i = 0; i < m_list.Count; i++)
            {
                if ((m_list[i] as Interval).Highlight)
                {
                    m_list.RemoveAt(i);
                    i--;
                }
            }
        }

        public bool isValid()
        {
            for (int i = 0; i < m_list.Count - 1; i++)
            {
                if (m_list[i].IntervalType == m_list[i + 1].IntervalType)
                    return false;
            }
            return true;
        }

        #region IList<Interval> Members

        public int IndexOf(Interval item)
        {
            return m_list.IndexOf(item);
        }

        public void Insert(int index, Interval item)
        {
            m_list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            m_list.RemoveAt(index);
        }

        #endregion

        #region ICollection<Interval> Members

        public void Add(Interval item)
        {
            m_list.Add(item);
        }

        public void Clear()
        {
            m_list.Clear();
        }

        public bool Contains(Interval item)
        {
            return m_list.Contains(item);
        }

        public void CopyTo(Interval[] array, int arrayIndex)
        {
            m_list.CopyTo(array, arrayIndex);
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Interval item)
        {
            try
            {
                m_list.Remove(item);
                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
        }

        #endregion

        #region IEnumerable<Interval> Members

        IEnumerator<Interval> IEnumerable<Interval>.GetEnumerator()
        {
            return (IEnumerator<Interval>)m_list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        #endregion

        #region IList Members

        public int Add(object value)
        {
            throw new NotImplementedException();
        }

        public bool Contains(object value)
        {
            throw new NotImplementedException();
        }

        public int IndexOf(object value)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }

        public bool IsFixedSize
        {
            get { throw new NotImplementedException(); }
        }

        public void Remove(object value)
        {
            m_list.Remove((Interval)value);
        }

        object IList.this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public bool IsSynchronized
        {
            get { throw new NotImplementedException(); }
        }

        public object SyncRoot
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
