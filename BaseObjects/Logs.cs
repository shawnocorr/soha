﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BaseObjects
{
    public class Logs
    {
        private static string m_trace_lock = "";
        private static string m_exception_lock = "";

        public static string AppDataFolder { get; set; }

        public static void LogTrace(string message)
        {
            lock (m_trace_lock)
            {
                Directory.CreateDirectory(AppDataFolder);
                Directory.CreateDirectory(AppDataFolder + @"Traces\");

                string trace_file_path = AppDataFolder + @"Traces\" + DateTime.Today.ToShortDateString().Replace("/", "_") + ".txt";

                using (StreamWriter writer = new StreamWriter(trace_file_path, true))
                {
                    writer.WriteLine(DateTime.Now.ToLongTimeString() + ": " + message);
                }
            }
        }

        public static void LogException(Exception message)
        {
            lock (m_exception_lock)
            {
                Directory.CreateDirectory(AppDataFolder);
                Directory.CreateDirectory(AppDataFolder + @"Traces\");

                string trace_file_path = AppDataFolder + @"Traces\"
                    + message.GetType().ToString() + "-"
                    + DateTime.Today.ToShortDateString().Replace("/", "_") + ".txt";

                using (StreamWriter writer = new StreamWriter(trace_file_path, true))
                {
                    writer.WriteLine(DateTime.Now.ToLongTimeString() + ": " + message);
                }
            }
        }
    }
}
