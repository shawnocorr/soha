﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace BaseObjects
{
    public struct Statistics
    {
        public int n;

        public double mean;
        public double median;
        public double[] mode;
        public double std_dev;
    }

    public struct FrameStatistics
    {
        public int n;

        public double[] row_mean;
        public double[] col_mean;
        public double mean;

        public double[] row_median;
        public double[] col_median;
        public double median;

        public double[][] row_mode;
        public double[][] col_mode;
        public double[] mode;

        public double[] row_std_dev;
        public double[] col_std_dev;
        public double std_dev;
    }

    public class StatisticalAnalysis
    {
        public static Statistics Analyze(ArrayList input)
        {
            if (input.Count < 1)
            {
                return new Statistics();
            }

            Statistics stats = new Statistics();

            input.Sort();

            stats.n = input.Count;

            double sum = 0;

            #region Calculate Median
            if (input.Count % 2 == 0)
            {
                int middle_index = input.Count / 2;
                stats.median = ((double)input[middle_index] + (double)input[middle_index - 1]) / 2;
            }
            else
            {
                stats.median = (double)input[input.Count / 2];
            }
            #endregion

            #region Calculate Mean
            sum = 0;
            foreach (Double dbl in input)
            {
                sum += dbl;
            }
            stats.mean = sum / input.Count;
            #endregion

            #region Calculate Median

            double hi = 0;
            foreach (double x in input)
            {
                if (x > hi)
                    hi = x;
            }

            stats.mode = new double[(int)hi];

            foreach (double x in input)
            {
                //stats.mode[(int)x]++;
            }

            #endregion

            #region Calculate Std Deviation (using median)
            sum = 0;
            foreach (Double dbl in input)
            {
                sum += Math.Pow((dbl - stats.median), 2);
            }
            stats.std_dev = Math.Sqrt(sum / input.Count);
            #endregion

            return stats;
        }

        public static FrameStatistics FrameAnalysis(double[,] input)
        {
            if (input == null || input.Length < 1)
            {
                return new FrameStatistics();
            }

            FrameStatistics stats = new FrameStatistics();

            stats.n = input.Length;

            double sum = 0;

            #region Calculate Mean
            //Column
            stats.col_mean = new double[input.GetLength(0)];

            for (int j = 0; j < input.GetLength(1); j++)
            {//Col x Row
                sum = 0;
                for (int i = 0; i < input.GetLength(0); i++)
                {
                    sum += input[i, j];
                }
                stats.col_mean[j] = sum / input.GetLength(1);
            }

            //Row
            stats.row_mean = new double[input.GetLength(0)];

            for (int i = 0; i < input.GetLength(0); i++)
            {//Row x Col
                sum = 0;
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    sum += input[i, j];
                }
                stats.row_mean[i] = sum / input.GetLength(0);
            }

            //Overall Mean
            for (int i = 0; i < input.GetLength(0); i++)
            {//Row x Col
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    sum += input[i, j];
                }
            }
            stats.mean = sum / stats.n;
            #endregion

            #region Calculate Standard Deviation

            sum = 0;

            foreach (double d in input)
            {
                sum += Math.Pow(d - stats.mean, 2);
            }

            stats.std_dev = Math.Sqrt(sum);
            #endregion

            return stats;
        }

        public static FrameStatistics FrameAnalysis(int[,] input)
        {
            if (input == null || input.Length < 1)
            {
                return new FrameStatistics();
            }

            FrameStatistics stats = new FrameStatistics();

            stats.n = input.Length;

            double sum = 0;

            #region Calculate Mean
            //Column
            stats.col_mean = new double[input.GetLength(0)];

            for (int j = 0; j < input.GetLength(1); j++)
            {//Col x Row
                sum = 0;
                for (int i = 0; i < input.GetLength(0); i++)
                {
                    sum += input[i, j];
                }
                stats.col_mean[j] = sum / input.GetLength(1);
            }

            //Row
            stats.row_mean = new double[input.GetLength(0)];

            for (int i = 0; i < input.GetLength(0); i++)
            {//Row x Col
                sum = 0;
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    sum += input[i, j];
                }
                stats.row_mean[i] = sum / input.GetLength(0);
            }

            //Overall Mean
            for (int i = 0; i < input.GetLength(0); i++)
            {//Row x Col
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    sum += input[i, j];
                }
            }
            stats.mean = sum / stats.n;
            #endregion

            return stats;
        }

        public static int RowMean(int[,] input, int row)
        {
            double sum = 0;
            for (int i = 0; i < input.GetLength(1); i++)
            {
                sum += input[row, i];
            }
            sum /= input.GetLength(0);

            return (int)sum;
        }

        public static int ColumnMean(int[,] input, int col)
        {
            double sum = 0;
            for (int i = 0; i < input.GetLength(0); i++)
            {
                sum += input[i, col];
            }
            sum /= input.GetLength(1);

            return (int)sum;
        }

        public static double Mean(int[,] matrix)
        {
            double sum = 0;
            foreach (int x in matrix)
            {
                sum += x;
            }

            return sum / matrix.Length;
        }

        public static float Mean(float[] input)
        {
            float sum = 0;
            for (int i = 0; i < input.Length; i++)
            {
                sum += input[i];
            }
            return sum / input.Length;
        }

        public static float Median(float[] input)
        {
            ArrayList list = new ArrayList();
            for (int i = 0; i < input.Length; i++)
            {
                list.Add(input[i]);
            }
            list.Sort();
            return (float)list[list.Count / 2];
        }

        public static float StandardDeviation(float[] input)
        {
            float x = Median(input);
            float sum = 0;
            for (int i = 0; i < input.Length; i++)
            {
                sum += (float)Math.Pow(input[i] - x, 2);
            }
            return (float)Math.Sqrt(sum / input.Length);
        }

        public static double Mean(double[] input)
        {
            if (input == null)
                return Double.NaN;

            double sum = 0;
            for (int i = 0; i < input.Length; i++)
            {
                sum += input[i];
            }
            return sum / input.Length;
        }

        public static double Median(double[] input)
        {
            if (input == null || input.Length == 0)
                return Double.NaN;

            ArrayList list = new ArrayList();
            for (int i = 0; i < input.Length; i++)
            {
                list.Add(input[i]);
            }
            list.Sort();
            return (double)list[list.Count / 2];
        }

        public static double StandardDeviation(double[] input)
        {
            if (input == null || input.Length == 0)
                return Double.NaN;

            double x = Median(input);
            double sum = 0;
            for (int i = 0; i < input.Length; i++)
            {
                sum += (double)Math.Pow(input[i] - x, 2);
            }
            return (double)Math.Sqrt(sum / input.Length);
        }

        public static double Sum(double[] input)
        {
            if (input == null)
                return Double.NaN;

            double x = 0;
            foreach (double D in input)
                x += D;
            return x;
        }
    }
}
