﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SOHA.Library.Video
{
    public struct VideoProperties
    {
        public int TotalFrames;
        public Size Dimensions;

        /// <summary>
        /// Resolution in bpp
        /// </summary>
        public int PixelResolution;

        public double PixelMax;
        public double PixelMin;

        public int KBPerFrame
        {
            get
            {
                if(Dimensions == null)
                    return -1;

                return (int)(Dimensions.Width * Dimensions.Height * PixelResolution / 8 / 1024.0);
            }
        }

        public double MBPerVideo
        {
            get
            {
                if(Dimensions == null)
                    return -1;

                return (int)(TotalFrames * KBPerFrame / 1024.0);
            }
        }
    }
}
