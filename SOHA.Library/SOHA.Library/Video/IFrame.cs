﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Mathematics;

namespace SOHA.Library.Video
{
    public interface IFrame
    {
        int Width { get; }
        int Height { get; }
        double Max { get; }
        Matrix Values { get; set; }
        double this[int x, int y] { get; set; }
        bool IsBlank { get; }

        void CalculateMax();
    }
}
