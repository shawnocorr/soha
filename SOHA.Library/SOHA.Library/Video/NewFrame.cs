﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using SOHA.Library.Mathematics;
using System.Runtime.InteropServices.ComTypes;
using System.IO;

namespace SOHA.Library.Video
{
    public class NewFrame : BFrame
    {

        [XmlIgnore]
        public int FrameNumber
        {
            get;
            set;
        }
        [XmlIgnore]
        public double TimeFromStart
        {
            get;
            set;
        }

        public NewFrame()
        {
            Values = new Matrix();
            Values[0, 0] = default(double);
            FrameNumber = default(int);
            TimeFromStart = default(double);
        }

        public NewFrame(Matrix values)
        {
            Values = values;
        }

        public NewFrame(int width, int height, double time, ref IStream inStream, int frameNumber)
        {
            #region Initialize Values

            Values = new Matrix(width, height);
            FrameNumber = frameNumber;
            TimeFromStart = time;

            #endregion

            byte[] px = new byte[width * height * 2];
            inStream.Read(px, px.Length, IntPtr.Zero);
            int ct = 0;

            #region Assign Pixel Values

            ushort H = ushort.MinValue;
            ushort L = ushort.MaxValue;
            for (int i = 0; i < Values.Columns; i++)
            {
                for (int j = 0; j < Values.Rows; j++)
                {
                    byte[] pix = new byte[2];
                    pix[0] = px[ct++];
                    pix[1] = px[ct++];

                    ushort testpix = BitConverter.ToUInt16(pix, 0);

                    if (testpix > H)
                        H = testpix;

                    if (testpix < L)
                        L = testpix;

                    Values[j, i] = (int)testpix;    //scaling * 255.0;

                    if (Values[j, i] > m_max)
                        m_max = Values[j, i];

                }
            }

            m_recalculate_max = false;

            #endregion
        }

        public static explicit operator NewFrame(Matrix A)
        {
            return new NewFrame(A);
        }

    }
}
