﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

using BitMiracle.LibTiff.Classic;
using System.Drawing;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;
using Newtonsoft.Json;

namespace SOHA.Library.Video
{
    /// <summary>
    /// Class created on 1/7/2017
    /// 
    /// Enables use of OME.TIF format files in SOHA
    /// </summary>
    [XmlInclude(typeof(OMETIFVideo))]
    public class OMETIFVideo : Video
    {

        private Tiff m_image_stack;

        public OMETIFVideo() { }

        public OMETIFVideo(string path)
        {
            try
            {
                m_video_path = path;

                Open();
            }
            catch(Exception exc)
            {
                throw new VideoFileOpenException("Error Opening '" + Path.GetFileName(path) + "'", exc);
            }
        }

        public static OMETIFVideo FromVideo(Video video)
        {
            OMETIFVideo tif = new OMETIFVideo();
            tif.m_fps = video.Fps;
            tif.m_total_frames = video.Frames;
            tif.m_frame_height = video.Height;
            tif.m_frame_width = video.Width;
            tif.m_video_path = video.VideoPath;
            tif.m_camera = video.Camera;
            tif.m_frame_times = video.FrameTimes;

            return tif;
        }

        #region Video Functions

        public override void Open()
        {
            frmWaitScreen frmWait = new frmWaitScreen("Please be patient. This may take a second...", "Processing...");
            frmWait.Show();

            if(!m_opened)
            {
                if(!File.Exists(m_video_path))
                    throw new VideoFileNotFoundException("File Not Found: " + m_video_path);

                using(m_image_stack = Tiff.Open(m_video_path, "r"))
                {
                    if(m_image_stack == null)
                        throw new VideoFileOpenException("TIFF File Open Failed: " + m_video_path);

                    m_total_frames = m_image_stack.NumberOfDirectories();
                    m_image_stack.SetDirectory(0);

                    // Find the width and height of the m_image_stack
                    FieldValue[] value = m_image_stack.GetField(TiffTag.IMAGEWIDTH);
                    m_frame_width = value[0].ToInt();

                    value = m_image_stack.GetField(TiffTag.IMAGELENGTH);
                    m_frame_height = value[0].ToInt();

                    //int m_image_stackSize = m_frame_height * m_frame_width;
                    //int[] raster = new int[m_image_stackSize];

                    #region Get FPS

                    short numberOfDirectories = m_image_stack.NumberOfDirectories();
                    Dictionary<string, FieldValue[]> tags = new Dictionary<string, FieldValue[]>();
                    m_image_stack.SetDirectory(0);

                    for(ushort t = ushort.MinValue; t < ushort.MaxValue; ++t)
                    {
                        TiffTag tag = (TiffTag)t;
                        value = m_image_stack.GetField(tag);
                        if(value != null)
                        {
                            tags.Add(tag.ToString(), value);
                        }
                    }

                    int field_json = 50839;
                    FieldValue[] tag_value = tags[field_json.ToString()];
                    string value_tags = System.Text.Encoding.UTF8.GetString((byte[])tag_value[1].Value);
                    value_tags = value_tags.Replace("\0","");
                    int index_brace_start = value_tags.IndexOf('{');
                    int index_brace_end = value_tags.IndexOf('}');
                    string json = value_tags.Substring(index_brace_start, index_brace_end - index_brace_start + 1);
                    OMETIFFrameProperties deJson = JsonConvert.DeserializeObject<OMETIFFrameProperties>(json);
                    string[] splits = json.Split(',');
                    double total_time = deJson.FrameInterval * m_total_frames / 1000;
                    m_fps = (int)(m_total_frames/total_time);

                    #endregion

                    #region Output/Debug All Directories

                    //Dictionary<string, FieldValue[]> tags = new Dictionary<string, FieldValue[]>();
                    //using(StreamWriter writer = new StreamWriter(@"C:\EnumerateTiffTags.txt"))
                    //{
                    //    short numberOfDirectories = m_image_stack.NumberOfDirectories();
                    //    for(short d = 0; d < numberOfDirectories; ++d)
                    //    {
                    //        if(d != 0)
                    //            writer.WriteLine("---------------------------------");

                    //        m_image_stack.SetDirectory((short)d);

                    //        writer.WriteLine("Image {0}, page {1} has following tags set:\n", m_video_path, d);
                    //        for(ushort t = ushort.MinValue; t < ushort.MaxValue; ++t)
                    //        {
                    //            TiffTag tag = (TiffTag)t;
                    //            value = m_image_stack.GetField(tag);
                    //            if(value != null)
                    //            {
                    //                tags.Add(tag.ToString(), value);
                    //                for(int j = 0; j < value.Length; j++)
                    //                {
                    //                    writer.WriteLine("{0}", tag.ToString());
                    //                    writer.WriteLine("{0} : {1}", value[j].Value.GetType().ToString(), value[j].ToString());
                    //                }

                    //                writer.WriteLine();
                    //            }
                    //        }
                    //    }
                    //}
                    
                    #endregion

                    m_opened = true;
                }
            }

            frmWait.Close();
        }

        public override void Close()
        {
            if(m_image_stack != null)
                m_image_stack.Close();
            //throw new NotImplementedException();
        }

        public override Bitmap GetBitmap(int p_frame)
        {
            throw new NotImplementedException();
        }

        public override void GetFrame(int p_frame)
        {
            throw new NotImplementedException();
        }

        public override Frame GetFrameSync(int p_frame)
        {
            if(!m_opened)
                throw new VideoFileOpenException("File Not Opened: " + m_video_path);

            using(m_image_stack = Tiff.Open(m_video_path, "r"))
            {
                if(!m_image_stack.SetDirectory((short)(p_frame - 1)))
                    throw new VideoFileOpenException("TIF SetDirectory() Failed: " + m_video_path);

                int[] raster = new int[m_frame_height * m_frame_width];

                /* ReadRGBAImage returns an image thats alpha channel is represented and image is "blotchy" */
                //if(!m_image_stack.ReadRGBAImage(m_frame_width, m_frame_height, raster))
                //    throw new VideoFileOpenException("TIF ReadRGBAImage Failed: " + m_video_path);

                byte[] scanline = new byte[m_frame_height * m_frame_width * 2];
                try
                {
                    for(int i = 0; i < m_frame_height; i++)
                    {
                        byte[] line = new byte[m_frame_width * 2];
                        m_image_stack.ReadScanline(line, i);
                        line.CopyTo(scanline, i * line.Length);
                    }
                }
                catch(Exception exc)
                {
                    throw new VideoFileOpenException("Error Scanning OMETIF lines.", exc);
                }

                double time = (p_frame - 1) * (1.0 / m_fps);
                Frame fr = new Frame(m_frame_width, m_frame_height, time, scanline, p_frame);
                //Frame fr = new Frame(m_frame_width, m_frame_height, 0, raster, 12, p_frame);

                return fr;
            }
        }

        public override Stream GetStream(int p_frame)
        {
            throw new NotImplementedException();
        }

        public override void GetMMode(int p_x_coordinate, MModeType type)
        {
            throw new NotImplementedException();
        }

        public override void GetMMode(int p_x_coordinate, int start_frame, int end_frame)
        {
            throw new NotImplementedException();
        }

        public override MMode GetMMode(int p_x_coordinate)
        {
            throw new NotImplementedException();
        }

        public override void GetPreprocess(List<Roi> rois)
        {
            throw new NotImplementedException();
        }

        public override void GetFrameTimes()
        {
            throw new NotImplementedException();
        }

        public override unsafe void GetSnapshot(string write_path)
        {
            throw new NotImplementedException();
        }

        public int error_frame = 0;
        /// <summary>
        /// Perform PreProcessing
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="cancel_token"></param>
        public unsafe override void Run(ProcessEventArgs arg, CancellationTokenSource cancel_token)
        {
            int FRAMES_TO_SKIP = Options.SkipFrames;
            int FRAMES_TO_RUN = m_total_frames - FRAMES_TO_SKIP;
            OnBegin(new EventArgs());

            CancelToken = cancel_token;

            int STATE = 0;
            bool cancelled = false;
            bool error = false;
            Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;
            Exception exc_return = null;
            Logs.LogTrace("Began Run(): [" + Thread.CurrentThread.ManagedThreadId + "] " + this.VideoPath);

            BindingList<Roi> rois = (arg.Arguments as PreprocessArguments).Rois;
            int progress = 0;
            int FRAMES = m_total_frames;
            Frame frame_a = null, frame_b = null;
            Stopwatch stopwatch = Stopwatch.StartNew();

            PreprocessData[] data_out = new PreprocessData[1];
            try
            {
                if(rois.Count == 0)
                {
                    rois.Add(new Roi(0, 0, new Size(this.Width, this.Height)));
                }

                Frame[] frame_cache = new Frame[2];

                data_out = new PreprocessData[rois.Count];
                for(int i = 0; i < data_out.Length; i++)
                {
                    data_out[i] = new PreprocessData(FRAMES - FRAMES_TO_SKIP);
                }

                Open();

                for(int n = 0; n < data_out.Length; n++)
                {
                    data_out[n].MMode.PixelValues = new double[FRAMES_TO_RUN, rois[n].Height];
                    data_out[n].MMode.XCoordinate = rois[n].X + rois[n].Width / 2;

                    m_frame_times = new double[FRAMES_TO_RUN];

                    Frame f = GetFrameSync(1 + FRAMES_TO_SKIP);
                    //GetFrame(frameObj, ref f, 1 + FRAMES_TO_SKIP);
                    Frame.Crop(ref f, (Rectangle)rois[n]);
                    frame_cache[1] = f;

                    Frame mmode_frame = GetFrameSync(1);//new Frame();
                    //GetFrame(frameObj, ref mmode_frame, 1);

                    //for(int i = 1; i <= FRAMES_TO_RUN; i++)
                    for(int i = FRAMES_TO_SKIP; i <= m_total_frames; i++)
                    {
                        STATE = i;

                        if(i >= m_total_frames)
                        {

                        }

                        #region Cancellation

                        if(cancel_token.IsCancellationRequested)
                            cancel_token.Token.ThrowIfCancellationRequested();

                        #endregion

                        frame_cache[0] = frame_cache[1];

                        if(i <= m_total_frames)
                        {
                            f = GetFrameSync(i);//new Frame();
                            //GetFrame(frameObj, ref f, i);
                            Frame.Crop(ref f, (Rectangle)rois[n]);
                            frame_cache[1] = f;
                        }

                        if(i > FRAMES_TO_SKIP)
                        {
                            mmode_frame = GetFrameSync(i); // new Frame();
                            //GetFrame(frameObj, ref mmode_frame, i);
                        }

                        if(i < m_total_frames)
                        {
                            for(int y = 0; y < data_out[n].MMode.Size.Height; y++)
                            {
                                data_out[n].MMode[i - FRAMES_TO_SKIP, y] = mmode_frame[data_out[n].MMode.XCoordinate, y];
                            }
                        }

                        //Frame f = new Frame();

                        //double std = frame_cache.Current.Mean();
                        //double std_smooth = frame_cache.Current.Mean();

                        //double area = frame_cache.Current.Values.Length;
                        double angle = 0;
                        double sum_bright = 0;
                        double max_relative_change = 0;
                        double sum_intensity = 0;
                        double sum_phase = 0;
                        for(int j = 0; j < frame_cache[0].PixelValues.GetLength(0); j++)
                        {
                            for(int k = 0; k < frame_cache[0].PixelValues.GetLength(1); k++)
                            {
                                #region FrameBrightness
                                sum_bright += -frame_cache[0][j, k];
                                #endregion

                                #region 2nd Order Functions
                                if(i < m_total_frames)
                                {
                                    #region PixelIntensityChange

                                    double delta = 0;
                                    delta += Math.Abs(frame_cache[1][j, k] - frame_cache[0][j, k]);
                                    sum_intensity += delta;

                                    #endregion
                                }
                                #endregion
                            }
                        }

                        m_frame_times[i - FRAMES_TO_SKIP] = (i - FRAMES_TO_SKIP) / (double)this.m_fps;
                        data_out[n].Darkness[i - FRAMES_TO_SKIP] = (double)(sum_bright);
                        if(i < m_total_frames - 1)
                        {
                            data_out[n].Intensity[i - FRAMES_TO_SKIP] = (double)(sum_intensity);
                        }

                        //Send Progress
                        int new_progress = i * 100 / FRAMES_TO_RUN;
                        if(progress < new_progress)
                        {
                            progress = new_progress;
                            //OnProgressUpdate(this, progress / data_out.Length);
                            OnUpdate(new AsyncCompletedEventArgs(null, false, progress));
                        }
                    }

                    #region Eliminate Intensity +/- 2 Std Dev

                    //data_out[n].Intensity = ArrayFunctions.PlusMinusWindow(data_out[n].Intensity, 3);

                    #endregion

                }
            }
            catch(AggregateException agg_exc)
            {

            }
            catch(OperationCanceledException op_cancel_exc)
            {
                cancelled = true;
                data_out = new PreprocessData[1];
                error = false;
                exc_return = op_cancel_exc;
            }
            catch(Exception exc)
            {
                Logs.LogException(exc);
                exc_return = exc;
                error = true;
            }


            Logs.LogTrace("[" + Thread.CurrentThread.ManagedThreadId + "] Processing took " + stopwatch.ElapsedMilliseconds
                    + " milliseconds. [" + m_total_frames + " Frames, " + m_total_frames * rois[0].Area() / stopwatch.ElapsedMilliseconds * 1000 + " pps]");


            OnEnd(new AsyncCompletedEventArgs(null, false, data_out));
            //base.OnEnd(new ProgressEventArgs(data_out, false, false, null));

        }

        public override void TestRun(ProcessEventArgs arg, CancellationTokenSource cancel_token)
        {
            throw new NotImplementedException();
        }

        public override void PreAnalyze(ProcessEventArgs args, CancellationTokenSource cancel_token)
        {
            throw new NotImplementedException();
        }
        
        #endregion

    }
}
