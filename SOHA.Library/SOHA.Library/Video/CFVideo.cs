﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenMcdf;
using System.IO;
using System.Runtime.InteropServices.ComTypes;

namespace SOHA.Library.Video
{
    public class CFVideo : Video
    {

        private CompoundFile cf;

        
        public CFVideo() { }

        public CFVideo(string path)
        {
            m_video_path = path;
            Open();
        }

        public override void Open()
        {
            if(m_opened)
            {
                //throw exception, shouldn't be trying to open a lot of times
                return;
            }

            cf = new CompoundFile(m_video_path, CFSUpdateMode.Update, CFSConfiguration.SectorRecycle | CFSConfiguration.NoValidationException | CFSConfiguration.EraseFreeSectors);

            CFStorage all_frames = cf.RootStorage.GetStorage("Field Data");
            
            int increment = 10000;
            int i = 1;
            bool done = false;
            while(!done)
            {
                try
                {
                    CFStorage field = all_frames.GetStorage("Field " + i);
                }
                catch(Exception exc)
                {
                    i -= increment;

                    if(increment != 1)
                    {
                        increment /= 10;
                    }
                    else
                    {
                        m_total_frames = i;
                        done = true;
                    }
                }
                finally
                {
                    i += increment;
                }
            }

            CFStream s_width = cf.RootStorage.GetStorage("Field Data").GetStorage("Field 1").GetStorage("i_Image1").GetStorage("Details").GetStream("Image_Width");
            byte[] b_width = s_width.GetData();
            double width = BitConverter.ToDouble(b_width, 0);
            m_frame_width = (int)width;

            CFStream s_height = cf.RootStorage.GetStorage("Field Data").GetStorage("Field 1").GetStorage("i_Image1").GetStorage("Details").GetStream("Image_Height");
            byte[] b_height = s_height.GetData();
            double height = BitConverter.ToDouble(b_height, 0);
            m_frame_height = (int)height;

            m_opened = true;
        }

        public override void Close()
        {
            cf.Close();

            m_opened = false;
        }

        public override Frame GetFrameSync(int p_frame)
        {
            if(!m_opened)
                throw new Exception("Video not opened.");

            double time = 0;

            CFStorage cfDetails = cf.RootStorage.GetStorage("Field Data").GetStorage("Field " + p_frame).GetStorage("i_Image1").GetStorage("Details");
            CFStream cfBitmap = cf.RootStorage.GetStorage("Field Data").GetStorage("Field " + p_frame).GetStorage("i_Image1").GetStream("Bitmap 1");


            Frame fr = new Frame(m_frame_width, m_frame_height, time, cfBitmap.GetData(), p_frame);

            return fr;
        }

        public override System.Drawing.Bitmap GetBitmap(int p_frame)
        {
            return null;
            //throw new NotImplementedException();
        }

        public override void GetFrame(int p_frame)
        {
            //throw new NotImplementedException();
        }

        public override Stream GetStream(int p_frame)
        {
            return null;
            //throw new NotImplementedException();
        }

        public override void GetMMode(int p_x_coordinate, MModeType type)
        {
            //throw new NotImplementedException();
        }

        public override void GetMMode(int p_x_coordinate, int start_frame, int end_frame)
        {
            //throw new NotImplementedException();
        }

        public override MMode GetMMode(int p_x_coordinate)
        {
            return null;
            //throw new NotImplementedException();
        }

        public override void GetPreprocess(List<Roi> rois)
        {
            //throw new NotImplementedException();
        }

        public override void GetFrameTimes()
        {
            //throw new NotImplementedException();
        }

        public override unsafe void GetSnapshot(string write_path)
        {
            //throw new NotImplementedException();
        }

        public override void Run(ProcessEventArgs arg, System.Threading.CancellationTokenSource cancel_token)
        {
            //throw new NotImplementedException();
        }

        public override void TestRun(ProcessEventArgs arg, System.Threading.CancellationTokenSource cancel_token)
        {
            //throw new NotImplementedException();
        }

        public override void PreAnalyze(ProcessEventArgs args, System.Threading.CancellationTokenSource cancel_token)
        {
            //throw new NotImplementedException();
        }
    }
}
