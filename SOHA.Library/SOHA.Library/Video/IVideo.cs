﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SOHA.Library;
using System.IO;
using System.Threading;

namespace SOHA.Library.Video
{
    public interface IVideo
    {
        void Open();
        void Close();
        Bitmap GetBitmap(int p_frame);
        
        void GetFrame(int p_frame);
        Frame GetFrameSync(int p_frame);
        Stream GetStream(int p_frame);

        void GetMMode(int p_x, MModeType type);
        void GetMMode(int p_x, int start, int end);
        MMode GetMMode(int p_x);

        void GetPreprocess(List<Roi> rois);
        void GetFrameTimes();

        /// <summary>
        /// Pre-Analyzes the Video File.
        /// Retrieves:
        ///     -Total Frames
        ///     -Frame Dimensions
        ///     -Frame Resolution
        ///     -Highest Pixel Value
        ///     -Lowest Pixel Value
        /// </summary>
        void PreAnalyze(ProcessEventArgs args, CancellationTokenSource cancel_token);
    }
}
