﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using SOHA.Library.Xml;

namespace SOHA.Library.Video
{
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class AVIBMPHeader
    {
        #region FIelds/Properties

        private SOHA.Library.Video.AVIVideo.BITMAPINFOHEADER m_bmp_info;

        private UInt32 biSize;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 BiSize
        {
            get { return m_bmp_info.biSize; }
            set { biSize = value; }
        }
        private Int32 biWidth;

        [Browsable(true)]
        [ReadOnly(true)]
        public Int32 BiWidth
        {
            get { return m_bmp_info.biWidth; }
            set { biWidth = value; }
        }
        private Int32 biHeight;

        [Browsable(true)]
        [ReadOnly(true)]
        public Int32 BiHeight
        {
            get { return m_bmp_info.biHeight; }
            set { biHeight = value; }
        }
        private Int16 biPlanes;

        [Browsable(true)]
        [ReadOnly(true)]
        public Int16 BiPlanes
        {
            get { return m_bmp_info.biPlanes; }
            set { biPlanes = value; }
        }
        private Int16 biBitCount;

        [Browsable(true)]
        [ReadOnly(true)]
        public Int16 BiBitCount
        {
            get { return m_bmp_info.biBitCount; }
            set { biBitCount = value; }
        }
        private UInt32 biCompression;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 BiCompression
        {
            get { return m_bmp_info.biCompression; }
            set { biCompression = value; }
        }
        private UInt32 biSizeImage;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 BiSizeImage
        {
            get { return m_bmp_info.biSizeImage; }
            set { biSizeImage = value; }
        }
        private Int32 biXPelsPerMeter;

        [Browsable(true)]
        [ReadOnly(true)]
        public Int32 BiXPelsPerMeter
        {
            get { return m_bmp_info.biXPelsPerMeter; }
            set { biXPelsPerMeter = value; }
        }
        private Int32 biYPelsPerMeter;

        [Browsable(true)]
        [ReadOnly(true)]
        public Int32 BiYPelsPerMeter
        {
            get { return m_bmp_info.biYPelsPerMeter; }
            set { biYPelsPerMeter = value; }
        }
        private UInt32 biClrUsed;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 BiClrUsed
        {
            get { return m_bmp_info.biClrUsed; }
            set { biClrUsed = value; }
        }
        private UInt32 biClrImportant;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 BiClrImportant
        {
            get { return m_bmp_info.biClrImportant; }
            set { biClrImportant = value; }
        }

        #endregion Fields/Properties

        public AVIBMPHeader(SOHA.Library.Video.AVIVideo.BITMAPINFOHEADER header)
        {
            m_bmp_info = header;
        }
    }
}
