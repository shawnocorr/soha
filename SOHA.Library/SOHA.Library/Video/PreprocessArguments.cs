﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using SOHA.Library.GPU;
using SOHA.Library.Mathematics;
using SOHA.Library.Imaging;

namespace SOHA.Library.Video
{
    public class PreprocessArguments
    {

        public bool EnableFilters
        {
            get;
            private set;
        }

        public BindingList<Roi> Rois
        {
            get;
            private set;
        }
        public List<IFrameFunction> Filters
        {
            get;
            private set;
        }

        public PreprocessArguments(BindingList<Roi> rois, List<IFrameFunction> filters)
        {
            Rois = rois;
            Filters = filters;
            EnableFilters = true;
        }

        public PreprocessArguments(BindingList<Roi> rois, List<IFrameFunction> filters, bool enable_filters)
        {
            Rois = rois;
            Filters = filters;
            EnableFilters = enable_filters;
        }

    }
}
