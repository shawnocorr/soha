﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Mathematics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;

namespace SOHA.Library.Video
{
    public abstract class BFrame : IFrame
    {

        protected Matrix m_values;
        protected double m_max;

        protected bool m_recalculate_max = true;
        protected bool m_is_blank = false;

        public int Width
        {
            get { return m_values.Rows; }
        }
        public int Height
        {
            get { return m_values.Columns; }
        }
        public double Max
        {
            get
            {
                if (m_recalculate_max)
                    CalculateMax();

                return m_max;
            }
        }
        public Matrix Values
        {
            get
            {
                return m_values;
            }
            set
            {
                m_values = value;
                CalculateMax();
            }
        }
        public double this[int x, int y]
        {
            get
            {
                return m_values[x, y];
            }
            set
            {
                m_values[x, y] = value;
                m_recalculate_max = true;
            }
        }
        public Size Size
        {
            get
            {
                return new Size(Width, Height);
            }
        }

        public bool IsBlank
        {
            get { return m_is_blank; }
        }
        public void CalculateMax()
        {
            m_max = 0;
            for (int i = 0; i < m_values.Rows; i++)
            {
                for (int j = 0; j < m_values.Columns; j++)
                {
                    if (m_values[i, j] > m_max)
                        m_max = m_values[i, j];
                }
            }
            m_recalculate_max = false;
        }

        #region Explicit Cast Operators

        public static explicit operator Bitmap(BFrame frame)
        {
            if (frame == null)
                throw new Exception("Frame object is null.");

            if (frame.Values == null)
                throw new Exception("Frame object Values are null.");

            Bitmap bmp = new Bitmap(frame.Width, frame.Height, PixelFormat.Format24bppRgb);

            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            double m = frame.Max;

            for (int y = 0; y < bmpdata.Height; y++)
            {
                unsafe
                {
                    byte* row = (byte*)bmpdata.Scan0 + (y * bmpdata.Stride);
                    for (int x = 0; x < bmpdata.Width; x++)
                    {
                        byte b = (byte)(int)(frame[x, y] / m * 255);
                        row[x * 3] = (byte)(int)(frame[x, y] / m * 255);
                        row[x * 3 + 1] = (byte)(int)(frame[x, y] / m * 255);
                        row[x * 3 + 2] = (byte)(int)(frame[x, y] / m * 255);
                    }
                }
            }

            bmp.UnlockBits(bmpdata);

            return bmp;
        }

        public static explicit operator Matrix(BFrame frame)
        {
            return frame.Values;
        }

        public static explicit operator MemoryStream(BFrame frame)
        {
            MemoryStream mem_stream = new MemoryStream();
            foreach (double d in frame.m_values)
            {
                Int16 val = (Int16)d;
                byte[] byte_array = BitConverter.GetBytes(val);
                mem_stream.Write(byte_array, 0, byte_array.Length);
            }
            return mem_stream;
        }

        #endregion
    }
}
