﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using iop = System.Runtime.InteropServices;

namespace SOHA.Library.Video
{
    public class ComStreamWrapper : Stream
    {
        private IStream m_stream;
        private IntPtr m_ptr;

        public readonly byte[] Buffer;

        public ComStreamWrapper(IStream source)
        {
            m_stream = source;
            m_ptr = iop.Marshal.AllocCoTaskMem(8);
            Buffer = new byte[Length];
            Read(Buffer, 0, Buffer.Length);
        }

        ~ComStreamWrapper()
        {
            iop.Marshal.FreeCoTaskMem(m_ptr);
        }

        public override bool CanRead
        {
            get { return true; }
        }
        public override bool CanSeek
        {
            get { return true; }
        }
        public override bool CanWrite
        {
            get { return true; }
        }

        public override void Flush()
        {
            m_stream.Commit(0);
        }

        public override long Length
        {
            get 
            {
                STATSTG stat;
                m_stream.Stat(out stat, 1);
                return stat.cbSize;
            }
        }

        public override long Position
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if(offset != 0) 
                throw new NotImplementedException();

            m_stream.Read(buffer, count, m_ptr);
            return iop.Marshal.ReadInt32(m_ptr);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            m_stream.Seek(offset, (int)origin, m_ptr);
            return iop.Marshal.ReadInt64(m_ptr);
        }

        public override void SetLength(long value)
        {
            m_stream.SetSize(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if(offset != 0) 
                throw new NotImplementedException();

            m_stream.Write(buffer, count, IntPtr.Zero);
        }

        public ushort[] ToArray()
        {
            ushort[] array = new ushort[Length * 2];



            return array;
        }
    }
}
