﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using SOHA.Library.Xml;
using System.Runtime.InteropServices;

namespace SOHA.Library.Video
{
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class AviStreamInfo
    {
        private SOHA.Library.Video.AVIVideo.AVISTREAMINFO m_avi_info;

        private UInt32 fccType;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 FccType
        {
            get { return m_avi_info.fccType; }
            set { fccType = value; }
        }
        private UInt32 fccHandler;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 FccHandler
        {
            get { return m_avi_info.fccHandler; }
            set { fccHandler = value; }
        }
        private UInt32 dwFlags;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwFlags
        {
            get { return m_avi_info.dwFlags; }
            set { dwFlags = value; }
        }
        private UInt32 dwCaps;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwCaps
        {
            get { return m_avi_info.dwCaps; }
            set { dwCaps = value; }
        }
        private UInt16 wPriority;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt16 WPriority
        {
            get { return m_avi_info.wPriority; }
            set { wPriority = value; }
        }
        private UInt16 wLanguage;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt16 WLanguage
        {
            get { return m_avi_info.wLanguage; }
            set { wLanguage = value; }
        }
        private UInt32 dwScale;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwScale
        {
            get { return m_avi_info.dwScale; }
            set { dwScale = value; }
        }
        private UInt32 dwRate;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwRate
        {
            get { return m_avi_info.dwRate; }
            set { dwRate = value; }
        }
        private UInt32 dwStart;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwStart
        {
            get { return m_avi_info.dwStart; }
            set { dwStart = value; }
        }
        private UInt32 dwLength;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwLength
        {
            get { return m_avi_info.dwLength; }
            set { dwLength = value; }
        }
        private UInt32 dwInitialFrames;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwInitialFrames
        {
            get { return m_avi_info.dwInitialFrames; }
            set { dwInitialFrames = value; }
        }
        private UInt32 dwSuggestedBufferSize;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwSuggestedBufferSize
        {
            get { return m_avi_info.dwSuggestedBufferSize; }
            set { dwSuggestedBufferSize = value; }
        }
        private UInt32 dwQuality;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwQuality
        {
            get { return m_avi_info.dwQuality; }
            set { dwQuality = value; }
        }
        private UInt32 dwSampleSize;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwSampleSize
        {
            get { return m_avi_info.dwSampleSize; }
            set { dwSampleSize = value; }
        }
        private SOHA.Library.Video.AVIVideo.RECT rcFrame;

        [Browsable(true)]
        [ReadOnly(true)]
        public SOHA.Library.Video.AVIVideo.RECT RcFrame
        {
            get { return m_avi_info.rcFrame; }
            set { rcFrame = value; }
        }
        private UInt32 dwEditCount;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwEditCount
        {
            get { return m_avi_info.dwEditCount; }
            set { dwEditCount = value; }
        }
        private UInt32 dwFormatChangeCount;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt32 DwFormatChangeCount
        {
            get { return m_avi_info.dwFormatChangeCount; }
            set { dwFormatChangeCount = value; }
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public UInt16[] szName;

        [Browsable(true)]
        [ReadOnly(true)]
        public UInt16[] SzName
        {
            get { return m_avi_info.szName; }
            set { szName = value; }
        }

        public AviStreamInfo(SOHA.Library.Video.AVIVideo.AVISTREAMINFO header)
        {
            m_avi_info = header;
        }
    }
}
