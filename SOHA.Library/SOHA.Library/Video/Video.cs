﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using System.Drawing;
using System.Threading;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using SOHA.Library.Xml;

namespace SOHA.Library.Video
{
    public delegate void MModeEventHandler(object sender, MMode mmode);
    public delegate void FrameEventHandler(object sender, Frame frame);
    public delegate void PreprocessEventHandler(object sender, PreprocessData[] ppData);
    public delegate void PreprocessProgressHandler(Video sender, int progress);
    public delegate void VideoOpenEventHandler(object sender, bool error);
    public delegate void VideoCloseEventHandler(object sender, bool error);

    public enum VideoFileType
    {
        CXD = 0,
        AVI
    }

    public enum MModeType
    {
        Snapshot,
        Full
    }

    public enum STREAM_SEEK : int
    {
        STREAM_SEEK_SET = 0,
        STREAM_SEEK_CUR = 1,
        STREAM_SEEK_END = 2
    }

    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public abstract class Video : IXmlSerializable, IVideo, IProcessable, IDisposable
    {
        //public static int FRAMES_TO_SKIP = 10;


        public Frame this[int i]
        {
            get
            {
                if(!PreAnalyzed)
                    throw new FrameException("Video has not been pre-analyzed.");

                return m_frames[i];
            }
        }

        protected VideoProperties m_properties;
        protected IntPtr ptrFrame;
        protected List<Frame> m_frames;

        //Deafult FALSE
        protected bool PreAnalyzed { get; set; }

        protected SOHA.Library.Video.AVIVideo.BITMAPINFOHEADER m_bmp_info;
        protected SOHA.Library.Video.AVIVideo.AVISTREAMINFO m_stream_info;
        protected Camera m_camera;
        protected string m_video_path;
        protected int m_fps;
        protected int m_total_frames;
        protected int m_frame_width;
        protected int m_frame_height;
        protected double[] m_frame_times;
        protected bool m_opened;
        protected bool m_cancel_process = false;

        [Browsable(true)]
        [ReadOnly(true)]
        public AVIBMPHeader InfoHeader
        {
            get
            {
                return new AVIBMPHeader(m_bmp_info);
            }
            set
            {
                //m_bmp_info = value;
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public AviStreamInfo AviInfoHeader
        {
            get
            {
                return new AviStreamInfo(m_stream_info);
            }
            set
            {
                //m_bmp_info = value;
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public Camera Camera
        {
            get
            {
                return m_camera;
            }
            set
            {
                m_camera = value;
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor),
            typeof(System.Drawing.Design.UITypeEditor))]
        public string VideoPath 
        { 
            get 
            { 
                return m_video_path; 
            }
            set
            {
                m_video_path = value;
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public int Fps 
        { 
            get 
            { 
                return m_fps; 
            }
            set
            {
                m_fps = value;
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public int Frames 
        { 
            get 
            { 
                return m_total_frames; 
            }
            set
            {
                m_total_frames = value;
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public int Width 
        { 
            get 
            { 
                return m_frame_width; 
            }
            set
            {
                m_frame_width = value;
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public int Height 
        { 
            get 
            { 
                return m_frame_height; 
            }
            set
            {
                m_frame_height = value;
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public double[] FrameTimes
        {
            get
            {
                return m_frame_times;
            }
            set
            {
                m_frame_times = value;
            }
        }

        public double TotalTime
        {
            get
            {
                if(FrameTimes != null)
                    return FrameTimes[FrameTimes.Length - 1];

                return 0;
            }
        }
        public string Name
        {
            get
            {
                return Path.GetFileNameWithoutExtension(m_video_path);
            }
        }
        public bool IsVideoPresent
        {
            get
            {
                if(String.IsNullOrWhiteSpace(m_video_path))
                    return false;

                return File.Exists(m_video_path);
            }
        }

        public event MModeEventHandler MModeComplete;
        public event FrameEventHandler FrameComplete;
        public event PreprocessEventHandler PreprocessComplete;
        public event PreprocessProgressHandler ProgressUpdate;
        public event VideoOpenEventHandler VideoOpenComplete;
        public event VideoCloseEventHandler VideoCloseComplete;
        public event EventHandler BitRateUpdate;

        public Video() { }

        public void CancelProcess()
        {
            m_cancel_process = true;
        }

        public bool IsFrameTimesValid()
        {
            if (m_frame_times == null ||
                m_frame_times.Count() != m_total_frames)
                return false;

            for (int i = 1; i < m_frame_times.Count(); i++)
            {
                if (m_frame_times[i] <= m_frame_times[i - 1])
                    return false;
            }

            return true;
        }

        public abstract void Open();
        public abstract void Close();
        public abstract Bitmap GetBitmap(int p_frame);
        public abstract void GetFrame(int p_frame);

        /// <summary>
        /// Gets a frame synchronously. Used in MarkPhase.
        /// 
        /// Benchmarks:
        ///     -Avg 4.5 ms
        ///     -Med 2 ms
        ///     -SD 5.6 ms
        ///     
        /// Using mean; max FPS 222
        /// </summary>
        /// <param name="p_frame"></param>
        /// <returns></returns>
        public abstract Frame GetFrameSync(int p_frame);
        public abstract Stream GetStream(int p_frame);
        public abstract void GetMMode(int p_x_coordinate, MModeType type);
        public abstract void GetMMode(int p_x_coordinate, int start_frame, int end_frame);
        public abstract MMode GetMMode(int p_x_coordinate);
        public abstract void GetPreprocess(List<Roi> rois);
        public abstract void GetFrameTimes();

        /// <summary>
        /// Writes a 100 frame video snapshot to the specified path.
        /// </summary>
        public abstract unsafe void GetSnapshot(string write_path);

        protected virtual void OnMModeComplete(object sender, MMode mmode)
        {
            if (MModeComplete != null)
                MModeComplete(sender, mmode);
        }

        protected virtual void OnFrameComplete(object sender, Frame frame)
        {
            if (FrameComplete != null)
                FrameComplete(sender, frame);
        }

        protected virtual void OnPreprocessComplete(object sender, PreprocessData[] ppData)
        {
            if (PreprocessComplete != null)
                PreprocessComplete(sender, ppData);
        }

        protected virtual void OnVideoOpenComplete(object sender, bool error)
        {
            if (VideoOpenComplete != null)
                VideoOpenComplete(sender, error);
        }

        protected virtual void OnVideoCloseComplete(object sender, bool error)
        {
            if (VideoCloseComplete != null)
                VideoCloseComplete(sender, error);
        }

        protected virtual void OnBitRateUpdate(int bits, TimeSpan time)
        {
            double bps = bits / time.TotalSeconds;
        }
        
        protected void OnProgressUpdate(Video sender, int progress)
        {
            if (ProgressUpdate != null)
                ProgressUpdate(sender, progress);
        }

        #region IXmlSerializable Members

        public virtual XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            try
            {
                //reader.Read();
                m_total_frames = Int32.Parse(reader.GetAttribute("Frames"));
                m_frame_height = Int32.Parse(reader.GetAttribute("Height"));
                m_frame_width = Int32.Parse(reader.GetAttribute("Width"));
                m_fps = Int32.Parse(reader.GetAttribute("Fps"));
                m_video_path = reader.GetAttribute("Path");

                reader.ReadToDescendant("Camera");
                XmlSerializer serializer = new XmlSerializer(typeof(Camera));
                m_camera = (Camera)serializer.Deserialize(reader);

                //reader.Read();
                serializer = new XmlSerializer(typeof(double[]));
                m_frame_times = (double[])serializer.Deserialize(reader);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Frames", m_total_frames.ToString());
            writer.WriteAttributeString("Height", m_frame_height.ToString());
            writer.WriteAttributeString("Width", m_frame_width.ToString());
            writer.WriteAttributeString("Fps", m_fps.ToString());
            writer.WriteAttributeString("Path", m_video_path.ToString());

            XmlSerializer serializer = new XmlSerializer(typeof(Camera));
            serializer.Serialize(writer, m_camera);

            serializer = new XmlSerializer(typeof(double[]));
            serializer.Serialize(writer, m_frame_times);
        }

        #endregion
        
        #region IProcess

        [Browsable(false)]
        public CancellationTokenSource CancelToken
        {
            get;
            protected set;
        }

        public abstract void Run(ProcessEventArgs arg, CancellationTokenSource cancel_token);
        public abstract void TestRun(ProcessEventArgs arg, CancellationTokenSource cancel_token);

        public event EventHandler BeginProcess;
        public event AsyncEventHandler UpdateProcess;
        public event AsyncEventHandler EndProcess;

        public void OnBegin(EventArgs e)
        {
            if (BeginProcess != null)
                BeginProcess(this, e);
        }

        public void OnUpdate(AsyncCompletedEventArgs e)
        {
            if (UpdateProcess != null)
                UpdateProcess(this, e);
        }

        public void OnEnd(AsyncCompletedEventArgs e)
        {
            if (EndProcess != null)
                EndProcess(this, e);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(disposing)
            {
                Close();
            }
        }

        #endregion

        #region IVideo Members

        public VideoProperties Properties
        {
            get
            {
                return m_properties;
            }
            private set
            {
                m_properties = value;
            }
        }

        public abstract void PreAnalyze(ProcessEventArgs args, CancellationTokenSource cancel_token);

        #endregion
    }

    [Serializable]
    public class VideoFileNotFoundException : Exception
    {
        public VideoFileNotFoundException() { }
        public VideoFileNotFoundException(string message) : base(message) { }
        public VideoFileNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected VideoFileNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class VideoFileOpenException : Exception
    {
        public VideoFileOpenException() { }
        public VideoFileOpenException(string message) : base(message) { }
        public VideoFileOpenException(string message, Exception inner) : base(message, inner) { }
        protected VideoFileOpenException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
