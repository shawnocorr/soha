﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Xml;
using SOHA.Library.Forms;
using WeifenLuo.WinFormsUI.Docking;


namespace SOHA.Library.Forms
{
    public enum SohaPhaseResult
    {
        Accept,
        Cancel,
        Reject
    }

    public partial class frmPhase : DockContent, IPhase
    {

        protected PhaseName PHASE;
        protected PhaseFormType m_phase_type = PhaseFormType.New;
        protected PhaseOutput m_output;
        protected PhaseData m_data;
        protected PhaseOptions m_options;

        protected Filegram m_filegram;
        protected bool m_video_present;
        protected bool m_initialized = false;

        public PhaseFormType Type
        {
            get
            {
                return m_phase_type;
            }
        }

        public frmPhase()
        {
            InitializeComponent();
        }

        public frmPhase(Filegram fg)
        {
            InitializeComponent();

            m_filegram = fg;
            m_filegram.Status = FilegramStatus.UserInterface;
            IsLocked = !fg.Video.IsVideoPresent;

            if(IsLocked)
            {
                string msg = "There is no CXD or AVI video file attached to this record.\n\n" +
                    "This phase will be \"locked\" and you be unable to make changes.\n";

                MessageBox.Show(msg, "Record Locked");
            }

            this.FormClosing += new FormClosingEventHandler(frm_FormClosing);
        }

        void Button_Click(object sender, EventArgs e)
        {
            OnComplete(sender, new PhaseCompleteEventArgs(AcceptButton.Equals(sender)));
        }

        /*
        public static frmPhase SpawnForm(Filegram f, PhaseFormType type)
        {
            #region Spawn Form

            Logs.LogTrace("Loading Phase [" + type.ToString() + "] Filegram [" + f.Name + "]");

            frmPhase frm = new frmPhase();
            switch (type)
            {
                case PhaseFormType.Mark:
                    frm = frmMark.Spawn(f);
                    break;

                case PhaseFormType.Interval:
                    frm = frmIntervalNew.Spawn(f);
                    break;

                case PhaseFormType.Phase:
                    frm = frmPhasePlusNew.Spawn(f);
                    break;

                case PhaseFormType.MModeSelect:
                    //frm = frmMModeSelect.Spawn(f);
                    frm = frmMmodeWorkBench.Spawn(f);
                    break;

                case PhaseFormType.RedDot:
                    frm = frmRedDotMovie.Spawn(f);
                    break;

                case PhaseFormType.Properties:
                    mdiMain.ShowForm(new frmFilegramProperties(f));
                    break;

                case PhaseFormType.WorkBench:
                    frm = frmWorkBench.Spawn(f);
                    break;

                default:
                    frm = null;
                    break;
            }

            #endregion

            frm.m_filegram = f;
            frm.FormClosing += new FormClosingEventHandler(frm_FormClosing);

            //Check For Video Present

            //f.Cxd.needed = true;
            return frm;
        }*/

        private void frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_filegram.Status = FilegramStatus.Idle;
            //frmPhase frm_phase = (sender as frmPhase);

            //if(frm_phase.PhaseComplete != null)
            //    frm_phase.PhaseComplete(sender, new PhaseCompleteEventArgs(false));
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Space)
            {
                OnComplete(this, new PhaseCompleteEventArgs(true, m_phase_type, m_output));
                e.Handled = true;
            }
            else if(e.KeyCode == Keys.Escape)
            {
                OnComplete(this, new PhaseCompleteEventArgs(false, m_phase_type, m_output));
                e.Handled = true;
            }

            base.OnKeyDown(e);
        }

        protected void OnButtonChanged(SohaPhaseResult result)
        {
            switch(result)
            {
                case SohaPhaseResult.Accept:

                    if(AcceptButton != null)
                        (AcceptButton as Button).Click += new EventHandler(Button_Click);
                    break;
                case SohaPhaseResult.Cancel:

                    if(CancelButton != null)
                        (CancelButton as Button).Click += new EventHandler(Button_Click);
                    break;
                case SohaPhaseResult.Reject:

                    if(RejectButton != null)
                        (RejectButton as Button).Click += new EventHandler(Button_Click);
                    break;
                default:
                    break;
            }
        }


        #region IPhase Members

        public event PhaseCompleteEventHandler PhaseBegin;
        public event PhaseCompleteEventHandler PhaseComplete;
        
        private IButtonControl m_accept, m_cancel, m_reject;

        public bool IsLocked { get; set; }

        [DefaultValue("")]
        new public IButtonControl AcceptButton
        {
            get
            {
                return m_accept;
            }
            set
            {
                m_accept = value;
                OnButtonChanged(SohaPhaseResult.Accept);
            }
        }
        [DefaultValue("")]
        new public IButtonControl CancelButton
        {
            get
            {
                return m_cancel;
            }
            set
            {
                m_cancel = value;
                OnButtonChanged(SohaPhaseResult.Cancel);
            }
        }
        [DefaultValue("")]
        public IButtonControl RejectButton
        {
            get
            {
                return m_reject;
            }
            set
            {
                m_reject = value;
                OnButtonChanged(SohaPhaseResult.Reject);
            }
        }

        void IPhase.OnComplete(object sender, PhaseCompleteEventArgs args)
        {
            if(PhaseComplete != null)
                PhaseComplete(sender, args);
        }

        void IPhase.OnPhaseBegin(object sender, PhaseCompleteEventArgs args)
        {
            if(PhaseBegin != null)
                PhaseBegin(this, args);
        }

        protected virtual void OnComplete(object sender, PhaseCompleteEventArgs args)
        {
            if(PhaseComplete != null)
                PhaseComplete(sender, args);
        }

        protected virtual void OnPhaseBegin(object sender, PhaseCompleteEventArgs args)
        {
            if(PhaseBegin != null)
                PhaseBegin(this, args);
        }

        #endregion
    }

    [global::System.Serializable]
    public class PhaseInitiationException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public PhaseInitiationException() { }
        public PhaseInitiationException(string message) : base(message) { }
        public PhaseInitiationException(string message, Exception inner) : base(message, inner) { }
        protected PhaseInitiationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
