﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHA.Library.Forms
{
    public delegate void PhaseCompleteEventHandler(object sender, PhaseCompleteEventArgs e);

    public interface IPhase
    {
        event PhaseCompleteEventHandler PhaseBegin;
        event PhaseCompleteEventHandler PhaseComplete;

        IButtonControl AcceptButton { get; set; }
        IButtonControl RejectButton { get; set; }
        IButtonControl CancelButton { get; set; }

        void OnPhaseBegin(object sender, PhaseCompleteEventArgs args);
        void OnComplete(object sender, PhaseCompleteEventArgs args);
    }
}
