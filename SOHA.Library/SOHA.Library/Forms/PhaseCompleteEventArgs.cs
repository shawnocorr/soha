﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Xml;

namespace SOHA.Library.Forms
{

    

    public class PhaseCompleteEventArgs : EventArgs
    {
        protected bool m_accept;
        protected PhaseFormType m_phase;
        protected PhaseOutput m_output;

        public bool Accept
        {
            get
            {
                return m_accept;
            }
        }
        public PhaseFormType Type
        {
            get
            {
                return m_phase;
            }
            set
            {
                m_phase = value;
            }
        }
        public PhaseOutput Output
        {
            get
            {
                return m_output;
            }
            set
            {
                m_output = value;
            }
        }

        public PhaseCompleteEventArgs(bool accept) :
            base()
        {
            m_accept = accept;
        }

        public PhaseCompleteEventArgs(bool accept, PhaseFormType phase, PhaseOutput output) :
            base()
        {
            m_accept = accept;
            m_phase = phase;
            m_output = output;
        }

    }
}
