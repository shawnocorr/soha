﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHA.Library
{
    public abstract class SohaObject : Object, ISohaObject, INotifyPropertyChanged, ISohaPropertyDescriptor
    {

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        #region ISohaPropertyDescriptor Members

        [Browsable(false)]
        public abstract string DisplayName { get; }

        [Browsable(false)]
        public abstract string Description { get; }

        #endregion
    }
}
