﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using SOHA.Library.Xml;

namespace SOHA.Library
{
    #region Enums

    public enum BeatType
    {
        Systole = 0,
        Diastole,
    }

    public enum OrientationType
    {
        Vertical = 0,
        Horizontal,
        Diagonal
    }

    public enum MeasurementType
    {
        Standard = 0,
        Diagonal
    }
    #endregion

    [Serializable]
    public class MarkPair : SohaObject
    {
        private Point[] m_marks = new Point[2];
        private BeatType m_beat = BeatType.Systole;
        private OrientationType m_orientation = OrientationType.Vertical;
        private bool m_highlight = false;
        
        [Browsable(false)]
        public override string DisplayName
        {
            get { return "MarkPair"; }
        }

        [Browsable(false)]
        public override string Description
        {
            get { return "A pair of marks defining a distance at a critical time in the heart operation."; }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public Point Mark1
        {
            get
            {
                return m_marks[0];
            }
            set
            {
                m_marks[0] = value;
                OnPropertyChanged("Mark1");
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public Point Mark2
        {
            get
            {
                return m_marks[1];
            }
            set
            {
                m_marks[1] = value;
                OnPropertyChanged("Mark2");
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public BeatType Beat
        {
            get
            {
                return m_beat;
            }
            set
            {
                m_beat = value;
                OnPropertyChanged("Beat");
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public OrientationType Orientation
        {
            get
            {
                return m_orientation;
            }
            set
            {
                if (value != OrientationType.Diagonal && value != OrientationType.Horizontal && value != OrientationType.Vertical)
                    throw new Exception("InvalidValue");
                m_orientation = value;
                OnPropertyChanged("Orientation");
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        public bool Highlight
        {
            get
            {
                return m_highlight;
            }
            set
            {
                m_highlight = value;
            }
        }

        public override string ToString()
        {
            return Beat.ToString() + "; (" + Mark1.ToString() + ":" + Mark2.ToString();
        }

        public MarkPair() { }

        public MarkPair(Point a)
        {
            m_marks[0] = a;
        }

        public MarkPair(Point a, Point b)
        {
            m_marks[0] = a;
            m_marks[1] = b;

            //Ensure top-left is first
        }

        public void Add(Point b)
        {
            if (!m_marks[1].Equals(new Point(0, 0)))
                throw new Exception();

            m_marks[1] = b;
        }

        #region Distance Measurement Functions

        /// <summary>
        /// Returns Euclidean distance between two points. Sqrt(delta_x^2 + delta_y^2).
        /// </summary>
        /// <returns>Euclidean Distance</returns>
        public double Euclidean()
        {
            double dx = m_marks[1].X - m_marks[0].X;
            double dy = m_marks[1].Y - m_marks[0].Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }

        /// <summary>
        /// Returns absolute distance in horizontal direction.
        /// </summary>
        /// <returns>Absolute Distance</returns>
        public double HorizontalDistance()
        {
            return (double)Math.Abs(m_marks[1].X - m_marks[0].X);
        }

        /// <summary>
        /// Returns absolute distance in vertical direction.
        /// </summary>
        /// <returns>Absolute Distance</returns>
        public double VerticalDistance()
        {
            return (double)Math.Abs(m_marks[1].Y - m_marks[0].Y);
        }

        #endregion

    }
}
