﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace SOHA.Library.Xml
{
    public class CollectionPropertyDescriptor<T,U> : PropertyDescriptor 
        where T : SohaCollection<U>
        where U : SohaObject
    {
        private T collection = default(T);
		private int index = -1;

        public CollectionPropertyDescriptor(T coll, int idx) : 
			base( "#"+idx.ToString(), null )
		{
			this.collection = coll;
			this.index = idx;
		} 

        public override AttributeCollection Attributes
        {
            get
            {
                return new AttributeCollection(null);
            }
        }

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override Type ComponentType
        {
            get
            {
                return this.collection.GetType();
            }
        }

        public override string DisplayName
        {
            get
            {
                if(index >= this.collection.Count) return "";

                U o = (U)this.collection[index];

                return o.DisplayName;
            }
        }

        public override string Description
        {
            get
            {
                if(index >= this.collection.Count) return "";

                U o = (U)this.collection[index];

                return o.Description;
            }
        }

        public override object GetValue(object component)
        {
            if(index >= this.collection.Count) return null;

            return this.collection[index];
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override string Name
        {
            get { return "#" + index.ToString(); }
        }

        public override Type PropertyType
        {
            get
            {
                if(index >= this.collection.Count) return null;

                return this.collection[index].GetType(); 
            }
        }

        public override void ResetValue(object component)
        {
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }

        public override void SetValue(object component, object value)
        {
            // this.collection[index] = value;
        }
    }
}
