﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using SOHA.Library;
using System.ComponentModel;

namespace SOHA.Library.Xml
{
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class IntervalPhaseData : PhaseData, IXmlSerializable
    {
        private IntervalCollection m_intervals;

        [Browsable(true)]
        [ReadOnly(true)]
        public IntervalCollection Intervals
        {
            get
            {
                return m_intervals;
            }
            set
            {
                m_intervals = value;
            }
        }

        public IntervalPhaseData()
            : base()
        {
            //m_intervals.
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            while (reader.Read())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(IntervalCollection));
                m_intervals = (IntervalCollection)serializer.Deserialize(reader);

                reader.ReadEndElement();
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(IntervalCollection));
            serializer.Serialize(writer, m_intervals);
        }
    }
}
