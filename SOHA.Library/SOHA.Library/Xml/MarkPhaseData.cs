﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;
using SOHA.Library;
using System.Collections.ObjectModel;

namespace SOHA.Library.Xml
{
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    [XmlRoot("MarkPhaseData")]
    public class MarkPhaseData : PhaseData
    {
        private SohaCollection<MarkPair> m_marks = new SohaCollection<MarkPair>();
        private SohaCollection<Roi> m_rois = new SohaCollection<Roi>();
        private double m_pixel_scale_ratio = 0;

        [Browsable(true)]
        [ReadOnly(true)]
        public SohaCollection<MarkPair> Marks
        {
            get
            {
                return m_marks;
            }
            set
            {
                m_marks = value;
            }
        }
        
        [Browsable(true)]
        [ReadOnly(true)]
        public SohaCollection<Roi> Rois
        {
            get
            {
                return m_rois;
            }
            set
            {
                m_rois = value;
            }
        }

        public double ScaleRatio
        {
            get
            {
                return m_pixel_scale_ratio;
            }
            set
            {
                m_pixel_scale_ratio = value;
            }
        }

        public MarkPhaseData()
            : base()
        {

        }

    }
}
