﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHA.Library.Xml
{
    public abstract class PhaseOptions : SohaObject
    {
        public PhaseOptions() { }

        public override string DisplayName
        {
            get { return "Options"; }
        }

        public override string Description
        {
            get { return "Description"; }
        }
    }
}
