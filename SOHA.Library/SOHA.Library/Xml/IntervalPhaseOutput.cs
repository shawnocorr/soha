﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SOHA.Library.Xml
{
    [XmlInclude(typeof(IntervalPhaseOutput))]
    public class IntervalPhaseOutput : PhaseOutput
    {

        public IntervalPhaseOutput()
        {
            Data.Add(new IntervalPhaseData());
            Settings.Add(new IntervalPhaseSettings());
            Options.Add(new IntervalPhaseOptions());
            m_phase_name = Xml.PhaseName.Interval;
        }

        public override void Clear()
        {
            base.Clear();

            Data.Add(new IntervalPhaseData());
            Options.Add(new IntervalPhaseOptions());
            Settings.Add(new IntervalPhaseSettings());
            m_phase_name = Xml.PhaseName.Interval;
        }
    }
}
