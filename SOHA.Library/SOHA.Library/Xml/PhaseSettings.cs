﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHA.Library.Xml
{
    public abstract class PhaseSettings : SohaObject, INotifyPropertyChanged
    {
        public event EventHandler SettingsChanged;
        new public event PropertyChangedEventHandler PropertyChanged;
        
        public PhaseSettings() { }

        new protected void OnPropertyChanged(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));

            if(SettingsChanged != null)
                SettingsChanged(this, new EventArgs());
        }

        protected void OnSettingsChanged(EventArgs e)
        {
            if(SettingsChanged != null)
                SettingsChanged(this, e);
        }

        public override string DisplayName
        {
            get { return "Settings"; }
        }

        public override string Description
        {
            get { return "Description"; }
        }
    }
}
