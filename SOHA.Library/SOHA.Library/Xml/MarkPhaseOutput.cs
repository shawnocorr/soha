﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;

namespace SOHA.Library.Xml
{
    [XmlInclude(typeof(MarkPhaseData))]
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class MarkPhaseOutput : PhaseOutput, IPhaseOutput
    {

        public MarkPhaseOutput()
        {
            Data.Add(new MarkPhaseData());
            Options.Add(new MarkPhaseOptions());
            Settings.Add(new MarkPhaseSettings());
            m_phase_name = Xml.PhaseName.Mark;
        }

        public override void Clear()
        {
            base.Clear();

            Data.Add(new MarkPhaseData());
            Options.Add(new MarkPhaseOptions());
            Settings.Add(new MarkPhaseSettings());
            m_phase_name = Xml.PhaseName.Mark;
        }
    }
}
