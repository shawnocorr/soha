﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Xml
{
    [Serializable]
    public class XmlDeserializationException : Exception
    {
        public XmlDeserializationException() { }
        public XmlDeserializationException(string message) : base(message) { }
        public XmlDeserializationException(string message, Exception inner) : base(message, inner) { }
        protected XmlDeserializationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class XmlSerializationException : Exception
    {
        public XmlSerializationException() { }
        public XmlSerializationException(string message) : base(message) { }
        public XmlSerializationException(string message, Exception inner) : base(message, inner) { }
        protected XmlSerializationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
