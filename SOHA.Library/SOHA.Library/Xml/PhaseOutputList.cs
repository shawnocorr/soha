﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Globalization;

namespace SOHA.Library.Xml
{
    [XmlRoot("PhaseOutputList")]

    [XmlInclude(typeof(MarkPhaseOutput))]
    [XmlInclude(typeof(PreprocessPhaseOutput))]
    [XmlInclude(typeof(IntervalPhaseOutput))]
    [XmlInclude(typeof(PhasePhaseOutput))]
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    [DescriptionAttribute("Expand to see the stored data for different phases.")]
    public class PhaseOutputList : SohaObject, IXmlSerializable, ICustomTypeDescriptor
    {

        private List<PhaseOutput> m_phases = new List<PhaseOutput>();

        [XmlElement("PhaseList")]
        public List<PhaseOutput> Phases
        {
            get
            {
                return m_phases;
            }
            set
            {
                m_phases = value;
            }
        }

        public PhaseOutput this[int index]
        {
            get
            {
                return m_phases[index];
            }
            set
            {
                m_phases[index] = value;
            }
        }
        public PhaseOutput this[PhaseName name]
        {
            get
            {
                foreach(PhaseOutput output in m_phases)
                {
                    if(output.PhaseName == name)
                        return output;
                }

                return null;
            }
            private set
            {
                throw new NotImplementedException();
            }
        }
        public PhaseOutput this[string name]
        {
            get
            {
                foreach(PhaseOutput output in m_phases)
                {
                    if(output.PhaseName.ToString() == name)
                        return output;
                }

                return null;
            }
            private set
            {
                throw new NotImplementedException();
            }
        }

        public PhaseOutputList()
        {
            m_phases = new List<PhaseOutput>();
            m_phases.Add(new MarkPhaseOutput());
            m_phases.Add(new PreprocessPhaseOutput());
            m_phases.Add(new IntervalPhaseOutput());
            m_phases.Add(new PhasePhaseOutput());

            m_phases[0].PhaseName = PhaseName.Mark;
            m_phases[1].PhaseName = PhaseName.Preprocess;
            m_phases[2].PhaseName = PhaseName.Interval;
            m_phases[3].PhaseName = PhaseName.Phase;
        }

        public bool CheckPostPhases(PhaseOutput phase)
        {
            int start = m_phases.IndexOf(phase);
            
            for(int i = start + 1; i <= m_phases.Count - 1; i++)
            {
                if(m_phases[i].Status != PhaseStatus.NA)
                    return true;
            }

            return false;
        }

        //public void CheckPostPhases(PhaseOutput phase)
        //{
        //    int start = m_phases.IndexOf(phase);
        //    Clear(start + 1);
        //}

        public IEnumerator<PhaseOutput> GetEnumerator()
        {
            return m_phases.GetEnumerator();
        }

        public void Clear()
        {
            foreach(PhaseOutput output in m_phases)
                output.Clear();
        }

        public void Clear(int start)
        {
            for(int i = start; i <= m_phases.Count - 1; i++)
            {
                m_phases[i].Clear();
            }
        }

        public void Clear(PhaseOutput phase)
        {
            int start = m_phases.IndexOf(phase);
            Clear(start + 1);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            try
            {
                while (reader.Read())
                {
                    if(!reader.IsStartElement())
                        continue;

                    Type t = Type.GetType("SOHA.Library.Xml." + reader.Name);
                    XmlSerializer serializer = new XmlSerializer(t);
                    object o = serializer.Deserialize(reader.ReadSubtree());
                    Type t1 = o.GetType();

                    if (o is MarkPhaseOutput)
                    {
                        m_phases[0] = o as MarkPhaseOutput;
                    }
                    else if (o is PreprocessPhaseOutput)
                    {
                        m_phases[1] = o as PreprocessPhaseOutput;
                    }
                    else if (o is IntervalPhaseOutput)
                    {
                        m_phases[2] = o as IntervalPhaseOutput;
                    }
                    else if (o is PhasePhaseOutput)
                    {
                        m_phases[3] = o as PhasePhaseOutput;
                    }

                    //reader.ReadEndElement();
                }
            }
            catch (Exception exc)
            {
                //Error Handling for reading XML files
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            foreach (PhaseOutput phase_out in m_phases)
            {
                XmlSerializer serializer = new XmlSerializer(phase_out.GetType());
                serializer.Serialize(writer, phase_out);
            }
        }

        #region ICustomTypeDescriptor Members

        // Implementation of ICustomTypeDescriptor: 

        public String GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public String GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return GetProperties();
        }

        public PropertyDescriptorCollection GetProperties()
        {
            // Create a new collection object PropertyDescriptorCollection
            PropertyDescriptorCollection pds = new PropertyDescriptorCollection(null);

            //// Iterate the list of employees
            ////for(int i = 0; i < Phases.Count; i++)
            ////{
            ////    PropertyDescriptor pd = new P
            ////    pds.Add(pd);
            ////}

            int n = this.Phases.Count;
            for(int i = 0; i < n; i++)
                pds.Add(new PhaseOutputListPropertyDescriptor(this, i));

            return pds;
        }

        #endregion


        public override string DisplayName
        {
            get { return "Phase Data"; }
        }

        public override string Description
        {
            get { return "Stored Data for all phases."; }
        }
    }
}
