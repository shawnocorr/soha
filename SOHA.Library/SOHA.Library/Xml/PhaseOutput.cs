﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;

namespace SOHA.Library.Xml
{

    public enum PhaseStatus
    {
        NA = 0,
        OK,
        Reject
    }

    public enum PhaseName
    {
        Mark = 1,
        Preprocess,
        Interval,
        Phase
    }

    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class PhaseOutput : IXmlSerializable, ICustomTypeDescriptor, IPhaseOutput
    {
        protected PhaseStatus m_phase_status;
        protected PhaseName m_phase_name;
        protected List<PhaseData> m_phase_data = new List<PhaseData>();
        protected List<PhaseSettings> m_phase_settings = new List<PhaseSettings>();
        protected List<PhaseOptions> m_phase_options = new List<PhaseOptions>();

        [Browsable(true)]
        public PhaseStatus Status
        {
            get { return m_phase_status; }
            set { m_phase_status = value; }
        }
        public PhaseName PhaseName
        {
            get { return m_phase_name; }
            set { m_phase_name = value; }
        }
        public List<PhaseData> Data
        {
            get { return m_phase_data; }
        }
        public List<PhaseSettings> Settings
        {
            get { return m_phase_settings; }
        }
        public List<PhaseOptions> Options
        {
            get { return m_phase_options; }
        }

        public PhaseOutput()
        {
            m_phase_status = PhaseStatus.NA;
        }

        /// <summary>
        /// Clears/Resets the <code>PhaseOutput</code> object. Overriding class should call this first.
        /// </summary>
        public virtual void Clear()
        {
            Data.Clear();
            Options.Clear();
            Settings.Clear();
            m_phase_status = PhaseStatus.NA;
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public virtual void ReadXml(System.Xml.XmlReader reader)
        {
            string status = reader["Status"];
            m_phase_status = (PhaseStatus)Enum.Parse(typeof(PhaseStatus), status);

            Data.Clear();
            Settings.Clear();
            Options.Clear();

            while(reader.Read())
            {
                if(!reader.IsStartElement())
                    continue;

                string node_name = reader.Name;
                Type t = Type.GetType("SOHA.Library.Xml." + reader.Name);
                XmlSerializer serializer = new XmlSerializer(t);
                object o = serializer.Deserialize(reader.ReadSubtree());
                
                if(node_name.Contains("Data"))
                {
                    Data.Add(o as PhaseData);
                }
                else if(node_name.Contains("Settings"))
                {
                    Settings.Add(o as PhaseSettings);
                }
                else if(node_name.Contains("Options"))
                {
                    Options.Add(o as PhaseOptions);
                }
                else
                {
                    throw new XmlReadException("Unrecognized Node Type: NOT (Data, Settings, Options)");
                }
            }

            if(Data.Count == 0)
            {
                Type t = Type.GetType("SOHA.Library.Xml." + m_phase_name + "PhaseData");
                Data.Add(Activator.CreateInstance(t) as PhaseData);
            }

            if(Settings.Count == 0)
            {
                Type t = Type.GetType("SOHA.Library.Xml." + m_phase_name + "PhaseSettings");
                Settings.Add(Activator.CreateInstance(t) as PhaseSettings);
            }

            if(Options.Count == 0)
            {
                Type t = Type.GetType("SOHA.Library.Xml." + m_phase_name + "PhaseOptions");
                Options.Add(Activator.CreateInstance(t) as PhaseOptions);
            }
        }

        public virtual void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("Status", m_phase_status.ToString());

            foreach(PhaseData data in m_phase_data)
            {
                XmlSerializer serializer = new XmlSerializer(data.GetType());
                serializer.Serialize(writer, data);
            }

            foreach(PhaseSettings settings in m_phase_settings)
            {
                XmlSerializer serializer = new XmlSerializer(settings.GetType());
                serializer.Serialize(writer, settings);
            }

            foreach(PhaseOptions options in m_phase_options)
            {
                XmlSerializer serializer = new XmlSerializer(options.GetType());
                serializer.Serialize(writer, options);
            }
        }

        #region ICustomTypeDescriptor Members

        public String GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public String GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return GetProperties();
        }

        public PropertyDescriptorCollection GetProperties()
        {
            PropertyDescriptorCollection pds = new PropertyDescriptorCollection(null);

            SohaCollection<PhaseData> data = new SohaCollection<PhaseData>(m_phase_data);
            int n = data.Count;
            for(int i = 0; i < n; i++)
            {
                CollectionPropertyDescriptor<SohaCollection<PhaseData>, PhaseData> pd = new CollectionPropertyDescriptor<SohaCollection<PhaseData>, PhaseData>(data, i);
                pds.Add(pd);
            }

            SohaCollection<PhaseOptions> options = new SohaCollection<PhaseOptions>(m_phase_options);
            n = data.Count;
            for(int i = 0; i < n; i++)
            {
                CollectionPropertyDescriptor<SohaCollection<PhaseOptions>, PhaseOptions> pd = new CollectionPropertyDescriptor<SohaCollection<PhaseOptions>, PhaseOptions>(options, i);
                pds.Add(pd);
            }

            SohaCollection<PhaseSettings> settings = new SohaCollection<PhaseSettings>(m_phase_settings);
            n = data.Count;
            for(int i = 0; i < n; i++)
            {
                CollectionPropertyDescriptor<SohaCollection<PhaseSettings>, PhaseSettings> pd = new CollectionPropertyDescriptor<SohaCollection<PhaseSettings>, PhaseSettings>(settings, i);
                pds.Add(pd);
            }

            return pds;
        }

        #endregion
    }

    /// <summary>
    /// Summary description for CollectionPropertyDescriptor.
    /// </summary>
    public class PhaseOutputListPropertyDescriptor : PropertyDescriptor
    {
        private PhaseOutputList collection = null;
        private int index = -1;

        public PhaseOutputListPropertyDescriptor(PhaseOutputList coll, int idx) :
            base("#" + idx.ToString(), null)
        {
            this.collection = coll;
            this.index = idx;
        }

        public override AttributeCollection Attributes
        {
            get
            {
                return new AttributeCollection(null);
            }
        }

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override Type ComponentType
        {
            get
            {
                return this.collection.GetType();
            }
        }

        public override string DisplayName
        {
            get
            {
                PhaseOutput emp = this.collection[index];
                return emp.PhaseName.ToString();
            }
        }

        public override string Description
        {
            get
            {
                PhaseOutput emp = this.collection[index];
                StringBuilder sb = new StringBuilder();
                sb.Append(emp.PhaseName.ToString());

                return sb.ToString();
            }
        }

        public override object GetValue(object component)
        {
            return this.collection[index];
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override string Name
        {
            get { return "#" + index.ToString(); }
        }

        public override Type PropertyType
        {
            get { return this.collection[index].GetType(); }
        }

        public override void ResetValue(object component)
        {
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }

        public override void SetValue(object component, object value)
        {
            // this.collection[index] = value;
        }
    }


}
