﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SOHA.Library.Xml
{
    [XmlInclude(typeof(PreprocessPhaseData))]
    public class PreprocessPhaseOutput : PhaseOutput
    {
        public PreprocessPhaseOutput()
        {
            Data.Add(new PreprocessPhaseData());
            Options.Add(new PreprocessPhaseOptions());
            Settings.Add(new PreprocessPhaseSettings());
            m_phase_name = Xml.PhaseName.Preprocess;
        }

        public override void Clear()
        {
            base.Clear();

            Data.Add(new PreprocessPhaseData());
            Options.Add(new PreprocessPhaseOptions());
            Settings.Add(new PreprocessPhaseSettings());
            m_phase_name = Xml.PhaseName.Preprocess;
        }
    }
}
