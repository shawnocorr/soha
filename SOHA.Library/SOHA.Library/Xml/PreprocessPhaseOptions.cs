﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;
using SOHA.Library.Imaging;

namespace SOHA.Library.Xml
{
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class PreprocessPhaseOptions : PhaseOptions
    {
        private bool m_mirror = false;
        private bool m_enable_filter = false;
        private List<IFrameFunction> m_filters = new List<IFrameFunction>();

        [Browsable(true)]
        [ReadOnly(true)]
        public bool EnableMirror
        {
            get 
            { 
                return m_mirror; 
            }
            set 
            { 
                m_mirror = value;
                OnPropertyChanged("EnableMirror");
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        [XmlIgnore()]
        public bool EnableFilters
        {
            get 
            { 
                return m_enable_filter; 
            }
            set 
            { 
                m_enable_filter = value;
                OnPropertyChanged("EnableFilters");
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        [XmlIgnore()]
        public List<IFrameFunction> Filters
        {
            get
            {
                return m_filters;
            }
            set
            {
                m_filters = value;
                OnPropertyChanged("Filters");
            }
        }

        public PreprocessPhaseOptions()
            : base()
        {
            m_mirror = false;
            m_enable_filter = false;
        }
    }
}
