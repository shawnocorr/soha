﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using SOHA.Library.Mathematics;

namespace SOHA.Library.Xml
{
    public class IntervalPhaseSettings : PhaseSettings
    {
        private int m_bright_threshold_hi = 67;
        private int m_bright_threshold_lo = 33;
        private int m_intensity_threshold = 33;
        private int m_min_intervals = 8;
        private double m_bright_hi_cutoff = 0;
        private double m_bright_lo_cutoff = 0;
        private double m_intensity_hi_cutoff = 0;
        private double m_intensity_lo_cutoff = 0;
        //private double m_fps = 160;
        private bool m_invert = false;
        private bool m_use_bright = false;


        public int BrightThresholdHi 
        {
            get
            {
                return m_bright_threshold_hi;
            }
            set
            {
                m_bright_threshold_hi = value;
                OnPropertyChanged("BrightThresholdHi");
            }
        }
        public int BrightThresholdLo
        {
            get
            {
                return m_bright_threshold_lo;
            }
            set
            {
                m_bright_threshold_lo = value;
                OnPropertyChanged("BrightThresholdLo");
            }
        }
        public double BrightLoCutoff
        {
            get
            {
                return m_bright_lo_cutoff;
            }
            set
            {
                m_bright_lo_cutoff = value;
                OnPropertyChanged("BrightLoCutoff");
            }
        }
        public double BrightHiCutoff
        {
            get
            {
                return m_bright_hi_cutoff;
            }
            set
            {
                m_bright_hi_cutoff = value;
                OnPropertyChanged("BrightHiCutoff");
            }
        }
        //public Filter BrightFilter { get; set; }
        public bool Invert
        {
            get
            {
                return m_invert;
            }
            set
            {
                m_invert = value;
                OnPropertyChanged("Invert");
            }
        }
        public bool UseBright 
        {
            get
            {
                return m_use_bright;
            }
            set
            {
                m_use_bright = value;
                OnPropertyChanged("UseBright");
            }
        }
        public bool ShowPhase { get; set; }
        public int PhaseOffset { get; set; }

        public int IntensityThreshold
        {
            get
            {
                return m_intensity_threshold;
            }
            set
            {
                m_intensity_threshold = value;
                OnPropertyChanged("IntensityThreshold");
            }
        }
        public int IntensityMinInterval
        {
            get
            {
                return m_min_intervals;
            }
            set
            {
                m_min_intervals = value;
                OnPropertyChanged("IntensityMinInterval");
            }
        }
        public double IntensityLoCutoff
        {
            get
            {
                return m_intensity_lo_cutoff;
            }
            set
            {
                m_intensity_lo_cutoff = value;
                OnPropertyChanged("IntensityLoCutoff");
            }
        }
        public double IntensityHiCutoff
        {
            get
            {
                return m_intensity_hi_cutoff;
            }
            set
            {
                m_intensity_hi_cutoff = value;
                OnPropertyChanged("IntensityHiCutoff");
            }
        }
        //public Filter IntensityFilter { get; set; }


        /// <summary>
        /// Declares a default Settings Object for Intervals
        ///     -Thresholds must be set at [100 - x] in a (0,100) range
        /// </summary>
        public IntervalPhaseSettings()
            : base()
        {
            BrightThresholdHi = 67;
            BrightThresholdLo = 80;
            BrightLoCutoff = 0.0;
            BrightHiCutoff = 100.0;

            Invert = false;
            UseBright = false;
            ShowPhase = false;
            PhaseOffset = 5;

            IntensityMinInterval = 8;
            IntensityThreshold = 80;
            IntensityHiCutoff = 100.0;
            IntensityLoCutoff = 0.0;
        }

        //Commented out 2/15/2015 - Deprecated
    }
}
