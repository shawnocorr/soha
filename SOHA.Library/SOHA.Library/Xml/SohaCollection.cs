﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHA.Library.Xml
{
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class SohaCollection<T> : BindingList<T>, ICustomTypeDescriptor, ISohaCollection<T>
        where T : SohaObject
    {

        public SohaCollection()
            : base()
        {

        }

        public SohaCollection(IList<T> items)
            : base(items)
        {

        }

        public static explicit operator BindingList<object>(SohaCollection<T> s)
        {
            return new BindingList<object>(s.Cast<object>().ToList<object>());
        }

        #region ICustomTypeDescriptor

        public String GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public String GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }


        /// <summary>
        /// Called to get the properties of this type. Returns properties with certain
        /// attributes. this restriction is not implemented here.
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns></returns>
        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return GetProperties();
        }

        /// <summary>
        /// Called to get the properties of this type.
        /// </summary>
        /// <returns></returns>
        public PropertyDescriptorCollection GetProperties()
        {
            PropertyDescriptorCollection pds = new PropertyDescriptorCollection(null);

            int n = this.Count;
            for(int i = 0; i < n; i++)
            {
                CollectionPropertyDescriptor<SohaCollection<T>, T> pd = new CollectionPropertyDescriptor<SohaCollection<T>, T>(this, i);
                pds.Add(pd);
            }

            return pds;
        }

        #endregion
    }
}
