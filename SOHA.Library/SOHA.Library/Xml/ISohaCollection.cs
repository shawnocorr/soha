﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Xml
{
    public interface ISohaCollection<T>
        where T : class
    {
        T this[int index] { get; }
    }
}
