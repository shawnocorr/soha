﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using SOHA.Library.Mathematics;
using System.ComponentModel;
using SOHA.Library.Imaging;

namespace SOHA.Library.Xml
{
    [XmlRoot("MarkPhaseOptions")]
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class MarkPhaseOptions : PhaseOptions, IXmlSerializable
    {
        [XmlElement("FilterCollection")]
        [XmlArrayItem("ImageFilter")]
        private List<IFrameFunction> m_filters = new List<IFrameFunction>();

        [Browsable(true)]
        [ReadOnly(true)]
        public List<IFrameFunction> Filters
        {
            get { return m_filters; }
            set { m_filters = value; }
        }

        public MarkPhaseOptions()
            : base()
        {

        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            while(reader.Read())
            {
                //XmlSerializer serializer = new XmlSerializer(typeof(List<ImageFilter>));
                //m_filters = (serializer.Deserialize(reader) as List<ImageFilter>).ToList();

                reader.ReadEndElement();
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            try
            {
                //XmlSerializer serializer = new XmlSerializer(typeof(List<ImageFilter>),  new XmlRootAttribute("FilterCollection"));
                //serializer.Serialize(writer, m_filters);
            }
            catch(Exception exc)
            {

            }
        }

        #endregion
    }
}
