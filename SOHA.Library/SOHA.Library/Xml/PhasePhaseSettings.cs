﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SOHA.Library.Xml
{
    public class PhasePhaseSettings : PhaseSettings
    {
        private double m_lo_cutoff = 0;
        private double m_hi_cutoff = 100;
        private int m_offset = 4;
        private int m_zoom = 1;
        private bool m_use_threshold = false;
        private int m_threshold = 33;


        public double LoCutoff
        {
            get
            {
                return m_lo_cutoff;
            }
            set
            {
                m_lo_cutoff = value;
                OnPropertyChanged("LoCutoff");
            }
        }
        public double HiCutoff
        {
            get
            {
                return m_hi_cutoff;
            }
            set
            {
                m_hi_cutoff = value;
                OnPropertyChanged("HiCutoff");
            }
        }
        public int Offset
        {
            get
            {
                return m_offset;
            }
            set
            {
                m_offset = value;
                OnPropertyChanged("Offset");
            }
        }
        public int Zoom
        {
            get
            {
                return m_zoom;
            }
            set
            {
                m_zoom = value;
                OnPropertyChanged("Zoom");
            }
        }
        public bool UseThreshold
        {
            get
            {
                return m_use_threshold;
            }
            set
            {
                m_use_threshold = value;
                OnPropertyChanged("UseThreshold");
            }
        }
        public int Threshold
        {
            get
            {
                return m_threshold;
            }
            set
            {
                m_threshold = value;
                OnPropertyChanged("Threshold");
            }
        }

        public PhasePhaseSettings()
            : base()
        {
            Offset = 4;
            LoCutoff = 0.0;
            HiCutoff = 100.0;
            Zoom = 2;
            UseThreshold = false;
            Threshold = 80;
        }
    }
}
