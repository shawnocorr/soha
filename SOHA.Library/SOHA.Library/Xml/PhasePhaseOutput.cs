﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;

namespace SOHA.Library.Xml
{
    [XmlInclude(typeof(PhasePhaseOutput))]
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class PhasePhaseOutput : PhaseOutput
    {

        public PhasePhaseOutput()
        {
            Data.Add(new PhasePhaseData());
            Settings.Add(new PhasePhaseSettings());
            Options.Add(new PhasePhaseOptions());

            m_phase_name = Xml.PhaseName.Phase;
        }

        public override void Clear()
        {
            base.Clear();

            Data.Add(new PhasePhaseData());
            Options.Add(new PhasePhaseOptions());
            Settings.Add(new PhasePhaseSettings());
            m_phase_name = Xml.PhaseName.Phase;
        }
    }
}
