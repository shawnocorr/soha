﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace SOHA.Library.Xml
{
    public class FilegramHeader
    {

        public string SohaLibraryVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
        public string SohaVersion
        {
            get
            {
                return "";// Assembly.GetAssembly();
            }
        }

        public FilegramHeader() { }
    }
}
