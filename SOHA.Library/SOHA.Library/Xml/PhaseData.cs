﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHA.Library.Xml
{
    public abstract class PhaseData : SohaObject
    {
        public PhaseData() { }


        public override string DisplayName
        {
            get { return "Data"; }
        }

        public override string Description
        {
            get { return "Description"; }
        }
    }
}
