﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Xml
{
    [Serializable]
    public class XmlReadException : Exception
    {
        public XmlReadException() { }
        public XmlReadException(string message) : base(message) { }
        public XmlReadException(string message, Exception inner) : base(message, inner) { }
        protected XmlReadException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
