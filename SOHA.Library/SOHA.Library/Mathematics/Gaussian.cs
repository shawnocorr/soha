﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Mathematics
{
    public class Gaussian
    {

        public static double[] GaussFunction(double sigma)
        {
            double std = sigma * sigma;

            int m = (int)(6 * sigma + 1);
            double[] filter = new double[m];
            int filter_center = m / 2;

            #region Calculate Gaussian values to within 6-Sigma

            double f_0 = (double)Math.Pow((2 * Math.Sqrt(Math.PI * 2)), -1);
            double den = 2 * sigma * sigma;

            if (m % 2 == 0)
            {//Even Length Filter
                filter[filter_center] = filter[filter_center - 1] = f_0;
            }
            else
            {//Odd Length Filter
                filter[filter_center] = f_0;
            }

            for (int i = 1; i <= m / 2; i++)
            {
                if (m % 2 == 0)
                {//Even Length Filter
                    if (i != m / 2)
                    {
                        filter[filter_center + i] = filter[filter_center - i - 1] = (double)Math.Exp(-i * i / den) * f_0;
                    }
                }
                else
                {//Odd Length Filter
                    filter[filter_center + i] = filter[filter_center - i] = (double)Math.Exp(-i * i / den) * f_0;
                }
            }

            #endregion

            return filter;
        }
    }
}
