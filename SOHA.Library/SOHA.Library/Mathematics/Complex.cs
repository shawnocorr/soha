﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Mathematics
{
    public class Complex
    {
        // Real and Imaginary parts of a complex number
        private double real, imaginary;

        public Complex(double real, double imaginary)
        {
            this.real = real;
            this.imaginary = imaginary;
        }

        // Accessor methods for accessing/setting private variables
        public double Real
        {
            get { return real; }
            set { real = value; }
        }

        public double Imaginary
        {
            get { return imaginary; }
            set { imaginary = value; }
        }

        public double Magnitude
        {
            get
            {
                return Math.Sqrt(real * real + imaginary * imaginary);
            }
        }

        public double Phase
        {
            get
            {
                return Math.Atan2(imaginary, real);
            }
        }

        //////////////////////////////////////////////
        //
        //  Implicit and Explicit conversion operators
        //

        // Implicit conversion of Complex-to-double
        public static implicit operator double(Complex c)
        {
            return c.Real;
        }

        // Explicit conversion of double-to-complex (requires 
        // explicit cast)
        public static explicit operator Complex(double f)
        {
            return new Complex(f, 0);
        }

        public Complex Power(int n)
        {
            double mag = Math.Sqrt(Math.Pow((double)real, n) + Math.Pow((double)imaginary, n));
            double ang = Math.Atan2((double)imaginary, (double)real);

            double new_mag = Math.Pow(mag, n);
            double new_ang = n * ang;

            double A = (double)(new_mag * Math.Cos(new_ang));
            double B = (double)(new_mag * Math.Sin(new_ang));

            return new Complex(A, B);
        }

        public Complex Conj()
        {
            return new Complex(this.real, -this.imaginary);
        }

        //////////////////////////////////////////////
        //
        //  Arithmetic overloaded operators:
        //  +, -, *, /, ==, !=
        //

        public static Complex operator +(Complex c)
        {
            return c;
        }

        public static Complex operator -(Complex c)
        {
            return new Complex(-c.Real, -c.Imaginary);
        }

        public static Complex operator +(Complex c1, Complex c2)
        {
            return new Complex(c1.Real + c2.Real, c1.Imaginary + c2.Imaginary);
        }

        public static Complex operator +(Complex c1, double num)
        {
            return new Complex(c1.Real + num, c1.Imaginary);
        }

        public static Complex operator +(double num, Complex c1)
        {
            return new Complex(c1.Real + num, c1.Imaginary);
        }

        public static Complex operator -(Complex c1, double num)
        {
            return new Complex(c1.Real - num, c1.Imaginary);
        }

        public static Complex operator -(double num, Complex c1)
        {
            return new Complex(num - c1.Real, -c1.Imaginary);
        }

        public static Complex operator -(Complex c1, Complex c2)
        {
            return new Complex(c1.Real - c2.Real, c1.Imaginary -
                c2.Imaginary);
        }

        public static Complex operator *(Complex c1, Complex c2)
        {
            return new Complex((c1.Real * c2.Real) -
               (c1.Imaginary * c2.Imaginary),
                        (c1.Real * c2.Imaginary) + (c1.Imaginary *
                         c2.Real));
        }

        public static Complex operator *(Complex c1, double num)
        {
            return new Complex(c1.Real * num, c1.Imaginary * num);
        }

        public static Complex operator *(double num, Complex c1)
        { return new Complex(c1.Real * num, c1.Imaginary * num); }

        public static Complex operator /(Complex c1, Complex c2)
        {
            double div = c2.Real * c2.Real + c2.Imaginary * c2.Imaginary;
            if (div == 0) throw new DivideByZeroException();

            return new Complex((c1.Real * c2.Real +
                c1.Imaginary * c2.Imaginary) / div,
                                (c1.Imaginary * c2.Real -
                                 c1.Real * c2.Imaginary) / div);
        }

        public static bool operator ==(Complex c1, Complex c2)
        {
            return (c1.Real == c2.Real) && (c1.Imaginary == c2.Imaginary);
        }

        public static bool operator !=(Complex c1, Complex c2)
        {
            return (c1.Real != c2.Real) || (c1.Imaginary != c2.Imaginary);
        }

        public override int GetHashCode()
        {
            return (Real.GetHashCode() ^ Imaginary.GetHashCode());
        }

        public override bool Equals(object o)
        {
            return (o is Complex) ? (this == (Complex)o) : false;
        }

        // Display the Complex Number in natural form:
        // ------------------------------------------------------------------
        // Note that calling this method will box the value into a string 
        // object and thus cause it to be allocated on the heap with a size of 
        // 24 bytes
        public override string ToString()
        {
            return (String.Format("{0} + {1}i", real, imaginary));
        }
    }
}
