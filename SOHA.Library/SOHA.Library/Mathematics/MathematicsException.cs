﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Mathematics
{
    [Serializable]
    public class MathematicsException : Exception
    {
        public MathematicsException() { }
        public MathematicsException(string message) : base(message) { }
        public MathematicsException(string message, Exception inner) : base(message, inner) { }
        protected MathematicsException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
