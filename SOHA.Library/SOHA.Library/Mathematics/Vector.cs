﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SOHA.Library.Mathematics
{
    public class Vector : Object, ICloneable, IEnumerable
    {
        private double[] m_vector;

        public double this[int index]
        {
            get
            {
                return m_vector[index];
            }
            set
            {
                m_vector[index] = value;
            }
        }

        public int Size
        {
            get 
            {
                if (m_vector == null)
                    return 0;

                return m_vector.Length; 
            }
        }

        public bool isEmpty
        {
            get
            {
                return (Size != 0);
            }
        }

        #region Constructors

        public Vector() { }

        public Vector(int size)
        {
            m_vector = new double[size];
        }

        public Vector(int size, object[] vals)
        {
            if (size != vals.Length)
                throw new InvalidOperationException("Vector Instantiation Error: Wrong Number of Values.");

            List<double> values;
            try
            {
                values = vals.Cast<double>().ToList<double>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("Vector Instantiation Error: Invalid type.");
            }

            m_vector = values.ToArray();
        }

        public Vector(object[] vals)
        {
            List<double> values;
            try
            {
                values = vals.Cast<double>().ToList<double>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("Vector Instantiation Error: Invalid type.");
            }

            m_vector = values.ToArray();
        }

        public Vector(params double[] list)
        {
            m_vector = list;
        }

        public Vector(List<int> list)
        {
            m_vector = new double[list.Count];
            for(int i = 0; i < m_vector.Length; i++)
                m_vector[i] = list[i];
        }

        public enum VectorCreationPlacement
        {
            Front,
            Back
        }

        public Vector(int size, object[] vals, VectorCreationPlacement placement)
        {
            throw new NotImplementedException();

            if (size > vals.Length)
                throw new InvalidOperationException("Vector Instantiation Error: Wrong Number of Values.");

            m_vector = new double[size];

            int start = placement == VectorCreationPlacement.Front ? 0 : size - 1;
            int increment = placement == VectorCreationPlacement.Front ? 1 : -1;

            //for(int i

            List<double> values;
            try
            {
                values = vals.Cast<double>().ToList<double>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("Vector Instantiation Error: Invalid type.");
            }

            m_vector = values.ToArray();
        }

        #endregion

        #region Shift Operations

        /// <summary>
        /// Returns a new Vector shifted left with 0 placed in.
        /// </summary>
        /// <returns></returns>
        public Vector LeftShift() 
        {
            Vector new_vector = new Vector(this.Size);

            for (int i = 0; i < new_vector.Size - 1; i++)
            {
                new_vector[i] = this[i + 1];
            }
            new_vector[new_vector.Size - 1] = 0;

            return new_vector;
        }
        
        /// <summary>
        /// Returns a new Vector shifted left with value placed in.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public Vector LeftShift(double value) 
        {
            Vector new_vector = new Vector(this.Size);

            for (int i = 0; i < new_vector.Size - 1; i++)
            {
                new_vector[i] = this[i + 1];
            }
            new_vector[new_vector.Size - 1] = value;

            return new_vector;
        }

        /// <summary>
        /// Returns a new Vector shifted right with 0 placed in.
        /// </summary>
        /// <returns></returns>
        public Vector RightShift()
        {
            Vector new_vector = new Vector(this.Size);
            
            new_vector[0] = 0;
            for (int i = 1; i < new_vector.Size; i++)
            {
                new_vector[i] = this[i - 1];
            }

            return new_vector;
        }

        /// <summary>
        /// Returns a new Vector shifted left with value placed in.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public Vector RightShift(double value)
        {
            Vector new_vector = new Vector(this.Size);

            new_vector[0] = value;
            for (int i = 1; i < new_vector.Size; i++)
            {
                new_vector[i] = this[i - 1];
            }

            return new_vector;
        }

        #endregion

        #region Statistical Operations

        public double Median()
        {
            IOrderedEnumerable<double> v = m_vector.OrderBy(x => x);

            int index = v.Count() / 2;

            if (index % 2 == 1)
            {
                return v.ElementAt<double>(index);
            }
            else
            {
                return (v.ElementAt<double>(index - 1) + v.ElementAt<double>(index)) / 2.0;
            }

        }

        #endregion

        #region Operator Overloads

        public static Vector operator +(Vector v1, Vector v2)
        {
            if (v1.Size != v2.Size)
                throw new InvalidOperationException("Vector Addition Error: Vectors of unequal size.");

            Vector ret = new Vector(v1.Size);
            for (int i = 0; i < ret.Size; i++)
            {
                ret[i] = v1[i] + v2[i];
            }
            return ret;
        }

        public static Vector operator -(Vector v1, Vector v2)
        {
            if (v1.Size != v2.Size)
                throw new InvalidOperationException("Vector Addition Error: Vectors of unequal size.");

            Vector ret = new Vector(v1.Size);
            for (int i = 0; i < ret.Size; i++)
            {
                ret[i] = v1[i] - v2[i];
            }
            return ret;
        }

        public static Vector operator *(double r, Vector v)
        {
            Vector v_ret = new Vector(v.Size);

            for(int i = 0; i<v.Size;i++)
            {
                v_ret[i] = r * v[i];
            }

            return v_ret;
        }

        public static Vector operator *(Vector v, double r)
        {
            Vector v_ret = new Vector(v.Size);

            for (int i = 0; i < v.Size; i++)
            {
                v_ret[i] = r * v[i];
            }

            return v_ret;
        }

        public static Matrix operator ~(Vector v)
        {
            int size = v.Size;
            Matrix ret = new Matrix(1, size);

            for (int i = 0; i < size; i++)
            {
                ret[0, i] = v[i];
            }

            return ret;
        }

        #endregion

        #region Implicit Casting

        public static implicit operator double[](Vector v)
        {
            return v.m_vector;
        }

        public static explicit operator Matrix(Vector v)
        {
            int size = v.Size;
            Matrix ret = new Matrix(size, 1);

            for (int i = 0; i < size; i++)
            {
                ret[i, 0] = v[i];
            }

            return ret;
        }

        public static explicit operator double[,](Vector v)
        {
            int size = v.Size;
            double[,] ret = new double[size, 1];

            for (int i = 0; i < size; i++)
            {
                ret[i, 0] = v[i];
            }

            return ret;
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            Vector new_vector = new Vector(this.Size);

            for (int i = 0; i < new_vector.Size; i++)
            {
                new_vector[i] = this[i];
            }

            return new_vector;
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (m_vector as IEnumerable).GetEnumerator();
        }

        #endregion

        public static Vector SubArray(Vector v, int index, int length)
        {
            double[] result = new double[length];
            Array.Copy(v, index, result, 0, length);
            return new Vector(result);
        }
    }
}
