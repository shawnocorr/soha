﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHA.Library.Mathematics
{
    public class FilterRange : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected double m_hi_cutoff;
        protected double m_lo_cutoff;

        public double HiCutoff
        {
            get
            {
                return m_hi_cutoff;
            }
            set
            {
                m_hi_cutoff = value;
                OnPropertyChanged("HiCutoff");
            }
        }
        public double LoCutoff
        {
            get
            {
                return m_lo_cutoff;
            }
            set
            {
                m_lo_cutoff = value;
                OnPropertyChanged("LoCutoff");
            }
        }

        public FilterRange()
        {
            m_lo_cutoff = 0;
            m_hi_cutoff = 100;
        }

        protected void OnPropertyChanged(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

    }
}
