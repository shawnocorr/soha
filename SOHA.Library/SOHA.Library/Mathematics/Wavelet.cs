﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Mathematics
{
    public class Wavelet
    {

        public static double[][] DWT(double[] x, int levels)
        {
            int n = x.Length;
            double[][] y = new double[levels][];

            for(int i = 0; i < levels; i++)
            {
                Filter HP = IIRButterworth.Butter(7, new double[] { .5 }, FilterType.High);
                Filter LP = IIRButterworth.Butter(7, new double[] { .5 }, FilterType.Low);

                double[] high_passed = HP.DualInputZeroPhaseFilter(x);
                double[] low_passed = LP.DualInputZeroPhaseFilter(x);

                high_passed = Sampling.Downsample(high_passed, 2);
                low_passed = Sampling.Downsample(low_passed, 2);

                y[i] = high_passed;
                x = low_passed;
            }

            return y;
        }
    }
}
