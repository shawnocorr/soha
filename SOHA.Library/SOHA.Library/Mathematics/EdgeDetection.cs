﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Mathematics;

namespace SOHA.Library.Mathematics
{
    public class EdgeDetection
    {

        public static readonly Matrix SOBEL_Y = new Matrix(3, 3, new double[] { -1, -2, -1, 0, 0, 0, 1, 2, 1 });
        public static readonly Matrix SOBEL_X = new Matrix(3, 3, new double[] { -1, 0, 1, -2, 0, 2, -1, 0, 1 });

        public static Matrix[] Sobel(Matrix M)
        {
            //Matrix Y = new Matrix(3,3,new double[]{-1,-2,-1,0,0,0,1,2,1});
            //Matrix X = new Matrix(3,3,new double[]{-1,0,1,-2,0,2,-1,0,1});

            Matrix GY = M.Convolute(SOBEL_Y);
            Matrix GX = M.Convolute(SOBEL_X);

            Matrix G = M.CloneEmpty();
            Matrix P = M.CloneEmpty();

            for (int i = 0; i < G.Rows; i++)
            {
                for (int j = 0; j < G.Columns; j++)
                {
                    G[i, j] = Math.Sqrt(GY[i, j] * GY[i, j] + GX[i, j] * GX[i, j]);
                    P[i, j] = Math.Atan2(GY[i, j], GX[i, j]);
                }
            }

            return new Matrix[] { G, P };
        }

        public static Matrix Canny(Matrix M, double blur_variance)
        {
            /* 1. Gaussian Blur */

            Matrix blurred = ImageProcessing.GaussFilter(M, FilterType.Low, new double[] { blur_variance });

            /* 2. Intensity Gradient (Magnitude & Phase) */

            Matrix[] gradient = Sobel(blurred);

            /* 3. Non-Maxima Suppression */

            /* 4. */

            return gradient[0];
        }
    }
}
