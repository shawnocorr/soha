﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Mathematics;

namespace SOHA.Library.Mathematics
{
    public enum FilterType
    {
        Low,
        High,
        Bandpass
    }

    public class Filter : FilterRange
    {
        /* Denominator, Feedback */
        protected double[] A;

        /* Numerator, Feedforward */
        protected double[] B;
        
        protected double m_fps = 160;

        public double[] Denominator
        {
            get
            {
                return A;
            }
            set
            {
                A = value;
            }
        }

        public double[] Numerator
        {
            get
            {
                return B;
            }
            set
            {
                B = value;
            }
        }

        public double Fps
        {
            get
            {
                return m_fps;
            }
            set
            {
                m_fps = value;
            }
        }


        public Filter()
            : base()
        {
            A = new double[1];
            A[0] = 1;

            B = new double[1];
            B[0] = 1;

            m_fps = 160;
        }

        public Filter(double fps)
            : base()
        {
            A = new double[1];
            A[0] = 1;

            B = new double[1];
            B[0] = 1;

            m_fps = fps;
        }

        public static Filter GetButterworth(int n, double[] Wn, FilterType type)
        {
            #region Precondition Checks

            if(n > 100)
                throw new ButterworthException("Filter Order too large.");

            //if (Wn.Length != 2)
            //    throw new ButterworthException("Invalid poles.");

            #endregion

            double[] u = new double[2];
            for(int i = 0; i < Wn.Length; i++)
            {
                int fs = 2;
                u[i] = 2 * fs * Math.Tan(Math.PI * Wn[i] / fs);
            }

            double Bw = 0;
            double center_freq = 0;
            switch(type)
            {
                case FilterType.Low:
                    center_freq = u[0];
                    break;
                case FilterType.High:
                    center_freq = u[0];
                    break;
                case FilterType.Bandpass:
                    Bw = u[1] - u[0];
                    center_freq = Math.Sqrt(u[1] * u[0]);
                    break;
                default:
                    break;
            }

            ZeroPoleGain zpk = buttap(n);
            StateSpace ss = zpk2ss(zpk);
            ss.Transform(type, Bw, center_freq);
            ss.Bilinear(2);
            Vector den = Polynomials.Poly(ss.A);
            Vector num = Polynomials.Poly(ss.A - ss.B * ss.C) + (ss.D - 1) * den;

            //Direct_FormII_Transposed_Filter filter = new Direct_FormII_Transposed_Filter();
            //filter.B = num;
            //filter.A = den;
            Filter filter = new Filter();
            filter.Denominator = den;
            filter.Numerator = num;
            return filter;
        }

        /// <summary>
        /// Non-Linear Phase Default Digital Filtering
        /// </summary>
        /// <param name="x">Input sequence</param>
        /// <returns>Filtered sequence</returns>
        public virtual double[] Filt(double[] x)
        {
            double[] y = new double[x.Length];

            for (int i = 0; i < x.Length; i++)
            {
                double val = B[0] * x[i];

                for (int j = 1; j < A.Length; j++)
                {
                    if (i - j < 0)
                        break;

                    val += B[j] * x[i - j];
                    val -= A[j] * y[i - j];
                }

                y[i] = val;
            }

            return y;
        }

        /// <summary>
        /// Zero-Phase Filter. Filter, Reverse, Filter, Reverse, Output
        /// </summary>
        /// <param name="x">Input Sequence</param>
        /// <returns>Output Sequence</returns>
        public virtual double[] ZeroPhaseFilter(double[] x)
        {
            double[] y = Filt(x);
            y = y.Reverse().ToArray();
            return Filt(y).Reverse().ToArray();
        }

        public virtual double[] DualInputZeroPhaseFilter(double[] x)
        {
            double[] new_x = new double[3*x.Length];

            for (int i = 0; i < x.Length; i++)
            {
                new_x[i] = new_x[i + x.Length] = new_x[i + 2*x.Length] = x[i];
            }

            double[] y = ZeroPhaseFilter(new_x);

            double[] new_y = new double[x.Length];

            for (int i = 0; i < new_y.Length; i++)
            {
                new_y[i] = y[i + x.Length];
            }

            return new_y;
        }


        private static ZeroPoleGain buttap(int n)
        {
            ZeroPoleGain zpk = new ZeroPoleGain();
            zpk.Initialize();

            for(int i = 1; i <= n - 1; i += 2)
            {
                double a = 0;
                double b = Math.PI * i / (2 * n) + a;
                double e = Math.Exp(a);
                Complex c = new Complex((float)(-e * Math.Cos(b)), (float)(e * Math.Sin(b)));
                zpk.p.Add(c);
                zpk.p.Add(c.Conj());
            }

            if(n % 2 == 1)
            {

            }

            zpk.k = (double)prod(zpk.p).Real;

            return zpk;
        }

        private static StateSpace zpk2ss(ZeroPoleGain zpk)
        {
            //Strip Infinites

            //Calculate Lengths
            int np = zpk.p.Count;
            int nz = zpk.z.Count;

            /* Quick and Dirty Method */

            StateSpace ss = new StateSpace();

            List<Matrix> a_matrices = new List<Matrix>();
            int i = 0;
            while(i < np)
            {
                double[] den = Polynomials.Poly(zpk.p.GetRange(i, 2));

                //Skipping... doesn't seem necessary
                //wn = 1;

                Matrix t = Matrix.Identity(2);
                Matrix a1 = new Matrix(2, 2, new double[] { -den[1], -den[2], 1, 0 });
                Matrix inv_t = ~t;
                a1 = inv_t * a1 * t;
                a_matrices.Add(a1);
                //Matrix b1 = inv_t * new Matrix(2, 1, new double[] { 1, 0 });
                //Matrix c1 = new Matrix(1, 2, new double[] { 0, 1 }) * t;
                //double d1 = 0;

                //int ma1 = ss.A.Size(0);
                //int na2 = a1.Size(1);

                //ss.A = new Matrix(new Matrix[] { ss.A, Matrix.Zeros(ma1, na2), b1 * ss.C, a1 }, "xx;xx");
                //ss.B = new Matrix(new Matrix[] { ss.B, b1 * ss.D }, "x;x");
                //ss.C = new Matrix(new Matrix[] { d1 * c1, c1 }, "xx");
                //ss.D = d1 * ss.D;

                i += 2;
            }

            ss.Initialize(a_matrices.Count * 2, a_matrices);

            #region Long Method

            ////Switch Real/Imaj Parts
            //int i = 0;
            //for (i = 0; i < np; i++)
            //{
            //    float tmp = zpk.p[i].Imaginary;
            //    //zpk.p[i].Imaginary = zpk.p[i].Real;
            //    //zpk.p[i].Real = tmp;
            //}

            ////Initialize SS Matrices
            //StateSpace ss = new StateSpace();
            //ss.Initialize();

            ////Odd/Even Pole Pairs

            ////Iterate Zeros
            //i = 0;
            //while (i < nz)
            //{
            //    i++;
            //}

            ////Iterate Poles
            //while (i < np)
            //{
            //    double[] den = Polynomials.Poly(zpk.p.GetRange(i, 2));

            //    //Skipping... doesn't seem necessary
            //    //wn = 1;

            //    Matrix t = Matrix.Identity(2);
            //    Matrix a1 = new Matrix(2, 2, new double[] { -den[1], -den[2], 1, 0 });
            //    Matrix inv_t = ~t;
            //    a1 = inv_t * a1 * t;
            //    Matrix b1 = inv_t * new Matrix(2, 1, new double[] { 1, 0 });
            //    Matrix c1 = new Matrix(1, 2, new double[] { 0, 1 }) * t;
            //    double d1 = 0;

            //    int ma1 = ss.A.Size(0);
            //    int na2 = a1.Size(1);

            //    ss.A = new Matrix(new Matrix[] { ss.A, Matrix.Zeros(ma1, na2), b1 * ss.C, a1 }, "xx;xx");
            //    ss.B = new Matrix(new Matrix[] { ss.B, b1 * ss.D }, "x;x");
            //    ss.C = new Matrix(new Matrix[] { d1 * c1, c1 }, "xx");
            //    ss.D = d1 * ss.D;

            //    i += 2;
            //}

            #endregion

            return ss;
        }

        private static Complex prod(List<Complex> list)
        {
            Complex product = new Complex(1.0f, 0.0f);
            foreach(Complex c in list)
            {
                product *= c;
            }

            return product;
        }
    }
}
