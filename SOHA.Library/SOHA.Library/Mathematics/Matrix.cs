﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using SOHA.Library.Xml;

namespace SOHA.Library.Mathematics
{
    [Serializable]
    [XmlRoot("Matrix")]
    public class Matrix : Object, ICloneable, IEnumerable, IXmlSerializable
    {

        public static Matrix TestMatrix
        {
            get
            {
                double[,] matrix = new double[3, 3];

                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                        matrix[i,j] = i * j + j;

                return new Matrix(matrix);
            }
        }

        /* jagged arrays offer performance improvements */
        protected double[,] m_matrix;

        public double this[int r, int c]
        {
            get
            {
                return m_matrix[r,c];
            }
            set
            {
                m_matrix[r,c] = value;
            }
        }

        public int Rows
        {
            get 
            {
                if (m_matrix == null)
                    return 0;

                return m_matrix.GetLength(0); 
            }
        }
        public int Columns
        {
            get 
            {
                if (m_matrix == null)
                    return 0;

                return m_matrix.GetLength(1); 
            }
        }
        public bool isSquare
        {
            get
            {
                return (Rows == Columns && Rows != 0);
            }
        }
        public bool isIdentity
        {
            get
            {
                if (!isSquare)
                    return false;

                bool identity = true;
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        if (i == j && this[i, j] != 1)
                            return false;

                        if (i != j && this[i, j] != 0)
                            return false;
                    }
                }
                return true;
            }
        }
        public bool isEmpty
        {
            get
            {
                return (Rows == 0 && Columns == 0);
            }
        }
        public double[,] Data
        {
            get
            {
                return m_matrix;
            }
        }

        public Matrix()
        {
            m_matrix = new double[1, 1];
            m_matrix[0, 0] = 1;
        }

        public Matrix(int n)
        {
            m_matrix = new double[n, n];
        }

        public Matrix(int rows, int cols)
        {
            m_matrix = new double[rows,cols];
        }

        public Matrix(int rows, int cols, double[] vals)
        {
            if (rows * cols != vals.Length)
                throw new InvalidOperationException("Matrix Instantiation Error: Wrong Number of Values.");
                        
            m_matrix = new double[rows,cols];

            int index = 0;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    m_matrix[i, j] = vals[index++];
                }
            }
        }

        /// <summary>
        /// Creates a new matrix consisting of the passed matrices
        /// according to the string format.
        /// 
        /// Format: ex. "xx;xx:
        /// - "x" specifies matrix, in order.
        /// - ";" specifies new row
        /// </summary>
        /// <param name="matrices"></param>
        /// <param name="format"></param>
        public Matrix(Matrix[] matrices, string format)
        {
            int xs = format.ToCharArray().Count(x => x == 'x');

            if (xs != matrices.Length)
                throw new InvalidOperationException("Error in Format string: x.");

            if (format[0] == ';' || format[format.Length - 1] == ';')
                throw new InvalidOperationException("Error in Format string: ;.");

            /* Check Matrix row equivalent and columns equivalent */

            //int colons = format.ToCharArray().Count(c => xs == ';');
            //int new_columns = format.Substring(0, format.IndexOf(';') - 1).Length * matrices[0].Columns;
            //int new_rows = colons * matrices[0].Rows;

            #region Determine Number of Rows and Columns for new Matrix

            int new_columns = 0;
            int new_rows = 0;
            int cur_hi_column = 0;
            int cur_hi_rows = 0;

            int i_m = 0;
            for (int i = 0; i < format.Length; i++)
            {

                if (format[i] == 'x')
                {
                    cur_hi_column += matrices[i_m].Columns;
                    if (matrices[i_m].Rows > cur_hi_rows)
                        cur_hi_rows += matrices[i_m].Rows;
                }

                if (format[i] == ';')
                {
                    if (cur_hi_column > new_columns)
                        new_columns = cur_hi_column;
                    new_rows += cur_hi_rows;

                    cur_hi_column = 0;
                    cur_hi_rows = 0;
                    i_m--;
                }

                i_m++;
            }
            if (cur_hi_column > new_columns)
                new_columns = cur_hi_column;
            new_rows += cur_hi_rows;

            #endregion

            m_matrix = new double[new_rows, new_columns];

            i_m = 0;
            int r_hold = 0;
            int c_hold = 0;
            int c = 0;
            int r = 0;
            for (int k = 0; k < format.Length; k++)
            {

                if (format[k] == 'x')
                {
                    if (!matrices[i_m].isEmpty)
                    {
                        r = r_hold;
                        for (int i = 0; i < matrices[i_m].Rows; i++)
                        {
                            c = c_hold;
                            for (int j = 0; j < matrices[i_m].Columns; j++)
                            {
                                m_matrix[r, c] = matrices[i_m][i, j];
                                c++;
                            }
                            r++;
                        }
                        r_hold = r;
                        r_hold = 0;
                    }
                }

                if (format[k] == ';')
                {
                    c_hold = c;
                    i_m--;
                }

                i_m++;
            }
        }

        public Matrix(double[,] values)
        {
            m_matrix = values;
        }

        /// <summary>
        /// Returns nxn identity matrix
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Matrix Identity(int n)
        {
            Matrix M = new Matrix(n, n);
            for (int i = 0; i < n; i++)
            {
                M[i, i] = 1;
            }
            return M;
        }

        public static Matrix Zeros(int n)
        {
            return Matrix.Zeros(n, n);
        }
        public static Matrix Zeros(int n, int m)
        {
            if (n == 0 || m == 0)
                return new Matrix();

            return new Matrix(n, m);
        }

        public static Matrix Augment(Matrix A, int add_rows, int add_cols)
        {
            Matrix Y = new Matrix(A.Rows + add_rows, A.Columns + add_cols);

            for (int i = 0; i < A.Rows; i++)
            {
                for (int j = 0; j < A.Columns; j++)
                {
                    Y[i, j] = A[i, j];
                }
            }

            return Y;
        }

        public static double[] Poly(Matrix A)
        {
            if (A.isEmpty)
                throw new InvalidOperationException("Matrix is empty.");

            if (!A.isSquare)
                throw new InvalidOperationException("Matrix is not square.");

            int n = A.Size(0);
            double[] ret_poly = new double[n + 1];

            for (int i = 0; i < n; i++)
            {
                List<double> coeffs = new List<double>(n);
                for (int j = 0; j < n; j++)
                {
                    coeffs.Add(A[i, j]);
                    double[] tmp_coeffs = Polynomials.Poly(coeffs);
                }
            }

            return ret_poly;
        }

        #region Flatten/Extrude Functions

        public static double[] Flatten(Matrix A)
        {
            double[] new_array = new double[A.Size(0)];

            int n = 0;
            for(int i = 0; i < A.Rows; i++)
            {
                for(int j = 0; j < A.Columns; j++)
                {
                    new_array[n] = A[i, j];
                    n++;
                }
            }

            return new_array;
        }

        public static double[] Flatten(double[,] A)
        {
            int rank = A.GetLength(0);
            double[] new_array = new double[rank * rank];

            int n = 0;
            for(int i = 0; i < rank; i++)
            {
                for(int j = 0; j < rank; j++)
                {
                    new_array[n] = A[i, j];
                    n++;
                }
            }

            return new_array;
        }

        public static double[,] Extrude(double[] array, int rank)
        {
            double[,] new_array = new Matrix(rank);

            int n = 0;
            for(int i = 0; i < rank; i++)
            {
                for(int j = 0; j < rank; j++)
                {
                    new_array[i, j] = array[n];
                    n++;
                }
            }

            return new_array;
        }

        //public static double[,] Extrude(double[] array, int rank)
        //{
        //    double[,] new_array = new Matrix(rank);

        //    int n = 0;
        //    for(int i = 0; i < rank; i++)
        //    {
        //        for(int j = 0; j < rank; j++)
        //        {
        //            new_array[i, j] = array[n];
        //            n++;
        //        }
        //    }

        //    return new_array;
        //}

        #endregion

        public Vector GetRowVector(int i)
        {
            if (this.isEmpty)
                return new Vector();

            if (i < 0 || i > this.Rows - 1)
                throw new InvalidOperationException("GetVector Error: Invalid Row Number.");

            Vector ret = new Vector(this.Columns);
            for (int j = 0; j < this.Columns; j++)
            {
                ret[j] = this[i,j];
            }
            return ret;
        }

        public Vector GetColumnVector(int j)
        {
            if (this.isEmpty)
                return new Vector();

            if (j < 0 || j > this.Columns - 1)
                throw new InvalidOperationException("GetVector Error: Invalid Row Number.");

            Vector ret = new Vector(this.Rows);
            for (int i = 0; i < this.Rows; i++)
            {
                ret[i] = this[i,j];
            }
            return ret;
        }

        public void FillRandom()
        {
            Random r = new Random(DateTime.Now.Second);
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    m_matrix[i,j] = r.Next(10);
                }
            }
        }

        #region Operator Overloads

        public static Matrix operator +(Matrix A, Matrix B)
        {
            if (A.isEmpty)
                return B;

            if (B.isEmpty)
                return A;

            //if (A.Rows == 0 || A.Columns == 0)
            //    throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix A.");

            //if (B.Rows == 0 || B.Columns == 0)
            //    throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix B.");

            if (A.Rows != B.Rows && A.Columns != B.Columns)
                throw new InvalidOperationException("Invalid Matrix Dimensions: Matrices must have same dimensions.");

            Matrix Y = new Matrix(A.Rows, A.Columns);
            for (int i = 0; i < Y.Rows; i++)
            {
                for (int j = 0; j < Y.Columns; j++)
                {
                    Y[i, j] = A[i, j] + B[i, j];
                }
            }
            return Y;
        }

        public static Matrix operator -(Matrix A, Matrix B)
        {
            if (A.Rows == 0 || A.Columns == 0)
                throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix A.");

            if (B.Rows == 0 || B.Columns == 0)
                throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix B.");

            if (A.Rows != B.Rows && A.Columns != B.Columns)
                throw new InvalidOperationException("Invalid Matrix Dimensions: Matrics must have same dimensions.");

            Matrix Y = new Matrix(A.Rows, A.Columns);
            for (int i = 0; i < Y.Rows; i++)
            {
                for (int j = 0; j < Y.Columns; j++)
                {
                    Y[i, j] = A[i, j] - B[i, j];
                }
            }
            return Y;
        }

        public static Matrix operator *(Matrix A, Matrix B)
        {
            if (A.isEmpty || B.isEmpty)
            {
                return new Matrix();
            }

            int r = A.Columns; int c = B.Rows;

            //if (A.Rows == 0 || A.Columns == 0)
            //    throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix A.");

            //if (B.Rows == 0 || B.Columns == 0)
            //    throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix B.");

            if (A.Columns != B.Rows)
                throw new InvalidOperationException("Invalid Matrix Dimensions: Matrics must have same inner dimensions.");

            Matrix Y = new Matrix(A.Rows, B.Columns);
            for (int i = 0; i < Y.Rows; i++)
            {
                for (int j = 0; j < Y.Columns; j++)
                {
                    double sum = 0;
                    for (int p = 0; p < A.Columns; p++)
                    {
                        sum += A[i, p] * B[p, j];
                    }
                    Y[i, j] = sum;
                }
            }
            return Y;
        }

        public static Matrix operator *(double r, Matrix A)
        {
            if (r == 0)
                return new Matrix();

            if (r == 1)
                return A;

            if (A.Rows == 0 || A.Columns == 0)
                throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix A.");

            Matrix Y = new Matrix(A.Rows, A.Columns);
            for (int i = 0; i < Y.Rows; i++)
            {
                for (int j = 0; j < Y.Columns; j++)
                {
                    Y[i, j] = r * A[i, j];
                }
            }
            return Y;
        }

        public static Matrix operator *(Matrix A, double r)
        {
            if (r == 0)
                return new Matrix();

            if (r == 1)
                return A;

            if (A.Rows == 0 || A.Columns == 0)
                throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix A.");

            Matrix Y = new Matrix(A.Rows, A.Columns);
            for (int i = 0; i < Y.Rows; i++)
            {
                for (int j = 0; j < Y.Columns; j++)
                {
                    Y[i, j] = r * A[i, j];
                }
            }
            return Y;
        }

        public static Matrix operator /(Matrix A, Matrix B)
        {
            return A * (~B);
        }

        public static Matrix operator /(double r, Matrix A)
        {
            return (1 / r) * A;
        }

        public static Matrix operator /(Matrix A, double r)
        {
            return (1 / r) * A;
        }

        public static double operator %(Matrix A, Matrix B)
        {
            if (!A.isSquare || !B.isSquare)
                throw new Exception("Matrices must be square to perform convolution.");

            if ((A.Rows != B.Rows) || (A.Columns != B.Columns))
                throw new Exception("Matrices are not of equivalent sizes.");

            double sum = 0;

            for (int i = 0; i < A.Rows; i++)
            {
                for (int j = 0; j < A.Columns; j++)
                {
                    sum += A[i, j] * B[i, j];
                }
            }

            return sum;
        }

        /// <summary>
        /// Calculates the inverse of Matrix A
        /// </summary>
        /// <param name="A">Matrix to be inverted</param>
        /// <returns>Inverse of Matrix A</returns>
        public static Matrix operator ~(Matrix A)
        {
            //Matrix must be square
            if (!A.isSquare)
                throw new InvalidOperationException("Matrix Inversion Error: Matrix not square.");

            if (Det(A) == 0)
                throw new InvalidOperationException("Matrix Inversion Error: Determinant is 0; Matrix is singular.");

            int n = A.Rows;

            Matrix Y = (Matrix)A.Clone();
            Matrix I = Identity(n);

            #region Move Zeros To Bottom

            for(int i = 0; i<n;i++)
            {
                bool zeros = true;
                for (int j = 0; j < n; j++)
                {
                    if (Y[i, j] != 0)
                    {
                        zeros = false;
                        break;
                    }
                }
                if (zeros)
                {
                    Y.rowswap(i, n - i);
                    I.rowswap(i, n - i);
                }
            }

            #endregion

            #region Forward Subsitution

            for (int j = 0; j < n - 1; j++)
            {
                for (int i = j + 1; i < n; i++)
                {
                    double x = -Y[i, j] / Y[j, j];
                    for (int c = 0; c < n; c++)
                    {
                        Y[i, c] = Y[i, c] + x * Y[j, c];
                        I[i, c] = I[i, c] + x * I[j, c];
                    }
                }
            }

            #endregion

            #region Back Substution

            for (int j = n - 1; j > 0; j--)
            {
                for (int i = j - 1; i >= 0; i--)
                {
                    double x = -Y[i, j] / Y[j, j];
                    for (int c = 0; c < n; c++)
                    {
                        Y[i, c] = Y[i, c] + x * Y[j, c];
                        I[i, c] = I[i, c] + x * I[j, c];
                    }
                }
            }

            #endregion

            #region Reduced Rows

            for (int i = 0; i < n; i++)
            {
                double r = Y[i, i];
                for (int j = 0; j < n; j++)
                {
                    Y[i, j] = Y[i, j] / r;
                    I[i, j] = I[i, j] / r;
                }
            }

            #endregion

            //Checks

            //Check Identity Matrix
            if (!Y.Round(4).isIdentity || !(A*I).Round(4).isIdentity)
                throw new InvalidOperationException("Matrix Inversion Error: Identity Matrix Post Check.");

            return I;
        }

        #endregion

        #region Matrix Operations

        public int Size(int dim)
        {
            if (m_matrix == null)
                return 0;

            return m_matrix.GetLength(dim);
        }

        public Matrix Round(int decimal_places)
        {
            int n = decimal_places;

            Matrix Y = new Matrix(this.Rows, this.Columns);
            for (int i = 0; i < Y.Rows; i++)
            {
                for (int j = 0; j < Y.Columns; j++)
                {
                    Y[i, j] = Math.Round(this[i, j], n); ;
                }
            }
            return Y;
        }

        public Matrix GetNeighborhood(int i, int j, int nhood)
        {
            if(nhood % 2 != 1)
                throw new InvalidOperationException("Neighborhood Size must be odd.");

            if (nhood < 3)
                throw new InvalidOperationException("Neighborhood Size must be >= 3.");

            int dn = (nhood - 1) / 2;

            Matrix Y = new Matrix(nhood, nhood);

            Y[dn, dn] = this[i, j];
            for (int di = 1; di <= dn; di++)
            {
                for (int dj = 1; dj <= dn; dj++)
                {
                    if (i - di >= 0)
                        Y[dn - di,dj] = this[i - di, j];

                    if (i + di < this.Rows - 1)
                        Y[dn + di,dj] = this[i + di, j];

                    if (j - dj >= 0)
                        Y[di, dn - dj] = this[i, j - dj];

                    if (j + dj < this.Columns - 1)
                        Y[di, dn + dj] = this[i, j + dj];

                    if ((i - di >= 0) && (j - dj >= 0))
                        Y[dn - di, dn - dj] = this[i - di, j - dj];

                    if ((i + di < this.Rows - 1) && (j + dj < this.Columns - 1))
                        Y[dn + di, dn + dj] = this[i + di, j + dj];

                    if ((i - di >= 0) && (j + dj < this.Columns - 1))
                        Y[dn - di, dn + dj] = this[i - di, j + dj];

                    if ((i + di < this.Rows - 1) && (j - dj >= 0))
                        Y[dn + di, dn - dj] = this[i + di, j - dj];

                }
            }

            return Y;
        }

        public Vector ToVector()
        {
            Vector V = new Vector(this.Rows * this.Columns);

            foreach (double d in this)
                V = V.LeftShift(d);

            return V;
        }

        public Matrix Convolute(Matrix X)
        {
            if (!X.isSquare)
                throw new Exception("Cannot convolute with a non-square matrix.");

            Matrix Y = this.CloneEmpty();

            int hood_size = X.Rows;

            for (int i = 0; i < this.Rows; i++)
            {
                for (int j = 0; j < this.Columns; j++)
                {
                    Matrix neighborhood = this.GetNeighborhood(i, j, hood_size);
                    Y[i, j] = neighborhood % X;
                }
            }

            return Y;
        }

        /// <summary>
        /// Calculates the determinant of Matrix A
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static double Det(Matrix A)
        {
            if (!A.isSquare)
                throw new InvalidOperationException("Matrix Inversion Error: Matrix not square.");

            int n = A.Rows;
            //double D = 0;

            /* Special Cases */


            /* Liebnitz
            List<int[]> permutations = AllPermutations(n);
            foreach (int[] v in permutations)
            {
                double prod = 1;
                for(int i = 0; i < n;i++)
                {
                    prod *= A[i, v[i] - 1];
                }
                D += sgn(v) * prod;
            }*/

            /* Row-Echelon Form    
             * - Row Swap: Multiply by -1
             * - Row Scalar: Multiply by scalar
             * - Add multiple of one row to another: DO NOTHING
             */
            double d;

            switch(n)
            {
                case 2:

                    return A[0, 0] * A[1, 1] - A[0, 1] * A[1, 0];

                    break;
                default:
                    Matrix Y = (Matrix)A.Clone();

                    int nr = Y.Rows;
                    int nc = Y.Columns;
                    int swaps = 0;
                    double scalar = 1;

                    #region Move Zero Rows

                    for (int i = 0; i < nr; i++)
                    {
                        bool zeros = true;
                        for (int j = 0; j < nc; j++)
                        {
                            if (Y[i, j] != 0)
                            {
                                zeros = false;
                                break;
                            }
                        }
                        if (zeros)
                        {
                            Y.rowswap(i, nr - 1);
                            swaps++;
                            nr--;
                        }
                    }

                    #endregion

                    #region Reduce Echelon Form

                    int p = 0;
                    while (p < nr && p < nc)
                    {
                    restart:
                        int r = 1;
                        while (Y[p, p] == 0)
                        {
                            if (p + r <= nr)
                            {
                                p++;
                                goto restart;
                            }
                            Y.rowswap(p, p + r);
                            swaps++;
                            r++;
                        }

                        for (; r < (nr - p); r++)
                        {
                            if (Y[p + r, p] != 0)
                            {
                                double x = -Y[p + r, p] / Y[p, p];
                                for (int c = p; c < nc; c++)
                                {
                                    Y[p + r, c] = Y[p, c] * x + Y[p + r, c];
                                    /* Dont think this is necessary*/
                                    //scalar *= x;
                                }
                            }
                        }
                        p++;
                    }

                    #endregion

                    #region Ensure Proper Zeros

                    //for (int i = 0; i < Y.Rows; i++)
                    //{
                    //    for (int j = 0; j <= i; j++)
                    //    {
                    //        Y[i, j] = 0;
                    //    }
                    //}

                    #endregion

                    d = 1;
                    for (int i = 0; i < Y.Rows; i++)
                    {
                        d *= Y[i, i];
                    }
                    d *= (swaps % 2 == 0 ? 1 : -1);
                    return d;
            }
        }

        public static Matrix REF(Matrix M)
        {
            Matrix Y = (Matrix)M.Clone();

            int nr = Y.Rows;
            int nc = Y.Columns;

            #region Move Zero Rows

            for (int i = 0; i < nr; i++)
            {
                bool zeros = true;
                for (int j = 0; j < nc; j++)
                {
                    if (Y[i, j] != 0)
                    {
                        zeros = false;
                        break;
                    }
                }
                if (zeros)
                {
                    Y.rowswap(i, nr - 1);
                    nr--;
                }
            }

            #endregion

            #region Reduce Echelon Form

            int p = 0;
            while (p < nr && p < nc)
            {
            restart:
                int r = 1;
                while (Y[p, p] == 0)
                {
                    if (p + r <= nr)
                    {
                        p++;
                        goto restart;
                    }
                    Y.rowswap(p, p + r);
                    r++;
                }

                for (; r < (nr - p); r++)
                {
                    if (Y[p + r, p] != 0)
                    {
                        double x = -Y[p + r, p] / Y[p, p];
                        for (int c = p; c < nc; c++)
                        {
                            Y[p + r, c] = Y[p, c] * x + Y[p + r, c];
                        }
                    }
                }
                p++;
            }

            #endregion

            #region Ensure Proper Zeros

            for (int i = 1; i < Y.Rows; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Y[i, j] = 0;
                }
            }

            #endregion

            return Y;
        }

        public static Matrix RREF(Matrix M)
        {
            Matrix Y = REF(M);

            for (int i = 0; i < Y.Rows; i++)
            {
                int c = i;
                while (c < Y.Columns && Y[i, c] == 0)
                    c++;

                if (c >= Y.Columns)
                    break;
                
                double scale = Y[i, c];
                for (int j = c; j < Y.Columns; j++)
                {
                    Y[i, j] /= scale;
                }
            }

            return Y;
        }

        /// <summary>
        /// Swap Rows. Alters Matrix.
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        private void rowswap(int r1, int r2)
        {
            //Checks
            if (r1 < 0 || r2 < 0 || r1 > Rows || r2 > Rows)
                throw new InvalidOperationException("Invalid Row Swap Operation.");

            double tmp;
            for (int i = 0; i < Columns; i++)
            {
                tmp = this[r1, i];
                this[r1, i] = this[r2, i];
                this[r2, i] = tmp;
            }
        }

        public Matrix DiagonalMajor()
        {
            Matrix Y = (Matrix)this.Clone();
            int n = this.Rows;

            #region Move Zeros To Bottom

            for (int i = 0; i < n; i++)
            {
                bool zeros = true;
                for (int j = 0; j < n; j++)
                {
                    if (Y[i, j] != 0)
                    {
                        zeros = false;
                        break;
                    }
                }
                if (zeros)
                {
                    Y.rowswap(i, n - i);
                    //I.rowswap(i, n - i);
                }
            }

            #endregion

            #region Forward Subsitution

            for (int j = 0; j < n - 1; j++)
            {
                for (int i = j + 1; i < n; i++)
                {
                    double x = -Y[i, j] / Y[j, j];
                    for (int c = 0; c < n; c++)
                    {
                        Y[i, c] = Y[i, c] + x * Y[j, c];
                        //I[i, c] = I[i, c] + x * I[j, c];
                    }
                }
            }

            #endregion

            #region Back Substution

            for (int j = n - 1; j > 0; j--)
            {
                for (int i = j - 1; i >= 0; i--)
                {
                    double x = -Y[i, j] / Y[j, j];
                    for (int c = 0; c < n; c++)
                    {
                        Y[i, c] = Y[i, c] + x * Y[j, c];
                        //I[i, c] = I[i, c] + x * I[j, c];
                    }
                }
            }

            #endregion

            return Y;
        }

        public Matrix DiagonalMinor()
        {
            Matrix Y = (Matrix)this.Clone();
            int n = this.Rows;

            #region Move Zeros To Top

            for (int i = 0; i < n; i++)
            {
                bool zeros = true;
                for (int j = 0; j < n; j++)
                {
                    if (Y[i, j] != 0)
                    {
                        zeros = false;
                        break;
                    }
                }
                if (zeros)
                {
                    Y.rowswap(i, n - i);
                    //I.rowswap(i, n - i);
                }
            }

            #endregion

            #region Forward Subsitution

            for (int j = 0; j < n - 1; j++)
            {
                for (int i = j + 1; i < n; i++)
                {
                    double x = -Y[i, j] / Y[j, j];
                    for (int c = 0; c < n; c++)
                    {
                        Y[i, c] = Y[i, c] + x * Y[j, c];
                        //I[i, c] = I[i, c] + x * I[j, c];
                    }
                }
            }

            #endregion

            #region Back Substution

            for (int j = n - 1; j > 0; j--)
            {
                for (int i = j - 1; i >= 0; i--)
                {
                    double x = -Y[i, j] / Y[j, j];
                    for (int c = 0; c < n; c++)
                    {
                        Y[i, c] = Y[i, c] + x * Y[j, c];
                        //I[i, c] = I[i, c] + x * I[j, c];
                    }
                }
            }

            #endregion

            return Y;
        }

        public double Trace()
        {
            double sum = 0;
            for (int i = 0; i < this.Rows; i++)
            {
                sum += this[i, i];
            }
            return sum;
        }

        public Matrix Power(int n)
        {
            Matrix Y = Matrix.Identity(this.Rows);

            for (int i = 0; i < n; i++)
            {
                Y *= this;
            }

            return Y;
        }

        #endregion

        #region Implicit Casting

        public static implicit operator double[,](Matrix A)
        {
            return A.m_matrix;
        }

        public static implicit operator double(Matrix A)
        {
            if (A.Rows != 1 && A.Columns != 1)
                throw new InvalidCastException("Cannot cast to double: Matrix is not 1x1.");

            return A[0, 0];
        }

        #endregion

        #region Permutations (HIGH ORDER OF MAGNITUDE)

        private static int sgn(int[] permutation)
        {
            int switches = 0;

            //Fill Original
            int n = permutation.Length;
            int[] original = new int[n];
            for (int i = 0; i < n; i++)
            {
                original[i] = i + 1;
            }

            int k = 0;
            while (!original.SequenceEqual(permutation))
            {

                if (original[k] != permutation[k])
                {
                    int tmp = original[k];
                    original[k] = original[k + 1];
                    original[k + 1] = tmp;
                    switches++;
                }
                
                k++;

                if (k == permutation.Length - 1)
                    k = 0;
            }

            return switches % 2 == 0 ? 1 : -1;
        }

        private static List<int[]> AllPermutations(int n)
        {
            List<int[]> permutations = new List<int[]>();

            //Fill Original
            List<int> original = new List<int>(n);
            for (int i = 0; i < n; i++)
            {
                original.Add(i + 1);
            }

            permutations = Permutations(original);
            #region Comments

            //permutations.Add((int[])original.Clone());

            //for (int i = 0; i < n; i++)
            //{
            //    int[] temp_perm = (int[])original.Clone();
            //    int tmp = temp_perm[0];
            //    temp_perm[0] = temp_perm[i];
            //    temp_perm[i] = tmp;
            //    permutations.Add(temp_perm);

            //    for (int d = 1; d < n; d++)
            //    {
                    
            //    }
            //}

            //for (int i = 0; i < n; i++)
            //{
            //    int[] temp_perm = (int[])original.Clone();
            //    int tmp = temp_perm[0];
            //    temp_perm[0] = temp_perm[i];
            //    temp_perm[i] = tmp;
            //    permutations.Add(temp_perm);
            //    for (int j_hold = n - 1; j_hold > 0; j_hold--)
            //    {
            //        for (int j = n - 1; j > 0; j--)
            //        {
            //            if (j_hold != j)
            //            {
            //                int[] temp_perm2 = (int[])temp_perm.Clone();
            //                int tmp2 = temp_perm2[j_hold];
            //                temp_perm2[j_hold] = temp_perm2[j];
            //                temp_perm2[j] = tmp2;
            //                permutations.Add(temp_perm2);
            //            }
            //        }
            //    }
            //}

            //for (int i = 0; i < n; i++)
            //{
            //    int[] temp_perm = (int[])original.Clone();
            //    int tmp = temp_perm[0];
            //    temp_perm[0] = temp_perm[i];
            //    temp_perm[i] = tmp;
            //    //permutations.Add(temp_perm);
            //    //for (int j_hold = n - 2; j_hold > 0; j_hold--)
            //    //{
            //    //    for (int j = n - 1; j > j_hold; j--)
            //    //    {
            //    //        int[] temp_perm2 = (int[])temp_perm.Clone();
            //    //        if (j_hold != j)
            //    //        {
            //    //            int tmp2 = temp_perm2[j_hold];
            //    //            temp_perm2[j_hold] = temp_perm2[j];
            //    //            temp_perm2[j] = tmp2;
            //    //            permutations.Add(temp_perm2);
            //    //        }
            //    //    }
            //    //}
            //}

            #endregion

            //Post Check
            if (!(permutations.Count == Binomials.Factorial(n)))
                throw new Exception("Incorrect Permutation Result: incorrect total number of permutations.");

            return permutations;
        }

        private static List<int[]> Permutations(List<int> list)
        {
            List<int[]> ret_list = new List<int[]>();

            if (list.Count == 2)
            {
                ret_list.Add(list.ToArray());

                list.Reverse();
                ret_list.Add(list.ToArray());
                return ret_list;
            }
            else
            {
                for (int i = 0; i < list.Count; i++)
                {
                    List<int> t_list = new List<int>();
                    t_list.Add(list[i]);
                    for (int j = 0; j < list.Count; j++)
                    {
                        if (i != j)
                            t_list.Add(list[j]);
                    }

                    List<int[]> perms = Permutations(t_list.GetRange(1, list.Count - 1));
                    int tmp = t_list[0];
                    for (int j = 0; j < perms.Count; j++)
                    {
                        List<int> plist = perms[j].ToList<int>();
                        plist.Insert(0, tmp);
                        ret_list.Add(plist.ToArray());
                    }
                }

                return ret_list;
            }
        }

        #endregion

        #region Object Overrides

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder sbuilder = new StringBuilder();
            
            sbuilder.Append("Matrix: ");
            sbuilder.Append(Rows + "x" + Columns + " ");
            sbuilder.AppendLine();

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    sbuilder.Append(m_matrix[i,j] + " ");
                }
                sbuilder.AppendLine();
            }

            return sbuilder.ToString();
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            Matrix new_matrix = new Matrix(this.Rows, this.Columns);
            for (int i = 0; i < new_matrix.Rows; i++)
            {
                for (int j = 0; j < new_matrix.Columns; j++)
                {
                    new_matrix[i, j] = this[i, j];
                }
            }
            return new_matrix;
        }

        public Matrix CloneEmpty()
        {
            Matrix new_matrix = new Matrix(this.Rows, this.Columns);

            return new_matrix;
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (m_matrix as IEnumerable).GetEnumerator();
        }

        public virtual void Add(object o)
        {

        }

        #endregion

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(double[]));
                double[] array = (double[])serializer.Deserialize(reader);

                double rank = Math.Sqrt(array.Length);
                if(rank != (int)Math.Sqrt(array.Length))
                    throw new XmlDeserializationException("Bad Array Size, not square.");


            }
            catch(Exception exc)
            {
                throw new XmlDeserializationException("Matrix", exc);
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(double[]));
                serializer.Serialize(writer, Matrix.Flatten(m_matrix));
            }
            catch(Exception exc)
            {
                throw new XmlSerializationException("Matrix", exc);
            }
        }

        #endregion
    }
}
