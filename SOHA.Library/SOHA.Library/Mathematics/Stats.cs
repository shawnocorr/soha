﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SOHA.Library.Mathematics
{
    public class Stats<T> where T : struct, IComparable
    {
        readonly T[] m_initial_values;
        T m_mean = default(T);
        T m_median = default(T);
        T m_variance = default(T);
        T m_std_dev = default(T);

        public T Mean
        {
            get { return m_mean; }
        }

        public T Median
        {
            get { return m_median; }
        }

        public T StdDev
        {
            get { return m_std_dev; }
        }

        public T SDMedian
        {
            get 
            {
                dynamic d = m_median;

                if(d == 0)
                    return default(T);

                return m_std_dev / d; 
            }
        }

        public T SDMean
        {
            get
            {
                dynamic d = m_mean;

                if(d == 0)
                    return default(T);

                return m_std_dev / d;
            }
        }

        public Stats(T[] values)
        {
            m_initial_values = values;

            //return, maintaining defaults (should be zero)
            if(values.Count() == 0) return;

            foreach(T val in m_initial_values)
            {
                dynamic d_val = val;
                m_mean += d_val;
            }
            dynamic d_N = m_initial_values.Length;
            m_mean /= d_N;

            ArrayList list = new ArrayList();
            for(int i = 0; i < values.Length; i++)
            {
                list.Add(values[i]);
            }
            list.Sort();
            dynamic d_median = (T)list[list.Count / 2];
            m_median = d_median;

            dynamic var = m_median;
            dynamic sum = 0;
            for(int i = 0; i < values.Length; i++)
            {
                sum += (double)Math.Pow(values[i] - var, 2);
            }
            //m_variance = 
            m_std_dev = (T)Math.Sqrt(sum / values.Length);
        }
    }
}
