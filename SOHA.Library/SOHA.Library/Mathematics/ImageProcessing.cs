﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Mathematics;

namespace SOHA.Library.Mathematics
{
    public class ImageProcessing
    {

        //public static Matrix Wiener(Matrix m) { }

        public static Matrix MedianFilter(Matrix m, int nhood) 
        {
            Matrix Y = m.CloneEmpty();

            for (int i = 0; i < m.Rows; i++)
            {
                for (int j = 0; j < m.Columns; j++)
                {
                    Matrix hood = m.GetNeighborhood(i, j, nhood);
                    Vector v_hood = hood.ToVector();
                    double median = v_hood.Median();
                    Y[i, j] = median;
                }
            }

            return Y;
        }

        public static Matrix GaussFilter(Matrix M, FilterType type, double[] variance)
        {
            /* Create Low Pass Filter from variance */
            double[] gauss_lpf = null;
            double[] gauss_hpf = null;
            if (variance.Length == 1)
            {
                gauss_lpf = Gaussian.GaussFunction(variance[0]);
            }
            else
            {
                gauss_lpf = Gaussian.GaussFunction(variance[0]);
                gauss_hpf = Gaussian.GaussFunction(variance[1]);
            }

            /* Create Low Pass Filtered Image */

            Matrix image_lpf = FilterLS(M, gauss_lpf);

            switch (type)
            {
                case FilterType.Low:
                    return image_lpf;
                    break;
                case FilterType.High:
                    return (M - image_lpf);
                    break;
                case FilterType.Bandpass:
                    return FilterLS(image_lpf, gauss_hpf);
                    break;
                default:
                    return M;
                    break;
            }
        }

        public static Matrix FilterLS(Matrix M, double[] filter)
        {
            Matrix T = M.CloneEmpty();

            int filtmed = filter.Length / 2;

            for (int i = 0; i < M.Rows; i++)
            {
                for (int j = 0; j < M.Columns; j++)
                {
                    double val = M[i,j]*filter[filtmed];

                    for (int k = 1; k <= filtmed; k++)
                    {
                        if(j - k >= 0 && filtmed + k < filter.Length)
                            val += M[i, j - k] * filter[filtmed + k];

                        if(j + k < M.Columns && filtmed - k >= 0)
                            val += M[i, j + k] * filter[filtmed - k];
                    }

                    T[i, j] = val / filter.Sum();
                }
            }

            Matrix R = T.CloneEmpty();

            for (int i = 0; i < M.Columns; i++)
            {
                for (int j = 0; j < M.Rows; j++)
                {
                    double val = T[j, i] * filter[filtmed];

                    for (int k = 1; k <= filtmed; k++)
                    {
                        if (j - k >= 0 && filtmed + k < filter.Length)
                            val += T[j - k, i] * filter[filtmed + k];

                        if (j + k < M.Rows && filtmed - k >= 0)
                            val += T[j + k, i] * filter[filtmed - k];
                    }

                    R[j, i] = val / filter.Sum();
                }
            }

            return R;
        }

        public static Matrix HistogramEqualization(Matrix M)
        {
            int max = (int)MatrixFunctions.MaxMin(M)[0];

            Dictionary<int, int> PDF = new Dictionary<int, int>();

            foreach (double d in M)
            {
                int key = (int)d;
                if (!PDF.ContainsKey(key))
                {
                    PDF.Add(key, 1);
                }
                else
                {
                    PDF[key]++;
                }
            }

            Dictionary<int, int> CDF = new Dictionary<int, int>();
            int accum = 0;

            for (int i = 0; i <= max; i++)
            {
                CDF.Add(i, 0);

                if (PDF.ContainsKey(i))
                {
                    accum += PDF[i];
                }

                CDF[i] = accum;
            }

            Matrix Y = M.CloneEmpty();

            IEnumerable<KeyValuePair<int,int>> kvp_sort = CDF.Where<KeyValuePair<int, int>>(x => x.Value > 0).OrderBy<KeyValuePair<int, int>, int>(x => x.Value);

            int cdf_min = (kvp_sort.ToArray<KeyValuePair<int, int>>())[0].Value;
            double den = (double)(max - 1) / (double)(M.Rows * M.Columns - cdf_min);

            for (int i = 0; i < M.Rows; i++)
            {
                for (int j = 0; j < M.Columns; j++)
                {
                    int t = (int)M[i, j];

                    Y[i, j] = (CDF[t] - cdf_min) * den;
                }
            }

            return Y;
        }

        public static Matrix OpticalFlow(Matrix[] M)
        {
            Matrix I_x = M[0].CloneEmpty();
            Matrix I_y = M[0].CloneEmpty();
            Matrix I_t = M[0].CloneEmpty();

            /* Calculate I_x, I_y */
            for (int i = 0; i < M[0].Rows; i++)
            {
                for (int j = 0; j < M[0].Columns; j++)
                {
                    Matrix hood = M[0].GetNeighborhood(i, j, 3);
                    I_x[i, j] = (hood[0, 2] - hood[0, 0]) + (hood[1, 2] - hood[1, 0]) + (hood[2, 2] - hood[2, 0]);
                    I_y[i, j] = (hood[2, 0] - hood[0, 0]) + (hood[2, 1] - hood[0, 1]) + (hood[2, 2] - hood[0, 2]);
                }
            }

            /* Calculate I_t */
            for (int i = 0; i < M[0].Rows; i++)
            {
                for (int j = 0; j < M[0].Columns; j++)
                {
                    Matrix hood_1 = M[0].GetNeighborhood(i, j, 3);
                    Matrix hood_2 = M[1].GetNeighborhood(i, j, 3);
                    Matrix hood_3 = M[2].GetNeighborhood(i, j, 3);
                    Matrix dI_t = hood_2 - hood_1;
                    //double sum = 0;
                    //foreach (double d in dI_t)
                    //    sum += d;

                    I_t[i, j] = (hood_2[1, 1] - hood_1[1, 1]);
                }
            }

            Matrix Y = M[0].CloneEmpty();

            for (int i = 0; i < M[0].Rows; i++)
            {
                for (int j = 0; j < M[0].Columns; j++)
                {
                    Y[i, j] = (double)Math.Abs(-I_t[i, j] / Math.Sqrt(I_x[i, j] * I_x[i, j] + I_y[i, j] * I_y[i, j]));
                }
            }

            return I_t;
        }

        public static List<double> XExtrapolate(List<double> function, int n)
        {
            List<double> new_function = new List<double>(function.Count * n);

            for (int i = 0; i < function.Count; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    new_function.Add(function[i]);

                }
            }

            return new_function;
        }

        public static List<double> XExtrapolateInverse(List<double> function, int n)
        {
            List<double> new_function = new List<double>(function.Count / n);

            for (int i = 0; i < function.Count; i += n)
            {
                new_function.Add(function[i]);
            }

            return new_function;
        }
    }
}
