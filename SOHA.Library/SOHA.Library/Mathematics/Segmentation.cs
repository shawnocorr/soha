﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Mathematics
{
    public class Segmentation
    {

        public static Matrix BiLevel(Matrix A)
        {
            double[] extrema = MatrixFunctions.MaxMin(A);
            double t = (extrema[0] + extrema[1]) / 2;
            double new_t = 0;
            int iters = 0;

            do
            {
                double[] sums = new double[2];
                sums[0] = 0; sums[1] = 0;
                int[] level_count = new int[2];
                level_count[0] = 0; level_count[1] = 0;

                for (int i = 0; i < A.Rows; i++)
                {
                    for (int j = 0; j < A.Columns; j++)
                    {
                        int index = A[i,j] < t ? 0 : 1;

                        sums[index] += A[i, j];
                        level_count[index]++;
                    }
                }

                new_t = 0.5 * (sums[0] / level_count[0] + sums[1] / level_count[1]);

                if (new_t == t)
                    break;

                t = new_t;
                iters++;
            } while (iters < 50);

            Matrix Y = A.CloneEmpty();

            for (int i = 0; i < Y.Rows; i++)
            {
                for (int j = 0; j < Y.Columns; j++)
                {
                    Y[i,j] = A[i, j] < t ? extrema[1] : extrema[0];
                }
            }

            return Y;
        }

        public static Matrix NLevel(Matrix A, int levels)
        {
            double[] extrema = MatrixFunctions.MaxMin(A);
            double intervals = extrema[0] / levels;
            double[] t = new double[levels - 1];

            for (int i = 0; i < t.Length; i++)
            {
                t[i] = (i + 1) * intervals;
            }

            double[] new_t = new double[levels - 1];
            int iters = 0;

            do
            {
                double[] sums = new double[levels];
                sums.Initialize();
                int[] level_count = new int[levels];
                sums.Initialize();

                for (int i = 0; i < A.Rows; i++)
                {
                    for (int j = 0; j < A.Columns; j++)
                    {
                        int k = 0;
                        while (k < t.Length && A[i, j] > t[k])
                            k++;

                        sums[k] += A[i, j];
                        level_count[k]++;
                    }
                }

                for (int i = 0; i < new_t.Length; i++)
                {
                    new_t[i] = 0.5 * (sums[i] / level_count[i] + sums[i + 1] / level_count[i + 1]);
                }

                bool convergence = true;
                for (int i = 0; i < new_t.Length; i++)
                {
                    if (new_t[i] != t[i])
                    {
                        convergence = false;
                        break;
                    }
                }

                for (int i = 0; i < new_t.Length; i++)
                {
                    t[i] = new_t[i];
                }

                if (convergence)
                    goto populate;

                iters++;
            } while (true);

            populate:

            Matrix Y = A.CloneEmpty();

            for (int i = 0; i < Y.Rows; i++)
            {
                for (int j = 0; j < Y.Columns; j++)
                {
                    int k = 0;
                    while (k < t.Length && A[i, j] > t[k])
                        k++;

                    if (k > 0)
                    {
                        Y[i, j] = t[k - 1];
                    }
                    else
                    {
                        Y[i, j] = 0;
                    }
                }
            }

            return Y;
        }
    }
}
