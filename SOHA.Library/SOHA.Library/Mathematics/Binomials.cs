﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Mathematics
{
    public class Binomials
    {
        static List<int> Primes = new List<int>() { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };

        public static double[] BinomialPower(double[] v1, int x)
        {
            if (v1.Length != 2)
                throw new Exception("Not a Binomial.");

            if (x == 0)
                return new double[] { 1 };

            if (x == 1)
                return v1;

            double[] ret_vector = BinomialExpansion(x);

            for (int i = 0; i < ret_vector.Length; i++)
            {
                ret_vector[i] *= Math.Pow(v1[0], x - i) * Math.Pow(v1[1], i);
            }

            return ret_vector;
        }

        public static double[] BinomialExpansion(int n)
        {
            double[] num = new double[n + 1];
            for (int i = 0; i < num.Length; i++)
            {
                num[i] = CalculateBinomial(n, i);
            }
            return num;
        }

        public static double CalculateBinomial(int n, int k)
        {
            var numerator = new List<int>();
            var denominator = new List<int>();
            var denominatorOld = new List<int>();

            // again ignore the k! common terms
            for (int i = k + 1; i <= n; i++)
                numerator.Add(i);

            for (int i = 1; i <= (n - k); i++)
            {
                denominator.AddRange(SplitIntoPrimeFactors(i));
            }

            // remove all common factors
            int remainder;
            for (int i = 0; i < numerator.Count(); i++)
            {
                for (int j = 0; j < denominator.Count()
                    && numerator[i] >= denominator[j]; j++)
                {
                    if (denominator[j] > 1)
                    {
                        int result = Math.DivRem(numerator[i], denominator[j], out remainder);
                        if (remainder == 0)
                        {
                            numerator[i] = result;
                            denominator[j] = 1;
                        }
                    }
                }
            }

            float denominatorResult = 1;
            float numeratorResult = 1;

            denominator.RemoveAll(x => x == 1);
            numerator.RemoveAll(x => x == 1);

            denominator.ForEach(d => denominatorResult = denominatorResult * d);
            numerator.ForEach(num => numeratorResult = numeratorResult * num);

            return numeratorResult / denominatorResult;
        }

        private static List<int> SplitIntoPrimeFactors(int x)
        {
            var results = new List<int>();
            int remainder = 0;

            int i = 0;
            while (!Primes.Contains(x) && x != 1)
            {
                int result = Math.DivRem(x, Primes[i], out remainder);
                if (remainder == 0)
                {
                    results.Add(Primes[i]);
                    x = result;
                    i = 0;
                }
                else
                {
                    i++;
                }
            }
            results.Add(x);
            return results;
        }

        public static double Factorial(int n)
        {
            if (n <= 1)
            {
                return 1;
            }
            else
            {
                return n * Factorial(n - 1);
            }
        }
    }
}
