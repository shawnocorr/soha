﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Mathematics
{
    public class ArrayFunctions
    {

        public static List<double> Normalize(List<double> data)
        {
            double H = data[0]; double L = data[0];

            foreach (double d in data)
            {
                if (d > H)
                    H = d;

                if (d < L)
                    L = d;
            }

            double scale = H - L;

            for (int i = 0; i < data.Count; i++)
            {
                data[i] -= L;
                data[i] /= scale;
            }

            return data;
        }

        public static List<double> Normalize(List<double> data, out double max, out double min)
        {
            double H = data[0]; double L = data[0];

            foreach(double d in data)
            {
                if(d > H)
                    H = d;

                if(d < L)
                    L = d;
            }

            max = H;
            min = L;
            double scale_factor = H - L;

            for(int i = 0; i < data.Count; i++)
            {
                data[i] -= L;
                data[i] /= scale_factor;
            }

            return data;
        }

        public static List<double> Normalize(List<double> data, double max, double min)
        {
            for(int i = 0; i < data.Count; i++)
            {
                data[i] -= min;
                data[i] /= (max - min);
            }

            return data;
        }

        public static List<Interval> MaxMinList(List<double> data)
        {
            List<Interval> maxmin = new List<Interval>();

            int depth = 5;

            for(int i = depth; i < data.Count - depth; i++)
            {
                double sum = 0;
                for(int j = i - depth; j < i; j++)
                {
                    sum += (data[j] - data[j+1]);
                }
                double delta_pre = sum / (depth + 1);

                sum = 0;
                for(int j = i; j < i + depth; j++)
                {
                    sum += (data[j] + data[j+1]);
                }
                double delta_post = sum / (depth + 1);

                if(delta_pre > 0 && delta_post < 0)
                {
                    maxmin.Add(new Interval(i, IntervalType.Diastole));
                    i += depth;
                }

                if(delta_pre < 0 && delta_post > 0)
                {
                    maxmin.Add(new Interval(i, IntervalType.Systole));
                    i += depth;
                }
            }

            return maxmin;
        }

        public static void Invert(ref List<double> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                data[i] = 1 - data[i];
            }
        }

        public static double[] PlusMinusWindow(double[] data, int sd)
        {
            double mean = Statistics.Mean(data);
            double window = sd * Statistics.StdDev(data);
            double hi = mean + window;
            double lo = mean - window;

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] > hi)
                    data[i] = hi;

                if (data[i] < lo)
                    data[i] = lo;
            }

            return data;
        }

        public static List<double> RShift(List<double> func,int shift)
        {
            List<double> new_func = new List<double>();

            for(int i =0;i < func.Count + shift;i++)
            {
                if (i < shift)
                {
                    new_func.Add(func[0]);
                }
                else
                {
                    new_func.Add(func[i - shift]);
                }
            }

            return new_func;
        }
    }
}
