﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Video;

namespace SOHA.Library
{

    public enum ProcessEventArgsType
    {
        Load,
        Preprocess
    }

    public class ProcessEventArgs : EventArgs
    {

        public ProcessEventArgsType EventType { get; private set; }
        public PreprocessArguments Arguments { get; private set; }

        public ProcessEventArgs()
            : base()
        {

        }

        public ProcessEventArgs(ProcessEventArgsType type)
            : base()
        {
            EventType = type;
        }

        public ProcessEventArgs(ProcessEventArgsType type, PreprocessArguments args)
            : base()
        {
            EventType = type;
            Arguments = args;
        }

    }
}
