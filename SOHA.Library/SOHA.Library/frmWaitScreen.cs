﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHA.Library
{
    public partial class frmWaitScreen : Form
    {
        private Cursor m_original;

        public frmWaitScreen()
        {
            InitializeComponent();
        }

        public frmWaitScreen(string label, string title)
        {
            InitializeComponent();

            this.Shown += new EventHandler(frmWaitScreen_Shown);
            this.FormClosed += new FormClosedEventHandler(frmWaitScreen_FormClosed);
            this.Text = title;
            lblTitle.Text = label;
            
            m_original = Cursor.Current;

            this.BringToFront();
            this.CenterToParent();
            this.TopMost = true;
            //this.ShowDialog();
        }

        void frmWaitScreen_FormClosed(object sender, FormClosedEventArgs e)
        {
            Cursor.Current = m_original;
        }

        private void frmWaitScreen_Shown(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
        } 
    }
}
