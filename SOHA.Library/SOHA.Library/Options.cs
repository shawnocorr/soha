﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SOHA.Library
{
    public static class Options
    {
        private static bool m_initialized = false;
        private static bool m_auto_gui = true;
        private static bool m_auto_process = true;
        private static bool m_auto_analyze = false;
        private static int m_skip_frames = 10;
        private static int m_process_queue_size = 4;

        public static bool AutoGUI 
        { 
            get
            {
                return m_auto_gui;
            } 
            set
            {
                m_auto_gui = value;
            } 
        }
        /// <summary>
        /// Automatically Preprocess file after Mark Phase
        /// </summary>
        public static bool AutoProcess
        {
            get
            {
                return m_auto_process;
            }
            set
            {
                m_auto_process = value;
            }
        }
        public static bool AutoPreAnalyze
        {
            get
            {
                return m_auto_analyze;
            }
            set
            {
                m_auto_analyze = value;
            }
        }
        public static int SkipFrames
        {
            get
            {
                return m_skip_frames;
            }
            set
            {
                m_skip_frames = value;
            }
        }
        public static int MaxProcessQueueSize
        {
            get
            {
                return m_process_queue_size;
            }
            set
            {
                m_process_queue_size = value;
            }
        }

        public static void WriteINI()
        {
            string INI_PATH = Path.GetDirectoryName(Application.ExecutablePath) + @"\options.ini";

            using(StreamWriter writer = new StreamWriter(INI_PATH))
            {
                writer.WriteLine("[GlobalOptions]");
                writer.WriteLine("AutoGUI=" + AutoGUI.ToString());
                writer.WriteLine("AutoProcess=" + AutoProcess.ToString());
                writer.WriteLine("AutoPreAnalyze=" + AutoPreAnalyze.ToString());
                writer.WriteLine("SkipFrames=" + SkipFrames.ToString());
                writer.WriteLine("MaxProcessQueueSize=" + MaxProcessQueueSize.ToString());
            }

        }

        public static void ReadINI()
        {
            string INI_PATH = Path.GetDirectoryName(Application.ExecutablePath) + @"\options.ini";

            //DEFAULTS
            if(!File.Exists(INI_PATH))
            {
                WriteINI();
            }

            if(m_initialized)
                throw new Exception("Attempted Re-initialization");

            using(StreamReader reader = new StreamReader(INI_PATH))
            {
                string line = "";
                while((line = reader.ReadLine()) != null)
                {
                    if(line.Contains("AutoGUI"))
                        AutoGUI = Boolean.Parse(line.Substring(line.IndexOf('=') + 1));

                    if(line.Contains("AutoProcess"))
                        AutoProcess = Boolean.Parse(line.Substring(line.IndexOf('=') + 1));
                    
                    if(line.Contains("SkipFrames"))
                        SkipFrames = int.Parse(line.Substring(line.IndexOf('=') + 1));

                    if(line.Contains("MaxProcessQueueSize"))
                        MaxProcessQueueSize = int.Parse(line.Substring(line.IndexOf('=') + 1));

                    if(line.Contains("AutoPreAnalyze"))
                        AutoPreAnalyze = Boolean.Parse(line.Substring(line.IndexOf('=') + 1));
                }
            }

            m_initialized = true;
        }
    }
}
