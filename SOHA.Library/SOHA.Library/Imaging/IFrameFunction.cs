﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHA.Library.Imaging
{
    public interface IFrameFunction
    {
        [Browsable(true)]
        [ReadOnly(true)]
        FunctionType FuncType { get; set; }
        [Browsable(true)]
        [ReadOnly(true)]
        bool Enable { get; set; }
        [Browsable(true)]
        [ReadOnly(true)]
        string NickName { get; set; }
        
        string ToString();
        Frame Execute(Frame frm);
    }
}
