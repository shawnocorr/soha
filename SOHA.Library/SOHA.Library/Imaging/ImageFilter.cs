﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.GPU;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Drawing;
using System.Xml.Serialization;
using SOHA.Library.Xml;
using SOHA.Library.Mathematics;

namespace SOHA.Library.Imaging
{
    [Serializable]
    [XmlRoot("ImageFilter")]
    public class ImageFilter : Matrix, IFrameFunction, /*IFilter,*/ ISerializable, IXmlSerializable
    {
        private const int MAX_SIZE = 7;

        private TextBox[,] m_filter_textboxes;

        public int Size { get; protected set; }

        public double Amplitude
        {
            get
            {
                double sum = 0;
                foreach(double f in this)
                {
                    sum += f;
                }
                return sum;
            }
        }

        public string NickName
        {
            get;
            set;
        }

        public ImageFilter()
            :base()
        {
            Size = 1;
        }

        public ImageFilter(int size)
            : base(size)
        {
            if (size < 3)
                throw new Exception("Size less than 3, cannot create ImageFilter.");

            if (size % 2 != 1)
                throw new Exception("Size must be odd, cannot create ImageFilter.");

            m_filter_textboxes = new TextBox[size, size];
            Size = size;
        }

        public ImageFilter(Matrix A)
            : base()
        {
            if (!A.isSquare)
                throw new Exception("Matrix is not square, cannot create ImageFilter.");

            if (A.Rows < 3)
                throw new Exception("Size less than 3, cannot create ImageFilter.");

            if (A.Rows % 2 != 1)
                throw new Exception("Size must be odd, cannot create ImageFilter.");

            m_filter_textboxes = new TextBox[A.Rows, A.Rows];
            m_matrix = A;
            Size = A.Rows;
        }

        protected ImageFilter(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            Size = int.Parse(info.GetString("Size"));
            NickName = info.GetString("NickName");
            m_matrix = (double[,])info.GetValue("Matrix",typeof(double[,]));
        }



        public static ImageFilter Normalize(ImageFilter filter)
        {
            return new ImageFilter((1 / filter.Amplitude) * (Matrix)filter);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sigma"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static ImageFilter Gaussian2D(float sigma, int k)
        {
            Matrix m = new Matrix(2 * k + 1);
            double two_sigma_squared = 2 * Math.Pow(sigma, 2);
            for(int i = 0; i < (2 * k + 1); i++)
            {
                for(int j = 0; j < (2 * k + 1); j++)
                {
                    //Add one to i and j in the formula, base 1 array counting (starts at 1, not zero)
                    m[i, j] = (1 / (two_sigma_squared * Math.PI)) * Math.Exp(-(Math.Pow((i+1) - k - 1, 2) + Math.Pow((j+1) - k - 1, 2)) / two_sigma_squared);
                }
            }

            //int n = (int)(6 * sigma + 1);

            //double[] gauss1D = Gaussian.GaussFunction(sigma);
            //Vector g = new Vector(gauss1D);
            //int length = gauss1D.Length;
            //int start = (length - N) / 2;
            //g = Vector.SubArray(g, start, N);

            //Matrix M = (Matrix)g * ~g;
            ImageFilter filter = new ImageFilter(m);

            return ImageFilter.Normalize(filter);
        }

        public static double[] RGBFilter(ImageFilter filter)
        {
            int size = filter.Size;
            double[] Filters = new double[3 * size * size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Filters[3 * (i * size + j)] = filter[i, j]; //red component
                    Filters[3 * (i * size + j) + 1] = filter[i, j]; //green
                    Filters[3 * (i * size + j) + 2] = filter[i, j]; //blue
                }
            }

            return Filters;
        }

        public static explicit operator CLFilter(ImageFilter filter)
        {
            if(filter.Size > MAX_SIZE)
                throw new MathematicsException("Filter Size > MAX_SIZE");

            float[] Filters = new float[3 * MAX_SIZE * MAX_SIZE];

            int border = (MAX_SIZE - filter.Size) / 2;

            for(int i = border; i < MAX_SIZE - border; i++)
            {
                for(int j = border; j < MAX_SIZE - border; j++)
                {
                    float val = (float)filter[i - border, j - border];

                    Filters[3 * (i * MAX_SIZE + j)] = val; //red component
                    Filters[3 * (i * MAX_SIZE + j) + 1] = val; //green
                    Filters[3 * (i * MAX_SIZE + j) + 2] = val; //blue
                }
            }

            return new CLFilter(Filters);
        }
        
        public static ImageFilter operator *(double r, ImageFilter A)
        {
            if(r == 0)
                return new ImageFilter(1);

            if(r == 1)
                return A;

            if(A.Rows == 0 || A.Columns == 0)
                throw new InvalidOperationException("Invalid Matrix Dimensions: Matrix A.");

            ImageFilter Y = new ImageFilter(A.Size);
            for(int i = 0; i < Y.Rows; i++)
            {
                for(int j = 0; j < Y.Columns; j++)
                {
                    Y[i, j] = r * A[i, j];
                }
            }
            return Y;
        }

        public override string ToString()
        {
            return NickName + " [" + this.Size + "x" + this.Size + "]";
        }

        public string ToTooltipString()
        {
            StringBuilder bldr = new StringBuilder();
            //bldr.Append("\t\t");

            for(int i = 0; i < MAX_SIZE; i++)
            {
                for(int j = 0; j < MAX_SIZE; j++)
                {
                    bldr.Append(Math.Round(this[i, j], 2));
                    bldr.Append("\t");
                }
                bldr.AppendLine();
            }

            return bldr.ToString();
        }

        public TableLayoutPanel ToTable()
        {
            TableLayoutPanel pnlFilterValues = new TableLayoutPanel();
            pnlFilterValues.RowCount = pnlFilterValues.ColumnCount = this.Rows;

            for(int i = 0; i < m_filter_textboxes.GetLength(0); i++)
            {
                for(int j = 0; j < m_filter_textboxes.GetLength(0); j++)
                {
                    m_filter_textboxes[i, j] = new TextBox();
                    m_filter_textboxes[i, j].Size = new Size(40, 20);
                    m_filter_textboxes[i, j].Text = this[i, j].ToString();
                    m_filter_textboxes[i, j].TextChanged += new EventHandler(ImageFilter_TextChanged);

                    pnlFilterValues.Controls.Add(m_filter_textboxes[i, j]);
                }
            }

            return pnlFilterValues;
        }

        void ImageFilter_TextChanged(object sender, EventArgs e)
        {
            double[,] old_matrix = (double[,])m_matrix.Clone();

            try
            {
                for(int i = 0; i < m_filter_textboxes.GetLength(0); i++)
                {
                    for(int j = 0; j < m_filter_textboxes.GetLength(0); j++)
                    {
                        m_matrix[i, j] = double.Parse(m_filter_textboxes[i, j].Text);
                    }
                }
            }
            catch(Exception exc)
            {
                m_matrix = old_matrix;
            }
        }

        //#region IFilter Members

        //public CLImage PerformFilter(CLImage image)
        //{
        //    return ((CLFilter)this).PerformFilter(image);
        //}

        //#endregion

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if(info == null)
                throw new ArgumentNullException("info");

            info.AddValue("Size", Size);
            info.AddValue("NickName", NickName);
            info.AddValue("Matrix", m_matrix);
        }

        #endregion

        public override void Add(object o)
        {
            base.Add(o);
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            try
            {
                Size = Int32.Parse(reader.GetAttribute("Size"));
                
                //XmlSerializer serializer = new XmlSerializer(typeof(double[]));
                //m_matrix = Matrix.Extrude((double[])serializer.Deserialize(reader),Size);

                XmlSerializer serializer = new XmlSerializer(typeof(Matrix));
                Matrix A = (Matrix)serializer.Deserialize(reader);
                m_matrix = A;

            }
            catch(Exception exc)
            {
                throw new XmlDeserializationException("ImageFilter", exc);
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            try
            {
                writer.WriteAttributeString("Size", Size.ToString());

                base.WriteXml(writer);
                //XmlSerializer serializer = new XmlSerializer(typeof(Matrix));
                //serializer.Serialize(writer, (Matrix)this);
            }
            catch(Exception exc)
            {
                throw new XmlSerializationException("ImageFilter", exc);
            }
        }

        #endregion

        #region IFrameFunction Members

        //Default On
        private bool m_enable = true;

        //Default Basic Function
        private FunctionType m_type = FunctionType.Filter;
        
        public FunctionType FuncType
        {
            get
            {
                return m_type;
            }
            set
            {
                m_type = value;
            }
        }


        public bool Enable
        {
            get
            {
                return m_enable;
            }
            set
            {
                m_enable = value;
            }
        }

        public Frame Execute(Frame frm)
        {
            FrameFilter tmp = new FrameFilter(this);

            return tmp.Execute(frm);
            //throw new NotImplementedException();
        }

        #endregion
    }
}
