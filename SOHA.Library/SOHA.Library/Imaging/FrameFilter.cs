﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Mathematics;

namespace SOHA.Library.Imaging
{
    public class FrameFilter : BFrameFunction
    {

        private ImageFilter m_filter = new ImageFilter();

        public FrameFilter()
        {
        }

        public FrameFilter(ImageFilter imgFilter)
        {
            m_filter = imgFilter;
        }

        public static implicit operator Matrix(FrameFilter a)
        {
            return new Matrix(a.m_filter.Data);
        }

        public override Frame Execute(Frame frm)
        {
            //IF disabled, return input Frame
            if(!Enable)
                return frm;

            frm.Filter(m_filter);
            return frm;
        }
    }
}
