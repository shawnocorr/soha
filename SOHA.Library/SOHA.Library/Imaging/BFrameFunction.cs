﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SOHA.Library.Imaging
{
    public abstract class BFrameFunction : IFrameFunction
    {
        //Default On
        private bool m_enable = true;

        //Default Basic Function
        private FunctionType m_type = FunctionType.Function;

    
        #region IFrameFunction Members

        public string NickName
        {
            get;
            set;
        }

        public bool Enable
        {
            get
            {
                return m_enable;
            }
            set
            {
                m_enable = value;
            }
        }

        public abstract Frame Execute(Frame frm);

        public FunctionType FuncType
        {
            get
            {
                return m_type;
            }
            set
            {
                m_type = value;
            }
        }

        public virtual string ToString()
        {
            return "BaseFrameFunction";
        }

        #endregion
    }
}
