﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SOHA.Library.Imaging
{
    public class Histogram
    {
        public Dictionary<int, int> PDF = new Dictionary<int, int>();
        public Dictionary<int, int> CDF = new Dictionary<int, int>();

        public Histogram()
        {

        }

        public Histogram(Frame pframe)
        {
            int max = (int)pframe.Max();
            //int cdf_value = 0;

            for(int x = 0; x <= max; x++)
            {
                PDF.Add(x, 0);
                CDF.Add(x, 0);
            }

            //PDF
            for(int i = 0; i < pframe.PixelValues.GetLength(1); i++)
            {
                for(int j = 0; j < pframe.PixelValues.GetLength(0); j++)
                {
                    int val = (int)pframe[j, i];

                    PDF[val]++;
                }
            }

            //CDF
            int sum = 0;
            for(int i = 0; i <= max; i++)
            {
                if(PDF.ContainsKey(i))
                    sum += PDF[i];

                CDF[i] = sum;
            }
        }

        public Dictionary<int, int> TrimPDF()
        {
            return (Dictionary<int, int>)PDF.Where(x => x.Value != 0).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        public Bitmap GetGraph()
        {
            //if(PDF.Keys

            int max = PDF.Max(x => x.Value);

            max = 300; // (int)(Math.Ceiling(max / nearest_100) * 100);

            Frame histBin = Frame.BlankFrame(PDF.Keys.Count, max, 255);

            //Draw PDF
            foreach(KeyValuePair<int, int> bin in PDF)
            {
                for(int i = 0; i < bin.Value; i++)
                {
                    int val = (int)(max - i - 1);

                    if(val >= 0)
                        histBin[bin.Key, val] = 128;
                }
            }

            //Draw CDF
            //int max_value = CDF.Max(x => x.Value);
            //foreach(KeyValuePair<int, int> bin in CDF)
            //{
            //    int val = (int)(bin.Value / (double)max * 100);
            //    histBin[bin.Key, (int)(max - val - 1)] = 0;
            //}

            return histBin.ToBitmap();
        }
    }
}
