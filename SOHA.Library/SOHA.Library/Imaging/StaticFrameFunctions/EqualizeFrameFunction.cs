﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using SOHA.Library.Xml;

namespace SOHA.Library.Imaging.StaticFrameFunctions
{
    [Serializable]
    [XmlRoot("EqualizeFrameFunction")]
    public sealed class EqualizeFrameFunction : FrameFunction, ISerializable, IXmlSerializable
    {

        public EqualizeFrameFunction() 
        {
            NickName = "Equalize";
        }

        public override Frame Execute(Frame frm)
        {
            frm.HistogramEqualize();

            return frm;
        }

        public override string ToString()
        {
            return NickName;
        }

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if(info == null)
                throw new ArgumentNullException("info");

            NickName = info.GetString("NickName");
        }

        #endregion

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(EqualizeFrameFunction));
                EqualizeFrameFunction A = (EqualizeFrameFunction)serializer.Deserialize(reader);
                NickName = A.NickName;

            }
            catch(Exception exc)
            {
                throw new XmlDeserializationException("EqualizeFrameFunction", exc);
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            try
            {
                writer.WriteAttributeString("NickName", NickName.ToString());

                //base.WriteXml(writer);
                //XmlSerializer serializer = new XmlSerializer(typeof(Matrix));
                //serializer.Serialize(writer, (Matrix)this);
            }
            catch(Exception exc)
            {
                throw new XmlSerializationException("EqualizeFrameFunction", exc);
            }
        }

        #endregion
    }
}
