﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using SOHA.Library.Xml;

namespace SOHA.Library.Imaging.StaticFrameFunctions
{
    [Serializable]
    [XmlRoot("SegmentFrameFunction")]
    public class SegmentFrameFunction : FrameFunction, ISerializable, IXmlSerializable
    {

        private int m_segments = 4;

        public SegmentFrameFunction() 
        {
            NickName = "Segmentation";
        }

        public SegmentFrameFunction(int segments)
        {
            m_segments = segments;
            NickName = "Segmentation";
        }

        public override Frame Execute(Frame frm)
        {
            return frm.Bin(m_segments);
        }

        public override string ToString()
        {
            return NickName + " [" + m_segments + "]";
        }

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if(info == null)
                throw new ArgumentNullException("info");

            NickName = info.GetString("NickName");
        }

        #endregion

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SegmentFrameFunction));
                SegmentFrameFunction A = (SegmentFrameFunction)serializer.Deserialize(reader);
                NickName = A.NickName;

            }
            catch(Exception exc)
            {
                throw new XmlDeserializationException("SegmentFrameFunction", exc);
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            try
            {
                writer.WriteAttributeString("NickName", NickName.ToString());

                //base.WriteXml(writer);
                //XmlSerializer serializer = new XmlSerializer(typeof(Matrix));
                //serializer.Serialize(writer, (Matrix)this);
            }
            catch(Exception exc)
            {
                throw new XmlSerializationException("SegmentFrameFunction", exc);
            }
        }

        #endregion
    }
}
