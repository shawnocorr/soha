﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;

namespace SOHA.Library
{
    /// <summary>
    /// Simulates a queue as a List object. FIFO preprocessing.
    /// 
    /// In use as of 1/31/2016
    /// </summary>
    public class ProcessQueue : List<IProcessable>, ITraceable
    {
        public event EventHandler Complete;
        public event EventHandler AllComplete;

        public static int MaximumTasks = 2;

        private static object asyncLock = new object();

        private Queue<ProcessEventArgs> Arguments = new Queue<ProcessEventArgs>();
        private List<IProcessable> RunningTasks = new List<IProcessable>();

        private bool m_isCancelling = false;
        private bool m_pause = true;

        public int Count
        {
            get
            {
                return base.Count;
            }
        }
        public bool IsEmpty
        {
            get
            {
                return base.Count == 0;
            }
        }
        public ProcessingQueueStatus Status
        {
            get;
            protected set;
        }
        public int Tasks
        {
            get
            {
                return RunningTasks.Count;
            }
        }

        public ProcessQueue() 
        {
            Status = ProcessingQueueStatus.Idle;
        }

        public ProcessQueue(bool delay)
        {
            m_pause = delay;

            Status = ProcessingQueueStatus.Idle;
        }

        public void Enqueue(IProcessable process, ProcessEventArgs args)
        {
            base.Add(process);
            Arguments.Enqueue(args);

            while (base.Count > 0 && RunningTasks.Count < MaximumTasks && !m_pause)
                Dequeue();
        }

        public void Dequeue()
        {
            IProcessable process = base[0];
            base.Remove(process);
            ProcessEventArgs args = Arguments.Dequeue();

            //process.UpdateProcess += new EventHandler(process_UpdateProcess);
            process.EndProcess += new AsyncEventHandler(process_EndProcess);

            CancellationTokenSource token_source = new CancellationTokenSource();
            CancellationToken cancel_token = token_source.Token;

            lock (asyncLock)
            {
                Task new_Task = new Task(()
                    => process.Run(args, token_source), cancel_token, TaskCreationOptions.LongRunning);
                RunningTasks.Add(process);
                new_Task.Start();
            }

            if(Status != ProcessingQueueStatus.Running)
                Status = ProcessingQueueStatus.Running;

            if (m_isCancelling)
                token_source.Cancel(true);
        }

        public void Start()
        {
            if(base.Count > 0
                && RunningTasks.Count < MaximumTasks
                && m_pause)
            {
                m_pause = false;
                Dequeue();
            }
        }

        public void Cancel(IProcessable process)
        {
            if (RunningTasks.Contains(process))
            {
                process.CancelToken.Cancel(true);
            }
        }

        public void Cancel(IEnumerable<IProcessable> processes)
        {
            foreach(IProcessable process in processes)
            {
                if(RunningTasks.Contains(process))
                {
                    process.CancelToken.Cancel(true);
                }
                else
                {
                    //Remove from queue
                    base.Remove(process);
                    process.OnEnd(new AsyncCompletedEventArgs(null, true, new object()));
                }
            }
        }

        public void CancelAll()
        {
            m_isCancelling = true;

            IEnumerable<IProcessable> queued_tasks = this.Except<IProcessable>(RunningTasks);

            //Empty the Queue
            base.Clear();
            foreach(IProcessable process1 in this)
                process1.OnEnd(new AsyncCompletedEventArgs(null, true, new object()));

            foreach (IProcessable process in RunningTasks)
                process.CancelToken.Cancel(true);
        }

        void process_EndProcess(object sender, AsyncCompletedEventArgs e)
        {
            IProcessable process = sender as IProcessable;
            RunningTasks.Remove(process);
            OnComplete(e);

            if (!IsEmpty)
            {
                while(base.Count > 0 && RunningTasks.Count < MaximumTasks)
                {
                    Dequeue();
                }
            }
            else
            {
                Status = ProcessingQueueStatus.Idle;
                OnAllComplete(new EventArgs());
                m_isCancelling = false;
            }
        }

        public event TraceEventHandler Trace;

        protected virtual void OnAllComplete(EventArgs e)
        {
            if(AllComplete != null)
                AllComplete(this, e);
        }

        protected virtual void OnComplete(EventArgs e)
        {
            if(Complete != null)
                Complete(this, e);
        }

        public void OnTrace(TraceEventArgs e)
        {
            if (Trace != null)
                Trace(this, e);
        }
    }
}
