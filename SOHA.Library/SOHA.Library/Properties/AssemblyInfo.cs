﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SOHA.Library")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("SOHA.Library")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("eb4df879-dae7-4adc-933f-7c59a65a243a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.14")]
[assembly: AssemblyFileVersion("1.1.0.14")]

/*
 * 1.0.0.1
 * 
 *  Added frame skipping in Run functions
 */

/*
 * 1.0.0.3
 * 
 *  Added Frame skipping to MMode production in CXD and AVI video
 */

/*
 * 1.0.0.4
 * 
 *  Added ShowPhase and Offset properties to IntervalSettings
 */

/*
 * 1.0.0.6
 * 
 *  Added TypeConverter, ExpandableObjectConverter: made classes able to be displayed in PropertyGrid
 *  Created Snapshot function for Video objects
 */

/*
 * 1.0.0.7
 * 
 * Added Clean() function to IntervalCollection to fix issue with Change intervals getting saved with Interval Phase 
 * 
 */

/*  1.1.0.0:   Rollout 3/2/2015
 * 
 *  1.1.0.1:    
 * 
 *  1.1.0.2:    Edited frame time grabbing in CXDVideo Run() function. Frame times are now retrieved for all frames
 *              regardless of frame skipping. The Filegram ToDataRow() function now implements calculcations that reflect
 *              this change.
 */