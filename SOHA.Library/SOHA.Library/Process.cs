﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;

namespace SOHA.Library
{
    public abstract class Processable : IProcessable
    {
        [Browsable(false)]
        public CancellationTokenSource CancelToken
        {
            get;
            protected set;
        }

        public event EventHandler ProgressBegin;
        public event ProcessProgressUpdate ProgressUpdate;
        public event ProcessComplete ProcessComplete;

        public abstract void Run(ProcessEventArgs arg, CancellationToken cancel_token);
        public abstract void Run(ProcessEventArgs args, CancellationTokenSource cancel_token);
        public abstract void TestRun(ProcessEventArgs args, CancellationTokenSource cancel_token);

        protected virtual void OnUpdate(object obj, int pct)
        {
            if (ProgressUpdate != null)
                ProgressUpdate(obj, pct);
        }

        protected virtual void OnComplete(ProgressEventArgs e)
        {
            if (ProcessComplete != null)
                ProcessComplete(this, e);
        }

        public event EventHandler BeginProcess;

        public event AsyncEventHandler UpdateProcess;

        public event AsyncEventHandler EndProcess;


        public virtual void OnBegin(EventArgs e)
        {
            if(ProgressBegin != null)
                ProgressBegin(this, e);
        }

        public virtual void OnUpdate(AsyncCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public virtual void OnEnd(AsyncCompletedEventArgs e)
        {
            if(EndProcess != null)
                EndProcess(this, e);
        }
    }
}
