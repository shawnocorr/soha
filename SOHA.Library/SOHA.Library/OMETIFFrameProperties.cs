﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace SOHA.Library
{
    public class OMETIFFrameProperties
    {
        [JsonProperty(PropertyName = "Interval_ms")]
        public int FrameInterval { get; set; }
    }
}
