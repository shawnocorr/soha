﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace SOHA.Library.Utilities
{
    public class PCProperties
    {
        private static List<object> Win32Objects = new List<object>(){
                "Win32_DiskDrive",
                "Win32_OperatingSystem",
                "Win32_Processor",
                "Win32_ComputerSystem",
                "Win32_StartupCommand",
                "Win32_ProgramGroup",
                "Win32_SystemDevices"};

        public static Dictionary<string, Dictionary<string, object>> AllObjects = new Dictionary<string, Dictionary<string, object>>();

        public PCProperties()
        {

        }

        public static void GetProperties()
        {
            ManagementObjectSearcher searcher;
            int i = 0;
            try
            {
                foreach(object win32 in Win32Objects)
                {
                    AllObjects.Add((string)win32, new Dictionary<string, object>());
                    searcher = new ManagementObjectSearcher(
                      "SELECT * FROM " + (string)win32);
                    foreach(ManagementObject wmi_HD in searcher.Get())
                    {
                        i++;
                        PropertyDataCollection searcherProperties =
                          wmi_HD.Properties;
                        foreach(PropertyData sp in searcherProperties)
                        {
                            if(!AllObjects[(string)win32].ContainsKey(sp.Name))
                                AllObjects[(string)win32].Add(sp.Name, sp.Value);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                //Don't care, return what you can... no need to go crazy
            }
        }
    }
}
