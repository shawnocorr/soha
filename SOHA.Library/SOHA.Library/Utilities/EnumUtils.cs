﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.Utilities
{
    public static class EnumUtils
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T Next<T>(T current)
        {
            var values = Enum.GetValues(typeof(T)).Cast<T>();
            int index_of_current = values.ToList<T>().IndexOf(current);
            int index_of_next = index_of_current + 1;

            if(index_of_current >= 0)
            {//Ensure current value was found in array
                if(index_of_next >= values.Count()) index_of_next = 0;

                return values.ToList<T>().ElementAt<T>(index_of_next);
            }

            return current;
        }

        public static T Previous<T>(T current)
        {
            var values = Enum.GetValues(typeof(T)).Cast<T>();
            int index_of_current = values.ToList<T>().IndexOf(current);
            int index_of_next = index_of_current - 1;

            if(index_of_current >= 0)
            {//Ensure current value was found in array
                if(index_of_next < 0) index_of_next = values.Count() - 1;

                return values.ToList<T>().ElementAt<T>(index_of_next);
            }

            return current;
        }
    }
}
