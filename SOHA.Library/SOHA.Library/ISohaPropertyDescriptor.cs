﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHA.Library
{
    public interface ISohaPropertyDescriptor
    {
        [Browsable(false)]
        [ReadOnly(true)]
        string DisplayName { get; }

        [Browsable(false)]
        [ReadOnly(true)]
        string Description { get; }
    }
}
