﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using SOHA.Library.Xml;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;

namespace SOHA.Library
{
    [XmlRoot("CameraCollection")]
    public class CameraCollection
    {
        private static SohaCollection<Camera> DEFAULTS = new SohaCollection<Camera>()
        {
            new Camera(true, "9300-1", 1.1627),
            new Camera(false, "9300-2", 1.0938),
            new Camera(false, "9100", 1.5625)
        };

        private static SohaCollection<Camera> m_list = null;

        public static SohaCollection<Camera> List
        {
            get
            {
                if(m_list == null)
                {
                    Read();
                }

                return m_list;
            }
        }

        public static Camera Default
        {
            get
            {
                return m_list.Where<Camera>(x => x.Default).First();
            }
        }

        private static void Read() 
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\SOHA\cameras.xml";

                if(!File.Exists(path))
                {
                    m_list = DEFAULTS;
                }
                else
                {
                    using(XmlTextReader reader = new XmlTextReader(path))
                    {
                        XmlSerializer deserializer = new XmlSerializer(typeof(SohaCollection<Camera>));
                        m_list = (SohaCollection<Camera>)deserializer.Deserialize(reader);
                    }
                }
            }
            catch(Exception exc)
            {

            }
        }

        public static void Write()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SohaCollection<Camera>));
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\SOHA\cameras.xml";
            using(XmlTextWriter writer = new XmlTextWriter(path, Encoding.UTF8))
            {
                serializer.Serialize(writer, m_list);
            }
        }
    }
}
