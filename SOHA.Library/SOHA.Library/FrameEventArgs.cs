﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library
{    
    public class FrameEventArgs : EventArgs
    {
        protected Frame m_frame;

        public Frame Frame
        {
            get
            {
                return m_frame;
            }
        }

        public FrameEventArgs(Frame frame)
        {
            m_frame = frame;
        }
    }
}
