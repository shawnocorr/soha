﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library
{
    public class Region : SohaObject
    {
        public int Start { get; set; }
        public int End { get; set; }

        public Region() { }

        public Region(int start, int end)
        {
            Start = start;
            End = end;
        }

        public override string DisplayName
        {
            get { return "{Region:" + Start + "; " + End + "}"; }
        }

        public override string Description
        {
            get { return "Region"; }
        }
    }
}
