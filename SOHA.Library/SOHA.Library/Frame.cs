﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices.ComTypes;
using SOHA.Library;
using System.Xml.Serialization;
using System.Drawing;
using System.Drawing.Imaging;
using SOHA.Library.Mathematics;
using SOHA.Library.GPU;
using System.Collections;
using SOHA.Library.Imaging;
using System.Windows.Forms;
using System.IO;

namespace SOHA.Library
{
    public class Frame : ICloneable
    {
        protected int m_frame_number;
        protected double[,] m_pixel_values;
        protected double m_time_from_start;
        protected Histogram m_histogram = null;

        [XmlIgnore]
        public double[,] PixelValues
        {
            get
            {
                return m_pixel_values;
            }
            set
            {
                m_pixel_values = value;
            }
        }
        [XmlIgnore]
        public double this[int x, int y]
        {
            get
            {
                return m_pixel_values[x, y];
            }
            set
            {
                m_pixel_values[x,y] = value;
            }
        }
        [XmlIgnore]
        public int FrameNumber
        {
            get
            {
                return m_frame_number;
            }
            set
            {
                m_frame_number = value;
            }
        }
        [XmlIgnore]
        public double TimeFromStart
        {
            get
            {
                return m_time_from_start;
            }
            set
            {
                m_time_from_start = 0;
            }
        }
        [XmlIgnore]
        public Size Size
        {
            get
            {
                return new Size(Width, Height);
            }
        }
        [XmlIgnore]
        public Histogram HistogramBin
        {
            get
            {
                if(m_histogram == null)
                    m_histogram = new Histogram(this);

                return m_histogram;
            }
            protected set
            {
                m_histogram = value;
            }
        }
        
        public int Width
        {
            get
            {
                return m_pixel_values.GetLength(0);
            }
        }
        public int Height
        {
            get
            {
                return m_pixel_values.GetLength(1);
            }
        }
        public int Top { get { return Height; } }

        private double Maximum = double.MinValue;
        private double Minimum = double.MaxValue;

        private int UID;

        public Frame()
        {
            m_frame_number = 0;
            m_time_from_start = 0;
            m_pixel_values = new double[1, 1];
            m_pixel_values[0, 0] = 0;

            Random rnd = new Random(DateTime.Now.Millisecond);
            UID = rnd.Next(1000000);
        }

        /// <summary>
        /// Instantiates a <code>Frame</code> object from a SS Istream
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="time"></param>
        /// <param name="inStream"></param>
        /// <param name="frameNumber"></param>
        public Frame(int width, int height, double time, ref IStream inStream, int frameNumber)
        {
            #region Initialize Values

            m_pixel_values = new double[width, height];
            m_frame_number = frameNumber;
            m_time_from_start = time;

            #endregion

            byte[] px = new byte[width * height * 2];
            inStream.Read(px, px.Length, IntPtr.Zero);
            int ct = 0;

            #region Assign Pixel Values

            ushort H = ushort.MinValue;
            ushort L = ushort.MaxValue;
            for (int i = 0; i < m_pixel_values.GetLength(1); i++)
            {
                for (int j = 0; j < m_pixel_values.GetLength(0); j++)
                {
                    byte[] pix = new byte[2];
                    pix[0] = px[ct++];
                    pix[1] = px[ct++];

                    ushort testpix = BitConverter.ToUInt16(pix, 0);

                    if (testpix > H)
                        H = testpix;

                    if (testpix < L)
                        L = testpix;

                    m_pixel_values[j, i] = (int)testpix;// / scaling * 255.0;
                }
            }

            #endregion

            Random rnd = new Random(DateTime.Now.Millisecond);
            UID = rnd.Next(1000000);
        }

        /// <summary>
        /// Converts Byte Array to Frame. 16bpp
        /// </summary>
        /// <param name="width">Frame Width</param>
        /// <param name="height">Frame Height</param>
        /// <param name="time">Frame Time</param>
        /// <param name="inStream">Byte Array of Pixel Values</param>
        /// <param name="frameNumber">Frame Number</param>
        public Frame(int width, int height, double time, byte[] inStream, int frameNumber)
        {
            #region Initialize Values

            m_pixel_values = new double[width, height];
            m_frame_number = frameNumber;
            m_time_from_start = time;

            #endregion

            byte[] px = inStream;

            //byte[] px = new byte[width * height * 2];
            //inStream.Read(px, px.Length, IntPtr.Zero);
            int ct = 0;

            #region Assign Pixel Values

            ushort H = ushort.MinValue;
            ushort L = ushort.MaxValue;
            for(int i = 0; i < m_pixel_values.GetLength(1); i++)
            {
                for(int j = 0; j < m_pixel_values.GetLength(0); j++)
                {
                    byte[] pix = new byte[2];
                    pix[0] = px[ct++];
                    pix[1] = px[ct++];

                    ushort testpix = BitConverter.ToUInt16(pix, 0);

                    if(testpix > H)
                        H = testpix;

                    if(testpix < L)
                        L = testpix;

                    m_pixel_values[j, i] = (int)testpix;// / scaling * 255.0;
                }
            }

            #endregion

            Random rnd = new Random(DateTime.Now.Millisecond);
            UID = rnd.Next(1000000);
        }

        /// <summary>
        /// Converts <code>int[]</code> to <code>Frame</code>. Used in OME.TIF format. BitDepth of 12 is used [0 - 4095].
        /// 
        /// **Deprecated - Not Used by OMETIF Files, reading changed to Scanline
        /// </summary>
        /// <param name="width">Frame Width</param>
        /// <param name="height">Frame Height</param>
        /// <param name="time">Frame Time</param>
        /// <param name="bitdepth">Bit Depth: must be less than or equal to 16</param>
        /// <param name="pixels"><code>int[]</code> array of pixel values</param>
        /// <param name="frameNumber">Frame Number</param>
        public Frame(int width, int height, double time, int[] pixels, int bitdepth, int frameNumber)
        {
            if(bitdepth > 16)
                throw new ArgumentOutOfRangeException("Bit-depth must be less than or equal to 16.");

            ushort bit_depth_mask = 4095;
            ushort bit_depth_mask2 = (ushort)((1 << bitdepth) - 1);

            m_pixel_values = new double[width, height];
            m_frame_number = frameNumber;
            m_time_from_start = time;

            for(int i = 0; i < pixels.Length; i++)
            {
                ushort testpix = (ushort)pixels[i];
                testpix = (ushort)(testpix & bit_depth_mask2);

                int row = i / width;
                int col = i % width;

                m_pixel_values[col, height - row - 1] = testpix;
            }
        }

        /// <summary>
        /// Creates a Frame by lengthwise fold backwards and subtract
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="time"></param>
        /// <param name="inStream"></param>
        /// <param name="frameNumber"></param>
        /// <param name="flags"></param>
        public Frame(int width, int height, double time, ref IStream inStream, int frameNumber, bool flags)
        {
            #region Initialize Values
            m_pixel_values = new double[width / 2, height];
            m_frame_number = frameNumber;
            m_time_from_start = time;
            #endregion

            byte[] px = new byte[width * height * 2];
            inStream.Read(px, px.Length, IntPtr.Zero);
            int ct = 0;

            //StreamWriter sw = new StreamWriter(@"C:\test_pix_values.txt");

            #region Assign Pixel Values

            ushort H = ushort.MinValue;
            ushort L = ushort.MaxValue;

            int w = m_pixel_values.GetLength(0);
            int h = m_pixel_values.GetLength(1);

            for(int i = 0; i < h; i++)
            {
                for(int j = 0; j < w; j++)
                {
                    byte[] pix = new byte[2];
                    pix[0] = px[ct++];
                    pix[1] = px[ct++];

                    ushort testpix = BitConverter.ToUInt16(pix, 0);

                    byte[] pix2 = new byte[2];
                    pix2[0] = px[ct + (2 * w) - 2];
                    pix2[1] = px[ct + (2 * w) - 1];

                    ushort testpix2 = BitConverter.ToUInt16(pix2, 0);

                    if(testpix > H)
                        H = testpix;

                    if(testpix < L)
                        L = testpix;

                    m_pixel_values[j, i] = (int)(testpix - testpix2);// / scaling * 255.0;
                }

                ct += w * 2;
            }

            #endregion

            Random rnd = new Random(DateTime.Now.Millisecond);
            UID = rnd.Next(1000000);
        }

        public Frame(Bitmap bmp, int frameNumber, int fps)
        {
            m_frame_number = frameNumber;
            m_time_from_start = m_frame_number / (double)fps;
            m_pixel_values = new double[bmp.Width, bmp.Height];

            BitmapData bmpdata = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);

            int PixelSize = 0;
            switch (bmp.PixelFormat)
            {
                case PixelFormat.Alpha:
                    break;
                case PixelFormat.Canonical:
                    break;
                case PixelFormat.DontCare:
                    break;
                case PixelFormat.Extended:
                    break;
                case PixelFormat.Format16bppArgb1555:
                    break;
                case PixelFormat.Format16bppGrayScale:
                    break;
                case PixelFormat.Format16bppRgb555:
                    break;
                case PixelFormat.Format16bppRgb565:
                    break;
                case PixelFormat.Format1bppIndexed:
                    break;
                case PixelFormat.Format24bppRgb:
                    break;
                case PixelFormat.Format32bppArgb:
                    PixelSize = 4;
                    break;
                case PixelFormat.Format32bppPArgb:
                    break;
                case PixelFormat.Format32bppRgb:
                    break;
                case PixelFormat.Format48bppRgb:
                    break;
                case PixelFormat.Format4bppIndexed:
                    break;
                case PixelFormat.Format64bppArgb:
                    break;
                case PixelFormat.Format64bppPArgb:
                    break;
                case PixelFormat.Format8bppIndexed:
                    break;
                case PixelFormat.Gdi:
                    break;
                case PixelFormat.Indexed:
                    break;
                case PixelFormat.Max:
                    break;
                case PixelFormat.PAlpha:
                    break;
                default:
                    break;
            }

            unsafe
            {
                for (int y = 0; y < bmpdata.Height; y++)
                {
                    byte* row = (byte*)bmpdata.Scan0 + (y * bmpdata.Stride);
                    for (int x = 0; x < bmpdata.Width; x++)
                    {
                        m_pixel_values[x, y] = row[x * PixelSize];
                    }
                }
            }

            Random rnd = new Random(DateTime.Now.Millisecond);
            UID = rnd.Next(1000000);
        }

        public Frame Normalize()
        {
            //Normalizes 0 - 255
            Frame new_frame = Frame.BlankFrame(this.Width, this.Height);
            double max = this.Max();
            double min = this.Min();

            for(int i = 0; i < m_pixel_values.GetLength(1); i++)
            {
                for(int j = 0; j < m_pixel_values.GetLength(0); j++)
                {
                    new_frame[j, i] = (int)((m_pixel_values[j, i] - min) / (max - min) * 255);
                }
            }

            return new_frame;
        }

        public void Normalize(double max, double min)
        {
            //Normalizes 0 - 255

            double scale = 255 / (max - min);
            for(int i = 0; i < m_pixel_values.GetLength(1); i++)
            {
                for(int j = 0; j < m_pixel_values.GetLength(0); j++)
                {
                    this[j, i] = (int)((m_pixel_values[j, i] - min) * scale);
                }
            }
        }

        public void HistogramEqualize()
        {
            Histogram hist = new Histogram(this);

            //Equalize
            IEnumerable<KeyValuePair<int, int>> kvp_sort = hist.CDF.Where<KeyValuePair<int, int>>(x => x.Value > 0).OrderBy<KeyValuePair<int, int>, int>(x => x.Value);

            int cdf_min = (kvp_sort.ToArray<KeyValuePair<int, int>>())[0].Value;
            double den = (double)(this.Max() - 1) / (double)(Width * Height - cdf_min);

            for(int i = 0; i < Width; i++)
            {
                for(int j = 0; j < Height; j++)
                {
                    int t = (int)this[i, j];

                    this[i, j] = (hist.CDF[t] - cdf_min) * den;
                }
            }

        }

        public void Invert()
        {
            double max = this.Max();
            double min = this.Min();

            for(int i = 0; i < m_pixel_values.GetLength(1); i++)
            {
                for(int j = 0; j < m_pixel_values.GetLength(0); j++)
                {
                    this[j, i] = max - this[j, i];
                }
            }
        }

        public static Frame BlankFrame(int width, int height)
        {
            Frame f = new Frame();
            f.m_pixel_values = new double[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    f.m_pixel_values[i, j] = 0;
                }
            }

            return f;
        }

        public static Frame BlankFrame(int width, int height, double val)
        {
            Frame f = new Frame();
            f.m_pixel_values = new double[width, height];

            for(int i = 0; i < width; i++)
            {
                for(int j = 0; j < height; j++)
                {
                    f.m_pixel_values[i, j] = val;
                }
            }

            return f;
        }

        public static Frame Hypot(Frame A, Frame B)
        {
            Frame ret = Frame.BlankFrame(A.Width, A.Height);

            for(int x = 0; x <= A.Width; x++)
            {
                for(int y = 0; y <= A.Height; y++)
                {
                    ret[x, y] = Math.Sqrt(A[x, y] * A[x, y] + B[x, y] * B[x, y]);
                }
            }

            return ret;

            //Bitmap resp = new Bitmap(A.Width, A.Height, A.ToBitmap().PixelFormat);

            //BitmapData bmd = resp.LockBits(new Rectangle(0, 0, A.Width, A.Height),
            // System.Drawing.Imaging.ImageLockMode.ReadOnly, A.ToBitmap().PixelFormat);

            ////Write data
            //unsafe
            //{
            //    for(int y = 0; y < bmd.Height; y++)
            //    {
            //        byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

            //        for(int x = 0; x < bmd.Width; x++)
            //        {
            //            byte x1 = A.[3 * (x + A.Width * y)];
            //            byte y1 = B.Data[3 * (x + B.Width * y)];

            //            byte r = (byte)Math.Sqrt(x1 * x1 + y1 * y1);

            //            row[x * PIXELSIZE] = r;
            //            row[x * PIXELSIZE + 1] = r;
            //            row[x * PIXELSIZE + 2] = r;
            //        }
            //    }
            //}

            ////Unlock bits
            //resp.UnlockBits(bmd);

            //return new Frame(resp);
        }

        public static Frame Arctan(Frame A, Frame B)
        {
            Frame ret = Frame.BlankFrame(A.Width, A.Height);

            for(int x = 0; x <= A.Width; x++)
            {
                for(int y = 0; y <= A.Height; y++)
                {
                    double radians = Math.Atan2(B[x, y], A[x, y]);
                    if(radians < Math.PI / 4)
                    {
                        radians = 0;
                    }
                    else if(radians < Math.PI / 2)
                    {
                        radians = 85;
                    }
                    else if(radians < 3 * Math.PI / 2)
                    {
                        radians = 170;
                    }
                    else if(radians <= Math.PI)
                    {
                        radians = 255;
                    }
                    ret[x, y] = radians;
                }
            }

            return ret;

            //Bitmap resp = new Bitmap(A.Width, A.Height, A.Bmp.PixelFormat);

            //BitmapData bmd = resp.LockBits(new Rectangle(0, 0, A.Width, A.Height),
            // System.Drawing.Imaging.ImageLockMode.ReadOnly, A.Bmp.PixelFormat);

            ////Write data
            //unsafe
            //{
            //    for(int y = 0; y < bmd.Height; y++)
            //    {
            //        byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

            //        for(int x = 0; x < bmd.Width; x++)
            //        {
            //            byte x1 = A.Data[3 * (x + A.Width * y)];
            //            byte y1 = B.Data[3 * (x + B.Width * y)];

            //            double radians = Math.Atan2(y1, x1);

            //            byte r = (byte)Math.Atan2(y1, x1);

            //            if(radians < Math.PI / 4)
            //            {
            //                r = 0;
            //            }
            //            else if(radians < Math.PI / 2)
            //            {
            //                r = 85;
            //            }
            //            else if(radians < 3 * Math.PI / 2)
            //            {
            //                r = 170;
            //            }
            //            else if(radians <= Math.PI)
            //            {
            //                r = 255;
            //            }

            //            row[x * PIXELSIZE] = r;
            //            row[x * PIXELSIZE + 1] = r;
            //            row[x * PIXELSIZE + 2] = r;
            //        }
            //    }
            //}

            ////Unlock bits
            //resp.UnlockBits(bmd);

            //return new CLImage(resp);
        }

        public static CLImage NonMaxSuppess(CLImage A, CLImage B)
        {
            Bitmap resp = new Bitmap(A.Width, A.Height, A.Bmp.PixelFormat);

            BitmapData bmd = resp.LockBits(new Rectangle(0, 0, A.Width, A.Height),
             System.Drawing.Imaging.ImageLockMode.ReadOnly, A.Bmp.PixelFormat);

            //Write data
            unsafe
            {
                for(int y = 1; y < bmd.Height - 1; y++)
                {
                    byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

                    for(int x = 1; x < bmd.Width - 1; x++)
                    {
                        byte x1 = A.Data[3 * (x + A.Width * y)];
                        byte y1 = B.Data[3 * (x + B.Width * y)];

                        double radians = Math.Atan2(y1, x1);

                        byte r = (byte)0;

                        if(y1 == 0)
                        {
                            r = (byte)(x1 > A.Data[3 * ((x - 1) + A.Width * y)]
                                && x1 > A.Data[3 * ((x + 1) + A.Width * y)] ? 255 : 0);
                        }
                        else if(y1 == 85)
                        {
                            r = (byte)(x1 > A.Data[3 * ((x - 1) + A.Width * (y - 1))]
                                && x1 > A.Data[3 * ((x + 1) + A.Width * (y + 1))] ? 255 : 0);
                        }
                        else if(y1 == 170)
                        {
                            r = (byte)(x1 > A.Data[3 * (x + A.Width * (y - 1))]
                                && x1 > A.Data[3 * (x + A.Width * (y + 1))] ? 255 : 0);
                        }
                        else if(y1 == 255)
                        {
                            r = (byte)(x1 > A.Data[3 * ((x + 1) + A.Width * (y - 1))]
                                && x1 > A.Data[3 * ((x - 1) + A.Width * (y + 1))] ? 255 : 0);
                        }

                        //row[x * PIXELSIZE] = r;
                        //row[x * PIXELSIZE + 1] = r;
                        //row[x * PIXELSIZE + 2] = r;
                    }
                }
            }

            //Unlock bits
            resp.UnlockBits(bmd);

            return new CLImage(resp);
        }
        public Frame Bin(int numBins)
        {
            double HIGH = this.Max();

            if(numBins <= 1)
                throw new FrameException("In Frame.Bin(): numBins must be greater than 1.");

            List<int> BIN_UPPER_BOUNDS = new List<int>();
            int bin_span = (int)(HIGH / numBins);
            for(int i = 0; i < HIGH; i += bin_span)
            {
                BIN_UPPER_BOUNDS.Add(i);
            }

            Frame new_frame = Frame.BlankFrame(this.Width, this.Height);
            double max = this.Max();
            double min = this.Min();

            for(int i = 0; i < m_pixel_values.GetLength(1); i++)
            {
                for(int j = 0; j < m_pixel_values.GetLength(0); j++)
                {
                    for(int k = 0; k < BIN_UPPER_BOUNDS.Count; k++)
                    {
                        if(k == BIN_UPPER_BOUNDS.Count - 1)
                        {
                            new_frame[j, i] = BIN_UPPER_BOUNDS[k];
                        }
                        else if(this[j, i] > BIN_UPPER_BOUNDS[k] && this[j, i] < BIN_UPPER_BOUNDS[k + 1])
                        {
                            new_frame[j, i] = BIN_UPPER_BOUNDS[k];
                            break;
                        }

                    }
                }
            }

            return new_frame;
        }

        public static bool IsBlankFrame(Frame f)
        {
            for (int i = 0; i < f.Width; i++)
            {
                for (int j = 0; j < f.Height; j++)
                {
                    if (f.m_pixel_values[i, j] == 0)
                        return true;
                }
            }

            return false;
        }

        public static void Crop(ref Frame frame, System.Drawing.Rectangle rect)
        {
            double[,] new_values = new double[rect.Width, rect.Height];
            //Frame ret_frame = new Frame(rect.Size, m_number);

            for (int i = rect.Left; i <= rect.Left + rect.Width - 1; i++)
            {
                for (int j = rect.Top; j < rect.Top + rect.Height - 1; j++)
                {
                    new_values[i - rect.Left, j - rect.Top] = frame.m_pixel_values[i, j];
                }
            }

            frame.m_pixel_values = new_values;
        }

        public Size TrimSize()
        {
            int real_x = 0;
            int real_y = 0;

            for (int x = this.Size.Width - 1; x >= 0; x--)
            {
                bool col_blank = true;
                for (int y = this.Size.Height - 1; y >= 0; y--)
                {
                    if (this[x, y] != 0 && this[x, y] != 255)
                    {
                        col_blank = false;
                        real_y = y + 1;
                        break;
                    }

                    //if (y == 0)
                    //{
                    //    real_y = x;
                    //}
                }

                if (!col_blank)
                    break;
            }

            for (int y = this.Size.Height - 1; y >= 0; y--)
            {
                bool col_blank = true;
                for (int x = this.Size.Width - 1; x >= 0; x--)
                {
                    if (this[x, y] != 0 && this[x, y] != 255)
                    {
                        col_blank = false;
                        real_x = x + 1;
                        break;
                    }

                    //if (x == 0)
                    //{
                    //    real_x = y;
                    //}
                }

                if (!col_blank)
                    break;
            }

            //if (real_y == 0 && real_x == 0)
            //    return this;

            //if (real_x == 0)
            //    real_x = this.Size.Height;

            //if (real_y == 0)
            //    real_y = this.Size.Width;

            return new Size(real_x, real_y);
        }

        public double Max()
        {
            if(Maximum != double.MinValue)
                return Maximum;

            double mx = 0;
            for (int i = 0; i < m_pixel_values.GetLength(0); i++)
            {
                for (int j = 0; j < m_pixel_values.GetLength(1); j++)
                {
                    if (m_pixel_values[i, j] > mx)
                        mx = m_pixel_values[i, j];
                }
            }

            Maximum = mx;
            return mx;
        }

        public double Min()
        {
            if(Minimum != double.MaxValue)
                return Minimum;

            double mx = double.MaxValue;
            for(int i = 0; i < m_pixel_values.GetLength(0); i++)
            {
                for(int j = 0; j < m_pixel_values.GetLength(1); j++)
                {
                    if(m_pixel_values[i, j] < mx)
                        mx = m_pixel_values[i, j];
                }
            }

            Minimum = mx;
            return mx;
        }

        public unsafe void Filter(ImageFilter filter)
        {
            double max = this.Max();
            Bitmap bmp = this.ToBitmap();
            CLImage img = new CLImage(bmp);

            GPUInterop.ApplyFilters(img, (CLFilter)filter);
            bmp = img.GetStoredBitmap(bmp);

            BitmapData bmpdata = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);
            
            for(int y = 0; y < bmpdata.Height; y++)
            {
                byte* row = (byte*)bmpdata.Scan0 + (y * bmpdata.Stride);
                for(int x = 0; x < bmpdata.Width; x++)
                {
                    m_pixel_values[x, y] = row[x * 3];// / 255.0 * max;
                }
            }

            max = this.Max();
        }

        public unsafe void Filter(List<ImageFilter> filters)
        {
            double max = this.Max();
            Bitmap bmp = this.ToBitmap();
            CLImage img = new CLImage(bmp);

            List<CLFilter> clfilters = new List<CLFilter>();
            foreach(ImageFilter imgFilter in filters)
                clfilters.Add((CLFilter)imgFilter);

            GPUInterop.ApplyFilters(img, clfilters);
            bmp = img.GetStoredBitmap(bmp);
            
            BitmapData bmpdata = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);
            unsafe
            {
                for(int y = 0; y < bmpdata.Height; y++)
                {
                    byte* row = (byte*)bmpdata.Scan0 + (y * bmpdata.Stride);
                    for(int x = 0; x < bmpdata.Width; x++)
                    {
                        m_pixel_values[x, y] = row[x * 4];// / 255.0 * max;
                    }
                }
            }

            max = this.Max();
        }

        /// <summary>
        /// Default ToBitmap() assumes bit depth of 12, all values normalize to 4096 then standardized to 0-255
        /// </summary>
        /// <returns></returns>
        public unsafe Bitmap ToBitmap()
        {
            int stride = 3 * this.Width;
            while (stride % 4 != 0)
                stride++;

            //Bit Depth of 12
            double max = 4096;//this.Max();

            IntPtr scan = new IntPtr();
            Bitmap bmp = new Bitmap(this.Width, this.Height, stride, PixelFormat.Format24bppRgb, scan);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            unsafe
            {
                byte* imgPtr = (byte*)(bmpdata.Scan0.ToPointer());
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = 0; j < bmpdata.Width; j++)
                    {
                        int p = (int)(this.m_pixel_values[j, i] / max * 255);
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                    }

                    imgPtr += stride - 3 * bmpdata.Width;
                }
            }

            bmp.UnlockBits(bmpdata);
            return bmp;
        }

        public unsafe Bitmap ToBitmap(int x, int y, int w, int h)
        {
            //if (x + w > m_pixel_values.GetLength(0))
            //    throw new Exception("Frame.ToBitmap(): Invalid width.");

            //if (y + h > m_pixel_values.GetLength(1))
            //    throw new Exception("Frame.ToBitmap(): Invalid height.");

            int stride = 3 * w;
            while (stride % 4 != 0)
                stride++;

            double max = this.Max();
            double min = this.Min();

            IntPtr scan = new IntPtr();
            Bitmap bmp = new Bitmap(w, h, stride, PixelFormat.Format24bppRgb, scan);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            unsafe
            {
                byte* imgPtr = (byte*)(bmpdata.Scan0.ToPointer());
                for (int i = 0; i < bmpdata.Height; i++)
                {
                    for (int j = 0; j < bmpdata.Width; j++)
                    {
                        int p;

                        if (j + x >= m_pixel_values.GetLength(0))
                        {
                            p = 0;
                        }
                        else
                        {
                            p = (int)((this.m_pixel_values[j + x, i] - min) / (max - min) * 255);
                        }

                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                        *imgPtr = (byte)(p);
                        imgPtr++;
                    }

                    imgPtr += stride - 3 * bmpdata.Width;
                }
            }

            bmp.UnlockBits(bmpdata);
            return bmp;
        }

        public static unsafe Bitmap ToRedDotMovie(Frame f1, Frame f2, double threshold)
        {
            Bitmap bmp = new Bitmap(f1.m_pixel_values.GetLength(0), f1.m_pixel_values.GetLength(1), PixelFormat.Format24bppRgb);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            double m = f1.Max();

            for (int y = 0; y < bmpdata.Height; y++)
            {
                byte* row = (byte*)bmpdata.Scan0 + (y * bmpdata.Stride);
                //byte[] row = new byte[bmpdata.Stride];
                for (int x = 0; x < bmpdata.Width; x++)
                {
                    double dI = Math.Abs(f2[x, y] - f1[x, y]) / f1[x, y];

                    if (dI > threshold)
                    {
                        /* Make Red Dot */
                        row[x * 3] = 0;
                        row[x * 3 + 1] = 0;
                        row[x * 3 + 2] = (byte)255;
                    }
                    else
                    {
                        byte b = (byte)(int)(f1[x, y] / m * 255);
                        row[x * 3] = b;
                        row[x * 3 + 1] = b;
                        row[x * 3 + 2] = b;
                    }
                }
            }

            bmp.UnlockBits(bmpdata);
            return bmp;
        }

        public override int GetHashCode()
        {
            return UID;
        }

        public override bool Equals(object obj)
        {
            if((Frame)obj == null)
                return false;

            return this.UID == (obj as Frame).UID;
        }

        #region ICloneable Members

        public object Clone()
        {
            Frame ret = Frame.BlankFrame(this.Width, this.Height);

            for(int i = 0; i < m_pixel_values.GetLength(0); i++)
            {
                for(int j = 0; j < m_pixel_values.GetLength(1); j++)
                {
                    ret[i, j] = this[i, j];
                }
            }

            return ret;
        }

        #endregion

        public virtual void Save(string filepath)
        {
            try
            {
                Bitmap b = this.ToBitmap();
                b.Save(filepath);
            }
            catch(Exception exc)
            {
                MessageBox.Show("Error Saving Mmode");
            }
        }
    }

    [Serializable]
    public class FrameException : Exception
    {
        public FrameException() { }
        public FrameException(string message) : base(message) { }
        public FrameException(string message, Exception inner) : base(message, inner) { }
        protected FrameException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
