﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library
{
    public interface IFileable
    {
        void ReadFile();
        void WriteFile();
    }
}
