﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using SOHA.Library.Xml;

namespace SOHA.Library
{

    [Flags]
    public enum IntervalType
    {
        Systole = 0,
        Diastole,
        Change,
        Period,
        MaxVelocity
    }

    public class Interval : SohaObject
    {
        protected int m_frame;
        protected IntervalType m_intervaltype;
        protected bool m_highlight = false;
        public double m_time;

        [Browsable(false)]
        [ReadOnly(true)]
        public int Frame
        {
            get
            {
                return m_frame;
            }
            set
            {
                m_frame = value;
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public IntervalType IntervalType
        {
            get
            {
                return m_intervaltype;
            }
            set
            {
                m_intervaltype = value;
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public bool Highlight
        {
            get
            {
                return m_highlight;
            }
            set
            {
                m_highlight = value;
            }
        }

        public Interval()
        {

        }

        public Interval(int frame, IntervalType interval)
        {
            m_intervaltype = interval;
            m_frame = frame;
        }

        public void ToggleHighlight()
        {
            m_highlight = !m_highlight;
        }

        public Interval Clone()
        {
            Interval new_int = new Interval(m_frame, m_intervaltype);
            new_int.Highlight = this.Highlight;

            return new_int;
        }

        #region Operator Overloads
        public static bool operator ==(Interval I1, Interval I2)
        {
            return (I1.IntervalType == I2.IntervalType);
        }

        public static bool operator !=(Interval I1, Interval I2)
        {
            return (I1.IntervalType != I2.IntervalType);
        }

        public static bool operator <(Interval I1, Interval I2)
        {
            return (I1.Frame < I2.Frame);
        }

        public static bool operator >(Interval I1, Interval I2)
        {
            return (I1.Frame > I2.Frame);
        }

        public static bool operator >=(Interval I1, Interval I2)
        {
            return (I1.Frame >= I2.Frame);
        }

        public static bool operator <=(Interval I1, Interval I2)
        {
            return (I1.Frame <= I2.Frame);
        }
        #endregion

        #region IComparable<Interval> Members

        public int CompareTo(Interval other)
        {
            return this.Frame.CompareTo(other.Frame);
        }

        #endregion

        public override string ToString()
        {
            return m_intervaltype.ToString() + "; " + m_frame;
        }

        public override string DisplayName
        {
            get { return "Interval"; }
        }

        public override string Description
        {
            get { return m_intervaltype.ToString(); }
        }
    }
}
