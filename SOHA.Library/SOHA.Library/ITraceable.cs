﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library
{

    public delegate void TraceEventHandler(object sender, TraceEventArgs e);

    public interface ITraceable
    {
        event TraceEventHandler Trace;

        void OnTrace(TraceEventArgs e);
    }

    public class TraceEventArgs : EventArgs
    {

        public string Message
        {
            get;
            protected set;
        }

        public TraceEventArgs(string msg)
            : base()
        {
            Message = msg;
        }
    }
}
