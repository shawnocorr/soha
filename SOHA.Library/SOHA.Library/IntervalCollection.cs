﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Xml;
using SOHA.Library.Mathematics;
using System.ComponentModel;

namespace SOHA.Library
{
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class IntervalCollection : SohaCollection<Interval>
    {
        public Stats<double> SystoleStats;
        public Stats<double> Diastoles;
        public Stats<double> HeartPeriods;
        public Stats<double> HeartRate;

        public double[] FrameTimes
        {
            get;
            set;
        }

        public bool IsAlternating
        {
            get
            {
                if(this.Count < 1)
                    return false;

                if(this.Count == 1)
                    return true;

                IntervalType last_type = this[0].IntervalType;

                for(int i = 1; i < this.Count - 1; i++)
                {
                    if(this[i].IntervalType == last_type)
                    {
                        //Not alternating
                        return false;
                    }
                    last_type = this[i].IntervalType;
                }

                return true;
            }
        }

        public IntervalCollection()
            : base()
        {

        }

        public IntervalCollection(IList<Interval> items)
            : base(items)
        {
            
        }

        public static IntervalCollection XExtrapolate(IntervalCollection list, int n)
        {
            IntervalCollection new_list = new IntervalCollection();

            for(int i = 0; i < list.Count; i++)
            {
                Interval interval = list[i].Clone();
                interval.Frame *= n;
                new_list.Add(interval);
            }

            return new_list;
        }

        public static IntervalCollection XExtrapolateInverse(IntervalCollection list, int n)
        {
            IntervalCollection new_list = new IntervalCollection();

            for(int i = 0; i < list.Count; i++)
            {
                Interval interval = list[i].Clone();
                interval.Frame /= n;
                new_list.Add(interval);
            }

            return new_list;
        }

        public static IntervalCollection Take(IntervalCollection init_list, int first_n)
        {
            IntervalCollection ret_list = new IntervalCollection();

            IEnumerable<int> SystolicIntervals =
                                (from Interval interval in init_list
                                 where (interval.IntervalType == IntervalType.Systole)
                                 select interval.Frame).Take(first_n);

            if(SystolicIntervals.Count() >= first_n)
            {

                int a = SystolicIntervals.ElementAt(first_n - 1);

                int b = (from Interval interval in init_list
                         where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                         select interval.Frame).First();

                return new IntervalCollection(init_list.TakeWhile(x => x.Frame <= b).ToList());
            }
            else
            {
                return init_list;
            }
        }

        public int IndexOfClosest(int frameNumber, PhaseFormType mmodeType)
        {
            int range = 5; //+ or -

            for(int i = 0; i < Count; i++)
            {

                if(mmodeType == PhaseFormType.Interval)
                {
                    if((this[i] as Interval).IntervalType != IntervalType.Systole
                        && (this[i] as Interval).IntervalType != IntervalType.Diastole)
                        continue;
                }

                if(mmodeType == PhaseFormType.Phase)
                {
                    if((this[i] as Interval).IntervalType != IntervalType.MaxVelocity
                        && (this[i] as Interval).IntervalType != IntervalType.Change)
                        continue;
                }

                if(frameNumber >= (this[i] as Interval).Frame - range &&
                    frameNumber <= (this[i] as Interval).Frame + range)
                {
                    return i;
                }
            }

            return -1;
        }

        public void Sort()
        {

            List<Interval> sortedIntervals = 
            (from interval in this
            orderby interval.Frame ascending
            select interval).ToList<Interval>();

            this.Clear();

            foreach(Interval interval in sortedIntervals)
                this.Add(interval);

        }

        /// <summary>
        /// Determines if the <code>IntervalCollection</code> is valid, i.e, alternates Systole/Diastole and Change/MaxVelocity only within heartbeats
        /// </summary>
        /// <returns>True if valid; false otherwise</returns>
        public bool IsValid()
        {
            //Sort();

            //IntervalType last_type = this[0].IntervalType;

            //for(int i = 0; i < this.Count - 1; i++)
            //{

            //}

            return true;
        }

        /// <summary>
        /// Removes interval types from collection which should not be present. Resulted from an error in v3.2.0.0 where change intervals were saved within the Interval phase collection
        /// </summary>
        /// <param name="phase">PhaseType</param>
        public void Clean(PhaseName phase)
        {

            if(phase != PhaseName.Interval && phase != PhaseName.Phase)
                return;

            if(phase == PhaseName.Interval)
            {
                //Remove non-systole and non-diastole intervals
                for(int i = 0; i < this.Count; i++)
                {
                    if(this[i].IntervalType != IntervalType.Systole && this[i].IntervalType != IntervalType.Diastole)
                    {
                        this.RemoveAt(i);
                        i--;
                    }
                }
            }
            //else if(phase == PhaseName.Phase)
            //{
            //    //Remove non-systole and non-diastole intervals
            //    foreach(Interval interval in this)
            //    {
            //        if(interval.IntervalType != IntervalType.Change && interval.IntervalType != IntervalType.MaxVelocity)
            //        {
            //            this.Remove(interval);
            //        }
            //    }
            //}

        }

        public IntervalCollection Clone()
        {
            IntervalCollection ret = new IntervalCollection();

            for(int i = 0; i < this.Count; i++)
            {
                Interval interval = new Interval();
                interval.Frame = this[i].Frame;
                interval.IntervalType = this[i].IntervalType;
                ret.Add(interval);
            }

            return ret;
        }
        
        public bool IsSorted()
        {
            for(int i = 1; i < this.Count; i++)
            {
                if(this[i].Frame < this[i - 1].Frame)
                    return false;
            }

            return true;
        }

        public void Shift(int shift)
        {
            foreach(Interval i in this)
            {
                i.Frame += shift;
            }
        }

        #region Statistic Functions

        public bool StatsCalculated
        {
            get;
            private set;
        }

        public void Run(double[] frame_times)
        {
            if(IsSorted())
                this.Sort();

            List<double> systolic_intervals = new List<double>();
            List<double> heart_periods = new List<double>();
            List<double> heart_rate = new List<double>();

            IEnumerable<int> SystolicIntervals =
                                from Interval interval in this
                                where (interval.IntervalType == IntervalType.Systole)
                                select interval.Frame;

            foreach(int a in SystolicIntervals)
            {
                //Get Next Diastole
                int b = 0;
                try
                {
                    b =
                        (from Interval interval in this
                         where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                         select interval.Frame).First();

                    systolic_intervals.Add(frame_times[b] - frame_times[a]);
                }
                catch(Exception exc)
                {

                }


                //Get Next Systole
                try
                {
                    int a1 =
                        (from Interval interval in this
                         where (interval.IntervalType == IntervalType.Systole && interval.Frame > a)
                         select interval.Frame).First();

                    //diastolic_intervals.Add(frame_times[a1] - frame_times[b]);
                    heart_periods.Add(frame_times[a1] - frame_times[a]);
                    heart_rate.Add(1 / (frame_times[a1] - frame_times[a]));
                }
                catch(Exception exc)
                {

                }
            }

            SystoleStats = new Stats<double>(systolic_intervals.ToArray());
            //Diastoles = new Stats<double>();
            HeartPeriods = new Stats<double>(heart_periods.ToArray());
            HeartRate = new Stats<double>(heart_rate.ToArray());


            StatsCalculated = true;
        }

        #endregion
    }
}
