﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using SOHA.Library.Xml;

namespace SOHA.Library
{
    [TypeConverter(typeof(SohaExpandableObjectConverter))]
    public class RegionCollection : SohaCollection<Region>
    {
        private List<Region> m_regions = new List<Region>();

        public Region this[int i]
        {
            get
            {
                return m_regions[i];
            }
        }

        public RegionCollection(IntervalCollection intervals)
        {
            this.Clear();
            intervals.Sort();

            IEnumerable<int> SystolicIntervals =
                                from Interval interval in intervals
                                where (interval.IntervalType == IntervalType.Systole)
                                select interval.Frame;

            foreach(int a in SystolicIntervals)
            {
                //Get Next Diastole
                int b = 0;
                try
                {
                    b =
                        (from Interval interval in intervals
                         where (interval.IntervalType == IntervalType.Diastole && interval.Frame > a)
                         select interval.Frame).First();

                    this.Add(new Region(a, b));
                    //systolic_intervals.Add(frame_times[b - 1] - frame_times[a - 1]);
                }
                catch(Exception exc)
                {

                }
            }
        }
    }
}
