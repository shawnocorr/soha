﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;

namespace SOHA.Library
{
    [XmlRoot("Camera")]
    public class Camera : SohaObject
    {
        private bool m_default = false;
        private string m_name = "";
        private double m_scalefactor = 0;
        
        [Browsable(false)]
        public override string DisplayName
        {
            get { return "Camera"; }
        }

        [Browsable(false)]
        public override string Description
        {
            get { return "A Camera and Scaling Factor."; }
        }

        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }
        public double ScaleFactor
        {
            get
            {
                return m_scalefactor;
            }
            set
            {
                m_scalefactor = value;
            }
        }
        public bool Default
        {
            get
            {
                return m_default;
            }
            set
            {
                m_default = value;
            }
        }
        public bool IsEmpty
        {
            get
            {
                return (this == null || m_name == "" || m_scalefactor <= 0.0);
            }
        }

        public Camera() { }

        public Camera(string name, double scalefactor)
        {
            m_name = name;
            m_scalefactor = scalefactor;
        }

        public Camera(bool def, string name, double factor)
        {
            m_default = def;
            m_name = name;
            m_scalefactor = factor;
        }

        public override string ToString()
        {
            return m_name + ", " + m_scalefactor.ToString();
        }

        public string ToFileString()
        {
            return m_default + ", " + m_name + ", " + m_scalefactor.ToString();
        }

        public override bool Equals(object cam)
        {
            Camera equate_cam = cam as Camera;

            if (equate_cam == null)
                return false;

            return (this.Name.Equals((cam as Camera).Name) && this.ScaleFactor == (cam as Camera).ScaleFactor);
        }
    }
}
