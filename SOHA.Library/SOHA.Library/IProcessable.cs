﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;

namespace SOHA.Library
{
    public delegate void ProcessProgressUpdate(object sender, int percent);
    public delegate void ProcessComplete(object sender, ProgressEventArgs e);

    public delegate void  AsyncEventHandler(object sender, AsyncCompletedEventArgs e);

    public interface IProcessable
    {
        [Browsable(false)]
        CancellationTokenSource CancelToken { get; }

        event EventHandler BeginProcess;
        event AsyncEventHandler UpdateProcess;
        event AsyncEventHandler EndProcess;

        void Run(ProcessEventArgs args, CancellationTokenSource cancel_token);
        void TestRun(ProcessEventArgs args, CancellationTokenSource cancel_token);

        void OnBegin(EventArgs e);
        void OnUpdate(AsyncCompletedEventArgs e);
        void OnEnd(AsyncCompletedEventArgs e);
    }

    public class ProgressEventArgs : EventArgs
    {

        public bool Cancelled
        {
            get;
            private set;
        }
        public object Return
        {
            get;
            private set;
        }
        public bool Error
        {
            get;
            private set;
        }
        public Exception Exception
        {
            get;
            private set;
        }
        
        public ProgressEventArgs()
            : base()
        {

        }

        public ProgressEventArgs(object ret, bool WasCancelled, bool error, Exception exc)
            : base()
        {
            Return = ret;
            Cancelled = WasCancelled;
            Error = error;
            Exception = exc;
        }
    }
}
