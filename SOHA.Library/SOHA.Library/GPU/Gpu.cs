﻿/*TH];
            //buffer.Write(buffer_bytes, 0, buffer_bytes.Length);

            varBuffer.WriteToDevice(buffer);
            varWidth.WriteToDevice(new long[] { buffer.Length });
            varBlock.WriteToDevice(new long[]{ 512 });
            long[] result = new long[1];

            CLCalc.Program.Variable[] args = new CLCalc.Program.Variable[] { varBuffer, varBlock, varWidth, varResult };
            kernelBrightness2.Execute(args, new int[1] { 4 });

            varResult.ReadFromDeviceTo(result);

            sum = result[0];
        }


        private static string src_new = @"

__kernel void VectorAdd(__global int   * buffer,
                        __global long   * width,
                        __global long   * result)
{
    int id = get_global_id(0);
    int w = width[0];

    result[0] = 0;

    for(int i = 0; i < w; i++)
    {
        result[0] += buffer[i];
    }
    
}



__kernel void VectorAdd2(__global int * buffer,
                __global int * block,
                __global long * length,
                __global long * result) 
{
    int global_index = get_global_id(0) * block;
    long accumulator = 0;
    int upper_bound = (global_index + 1) * block;
    if (upper_bound > length) upper_bound = length;
    while (global_index < upper_bound) 
    {
        long element = buffer[global_index];
        accumulator += element;
        global_index++;
    }
    result[get_group_id(0)] = accumulator;
}
";

        private static string src = @"

__kernel void vectorDifference(__global byte* a,
                        __global int* b,
                        __global int* c,
                        const unsigned int n)
{
    int id = get_global_id(0);

    if(id < n)
        c[id] = abs_diff(b[id], a[id]);
}

__kernel void VectorAdd(__global byte   * buffer,
                        __global long   * width,
                        __global long   * result)
{
    int id = get__global_id(0);
    int w = width[0];

    result[0] = 0;

    for(int i = 0; i < w; i++)
    {
        result[0] += buffer[i];
    }
    
}

__kernel void vectorSum(__global int* buffer,
            __const int block,
            __const int length,
            __global int* result) {

  int global_index = get_global_id(0) * block;
  int accumulator = 0;
  int upper_bound = (get_global_id(0) + 1) * block;
  if (upper_bound > length) upper_bound = length;
  while (global_index < upper_bound) {
    int element = buffer[global_index];
    accumulator += element;
    global_index++;
  }
  result[get_group_id(0)] = accumulator;
}
";
    }
}
*/