﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenCLTemplate;
using System.Diagnostics;

namespace SOHA.Library.GPU
{
    public static class GpuFilter
    {
        /// <summary>Static class already initialized?</summary>
        public static bool Initialized = false;

        /// <summary>Apply filter kernel</summary>
        private static CLCalc.Program.Kernel kernelApplyFilter;
        /// <summary>Apply filter kernel</summary>
        private static CLCalc.Program.Kernel kernelApplyFilterWorkDim2;
        private static CLCalc.Program.Kernel kernelBrightness;
        private static CLCalc.Program.Kernel kernelVectorDifference;

        /// <summary>Filter values</summary>
        private static CLCalc.Program.Variable varFilter;

        public static void Init(int FilterSize)
        {
            if (CLCalc.CLAcceleration == CLCalc.CLAccelerationType.Unknown) CLCalc.InitCL();

            //Compiles source code
            CLCalc.Program.Compile(new_src);

            //Creates kernel
            //kernelVectorDifference = new CLCalc.Program.Kernel("vectorDifference");
            //kernelBrightness = new CLCalc.Program.Kernel("Brightness");
            kernelApplyFilter = new CLCalc.Program.Kernel("ApplyFilter1");
            kernelApplyFilterWorkDim2 = new CLCalc.Program.Kernel("ImgFilter1");

            //Creates filter
            //varFilter = new CLCalc.Program.Variable(new float[3 * FilterSize * FilterSize]);
            varFilter = new CLCalc.Program.Variable(new float[FilterSize]);
            //Width
            varWidth = new CLCalc.Program.Variable(new int[1]);

            Initialized = true;
        }

        private static float[] FilteredVals;
        private static CLCalc.Program.Variable varFiltered;
        private static CLCalc.Program.Variable varWidth;

        /// <summary>Applies given filter to the image</summary>
        /// <param name="imgDt">Image to be filtered</param>
        /// <param name="Filter">Filter. [3*size*size]</param>
        public static void ApplyFilter(GpuFrame imgDt, float[] Filter, bool useOpenCL, bool useWorkDim2)
        {
            //Stopwatch sWatch = new Stopwatch();
            //sWatch.Start();

            //int FilterSize = (int)Math.Sqrt(Filter.Length / 3);
            int FilterSize = Filter.Length;

            //if (Filter.Length != 3 * FilterSize * FilterSize)
            //    throw new Exception("Invalid filter");

            if(!Initialized && useOpenCL) Init(FilterSize);

            //Writes filter to device
            if (useOpenCL) varFilter.WriteToDevice(Filter);

            if (FilteredVals == null || FilteredVals.Length != imgDt.Height * imgDt.Width * 3)
            {
                //Filtered values
                FilteredVals = new float[imgDt.Height * imgDt.Width];
                varFiltered = new CLCalc.Program.Variable(FilteredVals);
            }

            //Width
            if (useOpenCL) varWidth.WriteToDevice(new int[] { imgDt.Width });


            //Executes filtering
            int mean = (FilterSize - 1) / 2;
            if (useOpenCL)
            {
                CLCalc.Program.Variable[] args = new CLCalc.Program.Variable[] { imgDt.varData, varFilter, varFiltered, varWidth };
                if (useWorkDim2)
                {
                    kernelApplyFilterWorkDim2.Execute(args, new int[] { imgDt.Width - FilterSize, imgDt.Height - FilterSize });
                }
                else
                {
                    kernelApplyFilter.Execute(args, new int[] { imgDt.Height - FilterSize });
                }
                //Reads data back
                varFiltered.ReadFromDeviceTo(FilteredVals);

            }
            else
            {
                //ApplyFilter(imgDt.Data, Filter, FilteredVals, new int[] { imgDt.Width }, imgDt.Height - FilterSize);
            }

            //Writes to image data
            for (int y = mean; y < imgDt.Height - mean - 1; y++)
            {
                int wy = imgDt.Width * y;
                for (int x = mean; x < imgDt.Width - mean - 1; x++)
                {
                    //int ind = 3 * (x + wy);
                    int ind = (x + wy);
                    imgDt.RGBAData[ind] = (byte)FilteredVals[ind];
                    //imgDt.Data[ind + 1] = (byte)FilteredVals[ind + 1];
                    //imgDt.Data[ind + 2] = (byte)FilteredVals[ind + 2];
                }
            }

            //Writes filtered values
            //In the future this rewriting can be avoided
            //because byte_addressable will be widely available
            if (useOpenCL) imgDt.varData.WriteToDevice(imgDt.RGBAData);

            //sWatch.Stop();
            //Soha.Lib.Trace.TraceLog("CLFilterApplied: " + sWatch.Elapsed.ToString());
        }

        private static CLCalc.Program.Variable varBrightness;
        public static double Brightness(GpuFrame imgDt)
        {
            if(!Initialized) Init(7);

            CLCalc.Program.Variable varSum = new CLCalc.Program.Variable(new int[imgDt.Data.Length]);
            CLCalc.Program.Variable varBlock = new CLCalc.Program.Variable(new int[1]);
            CLCalc.Program.Variable varReturn = new CLCalc.Program.Variable(new int[imgDt.Data.Length]);

            float brightness = 0;
            int[] sum = new int[imgDt.Data.Length];
            int[] vecReturn = new int[imgDt.Data.Length];

            varWidth.WriteToDevice(new int[] { imgDt.Data.Length });
            varBlock.WriteToDevice(new int[]{512});
            varSum.WriteToDevice(sum);

            //kernelBrightness = new CLCalc.Program.Kernel("Brightness");
            //CLCalc.Program.Variable[] args = new CLCalc.Program.Variable[] { imgDt.varData, varWidth, varBrightness };
            kernelBrightness = new CLCalc.Program.Kernel("vectorSum");
            CLCalc.Program.Variable[] args = new CLCalc.Program.Variable[] { imgDt.varData, varBlock ,varWidth, varSum };
            
            kernelBrightness.Execute(args, new int[] { 4 });

            varSum.ReadFromDeviceTo(vecReturn);

            return (double)sum[0];
        }

        public static int[] VectorDifference(GpuFrame A, GpuFrame B)
        {
            if(A.Data.Length != B.Data.Length)
                throw new Exception("Vectors are not the same length");

            int size = A.Data.Length;

            if(!Initialized) Init(7);

            int[] vecResult = new int[size];
            int[] vecReturn = new int[size];

            CLCalc.Program.Variable varVec1 = new CLCalc.Program.Variable(new int[size]);
            CLCalc.Program.Variable varVec2 = new CLCalc.Program.Variable(new int[size]);
            CLCalc.Program.Variable varVec3 = new CLCalc.Program.Variable(new int[size]);
            CLCalc.Program.Variable varN = new CLCalc.Program.Variable(new int[1]);

            varVec1.WriteToDevice(A.Data);
            varVec2.WriteToDevice(B.Data);
            varVec3.WriteToDevice(vecResult);
            varN.WriteToDevice(new int[] { size });

            CLCalc.Program.Variable[] args = new CLCalc.Program.Variable[] { varVec1, varVec2, varVec3, varN };
            
            kernelVectorDifference.Execute(args, new int[] { size });
            varVec3.ReadFromDeviceTo(vecReturn);


            return vecReturn;

        }

        #region Filter Strings

        private static string src_old = @"

#define FILTERSIZE 7
#define CENTER 3

__kernel void ApplyFilter4(__global uchar * ImgData,
                          __global float * Filter,
                          __global float * FilteredImage,
                          __global int   * ImgWidth)

{
   int y = get_global_id(0);
   int w = ImgWidth[0];
   
   //Image values
   float ImgValues[3][FILTERSIZE][FILTERSIZE];

   //Copies filter to local memory
   float localfilter[3][FILTERSIZE][FILTERSIZE];
   
   //Initialization of ImgValues
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ImgValues[0][i][j] = ImgData[3*((y+i)*w+j)];
           ImgValues[1][i][j] = ImgData[3*((y+i)*w+j)+1];
           ImgValues[2][i][j] = ImgData[3*((y+i)*w+j)+2];

           localfilter[0][i][j] = Filter[3*(i*FILTERSIZE + j)];
           localfilter[1][i][j] = Filter[3*(i*FILTERSIZE + j)+1];
           localfilter[2][i][j] = Filter[3*(i*FILTERSIZE + j)+2];
       }
   }
   
   
   int xMax = w-CENTER;
   int ind = 0;
   for (int x = 0; x < xMax; x++)
   {
       //Calculates filtered value
       float4 filteredValue = 0;
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
              float4 pixVals = (float4)(ImgValues[0][i][j],ImgValues[1][i][j],ImgValues[2][i][j],0);
              float4 filterVals = (float4)(localfilter[0][i][j],localfilter[1][i][j],localfilter[2][i][j],0);
              
              filteredValue = pixVals*filterVals+filteredValue;

//              filteredValue.x = mad(ImgValues[0][i][j], localfilter[0][i][j], filteredValue.x);
//              filteredValue.y = mad(ImgValues[1][i][j], localfilter[1][i][j], filteredValue.y);
//              filteredValue.z = mad(ImgValues[2][i][j], localfilter[2][i][j], filteredValue.z);
          }
       }
       ind = 3*((y+CENTER)*w+x+CENTER);       
       FilteredImage[ind] = clamp(filteredValue.x,0.0f,255.0f);
       FilteredImage[ind+1] = clamp(filteredValue.y,0.0f,255.0f);
       FilteredImage[ind+2] = clamp(filteredValue.z,0.0f,255.0f);
   
       //Gets next filter values
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
             ImgValues[0][i][j] = ImgValues[0][i][j+1];
             ImgValues[1][i][j] = ImgValues[1][i][j+1];
             ImgValues[2][i][j] = ImgValues[2][i][j+1];
          }
          ind = 3*((y+i)*w+x+FILTERSIZE-1);
          ImgValues[0][i][FILTERSIZE-1] = ImgData[ind];
          ImgValues[1][i][FILTERSIZE-1] = ImgData[ind+1];
          ImgValues[2][i][FILTERSIZE-1] = ImgData[ind+2];
       }
       
   }    
}

kernel void ImgFilter4(global uchar * image,
                      global float * Filter,
                      global float * FilteredImage,
                      global int * Width)
                      
{
   int x = get_global_id(0);
   int y = get_global_id(1);
   int w = Width[0];
   int ind = 0;   
   int ind2 = 0;   

   float4 filteredVal = (float4)(0,0,0,0);
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ind = 3*(x+j + w*(y+i));
           ind2 = 3*(i*FILTERSIZE + j);
           filteredVal.x =  mad(Filter[ind2] ,   (float)image[ind],  filteredVal.x);
           filteredVal.y =  mad(Filter[ind2+1] , (float)image[ind+1],filteredVal.y);
           filteredVal.z =  mad(Filter[ind2+2] , (float)image[ind+2],filteredVal.z);
       }
   }
   ind = 3*(x+CENTER + w*(y+CENTER));
   FilteredImage[ind] = clamp(filteredVal.x,0.0f,255.0f);
   FilteredImage[ind+1] = clamp(filteredVal.y,0.0f,255.0f);
   FilteredImage[ind+2] = clamp(filteredVal.z,0.0f,255.0f);
}
";

        private static string src2 = @"

#define FILTERSIZE 7
#define CENTER 3

__kernel void ApplyFilter(__global uchar * ImgData,
                          __global float * Filter,
                          __global float * FilteredImage,
                          __global int   * ImgWidth)

{
   int y = get_global_id(0);
   int w = ImgWidth[0];
   
   //Image values
   float ImgValues[3][FILTERSIZE][FILTERSIZE];

   //Copies filter to local memory
   float localfilter[3][FILTERSIZE][FILTERSIZE];
   
   //Initialization of ImgValues
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ImgValues[0][i][j] = ImgData[3*((y+i)*w+j)];
           ImgValues[1][i][j] = ImgData[3*((y+i)*w+j)+1];
           ImgValues[2][i][j] = ImgData[3*((y+i)*w+j)+2];

           localfilter[0][i][j] = Filter[3*(i*FILTERSIZE + j)];
           localfilter[1][i][j] = Filter[3*(i*FILTERSIZE + j)+1];
           localfilter[2][i][j] = Filter[3*(i*FILTERSIZE + j)+2];
       }
   }
   
   
   int xMax = w-CENTER;
   int ind = 0;
   for (int x = 0; x < xMax; x++)
   {
       //Calculates filtered value
       float4 filteredValue = 0;
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
              float4 pixVals = (float4)(ImgValues[0][i][j],ImgValues[1][i][j],ImgValues[2][i][j],0);
              float4 filterVals = (float4)(localfilter[0][i][j],localfilter[1][i][j],localfilter[2][i][j],0);
              
              filteredValue = pixVals*filterVals+filteredValue;

//              filteredValue.x = mad(ImgValues[0][i][j], localfilter[0][i][j], filteredValue.x);
//              filteredValue.y = mad(ImgValues[1][i][j], localfilter[1][i][j], filteredValue.y);
//              filteredValue.z = mad(ImgValues[2][i][j], localfilter[2][i][j], filteredValue.z);
          }
       }
       ind = 3*((y+CENTER)*w+x+CENTER);       
       FilteredImage[ind] = clamp(filteredValue.x,0.0f,255.0f);
       FilteredImage[ind+1] = clamp(filteredValue.y,0.0f,255.0f);
       FilteredImage[ind+2] = clamp(filteredValue.z,0.0f,255.0f);
   
       //Gets next filter values
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
             ImgValues[0][i][j] = ImgValues[0][i][j+1];
             ImgValues[1][i][j] = ImgValues[1][i][j+1];
             ImgValues[2][i][j] = ImgValues[2][i][j+1];
          }
          ind = 3*((y+i)*w+x+FILTERSIZE-1);
          ImgValues[0][i][FILTERSIZE-1] = ImgData[ind];
          ImgValues[1][i][FILTERSIZE-1] = ImgData[ind+1];
          ImgValues[2][i][FILTERSIZE-1] = ImgData[ind+2];
       }
       
   }    
}

kernel void ImgFilter(global uchar * image,
                      global float * Filter,
                      global float * FilteredImage,
                      global int * Width)
                      
{
   int x = get_global_id(0);
   int y = get_global_id(1);
   int w = Width[0];
   int ind = 0;   
   int ind2 = 0;   

   float4 filteredVal = (float4)(0,0,0,0);
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ind = 3*(x+j + w*(y+i));
           ind2 = 3*(i*FILTERSIZE + j);
           filteredVal.x =  mad(Filter[ind2] ,   (float)image[ind],  filteredVal.x);
           filteredVal.y =  mad(Filter[ind2+1] , (float)image[ind+1],filteredVal.y);
           filteredVal.z =  mad(Filter[ind2+2] , (float)image[ind+2],filteredVal.z);
       }
   }
   ind = 3*(x+CENTER + w*(y+CENTER));
   FilteredImage[ind] = clamp(filteredVal.x,0.0f,255.0f);
   FilteredImage[ind+1] = clamp(filteredVal.y,0.0f,255.0f);
   FilteredImage[ind+2] = clamp(filteredVal.z,0.0f,255.0f);
}
";

        #endregion

        #region Brightness

        private static string src = @"

__kernel void vectorDifference(__global int* a,
                        __global int* b,
                        __global int* c,
                        const unsigned int n)
{
    int id = get_global_id(0);

    if(id < n)
        c[id] = abs_diff(b[id], a[id]);
}

__kernel void vectorSum(__global int* buffer,
            __const int block,
            __const int length,
            __global int* result) {

  int global_index = get_global_id(0) * block;
  int accumulator = 0;
  int upper_bound = (get_global_id(0) + 1) * block;
  if (upper_bound > length) upper_bound = length;
  while (global_index < upper_bound) {
    int element = buffer[global_index];
    accumulator += element;
    global_index++;
  }
  result[get_group_id(0)] = accumulator;
}
";
        #endregion

        #region Greyscale Filtering

        private static string new_src = @"

#define FILTERSIZE 7
#define CENTER 3

__kernel void ApplyFilter1(__global uchar * ImgData,
                __global float * Filter,
                __global float * FilteredImage,
                __global int   * ImgWidth)

{
   int y = get_global_id(0);
   int w = ImgWidth[0];
   
   //Image values
   float ImgValues[FILTERSIZE][FILTERSIZE];

   //Copies filter to local memory
   float localfilter[FILTERSIZE][FILTERSIZE];
   
   //Initialization of ImgValues
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ImgValues[i][j] = ImgData[((y+i)*w+j)];

           localfilter[i][j] = Filter[(i*FILTERSIZE + j)];
       }
   }
   
   
   int xMax = w-CENTER;
   int ind = 0;
   for (int x = 0; x < xMax; x++)
   {
       //Calculates filtered value
       float filteredValue = 0;
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
              float pixVals = (float)(ImgValues[i][j]);
              float filterVals = (float)(localfilter[i][j]);              
              filteredValue = pixVals*filterVals+filteredValue;
          }
       }
       ind = ((y+CENTER)*w+x+CENTER);       
       FilteredImage[ind] = clamp(filteredValue,0.0f,255.0f);
   
       //Gets next filter values
       for (int i = 0; i < FILTERSIZE; i++)
       {
          for (int j = 0; j < FILTERSIZE-1; j++)
          {
             ImgValues[i][j] = ImgValues[i][j+1];
          }
          ind = ((y+i)*w+x+FILTERSIZE-1);
          ImgValues[i][FILTERSIZE-1] = ImgData[ind];
       }
       
   }    
}

__kernel void ImgFilter1(global uchar * image,
                      global float * Filter,
                      global float * FilteredImage,
                      global int * Width)
                      
{
   int x = get_global_id(0);
   int y = get_global_id(1);
   int w = Width[0];
   int ind = 0;   
   int ind2 = 0;   

   float filteredVal = (float)(0);
   for (int i = 0; i < FILTERSIZE; i++)
   {
       for (int j = 0; j < FILTERSIZE; j++)
       {
           ind = (x+j + w*(y+i));
           ind2 = (i*FILTERSIZE + j);
           filteredVal =  mad(Filter[ind2],(float)image[ind],filteredVal);
       }
   }
   ind = (x+CENTER + w*(y+CENTER));
   FilteredImage[ind] = clamp(filteredVal,0.0f,255.0f);
}
";

        #endregion
    }
}