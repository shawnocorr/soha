﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.GPU
{
    public class BFilter
    {
        protected double[] m_filter;

        public double[] Filter
        {
            get
            {
                return m_filter;
            }
            set
            {
                m_filter = value;
            }
        }

        public BFilter() { }

    }
}
