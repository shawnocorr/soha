﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Video;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using SOHA.Library.Mathematics;
using OpenCLTemplate;

namespace SOHA.Library.GPU
{
    public class GpuFrame : BFrame
    {
        private const int PIXEL_SIZE = 3;

        private readonly Bitmap m_bmp;

        public CLCalc.Program.Variable varData;
        public int[] Data;
        public byte[] RGBAData;

        public GpuFrame(NewFrame frame)
        {
            m_bmp = (Bitmap)frame;

            m_values = frame.Values;

            if (CLCalc.CLAcceleration == CLCalc.CLAccelerationType.Unknown)
                CLCalc.InitCL();

            RGBAData = new byte[3 * Width * Height];

            ReadToLocalData(m_bmp);

            varData = new CLCalc.Program.Variable(RGBAData);
        }

        public GpuFrame(Frame frame)
        {
            m_bmp = frame.ToBitmap();

            m_values = new Matrix(frame.PixelValues);

            if(CLCalc.CLAcceleration == CLCalc.CLAccelerationType.Unknown)
                CLCalc.InitCL();

            RGBAData = new byte[3 * Width * Height];

            ReadToLocalData(m_bmp);

            varData = new CLCalc.Program.Variable(RGBAData);
        }

        public GpuFrame(MemoryStream stream, int width, int height)
        {
            #region Initialize Values

            Values = new Matrix(width, height);
            Data = new int[2 * Fourier.NextPower2(Values.Rows * Values.Columns)];

            #endregion

            byte[] px = new byte[width * height * 2];
            stream.Read(px, 0,px.Length);
            int ct = 0;

            #region Assign Pixel Values

            ushort H = ushort.MinValue;
            ushort L = ushort.MaxValue;
            for(int i = 0; i < Values.Columns; i++)
            {
                for(int j = 0; j < Values.Rows; j++)
                {
                    byte[] pix = new byte[2];
                    pix[0] = px[ct++];
                    pix[1] = px[ct++];

                    ushort testpix = BitConverter.ToUInt16(pix, 0);

                    if(testpix > H)
                        H = testpix;

                    if(testpix < L)
                        L = testpix;

                    Values[j, i] = (int)testpix;    //scaling * 255.0;
                    Data[i * Values.Rows + j] = (int)testpix;

                    if(Values[j, i] > m_max)
                        m_max = Values[j, i];

                }
            }

            m_recalculate_max = false;

            #endregion

            if(CLCalc.CLAcceleration == CLCalc.CLAccelerationType.Unknown)
                CLCalc.InitCL();

            //Pad to 2^n length

            //Data = new byte[3 * Width * Height];
            //ReadToLocalData((Bitmap)(new NewFrame(Values)));
            varData = new CLCalc.Program.Variable(Data);
        }

        private void ReadToLocalData(Bitmap bmp)
        {
            //Lock bits
            BitmapData bmd = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                             System.Drawing.Imaging.ImageLockMode.ReadOnly, bmp.PixelFormat);
            //Read data
            unsafe
            {
                for (int y = 0; y < bmd.Height; y++)
                {
                    byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

                    for (int x = 0; x < bmd.Width; x++)
                    {
                        //RGBAData[3 * (x + Width * y)] = row[x * PIXEL_SIZE];
                        //RGBAData[3 * (x + Width * y) + 1] = row[x * PIXEL_SIZE + 1];
                        //RGBAData[3 * (x + Width * y) + 2] = row[x * PIXEL_SIZE + 2];

                        RGBAData[(x + Width * y)] = row[x * PIXEL_SIZE];
                    }
                }
            }

            //Unlock bits
            bmp.UnlockBits(bmd);
        }

        public Bitmap GetStoredBitmap(Bitmap bmp)
        {
            Bitmap resp = new Bitmap(Width, Height, bmp.PixelFormat);

            BitmapData bmd = resp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
             System.Drawing.Imaging.ImageLockMode.ReadOnly, bmp.PixelFormat);

            //Write data
            unsafe
            {
                for (int y = 0; y < bmd.Height; y++)
                {
                    byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

                    for (int x = 0; x < bmd.Width; x++)
                    {
                        row[x * PIXEL_SIZE] = RGBAData[3 * (x + Width * y)];
                        row[x * PIXEL_SIZE + 1] = RGBAData[3 * (x + Width * y) + 1];
                        row[x * PIXEL_SIZE + 2] = RGBAData[3 * (x + Width * y) + 2];
                    }
                }
            }

            //Unlock bits
            resp.UnlockBits(bmd);

            return resp;
        }

        public void ReadBmp(Bitmap bmp)
        {
            ReadToLocalData(bmp);
            varData.WriteToDevice(RGBAData);
        }
    }
}
