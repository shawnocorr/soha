﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library.GPU
{
    [Serializable]
    public class GpuException : Exception
    {
        public GpuException() { }
        public GpuException(string message) : base(message) { }
        public GpuException(string message, Exception inner) : base(message, inner) { }
        protected GpuException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
