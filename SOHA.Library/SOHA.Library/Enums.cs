﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library
{
    public enum PhaseFormType
    {
        New = 0,
        Mark,
        Preprocess,
        Interval,
        Phase,
        RedDot,
        MModeSelect,
        Properties,
        WorkBench,
        IntervalWorkbench,
        Reset
    }

    public class Enums
    {
        public static List<PhaseFormType> UI_FORMS = new List<PhaseFormType>() { PhaseFormType.Mark, PhaseFormType.Interval, PhaseFormType.Phase };
    }
}
