﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHA.Library
{
    [Serializable]
    public class SohaException : Exception
    {
        public SohaException() { }
        public SohaException(string message) : base(message) { }
        public SohaException(string message, Exception inner) : base(message, inner) { }
        protected SohaException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
