﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.GPU;
using SOHA.Library.Mathematics;
using SOHA.Library.Imaging;
using System.Drawing;

namespace SOHA.Library.Vision
{
    public class Canny : BFrameFunction
    {

        public float Sigma { get; set; }
        public int GaussSize { get; set; }

        public Canny(float sigma, int gauss_size)
        {
            Sigma = sigma;
            GaussSize = gauss_size;
        }
        
        //#region IFilter Members

        //public CLImage PerformFilter(CLImage image)
        //{

        //    #region Gauss Smooth

        //    ImageFilter GaussSmooth = ImageFilter.Gaussian2D(Sigma, GaussSize);

        //    GaussSmooth.PerformFilter(image);

        //    #endregion

        //    CLImage Gx = (CLImage)image.Clone();
        //    CLImage Gy = (CLImage)image.Clone();

        //    ImageFilter Sx = new ImageFilter(EdgeDetection.SOBEL_X);
        //    ImageFilter Sy = new ImageFilter(EdgeDetection.SOBEL_Y);

        //    Sx.PerformFilter(Gx);
        //    Sy.PerformFilter(Gy);

        //    CLImage G = CLImage.Hypot(Gx, Gy);
        //    CLImage A = CLImage.Arctan(Gx, Gy);


        //    #region Non-Maxima Suppressions

        //    return CLImage.NonMaxSuppess(G, A);

        //    #endregion

        //}

        //#endregion

        public override Frame Execute(Frame frm)
        {
            //CLImage image = new CLImage(frm.ToBitmap());
            //Bitmap bmp = frm.ToBitmap();

            #region Gauss Smooth

            ImageFilter GaussSmooth = ImageFilter.Gaussian2D(Sigma, GaussSize);

            frm = GaussSmooth.Execute(frm);

            #endregion

            Frame Gx = (Frame)frm.Clone();
            Frame Gy = (Frame)frm.Clone();

            ImageFilter Sx = new ImageFilter(EdgeDetection.SOBEL_X);
            ImageFilter Sy = new ImageFilter(EdgeDetection.SOBEL_Y);

            Sx.Execute(Gx);
            Sy.Execute(Gy);

            Frame G = Frame.Hypot(Gx, Gy);
            Frame A = Frame.Arctan(Gx, Gy);


            #region Non-Maxima Suppressions

            //Frame ret = Frame.NonMaxSuppess(G, A);

            #endregion
            
            //bmp = image.GetStoredBitmap(bmp);

            return G;
        }
    }
}
