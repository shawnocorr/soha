﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using SOHAControls;
using SOHA.Library;
using SOHAControls.DisplayControls;

namespace Forms
{
    public partial class MModeDisplay : PictureBox
    {
        private bool suspend_layout = false;

        protected MMode m_mmode;
        protected IntervalCollection m_list;
        protected IntervalProperties m_interval = new IntervalProperties();
        protected PhaseFormType m_phase = PhaseFormType.Interval;
        protected int m_zoom = 1;
        protected int m_phase_offset;
        protected bool m_show_change_intervals = false;
        protected VideoFrameRange m_range;

        public MMode MMode
        {
            get { return m_mmode; }
            set
            {
                m_mmode = value;
                Paint();
            }
        }
        public IntervalCollection List
        {
            get { return m_list; }
            set
            {
                m_list = value;
                Paint();
            }
        }
        public IntervalProperties Interval
        {
            get
            {
                return m_interval;
            }
            set
            {
                m_interval = value;
                m_interval.IntervalChanged += new EventHandler(m_interval_IntervalChanged);
            }
        }
        public int PhaseOffset
        {
            get
            {
                return m_phase_offset;
            }
            set
            {
                m_phase_offset = value;
                //OnToggleChangeIntervals();
            }
        }
        
        [Browsable(true)]
        public bool ShowChangeIntervals
        {
            get
            {
                return m_show_change_intervals;
            }
            set
            {
                m_show_change_intervals = value;
                OnToggleChangeIntervals();
            }
        }
        public int Zoom
        {
            get
            {
                return m_zoom;
            }
            set
            {
                m_zoom = value;
                Paint();
            }
        }
        public VideoFrameRange Range
        {
            get
            {
                return m_range;
            }
            set
            {
                if(value == null)
                    return;

                if(m_range != null)
                    m_range.RangeChanged -= new EventHandler<VideoFrameRangeEventArgs>(m_range_RangeChanged);

                m_range = value;
                m_range.RangeChanged += new EventHandler<VideoFrameRangeEventArgs>(m_range_RangeChanged);
                Paint();
            }
        }

        void m_range_RangeChanged(object sender, VideoFrameRangeEventArgs e)
        {            
            Paint(e);// (e.Range);
        }

        public bool DisplayCoordinates
        {
            get;
            set;
        }

        [Browsable(true)]
        public PhaseFormType Phase { get; set; }
        
        public event EventHandler ListChanged;

        void m_interval_IntervalChanged(object sender, EventArgs e)
        {
            Paint();
        }

        public MModeDisplay()
        {
            InitializeComponent();
        }

        public MModeDisplay(IContainer container)
        {
            container.Add(this);

            InitializeComponent();

            this.BorderStyle = BorderStyle.FixedSingle;
            //List.ListChanged += new ListChangedEventHandler(List_ListChanged);
        }

        void List_ListChanged(object sender, ListChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        protected void OnPhaseTypeChanged()
        {
            popupAddInterval.Items.Clear();
            if(Phase == PhaseFormType.Phase)
            {
                this.popupAddInterval.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                    this.addChangeToolStripMenuItem,
                    this.addVelocityToolStripMenuItem});
            }
            else if(Phase == PhaseFormType.Interval)
            {
                this.popupAddInterval.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                    this.addSystoleToolStripMenuItem,
                    this.addDiastoleToolStripMenuItem});
            }
        }

        protected void CreatePopupMenu()
        {
            popupAddInterval.Items.Clear();
            if(Phase == PhaseFormType.Phase)
            {
                this.popupAddInterval.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                    this.addChangeToolStripMenuItem,
                    this.addVelocityToolStripMenuItem});
            }
            else if(Phase == PhaseFormType.Interval)
            {
                this.popupAddInterval.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                    this.addSystoleToolStripMenuItem,
                    this.addDiastoleToolStripMenuItem});
            }
        }

        protected void OnListChanged()
        {
            if (ListChanged != null)
                ListChanged(this, new EventArgs());
        }

        protected void OnToggleChangeIntervals()
        {
            Paint();
        }

        public void SuspendPaint() { suspend_layout = true; }
        public void ResumePaint(bool resume)
        {
            suspend_layout = false;

            if(resume)
                Paint();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);
        }

        protected new void Paint()
        {
            Paint(new VideoFrameRangeEventArgs(m_range));
            return;

            if (suspend_layout)
                return;

            Bitmap b;
            MMode new_mmode = new MMode();

            if (m_mmode != null)
            {
                //Paint MMode
                if (m_interval.Interval != -1 && m_interval.IntervalSize != -1)
                {
                    int start = (m_interval.Interval - 1) * 1000;
                    new_mmode = SOHA.Library.MMode.XExtrapolate(m_mmode, m_zoom);
                    b = new_mmode.ToBitmap(start, 0, m_interval.IntervalSize, m_mmode.Height);
                    //f = m_mmode.ToFrame(m_interval.Interval, m_interval.IntervalSize);
                }
                //if(m_range != null)
                //{
                //    int start = (m_range.Range - 1) * m_range.
                //    new_mmode = SOHA.Library.MMode.XExtrapolate(m_mmode, m_zoom);
                //    b = new_mmode.ToBitmap(start, 0, m_interval.IntervalSize, m_mmode.Height);

                //}
                else
                {
                    b = m_mmode.ToBitmap();
                }

                Graphics g = Graphics.FromImage(b);

                if (m_list != null)
                {
                    IntervalCollection new_list = IntervalCollection.XExtrapolate(m_list, m_zoom);

                    //Paint IntervalCollection
                    for (int j = 0; j < new_list.Count; j++)
                    {
                        //if(new_list[j].IntervalType == IntervalType.Change && !m_show_change_intervals)
                        //    continue;

                        Pen p = new Pen(Color.Red, 1);
                        if ((new_list[j].Frame >= (m_interval.Interval - 1) * m_interval.IntervalSize) 
                            && (new_list[j].Frame < (m_interval.Interval) * m_interval.IntervalSize))
                        {
                            int x = new_list[j].Frame % m_interval.IntervalSize;
                            //x = (int)((m_intervallist[j].Frame % 1000) / 1000 * b.Width);

                            switch (new_list[j].IntervalType)
                            {
                                case IntervalType.Systole:
                                    p.Color = Color.Green;
                                    break;
                                case IntervalType.Diastole:
                                    p.Color = Color.Red;
                                    break;
                                case IntervalType.Change:
                                    p.Color = Color.Blue;
                                    break;
                                case IntervalType.MaxVelocity:
                                    p.Color = Color.Orange;
                                    break;
                                default:
                                    break;
                            }

                            if (new_list[j].Highlight)
                                p.Color = Color.Yellow;

                            g.DrawLine(p, x, 0, x, g.ClipBounds.Bottom);
                        }
                    }
                }

                g.Dispose();
                BackgroundImage = b;
                BackgroundImageLayout = ImageLayout.Stretch;
            }
        }

        protected new void Paint(VideoFrameRangeEventArgs e)
        {
            if (suspend_layout)
                return;

            Bitmap b;
            MMode new_mmode = new MMode();

            if(m_mmode != null)
            {
                //Paint MMode
                if(m_range != null)
                {
                    int start = e.Range.StartFrame;
                    int end = start + e.Range.Range;
                    new_mmode = SOHA.Library.MMode.XExtrapolate(m_mmode, m_zoom);
                    b = new_mmode.ToBitmap(start, 0, end - start, m_mmode.Height);

                }
                else
                {
                    b = m_mmode.ToBitmap();
                }

                Graphics g = Graphics.FromImage(b);

                if(m_list != null)
                {
                    int start = e.Range.StartFrame;
                    int end = start + e.Range.Range;

                    IntervalCollection list_within_range = new IntervalCollection(m_list.Where(x => x.Frame >= start && x.Frame <= end).ToList());

                    //Paint IntervalCollection
                    for(int j = 0; j < list_within_range.Count; j++)
                    {
                        if(list_within_range[j].IntervalType == IntervalType.Change && !m_show_change_intervals)
                            continue;

                        Pen p = new Pen(Color.Red, 1);

                        int x = list_within_range[j].Frame % e.Range.Range;

                        switch(list_within_range[j].IntervalType)
                        {
                            case IntervalType.Systole:
                                p.Color = Color.Green;
                                break;
                            case IntervalType.Diastole:
                                p.Color = Color.Red;
                                break;
                            case IntervalType.Change:
                                p.Color = Color.Blue;
                                break;
                            case IntervalType.MaxVelocity:
                                p.Color = Color.Orange;
                                break;
                            default:
                                break;
                        }

                        if(list_within_range[j].Highlight)
                            p.Color = Color.Yellow;

                        g.DrawLine(p, x, 0, x, g.ClipBounds.Bottom);
                    }
                }

                g.Dispose();
                BackgroundImage = b;
                BackgroundImageLayout = ImageLayout.Stretch;
            }
        }

        public double GetXValue(int x)
        {
            if (m_mmode == null)
                return 0;

            return (int)((x / (double)this.Width) * 1000) / m_zoom;
        }

        void MModeDisplay_MouseEnter(object sender, System.EventArgs e)
        {
            this.Focus();
        }

        void MModeDisplay_PreviewKeyDown(object sender, System.Windows.Forms.PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                lock (m_list)
                {
                    for (int i = 0; i < m_list.Count; i++)
                    {
                        if (m_list[i].Highlight)
                        {
                            m_list.RemoveAt(i--);
                        }
                    }
                }
            }

            Paint();
            OnListChanged();
        }

        int mmode_rtclk_x = 0; 
        void MModeDisplay_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                IntervalCollection new_list = IntervalCollection.XExtrapolate(m_list, m_zoom);
                int frame = ((m_interval.Interval - 1) * m_interval.IntervalSize) + (int)((e.X / (double)this.Width) * m_interval.IntervalSize);

                if (m_list != null)
                {
                    int index = new_list.IndexOfClosest(frame, Phase);

                    if (index >= 0)
                    {
                        if(Phase == PhaseFormType.Interval)
                            if (m_list[index].IntervalType == IntervalType.Systole 
                                || m_list[index].IntervalType == IntervalType.Diastole
                                || m_list[index].IntervalType == IntervalType.Change)
                                m_list[index].ToggleHighlight();

                        if(Phase == PhaseFormType.Phase)
                            if (m_list[index].IntervalType == IntervalType.MaxVelocity || m_list[index].IntervalType == IntervalType.Change)
                                m_list[index].ToggleHighlight();
                    }
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                mmode_rtclk_x = (int)(e.X / (double)this.Width * m_interval.IntervalSize);
                mmode_rtclk_x /= m_zoom;
                CreatePopupMenu();
                popupAddInterval.Show((sender as Control), e.X, e.Y);
            }

            Paint();
        }

        private void addSystoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_list.Add(new Interval(mmode_rtclk_x + (m_interval.IntervalSize * (m_interval.Interval - 1)), IntervalType.Systole));
            m_list.Sort();
            Paint();
            OnListChanged();
        }

        private void addDiastoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_list.Add(new Interval(mmode_rtclk_x + (m_interval.IntervalSize * (m_interval.Interval - 1)), IntervalType.Diastole));
            m_list.Sort();
            Paint();
            OnListChanged();
        }

        void addChangeToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            int x_coord = mmode_rtclk_x;
            x_coord += (int)(m_interval.IntervalSize / (double)m_zoom * (m_interval.Interval - 1));
            m_list.Add(new Interval(x_coord, IntervalType.Change));
            m_list.Sort();
            Paint();
            OnListChanged();
        }

        void addVelocityToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            int x_coord = mmode_rtclk_x;
            x_coord += (int)(m_interval.IntervalSize / (double)m_zoom * (m_interval.Interval - 1));
            m_list.Add(new Interval(x_coord, IntervalType.MaxVelocity));
            m_list.Sort();
            Paint();
            OnListChanged();
        }
    }
}
