﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Mathematics;
using SOHA.Library.Xml;

namespace SOHAControls
{

    public struct NumeralStats
    {
        public double Max;
        public double Min;
        public bool Set;
    }

    public partial class BrightDisplay : FunctionDisplay, IIntervalable, INotifyPropertyChanged
    {
        public event EventHandler IntervalsChanged;
        public event EventHandler ThresholdsChanged;

        //protected double[] m_thresholds = new double[2];

        protected int m_hi_threshold;
        protected int m_lo_threshold;
        protected bool m_invert;
        protected bool m_use_darkness;
        protected double m_hi_pass_frequency;
        protected Filter m_filter;
        public NumeralStats m_stats;
        private List<double> m_last_data;

        //public double[] Thresholds
        //{
        //    get
        //    {
        //        return m_thresholds;
        //    }
        //    set
        //    {
        //        m_thresholds = value;
        //        this.Invalidate();
        //    }
        //}
        [Bindable(true)]
        public int HiThreshold
        {
            get
            {
                return m_hi_threshold;
            }
            set
            {
                m_hi_threshold = value;
                OnPropertyChanged("HiThreshold");
                OnThresholdsChanged(new EventArgs());
                this.Invalidate();
            }
        }
        [Bindable(true)]
        public int LoThreshold
        {
            get
            {
                return m_lo_threshold;
            }
            set
            {
                m_lo_threshold = value;
                OnPropertyChanged("LoThreshold");
                OnThresholdsChanged(new EventArgs());
                this.Invalidate();
            }
        }
        [Bindable(true)]
        public bool Invert
        {
            get
            {
                return m_invert;
            }
            set
            {
                m_invert = value;
                OnPropertyChanged("Invert");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }
        [Bindable(true)]
        public bool UseDarkness
        {
            get
            {
                return m_use_darkness;
            }
            set
            {
                m_use_darkness = value;
                OnPropertyChanged("UseDarkness");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }
        [Bindable(true)]
        public double HiCutoff
        {
            get
            {
                return m_hi_cutoff;
            }
            set
            {
                m_hi_cutoff = value;
                OnPropertyChanged("HiCutoff");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }
        [Bindable(true)]
        public double LoCutoff
        {
            get
            {
                return m_lo_cutoff;
            }
            set
            {
                m_lo_cutoff = value;
                OnPropertyChanged("LoCutoff");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }

        public double FilterFrequency
        {
            get
            {
                return m_hi_pass_frequency;
            }
            set
            {
                if (value < 0 || value >= 1.0)
                    throw new ArgumentOutOfRangeException("FilterFrequency", "Value must be between 0 (inclusive) and 1.");

                m_hi_pass_frequency = value;
                this.Invalidate();
            }
        }
        public Filter Filter
        {
            get { return m_filter; }
            set
            {
                m_filter = value;
                base.Paint();
            }
        }

        public BrightDisplay()
        {
            InitializeComponent();

            plot.MouseClick += new MouseEventHandler(plot_MouseClick);
        }

        void plot_MouseClick(object sender, MouseEventArgs e)
        {
            //double[] thresh = (double[])Thresholds.Clone();
            int val = (int)(e.Y / (double)plot.ClientSize.Height * 100);

            if (e.Button == MouseButtons.Left)
                LoThreshold = val;

            if (e.Button == MouseButtons.Right)
                HiThreshold = val;

            this.Invalidate();
            //OnIntervalsChanged(new EventArgs());
            OnThresholdsChanged(new EventArgs());
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            //OnIntervalsChanged(new EventArgs());
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
        }

        protected void OnIntervalsChanged(EventArgs e)
        {
            if (IntervalsChanged != null)
                IntervalsChanged(this, e);
        }

        protected void OnThresholdsChanged(EventArgs e)
        {
            if (ThresholdsChanged != null)
                ThresholdsChanged(this, new EventArgs());
        }

        public override void PreFilter(ref List<double> func)
        {
            int N = 3;
            Filter m_filter = new Filter();
            double[] data = func.ToArray();
            bool perform_filter = false;

            double wc_high = HiCutoff / Fps;
            double wc_low = LoCutoff / Fps;

            if (wc_high > 0 && wc_high < 1 && wc_low > 0 && wc_low < 1 && wc_high > wc_low)
            {
                //BandPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_low, wc_high }, FilterType.Bandpass);
                perform_filter = true;
            }
            else if (wc_high > 0 && wc_high < 1)
            {
                //HighPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_high }, FilterType.Low);
                perform_filter = true;
            }
            else if (wc_low > 0 && wc_low < 1)
            {
                //LoPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_low }, FilterType.High);
                perform_filter = true;
            }

            if (perform_filter)
                data = m_filter.DualInputZeroPhaseFilter(data);

            //if (m_lo_pass_frequency > 0)
            //{
            //    Filter filter = IIRButterworth.Butter(4, new double[] { m_lo_pass_frequency }, FilterType.Low);
            //    data = filter.DualInputZeroPhaseFilter(data);
            //}

            double max = data.Max();
            double min = data.Min();

            if(m_stats.Set)
            {
                func = ArrayFunctions.Normalize(data.ToList(), m_stats.Max, m_stats.Min);
            }
            else
            {
                func = ArrayFunctions.Normalize(data.ToList(), out max, out min);
                //List<double> temp = ArrayFunctions.MaxMinList(data.ToList());
            }

            m_stats.Max = max;
            m_stats.Min = min;

            if (m_invert)
                ArrayFunctions.Invert(ref func);

            m_last_data = func;
        }

        public double[] GetValues(int x, int interval, int interval_size)
        {
            if (m_last_data == null || m_last_data.Count == 0)
                return new double[2];

            double[] point = new double[2];
            point[0] = (int)((x / (double)plot.Width) * interval_size) + (interval - 1) * 1000;
            if (point[0] < m_last_data.Count)
                point[1] = m_last_data[(int)point[0]];

            return point;
        }

        public override void AddThresholds(Graphics g, Bitmap b)
        {
            g.DrawLine(new Pen(Color.Green, 1),
                0,
                (float)m_lo_threshold,
                b.Width,
                (float)m_lo_threshold);
            g.DrawLine(new Pen(Color.Red, 1),
                0,
                (float)m_hi_threshold,
                b.Width,
                (float)m_hi_threshold);
        }

        public void LoadSettings(IntervalPhaseSettings settings)
        {
            m_hi_threshold = settings.BrightThresholdHi;
            m_lo_threshold = settings.BrightThresholdLo;
            this.Invalidate();
        }

        #region IIntervalable Members

        public List<Interval> GetIntervals()
        {

            List<Interval> ret = new List<Interval>();

            double[] tmpdata = new double[m_data.Count];
            m_data.CopyTo(tmpdata);
            List<double> temp = tmpdata.ToList();

            PreFilter(ref temp);
            double[] tmp = temp.ToArray();
            
            temp = temp.ToList();

            //return ArrayFunctions.MaxMinList(temp);

            double threshold_lo = (LoThreshold) / 100.0;
            double threshold_hi = (HiThreshold) / 100.0;


            for (int i = 1; i < temp.Count; i++)
            {
                IntervalType t = IntervalType.Period;
                if ((temp[i - 1] < threshold_lo) && (temp[i] > threshold_lo))
                {
                    t = IntervalType.Systole;
                }
                else if ((temp[i - 1] > threshold_lo) && (temp[i] < threshold_lo))
                {
                    t = IntervalType.Diastole;
                }
                else if ((temp[i - 1] < threshold_hi) && (temp[i] > threshold_hi))
                {
                    t = IntervalType.Diastole;
                }
                else if ((temp[i - 1] > threshold_hi) && (temp[i] < threshold_hi))
                {
                    t = IntervalType.Systole;
                }

                if(t != IntervalType.Period)
                    ret.Add(new Interval(i, t));
            }

            return ret;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        } 
    }
}
