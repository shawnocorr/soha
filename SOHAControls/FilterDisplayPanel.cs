﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHAControls.FilterControls;
using SOHA.Library.GPU;
using SOHA.Library.Imaging;

namespace SOHAControls
{
    public partial class FilterDisplayPanel : FlowLayoutPanel
    {

        public event EventHandler FilterUpdate;

        protected List<IFrameFunction> m_filters = new List<IFrameFunction>();

        public FilterDisplayPanel()
        {
            this.AllowDrop = true;
            InitializeComponent();
        }

        public FilterDisplayPanel(IContainer container)
        {
            this.AllowDrop = true;
            container.Add(this);

            InitializeComponent();

            ControlAdded += new ControlEventHandler(FiltersChanged);
            ControlRemoved -= new ControlEventHandler(FiltersChanged);
        }

        void FiltersChanged(object sender, ControlEventArgs e)
        {
            OnFilterUpdate(e);
        }

        protected override void OnDragDrop(DragEventArgs e)
        {
            //base.OnDragDrop(drgevent);

            IFrameFunction filter = (IFrameFunction)e.Data.GetData(DataFormats.Serializable);
            //ImageFilter filter = (ImageFilter)e.Data.GetData(DataFormats.Serializable);
            m_filters.Add(filter);
            ucFilterPanelDisplay display = new ucFilterPanelDisplay(filter);
            display.Width = this.Width - display.Margin.Left - display.Margin.Right;
            display.Height = 100;
            display.Tag = this.Controls.Count + 1;
            display.Text = filter.ToString();

            //flwPanelFilters.SuspendLayout();
            this.Controls.Add(display);

            this.Invalidate(true);
        }

        protected override void OnDragEnter(DragEventArgs e)
        {
            //base.OnDragEnter(drgevent);

            if(e.Data.GetDataPresent(DataFormats.Serializable))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        protected override void OnPreviewKeyDown(PreviewKeyDownEventArgs e)
        {
            if(e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                this.SuspendLayout();
                for(int i = 0; i < this.Controls.Count; i++)
                {
                    if(((ISelectable)this.Controls[i]).IsSelected)
                    {
                        this.Controls.RemoveAt(i);
                        m_filters.RemoveAt(i);
                        i--;
                    }
                }
                this.ResumeLayout();
            }
            else if(e.KeyCode == Keys.A || e.KeyCode == Keys.D)
            {

            }
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            this.Focus();
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            this.Focus();
            base.OnMouseLeave(e);
        }

        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    base.OnPaint(e);
        //}

        protected virtual void OnFilterUpdate(EventArgs e)
        {
            if(FilterUpdate != null)
                FilterUpdate(this, e);
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }

        public virtual List<IFrameFunction> GetFilters()
        {
            return m_filters;
        }

        public virtual void SetFilters(List<IFrameFunction> filters)
        {
            m_filters = filters;

            this.Controls.Clear();
            foreach(ImageFilter filter in m_filters)
            {
                ucFilterPanelDisplay display = new ucFilterPanelDisplay(filter);
                display.Width = this.Width - display.Margin.Left - display.Margin.Right;
                display.Height = 100;
                display.Tag = this.Controls.Count + 1;
                display.Text = filter.ToString();

                this.Controls.Add(display);
                display.Refresh();
            }
        }

        
    }
}
