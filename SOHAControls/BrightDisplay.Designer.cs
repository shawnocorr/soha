﻿using System.Windows.Forms;
using System.Drawing;
namespace SOHAControls
{
    partial class BrightDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;

            //InitializeToolStripMenu();
        }

        private void InitializeToolStripMenu()
        {
            this.toolStrip = new ToolStrip();
            this.thresholdDropDown = new ToolStripDropDownButton();
            this.thresholdDropDownHost = new ToolStripDropDown();
            this.thresholdPanel = new TableLayoutPanel();
            this.hiThreshScrollBar = new VScrollBar();
            this.loThreshScrollBar = new VScrollBar();
            this.lblHiThresh = new Label();
            this.lblLoThresh = new Label();

            this.filterDropDown = new ToolStripDropDownButton();
            this.filterDropDownHost = new ToolStripDropDown();
            this.filterPanel = new TableLayoutPanel();
            this.filterScrollBar = new HScrollBar();

            this.useDarkness = new ToolStripButton();

            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.thresholdPanel.SuspendLayout();
            this.filterPanel.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();

            this.toolStrip.AllowDrop = false;
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            this.toolStripContainer1.TopToolStripPanel.RenderMode = ToolStripRenderMode.System;
            this.toolStrip.RenderMode = ToolStripRenderMode.System;
            //this.toolStrip.BackColor = Color.Transparent;

            this.useDarkness.Text = "Use Darkness";
            this.useDarkness.CheckOnClick = true;

            //Threshold Drop Down

            this.hiThreshScrollBar.Margin = Padding.Empty;
            this.hiThreshScrollBar.Anchor = AnchorStyles.None;
            this.hiThreshScrollBar.Name = "hiThreshScrollBar";
            this.loThreshScrollBar.Margin = Padding.Empty;
            this.loThreshScrollBar.Anchor = AnchorStyles.None;
            this.loThreshScrollBar.Name = "loThreshScrollBar";
            this.lblHiThresh.Text = "High";
            this.lblHiThresh.Anchor = AnchorStyles.None;
            this.lblLoThresh.Text = "Low";
            this.lblLoThresh.Anchor = AnchorStyles.None;
            // 
            // tableLayoutPanel1
            // 
            this.thresholdPanel.ColumnCount = 2;
            this.thresholdPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.thresholdPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.thresholdPanel.Controls.Add(this.lblLoThresh, 1, 0);
            this.thresholdPanel.Controls.Add(this.hiThreshScrollBar, 0, 1);
            this.thresholdPanel.Controls.Add(this.loThreshScrollBar, 1, 1);
            this.thresholdPanel.Controls.Add(this.lblHiThresh, 0, 0);
            this.thresholdPanel.Location = new System.Drawing.Point(12, 12);
            this.thresholdPanel.Name = "tableLayoutPanel1";
            this.thresholdPanel.RowCount = 2;
            this.thresholdPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, this.lblHiThresh.Height));
            this.thresholdPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, this.hiThreshScrollBar.Height));
            this.thresholdPanel.Size = new System.Drawing.Size(50, 100);
            this.thresholdPanel.TabIndex = 0;
            this.thresholdPanel.Anchor = AnchorStyles.None;

            this.thresholdDropDownHost.Items.Add(new ToolStripControlHost(this.thresholdPanel));

            this.thresholdDropDown.DropDown = this.thresholdDropDownHost;
            this.thresholdDropDown.BackColor = Color.Transparent;
            this.thresholdDropDown.Padding = Padding.Empty;
            this.thresholdDropDown.Text = "Thresholds";

            //Filter Drop Down

            this.filterScrollBar.Margin = Padding.Empty;
            this.filterScrollBar.Anchor = AnchorStyles.None;
            this.filterScrollBar.Name = "filterScrollBar";
            //this.lblFilter.Text = "HP Filter";
            //this.lblFilter.Anchor = AnchorStyles.None;

            //this.filterPanel.ColumnCount = 1;
            //this.filterPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            //this.filterPanel.RowCount = 2;
            //this.filterPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
            //this.filterPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 80f));
            //this.filterPanel.Name = "filterPanel";
            //this.filterPanel.Location = new System.Drawing.Point(12, 12);
            //this.filterPanel.Size = new System.Drawing.Size(50, 100);
            //this.filterPanel.TabIndex = 0;
            //this.filterPanel.Anchor = AnchorStyles.None;
            //this.filterPanel.Controls.Add(this.lblFilter, 0, 0);
            //this.filterPanel.Controls.Add(this.filterScrollBar, 0, 1);

            this.filterDropDownHost.Items.Add(new ToolStripControlHost(this.filterScrollBar));

            this.filterDropDown.DropDown = this.filterDropDownHost;
            this.filterDropDown.BackColor = Color.Transparent;
            this.filterDropDown.Padding = Padding.Empty;
            this.filterDropDown.Text = "HP Filter";

            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                this.useDarkness,
                this.thresholdDropDown,
                this.filterDropDown});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip1";
            this.toolStrip.Size = new System.Drawing.Size(95, 25);
            this.toolStrip.TabIndex = 0;

            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.thresholdPanel.ResumeLayout(false);
            this.thresholdPanel.PerformLayout();
            this.ResumeLayout(false);
        }

        protected ToolStrip toolStrip;
        protected ToolStripDropDownButton thresholdDropDown;
        protected ToolStripDropDown thresholdDropDownHost;
        protected VScrollBar hiThreshScrollBar;
        protected VScrollBar loThreshScrollBar;
        protected Label lblHiThresh;
        protected Label lblLoThresh;
        protected TableLayoutPanel thresholdPanel;

        protected ToolStripDropDownButton filterDropDown;
        protected ToolStripDropDown filterDropDownHost;
        protected HScrollBar filterScrollBar;
        protected TableLayoutPanel filterPanel;

        protected ToolStripButton useDarkness;

        #endregion
    }
}
