﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library;

namespace SOHAControls
{
    interface IIntervalable
    {
        List<Interval> GetIntervals();
    }
}
