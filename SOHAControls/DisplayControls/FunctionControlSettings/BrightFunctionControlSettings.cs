﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SOHAControls.DisplayControls.FunctionControlSettings
{
    public class BrightFunctionControlSettings : EventArgs, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected bool m_invert = false;

        public bool Invert
        {
            get
            {
                return m_invert;
            }
            set
            {
                m_invert = value;
                OnPropertyChanged("Invert");
            }
        }

        public BrightFunctionControlSettings()
        {

        }


        protected void OnPropertyChanged(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
