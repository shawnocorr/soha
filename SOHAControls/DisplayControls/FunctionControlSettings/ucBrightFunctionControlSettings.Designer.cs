﻿namespace SOHAControls.DisplayControls.FunctionControlSettings
{
    partial class ucBrightFunctionControlSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.scrollDarknessHigh = new System.Windows.Forms.VScrollBar();
            this.scrollDarknessLow = new System.Windows.Forms.VScrollBar();
            this.chkInvert = new System.Windows.Forms.CheckBox();
            this.chkUseDarkness = new System.Windows.Forms.CheckBox();
            this.bpFilterBright = new SOHAControls.BandPassFilterControl();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.scrollDarknessHigh, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.scrollDarknessLow, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.chkInvert, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.chkUseDarkness, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.bpFilterBright, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(443, 250);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Low";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "High";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scrollDarknessHigh
            // 
            this.scrollDarknessHigh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrollDarknessHigh.Location = new System.Drawing.Point(11, 20);
            this.scrollDarknessHigh.Maximum = 111;
            this.scrollDarknessHigh.Name = "scrollDarknessHigh";
            this.tableLayoutPanel4.SetRowSpan(this.scrollDarknessHigh, 3);
            this.scrollDarknessHigh.Size = new System.Drawing.Size(17, 230);
            this.scrollDarknessHigh.TabIndex = 2;
            // 
            // scrollDarknessLow
            // 
            this.scrollDarknessLow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrollDarknessLow.Location = new System.Drawing.Point(51, 20);
            this.scrollDarknessLow.Maximum = 111;
            this.scrollDarknessLow.Name = "scrollDarknessLow";
            this.tableLayoutPanel4.SetRowSpan(this.scrollDarknessLow, 3);
            this.scrollDarknessLow.Size = new System.Drawing.Size(17, 230);
            this.scrollDarknessLow.TabIndex = 3;
            // 
            // chkInvert
            // 
            this.chkInvert.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkInvert.AutoSize = true;
            this.chkInvert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkInvert.Location = new System.Drawing.Point(83, 193);
            this.chkInvert.Name = "chkInvert";
            this.chkInvert.Size = new System.Drawing.Size(357, 24);
            this.chkInvert.TabIndex = 4;
            this.chkInvert.Text = "Invert";
            this.chkInvert.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chkInvert.UseVisualStyleBackColor = true;
            // 
            // chkUseDarkness
            // 
            this.chkUseDarkness.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUseDarkness.AutoSize = true;
            this.chkUseDarkness.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkUseDarkness.Location = new System.Drawing.Point(83, 223);
            this.chkUseDarkness.Name = "chkUseDarkness";
            this.chkUseDarkness.Size = new System.Drawing.Size(357, 24);
            this.chkUseDarkness.TabIndex = 5;
            this.chkUseDarkness.Text = "Use Darkness";
            this.chkUseDarkness.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chkUseDarkness.UseVisualStyleBackColor = true;
            // 
            // bpFilterBright
            // 
            this.bpFilterBright.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bpFilterBright.Filter = null;
            this.bpFilterBright.HiCutoff = 0D;
            this.bpFilterBright.Location = new System.Drawing.Point(83, 3);
            this.bpFilterBright.LoCutoff = 0D;
            this.bpFilterBright.Name = "bpFilterBright";
            this.tableLayoutPanel4.SetRowSpan(this.bpFilterBright, 2);
            this.bpFilterBright.Size = new System.Drawing.Size(357, 184);
            this.bpFilterBright.TabIndex = 6;
            // 
            // ucBrightFunctionControlSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Name = "ucBrightFunctionControlSettings";
            this.Size = new System.Drawing.Size(443, 250);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.VScrollBar scrollDarknessHigh;
        private System.Windows.Forms.VScrollBar scrollDarknessLow;
        private System.Windows.Forms.CheckBox chkInvert;
        private System.Windows.Forms.CheckBox chkUseDarkness;
        private BandPassFilterControl bpFilterBright;
    }
}
