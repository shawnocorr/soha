﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHAControls.DisplayControls.FunctionControlSettings
{
    public partial class ucBrightFunctionControlSettings : UserControl
    {
        public event EventHandler<BrightFunctionControlSettings> SettingsChanged;

        protected BrightFunctionControlSettings m_settings = new BrightFunctionControlSettings();
        
        public ucBrightFunctionControlSettings()
        {
            InitializeComponent();

            //chkInvert.DataBindings.Add("Checked", m_settings, "Invert", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private BrightFunctionControlSettings GetSettings()
        {
            BrightFunctionControlSettings settings = new BrightFunctionControlSettings();
            settings.Invert = chkInvert.Checked;

            return settings;
        }

        protected virtual void OnSettingsChanged()
        {
            if(SettingsChanged != null)
                SettingsChanged(this, GetSettings());
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
