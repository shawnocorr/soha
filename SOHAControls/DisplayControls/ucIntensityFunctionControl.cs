﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library;
using ZedGraph;
using SOHA.Library.Xml;

namespace SOHAControls.DisplayControls
{
    public partial class ucIntensityFunctionControl : ucFunctionControl, IIntervalableFunctionControl
    {

        //protected int m_min_intervals;

        //[Bindable(true)]
        //public double Threshold
        //{
        //    get
        //    {
        //        return m_thresholds[0];
        //    }
        //    set
        //    {
        //        m_thresholds[0] = value;
        //        OnPropertyChanged("Threshold");
        //        OnThresholdsChanged(new EventArgs());
        //        this.Invalidate();
        //    }
        //}
        //[Bindable(true)]
        //public int MinIntervals
        //{
        //    get
        //    {
        //        return m_min_intervals;
        //    }
        //    set
        //    {
        //        m_min_intervals = value;
        //        OnPropertyChanged("MinIntervals");
        //        OnThresholdsChanged(new EventArgs());
        //        //Paint();
        //        //this.Invalidate();
        //    }
        //}

        public ucIntensityFunctionControl()
            :base()
        {
            InitializeComponent();

            //m_min_intervals = 8;
        }

        public List<Interval> GetIntervals()
        {
            if(!m_loaded || m_graphed_values == null) return null;

            List<Interval> ret = new List<Interval>();

            List<double> temp = m_graphed_values;
            double max = m_graphed_values.Max();
            double min = m_graphed_values.Min();
            double spread = max - min;

            //double[] tmpdata = new double[m_data.Count];
            //m_data.CopyTo(tmpdata);
            //List<double> temp = tmpdata.ToList();

            //PreFilter(ref temp);

            double threshold = (m_settings as IntervalPhaseSettings).IntensityThreshold / 100.0 * spread + min;// m_thresholds[0]; //(100 - m_thresholds[0]) / 100.0;

            //Scale threshold value to range of plot y values

            for(int i = 1; i < temp.Count; i++)
            {
                if((temp[i - 1] < threshold) && (temp[i] > threshold))
                {
                    Interval l_interval = new Interval(i, IntervalType.Systole);
                    ret.Add(l_interval);
                }
                else if((temp[i - 1] > threshold) && (temp[i] < threshold))
                {
                    Interval l_interval = new Interval(i, IntervalType.Diastole);
                    ret.Add(l_interval);
                }
            }

            return ret;
        }

        public override void DrawGraph()
        {
            if(!m_loaded || this.DesignMode || m_fx == null || m_suspend_graph)
                return;

            if(zedGraphControl1.GraphPane == null)
                return;

            GraphPane myPane = zedGraphControl1.GraphPane;
            CurveItem l_function = myPane.CurveList["Function"];
            myPane.CurveList.Clear();
            //myPane.CurveList.Remove(l_function);
            //myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            myPane.Title.Text = "Intensity";
            myPane.XAxis.Title.Text = "Frame";
            myPane.YAxis.Title.Text = "Value";

            //Set data to initial function value
            double[] data = m_fx.Values.ToArray();

            #region Filter, if necessary

            bool perform_filter = false;
            Filter m_filter = new Filter();
            m_filter.HiCutoff = (m_settings as IntervalPhaseSettings).IntensityHiCutoff;
            m_filter.LoCutoff = (m_settings as IntervalPhaseSettings).IntensityLoCutoff;
            m_filter.Fps = 160;

            if(m_filter != null)
            {
                int N = 3;

                double wc_high = m_filter.HiCutoff / m_filter.Fps;
                double wc_low = m_filter.LoCutoff / m_filter.Fps;

                if(wc_high > 0 && wc_high < 1 && wc_low > 0 && wc_low < 1 && wc_high > wc_low)
                {
                    //BandPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_low, wc_high }, FilterType.Bandpass);
                    perform_filter = true;
                }
                else if(wc_high > 0 && wc_high < 1)
                {
                    //HighPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_high }, FilterType.Low);
                    perform_filter = true;
                }
                else if(wc_low > 0 && wc_low < 1)
                {
                    //LoPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_low }, FilterType.High);
                    perform_filter = true;
                }

                if(perform_filter)
                    data = m_filter.DualInputZeroPhaseFilter(data);
            }

            m_graphed_values = data.ToList();
            //if (m_lo_pass_frequency > 0)
            //{
            //    Filter filter = IIRButterworth.Butter(4, new double[] { m_lo_pass_frequency }, FilterType.Low);
            //    data = filter.DualInputZeroPhaseFilter(data);
            //}

            //double max = data.Max();
            //double min = data.Min();

            //if(m_stats.Set)
            //{
            //    func = ArrayFunctions.Normalize(data.ToList(), m_stats.Max, m_stats.Min);
            //}
            //else
            //{
            //    func = ArrayFunctions.Normalize(data.ToList(), out max, out min);
            //    //List<double> temp = ArrayFunctions.MaxMinList(data.ToList());
            //}

            //m_stats.Max = max;
            //m_stats.Min = min;

            //m_last_data = func;

            #endregion

            List<double> filtered_data = data.ToList();

            if(IsAlwaysNormalize)
            {
                filtered_data = ArrayFunctions.Normalize(filtered_data);
                zedGraphControl1.GraphPane.YAxis.Scale.Min = 0;
                zedGraphControl1.GraphPane.YAxis.Scale.Max = 1.0;
            }

            //Get function
            PointPairList y = new PointPairList();
            for(int i = m_range.StartFrame; i < m_range.EndFrame && i < filtered_data.Count; i++)
            {
                y.Add(i, filtered_data[i]);
            }

            LineItem barPDF = myPane.AddCurve("Function", y, Color.Aqua, SymbolType.None);

            DrawThresholds();

            zedGraphControl1.GraphPane.XAxis.Scale.Min = m_range.StartFrame;
            zedGraphControl1.GraphPane.XAxis.Scale.Max = m_range.StartFrame + m_range.Range;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public override void DrawThresholds()
        {
            if(!m_loaded) return;
            
            GraphPane myPane = zedGraphControl1.GraphPane;
            RectangleF plot = myPane.Chart.Rect;
            RectangleF control = myPane.Rect;

            myPane.CurveList.Remove(myPane.CurveList["ThresholdLo"]);
            myPane.CurveList.Remove(myPane.CurveList["ThresholdHi"]);

            double y0 = (m_settings as IntervalPhaseSettings).IntensityThreshold;
            if(y0 != null && y0 != 0)
            {

                if(IsAlwaysNormalize)
                {
                    //If > 1, treat as a percent
                    if(y0 > 1) y0 /= 100;
                }

                PointPairList y = new PointPairList();
                for(int i = 0; i < m_fx.Values.Count; i++)
                {
                    y.Add(i, y0);
                }

                LineItem barPDF = myPane.AddCurve("ThresholdLo", y, Color.Green, SymbolType.None);
            }

            zedGraphControl1.Refresh();
        }
    }
}
