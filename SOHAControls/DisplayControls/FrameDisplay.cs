﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Video;
using SOHA.Library.Xml;
using SOHA.Library.Mathematics;
using SOHA.Library.GPU;
using SOHA.Library.Imaging;
using System.Diagnostics;

namespace SOHAControls.DisplayControls
{
    public partial class FrameDisplay : UserControl, IFrameDisplay
    {
        private const int LARGE_INTERVAL_FRAME_SKIP = 5;

        public event EventHandler VideoChanged;
        public event EventHandler<FrameEventArgs> FrameChanged;
        public event EventHandler CoordinatesChanged;
        public event EventHandler DataChanged;
        public event EventHandler FiltersChanged;
        public event EventHandler HistogramUpdated;

        private bool MOUSE_OVER = false;
        private MouseEventArgs MOUSE_DRAG_START;
        private MouseEventArgs MOUSE_DRAG;

        private int m_frame_number = 0;
        private Bitmap m_current_bmp;
        private Frame m_post_function_frame = null;
        private Frame m_frame;
        private Video m_video;

        private Point m_coordinates;
        private SohaCollection<Roi> m_roi = new SohaCollection<Roi>();
        private SohaCollection<MarkPair> m_marks = new SohaCollection<MarkPair>();
        private List<IFrameFunction> m_filters = new List<IFrameFunction>();
        private Stopwatch swFilters = new Stopwatch();

        public Stopwatch LastFilterWatch
        {
            get
            {
                return swFilters;
            }
        }

        public Frame PostOpFrame
        {
            get
            {
                return m_post_function_frame;
            }
        }

        private bool m_interactive;
        [Browsable(true)]
        [ReadOnly(false)]
        public bool UserInteractive
        {
            get
            {
                return m_interactive;
            }
            set
            {
                m_interactive = value;
                OnUserInteractiveChanged();
            }
        }

        private bool m_mmode;
        [Browsable(true)]
        [ReadOnly(false)]
        public bool MmodeDisplay
        {
            get
            {
                return m_mmode;
            }
            set
            {
                m_mmode = value;
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public Point Coordinates
        {
            get { return m_coordinates; }
            set 
            {
                m_coordinates = value;
                OnCoordinatesChanged();
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public Frame CurrentFrame
        {
            get { return m_frame; }
            private set 
            {
                if(value == null)
                    return;

                m_frame = value;
                OnFrameChanged();
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public Video VideoFile
        {
            get { return m_video; }
            set
            {
                if(value == null)
                    return;

                m_video = value;
                OnVideoChanged();
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public SohaCollection<MarkPair> Marks
        {
            get { return m_marks; }
            set { m_marks = value; }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public SohaCollection<Roi> ROI
        {
            get { return m_roi; }
            set { m_roi = value; }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public List<IFrameFunction> Filters
        {
            get
            {
                return m_filters;
            }
            set
            {
                m_filters = value;
                OnFiltersChanged();
            }
        }

        public bool EnableFilters
        {
            get;
            set;
        }

        #region Drawing Pens

        private Pen m_highlight_pen = new Pen(Color.Yellow, 3);

        public Pen HighlightPen
        {
            get { return m_highlight_pen; }
            set { m_highlight_pen = value; }
        }

        private Pen m_normal_pen = new Pen(Color.Red, 1);

        public Pen NormalPen
        {
            get { return m_normal_pen; }
            set { m_normal_pen = value; }
        }

        #endregion

        public FrameDisplay()
        {
            InitializeComponent();
            Initialize();
        }

        private void Initialize()
        {

            m_frame = Frame.BlankFrame(imgFrame.Width, imgFrame.Height);
            EnableFilters = false;
        }

        new public void KeyPress(KeyEventArgs key_event)
        {
            bool perform = true;
            int current = hscrollFrame.Value;
            int val = (key_event.Shift) ? LARGE_INTERVAL_FRAME_SKIP : 1;
            int newval = 0;

            switch(key_event.KeyCode)
            {
                case Keys.A:
                case Keys.Left:
                    newval = current - val;
                    break;
                case Keys.D:
                case Keys.Right:
                    newval = current + val;
                    break;
                default:
                    perform = false;
                    break;
            }

            if(perform)
            {
                if(newval < 1)
                    newval = 1;

                if(newval > m_video.Frames)
                    newval = m_video.Frames;

                hscrollFrame.Value = newval;

                CurrentFrame = Options.AutoPreAnalyze ? m_video[newval] : m_video.GetFrameSync(newval);
                //if(PreAnalyzed)
                //    CurrentFrame = m_video[newval];//m_video.GetFrameSync(newval);
            }
        }

        #region PictureBox Event Handlers

        #region Mouse Event Handlers

        protected override void OnResize(EventArgs e)
        {
            MOUSE_DRAG_START = null;
            base.OnResize(e);
        }

        private void imgFrame_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
                MOUSE_DRAG_START = e;
        }

        private void imgFrame_MouseEnter(object sender, EventArgs e)
        {
            MOUSE_OVER = true;
            Cursor.Hide();
            imgFrame.Invalidate();
        }

        private void imgFrame_MouseLeave(object sender, EventArgs e)
        {
            MOUSE_OVER = false;
            Cursor.Show();
            imgFrame.Invalidate();
        }

        private void imgFrame_MouseMove(object sender, MouseEventArgs e)
        {
            if(MOUSE_DRAG_START != null && e.Button != System.Windows.Forms.MouseButtons.Left)
            {
                MOUSE_DRAG_START = null;
            }
            else
            {
                MOUSE_DRAG = e;
            }

            Coordinates = new Point(e.X, imgFrame.Height - e.Y);
            //this.Refresh();
            imgFrame.Invalidate();
        }

        private void imgFrame_MouseUp(object sender, MouseEventArgs e)
        {
            MOUSE_DRAG = null;

            if(e.Button == MouseButtons.Left)
            {
                if(MOUSE_DRAG_START == null)
                    return;

                if((Math.Abs(e.X - MOUSE_DRAG_START.X) <= 10) && (Math.Abs(e.Y - MOUSE_DRAG_START.Y) <= 10))
                {//Just a regular click, add a measurement point

                    //Convert
                    System.Drawing.Point newPoint = e.Location;
                    System.Drawing.Size l_init_size = new System.Drawing.Size(m_frame.Width, m_frame.Height);

                    int x = (int)((double)MOUSE_DRAG_START.X / imgFrame.Width * l_init_size.Width);
                    int y = (int)((double)MOUSE_DRAG_START.Y / imgFrame.Height * l_init_size.Height);

                    //Add
                    Point p = new Point(x, y);
                    bool point_added = false;
                    foreach(MarkPair mp in m_marks)
                    {
                        if(mp.Mark1 == null || mp.Mark1.Equals(new Point(0, 0)))
                        {
                            mp.Mark1 = p;
                            point_added = true;
                            break;
                        }
                        if(mp.Mark2 == null || mp.Mark2.Equals(new Point(0, 0)))
                        {
                            mp.Mark2 = p;
                            point_added = true;
                            break;
                        }
                    }

                    if(!point_added)
                    {
                        m_marks.Add(new MarkPair(p));
                    }
                    else
                    {
                        
                    }
                }
                else
                {//A Click and Drag, add an ROI

                    //Convert
                    Size l_init_size = m_frame.Size;
                    int x1 = (int)((double)MOUSE_DRAG_START.X / imgFrame.Width * l_init_size.Width);
                    int y1 = (int)((double)MOUSE_DRAG_START.Y / imgFrame.Height * l_init_size.Height);
                    int x2 = (int)((double)e.X / imgFrame.Width * l_init_size.Width);
                    int y2 = (int)((double)e.Y / imgFrame.Height * l_init_size.Height);
                        
                    //Insures Rectangle/ROI's TopLeft Position is first
                    //TopLeft is minimum in X and Y, absolute distance in each axis is the length of the sides
                    int min_x = Math.Min(x1, x2);
                    int min_y = Math.Min(y1, y2); 
                    int max_x = Math.Max(x1, x2);
                    int max_y = Math.Max(y1, y2);
                    double dx = x2 - x1, dy = y2 - y1;
                    //double tan = Math.Tan(dy / dx);
                    //double sin = Math.Sin(dy / dx);
                    //double cos = Math.Cos(dy / dx);

                    Roi new_roi = new Roi(min_x, min_y, (int)Math.Abs(dx), (int)Math.Abs(dy));
                    //Roi new_roi = new Roi(x1, y1, x2 - x1, y2 - y1);

                    m_roi.Add(new_roi);
                }

                OnDataChanged();
            }
        }

        #endregion

        private void imgFrame_Paint(object sender, PaintEventArgs e)
        {            
            if(CurrentFrame != null && !this.DesignMode)
            {
                m_post_function_frame = (Frame)CurrentFrame.Clone();

                try
                {
                    Frame frm = (Frame)CurrentFrame.Clone();
                    Bitmap b = m_current_bmp;//Conversions.FromFrame(CurrentFrame, (double)numContrast.Value);
                    List<IFrameFunction> filters = m_filters;//frmFilters.GetAllFilters();

                    //GpuFrame gFrame = null;
                    swFilters.Reset();
                    swFilters.Start();
                    if(EnableFilters && filters.Count > 0)
                    {
                        foreach(IFrameFunction func in filters)
                        {
                            switch(func.FuncType)
                            {
                                case FunctionType.Function:

                                    frm = func.Execute(frm);

                                    break;
                                case FunctionType.Filter:

                                    frm = func.Execute(frm);

                                    break;
                                default:
                                    break;
                            }
                        }
                        swFilters.Stop();

                        m_post_function_frame = (Frame)frm.Clone();

                        b = frm.ToBitmap();
                        e.Graphics.DrawImage(b, imgFrame.Bounds, 0, 0, CurrentFrame.Width, CurrentFrame.Height, GraphicsUnit.Pixel);
                    }


                    imgFrame.BackgroundImage = b;
                    imgFrame.BackgroundImageLayout = ImageLayout.Stretch;

                    if(m_interactive)
                    {
                        Size l_initial_size = CurrentFrame.Size;

                        #region Paint MarkList

                        foreach(MarkPair mp in m_marks)
                        {
                            Pen p = mp.Highlight ? HighlightPen : NormalPen;
                            int radius = 2;

                            int x1 = (int)((double)mp.Mark1.X / l_initial_size.Width * imgFrame.Width);
                            int y1 = (int)((double)mp.Mark1.Y / l_initial_size.Height * imgFrame.Height);
                            e.Graphics.DrawEllipse(p, new System.Drawing.Rectangle(x1 - radius, y1 - radius, 2 * radius, 2 * radius));

                            if(mp.Mark2 != null)
                            {
                                int x2 = (int)((double)mp.Mark2.X / l_initial_size.Width * imgFrame.Width);
                                int y2 = (int)((double)mp.Mark2.Y / l_initial_size.Height * imgFrame.Height);
                                e.Graphics.DrawEllipse(p, new System.Drawing.Rectangle(x2 - radius, y2 - radius, 2 * radius, 2 * radius));
                            }
                        }

                        #endregion

                        #region Paint RoiList

                        foreach(Roi roi in m_roi)
                        {
                            Pen p = roi.Highlight ? HighlightPen : NormalPen;

                            double xf = (double)l_initial_size.Width / imgFrame.Width;
                            double yf = (double)l_initial_size.Height / imgFrame.Height;
                            System.Drawing.Rectangle r = new System.Drawing.Rectangle((int)(roi.X / xf), (int)(roi.Y / yf), (int)(roi.Width / xf), (int)(roi.Height / yf));

                            e.Graphics.DrawRectangle(p, r);
                        }

                        #endregion

                        #region Paint Crosshairs

                        if(MOUSE_OVER)
                        {
                            System.Drawing.Point screen_pos = imgFrame.PointToClient(Cursor.Position);
                            e.Graphics.DrawLine(m_normal_pen, 0, screen_pos.Y, imgFrame.Width, screen_pos.Y);
                            e.Graphics.DrawLine(m_normal_pen, screen_pos.X, 0, screen_pos.X, imgFrame.Height);
                        }

                        #endregion

                        #region Paint In-Progress ROI

                        if(MOUSE_DRAG_START != null && MOUSE_DRAG != null)
                        {
                            int min_x = Math.Min(MOUSE_DRAG_START.X, MOUSE_DRAG.X);
                            int min_y = Math.Min(MOUSE_DRAG_START.Y, MOUSE_DRAG.Y);
                            int max_x = Math.Max(MOUSE_DRAG_START.X, MOUSE_DRAG.X);
                            int max_y = Math.Max(MOUSE_DRAG_START.Y, MOUSE_DRAG.Y);
                            double dx = MOUSE_DRAG.X - MOUSE_DRAG_START.X, dy = MOUSE_DRAG.Y - MOUSE_DRAG_START.Y;

                            System.Drawing.Rectangle r = new System.Drawing.Rectangle(
                                min_x,
                                min_y,
                                (int)Math.Abs(dx),
                                (int)Math.Abs(dy)
                                );

                            e.Graphics.DrawRectangle(NormalPen, r);
                        }

                        #endregion
                    }

                }
                catch(Exception exc)
                {
                    Logs.LogException(exc);
                    MessageBox.Show("Error Painting Frame: " + exc.Message);
                }
                finally
                {
                }

                //OnHistogramUpdated();
            }
        }

        public void ReDraw()
        {
            imgFrame_Paint(this, new PaintEventArgs(this.CreateGraphics(),this.Bounds));
        }

        #endregion

        #region FrameDisplay Event Fires

        protected void OnFiltersChanged()
        {
            if(FiltersChanged != null)
                FiltersChanged(this, new EventArgs());

            imgFrame.Refresh();
        }

        protected void OnCoordinatesChanged()
        {
            if(CoordinatesChanged != null)
                CoordinatesChanged(this, new EventArgs());
        }

        protected void OnVideoChanged()
        {

            if(Options.AutoPreAnalyze)
                m_video.PreAnalyze(new ProcessEventArgs(), null);

            m_video.Open();
            m_frame_number = 1;
            CurrentFrame = Options.AutoPreAnalyze ? m_video[m_frame_number] : m_video.GetFrameSync(m_frame_number);

            //CurrentFrame = m_video[m_frame_number];//m_video.GetFrameSync(m_frame_number);

            //Setup Scroll Bar
            hscrollFrame.Minimum = 1;
            hscrollFrame.Maximum = m_video.Frames;
            hscrollFrame.LargeChange = LARGE_INTERVAL_FRAME_SKIP;
            hscrollFrame.Value = 1;
            hscrollFrame.Scroll += new ScrollEventHandler(hscrollFrame_Scroll);
            
            if(VideoChanged != null)
                VideoChanged(this, new EventArgs());
        }

        protected void OnFrameChanged()
        {
            //Async Refresh, cascade into Async Histogram Update
            imgFrame.Invalidate(true);
            this.Update();

            m_current_bmp = CurrentFrame.ToBitmap();
                        
            if(FrameChanged != null)
                FrameChanged(this, new FrameEventArgs(m_frame));
        }

        protected void OnDataChanged()
        {
            if(DataChanged != null)
                DataChanged(this, new EventArgs());
        }
        
        protected void OnUserInteractiveChanged()
        {
            if(m_interactive)
            {
                this.imgFrame.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseDown);
                this.imgFrame.MouseEnter += new System.EventHandler(this.imgFrame_MouseEnter);
                this.imgFrame.MouseLeave += new System.EventHandler(this.imgFrame_MouseLeave);
                this.imgFrame.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseMove);
                this.imgFrame.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseUp);
            }
            else
            {
                this.imgFrame.MouseDown -= new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseDown);
                this.imgFrame.MouseEnter -= new System.EventHandler(this.imgFrame_MouseEnter);
                this.imgFrame.MouseLeave -= new System.EventHandler(this.imgFrame_MouseLeave);
                this.imgFrame.MouseMove -= new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseMove);
                this.imgFrame.MouseUp -= new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseUp);
            }
        }

        protected void OnHistogramUpdated()
        {
            if(HistogramUpdated != null)
                HistogramUpdated(this, new EventArgs());
        }

        #endregion

        void hscrollFrame_Scroll(object sender, ScrollEventArgs e)
        {
            if(e.NewValue != e.OldValue)
            {
                m_frame_number = e.NewValue;
                CurrentFrame = Options.AutoPreAnalyze ? m_video[m_frame_number] : m_video.GetFrameSync(m_frame_number);

                //CurrentFrame = m_video[m_frame_number]; //m_video.GetFrameSync(m_frame_number);
            }
        }
    }
}
