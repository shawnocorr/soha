﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Xml;
using ZedGraph;
using SOHA.Library;

namespace SOHAControls.DisplayControls
{
    public partial class ucPhaseFunctionControlDisplay : UserControl, IIntervalableFunctionControl, IRangeable, IGraphable
    {
        public event EventHandler ThresholdsChanged;

        private PhasePhaseSettings m_settings;
        private VideoFrameRange m_range;

        public VideoFrameRange Range
        {
            get
            {
                return ucPhaseFunctionControl1.Range;
            }
            set
            {
                ucPhaseFunctionControl1.Range = value;
            }
        }
        public List<double> YValues
        {
            get
            {
                return ucPhaseFunctionControl1.YValues;
            }
        }
        public IntervalCollection InitialIntervals
        {
            get
            {
                return ucPhaseFunctionControl1.InitialIntervals;
            }
            set
            {
                ucPhaseFunctionControl1.InitialIntervals = value;
            }
        }

        public ucPhaseFunctionControlDisplay()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            if(!this.DesignMode)
            {
                m_settings.SettingsChanged += new EventHandler(m_settings_SettingsChanged);

                ucPhaseFunctionControl1.MouseClickZedGraph += new MouseEventHandler(ucPhaseFunctionControl1_MouseClickZedGraph);
                //ucPhaseFunctionControl1.ThresholdsChanged += new EventHandler(ucPhaseFunctionControl1_ThresholdsChanged);

                
                chkThreshold.DataBindings.Add("Checked", m_settings, "UseThreshold", true, DataSourceUpdateMode.OnPropertyChanged);
                numOffset.DataBindings.Add("Value", m_settings, "Offset", true, DataSourceUpdateMode.OnPropertyChanged);
                //numMinInterval.DataBindings.Add("Value", m_settings, "IntensityMinInterval", true, DataSourceUpdateMode.OnPropertyChanged);


                scrollIntensity.Value = 100 - m_settings.Threshold;
                //label2.Text = m_settings.Threshold.ToString("#.##");

                numHiPass.Value = (int)(m_settings as PhasePhaseSettings).HiCutoff;
                numLoPass.Value = (int)(m_settings as PhasePhaseSettings).LoCutoff;
                numHiPass.ValueChanged += new EventHandler(numValueChanged);
                numLoPass.ValueChanged += new EventHandler(numValueChanged);

                DrawGraph();
            }

            base.OnLoad(e);
        }

        void m_settings_SettingsChanged(object sender, EventArgs e)
        {
            scrollIntensity.Value = 100 - m_settings.Threshold;
            //label2.Text = m_settings.Threshold.ToString("#.##");

            ucPhaseFunctionControl1.DrawGraph();
        }

        void ucPhaseFunctionControl1_MouseClickZedGraph(object sender, MouseEventArgs e)
        {
            GraphPane myPane = ucPhaseFunctionControl1.GraphPane;
            RectangleF plot = myPane.Chart.Rect;
            RectangleF control = myPane.Rect;

            #region Determine if cursor is over plot

            bool cursor_over_plot = false;

            if(e.X >= plot.Left && e.X <= control.Width - plot.Left)
                cursor_over_plot = true;

            if(!cursor_over_plot)
                return;

            #endregion

            #region Cursor Over Plot and Left Click, Add Threshold

            if(cursor_over_plot &&
                e.Button == System.Windows.Forms.MouseButtons.Left)
            {

                //myPane.CurveList.Remove(myPane.CurveList["ThresholdLo"]);

                //Shawn, you're an idiot. That invert is so simple.
                double invert_e_y = Math.Abs(e.Y - control.Height);

                double click_ratio_scaled = (invert_e_y - plot.Y) / plot.Height;//(((double)zedGraphControl1.Height - e.Y) / (double)zedGraphControl1.Height);
                double y_val = click_ratio_scaled * Math.Abs(myPane.YAxis.Scale.Max - myPane.YAxis.Scale.Min) + myPane.YAxis.Scale.Min;

                //Add Threshold
                m_settings.Threshold = (int)(y_val * 100.0);
                //m_thresholds[0] = y_val;

                //OnPropertyChanged("LoThreshold");
                //PointPairList y = new PointPairList();
                //for(int i = 0; i < m_fx.Values.Count; i++)
                //{
                //    y.Add(i, y_val);
                //}

                //PointPairList y = new PointPairList();
                //int x = 0;
                //foreach(int z in (m_filegram.Phases[Library.Xml.PhaseName.Preprocess].Data[0] as PreprocessPhaseData).Brightness)
                //{
                //    y.Add(x++, y_val);
                //}

                //LineItem barPDF = myPane.AddCurve("ThresholdLo", y, Color.DarkRed, SymbolType.None);

                //zedGraphControl1.Refresh();
            }

            #endregion
        }

        void numValueChanged(object sender, EventArgs e)
        {
            m_settings.HiCutoff = (double)numHiPass.Value;
            m_settings.LoCutoff = (double)numLoPass.Value;

            //Filter filt = new Filter();
            //filt.HiCutoff = (double)numHiPass.Value;
            //filt.LoCutoff = (double)numLoPass.Value;

            //ucIntensityFunctionControl1.Filter = filt;
        }

        /// <summary>
        /// Given an input function, assumes all x values are ascending positive integers.
        /// </summary>
        /// <param name="y">Input Function</param>
        public void SetXValuesFromY(List<double> y)
        {
            ucPhaseFunctionControl1.SetXValuesFromY(y);


            //Set Scroll Bars
            //scrollDarknessHigh.Minimum = scrollDarknessLow.Minimum = ucIntensityFunctionControl1.GraphPane.YAxis.Scale.Min;
            //scrollDarknessHigh.Maximum = scrollDarknessLow.Maximum = ucIntensityFunctionControl1.GraphPane.YAxis.Scale.Max;
        }
        
        public void SetSettings(ref PhasePhaseSettings settings)
        {
            //Add Data Binding?

            m_settings = settings as PhasePhaseSettings;
            ucPhaseFunctionControl1.Settings = m_settings;
        }

        public PhasePhaseSettings GetSettings()
        {
            return m_settings;
        }

        #region IIntervalableFunctionControl Members

        public List<SOHA.Library.Interval> GetIntervals()
        {
            return ucPhaseFunctionControl1.GetIntervals();
        }

        #endregion

        #region IRangeable Members

        public event EventHandler VideoFrameRangeChanged;

        public void OnVideoFrameRangeChanged(EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IGraphable Members

        public void SuspendGraph()
        {
            ucPhaseFunctionControl1.SuspendGraph();
        }

        public void ResumeGraph()
        {
            ucPhaseFunctionControl1.ResumeGraph();
        }

        public void DrawGraph()
        {
            ucPhaseFunctionControl1.DrawGraph();
        }

        #endregion

        private void numZoom_ValueChanged(object sender, EventArgs e)
        {
            int multiplier = (int)numZoom.Value;
            ucPhaseFunctionControl1.Range.ChangeRange(1000 / multiplier);
        }
    }
}
