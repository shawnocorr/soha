﻿namespace SOHAControls.DisplayControls
{
    partial class FrameDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hscrollFrame = new System.Windows.Forms.HScrollBar();
            this.tblMain = new System.Windows.Forms.TableLayoutPanel();
            this.imgFrame = new System.Windows.Forms.PictureBox();
            this.tblMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFrame)).BeginInit();
            this.SuspendLayout();
            // 
            // hscrollFrame
            // 
            this.hscrollFrame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hscrollFrame.Location = new System.Drawing.Point(0, 374);
            this.hscrollFrame.Name = "hscrollFrame";
            this.hscrollFrame.Size = new System.Drawing.Size(706, 30);
            this.hscrollFrame.TabIndex = 0;
            // 
            // tblMain
            // 
            this.tblMain.ColumnCount = 1;
            this.tblMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblMain.Controls.Add(this.hscrollFrame, 0, 1);
            this.tblMain.Controls.Add(this.imgFrame, 0, 0);
            this.tblMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblMain.Location = new System.Drawing.Point(0, 0);
            this.tblMain.Margin = new System.Windows.Forms.Padding(0);
            this.tblMain.Name = "tblMain";
            this.tblMain.RowCount = 2;
            this.tblMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tblMain.Size = new System.Drawing.Size(706, 404);
            this.tblMain.TabIndex = 2;
            // 
            // imgFrame
            // 
            this.imgFrame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgFrame.Location = new System.Drawing.Point(3, 3);
            this.imgFrame.Name = "imgFrame";
            this.imgFrame.Size = new System.Drawing.Size(700, 368);
            this.imgFrame.TabIndex = 1;
            this.imgFrame.TabStop = false;
            this.imgFrame.Paint += new System.Windows.Forms.PaintEventHandler(this.imgFrame_Paint);
            this.imgFrame.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseDown);
            this.imgFrame.MouseEnter += new System.EventHandler(this.imgFrame_MouseEnter);
            this.imgFrame.MouseLeave += new System.EventHandler(this.imgFrame_MouseLeave);
            this.imgFrame.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseMove);
            //this.imgFrame.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imgFrame_MouseUp);
            // 
            // FrameDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tblMain);
            this.Name = "FrameDisplay";
            this.Size = new System.Drawing.Size(706, 404);
            this.tblMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgFrame)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.HScrollBar hscrollFrame;
        private System.Windows.Forms.TableLayoutPanel tblMain;
        protected System.Windows.Forms.PictureBox imgFrame;
    }
}
