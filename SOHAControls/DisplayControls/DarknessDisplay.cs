﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SOHAControls.DisplayControls
{
    public partial class DarknessDisplay : Display
    {
        public DarknessDisplay()
            : base()
        {
            InitializeComponent();
        }

        public DarknessDisplay(IContainer container)
            :base(container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnMouseClick(System.Windows.Forms.MouseEventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
