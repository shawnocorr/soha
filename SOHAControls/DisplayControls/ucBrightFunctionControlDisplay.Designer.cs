﻿namespace SOHAControls.DisplayControls
{
    partial class ucBrightFunctionControlDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.scrollDarknessHigh = new System.Windows.Forms.VScrollBar();
            this.scrollDarknessLow = new System.Windows.Forms.VScrollBar();
            this.chkInvert = new System.Windows.Forms.CheckBox();
            this.chkUseDarkness = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numHiPass = new System.Windows.Forms.NumericUpDown();
            this.numLoPass = new System.Windows.Forms.NumericUpDown();
            this.ucBrightFunctionControl1 = new SOHAControls.DisplayControls.ucBrightFunctionControl();
            this.lblLowValue = new System.Windows.Forms.Label();
            this.lblHighValue = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHiPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLoPass)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ucBrightFunctionControl1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(925, 299);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.lblHighValue, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.lblLowValue, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.chkInvert, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.chkUseDarkness, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.groupBox1, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.scrollDarknessLow, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.scrollDarknessHigh, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(728, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(194, 293);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Low";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "High";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scrollDarknessHigh
            // 
            this.scrollDarknessHigh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrollDarknessHigh.Location = new System.Drawing.Point(51, 20);
            this.scrollDarknessHigh.Maximum = 111;
            this.scrollDarknessHigh.Name = "scrollDarknessHigh";
            this.tableLayoutPanel4.SetRowSpan(this.scrollDarknessHigh, 2);
            this.scrollDarknessHigh.Size = new System.Drawing.Size(17, 243);
            this.scrollDarknessHigh.TabIndex = 2;
            // 
            // scrollDarknessLow
            // 
            this.scrollDarknessLow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrollDarknessLow.Location = new System.Drawing.Point(11, 20);
            this.scrollDarknessLow.Maximum = 111;
            this.scrollDarknessLow.Name = "scrollDarknessLow";
            this.tableLayoutPanel4.SetRowSpan(this.scrollDarknessLow, 2);
            this.scrollDarknessLow.Size = new System.Drawing.Size(17, 243);
            this.scrollDarknessLow.TabIndex = 3;
            // 
            // chkInvert
            // 
            this.chkInvert.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkInvert.AutoSize = true;
            this.chkInvert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkInvert.Location = new System.Drawing.Point(83, 236);
            this.chkInvert.Name = "chkInvert";
            this.chkInvert.Size = new System.Drawing.Size(108, 24);
            this.chkInvert.TabIndex = 4;
            this.chkInvert.Text = "Invert";
            this.chkInvert.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chkInvert.UseVisualStyleBackColor = true;
            // 
            // chkUseDarkness
            // 
            this.chkUseDarkness.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUseDarkness.AutoSize = true;
            this.chkUseDarkness.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkUseDarkness.Location = new System.Drawing.Point(83, 266);
            this.chkUseDarkness.Name = "chkUseDarkness";
            this.chkUseDarkness.Size = new System.Drawing.Size(108, 24);
            this.chkUseDarkness.TabIndex = 5;
            this.chkUseDarkness.Text = "Use Darkness";
            this.chkUseDarkness.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chkUseDarkness.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(83, 3);
            this.groupBox1.Name = "groupBox1";
            this.tableLayoutPanel4.SetRowSpan(this.groupBox1, 2);
            this.groupBox1.Size = new System.Drawing.Size(108, 227);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.numHiPass, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.numLoPass, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(102, 208);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "HighPass";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 26);
            this.label4.TabIndex = 1;
            this.label4.Text = "LowPass";
            // 
            // numHiPass
            // 
            this.numHiPass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numHiPass.Location = new System.Drawing.Point(54, 42);
            this.numHiPass.Name = "numHiPass";
            this.numHiPass.Size = new System.Drawing.Size(45, 20);
            this.numHiPass.TabIndex = 2;
            // 
            // numLoPass
            // 
            this.numLoPass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numLoPass.Location = new System.Drawing.Point(54, 146);
            this.numLoPass.Name = "numLoPass";
            this.numLoPass.Size = new System.Drawing.Size(45, 20);
            this.numLoPass.TabIndex = 3;
            // 
            // ucBrightFunctionControl1
            // 
            this.ucBrightFunctionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucBrightFunctionControl1.Function = null;
            this.ucBrightFunctionControl1.IsAlwaysNormalize = true;
            this.ucBrightFunctionControl1.Location = new System.Drawing.Point(3, 3);
            this.ucBrightFunctionControl1.Name = "ucBrightFunctionControl1";
            this.ucBrightFunctionControl1.Range = null;
            this.ucBrightFunctionControl1.RightClickThresholdEnable = true;
            this.ucBrightFunctionControl1.Size = new System.Drawing.Size(719, 293);
            this.ucBrightFunctionControl1.TabIndex = 0;
            // 
            // lblLowValue
            // 
            this.lblLowValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblLowValue.AutoSize = true;
            this.lblLowValue.Location = new System.Drawing.Point(6, 271);
            this.lblLowValue.Name = "lblLowValue";
            this.lblLowValue.Size = new System.Drawing.Size(28, 13);
            this.lblLowValue.TabIndex = 7;
            this.lblLowValue.Text = "XXX";
            this.lblLowValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHighValue
            // 
            this.lblHighValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHighValue.AutoSize = true;
            this.lblHighValue.Location = new System.Drawing.Point(46, 271);
            this.lblHighValue.Name = "lblHighValue";
            this.lblHighValue.Size = new System.Drawing.Size(28, 13);
            this.lblHighValue.TabIndex = 8;
            this.lblHighValue.Text = "XXX";
            this.lblHighValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ucBrightFunctionControlDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ucBrightFunctionControlDisplay";
            this.Size = new System.Drawing.Size(925, 299);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHiPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLoPass)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ucBrightFunctionControl ucBrightFunctionControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.VScrollBar scrollDarknessHigh;
        private System.Windows.Forms.VScrollBar scrollDarknessLow;
        private System.Windows.Forms.CheckBox chkInvert;
        private System.Windows.Forms.CheckBox chkUseDarkness;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numHiPass;
        private System.Windows.Forms.NumericUpDown numLoPass;
        private System.Windows.Forms.Label lblHighValue;
        private System.Windows.Forms.Label lblLowValue;
    }
}
