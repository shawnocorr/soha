﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library;

namespace SOHAControls.DisplayControls
{
    public interface IIntervalableFunctionControl
    {

        List<Interval> GetIntervals();
    }
}
