﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using SOHA.Library.Mathematics;

namespace SOHAControls.DisplayControls
{
    public class GraphEventArgs : PaintEventArgs
    {

        public Filter Filter { get; protected set; }
        public List<double> Y { get; protected set; }
        public List<double> X { get; protected set; }        

        public GraphEventArgs(List<double> y, List<double> t, Filter f, Graphics g, Rectangle r)
            : base(g, r)
        {
            Filter = f;
            X = t;
            Y = y;
        }

    }
}
