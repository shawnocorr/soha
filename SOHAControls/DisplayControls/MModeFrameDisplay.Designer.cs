﻿namespace SOHAControls.DisplayControls
{
    partial class MModeFrameDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SOHAControls.IntervalProperties intervalProperties1 = new SOHAControls.IntervalProperties();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MModeFrameDisplay));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mModeDisplay1 = new Forms.MModeDisplay(this.components);
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.mModeThumbnail1 = new SOHAControls.MModeThumbnail(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.ucMmodeSelection1 = new SOHAControls.DisplayControls.ucMmodeSelection();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.mModeDisplay1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ucMmodeSelection1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1479, 304);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // mModeDisplay1
            // 
            this.mModeDisplay1.DisplayCoordinates = false;
            this.mModeDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            intervalProperties1.Interval = 1;
            intervalProperties1.IntervalSize = 1000;
            this.mModeDisplay1.Interval = intervalProperties1;
            this.mModeDisplay1.List = null;
            this.mModeDisplay1.Location = new System.Drawing.Point(403, 3);
            this.mModeDisplay1.MMode = null;
            this.mModeDisplay1.Name = "mModeDisplay1";
            this.mModeDisplay1.Phase = SOHA.Library.PhaseFormType.New;
            this.mModeDisplay1.PhaseOffset = 0;
            this.tableLayoutPanel1.SetRowSpan(this.mModeDisplay1, 2);
            this.mModeDisplay1.ShowChangeIntervals = false;
            this.mModeDisplay1.Size = new System.Drawing.Size(1073, 278);
            this.mModeDisplay1.TabIndex = 0;
            this.mModeDisplay1.TabStop = false;
            this.mModeDisplay1.Zoom = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.mModeThumbnail1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(400, 134);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // mModeThumbnail1
            // 
            this.mModeThumbnail1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mModeThumbnail1.BackgroundImage")));
            this.mModeThumbnail1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mModeThumbnail1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mModeThumbnail1.Location = new System.Drawing.Point(3, 3);
            this.mModeThumbnail1.Name = "mModeThumbnail1";
            this.mModeThumbnail1.Size = new System.Drawing.Size(394, 108);
            this.mModeThumbnail1.TabIndex = 2;
            this.mModeThumbnail1.TabStop = false;
            this.mModeThumbnail1.Thumbnail = ((System.Drawing.Bitmap)(resources.GetObject("mModeThumbnail1.Thumbnail")));
            this.mModeThumbnail1.XCoord = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // ucMmodeSelection1
            // 
            this.ucMmodeSelection1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucMmodeSelection1.Location = new System.Drawing.Point(3, 137);
            this.ucMmodeSelection1.Name = "ucMmodeSelection1";
            this.ucMmodeSelection1.Size = new System.Drawing.Size(394, 144);
            this.ucMmodeSelection1.TabIndex = 2;
            this.ucMmodeSelection1.VideoFile = null;
            this.ucMmodeSelection1.CreateMmode += new System.EventHandler<SOHAControls.DisplayControls.CreateMmodeEventArgs>(this.ucMmodeSelection1_CreateMmode);
            // 
            // MModeFrameDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MModeFrameDisplay";
            this.Size = new System.Drawing.Size(1479, 304);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.mModeDisplay1_Paint);
            this.Resize += new System.EventHandler(this.MModeFrameDisplay_Resize);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Forms.MModeDisplay mModeDisplay1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MModeThumbnail mModeThumbnail1;
        private System.Windows.Forms.Label label1;
        private ucMmodeSelection ucMmodeSelection1;


    }
}
