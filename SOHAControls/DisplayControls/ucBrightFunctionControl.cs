﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library;
using ZedGraph;
using SOHA.Library.Xml;

namespace SOHAControls.DisplayControls
{
    public partial class ucBrightFunctionControl : ucFunctionControl, IIntervalableFunctionControl
    {

        //protected bool m_use_darkness;

        //[Bindable(true)]
        //public double HiThreshold
        //{
        //    get
        //    {
        //        return m_thresholds[1];
        //    }
        //    set
        //    {
        //        m_thresholds[1] = value;
        //        OnPropertyChanged("HiThreshold");
        //        OnThresholdsChanged(new EventArgs());
        //        base.DrawGraph();
        //    }
        //}
        //[Bindable(true)]
        //public double LoThreshold
        //{
        //    get
        //    {
        //        return m_thresholds[0];
        //    }
        //    set
        //    {
        //        m_thresholds[0] = value;
        //        OnPropertyChanged("LoThreshold");
        //        OnThresholdsChanged(new EventArgs());
        //        base.DrawGraph();
        //    }
        //}
        //[Bindable(true)]
        //public bool Invert
        //{
        //    get
        //    {
        //        return m_invert;
        //    }
        //    set
        //    {
        //        m_invert = value;
        //        OnPropertyChanged("Invert");
        //        OnThresholdsChanged(new EventArgs());
        //        base.DrawGraph();
        //    }
        //}
        //[Bindable(true)]
        //public bool UseDarkness
        //{
        //    get
        //    {
        //        return m_use_darkness;
        //    }
        //    set
        //    {
        //        m_use_darkness = value;
        //        OnPropertyChanged("UseDarkness");
        //        OnThresholdsChanged(new EventArgs());
        //        base.DrawGraph();
        //    }
        //}

        public ucBrightFunctionControl()
            :base()
        {
            InitializeComponent();

            //m_invert = false;
            //m_use_darkness = false;
        }

        public List<Interval> GetIntervals()
        {
            if(!m_loaded || m_graphed_values == null) return null;

            List<Interval> ret = new List<Interval>();

            //double[] tmpdata = new double[YValues.Count];
            //YValues.CopyTo(tmpdata);
            //List<double> temp = tmpdata.ToList();

            //Filter m_filter = new Filter(

            //PreFilter(ref temp);
            //double[] tmp = temp.ToArray();

            List<double> temp = m_graphed_values;
            double max = m_graphed_values.Max();
            double min = m_graphed_values.Min();
            double spread = max - min;

            //return ArrayFunctions.MaxMinList(temp);

            double threshold_lo = (m_settings as IntervalPhaseSettings).BrightThresholdLo / 100.0 * spread + min;// m_thresholds[0]; //(LoThreshold) / 100.0;
            double threshold_hi = (m_settings as IntervalPhaseSettings).BrightThresholdHi / 100.0 * spread + min; //m_thresholds[1]; //(HiThreshold) / 100.0;


            for(int i = 1; i < temp.Count; i++)
            {
                IntervalType t = IntervalType.Period;
                if((temp[i - 1] < threshold_lo) && (temp[i] > threshold_lo))
                {
                    t = IntervalType.Systole;
                }
                else if((temp[i - 1] > threshold_lo) && (temp[i] < threshold_lo))
                {
                    t = IntervalType.Diastole;
                }
                else if((temp[i - 1] < threshold_hi) && (temp[i] > threshold_hi))
                {
                    t = IntervalType.Diastole;
                }
                else if((temp[i - 1] > threshold_hi) && (temp[i] < threshold_hi))
                {
                    t = IntervalType.Systole;
                }

                if(t != IntervalType.Period)
                    ret.Add(new Interval(i, t));
            }

            return ret;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        public override void DrawGraph()
        {
            if(!m_loaded || this.DesignMode || m_fx == null || m_suspend_graph)
                return;

            if(zedGraphControl1.GraphPane == null)
                return;

            GraphPane myPane = zedGraphControl1.GraphPane;
            CurveItem l_function = myPane.CurveList["Function"];
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            myPane.Title.Text = "Brightness";
            myPane.XAxis.Title.Text = "Frame";
            myPane.YAxis.Title.Text = "Value";

            //Set data to initial function value
            double[] data = m_fx.Values.ToArray();

            #region Filter, if necessary

            bool perform_filter = false;
            Filter m_filter = new Filter();
            m_filter.HiCutoff = (m_settings as IntervalPhaseSettings).BrightHiCutoff;
            m_filter.LoCutoff = (m_settings as IntervalPhaseSettings).BrightLoCutoff;
            m_filter.Fps = 160;

            if(m_filter != null)
            {
                int N = 3;

                double wc_high = m_filter.HiCutoff / m_filter.Fps;
                double wc_low = m_filter.LoCutoff / m_filter.Fps;

                if(wc_high > 0 && wc_high < 1 && wc_low > 0 && wc_low < 1 && wc_high > wc_low)
                {
                    //BandPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_low, wc_high }, FilterType.Bandpass);
                    perform_filter = true;
                }
                else if(wc_high > 0 && wc_high < 1)
                {
                    //HighPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_high }, FilterType.Low);
                    perform_filter = true;
                }
                else if(wc_low > 0 && wc_low < 1)
                {
                    //LoPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_low }, FilterType.High);
                    perform_filter = true;
                }

                if(perform_filter)
                    data = m_filter.DualInputZeroPhaseFilter(data);
            }

            m_graphed_values = data.ToList();
            //if (m_lo_pass_frequency > 0)
            //{
            //    Filter filter = IIRButterworth.Butter(4, new double[] { m_lo_pass_frequency }, FilterType.Low);
            //    data = filter.DualInputZeroPhaseFilter(data);
            //}

            //double max = data.Max();
            //double min = data.Min();

            //if(m_stats.Set)
            //{
            //    func = ArrayFunctions.Normalize(data.ToList(), m_stats.Max, m_stats.Min);
            //}
            //else
            //{
            //    func = ArrayFunctions.Normalize(data.ToList(), out max, out min);
            //    //List<double> temp = ArrayFunctions.MaxMinList(data.ToList());
            //}

            //m_stats.Max = max;
            //m_stats.Min = min;

            //m_last_data = func;

            #endregion

            List<double> filtered_data = data.ToList();

            if(IsAlwaysNormalize)
            {
                filtered_data = ArrayFunctions.Normalize(filtered_data);
                zedGraphControl1.GraphPane.YAxis.Scale.Min = 0;
                zedGraphControl1.GraphPane.YAxis.Scale.Max = 1.0;
            }

            //Add Inversion
            //Needs to look at m_settings in ucBrightFunctionControlDisplay
            if(m_invert || (m_settings as IntervalPhaseSettings).Invert)
            {
                //zedGraphControl1.GraphPane.YAxis.Scale.IsReverse = false;
                ArrayFunctions.Invert(ref filtered_data);
            }
            else
            {

            }

            //Get function
            PointPairList y = new PointPairList();
            for(int i = m_range.StartFrame; i < m_range.EndFrame && i < filtered_data.Count; i++)
            {
                y.Add(i, filtered_data[i]);
            }

            LineItem barPDF = myPane.AddCurve("Function", y, Color.Aqua, SymbolType.None);

            DrawThresholds();

            zedGraphControl1.GraphPane.XAxis.Scale.Min = m_range.StartFrame;
            zedGraphControl1.GraphPane.XAxis.Scale.Max = m_range.StartFrame + m_range.Range;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public override void DrawThresholds()
        {
            if(!m_loaded) return;

            GraphPane myPane = zedGraphControl1.GraphPane;
            RectangleF plot = myPane.Chart.Rect;
            RectangleF control = myPane.Rect;

            myPane.CurveList.Remove(myPane.CurveList["ThresholdLo"]);
            myPane.CurveList.Remove(myPane.CurveList["ThresholdHi"]);

            double y0 = (m_settings as IntervalPhaseSettings).BrightThresholdLo;
            if(y0 != null && y0 != 0)
            {

                if(IsAlwaysNormalize)
                {
                    //If > 1, treat as a percent
                    if(y0 > 1) y0 /= 100;
                }

                PointPairList y = new PointPairList();
                for(int i = 0; i < m_fx.Values.Count; i++)
                {
                    y.Add(i, y0);
                }

                LineItem barPDF = myPane.AddCurve("ThresholdLo", y, Color.Green, SymbolType.None);
            }

            double y1 = (m_settings as IntervalPhaseSettings).BrightThresholdHi;
            if(y1 != null && y1 != 0 && RightClickThresholdEnable)
            {

                if(IsAlwaysNormalize)
                {
                    //If > 1, treat as a percent
                    if(y1 > 1) y1 /= 100;
                }

                PointPairList y = new PointPairList();
                for(int i = 0; i < m_fx.Values.Count; i++)
                {
                    y.Add(i, y1);
                }

                LineItem barPDF = myPane.AddCurve("ThresholdHi", y, Color.Red, SymbolType.None);
            }
            zedGraphControl1.Refresh();
        }
    }
}
