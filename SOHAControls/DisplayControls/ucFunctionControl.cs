﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using SOHA.Library.Mathematics;
using SOHA.Library.Xml;

namespace SOHAControls.DisplayControls
{
    public partial class ucFunctionControl : UserControl, INotifyPropertyChanged, IGraphable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        //public event EventHandler IntervalsChanged;
        public event EventHandler ThresholdsChanged;
        public event EventHandler VideoFrameRangeChanged;
        public event MouseEventHandler MouseClickZedGraph;

        //private int x_width = 500;
        protected bool m_cursor_over_plot = false;
        protected bool m_suspend_graph = false;
        protected bool m_loaded = false;
        protected bool m_invert;
        protected Filter m_filter = new Filter();
        protected PhaseSettings m_settings;

        protected Dictionary<double, double> m_fx;
        protected VideoFrameRange m_range;
        protected List<double> m_graphed_values;

        protected List<double> m_thresholds = new List<double>();

        public Dictionary<double, double> Function
        {
            get
            {
                return m_fx;
            }
            set
            {
                if(value == null)
                    return;

                m_fx = value;
                OnFunctionChanged();
            }
        }
        public VideoFrameRange Range
        {
            get
            {
                return m_range;
            }
            set
            {
                if(value == null)
                    return;

                if(m_range != null)
                    m_range.RangeChanged -= new EventHandler<VideoFrameRangeEventArgs>(m_range_RangeChanged);

                m_range = value;
                m_range.RangeChanged += new EventHandler<VideoFrameRangeEventArgs>(m_range_RangeChanged);
                DrawGraph();
            }
        }
        //public List<double> Thresholds

        //Read-Only Properties
        public List<double> YValues
        {
            get
            {
                return m_graphed_values;
            }
        }
        public RectangleF PlotRectangle
        {
            get
            {
                return this.zedGraphControl1.GraphPane.Chart.Rect;
            }
        }
        public GraphPane GraphPane
        {
            get
            {
                return zedGraphControl1.GraphPane;
            }
        }
        public PhaseSettings Settings
        {
            get
            {
                return m_settings;
            }
            set
            {
                m_settings = value;
            }
        }

        [Browsable(true)]
        public bool RightClickThresholdEnable
        {
            get;
            set;
        }
        [Browsable(true)]
        public bool IsAlwaysNormalize
        {
            get;
            set;
        }

        public ucFunctionControl()
        {
            InitializeComponent();
            m_thresholds = new List<double>(new double[] { 0, 0 });

            zedGraphControl1.MouseEnter += new EventHandler(zedGraphControl1_MouseEnter);
            zedGraphControl1.MouseLeave += new EventHandler(zedGraphControl1_MouseLeave);
            zedGraphControl1.MouseClick += new MouseEventHandler(zedGraphControl1_MouseClick);

        }

        void m_settings_SettingsChanged(object sender, EventArgs e)
        {
            DrawGraph();
        }

        void zedGraphControl1_MouseLeave(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Default;
        }

        void zedGraphControl1_MouseEnter(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Cross;
        }

        protected virtual void OnFunctionChanged()
        {
            DrawGraph();
        }

        public virtual void DrawGraph()
        {
            throw new NotImplementedException();
        }

        public virtual void DrawThresholds()
        {
            throw new NotImplementedException();            
        }

        public void SuspendGraph() { m_suspend_graph = true; }
        public void ResumeGraph() { m_suspend_graph = false; }

        private void GraphFunction(VideoFrameRange range)
        {
            GraphPane myPane = zedGraphControl1.GraphPane;
            CurveItem l_function = myPane.CurveList["Function"];
            myPane.CurveList.Remove(l_function); 
            //myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            myPane.Title.Text = "Function";
            myPane.XAxis.Title.Text = "Frame";
            myPane.YAxis.Title.Text = "Value";

            PointPairList y = new PointPairList();
            for(int i = range.StartFrame; i < range.EndFrame && i < m_fx.Count; i++)
            {
                y.Add(i, m_fx[i]);
            }

            LineItem barPDF = myPane.AddCurve("Function", y, Color.Aqua, SymbolType.None);

            zedGraphControl1.GraphPane.XAxis.Scale.Min = range.StartFrame;
            zedGraphControl1.GraphPane.XAxis.Scale.Max = range.StartFrame + range.Range; 
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        /// <summary>
        /// Given an input function, assumes all x values are ascending positive integers.
        /// </summary>
        /// <param name="y">Input Function</param>
        public void SetXValuesFromY(List<double> y)
        {
            m_fx = new Dictionary<double, double>();
            for(int i = 0; i < y.Count; i++)
            {
                m_fx.Add(i, y[i]);
            }
            DrawGraph();
            //this.Invalidate();
        }

        void m_range_RangeChanged(object sender, VideoFrameRangeEventArgs e)
        {
            m_range = e.Range;
            DrawGraph();
            //GraphFunction(e.Range);
        }

        //Don't think this is necessary
        private void zedGraphControl1_CursorChanged(object sender, EventArgs e)
        {
            m_cursor_over_plot = Cursor.Current == Cursors.Cross;
        }

        protected void zedGraphControl1_MouseClick(object sender, MouseEventArgs e)
        {
            OnMouseClickZedGraph(sender, e);
            

            //OnThresholdsChanged(e);

        }

        protected void OnMouseClickZedGraph(object sender, MouseEventArgs e)
        {
            if(MouseClickZedGraph != null)
                MouseClickZedGraph(sender, e);
        }

        protected void OnPropertyChanged(string name)
        {
            if(!m_loaded) return;

            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        protected void OnThresholdsChanged(EventArgs e)
        {
            if(!m_loaded) return;

            DrawThresholds();

            if(ThresholdsChanged != null)
                ThresholdsChanged(this, new EventArgs());
        }

        protected void OnVideoFrameRangeChanged(EventArgs e)
        {
            if(!m_loaded) return;

            if(VideoFrameRangeChanged != null)
                VideoFrameRangeChanged(this, e);
        }

        protected override void OnLoad(EventArgs e)
        {
            if(this.DesignMode)
                return;

            m_loaded = true;

            //m_settings.SettingsChanged += new EventHandler(m_settings_SettingsChanged);
            //m_thresholds[0] = m_settings.BrightThresholdLo;
            //m_thresholds[1] = m_settings.BrightThresholdHi;

            DrawGraph();

            base.OnLoad(e);
        }


        #region IGraphable Members


        public void SetSettings(ref PhaseSettings settings)
        {
            throw new NotImplementedException();
        }

        public PhaseSettings GetSettings()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
