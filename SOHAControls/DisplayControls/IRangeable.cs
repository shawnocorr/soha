﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHAControls.DisplayControls
{
    public interface IRangeable
    {
        event EventHandler VideoFrameRangeChanged;

        void OnVideoFrameRangeChanged(EventArgs e);
    }
}
