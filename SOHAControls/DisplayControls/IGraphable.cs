﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Xml;

namespace SOHAControls.DisplayControls
{
    public interface IGraphable
    {
        void SuspendGraph();
        void ResumeGraph();
        void DrawGraph();

        //void SetSettings(ref PhaseSettings settings);
        //PhaseSettings GetSettings();
    }
}
