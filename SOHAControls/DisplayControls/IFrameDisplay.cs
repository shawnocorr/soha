﻿using System;
using SOHA.Library.Video;
using System.Drawing;
using SOHA.Library;
using System.Diagnostics;
using SOHA.Library.Xml;
namespace SOHAControls.DisplayControls
{
    interface IFrameDisplay
    {
        Point Coordinates { get; set; }
        event EventHandler CoordinatesChanged;
        Frame CurrentFrame { get; }
        event EventHandler DataChanged;
        bool EnableFilters { get; set; }
        System.Collections.Generic.List<SOHA.Library.Imaging.IFrameFunction> Filters { get; set; }
        event EventHandler FiltersChanged;
        Pen HighlightPen { get; set; }
        event EventHandler HistogramUpdated;
        void KeyPress(System.Windows.Forms.KeyEventArgs key_event);
        Stopwatch LastFilterWatch { get; }
        SohaCollection<SOHA.Library.MarkPair> Marks { get; set; }
        bool MmodeDisplay { get; set; }
        System.Drawing.Pen NormalPen { get; set; }
        Frame PostOpFrame { get; }
        SohaCollection<SOHA.Library.Roi> ROI { get; set; }
        bool UserInteractive { get; set; }
        event EventHandler VideoChanged;
        Video VideoFile { get; set; }


        event EventHandler<FrameEventArgs> FrameChanged;
    }
}
