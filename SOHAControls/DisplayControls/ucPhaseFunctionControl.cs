﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using ZedGraph;
using SOHA.Library.Mathematics;
using SOHA.Library.Xml;

namespace SOHAControls.DisplayControls
{
    public partial class ucPhaseFunctionControl : ucFunctionControl, IIntervalableFunctionControl
    {

        private PhasePhaseSettings Settings
        {
            get
            {
                return base.Settings as PhasePhaseSettings;
            }
        }

        protected IntervalCollection m_initial_intervals;


        public IntervalCollection InitialIntervals
        {
            get
            {
                return m_initial_intervals;
            }
            set
            {
                m_initial_intervals = value;
            }
        }

        public ucPhaseFunctionControl()
            :base()
        {
            InitializeComponent();
        }

        public List<Interval> GetIntervals()
        {
            if(!m_loaded || m_graphed_values == null) return null;

            IntervalCollection changes = new IntervalCollection();
            IntervalCollection m_intervallist = m_initial_intervals;

            if(!Settings.UseThreshold)
            {
                int start = m_intervallist.IndexOf(m_intervallist.First(x => x.IntervalType == IntervalType.Systole));

                // Start at 1; skip first interval; ensure accuracy
                // 2/15/2015 - subtract 1 from Count for straggling systolic start with no diastolic end
                for(int i = start; i < m_intervallist.Count - 1; i++)
                {
                    if(m_intervallist[i].IntervalType == IntervalType.Systole)
                    {
                        double lo = double.MaxValue; double hi = double.MinValue; int index_lo = 0; int index_hi = 0;
                        for(int j = m_intervallist[i].Frame + Settings.Offset; j <= m_intervallist[i + 1].Frame - Settings.Offset; j++)
                        {
                            //Find Minimum
                            if(m_graphed_values[j] < lo)
                            {
                                lo = m_graphed_values[j];
                                index_lo = j + 1;
                            }
                        }

                        for(int j = m_intervallist[i].Frame; j <= m_intervallist[i + 1].Frame; j++)
                        {
                            //Find First Maximum
                            if(m_graphed_values[j] > hi)
                            {
                                hi = m_graphed_values[j];
                                index_hi = j;
                            }
                        }

                        if(index_lo != 0)
                        {
                            changes.Add(new Interval(index_lo/* / m_zoom*/, IntervalType.Change));
                            changes.Add(new Interval(index_hi/* / m_zoom*/, IntervalType.MaxVelocity));
                        }
                    }
                }

                //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, this.Zoom);
            }
            else
            {
                //Isometric

                //List<double> data = InitialData;
                //PreFilter(ref data);
                //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, Zoom);
                double max_value = m_graphed_values.Max();
                double min = m_graphed_values.Min();
                double spread = max_value - min;
                double threshold = Settings.Threshold / 100.0 * spread + min;;
                int offset = Settings.Offset;

                int total_beats = (from interval in m_intervallist
                                   where interval.IntervalType == IntervalType.Systole
                                   select interval).Count();

                //Take First 5 later in algorithm

                IEnumerable<Interval> systoles = (from interval in m_intervallist
                                where interval.IntervalType == IntervalType.Systole
                                select interval);

                //if(total_beats >= 5)
                //{
                //    systoles = (from interval in m_intervallist
                //                where interval.IntervalType == IntervalType.Systole
                //                select interval).Take(5);
                //}
                //else
                //{
                //    systoles = (from interval in m_intervallist
                //                where interval.IntervalType == IntervalType.Systole
                //                select interval);
                //}

                foreach(Interval systole in systoles)
                {
                    var diastoles = (from dias in m_intervallist
                                     where dias.IntervalType == IntervalType.Diastole && dias.Frame > systole.Frame
                                     select dias);

                    if(diastoles == null || diastoles.Count() == 0)
                    {
                        continue;
                    }

                    Interval diastole = diastoles.First();

                    int max_index = systole.Frame + offset;

                    //if (max_index > data.Count)
                    //    max_index = data.Count - 1;

                    double max = m_graphed_values[max_index];
                    for(int i = systole.Frame + offset; i <= diastole.Frame - offset; i++)
                    {
                        double left = m_graphed_values[i - 1];
                        double right = m_graphed_values[i];
                        if((left < threshold && right > threshold) ||
                            (left > threshold && right < threshold))
                        {
                            //Interval new_interval = new SOHA.Library.Interval(
                            changes.Add(new Interval(i /*/ m_zoom*/, IntervalType.Change));
                        }

                        if(right > max)
                        {
                            max_index = i;
                            max = m_graphed_values[max_index];
                        }
                    }
                    changes.Add(new Interval(max_index /*/ m_zoom*/, IntervalType.MaxVelocity));
                }

                //changes = IntervalCollection.XExtrapolate(new IntervalCollection(changes), Zoom).ToList();
            }

            return changes.ToList();
        }

        public override void DrawGraph()
        {
            if(!m_loaded || this.DesignMode || m_fx == null || m_suspend_graph)
                return;

            if(zedGraphControl1.GraphPane == null)
                return;

            GraphPane myPane = zedGraphControl1.GraphPane;
            CurveItem l_function = myPane.CurveList["Function"];
            myPane.CurveList.Clear();
            //myPane.CurveList.Remove(l_function);
            //myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            myPane.Title.Text = "Intensity";
            myPane.XAxis.Title.Text = "Frame";
            myPane.YAxis.Title.Text = "Value";

            //Set data to initial function value
            double[] data = m_fx.Values.ToArray();

            #region Filter, if necessary

            bool perform_filter = false;
            Filter m_filter = new Filter();
            m_filter.HiCutoff = (m_settings as PhasePhaseSettings).HiCutoff;
            m_filter.LoCutoff = (m_settings as PhasePhaseSettings).LoCutoff;
            m_filter.Fps = 160;

            if(m_filter != null)
            {
                int N = 3;

                double wc_high = m_filter.HiCutoff / m_filter.Fps;
                double wc_low = m_filter.LoCutoff / m_filter.Fps;

                if(wc_high > 0 && wc_high < 1 && wc_low > 0 && wc_low < 1 && wc_high > wc_low)
                {
                    //BandPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_low, wc_high }, FilterType.Bandpass);
                    perform_filter = true;
                }
                else if(wc_high > 0 && wc_high < 1)
                {
                    //HighPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_high }, FilterType.Low);
                    perform_filter = true;
                }
                else if(wc_low > 0 && wc_low < 1)
                {
                    //LoPass
                    m_filter = IIRButterworth.Butter(N, new double[] { wc_low }, FilterType.High);
                    perform_filter = true;
                }

                if(perform_filter)
                    data = m_filter.DualInputZeroPhaseFilter(data);
            }

            m_graphed_values = data.ToList();
            //if (m_lo_pass_frequency > 0)
            //{
            //    Filter filter = IIRButterworth.Butter(4, new double[] { m_lo_pass_frequency }, FilterType.Low);
            //    data = filter.DualInputZeroPhaseFilter(data);
            //}

            //double max = data.Max();
            //double min = data.Min();

            //if(m_stats.Set)
            //{
            //    func = ArrayFunctions.Normalize(data.ToList(), m_stats.Max, m_stats.Min);
            //}
            //else
            //{
            //    func = ArrayFunctions.Normalize(data.ToList(), out max, out min);
            //    //List<double> temp = ArrayFunctions.MaxMinList(data.ToList());
            //}

            //m_stats.Max = max;
            //m_stats.Min = min;

            //m_last_data = func;

            #endregion

            List<double> filtered_data = data.ToList();

            if(IsAlwaysNormalize)
            {
                filtered_data = ArrayFunctions.Normalize(filtered_data);
                zedGraphControl1.GraphPane.YAxis.Scale.Min = 0;
                zedGraphControl1.GraphPane.YAxis.Scale.Max = 1.0;
            }

            //Get function
            //m_graphed_values = filtered_data;
            PointPairList y = new PointPairList();
            for(int i = m_range.StartFrame; i < m_range.EndFrame && i < filtered_data.Count; i++)
            {
                y.Add(i, filtered_data[i]);
            }

            LineItem barPDF = myPane.AddCurve("Function", y, Color.Aqua, SymbolType.None);

            DrawThresholds();

            zedGraphControl1.GraphPane.XAxis.Scale.Min = m_range.StartFrame;
            zedGraphControl1.GraphPane.XAxis.Scale.Max = m_range.StartFrame + m_range.Range;
            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();
        }

        public override void DrawThresholds()
        {
            if(!m_loaded) return;

            GraphPane myPane = zedGraphControl1.GraphPane;
            RectangleF plot = myPane.Chart.Rect;
            RectangleF control = myPane.Rect;

            myPane.CurveList.Remove(myPane.CurveList["ThresholdLo"]);
            myPane.CurveList.Remove(myPane.CurveList["ThresholdHi"]);

            double y0 = (m_settings as PhasePhaseSettings).Threshold;
            if(y0 != null && y0 != 0)
            {

                if(IsAlwaysNormalize)
                {
                    //If > 1, treat as a percent
                    if(y0 > 1) y0 /= 100;
                }

                PointPairList y = new PointPairList();
                for(int i = 0; i < m_fx.Values.Count; i++)
                {
                    y.Add(i, y0);
                }

                LineItem barPDF = myPane.AddCurve("ThresholdLo", y, Color.Green, SymbolType.None);
            }

            zedGraphControl1.Refresh();
        }
    }
}
