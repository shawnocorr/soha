﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHAControls.DisplayControls
{

    public enum CreateMmodeType
    {
        ByTime,
        ByFrame
    }

    public class CreateMmodeEventArgs : EventArgs
    {
        public bool FullVideo { get; set; }
        public CreateMmodeType MmodeType { get; set; }
        public int Start { get; set; }
        public int End { get; set; }

        public CreateMmodeEventArgs() : base() { }
        public CreateMmodeEventArgs(bool isFullVideo, CreateMmodeType type, int start, int end)
            : base()
        {
            FullVideo = isFullVideo;
            MmodeType = type;
            Start = start;
            End = end;
        }
    }

    [Serializable]
    public class CreateMmodeException : Exception
    {
        public CreateMmodeException() { }
        public CreateMmodeException(string message) : base(message) { }
        public CreateMmodeException(string message, Exception inner) : base(message, inner) { }
        protected CreateMmodeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
