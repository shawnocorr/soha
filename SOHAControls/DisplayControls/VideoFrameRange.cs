﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Video;

namespace SOHAControls.DisplayControls
{
    public class VideoFrameRange
    {
        public event EventHandler<VideoFrameRangeEventArgs> RangeChanged;

        private int m_current_range = 1;
        private int m_total_frames = 4000;
        private int m_range = 1000;

        public int StartFrame
        {
            get
            {
                return (m_current_range - 1) * m_range + 1;
            }
        }
        public int EndFrame
        {
            get
            {
                return (m_current_range * m_range < m_total_frames ? m_current_range * m_range : m_total_frames);
            }
        }
        public int Range
        {
            get
            {
                return m_range;
            }
        }
                
        public VideoFrameRange() { }

        public VideoFrameRange(Video video)
        {
            m_total_frames = video.Frames;
        }

        public VideoFrameRange(Video video, int range)
        {
            m_total_frames = video.Frames;
            m_range = range;
        }

        public void NextRange()
        {
            if(m_current_range >= (int)Math.Ceiling(m_total_frames / (decimal)m_range))
                return;

            m_current_range++;

            OnRangeChanged(new VideoFrameRangeEventArgs(this));
        }

        public void PreviousRange()
        {
            if(m_current_range == 1)
                return;

            m_current_range--;

            OnRangeChanged(new VideoFrameRangeEventArgs(this));
        }

        public void ChangeRange(int newrange)
        {
            m_range = newrange;
            OnRangeChanged(new VideoFrameRangeEventArgs(this));
        }

        protected void OnRangeChanged(VideoFrameRangeEventArgs e)
        {
            if(RangeChanged != null)
                RangeChanged(this, e);
        }
    }

    public class VideoFrameRangeEventArgs : EventArgs
    {
        private VideoFrameRange m_range;
        public VideoFrameRange Range
        {
            get
            {
                return m_range;
            }
        }

        public VideoFrameRangeEventArgs(VideoFrameRange range)
        {
            m_range = range;
        }
    }
}
