﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Video;
using SOHA.Library.Xml;
using System.Diagnostics;
using SOHA.Library.Imaging;

namespace SOHAControls.DisplayControls
{
    public partial class MModeFrameDisplay : UserControl, IFrameDisplay
    {
        public event EventHandler<CreateMmodeEventArgs> CreateMMode;
        public event EventHandler<FrameEventArgs> FrameChanged;

        private string m_video_info;

        private Video m_video;
        private MMode m_mmode;
        private Frame m_post_function_frame;
        private Bitmap m_current_bmp;
        private Stopwatch swFilters = new Stopwatch();
        private List<IFrameFunction> m_filters = new List<IFrameFunction>();

        private int m_xcoord;
        public int XCoord
        {
            get
            {
                return m_xcoord;
            }
            set
            {
                m_xcoord = value;
                OnSetXCoordinate();
            }
        }

        private void OnSetXCoordinate()
        {
            if(m_video == null)
                return;
                

            //Get New MMode
            mModeDisplay1.MMode = m_video.GetMMode(m_xcoord);
            m_current_bmp = CurrentFrame.ToBitmap();
            label1.Text = m_video_info + " { x: " + m_xcoord + " }";
            //mModeDisplay1.Refresh();

            //throw new NotImplementedException();
        }

        public MModeFrameDisplay()
        {
            InitializeComponent();
            Initialize();
        }

        private void Initialize()
        {
            mModeDisplay1.Paint += new PaintEventHandler(mModeDisplay1_Paint);
            mModeThumbnail1.XCoordChanged += new MouseEventHandler(mModeThumbnail1_XCoordChanged);
            //m_xcoord = 1;
            //m_mmode = (MMode)Frame.BlankFrame(imgFrame.Width, imgFrame.Height);
            //EnableFilters = false;
        }

        private void scrollXCoord_Scroll(object sender, ScrollEventArgs e)
        {
            XCoord = e.NewValue;
            //mModeThumbnail1_XCoordChanged(mModeThumbnail1, null);

        }

        void mModeThumbnail1_XCoordChanged(object sender, MouseEventArgs e)
        {
            //if(m_collecting_mmode) return;

            //m_collecting_mmode = true;
            //m_filegram.Video.GetMMode((sender as MModeThumbnail).XCoord, 1, 500);

            XCoord = (sender as MModeThumbnail).XCoord;
        }

        #region IFrameDisplay Members

        public Point Coordinates
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public event EventHandler CoordinatesChanged;

        public Frame CurrentFrame
        {
            get { return mModeDisplay1.MMode; }
        }

        public event EventHandler DataChanged;

        public bool EnableFilters
        {
            get;
            set;
        }

        public List<IFrameFunction> Filters
        {
            get
            {
                return m_filters;
            }
            set
            {
                m_filters = value;
                OnFiltersChanged();
            }
        }

        public event EventHandler FiltersChanged;

        public Pen HighlightPen
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public event EventHandler HistogramUpdated;

        public new void KeyPress(KeyEventArgs key_event)
        {
            throw new NotImplementedException();
        }

        public Stopwatch LastFilterWatch
        {
            get { throw new NotImplementedException(); }
        }

        public SohaCollection<MarkPair> Marks
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool MmodeDisplay
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Pen NormalPen
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public Frame PostOpFrame
        {
            get
            {
                return m_post_function_frame;
            }
        }

        public SOHA.Library.Xml.SohaCollection<Roi> ROI
        {
            get
            {
                return new SohaCollection<Roi>();
            }
            set
            {

            }
        }

        public bool UserInteractive
        {
            get
            {
                return false;
            }
            set
            {

            }
        }

        public event EventHandler VideoChanged;

        [Browsable(false)]
        [ReadOnly(true)]
        public Video VideoFile
        {
            get
            {
                return m_video;
            }
            set
            {
                if(value == null)
                    return;

                m_video = value;
                OnVideoChanged();
            }
        }

        protected void OnVideoChanged()
        {

            m_video.Open();
            XCoord = (int)((1.0 / 3) * m_video.Width);
            mModeThumbnail1.Thumbnail = m_video.GetFrameSync(1).ToBitmap();
            
            //MmodeSelection Box Settings

            m_video_info = "[Total Frames: " + m_video.Frames + "; Total Time: " + 
                (m_video.FrameTimes != null ? Math.Round(m_video.FrameTimes[m_video.FrameTimes.Length - 1],0).ToString() : "?") + " s]";
            label1.Text = m_video_info;
            //Setup Scroll Bar
            //Default

            //scrollXCoord.Minimum = 1;
            //scrollXCoord.Maximum = m_video.Width;
            //scrollXCoord.LargeChange = 1;
            //scrollXCoord.Value = 1;
            //scrollXCoord.Scroll += new ScrollEventHandler(hscrollFrame_Scroll);

            if(VideoChanged != null)
                VideoChanged(this, new EventArgs());
        }

        protected void OnFiltersChanged()
        {
            if(FiltersChanged != null)
                FiltersChanged(this, new EventArgs());

            mModeDisplay1.Refresh();
        }
        
        protected void OnFrameChanged()
        {
            if(FrameChanged != null)
                FrameChanged(this, new FrameEventArgs(m_mmode));

            m_current_bmp = CurrentFrame.ToBitmap();

            //Async Refresh, cascade into Async Histogram Update
            mModeDisplay1.Refresh();
        }

        #endregion

        private void mModeDisplay1_Paint(object sender, PaintEventArgs e)
        {
            if(CurrentFrame != null && !this.DesignMode)
            {
                m_post_function_frame = (Frame)CurrentFrame.Clone();

                try
                {
                    Frame frm = (Frame)CurrentFrame.Clone();
                    Bitmap b = m_current_bmp;//Conversions.FromFrame(CurrentFrame, (double)numContrast.Value);
                    List<IFrameFunction> filters = m_filters;//frmFilters.GetAllFilters();

                    //GpuFrame gFrame = null;
                    swFilters.Reset();
                    swFilters.Start();
                    if(EnableFilters && filters.Count > 0)
                    {
                        foreach(IFrameFunction func in filters)
                        {
                            switch(func.FuncType)
                            {
                                case FunctionType.Function:

                                    frm = func.Execute(frm);

                                    break;
                                case FunctionType.Filter:

                                    frm = func.Execute(frm);

                                    break;
                                default:
                                    break;
                            }
                        }
                        swFilters.Stop();

                        m_post_function_frame = (Frame)frm.Clone();

                        b = frm.ToBitmap();
                        e.Graphics.DrawImage(b, mModeDisplay1.Bounds, 0, 0, CurrentFrame.Width, CurrentFrame.Height, GraphicsUnit.Pixel);
                    }


                    mModeDisplay1.BackgroundImage = b;
                    mModeDisplay1.BackgroundImageLayout = ImageLayout.Stretch;

                }
                catch(Exception exc)
                {
                    Logs.LogException(exc);
                    MessageBox.Show("Error Painting Frame: " + exc.Message);
                }
                finally
                {
                }

                OnHistogramUpdated();
            }
        }

        protected void OnHistogramUpdated()
        {
            if(HistogramUpdated != null)
                HistogramUpdated(this, new EventArgs());
        }

        private void ucMmodeSelection1_CreateMmode(object sender, CreateMmodeEventArgs e)
        {
            if(CreateMMode != null)
                CreateMMode(this, e);
        }

        private void MModeFrameDisplay_Resize(object sender, EventArgs e)
        {
            int new_height = this.Height;
            int MAX = 400;

            if(new_height < MAX)
            {
                tableLayoutPanel1.RowStyles[tableLayoutPanel1.RowCount - 1].SizeType = SizeType.Absolute;
                tableLayoutPanel1.RowStyles[tableLayoutPanel1.RowCount - 1].Height = 0;
            }
            else
            {
                tableLayoutPanel1.RowStyles[tableLayoutPanel1.RowCount - 1].SizeType = SizeType.Absolute;
                tableLayoutPanel1.RowStyles[tableLayoutPanel1.RowCount - 1].Height = new_height - 400;
            }
        }
    }

    
}
