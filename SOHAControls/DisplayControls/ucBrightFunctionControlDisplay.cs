﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library.Xml;
using ZedGraph;

namespace SOHAControls.DisplayControls
{
    public partial class ucBrightFunctionControlDisplay : UserControl, IIntervalableFunctionControl, IRangeable, IGraphable
    {
        public event EventHandler ThresholdsChanged;

        private IntervalPhaseSettings m_settings;

        private VideoFrameRange m_range;
        
        //[Bindable(true)]
        //public double HiThreshold
        //{
        //    get
        //    {
        //        return ucBrightFunctionControl1.HiThreshold;
        //    }
        //    set
        //    {
        //        ucBrightFunctionControl1.HiThreshold = value;
        //    }
        //}
        //[Bindable(true)]
        //public double LoThreshold
        //{
        //    get
        //    {
        //        return ucBrightFunctionControl1.LoThreshold;
        //    }
        //    set
        //    {
        //        ucBrightFunctionControl1.LoThreshold = value;
        //    }
        //}
        //[Bindable(true)]
        //public bool Invert
        //{
        //    get
        //    {
        //        return ucBrightFunctionControl1.Invert;
        //    }
        //    set
        //    {
        //        ucBrightFunctionControl1.Invert = value;
        //    }
        //}
        //[Bindable(true)]
        //public bool UseDarkness
        //{
        //    get
        //    {
        //        return ucBrightFunctionControl1.UseDarkness;
        //    }
        //    set
        //    {
        //        ucBrightFunctionControl1.UseDarkness = value;
        //    }
        //}

        public RectangleF PlotRectangle
        {
            get
            {
                return ucBrightFunctionControl1.PlotRectangle;
            }
        }

        public VideoFrameRange Range
        {
            get
            {
                return ucBrightFunctionControl1.Range;
            }
            set
            {
                ucBrightFunctionControl1.Range = value;
            }
        }
        public List<double> YValues
        {
            get
            {
                return ucBrightFunctionControl1.YValues;
            }
        }

        public ucBrightFunctionControlDisplay()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            if(!this.DesignMode)
            {
                m_settings.SettingsChanged += new EventHandler(m_settings_SettingsChanged);
                ucBrightFunctionControl1.MouseClickZedGraph += new MouseEventHandler(ucBrightFunctionControl1_MouseClickZedGraph);
                ucBrightFunctionControl1.ThresholdsChanged += new EventHandler(ucBrightFunctionControl1_ThresholdsChanged);

                chkInvert.DataBindings.Add("Checked", m_settings, "Invert", true, DataSourceUpdateMode.OnPropertyChanged);
                chkUseDarkness.DataBindings.Add("Checked", m_settings, "UseBright", true, DataSourceUpdateMode.OnPropertyChanged);


                scrollDarknessHigh.Value = 100 - m_settings.BrightThresholdHi;
                scrollDarknessLow.Value = 100 - m_settings.BrightThresholdLo;
                lblHighValue.Text = m_settings.BrightThresholdHi.ToString("#.##");
                lblLowValue.Text = m_settings.BrightThresholdLo.ToString("#.##");


                numHiPass.Value = (int)(m_settings as IntervalPhaseSettings).BrightHiCutoff;
                numLoPass.Value = (int)(m_settings as IntervalPhaseSettings).BrightLoCutoff;
                numHiPass.ValueChanged += new EventHandler(numValueChanged);
                numLoPass.ValueChanged += new EventHandler(numValueChanged);

                DrawGraph();
            }

            base.OnLoad(e);
        }

        void ucBrightFunctionControl1_MouseClickZedGraph(object sender, MouseEventArgs e)
        {
            GraphPane myPane = ucBrightFunctionControl1.GraphPane;
            RectangleF plot = myPane.Chart.Rect;
            RectangleF control = myPane.Rect;

            #region Determine if cursor is over plot

            bool cursor_over_plot = false;

            if(e.X >= plot.Left && e.X <= control.Width - plot.Left)
                cursor_over_plot = true;

            if(!cursor_over_plot)
                return;

            #endregion

            #region Cursor Over Plot and Left Click, Add Threshold

            if(cursor_over_plot &&
                e.Button == System.Windows.Forms.MouseButtons.Left)
            {

                //myPane.CurveList.Remove(myPane.CurveList["ThresholdLo"]);

                //Shawn, you're an idiot. That invert is so simple.
                double invert_e_y = Math.Abs(e.Y - control.Height);

                double click_ratio_scaled = (invert_e_y - plot.Y) / plot.Height;//(((double)zedGraphControl1.Height - e.Y) / (double)zedGraphControl1.Height);
                double y_val = click_ratio_scaled * Math.Abs(myPane.YAxis.Scale.Max - myPane.YAxis.Scale.Min) + myPane.YAxis.Scale.Min;

                //Add Threshold
                m_settings.BrightThresholdLo = (int)(y_val * 100.0);
                //m_thresholds[0] = y_val;

                //OnPropertyChanged("LoThreshold");
                //PointPairList y = new PointPairList();
                //for(int i = 0; i < m_fx.Values.Count; i++)
                //{
                //    y.Add(i, y_val);
                //}

                //PointPairList y = new PointPairList();
                //int x = 0;
                //foreach(int z in (m_filegram.Phases[Library.Xml.PhaseName.Preprocess].Data[0] as PreprocessPhaseData).Brightness)
                //{
                //    y.Add(x++, y_val);
                //}

                //LineItem barPDF = myPane.AddCurve("ThresholdLo", y, Color.DarkRed, SymbolType.None);

                //zedGraphControl1.Refresh();
            }

            #endregion

            #region Cursor Over Plot and Right Click, Add Second Threshold

            if(cursor_over_plot &&
                ucBrightFunctionControl1.RightClickThresholdEnable &&
                e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                myPane.CurveList.Remove(myPane.CurveList["ThresholdHi"]);

                //Shawn, you're an idiot. That invert is so simple.
                double invert_e_y = Math.Abs(e.Y - control.Height);

                double click_ratio_scaled = (invert_e_y - plot.Y) / plot.Height;//(((double)zedGraphControl1.Height - e.Y) / (double)zedGraphControl1.Height);
                double y_val = click_ratio_scaled * Math.Abs(myPane.YAxis.Scale.Max - myPane.YAxis.Scale.Min) + myPane.YAxis.Scale.Min;

                //Add Threshold
                m_settings.BrightThresholdHi = (int)(y_val * 100.0);
                //m_thresholds[1] = y_val;
                //OnPropertyChanged("HiThreshold");
                //PointPairList y = new PointPairList();
                //for(int i = 0; i < m_fx.Values.Count ; i++)
                //{
                //    y.Add(i, y_val);
                //}

                //PointPairList y = new PointPairList();
                //int x = 0;
                //foreach(int z in (m_filegram.Phases[Library.Xml.PhaseName.Preprocess].Data[0] as PreprocessPhaseData).Brightness)
                //{
                //    y.Add(x++, y_val);
                //}

                //LineItem barPDF = myPane.AddCurve("ThresholdHi", y, Color.DarkBlue, SymbolType.None);

                //zedGraphControl1.Refresh();
            }

            #endregion
        }

        /// <summary>
        /// Called after:
        /// -Interaction with scroll bar itself
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void scrollDarknessHigh_Scroll(object sender, ScrollEventArgs e)
        {
            m_settings.BrightThresholdHi = e.NewValue;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Called after:
        /// -Value changed by plot click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void scrollDarknessHigh_ValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void m_settings_SettingsChanged(object sender, EventArgs e)
        {
            if(m_settings.BrightThresholdHi < 0 || m_settings.BrightThresholdHi > 100
                || m_settings.BrightThresholdLo < 0 || m_settings.BrightThresholdLo > 100)
            {
                return;
            }

            scrollDarknessHigh.Value = 100 - m_settings.BrightThresholdHi;
            scrollDarknessLow.Value = 100 - m_settings.BrightThresholdLo;
            lblHighValue.Text = m_settings.BrightThresholdHi.ToString("#.##");
            lblLowValue.Text = m_settings.BrightThresholdLo.ToString("#.##");
            
            ucBrightFunctionControl1.DrawGraph();
        }

        public void SetSettings(ref IntervalPhaseSettings settings)
        {
            //Add Data Binding?

            m_settings = settings;
            ucBrightFunctionControl1.Settings = m_settings;
            //m_settings.Invert = true;
            //chkInvert.DataBindings.Add("Checked", m_settings, "UseBright", true, DataSourceUpdateMode.OnPropertyChanged);

            //HiThreshold = settings.BrightThresholdHi;
            //LoThreshold = settings.BrightThresholdLo;
            //numHiPass.Value = (decimal)settings.BrightHiCutoff;
            //numLoPass.Value = (decimal)settings.BrightLoCutoff;
            //Invert = settings.Invert;
            //UseDarkness = settings.UseBright;

            //m_settings.Invert = true;

            //this.Invalidate();
            //this.Refresh();
        }

        public IntervalPhaseSettings GetSettings()
        {
            //IntervalPhaseSettings settings = new IntervalPhaseSettings();

            //settings.Invert = chkInvert.Checked;
            //settings.UseBright = chkUseDarkness.Checked;
            //settings.BrightThresholdHi = scrollDarknessHigh.Value;
            //settings.BrightThresholdLo = scrollDarknessLow.Value;

            return m_settings;
        }

        void numValueChanged(object sender, EventArgs e)
        {
            m_settings.BrightHiCutoff = (double)numHiPass.Value;
            m_settings.BrightLoCutoff = (double)numLoPass.Value;

            //Filter filt = new Filter();
            //filt.HiCutoff = (double)numHiPass.Value;
            //filt.LoCutoff = (double)numLoPass.Value;

            //ucBrightFunctionControl1.Filter = filt;
        }

        void ucBrightFunctionControl1_ThresholdsChanged(object sender, EventArgs e)
        {
            //double max = ucBrightFunctionControl1.GraphPane.YAxis.Scale.Max;
            //double min = ucBrightFunctionControl1.GraphPane.YAxis.Scale.Min;

            //double lo = m_settings.BrightThresholdLo; //ucBrightFunctionControl1.LoThreshold;
            //double hi = m_settings.BrightThresholdHi; //ucBrightFunctionControl1.HiThreshold;

            //if(hi > min && hi < max)
            //    scrollDarknessHigh.Value = 100 - (int)((hi - min) / (max - min) * 100);

            //if(lo > min && lo < max)
            //    scrollDarknessLow.Value = 100 - (int)((lo - min) / (max - min) * 100);

            //scrollDarknessHigh.Value = 100 - ucBrightFunctionControl1.Settings.BrightThresholdHi;
            //scrollDarknessLow.Value = 100 - ucBrightFunctionControl1.Settings.BrightThresholdLo;

            OnThresholdChanged(e);
        }

        void OnThresholdChanged(EventArgs e)
        {
            if(ThresholdsChanged != null)
                ThresholdsChanged(this, e);
        }

        /// <summary>
        /// Given an input function, assumes all x values are ascending positive integers.
        /// </summary>
        /// <param name="y">Input Function</param>
        public void SetXValuesFromY(List<double> y)
        {
            ucBrightFunctionControl1.SetXValuesFromY(y);
            
            //Set Scroll Bars
            //scrollDarknessHigh.Minimum = scrollDarknessLow.Minimum = ucBrightFunctionControl1.GraphPane.YAxis.Scale.Min;
            //scrollDarknessHigh.Maximum = scrollDarknessLow.Maximum = ucBrightFunctionControl1.GraphPane.YAxis.Scale.Max;
        }

        #region IIntervalableFunctionControl Members

        public List<SOHA.Library.Interval> GetIntervals()
        {
            return ucBrightFunctionControl1.GetIntervals();
        }

        #endregion

        #region IRangeable Members

        public event EventHandler VideoFrameRangeChanged;

        public void OnVideoFrameRangeChanged(EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IGraphable Members

        public void SuspendGraph()
        {
            ucBrightFunctionControl1.SuspendGraph();
        }

        public void ResumeGraph()
        {
            ucBrightFunctionControl1.ResumeGraph();
        }

        public void DrawGraph()
        {
            ucBrightFunctionControl1.DrawGraph();
        }

        #endregion
    }
}
