﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using System.Drawing.Imaging;
using SOHA.Library.Imaging;
using ZedGraph;

namespace SOHAControls.DisplayControls
{
    public partial class ucHistogram : UserControl
    {
        public ucHistogram()
        {
            InitializeComponent();
        }

        public void SetFrame(Frame frame)
        {
            GraphPane myPane = zedGraphControl1.GraphPane;
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            myPane.Title.Text = "Histogram";
            myPane.XAxis.Title.Text = "Pixel Value";
            myPane.YAxis.Title.Text = "Count";

            PointPairList pdf = new PointPairList();
            foreach(KeyValuePair<int, int> kvp in frame.HistogramBin.PDF)
            {
                pdf.Add(kvp.Key,kvp.Value);
            }

            BarItem barPDF = myPane.AddBar("PDF", pdf, Color.Red);

            zedGraphControl1.AxisChange();

            PointPairList cdf = new PointPairList();
            double max = frame.HistogramBin.CDF.Max(x => x.Value);
            double height = zedGraphControl1.Height;

            foreach(KeyValuePair<int, int> kvp in frame.HistogramBin.CDF)
            {
                cdf.Add(kvp.Key, height * kvp.Value / max);
            }

            LineItem lineCDF = myPane.AddCurve("CDF", cdf, Color.Blue);

            zedGraphControl1.Refresh();
        }
    }
}
