﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using SOHA.Library.Mathematics;

namespace SOHAControls.DisplayControls
{
    public abstract partial class Display : PictureBox, IWindowable
    {

        //Filtering
        //Interval (tied to thresholds?)
        //Threshold(s)

        protected readonly double[] m_data;

        public Display()
        {
            InitializeComponent();
            InitializeDisplay();
        }

        public Display(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            InitializeDisplay();
        }

        private void InitializeDisplay()
        {
            BackColor = Color.White;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

            CurrentWindow = 1;
            WindowSize = 1000;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            if(pe is GraphEventArgs)
            {
                GraphEventArgs ge = pe as GraphEventArgs;

                Bitmap b = new Bitmap(WindowSize, 100);
                Graphics g = Graphics.FromImage((Image)b);
                Pen p = new Pen(Color.Black, 1);
                g.Clear(Color.White);

                int start = (CurrentWindow - 1) * WindowSize;
                int end = CurrentWindow * WindowSize - 1;

                double[] data = ge.Y.ToArray();

                Filter(ref data, ge.Filter);
                PreGraph(ref data);

                //double[] data = new double[f.Count];
                //f.CopyTo(data);

                for(int i = start + 1; i < end && i < data.Length; i++)
                {
                    g.DrawLine(p,
                        ((i - 1) % WindowSize),
                        100 - (float)data[i - 1] * 100,
                        (i % WindowSize),
                        100 - (float)data[i] * 100);
                }

                PostGraph(g, b);

                BackgroundImage = b;
                BackgroundImageLayout = ImageLayout.Stretch;

                g.Dispose();
            }            

            base.OnPaint(pe);
        }

        protected virtual void Filter(ref double[] data, Filter filter )
        {
            data = filter.DualInputZeroPhaseFilter(data);

            //int N = 3;
            //Filter m_filter = new Filter();
            //double[] data = func.ToArray();

            ////data = ArrayFunctions.PlusMinusWindow(data, 3);
            ////data_out[n].Intensity = ArrayFunctions.PlusMinusWindow(data_out[n].Intensity, 3);
            //bool perform_filter = false;

            //double wc_high = HiCutoff / Fps;
            //double wc_low = LoCutoff / Fps;

            //if(wc_high > 0 && wc_high < 1 && wc_low > 0 && wc_low < 1 && wc_high > wc_low)
            //{
            //    //BandPass
            //    m_filter = IIRButterworth.Butter(N, new double[] { wc_low, wc_high }, FilterType.Bandpass);
            //    perform_filter = true;
            //}
            //else if(wc_high > 0 && wc_high < 1)
            //{
            //    //HighPass
            //    m_filter = IIRButterworth.Butter(N, new double[] { wc_high }, FilterType.Low);
            //    perform_filter = true;
            //}
            //else if(wc_low > 0 && wc_low < 1)
            //{
            //    //LoPass
            //    m_filter = IIRButterworth.Butter(N, new double[] { wc_low }, FilterType.High);
            //    perform_filter = true;
            //}

            //if(perform_filter)
            //    data = m_filter.DualInputZeroPhaseFilter(data);

            ////if (m_lo_pass_frequency > 0)
            ////{
            ////    Filter filter = IIRButterworth.Butter(4, new double[] { m_lo_pass_frequency }, FilterType.Low);
            ////    data = filter.DualInputZeroPhaseFilter(data);
            ////}

            //func = ArrayFunctions.Normalize(data.ToList());
            //m_last_data = func.ToArray();
        }

        protected virtual void PreGraph(ref double[] data)
        {

        }

        protected virtual void PostGraph(Graphics g, Bitmap b)
        {
            //AddThresholds(g, b);
        }

        protected abstract override void OnMouseClick(MouseEventArgs e);


        #region IWindowable Members

        public event EventHandler WindowChanged;

        public int CurrentWindow
        {
            get;
            set;
        }

        public int WindowSize
        {
            get;
            set;
        }

        public void NextWindow()
        {
            CurrentWindow++;

            int MAX_WINDOW = (int)Math.Ceiling((double)m_data.Length / WindowSize);

            if(CurrentWindow > MAX_WINDOW)
            {
                CurrentWindow = MAX_WINDOW;
            }
            else
            {
                OnWindowChanged(new EventArgs());
            }
        }

        public void PreviousWindow()
        {
            CurrentWindow--;

            if(CurrentWindow < 1)
            {
                CurrentWindow = 0;
            }
            else
            {
                OnWindowChanged(new EventArgs());
            }
        }

        public void OnWindowChanged(EventArgs e)
        {
            //Redraw
            Invalidate();

            if(WindowChanged != null)
                WindowChanged(this, e);
        }

        #endregion
    }
}
