﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Video;

namespace SOHAControls.DisplayControls
{
    public partial class ucMmodeSelection : UserControl
    {
        public event EventHandler<CreateMmodeEventArgs> CreateMmode;

        private Video m_video;
        public Video VideoFile
        {
            get
            {
                return m_video;
            }
            set
            {
                if(value == null)
                    return;

                m_video = value;
                OnVideoChanged();
            }
        }

        public bool FullVideo
        {
            get
            {
                return chkFullVideo.Checked;
            }
        }

        public ucMmodeSelection()
        {
            InitializeComponent();
            groupBox1.ProcessAllControls(ctrl => ctrl.Enabled = false);
            //m_video = video;
        }

        public ucMmodeSelection(Video video)
        {
            InitializeComponent();

            m_video = video;
        }

        private void chkFullVideo_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.ProcessAllControls(ctrl => ctrl.Enabled = !FullVideo);
        }

        protected void OnVideoChanged()
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateMmodeType type = rdoByFrame.Checked ? CreateMmodeType.ByFrame : CreateMmodeType.ByTime;
            if(CreateMmode != null)
                CreateMmode(this, new CreateMmodeEventArgs(FullVideo, type, (int)numStart.Value, (int)numEnd.Value));
        }
    }
}
