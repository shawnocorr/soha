﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHAControls.DisplayControls
{
    public interface IWindowable
    {

        event EventHandler WindowChanged;

        int CurrentWindow { get; set; }
        int WindowSize { get; set; }

        void NextWindow();
        void PreviousWindow();
        void OnWindowChanged(EventArgs e);

    }
}
