﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library.Xml;
using ZedGraph;

namespace SOHAControls.DisplayControls
{
    public partial class ucIntensityFunctionControlDisplay : UserControl, IIntervalableFunctionControl, IRangeable, IGraphable
    {
        public event EventHandler ThresholdsChanged;

        private IntervalPhaseSettings m_settings;

        private VideoFrameRange m_range;

        public VideoFrameRange Range
        {
            get
            {
                return ucIntensityFunctionControl1.Range;
            }
            set
            {
                ucIntensityFunctionControl1.Range = value;
            }
        }
        public List<double> YValues
        {
            get
            {
                return ucIntensityFunctionControl1.YValues;
            }
        }

        public ucIntensityFunctionControlDisplay()
        {
            InitializeComponent();

            //numMinInterval.Value = 8;
        }

        protected override void OnLoad(EventArgs e)
        {
            if(!this.DesignMode)
            {
                m_settings.SettingsChanged += new EventHandler(m_settings_SettingsChanged);
                ucIntensityFunctionControl1.MouseClickZedGraph += new MouseEventHandler(ucIntensityFunctionControl1_MouseClickZedGraph);
                ucIntensityFunctionControl1.ThresholdsChanged += new EventHandler(ucIntensityFunctionControl1_ThresholdsChanged);
                
                numMinInterval.DataBindings.Add("Value", m_settings, "IntensityMinInterval", true, DataSourceUpdateMode.OnPropertyChanged);

                scrollThreshold.Value = 100 - m_settings.IntensityThreshold;
                label2.Text = m_settings.IntensityThreshold.ToString("#.##");

                numHiPass.Value = (int)(m_settings as IntervalPhaseSettings).IntensityHiCutoff;
                numLoPass.Value = (int)(m_settings as IntervalPhaseSettings).IntensityLoCutoff;
                numHiPass.ValueChanged += new EventHandler(numValueChanged);
                numLoPass.ValueChanged += new EventHandler(numValueChanged);
                
                DrawGraph();
            }

            base.OnLoad(e);
        }

        void ucIntensityFunctionControl1_MouseClickZedGraph(object sender, MouseEventArgs e)
        {
            GraphPane myPane = ucIntensityFunctionControl1.GraphPane;
            RectangleF plot = myPane.Chart.Rect;
            RectangleF control = myPane.Rect;

            #region Determine if cursor is over plot

            bool cursor_over_plot = false;

            if(e.X >= plot.Left && e.X <= control.Width - plot.Left)
                cursor_over_plot = true;

            if(!cursor_over_plot)
                return;

            #endregion

            #region Cursor Over Plot and Left Click, Add Threshold

            if(cursor_over_plot &&
                e.Button == System.Windows.Forms.MouseButtons.Left)
            {

                //myPane.CurveList.Remove(myPane.CurveList["ThresholdLo"]);

                //Shawn, you're an idiot. That invert is so simple.
                double invert_e_y = Math.Abs(e.Y - control.Height);

                double click_ratio_scaled = (invert_e_y - plot.Y) / plot.Height;//(((double)zedGraphControl1.Height - e.Y) / (double)zedGraphControl1.Height);
                double y_val = click_ratio_scaled * Math.Abs(myPane.YAxis.Scale.Max - myPane.YAxis.Scale.Min) + myPane.YAxis.Scale.Min;

                //Add Threshold
                m_settings.IntensityThreshold = (int)(y_val * 100.0);
                //m_thresholds[0] = y_val;

                //OnPropertyChanged("LoThreshold");
                //PointPairList y = new PointPairList();
                //for(int i = 0; i < m_fx.Values.Count; i++)
                //{
                //    y.Add(i, y_val);
                //}

                //PointPairList y = new PointPairList();
                //int x = 0;
                //foreach(int z in (m_filegram.Phases[Library.Xml.PhaseName.Preprocess].Data[0] as PreprocessPhaseData).Brightness)
                //{
                //    y.Add(x++, y_val);
                //}

                //LineItem barPDF = myPane.AddCurve("ThresholdLo", y, Color.DarkRed, SymbolType.None);

                //zedGraphControl1.Refresh();
            }

            #endregion
        }

        void m_settings_SettingsChanged(object sender, EventArgs e)
        {
            //ucIntensityFunctionControl1.SuspendGraph();

            scrollThreshold.Value = 100 - m_settings.IntensityThreshold;
            label2.Text = m_settings.IntensityThreshold.ToString("#.##");

            //m_settings.IntensityMinInterval = (int)numMinInterval.Value;

            ucIntensityFunctionControl1.DrawGraph();
        }

        void numMinInterval_ValueChanged(object sender, EventArgs e)
        {
            m_settings.IntensityMinInterval = (int)numMinInterval.Value;
        }

        void numValueChanged(object sender, EventArgs e)
        {
            m_settings.IntensityHiCutoff = (double)numHiPass.Value;
            m_settings.IntensityLoCutoff = (double)numLoPass.Value;

            //Filter filt = new Filter();
            //filt.HiCutoff = (double)numHiPass.Value;
            //filt.LoCutoff = (double)numLoPass.Value;

            //ucIntensityFunctionControl1.Filter = filt;
        }

        void scrollDarkness_ValueChanged(object sender, EventArgs e)
        {
            double max = ucIntensityFunctionControl1.GraphPane.YAxis.Scale.Max;
            double min = ucIntensityFunctionControl1.GraphPane.YAxis.Scale.Min;

            //double lo = scrollDarknessLow.Value;
            //double hi = scrollThreshold.Value;

            //ucIntensityFunctionControl1.HiThreshold = (hi / 100.0) * (max - min) + min;
            //ucIntensityFunctionControl1.LoThreshold = (lo / 100.0) * (max - min) + min;

            //if(hi > min && hi < max)
            //    scrollDarknessHigh.Value = 100 - (int)((hi - min) / (max - min) * 100);

            //if(lo > min && lo < max)
            //    scrollDarknessLow.Value = 100 - (int)((lo - min) / (max - min) * 100);
        }

        void ucIntensityFunctionControl1_ThresholdsChanged(object sender, EventArgs e)
        {
            double max = ucIntensityFunctionControl1.GraphPane.YAxis.Scale.Max;
            double min = ucIntensityFunctionControl1.GraphPane.YAxis.Scale.Min;

            //double thresh = ucIntensityFunctionControl1.Threshold;

            //if(thresh > min && thresh < max)
            //    scrollThreshold.Value = 100 - (int)((thresh - min) / (max - min) * 100);

            OnThresholdChanged(e);

        }

        void OnThresholdChanged(EventArgs e)
        {
            if(ThresholdsChanged != null)
                ThresholdsChanged(this, e);
        }

        /// <summary>
        /// Given an input function, assumes all x values are ascending positive integers.
        /// </summary>
        /// <param name="y">Input Function</param>
        public void SetXValuesFromY(List<double> y)
        {
            ucIntensityFunctionControl1.SetXValuesFromY(y);


            //Set Scroll Bars
            //scrollDarknessHigh.Minimum = scrollDarknessLow.Minimum = ucIntensityFunctionControl1.GraphPane.YAxis.Scale.Min;
            //scrollDarknessHigh.Maximum = scrollDarknessLow.Maximum = ucIntensityFunctionControl1.GraphPane.YAxis.Scale.Max;
        }

        public void SetSettings(ref IntervalPhaseSettings settings)
        {
            //Add Data Binding?

            m_settings = settings;
            ucIntensityFunctionControl1.Settings = m_settings;
        }

        public IntervalPhaseSettings GetSettings()
        {
            return m_settings;
        }

        #region IIntervalableFunctionControl Members

        public List<SOHA.Library.Interval> GetIntervals()
        {
            return ucIntensityFunctionControl1.GetIntervals();
        }

        #endregion

        #region IRangeable Members

        public event EventHandler VideoFrameRangeChanged;

        public void OnVideoFrameRangeChanged(EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IGraphable Members

        public void SuspendGraph()
        {
            ucIntensityFunctionControl1.SuspendGraph();
        }

        public void ResumeGraph()
        {
            ucIntensityFunctionControl1.ResumeGraph();
        }

        public void DrawGraph()
        {
            ucIntensityFunctionControl1.DrawGraph();
        }

        #endregion
    }
}
