﻿namespace SOHAControls.DisplayControls
{
    partial class ucMModeFunctionControlDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SOHAControls.IntervalProperties intervalProperties1 = new SOHAControls.IntervalProperties();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucMModeFunctionControlDisplay));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mModeDisplay = new Forms.MModeDisplay(this.components);
            this.mModeThumbnail = new SOHAControls.MModeThumbnail(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.mModeThumbnail, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mModeDisplay, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1017, 333);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // mModeDisplay
            // 
            this.mModeDisplay.BackColor = System.Drawing.Color.White;
            this.mModeDisplay.DisplayCoordinates = false;
            intervalProperties1.Interval = 1;
            intervalProperties1.IntervalSize = 1000;
            this.mModeDisplay.Interval = intervalProperties1;
            this.mModeDisplay.List = null;
            this.mModeDisplay.Location = new System.Drawing.Point(35, 3);
            this.mModeDisplay.Margin = new System.Windows.Forms.Padding(35, 3, 3, 3);
            this.mModeDisplay.MMode = null;
            this.mModeDisplay.Name = "mModeDisplay";
            this.mModeDisplay.Phase = SOHA.Library.PhaseFormType.Interval;
            this.mModeDisplay.PhaseOffset = 0;
            this.mModeDisplay.Range = null;
            this.mModeDisplay.ShowChangeIntervals = false;
            this.mModeDisplay.Size = new System.Drawing.Size(500, 100);
            this.mModeDisplay.TabIndex = 1;
            this.mModeDisplay.TabStop = false;
            this.mModeDisplay.Zoom = 1;
            // 
            // mModeThumbnail
            // 
            this.mModeThumbnail.BackColor = System.Drawing.Color.White;
            this.mModeThumbnail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mModeThumbnail.BackgroundImage")));
            this.mModeThumbnail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mModeThumbnail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mModeThumbnail.Location = new System.Drawing.Point(820, 3);
            this.mModeThumbnail.Name = "mModeThumbnail";
            this.mModeThumbnail.Size = new System.Drawing.Size(194, 327);
            this.mModeThumbnail.TabIndex = 2;
            this.mModeThumbnail.TabStop = false;
            this.mModeThumbnail.Thumbnail = ((System.Drawing.Bitmap)(resources.GetObject("mModeThumbnail.Thumbnail")));
            this.mModeThumbnail.XCoord = 0;
            // 
            // ucMModeFunctionControlDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ucMModeFunctionControlDisplay";
            this.Size = new System.Drawing.Size(1017, 333);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mModeDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mModeThumbnail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Forms.MModeDisplay mModeDisplay;
        private MModeThumbnail mModeThumbnail;
    }
}
