﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Mathematics;

namespace SOHAControls
{
    public partial class BandPassFilterControl : UserControl
    {
        private Filter m_filter;

        public Filter Filter
        {
            get { return m_filter; }
            set
            {
                if(m_filter == null)
                    return;

                m_filter = value;
                OnPropertyChanged("Filter");
                //this.Invalidate();
                //base.Paint();
            }
        }

        private double m_hi_cutoff = 0;
        private double m_lo_cutoff = 0;

        [Bindable(true)]
        public double HiCutoff
        {
            get
            {
                return m_hi_cutoff;
            }
            set
            {
                m_hi_cutoff = value;
                m_filter.HiCutoff = value;
                OnPropertyChanged("HiCutoff");
                OnPropertyChanged("Filter");
            }
        }
        [Bindable(true)]
        public double LoCutoff
        {
            get
            {
                return m_lo_cutoff;
            }
            set
            {
                m_lo_cutoff = value;
                m_filter.LoCutoff = value;
                OnPropertyChanged("LoCutoff");
                OnPropertyChanged("Filter");
            }
        }
        
        private int m_nyquist = 80; //DEFAULT 160 fps

        public BandPassFilterControl()
        {
            InitializeComponent();

            numHiCutoff.DataBindings.Add(new Binding("Value", this, "HiCutoff", false, DataSourceUpdateMode.OnPropertyChanged));
            numLoCutoff.DataBindings.Add(new Binding("Value", this, "LoCutoff", false, DataSourceUpdateMode.OnPropertyChanged));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
