﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHA.SohaControls
{
    public partial class SohaToolTip : ToolTip
    {
        public SohaToolTip()
        {
            InitializeComponent();
            Initialize();
        }

        public SohaToolTip(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            Initialize();
        }

        private void Initialize()
        {
            // Set up the delays for the ToolTip.
            this.AutoPopDelay = 5000;
            this.InitialDelay = 1000;
            this.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            this.ShowAlways = true;
        }
    }
}
