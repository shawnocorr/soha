﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Mathematics;

namespace SOHAControls
{
    public partial class IntensityDisplay : FunctionDisplay, IIntervalable, INotifyPropertyChanged
    {
        public event EventHandler IntervalsChanged;
        public event EventHandler ThresholdsChanged;

        protected int m_threshold;
        protected int m_min_intervals;
        protected double m_lo_pass_frequency;
        //private List<double> m_last_data;

        [Bindable(true)]
        public int Threshold
        {
            get
            {
                return m_threshold;
            }
            set
            {
                m_threshold = value;
                OnPropertyChanged("Threshold");
                OnThresholdsChanged(new EventArgs());
                this.Invalidate();
            }
        }
        [Bindable(true)]
        public int MinIntervals
        {
            get
            {
                return m_min_intervals;
            }
            set
            {
                m_min_intervals = value;
                OnPropertyChanged("MinIntervals");
                OnThresholdsChanged(new EventArgs());
                Paint();
                //this.Invalidate();
            }
        }
        [Bindable(true)]
        public double HiCutoff
        {
            get
            {
                return m_hi_cutoff;
            }
            set
            {
                m_hi_cutoff = value;
                OnPropertyChanged("HiCutoff");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }
        [Bindable(true)]
        public double LoCutoff
        {
            get
            {
                return m_lo_cutoff;
            }
            set
            {
                m_lo_cutoff = value;
                OnPropertyChanged("LoCutoff");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }

        public IntensityDisplay()
        {
            InitializeComponent();

            plot.MouseClick += new MouseEventHandler(plot_MouseClick);
        }

        void plot_MouseClick(object sender, MouseEventArgs e)
        {
            int val = (int)(e.Y / (double)plot.ClientSize.Height * 100);

            Threshold = val;

            this.Invalidate();
            //OnIntervalsChanged(new EventArgs());
            OnThresholdsChanged(new EventArgs());
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            //OnIntervalsChanged(new EventArgs());
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
        }

        protected void OnIntervalsChanged(EventArgs e)
        {
            if (IntervalsChanged != null)
                IntervalsChanged(this, e);
        }

        protected void OnThresholdsChanged(EventArgs e)
        {
            if (ThresholdsChanged != null)
                ThresholdsChanged(this, new EventArgs());
        }

        public override void PreFilter(ref List<double> func)
        {
            int N = 3;
            Filter m_filter = new Filter();
            double[] data = func.ToArray();
            //data = ArrayFunctions.PlusMinusWindow(data, 3);
            //data_out[n].Intensity = ArrayFunctions.PlusMinusWindow(data_out[n].Intensity, 3);
            bool perform_filter = false;

            double wc_high = HiCutoff / Fps;
            double wc_low = LoCutoff / Fps;

            if (wc_high > 0 && wc_high < 1 && wc_low > 0 && wc_low < 1 && wc_high > wc_low)
            {
                //BandPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_low, wc_high }, FilterType.Bandpass);
                perform_filter = true;
            }
            else if (wc_high > 0 && wc_high < 1)
            {
                //HighPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_high }, FilterType.Low);
                perform_filter = true;
            }
            else if(wc_low > 0 && wc_low < 1)
            {
                //LoPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_low }, FilterType.High);
                perform_filter = true;
            }

            if (perform_filter)
                data = m_filter.DualInputZeroPhaseFilter(data);

            //if (m_lo_pass_frequency > 0)
            //{
            //    Filter filter = IIRButterworth.Butter(4, new double[] { m_lo_pass_frequency }, FilterType.Low);
            //    data = filter.DualInputZeroPhaseFilter(data);
            //}

            func = ArrayFunctions.Normalize(data.ToList());
            m_last_data = func.ToArray();
        }

        public double[] GetValues(int x, int interval, int interval_size)
        {
            if (m_last_data == null || m_last_data.Length == 0)
                return new double[2];

            double[] point = new double[2];
            point[0] = (int)((x / (double)plot.Width) * interval_size) + (interval - 1) * 1000;
            if(point[0] < m_last_data.Length)
                point[1] = m_last_data[(int)point[0]];

            return point;
        }

        public override void AddThresholds(Graphics g, Bitmap b)
        {
            g.DrawLine(new Pen(Color.Red, 1),
                0,
                (float)m_threshold,
                b.Width,
                (float)m_threshold);
        }

        #region IIntervalable Members

        public List<Interval> GetIntervals()
        {
            List<Interval> ret = new List<Interval>();

            double[] tmpdata = new double[m_data.Count];
            m_data.CopyTo(tmpdata);
            List<double> temp = tmpdata.ToList();

            PreFilter(ref temp);

            double threshold = (100 - m_threshold) / 100.0;

            for (int i = 1; i < temp.Count; i++)
            {
                if ((temp[i - 1] < threshold) && (temp[i] > threshold))
                {
                    Interval l_interval = new Interval(i, IntervalType.Systole);
                    ret.Add(l_interval);
                }
                else if ((temp[i - 1] > threshold) && (temp[i] < threshold))
                {
                    Interval l_interval = new Interval(i, IntervalType.Diastole);
                    ret.Add(l_interval);
                }
            }

            return ret;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        } 
    }
}
