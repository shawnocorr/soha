﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SOHAControls
{
    public partial class MModeThumbnail : PictureBox
    {
        public event MouseEventHandler XCoordChanged;

        private static Bitmap dflt = new Bitmap(100, 100);

        protected Bitmap m_thumbnail = dflt;
        protected int m_coord;

        public Bitmap Thumbnail
        {
            get { return m_thumbnail; }
            set 
            { 
                m_thumbnail = value;
                Paint();
            }
        }
        public int XCoord
        {
            get { return m_coord; }
            set
            {
                m_coord = value;
                Paint();
            }
        }

        public MModeThumbnail()
        {
            InitializeComponent();
        }

        public MModeThumbnail(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected void Paint()
        {
            if (m_thumbnail != null)
            {

                Bitmap b = (Bitmap)m_thumbnail.Clone();
                Graphics g = Graphics.FromImage(b);

                if (m_coord != 0 && BackgroundImage != null)
                {
                    int x = (int)((double)m_coord / (double)m_thumbnail.Width * (double)b.Width);
                    g.DrawLine(new Pen(Color.Red, 1), x, 0, x, b.Height);
                }

                g.Dispose();

                BackgroundImage = b;
                BackgroundImageLayout = ImageLayout.Stretch;
            }
        }

        void MModeThumbnail_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (BackgroundImage != null)
            {
                m_coord = (int)((double)e.X / (double)this.Width * (double)BackgroundImage.Width);
                Paint();

                if (XCoordChanged != null)
                    XCoordChanged(this, new MouseEventArgs(MouseButtons.Left, 0, m_coord, 0, 0));
            }
        }
    }
}
