﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SOHAControls")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("SOHAControls")]
[assembly: AssemblyCopyright("Copyright © Oaktree Technologies 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9931b448-6428-47e3-8c93-8695917639ea")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.3.0.5")]
[assembly: AssemblyFileVersion("2.3.0.5")]

/*  1.21:   Added to PhaseDisplay.GetIntervals(); changed take 1st 5 phase intervals to take first n, or 5, if > 5
 * 
 * 
 * 
 */

/*  1.22:   Added to Change Intervals to Intensity GetIntervals()
 *          MModeDisplay includes Show/Hide Change Intervals
 * 
 * 
 * 
 */

/*  2.0.0.1:   Began new heirarchy on Display UserControls
 * 
 * 
 * 
 */

/*  2.0.0.2:   Rollout 2/23/2015
 * 
 * 
 * 
 */

/*  2.1.0.0:   Rollout 3/2/2015
 * 
 * 
 * 
 */

/*  2.1.0.1:
 *      Added AllowNulls bool field to NumericTextbox
 * 
 * 
 */

/*  2.2.0.0:
 *      Created FrameDisplay UserControl
 * 
 * 
 */
