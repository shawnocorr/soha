﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHA.SohaControls
{
    public partial class SilentNumericUpDown : NumericUpDown
    {
        public SilentNumericUpDown()
        {
            InitializeComponent();
        }

        public SilentNumericUpDown(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            //Suppress BEEP sound
            if((e.KeyCode == Keys.A) || (e.KeyCode == Keys.D))
                e.Handled = e.SuppressKeyPress = true;

            base.OnKeyDown(e);
        }
    }
}
