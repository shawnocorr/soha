﻿namespace Forms
{
    partial class MModeDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            this.MouseClick += new System.Windows.Forms.MouseEventHandler(MModeDisplay_MouseClick);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(MModeDisplay_PreviewKeyDown);
            this.MouseEnter += new System.EventHandler(MModeDisplay_MouseEnter);

            this.popupAddInterval = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addSystoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addDiastoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addVelocityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addChangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.popupAddInterval.SuspendLayout();
            // 
            // popupAddInterval
            // 
            this.popupAddInterval.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSystoleToolStripMenuItem,
            this.addDiastoleToolStripMenuItem});
            this.popupAddInterval.Name = "popupAddInterval";
            this.popupAddInterval.Size = new System.Drawing.Size(146, 48);
            // 
            // addSystoleToolStripMenuItem
            // 
            this.addSystoleToolStripMenuItem.Name = "addSystoleToolStripMenuItem";
            this.addSystoleToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.addSystoleToolStripMenuItem.Text = "Add Systole";
            this.addSystoleToolStripMenuItem.Click += new System.EventHandler(this.addSystoleToolStripMenuItem_Click);
            // 
            // addDiastoleToolStripMenuItem
            // 
            this.addDiastoleToolStripMenuItem.Name = "addDiastoleToolStripMenuItem";
            this.addDiastoleToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.addDiastoleToolStripMenuItem.Text = "Add Diastole";
            this.addDiastoleToolStripMenuItem.Click += new System.EventHandler(this.addDiastoleToolStripMenuItem_Click);
            // 
            // addSystoleToolStripMenuItem
            // 
            this.addVelocityToolStripMenuItem.Name = "addVelocityToolStripMenuItem";
            this.addVelocityToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.addVelocityToolStripMenuItem.Text = "Add Max Velocity";
            this.addVelocityToolStripMenuItem.Click += new System.EventHandler(addVelocityToolStripMenuItem_Click);
            // 
            // addDiastoleToolStripMenuItem
            // 
            this.addChangeToolStripMenuItem.Name = "addDiastoleToolStripMenuItem";
            this.addChangeToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.addChangeToolStripMenuItem.Text = "Add Phase Change";
            this.addChangeToolStripMenuItem.Click += new System.EventHandler(addChangeToolStripMenuItem_Click);

            this.popupAddInterval.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip popupAddInterval;
        private System.Windows.Forms.ToolStripMenuItem addSystoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addDiastoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addVelocityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addChangeToolStripMenuItem;
    }
}
