﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;
using SOHA.Library.Mathematics;

namespace SOHAControls
{
    public partial class PhaseDisplay : FunctionDisplay, IIntervalable, INotifyPropertyChanged
    {
        public event EventHandler IntervalsChanged;
        public event EventHandler ThresholdsChanged;

        protected int m_offset;
        protected int m_threshold;
        protected bool m_use_threshold;
        protected int m_zoom;
        private List<double> m_last_data;

        public IntervalCollection InitialList
        {
            get;
            set;
        }
        [Bindable(true)]
        public int Offset
        {
            get
            {
                return m_offset;
            }
            set
            {
                m_offset = value;
                OnPropertyChanged("Offset");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }
        [Bindable(true)]
        public bool UseThreshold
        {
            get
            {
                return m_use_threshold;
            }
            set
            {
                m_use_threshold = value;
                OnPropertyChanged("UseThreshold");
                OnThresholdsChanged(new EventArgs());
                this.Invalidate();
            }
        }
        [Bindable(true)]
        public int Threshold
        {
            get
            {
                return m_threshold;
            }
            set
            {
                m_threshold = value;
                OnPropertyChanged("Threshold");
                OnThresholdsChanged(new EventArgs());
                this.Invalidate();
            }
        }
        [Bindable(true)]
        public double HiCutoff
        {
            get
            {
                return m_hi_cutoff;
            }
            set
            {
                m_hi_cutoff = value;
                OnPropertyChanged("HiCutoff");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }
        [Bindable(true)]
        public double LoCutoff
        {
            get
            {
                return m_lo_cutoff;
            }
            set
            {
                m_lo_cutoff = value;
                OnPropertyChanged("LoCutoff");
                OnThresholdsChanged(new EventArgs());
                Paint();
            }
        }
        [Bindable(true)]
        public int Zoom
        {
            get
            {
                return m_zoom;
            }
            set
            {
                m_zoom = value;
                OnPropertyChanged("Zoom");
                OnZoomChanged(new EventArgs());
                this.Invalidate();
            }
        }
        
        public PhaseDisplay()
        {
            InitializeComponent();

            plot.MouseMove += new MouseEventHandler(plot_MouseMove);
            plot.MouseClick += new MouseEventHandler(plot_MouseClick);
            plot.Paint += new PaintEventHandler(plot_Paint);
        }

        void plot_MouseMove(object sender, MouseEventArgs e)
        {
            OnMouseMove(e);
        }

        void plot_MouseClick(object sender, MouseEventArgs e)
        {
            int val = (int)(e.Y / (double)plot.ClientSize.Height * 100);

            Threshold = val;

            this.Invalidate();
            //OnIntervalsChanged(new EventArgs());
            OnThresholdsChanged(new EventArgs());
        }

        void plot_Paint(object sender, PaintEventArgs e)
        {
            OnIntervalsChanged(new EventArgs());
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if(!this.DesignMode)
            {
                m_data = ImageProcessing.XExtrapolate(InitialData, m_zoom);
                base.OnPaint(e);
                OnIntervalsChanged(new EventArgs());
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
        }

        protected void OnIntervalsChanged(EventArgs e)
        {
            if (IntervalsChanged != null)
                IntervalsChanged(this, e);
        }

        public override void PreFilter(ref List<double> func)
        {
            int N = 3;
            Filter m_filter = new Filter();
            double[] data = func.ToArray();
            bool perform_filter = false;

            double wc_high = HiCutoff / Fps;
            double wc_low = LoCutoff / Fps;

            if (wc_high > 0 && wc_high < 1 && wc_low > 0 && wc_low < 1 && wc_high > wc_low)
            {
                //BandPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_low, wc_high }, FilterType.Bandpass);
                perform_filter = true;
            }
            else if (wc_high > 0 && wc_high < 1)
            {
                //HighPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_high }, FilterType.Low);
                perform_filter = true;
            }
            else if (wc_low > 0 && wc_low < 1)
            {
                //LoPass
                m_filter = IIRButterworth.Butter(N, new double[] { wc_low }, FilterType.High);
                perform_filter = true;
            }

            if (perform_filter)
                data = m_filter.DualInputZeroPhaseFilter(data);

            //if (m_lo_pass_frequency > 0)
            //{
            //    Filter filter = IIRButterworth.Butter(4, new double[] { m_lo_pass_frequency }, FilterType.Low);
            //    data = filter.DualInputZeroPhaseFilter(data);
            //}

            func = ArrayFunctions.Normalize(data.ToList());
            //func = ImageProcessing.XExtrapolate(func, m_zoom);
            m_last_data = func;
        }

        public override void AddThresholds(Graphics g, Bitmap b)
        {
            g.DrawLine(new Pen(Color.Blue, 1),
                0,
                (float)m_threshold,
                b.Width,
                (float)m_threshold);
        }

        public double[] GetValues(int x, int interval, int interval_size)
        {
            if (m_last_data == null || m_last_data.Count == 0)
                return new double[2];

            interval_size /= m_zoom;
            double[] point = new double[2];
            point[0] = (int)((x / (double)plot.Width) * interval_size) + (interval - 1) * interval_size;
            if (point[0] < m_last_data.Count)
                point[1] = m_last_data[(int)point[0]];

            return point;
        }

        #region IIntervalable Members

        public List<Interval> GetIntervals()
        {
            if(InitialList.Count == 0) return new List<Interval>();

            List<Interval> changes = new List<Interval>();
            IntervalCollection m_intervallist = InitialList;
            List<double> data = ImageProcessing.XExtrapolate(InitialData, Zoom);
            PreFilter(ref data);
            m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, Zoom);

            if(!m_use_threshold)
            {
                int start = m_intervallist.IndexOf(m_intervallist.First(x => x.IntervalType == IntervalType.Systole));

                // Start at 1; skip first interval; ensure accuracy
                // 2/15/2015 - subtract 1 from Count for straggling systolic start with no diastolic end
                for(int i = start; i < m_intervallist.Count - 1; i++)
                {
                    if(m_intervallist[i].IntervalType == IntervalType.Systole)
                    {
                        double lo = 1.0f; double hi = 0.0f; int index_lo = 0; int index_hi = 0;
                        for(int j = m_intervallist[i].Frame + m_offset; j <= m_intervallist[i + 1].Frame - m_offset; j++)
                        {
                            //Find Minimum
                            if(data[j] < lo)
                            {
                                lo = data[j];
                                index_lo = j + 1;
                            }
                        }

                        for(int j = m_intervallist[i].Frame; j <= m_intervallist[i + 1].Frame; j++)
                        {
                            //Find First Maximum
                            if(data[j] > hi)
                            {
                                hi = data[j];
                                index_hi = j;
                            }
                        }

                        if(index_lo != 0)
                        {
                            changes.Add(new Interval(index_lo / m_zoom, IntervalType.Change));
                            changes.Add(new Interval(index_hi / m_zoom, IntervalType.MaxVelocity));
                        }
                    }
                }

                //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, this.Zoom);
            }
            else
            {
                //Isometric

                //List<double> data = InitialData;
                //PreFilter(ref data);
                //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, Zoom);
                double threshold = (100 - m_threshold) / 100.0;
                int offset = m_offset;

                int total_beats = (from interval in m_intervallist
                                    where interval.IntervalType == IntervalType.Systole
                                    select interval).Count();

                IEnumerable<Interval> systoles;
                if(total_beats >= 5)
                {
                    systoles = (from interval in m_intervallist
                                where interval.IntervalType == IntervalType.Systole
                                select interval).Take(5);
                }
                else
                {
                    systoles = (from interval in m_intervallist
                                where interval.IntervalType == IntervalType.Systole
                                select interval);
                }

                foreach(Interval systole in systoles)
                {
                    Interval diastole = (from dias in m_intervallist
                                            where dias.IntervalType == IntervalType.Diastole && dias.Frame > systole.Frame
                                            select dias).First();

                    int max_index = systole.Frame + offset;

                    //if (max_index > data.Count)
                    //    max_index = data.Count - 1;

                    double max = data[max_index];
                    for(int i = systole.Frame + offset; i <= diastole.Frame - offset; i++)
                    {
                        double left = data[i - 1];
                        double right = data[i];
                        if((left < threshold && right > threshold) ||
                            (left > threshold && right < threshold))
                        {
                            //Interval new_interval = new SOHA.Library.Interval(
                            changes.Add(new Interval(i / m_zoom, IntervalType.Change));
                        }

                        if(right > max)
                        {
                            max_index = i;
                            max = data[max_index];
                        }
                    }
                    changes.Add(new Interval(max_index / m_zoom, IntervalType.MaxVelocity));
                }

                //changes = IntervalCollection.XExtrapolate(new IntervalCollection(changes), Zoom).ToList();
            }

            return changes;
        }

        public List<Interval> GetIntervals(int zoom)
        {
            List<Interval> changes = new List<Interval>();

            List<double> data = m_data;
            //data = ImageProcessing.XExtrapolate(data, zoom);
            PreFilter(ref data);

            IntervalCollection m_intervallist = InitialList;
            //m_intervallist = IntervalCollection.XExtrapolate(m_intervallist, zoom);

            if (!m_use_threshold)
            {

                int start = m_intervallist.IndexOf(m_intervallist.First(x => x.IntervalType == IntervalType.Systole));

                /* Start at 1; skip first interval; ensure accuracy */
                for (int i = start; i < m_intervallist.Count; i++)
                {
                    if (m_intervallist[i].IntervalType == IntervalType.Systole)
                    {
                        double lo = 1.0f; double hi = 0.0f; int index_lo = 0; int index_hi = 0;
                        for (int j = m_intervallist[i].Frame + m_offset; j <= m_intervallist[i + 1].Frame - m_offset; j++)
                        {

                            //Find Minimum
                            if (data[j] < lo)
                            {
                                lo = data[j];
                                index_lo = j + 1;
                            }
                        }

                        for (int j = m_intervallist[i].Frame; j <= m_intervallist[i + 1].Frame; j++)
                        {
                            //Find First Maximum
                            if (data[j] > hi)
                            {
                                hi = data[j];
                                index_hi = j + 1;
                            }
                        }

                        if (index_lo != 0)
                        {
                            changes.Add(new Interval(index_lo, IntervalType.Change));
                            changes.Add(new Interval(index_hi + 1, IntervalType.MaxVelocity));
                        }
                    }
                }
            }
            else
            {
                double threshold = (100 - m_threshold) / 100.0;
                int offset = m_offset;

                IEnumerable<Interval> systoles = (from interval in m_intervallist
                                                  where interval.IntervalType == IntervalType.Systole
                                                  select interval).Take(5);

                foreach (Interval systole in systoles)
                {
                    Interval diastole = (from dias in m_intervallist
                                         where dias.IntervalType == IntervalType.Diastole && dias.Frame > systole.Frame
                                         select dias).First();

                    int max_index = systole.Frame + offset;
                    double max = data[max_index];
                    for (int i = systole.Frame + offset; i <= diastole.Frame - offset; i++)
                    {
                        if ((data[i - 1] < threshold && data[i] > threshold) ||
                            (data[i - 1] > threshold && data[i] < threshold))
                        {
                            //Interval new_interval = new SOHA.Library.Interval(
                            changes.Add(new Interval(i, IntervalType.Change));
                        }

                        if (data[i] > max)
                        {
                            max_index = i;
                            max = data[max_index];
                        }
                    }
                    changes.Add(new Interval(max_index, IntervalType.MaxVelocity));
                }
            }


            return changes;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        protected void OnThresholdsChanged(EventArgs e)
        {
            if (ThresholdsChanged != null)
                ThresholdsChanged(this, new EventArgs());
        }

        protected void OnZoomChanged(EventArgs e)
        {

        }
    }
}
