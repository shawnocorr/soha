﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Xml;
using SOHA.Library;
using System.Drawing;
using SOHA.Library.Utilities;

namespace SOHAControls.DataDisplayControls
{

    public enum SohaDataGridViewType
    {
        MarkPair,
        ROI
    }

    public partial class SohaDataGridView : DataGridView
    {

        public event EventHandler DataUpdated;

        [AttributeProvider(typeof(IBindingList))]
        [RefreshProperties(RefreshProperties.Repaint)]
        [DefaultValue("")]
        new public object DataSource
        {
            get
            {
                return base.DataSource;
            }
            set
            {
                base.DataSource = value;
            }
        }

        [Browsable(true)]
        public SohaDataGridViewType DataGridType
        {
            get;
            set;
        }

        public SohaDataGridView()
        {
            InitializeComponent();
        }

        public SohaDataGridView(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnUserDeletedRow(DataGridViewRowEventArgs e)
        {
            OnDataUpdated(new EventArgs());
            base.OnUserDeletedRow(e);
        }

        protected override void OnCellMouseClick(DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex < 0 || e.ColumnIndex < 0)
                return;
            //SohaCollection<SohaObject> source = new SohaCollection<SohaObject>((IList<SohaObject>)this.DataSource);

            //SohaCollection<SohaObject> source = (SohaCollection<SohaObject>)this.DataSource;
            switch(DataGridType)
            {
                case SohaDataGridViewType.MarkPair:

                    #region MarkPair

                    BindingList<MarkPair> source = (BindingList<MarkPair>)this.DataSource;
                    MarkPair mp = source[e.RowIndex] as MarkPair;

                    switch (e.ColumnIndex)
                    {
                        case 0:
                            //mp.Mark1 = new Point(0, 0);
                            break;
                        case 1:
                            //mp.Mark2 = new Point(0, 0);
                            break;
                        case 2:

                            if(e.Button == System.Windows.Forms.MouseButtons.Left)
                                mp.Beat = EnumUtils.Next<BeatType>(mp.Beat);

                            if(e.Button == System.Windows.Forms.MouseButtons.Right)
                                mp.Beat = EnumUtils.Previous<BeatType>(mp.Beat);

                            break;
                        case 3:
                            if(e.Button == System.Windows.Forms.MouseButtons.Left)
                                mp.Orientation = EnumUtils.Next<OrientationType>(mp.Orientation);

                            if(e.Button == System.Windows.Forms.MouseButtons.Right)
                                mp.Orientation = EnumUtils.Previous<OrientationType>(mp.Orientation);

                            break;

                        case 4:
                            mp.Highlight = !(mp.Highlight);
                            break;
                    }

                    #endregion

                    break;
                case SohaDataGridViewType.ROI:

                    #region Roi

                    BindingList<Roi> source_roi = (BindingList<Roi>)this.DataSource;
                    Roi roi = source_roi[e.RowIndex] as Roi;

                    string name = "";
                    if (e.ColumnIndex == 0)
                    {
                        //if (InputForm.InputBox("Roi Name", "Enter Roi Name:", ref name) == DialogResult.OK)
                        //{
                        //    roi.Name = name;
                        //}
                    }

                    if (e.ColumnIndex == 5)
                    {
                        roi.Highlight = !roi.Highlight;
                    }

                    #endregion

                    break;
                default:
                    break;
            }

            OnDataUpdated(new EventArgs());
            this.Invalidate();
            this.Update();
            base.OnCellMouseClick(e);
        }

        protected virtual void OnDataUpdated(EventArgs e)
        {
            if(DataUpdated != null)
                DataUpdated(this, e);
        }
    }
}
