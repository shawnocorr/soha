﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library;

namespace SOHAControls
{
    public partial class PhaseToolStripItem : ToolStripMenuItem
    {
        private PhaseFormType m_phase;

        public PhaseFormType Phase
        {
            get
            {
                return m_phase;
            }
            set
            {
                m_phase = value;
            }
        }

        public PhaseToolStripItem()
            : base()
        {

        }

        //public PhaseToolStripItem()
        //{
        //    InitializeComponent();
        //}

        //public PhaseToolStripItem(IContainer container)
        //{
        //    container.Add(this);

        //    InitializeComponent();
        //}
    }
}
