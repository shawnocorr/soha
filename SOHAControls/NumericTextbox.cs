﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace SOHAControls
{
    public partial class NumericTextbox : TextBox
    {
        private const double DEFAULT = 3;

        private bool m_allow_negatives = true;

        public event EventHandler ValueChanged;

        [Bindable(true)]
        public bool AllowNegatives
        {
            get
            {
                return m_allow_negatives;
            }
            set
            {
                m_allow_negatives = value;
            }
        }
        [Bindable(true)]
        public bool AllowNulls { get; set; }
        
        public NumericTextbox()
        {
            InitializeComponent();
            this.Text = DEFAULT.ToString();
        }

        public NumericTextbox(IContainer container)
        {
            this.Text = DEFAULT.ToString();
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
            string decimalSeparator = numberFormatInfo.NumberDecimalSeparator;
            string groupSeparator = numberFormatInfo.NumberGroupSeparator;
            string negativeSign = numberFormatInfo.NegativeSign;

            string keyInput = e.KeyChar.ToString();

            if(Char.IsDigit(e.KeyChar))
            {
                // Digits are OK
            }
            else if(keyInput.Equals(decimalSeparator))
            {
                // Decimal separator is OK

                //Don't allow multiples
                if(this.Text.Contains(keyInput))
                    e.Handled = true;
            }
            else if(keyInput.Equals(negativeSign) && m_allow_negatives)
            {
                //Only allow negative sign as first character
                if(!string.IsNullOrEmpty(this.Text))
                    e.Handled = true;
            }
            else if(e.KeyChar == '\b')
            {
                // Backspace key is OK
            }
            else if(e.KeyChar == '\r')
            {
                // Enter key
                OnValueChanged(new EventArgs());
            }
            else
            {
                // Swallow this invalid key and beep
                e.Handled = true;
                //    MessageBeep();
            }
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
        }

        protected virtual void OnValueChanged(EventArgs e)
        {
            if(ValueChanged != null)
                ValueChanged(this, e);
        }
        
        public int IntValue
        {
            get
            {
                if(!AllowNulls && string.IsNullOrWhiteSpace(this.Text))
                    return 0;

                return Int32.Parse(this.Text);
            }
        }

        public decimal DecimalValue
        {
            get
            {
                if(!AllowNulls && string.IsNullOrWhiteSpace(this.Text))
                    return 0;

                return Decimal.Parse(this.Text);
            }
        }
    }
}
