﻿using System.Windows.Forms;
using System.Drawing;
using SOHA.SohaControls;
namespace SOHAControls
{
    partial class IntensityDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;

            //InitializeToolStripMenu();
        }

        private void InitializeToolStripMenu()
        {
            this.toolStrip = new ToolStrip();
            this.thresholdDropDown = new ToolStripDropDownButton();
            this.thresholdDropDownHost = new ToolStripDropDown();
            this.thresholdScrollBar = new VScrollBar();

            this.lblMinIntervals = new ToolStripLabel();
            this.numMinIntervals = new SilentNumericUpDown();

            this.filterDropDown = new ToolStripDropDownButton();
            this.filterDropDownHost = new ToolStripDropDown();
            this.filterScrollBar = new HScrollBar();

            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();

            this.toolStrip.AllowDrop = false;
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            this.toolStripContainer1.TopToolStripPanel.RenderMode = ToolStripRenderMode.System;
            this.toolStrip.RenderMode = ToolStripRenderMode.System;

            this.thresholdScrollBar.Margin = Padding.Empty;
            this.thresholdScrollBar.Anchor = AnchorStyles.None;
            this.thresholdScrollBar.Name = "thresholdScrollBar";
            this.thresholdDropDownHost.Items.Add(new ToolStripControlHost(this.thresholdScrollBar));
            this.thresholdDropDown.DropDown = this.thresholdDropDownHost;
            this.thresholdDropDown.BackColor = Color.Transparent;
            this.thresholdDropDown.Padding = Padding.Empty;
            this.thresholdDropDown.Text = "Threshold";

            this.filterScrollBar.Margin = Padding.Empty;
            this.filterScrollBar.Anchor = AnchorStyles.None;
            this.filterScrollBar.Name = "filterScrollBar";
            this.filterDropDownHost.Items.Add(new ToolStripControlHost(this.filterScrollBar));
            this.filterDropDown.DropDown = this.filterDropDownHost;
            this.filterDropDown.BackColor = Color.Transparent;
            this.filterDropDown.Padding = Padding.Empty;
            this.filterDropDown.Text = "Filter";

            this.lblMinIntervals.Name = "lblMinIntervals";
            this.lblMinIntervals.Text = "Min Interval";

            this.numMinIntervals.Name = "numMinIntervals";
            this.numMinIntervalsHost = new ToolStripControlHost(this.numMinIntervals);

            this.toolStrip.Dock = DockStyle.None;
            this.toolStrip.Items.AddRange(new ToolStripItem[]{
                this.thresholdDropDown,
                this.lblMinIntervals,
                this.numMinIntervalsHost,
                this.filterDropDown});
            this.toolStrip.Location = new Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new Size(95, 25);
            this.toolStrip.TabIndex = 0;

            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
        }

        protected ToolStrip toolStrip;

        protected ToolStripDropDownButton thresholdDropDown;
        protected ToolStripDropDown thresholdDropDownHost;
        protected VScrollBar thresholdScrollBar;

        protected ToolStripLabel lblMinIntervals;
        protected ToolStripControlHost numMinIntervalsHost;
        protected SilentNumericUpDown numMinIntervals;

        protected ToolStripDropDownButton filterDropDown;
        protected ToolStripDropDown filterDropDownHost;
        protected HScrollBar filterScrollBar;

        #endregion
    }
}
