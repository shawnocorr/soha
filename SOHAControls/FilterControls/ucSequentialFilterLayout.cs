﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library.GPU;
using SOHA.Library.Imaging;

namespace SOHAControls.FilterControls
{
    public partial class ucSequentialFilterLayout : UserControl
    {
        public event EventHandler Updated;

        private List<CLFilter> m_filters = new List<CLFilter>();

        public List<IFrameFunction> GetFilters
        {
            get
            {
                return flwPanelFilters.GetFilters();
            }
        }

        public ucSequentialFilterLayout()
        {
            InitializeComponent();
            Initialize();

        }

        private void Initialize()
        {
            flwPanelFilters.FilterUpdate += new EventHandler(flwPanelFilters_FilterUpdate);
        }

        void flwPanelFilters_FilterUpdate(object sender, EventArgs e)
        {
            OnUpdate(e);
        }

        protected void OnUpdate(EventArgs e)
        {
            if(Updated != null)
                Updated(this, e);
        }

        private void grpFilters_DragDrop(object sender, DragEventArgs e)
        {
            ImageFilter filter = (ImageFilter)e.Data.GetData(DataFormats.Serializable);
            m_filters.Add((CLFilter)filter);
            ucFilterDisplay display = new ucFilterDisplay(filter);
            display.Width = flwPanelFilters.Width - display.Margin.Left - display.Margin.Right;
            display.Height = 100;
            display.Tag = flwPanelFilters.Controls.Count + 1;
            display.Text = filter.ToString();

            flwPanelFilters.Controls.Add(display);

        }

        private void grpFilters_DragEnter(object sender, DragEventArgs e)
        {
            if(e.Data.GetDataPresent(DataFormats.Serializable))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }
    }
}
