﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library.Imaging;

namespace SOHAControls.FilterControls
{
    public partial class uc2DFilter : UserControl, INotifyPropertyChanged
    {

        protected ImageFilter m_filter;

        public ImageFilter Filter
        {
            get
            {
                return m_filter;
            }
            set
            {
                m_filter = value;
                OnFilterChanged();
            }
        }

        public uc2DFilter()
        {
            InitializeComponent();
        }

        protected virtual void OnFilterChanged()
        {

            if (m_filter == null)
                return;
            
            int N = m_filter.Size;

            tblMatrix.Controls.Clear();
            tblMatrix.RowStyles.Clear();
            tblMatrix.RowCount = N;
            tblMatrix.RowStyles.Add(new RowStyle(SizeType.Percent, 100f / N));
            tblMatrix.ColumnStyles.Clear();
            tblMatrix.ColumnCount = N;
            tblMatrix.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f / N));




            foreach (double d in m_filter)
            {
                TextBox text = new TextBox();
                text.Text = d.ToString();
                text.Enabled = false;
                text.Anchor = AnchorStyles.None;
                tblMatrix.Controls.Add(text);
            }

            OnPropertyChanged("Filter");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
