﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library.Imaging;

namespace SOHAControls.FilterControls
{
    public partial class ucFilterPanelDisplay : UserControl, ISelectable
    {

        private const int SHORT_HEIGHT = 20;
        private const int LARGE_HEIGHT = 150;
        private static readonly Color NORMAL_COLOR = DefaultBackColor;
        private static readonly Color HIGHTLIGHT_COLOR = Color.Yellow;


        public Matrix Matrix
        {
            get
            {
                return matrixDataGridView1.Matrix;
            }
            set
            {
                matrixDataGridView1.Matrix = value;
            }
        }

        public string Text
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public ucFilterPanelDisplay()
        {
            InitializeComponent();
        }

        public ucFilterPanelDisplay(IFrameFunction filter)
        {
            InitializeComponent();
            if(filter is ImageFilter)
            {
                Matrix = new Matrix((filter as ImageFilter).Data);
            }
            else
            {
                Matrix = null;
                matrixDataGridView1.Visible = false;
            }
            Text = filter.NickName;

            this.label1.MouseClick += new MouseEventHandler(label1_MouseClick);
        }

        void label1_MouseClick(object sender, MouseEventArgs e)
        {
            if(Matrix != null && e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                matrixDataGridView1.Visible = !matrixDataGridView1.Visible;

                //Resize control
                this.Height = matrixDataGridView1.Visible ? LARGE_HEIGHT : SHORT_HEIGHT;
            }
            else if(e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                IsHighlighted = !IsHighlighted;
                this.InvokePaint(this, new PaintEventArgs(this.CreateGraphics(), this.Bounds));
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle, Color.Yellow, ButtonBorderStyle.Solid);
            this.BackColor = IsHighlighted ? HIGHTLIGHT_COLOR : NORMAL_COLOR;

            base.OnPaint(e);
        }

        #region ISelectable Members

        public bool IsHighlighted
        {
            get;
            private set;
        }

        public bool IsSelected
        {
            get { return IsHighlighted; }
        }

        public void ToggleHighlight()
        {
            throw new NotImplementedException();
        }

        public void ToggleSelected()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
