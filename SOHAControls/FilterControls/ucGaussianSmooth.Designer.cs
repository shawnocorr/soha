﻿using SOHA.SohaControls;

namespace SOHAControls.FilterControls
{
    partial class ucGaussianSmooth
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpFilterParams = new System.Windows.Forms.GroupBox();
            this.tblParams = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numSize = new SOHA.SohaControls.SilentNumericUpDown(this.components);
            this.numSigma = new SOHAControls.NumericTextbox(this.components);
            this.grpFilterParams.SuspendLayout();
            this.tblParams.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSize)).BeginInit();
            this.SuspendLayout();
            // 
            // grpFilterParams
            // 
            this.grpFilterParams.Controls.Add(this.tblParams);
            this.grpFilterParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpFilterParams.Location = new System.Drawing.Point(0, 0);
            this.grpFilterParams.Name = "grpFilterParams";
            this.grpFilterParams.Size = new System.Drawing.Size(220, 70);
            this.grpFilterParams.TabIndex = 0;
            this.grpFilterParams.TabStop = false;
            this.grpFilterParams.Text = "Gaussian Parameters";
            // 
            // tblParams
            // 
            this.tblParams.ColumnCount = 4;
            this.tblParams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblParams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblParams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblParams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblParams.Controls.Add(this.label1, 0, 0);
            this.tblParams.Controls.Add(this.label2, 2, 0);
            this.tblParams.Controls.Add(this.numSize, 3, 0);
            this.tblParams.Controls.Add(this.numSigma, 1, 0);
            this.tblParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblParams.Location = new System.Drawing.Point(3, 16);
            this.tblParams.Name = "tblParams";
            this.tblParams.RowCount = 1;
            this.tblParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblParams.Size = new System.Drawing.Size(214, 51);
            this.tblParams.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sigma";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Size";
            // 
            // numSize
            // 
            this.numSize.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numSize.Location = new System.Drawing.Point(162, 15);
            this.numSize.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSize.Name = "numSize";
            this.numSize.Size = new System.Drawing.Size(49, 20);
            this.numSize.TabIndex = 3;
            this.numSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numSigma
            // 
            this.numSigma.AllowNegatives = true;
            this.numSigma.AllowNulls = false;
            this.numSigma.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numSigma.Location = new System.Drawing.Point(56, 15);
            this.numSigma.Name = "numSigma";
            this.numSigma.Size = new System.Drawing.Size(47, 20);
            this.numSigma.TabIndex = 4;
            this.numSigma.Text = "1.4";
            // 
            // ucGaussianSmooth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpFilterParams);
            this.Name = "ucGaussianSmooth";
            this.Size = new System.Drawing.Size(220, 70);
            this.grpFilterParams.ResumeLayout(false);
            this.tblParams.ResumeLayout(false);
            this.tblParams.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpFilterParams;
        private System.Windows.Forms.TableLayoutPanel tblParams;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private SilentNumericUpDown numSize;
        private SOHAControls.NumericTextbox numSigma;
    }
}
