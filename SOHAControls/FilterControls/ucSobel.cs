﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.SohaControls;
using SOHA.Library.Imaging;

namespace SOHAControls.FilterControls
{
    public partial class ucSobel : BFilterControl, IFilterControl
    {

        private List<Control> EXCEPTIONS = new List<Control>();

        public ucSobel()
        {
            InitializeComponent();

            if(!DesignMode)
            {
                //EXCEPTIONS.Add(ddAxis);
                //EXCEPTIONS.Add(numSize);

                this.ProcessAllControls(ctrl => ctrl.MouseDown += new MouseEventHandler(BeginDrag), typeof(SilentNumericUpDown), false);
                this.ProcessAllControls(ctrl => ctrl.MouseDown += new MouseEventHandler(BeginDrag), EXCEPTIONS);
                this.ProcessAllControls(ctrl => ctrl.MouseDoubleClick += new MouseEventHandler(Control_MouseDoubleClick), typeof(SilentNumericUpDown), false);
                this.ddAxis.SelectedValueChanged += new EventHandler(CalculateFilter);
                this.numSize.ValueChanged += new EventHandler(CalculateFilter);

                CalculateFilter(this, new EventArgs());
            }
        }


        void CalculateFilter(object sender, EventArgs e)
        {
            if(ddAxis.SelectedItem == null)
                return;
            //m_filter = ImageFilter.Gaussian2D((float)numSigma.Value, (int)numSize.Value);

            string AXIS = ddAxis.SelectedItem.ToString();

            ImageFilter filter = new ImageFilter();
            if(AXIS == "X")
                filter = new ImageFilter(EdgeDetection.SOBEL_X);
            else
                filter = new ImageFilter(EdgeDetection.SOBEL_Y);

            double A = (double)numSize.Value;

            m_filter = (ImageFilter)(A * filter);

            m_filter.NickName = "Sobel";
        }

        void Control_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //DoDragDrop(this.ToString(), DragDropEffects.Copy | DragDropEffects.Move);
        }

        void BeginDrag(object sender, MouseEventArgs e)
        {
            if(this.Filter != null)
                DoDragDrop(this.Filter, DragDropEffects.Copy | DragDropEffects.Move);
        }
    }
}
