﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using SOHA.Library.Mathematics;
using System.Windows.Forms;
using SOHA.SohaControls;
using System.Drawing;
using SOHAControls;
using SOHAControls.DataDisplayControls;
using SOHA.Library.Imaging;

namespace SOHAControls.FilterControls
{
    public partial class ucFilterDisplay : MatrixDataGridView, ISelectable
    {
        private Font m_font = new Font(FontFamily.GenericSansSerif, 8f);

        private string m_name = "";
        public string FilterName
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public ucFilterDisplay(IFrameFunction filter)
        {
            InitializeComponent();
            InitializeFilterDisplay(filter);
        }

        public ucFilterDisplay(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        private void InitializeFilterDisplay(IFrameFunction filter)
        {
            FilterName = filter.NickName;

            if(filter.FuncType == SOHA.Library.FunctionType.Filter)
                base.Matrix = (Matrix)filter;
        }
        
        protected override void OnMouseEnter(EventArgs e)
        {
            m_isHighlighted = true;

            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            m_isHighlighted = false;

            base.OnMouseLeave(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            m_isSelected = !m_isSelected;

            base.OnMouseClick(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }

        #region IFilterControl Members

        private bool m_isHighlighted = false;
        private bool m_isSelected = false;

        public bool IsHighlighted
        {
            get { return m_isHighlighted; }
        }

        public bool IsSelected
        {
            get { return m_isSelected; }
        }

        public void ToggleHighlight()
        {
            m_isHighlighted = !m_isHighlighted;

            this.BackColor = (m_isHighlighted ? Color.Gray : DefaultBackColor);

            if(m_isHighlighted)
            {

            }
            else
            {

            }
        }

        public void ToggleSelected()
        {
            m_isSelected = !m_isSelected;

            this.BackColor = (m_isSelected ? Color.Black : DefaultBackColor);

            if(m_isSelected)
            {

            }
            else
            {

            }
        }

        #endregion
    }
}
