﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOHA.Library.Mathematics;
using SOHA.Library.Imaging;

namespace SOHAControls.FilterControls
{
    public interface IFilterControl : ISelectable
    {
        IFrameFunction Filter { get; }
    }
}
