﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHAControls.FilterControls
{
    public interface ISelectable
    {
        bool IsHighlighted { get; }
        bool IsSelected { get; }

        void ToggleHighlight();
        void ToggleSelected();
    }
}
