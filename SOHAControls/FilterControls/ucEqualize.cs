﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Imaging;
using SOHA.Library.Imaging.StaticFrameFunctions;

namespace SOHAControls.FilterControls
{
    public partial class ucEqualize : BFilterControl, IFilterControl
    {
        public ucEqualize()
        {
            InitializeComponent();

            if(!DesignMode)
            {
                this.ProcessAllControls(ctrl => ctrl.MouseDown += new MouseEventHandler(BeginDrag));
                //this.ProcessAllControls(ctrl => ctrl.MouseDoubleClick += new MouseEventHandler(Control_MouseDoubleClick), typeof(SilentNumericUpDown), false);
            }

            CalculateFilter(this, new EventArgs());
        }

        void CalculateFilter(object sender, EventArgs e)
        {
            m_filter = new EqualizeFrameFunction();
            m_filter.NickName = "Equalize";
        }

        void Control_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //DoDragDrop(this.ToString(), DragDropEffects.Copy | DragDropEffects.Move);
        }

        void BeginDrag(object sender, MouseEventArgs e)
        {
            DoDragDrop(this.Filter, DragDropEffects.Copy | DragDropEffects.Move);
        }
    }
}
