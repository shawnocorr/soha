﻿namespace SOHAControls.FilterControls
{
    partial class ucSequentialFilterLayout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpFilters = new System.Windows.Forms.GroupBox();
            this.flwPanelFilters = new SOHAControls.FilterDisplayPanel(this.components);
            this.grpFilters.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpFilters
            // 
            this.grpFilters.Controls.Add(this.flwPanelFilters);
            this.grpFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpFilters.Location = new System.Drawing.Point(0, 0);
            this.grpFilters.Name = "grpFilters";
            this.grpFilters.Size = new System.Drawing.Size(430, 600);
            this.grpFilters.TabIndex = 2;
            this.grpFilters.TabStop = false;
            this.grpFilters.Text = "Filters (Sequential)";
            this.grpFilters.DragDrop += new System.Windows.Forms.DragEventHandler(this.grpFilters_DragDrop);
            this.grpFilters.DragEnter += new System.Windows.Forms.DragEventHandler(this.grpFilters_DragEnter);
            // 
            // flwPanelFilters
            // 
            this.flwPanelFilters.AllowDrop = true;
            this.flwPanelFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flwPanelFilters.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flwPanelFilters.Location = new System.Drawing.Point(3, 16);
            this.flwPanelFilters.Name = "flwPanelFilters";
            this.flwPanelFilters.Size = new System.Drawing.Size(424, 581);
            this.flwPanelFilters.TabIndex = 0;
            // 
            // ucSequentialFilterLayout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpFilters);
            this.Name = "ucSequentialFilterLayout";
            this.Size = new System.Drawing.Size(430, 600);
            this.grpFilters.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpFilters;
        private FilterDisplayPanel flwPanelFilters;
    }
}
