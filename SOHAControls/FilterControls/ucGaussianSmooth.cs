﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.SohaControls;
using SOHA.Library.Imaging;

namespace SOHAControls.FilterControls
{
    public partial class ucGaussianSmooth : BFilterControl, IFilterControl
    {
        public ucGaussianSmooth()
        {
            InitializeComponent();

            if(!DesignMode)
            {
                this.ProcessAllControls(ctrl => ctrl.MouseDown += new MouseEventHandler(BeginDrag), typeof(SilentNumericUpDown), false);
                this.ProcessAllControls(ctrl => ctrl.MouseDoubleClick += new MouseEventHandler(Control_MouseDoubleClick), typeof(SilentNumericUpDown), false);
                this.numSigma.ValueChanged += new EventHandler(CalculateFilter);
                this.numSize.ValueChanged += new EventHandler(CalculateFilter);
                CalculateFilter(this, new EventArgs());
            }
        }

        void CalculateFilter(object sender, EventArgs e)
        {
            m_filter = ImageFilter.Gaussian2D((float)numSigma.DecimalValue, (int)numSize.Value);
            m_filter.NickName = "Gauss";
        }

        void Control_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //DoDragDrop(this.ToString(), DragDropEffects.Copy | DragDropEffects.Move);
        }

        void BeginDrag(object sender, MouseEventArgs e)
        {
            DoDragDrop(this.Filter, DragDropEffects.Copy | DragDropEffects.Move);
        }
    }
}
