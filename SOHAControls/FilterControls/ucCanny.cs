﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.Library.Imaging;
using SOHA.Library.Vision;

namespace SOHAControls.FilterControls
{
    public partial class ucCanny : BFilterControl, IFilterControl
    {
        public ucCanny()
        {
            InitializeComponent();

            if(!DesignMode)
            {
                this.ProcessAllControls(ctrl => ctrl.MouseDown += new MouseEventHandler(BeginDrag));
                //this.ProcessAllControls(ctrl => ctrl.MouseDoubleClick += new MouseEventHandler(Control_MouseDoubleClick), typeof(SilentNumericUpDown), false);
            }

            CalculateFilter(this, new EventArgs());
        }
        
        void CalculateFilter(object sender, EventArgs e)
        {
            m_filter = new Canny(1.4f,5);
            m_filter.NickName = "Canny";
        }

        void Control_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //DoDragDrop(this.ToString(), DragDropEffects.Copy | DragDropEffects.Move);
        }

        void BeginDrag(object sender, MouseEventArgs e)
        {
            DoDragDrop(this.Filter, DragDropEffects.Copy | DragDropEffects.Move);
        }
    }

}
