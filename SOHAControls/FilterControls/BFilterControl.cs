﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SOHA.Library.Mathematics;
using SOHA.SohaControls;
using SOHA.Library.Imaging;

namespace SOHAControls.FilterControls
{
    public partial class BFilterControl : UserControl, IFilterControl
    {

        protected IFrameFunction m_filter;
        protected SohaToolTip m_tooltip;

        public BFilterControl()
        {
            InitializeComponent();

        }

        #region IFilterControl Members

        public IFrameFunction Filter
        {
            get 
            { 
                return m_filter; 
            }
            protected set
            {
                m_filter = value;
            }
        }

        public bool IsHighlighted
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsSelected
        {
            get { throw new NotImplementedException(); }
        }

        public void ToggleHighlight()
        {
            throw new NotImplementedException();
        }

        public void ToggleSelected()
        {
            throw new NotImplementedException();
        }

        IFrameFunction IFilterControl.Filter
        {
            get { return m_filter; }
        }

        #endregion
    }
}
