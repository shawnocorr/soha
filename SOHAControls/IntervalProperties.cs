﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOHAControls
{
    public class IntervalProperties
    {
        public event EventHandler IntervalChanged;

        protected int m_interval_size;
        protected int m_interval;

        private int m_max_interval;

        public int IntervalSize
        {
            get
            {
                return m_interval_size;
            }
            set
            {
                m_interval_size = value;
                OnIntervalChanged(new EventArgs());
            }
        }
        public int Interval
        {
            get
            {
                return m_interval;
            }
            set
            {
                m_interval = value;
                OnIntervalChanged(new EventArgs());
            }
        }

        public IntervalProperties() 
        {
            m_interval = 1;
            m_interval_size = 1000;
        }

        protected void OnIntervalChanged(EventArgs e)
        {
            if (IntervalChanged != null)
                IntervalChanged(this, e);
        }
    }
}
