﻿using System.Windows.Forms;
using System.Drawing;
using SOHA.SohaControls;
namespace SOHAControls
{
    partial class PhaseDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;

            //InitializeToolStripMenu();
        }

        private void InitializeToolStripMenu()
        {
            this.toolStrip = new ToolStrip();

            this.filterDropDown = new ToolStripDropDownButton();
            this.filterDropDownHost = new ToolStripDropDown();
            this.filterScrollBar = new HScrollBar();

            this.lblNumOffset = new ToolStripLabel();
            this.numOffset = new SilentNumericUpDown();

            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();

            this.toolStrip.AllowDrop = false;
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            this.toolStripContainer1.TopToolStripPanel.RenderMode = ToolStripRenderMode.System;
            this.toolStrip.RenderMode = ToolStripRenderMode.System;

            this.filterScrollBar.Margin = Padding.Empty;
            this.filterScrollBar.Anchor = AnchorStyles.None;
            this.filterScrollBar.Name = "filterScrollBar";
            this.filterDropDownHost.Items.Add(new ToolStripControlHost(this.filterScrollBar));
            this.filterDropDown.DropDown = this.filterDropDownHost;
            this.filterDropDown.BackColor = Color.Transparent;
            this.filterDropDown.Padding = Padding.Empty;
            this.filterDropDown.Text = "Filter";

            this.lblNumOffset.Name = "lblNumOffset";
            this.lblNumOffset.Text = "Offset";

            this.numOffset.Name = "numOffset";
            this.numOffsetHost = new ToolStripControlHost(this.numOffset);

            this.toolStrip.Dock = DockStyle.None;
            this.toolStrip.Items.AddRange(new ToolStripItem[]{
                this.filterDropDown,
                this.lblNumOffset,
                this.numOffsetHost});
            this.toolStrip.Location = new Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new Size(95, 25);
            this.toolStrip.TabIndex = 0;

            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
        }

        protected ToolStrip toolStrip;

        protected ToolStripDropDownButton filterDropDown;
        protected ToolStripDropDown filterDropDownHost;
        protected HScrollBar filterScrollBar;

        protected ToolStripLabel lblNumOffset;
        protected ToolStripControlHost numOffsetHost;
        protected SilentNumericUpDown numOffset;

        #endregion
    }
}
