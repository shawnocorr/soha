﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHAControls
{
    public static class Extensions
    {

        public static void ProcessAllControls(this Control control, Action<Control> action)
        {
            foreach(Control ctrl in control.Controls)
            {
                ProcessAllControls(ctrl, action);
                action(ctrl);
            }
        }

        public static void ProcessAllControls(this Control control, Action<Control> action, Type type, bool doAction)
        {
            foreach(Control ctrl in control.Controls)
            {
                ProcessAllControls(ctrl, action);
                if(doAction && ctrl.GetType() == type)
                    action(ctrl);

                if(!doAction && ctrl.GetType() != type)
                    action(ctrl);
            }
        }

        public static void ProcessAllControls(this Control control, Action<Control> action, List<Control> exceptions)
        {
            foreach(Control ctrl in control.Controls)
            {
                ProcessAllControls(ctrl, action, exceptions);
                if(!exceptions.Contains(ctrl))
                    action(ctrl);
            }
        }

        public static IEnumerable<Control> GetAllControls(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                .Concat(controls)
                .Where(c => c.GetType() == type);
        }

    }
}
