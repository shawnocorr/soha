﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SOHAControls
{
    public partial class FunctionDisplay : UserControl
    {
        public event EventHandler DataChanged;

        public List<double> InitialData { protected get; set; }

        protected double[] m_last_data;
        protected List<double> m_data;
        protected IntervalProperties m_interval = new IntervalProperties();
        protected double m_hi_cutoff;
        protected double m_lo_cutoff;
        protected double m_fps;
        //protected bool m_flip = false;

        public List<double> Data
        {
            get
            {
                return m_data;
            }
            set
            {
                m_data = value;
            }
        }
        public IntervalProperties Interval
        {
            get
            {
                return m_interval;
            }
            set
            {
                m_interval = value;
                m_interval.IntervalChanged +=new EventHandler(m_interval_IntervalChanged);
            }
        }
        public double[] LastData { get { return m_last_data; } }
        public double Fps
        {
            get
            {
                return m_fps;
            }
            set
            {
                m_fps = value;
            }
        }

        public FunctionDisplay()
        {
            InitializeComponent();

            plot.BackColor = Color.White;
            plot.BorderStyle = BorderStyle.FixedSingle;

            //m_interval.IntervalChanged += new EventHandler(m_interval_IntervalChanged);
        }

        void m_interval_IntervalChanged(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        protected virtual void OnDataChanged(EventArgs e)
        {
            if (DataChanged != null)
                DataChanged(this, e);
        }

        protected virtual void Graph(List<double> f, List<double> t)
        {            
            Bitmap b = new Bitmap(m_interval.IntervalSize, 100);
            Graphics g = Graphics.FromImage((Image)b);
            Pen p = new Pen(Color.Black, 1);
            g.Clear(Color.White);

            int start = (m_interval.Interval - 1) * m_interval.IntervalSize;
            int end = m_interval.Interval * m_interval.IntervalSize - 1;

            PreFilter(ref f);

            double[] data = new double[f.Count];
            f.CopyTo(data);

            for (int i = start + 1; i < end && i < data.Length; i++)
            {
                g.DrawLine(p,
                    ((i - 1) % m_interval.IntervalSize),
                    100 - (float)data[i - 1] * 100,
                    (i % m_interval.IntervalSize),
                    100 - (float)data[i] * 100);
            }

            AddThresholds(g,b);

            plot.BackgroundImage = b;
            plot.BackgroundImageLayout = ImageLayout.Stretch;

            g.Dispose();
        }

        protected new void Paint()
        {
            if (m_data == null || m_interval == null)
                return;

            double[] tmpdata = new double[m_data.Count];
            m_data.CopyTo(tmpdata);

            Graph(tmpdata.ToList(), new List<double>());
        }

        protected override void OnResize(EventArgs e)
        {
            plot.Location = new Point(0, 0);
            plot.ClientSize = new Size(this.Size.Width, this.Size.Height - toolStripContainer1.TopToolStripPanel.Height);
            plot.Invalidate();
            this.Invalidate();
            //base.OnResize(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (m_data == null || m_interval == null)
                return;

            double[] tmpdata = new double[m_data.Count];
            m_data.CopyTo(tmpdata);

            Graph(tmpdata.ToList(), new List<double>());
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
        }

        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            base.OnInvalidated(e);
        }

        public virtual void PreFilter(ref List<double> func)
        {

        }

        public virtual void AddThresholds(Graphics g, Bitmap b)
        {

        }
    }
}
