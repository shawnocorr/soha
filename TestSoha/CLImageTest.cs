﻿using SOHA.Library.GPU;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;

namespace TestSoha
{
    
    
    /// <summary>
    ///This is a test class for CLImageTest and is intended
    ///to contain all CLImageTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CLImageTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CLImage Constructor
        ///</summary>
        [TestMethod()]
        public void CLImageConstructorTest()
        {
            Bitmap bmp = null; // TODO: Initialize to an appropriate value
            CLImage target = new CLImage(bmp);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ReadToLocalData
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHA.Library.GPU.dll")]
        public void ReadToLocalDataTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            CLImage_Accessor target = new CLImage_Accessor(param0); // TODO: Initialize to an appropriate value
            Bitmap bmp = null; // TODO: Initialize to an appropriate value
            target.ReadToLocalData(bmp);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Height
        ///</summary>
        [TestMethod()]
        public void HeightTest()
        {
            Bitmap bmp = null; // TODO: Initialize to an appropriate value
            CLImage target = new CLImage(bmp); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Height;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Width
        ///</summary>
        [TestMethod()]
        public void WidthTest()
        {
            Bitmap bmp = null; // TODO: Initialize to an appropriate value
            CLImage target = new CLImage(bmp); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Width;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CLImage Constructor
        ///</summary>
        [TestMethod()]
        public void CLImageConstructorTest1()
        {
            Bitmap bmp = null; // TODO: Initialize to an appropriate value
            CLImage target = new CLImage(bmp);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ReadToLocalData
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SOHA.Library.GPU.dll")]
        public void ReadToLocalDataTest1()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            CLImage_Accessor target = new CLImage_Accessor(param0); // TODO: Initialize to an appropriate value
            Bitmap bmp = null; // TODO: Initialize to an appropriate value
            target.ReadToLocalData(bmp);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Height
        ///</summary>
        [TestMethod()]
        public void HeightTest1()
        {
            Bitmap bmp = null; // TODO: Initialize to an appropriate value
            CLImage target = new CLImage(bmp); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Height;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Width
        ///</summary>
        [TestMethod()]
        public void WidthTest1()
        {
            Bitmap bmp = null; // TODO: Initialize to an appropriate value
            CLImage target = new CLImage(bmp); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Width;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
