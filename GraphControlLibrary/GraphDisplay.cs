﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using OpticalHeartBeatAnalysis.Mathematics.Filtering;
using Mathematics;

namespace GraphControlLibrary
{
    public abstract partial class GraphDisplay : PictureBox
    {
        protected List<double> m_data;
        protected List<int> m_thresholds;
        protected IntervalProperties m_interval;
        protected bool m_flip = false;
        protected Filter m_filter;

        public event EventHandler DataChanged;

        public List<double> Data
        {
            get
            {
                return m_data;
            }
            set
            {
                m_data = value;
            }
        }
        public IntervalProperties Interval
        {
            get
            {
                return m_interval;
            }
            set
            {
                m_interval = value;
                m_interval.IntervalChanged += new EventHandler(m_interval_IntervalChanged);
            }
        }
                
        public bool Flip
        {
            get
            {
                return m_flip;
            }
            set
            {
                m_flip = value;
            }
        }

        public GraphDisplay()
        {
            InitializeComponent();

            Initialize();
        }

        public GraphDisplay(IContainer container)
        {
            container.Add(this);

            InitializeComponent();

            Initialize();
        }

        protected virtual void Initialize()
        {
            this.BackColor = Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        }

        void m_interval_IntervalChanged(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        protected virtual void OnDataChanged(EventArgs e)
        {
            if (DataChanged != null)
                DataChanged(this, e);
        }

        protected virtual void Graph()
        {
            Bitmap b = new Bitmap(m_interval.IntervalSize, 100);
            Graphics g = Graphics.FromImage((Image)b);
            Pen p = new Pen(Color.Black, 1);
            g.Clear(Color.White);

            int start = (m_interval.Interval - 1) * m_interval.IntervalSize;
            int end = m_interval.Interval * m_interval.IntervalSize - 1;

            PreFilter();

            double[] data = new double[m_data.Count];
            m_data.CopyTo(data);
            //if (m_flip)
            //    _1DFunctions<double>.Invert(ref data);

            for (int i = start + 1; i < end && i < data.Length; i++)
            {
                g.DrawLine(p,
                    ((i - 1) % m_interval.IntervalSize),
                    100 - (float)data[i - 1] * 100,
                    (i % m_interval.IntervalSize),
                    100 - (float)data[i] * 100);
            }

            AddThresholds(g, b);

            this.BackgroundImage = b;
            this.BackgroundImageLayout = ImageLayout.Stretch;

            g.Dispose();
        }

        protected new void Paint()
        {
            if (m_data == null || m_interval == null)
                return;

            double[] tmpdata = new double[m_data.Count];
            m_data.CopyTo(tmpdata);

            Graph();
        }

        protected override void OnResize(EventArgs e)
        {
            //plot.Location = new Point(0, 0);
            //plot.ClientSize = new Size(this.Size.Width, this.Size.Height - toolStripContainer1.TopToolStripPanel.Height);
            //plot.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (m_data == null)
                return;

            double[] tmpdata = new double[m_data.Count];
            m_data.CopyTo(tmpdata);

            Graph();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
        }

        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            base.OnInvalidated(e);
        }

        protected virtual void PreFilter()
        {
            double[] data = m_data.ToArray();

            if (m_filter != null)
                data = m_filter.DualInputZeroPhaseFilter(data);

            m_data = data.ToList<double>();

            m_data = ArrayFunctions.Normalize(m_data);
        }

        public abstract void AddThresholds(Graphics g, Bitmap b);
    }
}
